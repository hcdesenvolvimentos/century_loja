<?php

/**
 * Plugin Name: Base Century Sports
 * Description: Controle base do tema Century Sports.
 * Version: 0.1
 * Author: Agência HC Desenvolvimentos
 * Author URI: http://hcdesenvolvimentos.com.br
 * Licence: GPL2
 */


	function baseCenturySports () {

		// TIPOS DE CONTEÚDO
		conteudosCenturySports();

		taxonomiaCenturySports();

		metaboxesCenturySports();
	}

	/****************************************************
	* TIPOS DE CONTEÚDO
	*****************************************************/

	function conteudosCenturySports (){

		// TIPOS DE CONTEÚDO
		tipoDestaque();

		// TIPOS DE CONTEÚDO
		tipoPromocao();

		// TIPO DE PERGUNTAS
		tipoPerguntasFrequentes();
		
		// TIPO COMO COMPRAR
		tipoComoComprar();


		/* ALTERAÇÃO DO TÍTULO PADRÃO */
		add_filter( 'enter_title_here', 'trocarTituloPadrao' );
		function trocarTituloPadrao($titulo){

			switch (get_current_screen()->post_type) {

				case 'destaque':
					$titulo = 'Título do destaque';
				break;
				
				default:
				break;
			}

		    return $titulo;

		}

	}

	// CUSTOM POST TYPE DESTAQUES
	function tipoDestaque() {

		$rotulosDestaque = array(
								'name'               => 'Destaques',
								'singular_name'      => 'destaque',
								'menu_name'          => 'Destaques',
								'name_admin_bar'     => 'Destaques',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo destaque',
								'new_item'           => 'Novo destaque',
								'edit_item'          => 'Editar destaque',
								'view_item'          => 'Ver destaque',
								'all_items'          => 'Todos os destaques',
								'search_items'       => 'Buscar destaque',
								'parent_item_colon'  => 'Dos destaques',
								'not_found'          => 'Nenhum destaque cadastrado.',
								'not_found_in_trash' => 'Nenhum destaque na lixeira.'
							);

		$argsDestaque 	= array(
								'labels'             => $rotulosDestaque,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'destaque' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('destaque', $argsDestaque);

	}

	// CUSTOM POST TYPE PROMOÇÃO
	function tipoPromocao() {

		$rotulosPromocao = array(
								'name'               => 'banners de promoções',
								'singular_name'      => 'banner de promoção',
								'menu_name'          => 'Banners de promoções',
								'name_admin_bar'     => 'banners de promoções',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo banner de promoção',
								'new_item'           => 'Novo banner de promoção',
								'edit_item'          => 'Editar banner de promoção',
								'view_item'          => 'Ver banner de promoção',
								'all_items'          => 'Todos os banners de promoções',
								'search_items'       => 'Buscar banner de promoção',
								'parent_item_colon'  => 'Dos banners de promoções',
								'not_found'          => 'Nenhum banner de promoção cadastrado.',
								'not_found_in_trash' => 'Nenhum banner de promoção na lixeira.'
							);

		$argsPromocao 	= array(
								'labels'             => $rotulosPromocao,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'banner-promocao' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','thumbnail')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('promocao', $argsPromocao);

	}

	// CUSTOM POST TYPE PERGUNTAS FREQUENTES
	function tipoPerguntasFrequentes() {

		$rotulosPerguntasFrequentes = array(
								'name'               => 'Perguntas frequentes',
								'singular_name'      => 'perguntas_frequentes',
								'menu_name'          => 'Perguntas frequentes',
								'name_admin_bar'     => 'Perguntas frequentes',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar nova pergunta',
								'new_item'           => 'Nova pergunta',
								'edit_item'          => 'Editar pergunta',
								'view_item'          => 'Ver pergunta',
								'all_items'          => 'Todas as perguntas',
								'search_items'       => 'Buscar pergunta frequente',
								'parent_item_colon'  => 'Das perguntas frequentes',
								'not_found'          => 'Nenhuma pergunta cadastrado.',
								'not_found_in_trash' => 'Nenhuma pergunta na lixeira.'
							);

		$argsPerguntasFrequentes 	= array(
								'labels'             => $rotulosPerguntasFrequentes,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'perguntas_frequentes' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('perguntas_frequentes', $argsPerguntasFrequentes);

	}

	// CUSTOM POST TYPE COMO COMPRAR
	function tipoComoComprar() {

		$rotulosComoComprar = array(
								'name'               => 'Como comprar',
								'singular_name'      => 'como_comprar',
								'menu_name'          => 'Como comprar',
								'name_admin_bar'     => 'Como comprar',
								'add_new'            => 'Adicionar novo',
								'add_new_item'       => 'Adicionar novo como comprar',
								'new_item'           => 'Nova pergunta',
								'edit_item'          => 'Editar pergunta',
								'view_item'          => 'Ver pergunta',
								'all_items'          => 'Todas as perguntas',
								'search_items'       => 'Buscar como comprar',
								'parent_item_colon'  => 'Das perguntas',
								'not_found'          => 'Nenhuma pergunta cadastrado.',
								'not_found_in_trash' => 'Nenhuma pergunta na lixeira.'
							);

		$argsComoComprar 	= array(
								'labels'             => $rotulosComoComprar,
								'public'             => true,
								'publicly_queryable' => true,
								'show_ui'            => true,
								'show_in_menu'       => true,
								'menu_position'		 => 4,
								'menu_icon'          => 'dashicons-megaphone',
								'query_var'          => true,
								'rewrite'            => array( 'slug' => 'como_comprar' ),
								'capability_type'    => 'post',
								'has_archive'        => true,
								'hierarchical'       => false,
								'supports'           => array( 'title','editor')
							);

		// REGISTRA O TIPO CUSTOMIZADO
		register_post_type('como_comprar', $argsComoComprar);

	}


	/****************************************************
	* TAXONOMIA
	*****************************************************/
	function taxonomiaCenturySports () {		
		//taxonomiaCategoriaDestaque();
	}
		// TAXONOMIA DE DESTAQUE
		function taxonomiaCategoriaDestaque() {

			$rotulosCategoriaDestaque = array(
				'name'              => 'Categorias de destaque',
				'singular_name'     => 'Categoria de destaque',
				'search_items'      => 'Buscar categorias de destaque',
				'all_items'         => 'Todas as categorias de destaque',
				'parent_item'       => 'Categoria de destaque pai',
				'parent_item_colon' => 'Categoria de destaque pai:',
				'edit_item'         => 'Editar categoria de destaque',
				'update_item'       => 'Atualizar categoria de destaque',
				'add_new_item'      => 'Nova categoria de destaque',
				'new_item_name'     => 'Nova categoria',
				'menu_name'         => 'Categorias de destaque',
				);

			$argsCategoriaDestaque 		= array(
				'hierarchical'      => true,
				'labels'            => $rotulosCategoriaDestaque,
				'show_ui'           => true,
				'show_admin_column' => true,
				'query_var'         => true,
				'rewrite'           => array( 'slug' => 'categoria-destaque' ),
				);

			register_taxonomy( 'categoriaDestaque', array( 'destaque' ), $argsCategoriaDestaque );

		}			

    /****************************************************
	* META BOXES
	*****************************************************/
	function metaboxesCenturySports(){

		add_filter( 'rwmb_meta_boxes', 'registraMetaboxes' );

	}

		function registraMetaboxes( $metaboxes ){

			$prefix = 'CenturySports_';

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxDestaqueInicial',
				'title'			=> 'Detalhes do Destaque',
				'pages' 		=> array( 'destaque' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link destaque: ',
						'id'    => "{$prefix}destaque_link",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxPromocao',
				'title'			=> 'Detalhes do banner',
				'pages' 		=> array( 'promocao' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Link banner: ',
						'id'    => "{$prefix}banner_link",
						'desc'  => '',
						'type'  => 'text',
					),	
					
									
				),
			);

			// METABOX DE DESTAQUE
			$metaboxes[] = array(

				'id'			=> 'detalhesMetaboxProduto',
				'title'			=> 'Foto zoom produto',
				'pages' 		=> array( 'product' ),
				'context' 		=> 'normal',
				'priority' 		=> 'high',
				'autosave' 		=> false,
				'fields' 		=> array(

					array(
						'name'  => 'Foto zoom produto: ',
						'id'    => "{$prefix}produto_zoom",
						'desc'  => '',
						'type'  		   => 'image_advanced',
                        'max_file_uploads' => 1
					),	
					
									
				),
			);

			return $metaboxes;
		}

		function metaboxjs(){

			global $post;
			$template = get_post_meta($post->ID, '_wp_page_template', true);
			$template = explode('/', $template);
			$template = explode('.', $template[1]);
			$template = $template[0];

			if($template != ''){
				wp_enqueue_script( 'metabox-js', get_template_directory_uri() 	. '/js/metabox_' . $template . '.js', array('jquery'), '1.0', true);
			}
		}

	/****************************************************
	* SHORTCODES
	*****************************************************/
	function shortcodesCenturySports(){

	}

	/****************************************************
	* ATALHOS VISUAL COMPOSER
	*****************************************************/
	function visualcomposerCenturySports(){

	    if (class_exists('WPBakeryVisualComposer')){

		}

	}

  	/****************************************************
	* AÇÕES
	*****************************************************/

	// INICIA A FUNÇÃO PRINCIPAL
	add_action('init', 'baseCenturySports');

	// IMPLEMENTAÇÃO ADICIONAL PARA EXIBIR/OCULTAR META BOX DE PÁGINAS SIMPLES
	add_action( 'add_meta_boxes', 'metaboxjs');

	// FLUSHS
	function rewrite_flush() {

    	baseCenturySports();

   		flush_rewrite_rules();
	}

	register_activation_hook( __FILE__, 'rewrite_flush' );