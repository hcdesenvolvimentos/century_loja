<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/archive-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.4.0
 */

defined( 'ABSPATH' ) || exit;

get_header( 'shop' );

?>
<!-- PÁGINA DE CATEGORIAS -->
<div class="pg pg-categorias">
	<div class="containerFull">
		<?php do_action( 'woocommerce_before_main_content' ); ?>
		<div class="row">
			<!-- SIDEBAR -->
			<div class="col-sm-3">
				<h1 class="titulo"><?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) {woocommerce_page_title();} ?></h1>
				<div id="sidebar">
					<?php include (TEMPLATEPATH . '/templates/sidebar_loja.php');  ?>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="banner_cat">
					<figure>
						<?php if (function_exists('z_taxonomy_image')) z_taxonomy_image(); ?>
					</figure>
				</div>
				<div class="nav-paginador">
					<div class="paginador">
						<?php woocommerce_pagination(); ?>
					</div>
					
					<div class="filtro">
						<?php woocommerce_catalog_ordering(); ?>
					</div>
				</div>
				<div class="lista-produtos">

					<ul class="produtos">
							<?php

								if ( wc_get_loop_prop( 'total' ) ) {
									while ( have_posts() ) {
										the_post();

										/**
										 * Hook: woocommerce_shop_loop.
										 *
										 * @hooked WC_Structured_Data::generate_product_data() - 10
										 */
										
										include (TEMPLATEPATH . '/templates/spot_produto_categoria.php'); 
									}
								}

							
								
							?>
					</ul>
			</div>
				<div class="nav-paginador final">
					<div class="paginador">
						<?php woocommerce_pagination(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php

get_footer( 'shop' );
