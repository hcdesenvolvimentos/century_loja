<?php
/**
 * The Template for displaying products in a product category. Simply includes the archive template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/taxonomy-product_cat.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header( 'shop' );
?>

<!-- PÁGINA DE CATEGORIAS -->
<div class="pg pg-categorias">
	<div class="containerFull">
		<?php do_action( 'woocommerce_before_main_content' ); ?>
		<div class="row">
			<!-- SIDEBAR -->
			<div class="col-sm-3">
				<h1 class="titulo"><?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) {woocommerce_page_title();} ?></h1>
				<div id="sidebar">
					<?php include (TEMPLATEPATH . '/templates/sidebar_loja.php');  ?>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="banner_cat">
					<figure>
						<?php if (function_exists('z_taxonomy_image')) z_taxonomy_image(); ?>
					</figure>
				</div>
				<div class="nav-paginador">
					<div class="paginador">
						<?php woocommerce_pagination(); ?>
					</div>
					<div class="filtro">
						<form action="">
							<select name="filtro" id="filtro">
								<option value="Mais vendidos">Mais vendidos</option>
								<option value="Lançamentos">Lançamentos</option>
								<option value="Mais antigos">Mais antigos</option>
							</select>
						</form>
					</div>
				</div>
				<div class="lista-produtos">
					<ul class="produtos">
					<?php 
						woocommerce_product_loop_start();

						if ( wc_get_loop_prop( 'total' ) ) {
							while ( have_posts() ) {
								the_post();

								/**
								 * Hook: woocommerce_shop_loop.
								 *
								 * @hooked WC_Structured_Data::generate_product_data() - 10
								 */
								do_action( 'woocommerce_shop_loop' );

								include (TEMPLATEPATH . '/templates/spot_produto_categoria.php'); 
							}
						}

						woocommerce_product_loop_end(); 
						?>
					</ul>
				</div>
				<div class="nav-paginador final">
					<div class="paginador">
						<?php woocommerce_pagination(); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer( 'shop' ); ?>