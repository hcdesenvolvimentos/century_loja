<?php
/**
 * Cross-sells
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/cart/cross-sells.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see 	    https://docs.woocommerce.com/document/template-structure/
 * @package 	WooCommerce/Templates
 * @version     3.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( $cross_sells ) : ?>

	<div class="cross-sells">

		<h2><?php _e( 'You may be interested in&hellip;', 'woocommerce' ); ?></h2>

	<section class="relacionados">
			<h6 class="hidden">SEÇÃO DE PRODUTOS RELACIONADOS</h6>
			<div class="lista-produtos">
				
				<ul class="carrossel carrossel-relacionados">

			<?php foreach ( $cross_sells as $cross_sell ) : ?>

				<?php
				 	$post_object = get_post( $cross_sell->get_id() );

					setup_postdata( $GLOBALS['post'] =& $post_object );

					//TEMPLATE SPOT CARROSSEL
			include (TEMPLATEPATH . '/templates/spot_produto_categoria.php');  ?>

			<?php endforeach; ?>

	</ul>
				<div class="setas-carrossel-relacionados">
					<span class="setas prevRelacionados"><img src="<?php echo get_template_directory_uri(); ?>/img/angle-up-p.svg" alt="prevRelacionados"></span>
					<span class="setas nextRelacionados"><img src="<?php echo get_template_directory_uri(); ?>/img/angle-down-p.svg" alt="nextRelacionados"></span>
				</div>
			</div>
		</section>

	</div>

<?php endif;

wp_reset_postdata();
