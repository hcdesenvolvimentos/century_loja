<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/content-single-product.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce/Templates
 * @version 3.6.0
 */

defined( 'ABSPATH' ) || exit;

global $product;
$minitaturas = $product->get_gallery_attachment_ids();
$minitaturasMobile = $product->get_gallery_attachment_ids();
$fotoProduto = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
$fotoProduto = $fotoProduto[0];
/**
 * Hook: woocommerce_before_single_product.
 *
 * @hooked wc_print_notices - 10
 */
do_action( 'woocommerce_before_single_product' );

if ( post_password_required() ) {
	echo get_the_password_form(); // WPCS: XSS ok.
	return;
}

 ?>

<!-- PÁGINA DE PRODUTO -->
<div class="pg pg-produto">
	<div class="containerFull">
		<section class="secao-produto">
			<h6 class="hidden"><?php echo get_the_title() ?></h6>
			<div class="row">
				<div class="col-sm-7">
					<div class="imagens">
						<div class="row">
							<div class="col-sm-2 sm-hidden">		
								<ul class="carrossel-produto">
									<?php 
										foreach ($minitaturas  as $miniatura ):
											$miniatura_url = wp_get_attachment_url( $miniatura );
									?>
									<li class="imagem-produto"><img src="<?php echo $miniatura_url  ?>" alt="<?php echo get_the_title() ?>" data-imagem="<?php echo $miniatura_url  ?>"></li>
									<?php endforeach; ?>
								</ul>
							</div>
							<div class="col-sm-10">		
								<figure class="imagem-destaque-produto zoom"  id='ex1'>
									<img src="<?php echo $fotoProduto ?>" alt="<?php echo get_the_title() ?>">
								</figure>
							</div>
							<div class="col-sm-2 deskTopHidden">		
								<ul class="carrossel-produto">
									<?php 
										foreach ($minitaturasMobile  as $minitaturasMobile ):
											$miniatura_url_mobile = wp_get_attachment_url( $minitaturasMobile );
									?>
									<li class="imagem-produto"><img src="<?php echo $miniatura_url_mobile  ?>" alt="<?php echo get_the_title() ?>" data-imagem="<?php echo $miniatura_url_mobile  ?>"></li>
									<?php endforeach; ?>
								</ul>
							</div>
						</div>
					</div>
				</div>
				<div class="col-sm-5">
					<div class="info-produto">
						<?php do_action( 'woocommerce_single_product_summary' ); ?>
						<div class="sociais">
							<ul class="ul-sociais">
								<li class="li-sociais">
									<a href="https://www.facebook.com/sharer.php?u=<?php the_permalink() ?>" target="_blank" title="Compartilhar <?php the_title();?> no Facebook" class="facebook"><i class="fab fa-facebook-f"></i></a>
								</li>
								<li class="li-sociais">
									<a href="http://twitter.com/intent/tweet?text=<?php the_title();?>&url=<?php the_permalink();?>&via=seunomenotwitter" title="Twittar sobre <?php the_title();?>" target="_blank"><i class="fab fa-twitter"></i></a>
								</li>
								<li class="li-sociais">
									<a href="whatsapp://send?link=<?php the_permalink() ?>" data-action="share/whatsapp/share" class="pinterest"><i class="fab fa-whatsapp"></i></a>
								</li>
								<li class="li-sociais">
									<a href="https://plus.google.com/share?url=<?php the_permalink() ?>" onclick="javascript:window.open(this.href,
  '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=600,width=600');return false;" class="google"><i class="fab fa-google-plus-g"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</section>
	
	<div class="descricao">
			<h4 class="titulo-dois titulo-descricao">Descricao</h4>
			<div class="descricao-texto"><?php echo the_content() ?></div>
		</div>

		<?php 
			global $product;
				$related = $product->get_related();
			    $relatedQtd = count($related);
			    $upsells = $product->get_upsells();
			    $upsellsQtd = count($upsells);
			    $meta_query = WC()->query->get_meta_query();
			    $args = array(
					'post_type'           => 'product',
					'ignore_sticky_posts' => 1,
					'no_found_rows'       => 1,
					'posts_per_page'      => $upsellsQtd,
					'orderby'             => 'rand',
					'post__in'            => $upsells,
					'post__not_in'        => array( $product->id ),
					'meta_query'          => $meta_query
			    );
		    	$loop = new WP_Query( $args );                
		if ($loop):

		?>
		<section class="relacionados">
			<h6 class="hidden">SEÇÃO DE PRODUTOS RELACIONADOS</h6>
			<h4 class="titulo-dois titulo-relacionados">Relacionados</h4>
			<div class="lista-produtos">
				
				<ul class="carrossel carrossel-relacionados">
				<?php 
				while($loop ->have_posts()): $loop ->the_post(); 
					//TEMPLATE SPOT CARROSSEL
					include (TEMPLATEPATH . '/templates/spot_produto _carrossel.php'); 
				endwhile; wp_reset_query();
				?>
				</ul>
				<div class="setas-carrossel-relacionados">
					<span class="setas prevRelacionados"><img src="<?php echo get_template_directory_uri(); ?>/img/angle-up-p.svg" alt="prevRelacionados"></span>
					<span class="setas nextRelacionados"><img src="<?php echo get_template_directory_uri(); ?>/img/angle-down-p.svg" alt="nextRelacionados"></span>
				</div>
			</div>
		</section>
		<?php endif; ?>
	</div>
</div>

 <script type="text/javascript">
 	$("#ex1").zoom();
 </script>