<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Century_Sports_Loja
 */
global $configuracao;
?>

<!-- RODAPÉ -->
<footer>
	<div class="rodape">
		<div class="containerFull">
			<div class="row">
				<div class="col-md-3">
					<div class="info-footer atendimento">
						<h5 class="titulo">Atendimento</h5>
						<p class="fone"><?php echo $configuracao['config_site_rodape_contato'] ?></p>
						<p class="whatsapp"><?php echo $configuracao['config_site_rodape_whatsapp'] ?></p>
						<span class="horario-atendimento"><?php echo $configuracao['config_site_rodape_horario_atendimento'] ?></span>
						<ul class="rede-social">
							<li class="li-rede-social">
								<a href="<?php echo $configuracao['config_site_rodape_linkedin'] ?>"><i class="fab fa-linkedin-in"></i></a>
							</li>
							<li class="li-rede-social twitter">
								<a href="<?php echo $configuracao['config_site_rodape_twitter'] ?>"><i class="fab fa-twitter"></i></a>
							</li>
							<li class="li-rede-social facebook">
								<a href="<?php echo $configuracao['config_site_rodape_facebook'] ?>"><i class="fab fa-facebook-f"></i></a>
							</li>
						</ul>
						<div class="info">
							<span class="endereco"><?php echo $configuracao['config_site_rodape_info'] ?></span>
							<span class="endereco"><?php echo $configuracao['config_site_rodape_endereco'] ?></span>
						</div>
					</div>
				</div>
				<div class="col-md-2">
					<div class="info-footer privacidade">
						<h5 class="titulo">Privacidade</h5>
						<nav class="nav-footer">
							<a href="#" class="item-nav-footer">Compra segura</a>
							<a href="#" class="item-nav-footer">Política de privacidade</a>
							<a href="#" class="item-nav-footer">Quem somos</a>
						</nav>
					</div>
				</div>
				<div class="col-md-4">
					<div class="info-footer ajuda">
						<h5 class="titulo">Ajuda e suporte</h5>
						<div class="nav-footer">
							<a href="#" class="item-nav-footer">Trocas e devoluções</a>
							<a href="#" class="item-nav-footer">Frete e entrega</a>
							<a href="#" class="item-nav-footer">Como comprar</a>
							<a href="#" class="item-nav-footer">Formas de pagamnto</a>
							<a href="#" class="item-nav-footer">Contato</a>
							<a href="#" class="item-nav-footer">Marcas</a>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="facebook" id="facebook">
						<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fcenturysports%2F&tabs=timeline&width=350&height=130&small_header=false&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="350" height="130" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true" allow="encrypted-media"></iframe>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="formas-pagamento">
		<div class="containerFull">
			<div class="info-footer pagamento">
				<h5 class="titulo">Formas de pagamento</h5>
				<ul class="ul-formas">
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/visa.png" alt="visa"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/mastercard.png" alt="mastercard"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/banco.png" alt="banco"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/americanexpress.png" alt="americanexpress"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/hipercard.png" alt="hipercard"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/aura.png" alt="aura"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/bradesco.png" alt="bradesco"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/itau.png" alt="itau"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/bancobrasil.png" alt="bancobrasil"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/banrisul.png" alt="banrisul"></li>
					<li class="li-formas"><img src="<?php echo get_template_directory_uri(); ?>/img/boleto.png" alt="boleto"></li>
					<li class="li-formas pagseguro"><img src="<?php echo get_template_directory_uri(); ?>/img/pagseguro.png" alt="pagseguro"></li>
				</ul>
			</div>
			<div class="info-footer certificado">
				<h5 class="titulo">Certificados</h5>
				<img src="<?php echo get_template_directory_uri(); ?>/img/selo.png" alt="Selo">
			</div>
		</div>
	</div>
	<div class="copyright">
		<p><?php echo $configuracao['config_site_rodape_copyright'] ?></p>
	</div>
</footer>

<?php wp_footer(); ?>

</body>
</html>
