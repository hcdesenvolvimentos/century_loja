$(function(){

	$('.carrossel-categorias').slick({
		slidesToShow: 7,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 4,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 5,
	          }
	        }
	      ]
	});

	$('.carrossel-marcas').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		prevArrow: $('.prev5'),
		nextArrow: $('.next5'),
		responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 1,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]
	});

	$('.carrossel-promocoes').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		prevArrow: $('.prev4'),
		nextArrow: $('.next4'),
		 responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 1,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]
	});

	$('.carrossel-produtos-inicial-padrao').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]

	});

	$('.carrossel-destaque').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		arrows: false
	});

	$('.carrossel-produto').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		infinite: true,
		vertical: true,
		draggable: false,
		autoplay: true,
		autoplaySpeed: 2000,
		focusOnSelect: false,
		responsive: [
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           vertical: false,
	            draggable: true,
	          }
	        },
	        {
	          breakpoint: 767,
	          settings: {
	          	slidesToShow: 3,
	            vertical: false,
	            draggable: true,
	          }
	        },
	       
	      ]
	});

	$('.carrossel-relacionados').slick({
		slidesToShow: 3,
		slidesToScroll: 1,
		infinite: true,
		autoplay: true,
		autoplaySpeed: 2000,
		prevArrow: $('.prevRelacionados'),
		nextArrow: $('.nextRelacionados'),
			responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]
	});

	$('.pg-perguntas-frequentes .passos .passos-tab .passos-tab-titulo a').click(function(e){
		e.preventDefault();
		let data = $(this).attr('data-id');

		if($('.pg-perguntas-frequentes .passos .passos-tab').hasClass('open')){
			$('.pg-perguntas-frequentes .passos .passos-tab').removeClass('open');
		}

		$('.pg-perguntas-frequentes .passos .passos-tab').removeClass('open');
		$('.pg-perguntas-frequentes .passos .passos-tab#'+data).addClass('open');

	});

	$('header .main-menu .nav-right .search form .search-icon').click(function(e){
		$(this).css('color', 'var(--roxo)');
		$('header .main-menu .nav-right .search form .search-bar').addClass('openSearch');
		
		$('header .main-menu .nav-right .search form .search-bar input[type="text"]').css('padding', '10px 14px 10px 25px');
		$('header .main-menu .nav-right .search form .search-bar .close-search-bar').css('display', 'block');
		$('header .main-menu .navbar .navbar-collapse ul.menu').css('opacity', '0');
		$('header .main-menu .nav-right .search form .search-bar input[type="submit"]').css('display', 'block');
	});

	$('header .main-menu .nav-right .search form .search-bar .close-search-bar').click(function(){
		setTimeout(function(){ 
			$('header .main-menu .nav-right .search form .search-bar').removeClass('openSearch');
			$('header .main-menu .nav-right .search form .search-bar').css('min-width', '0');
			setTimeout(function(){
				$('header .main-menu .nav-right .search form .search-bar input[type="text"]').css('padding', '0');
			},180);
			$('header .main-menu .nav-right .search form .search-bar .close-search-bar').css('display', 'none');
			$('header .main-menu .navbar .navbar-collapse ul.menu').css('opacity', '1');
		}, 300);
		$('header .main-menu .nav-right .search form .search-bar input[type="submit"]').css('display', 'none');
		$('header .main-menu .nav-right .search form .search-icon').css('color', '#fff');
		$('header .main-menu .nav-right .search form .search-icon').addClass('esconderIcone');
		setTimeout(function(){ 
			$('header .main-menu .nav-right .search form .search-icon').removeClass('esconderIcone');
		}, 700);

	});

	$('.pg-produto .secao-produto .imagens .carrossel-produto .imagem-produto').click(function(e){
		let imagem = $(this).children().attr('src');
		$('.pg-produto .secao-produto .imagens .imagem-destaque-produto img').attr('src', imagem);
	});

	$('.pg .lista-produtos .item-produto .produto a figure .imagem-produto').mouseenter(function(e){
		let imgZoom = $(this).attr('data-img-zoom');
		$(this).attr('src', imgZoom);
	});

	$('.pg .lista-produtos .item-produto .produto a figure .imagem-produto').mouseleave(function(e){
		let imgPadrao = $(this).attr('data-img-padrao');
		$(this).attr('src', imgPadrao);
	});

	$('.pg-inicial .secao-produtos .escolha-por-marca .carrossel-produtos-inicial').slick({
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 2000,
		infinite: true,
		responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]
	});

	$('.pg-inicial .secao-produtos .filtro-marcas a').click(function(e){
		e.preventDefault();
		$('.pg-inicial .secao-produtos .filtro-marcas a').removeClass('filtroAtivo');
		$(this).addClass('filtroAtivo');
		
		let dataId = $(this).attr('data-id');
		console.log(dataId);
		$('.carrossel-produtos-inicial').slick('unslick');
		$('.pg-inicial .secao-produtos .escolha-por-marca .lista-produtos').hide();
		$('.pg-inicial .secao-produtos .escolha-por-marca .lista-produtos#'+dataId).show();

		$('.pg-inicial .secao-produtos .escolha-por-marca .carrossel-produtos-inicial.carrossel'+dataId).slick({
			slidesToShow: 4,
			slidesToScroll: 1,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 2000,
			responsive: [
	       
	       	{
	          breakpoint: 600,
	          settings: {
	            slidesToShow: 2,
	           
	          }
	        },
	        {
	          breakpoint: 768,
	          settings: {
	            slidesToShow: 3,
	           
	          }
	        },
	        {
	          breakpoint: 991,
	          settings: {
	            slidesToShow: 3,
	          }
	        }
	      ]
		});

	});

	$('.icons.usuario .fas.fa-times').click(function(e){
		$("#collapse").removeClass("openMenuMobile");
		$("body").removeClass("overlayBackground");
	});
	$('.openaMenuMobileBtn').click(function(e){
		$("#collapse").addClass("openMenuMobile");
		$("body").addClass("overlayBackground");
	});
	  

});