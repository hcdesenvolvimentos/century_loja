<?php if ($configuracao['inicial_banner_promocional_promocionais_esconder'] != "1"): ?>
<section class="secao-marcas">
	<div class="containerFull">
		<h3 class="titulo">Navegue pelas marcas</h3>
		<div class="marcas">
			<ul class="carrossel-marcas">
				<?php  
					$categoria = 'product_cat';
					// LISTA AS CATEGORIAS DE PRODUTO
					$categoriasProdutos = get_terms( $categoria, array(
						'orderby'       => 'count',
						'hide_empty'    => true,
						'parent'	    => 27,
						'order'         => 'DESC',
					));
					foreach( $categoriasProdutos as $categoriasProdutos ):
						$cat_thumb_id  = get_woocommerce_term_meta( $categoriasProdutos->term_id, 'thumbnail_id', true );
						$cat_thumb_url = wp_get_attachment_thumb_url( $cat_thumb_id );
						$term_link     = get_term_link( $categoriasProdutos, 'product_cat' );
				?>
				<li class="item-marca">
					<a href="<?php echo $term_link   ?>">
						<figure>
							<img src="<?php echo $cat_thumb_url ?>" alt="<?php echo $categoriasProdutos->name ?>">
						</figure>
					</a>
				</li>
				<?php endforeach; wp_reset_query(); ?>
			</ul>
			<div class="setas-carrossel-categorias">
				<span class="setas prev5"><img src="<?php echo get_template_directory_uri(); ?>/img/prev.svg" alt="prev"></span>
				<span class="setas next5"><img src="<?php echo get_template_directory_uri(); ?>/img/next.svg" alt="next"></span>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>