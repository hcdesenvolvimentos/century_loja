<?php if ($configuracao['inicial_sessao_categoroia_carrossel'] != "1"): ?>
<section class="secao-categorias">
	<h6 class="hidden"><?php echo $configuracao['inicial_sessao_categoroia_titulo'] ?></h6>
	<div class="containerCategorias">
		<div class="todas-categorias">
			<h3 class="titulo"><?php echo $configuracao['inicial_sessao_categoroia_titulo'] ?></h3>
			<ul class="carrossel-categorias">
				<?php  
					$categoria = 'product_cat';
					// LISTA AS CATEGORIAS DE PRODUTO
					$categoriasProdutos = get_terms( $categoria, array(
						'orderby'    => 'count',
						'hide_empty' => true,
						'parent'	 => $configuracao['inicial_sessao_categoroia_carrossel_id'],
						'order'      => 'DESC',
					));
					foreach( $categoriasProdutos as $categoriasProdutos ):
						$cat_thumb_id = get_woocommerce_term_meta( $categoriasProdutos->term_id, 'thumbnail_id', true );
						$cat_thumb_url = wp_get_attachment_thumb_url( $cat_thumb_id );
						$term_link = get_term_link( $categoriasProdutos, 'product_cat' );
				?>	
				<li class="item item-categoria">
					<a href="<?php echo $term_link; ?>">
						<h3 class="hidden"><?php echo $categoriasProdutos->name; ?></h3>
						<figure>
							<img src="<?php echo $cat_thumb_url; ?>" alt="<?php echo $categoriasProdutos->name; ?>">
							<figcaption class="hidden"><?php echo $categoriasProdutos->name; ?></figcaption> 
							<h3><?php echo $categoriasProdutos->name; ?></h3>
							<!-- Trocar para título -->
						</figure>
					</a>
				</li>
				<?php endforeach; wp_reset_query(); ?>
			</ul>
		</div>
	</div>
</section>
<?php endif; ?>
