<!-- CARROSSEL DE DESTAQUE -->
<section class="carrosselDestaque secao">
	<h6 class="hidden">SEÇÃO DESTAQUE</h6>
	<div class="carrossel-destaque">
		<?php 
			//LOOP DE POST SERVIÇOS
			$destaques = new WP_Query( array( 'post_type' => 'destaque', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
			while ( $destaques->have_posts() ) : $destaques->the_post();
		 ?>
		<!-- ITEM -->
		<a href="<?php echo $destaque_link = rwmb_meta('CenturySports_destaque_link'); ?>" class="item" style="background: url(<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>);">
			<figure>
				<img class="img-responsive" src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
			</figure>
		</a>
		<?php endwhile; wp_reset_query(); ?>
	</div>
</section>