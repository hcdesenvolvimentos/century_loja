<?php 
	$tituloSessao_3  = $configuracao["inicial_sessao_carrossel_departamento_titulo_3"];
	$slugCategoria_3 = $configuracao["inicial_sessao_carrossel_departamento_slug_3"];
	$produtosCarrossel_3 = new WP_Query(array(
		'post_type'     => 'product',
		'posts_per_page'   => -1,
		'order' => 'rand',
		'tax_query'     => array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $slugCategoria_3,
				)
			)
		)
	);
	if ($slugCategoria_3):
?>
<section class="secao-produtos">
	<h6 class="hidden"><?php  echo "Sessão departamento ".$tituloSessao_3 ?></h6>
	<div class="containerFull">
		<div class="produtos">
			<h3 class="titulo-dois titulo-categoria-produtos"><?php  echo  $tituloSessao_3 ?></h3>
			<div class="lista-produtos">
				<ul class="carrossel carrossel-produtos-inicial-padrao">
					<?php 
						// LOOP DE POST
						while ( $produtosCarrossel_3->have_posts() ) : $produtosCarrossel_3->the_post();

							//TEMPLATE SPOT CARROSSEL
							include (TEMPLATEPATH . '/templates/spot_produto _carrossel.php');
					
					  	endwhile; wp_reset_query();  
					  ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
