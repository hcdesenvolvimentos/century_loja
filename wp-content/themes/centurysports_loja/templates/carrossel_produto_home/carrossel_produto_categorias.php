
<?php 
 if ($configuracao['inicial_sessao_categoroia_carrossel_produto'] != "1"):?>
<section class="secao-produtos">
	<h6 class="hidden">SEÇÃO DE PRODUTOS</h6>
	
	<div class="containerFull">
		<div class="escolha-por-marca">
		
			<nav class="filtro-marcas">
				<?php 

					$categoria = 'product_cat';
					// LISTA AS CATEGORIAS DE PRODUTO
					$categoriasProdutos = get_terms( $categoria, array(
						'orderby'    => 'count',
						'hide_empty' => true,
						'parent'	 => $configuracao['inicial_sessao_categoroia_id'],
						'order'      => 'DESC',
					));

					$j =  0;
					foreach( $categoriasProdutos as $categoriasProdutos ):
						$verificacao =  in_array($categoriasProdutos->slug,$configuracao['inicial_sessao_categoroia_carrossel_selecao_produto']);
						if ($verificacao):
				?>
				<a href="#" data-id="<?php echo $categoriasProdutos->slug ?>" class=" <?php if ($j == 0) {echo "filtroAtivo";} ?>"><?php echo $categoriasProdutos->name; ?>
					<span class="borda"></span>
				</a>
				<?php $j++; endif;endforeach; wp_reset_query(); ?>
			</nav>
			
			<?php 

				$categoriaContainer = 'product_cat';
				// LISTA AS CATEGORIAS DE PRODUTO
				$categoriasProdutosContainer = get_terms( $categoriaContainer, array(
					'orderby'    => 'count',
					'hide_empty' => true,
					'parent'	 => $configuracao['inicial_sessao_categoroia_id'],
					'order'      => 'DESC',
				));
				
				$i =  0;
				foreach( $categoriasProdutosContainer as $categoriasProdutosContainer ):
					$verificacaoContainer =  in_array($categoriasProdutosContainer->slug,$configuracao['inicial_sessao_categoroia_carrossel_selecao_produto']);
					if ($verificacaoContainer):

					//LOOP DE POST CATEGORIA DESTAQUE				
					$produtos = new WP_Query(array(
						'post_type'        => 'product',
						'posts_per_page'   => -1,
						'order' => 'rand',
						'tax_query'        => array(
							array(
								'taxonomy' => $categoriaContainer,
								'field'    => 'slug',
								'terms'    => $categoriasProdutosContainer->slug,
								)
							)
						)
					);

			?>
			<div class="lista-produtos <?php if ($i == 0) {echo "carrosselAtivo";} ?>" id="<?php echo $categoriasProdutosContainer->slug ?>">
				<ul class="carrossel carrossel-produtos-inicial carrossel<?php echo $categoriasProdutosContainer->slug ?>">
					<?php 
						// LOOP DE POST
						while ( $produtos->have_posts() ) : $produtos->the_post();

							//TEMPLATE SPOT CARROSSEL
							include (TEMPLATEPATH . '/templates/spot_produto _carrossel.php');
					
					  	endwhile; wp_reset_query();  
					  ?>
				</ul>
			</div>
			<?php $i++;endif; endforeach; wp_reset_query(); ?>
			
		</div>
	</div>
</section>
<?php endif; ?>