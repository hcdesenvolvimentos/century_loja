<?php 
	$tituloSessao  = $configuracao["inicial_sessao_carrossel_departamento_titulo"];
	$slugCategoria = $configuracao["inicial_sessao_carrossel_departamento_slug"];
	$produtosCarrossel_1 = new WP_Query(array(
		'post_type'     => 'product',
		'posts_per_page'   => -1,
		'order' => 'rand',
		'tax_query'     => array(
			array(
				'taxonomy' => 'product_cat',
				'field'    => 'slug',
				'terms'    => $slugCategoria ,
				)
			)
		)
	);
	if ($slugCategoria):
?>
<section class="secao-produtos">
	<h6 class="hidden"><?php  echo "Sessão departamento ".$tituloSessao ?></h6>
	<div class="containerFull">
		<div class="produtos">
			<h3 class="titulo-dois titulo-categoria-produtos"><?php  echo  $tituloSessao ?></h3>
			<div class="lista-produtos">
				<ul class="carrossel carrossel-produtos-inicial-padrao">
					<?php 
						// LOOP DE POST
						while ( $produtosCarrossel_1->have_posts() ) : $produtosCarrossel_1->the_post();

							//TEMPLATE SPOT CARROSSEL
							include (TEMPLATEPATH . '/templates/spot_produto _carrossel.php');
					
					  	endwhile; wp_reset_query();  
					  ?>
				</ul>
			</div>
		</div>
	</div>
</section>
<?php endif; ?>
