
<?php  if ($configuracao['inicial_mini_banner_modelo_hidden'] != "1"): ?>
<section class="secao-categorias">
	<h6 class="hidden">Banners Promocionais</h6>
	<div class="containerCategorias">
		<ul class="categorias-destaque">
			<li class="li-categorias-destaque">
				<a href="<?php echo $configuracao['inicial_mini_banner_link_1']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_1']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titulo_1'] ?>">
					</figure>
				</a>
			</li>

			<?php if ($configuracao['inicial_mini_banner_modelo'] != "0") : ?>
			<li class="li-categorias-destaque">
				<a href="<?php echo $configuracao['inicial_mini_banner_link_2']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_2']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titulo_2'] ?>">
					</figure>
				</a>
			</li>
			<?php else: ?>
			<li class="li-categorias-destaque categoria-menor">
				<a href="<?php echo $configuracao['inicial_mini_banner_link_2']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_2']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titulo_2'] ?>">
					</figure>
				</a>
				<a href="<?php echo $configuracao['inicial_mini_banner_lin0_3']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_3']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titul0_3']?>">
					</figure>
				</a>
			</li>
			<?php endif; if ($configuracao['inicial_mini_banner_modelo'] != "0"): ?>
			<li class="li-categorias-destaque">
				<a href="<?php echo $configuracao['inicial_mini_banner_lin0_3']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_3']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titul0_3'] ?>">
					</figure>
				</a>
			</li>
			<?php else: ?>
			<li class="li-categorias-destaque">
				<a href="<?php echo $configuracao['inicial_mini_banner_link_4']?>">
					<figure class="hidden">
						<img src="<?php echo $configuracao['inicial_mini_banner_4']['url'] ?> " alt="<?php echo $configuracao['inicial_mini_banner_titulo_4'] ?>">
					</figure>
				</a>
			</li>
			<?php endif; ?>
		</ul>
	</div>
</section>
<?php endif; ?>

		
