/* This snippet removes the action that inserts thumbnails to products in the loop
 * and re-adds the function customized with our wrapper in it.
 * It applies to all archives with products.
 *
 * @original plugin: WooCommerce
 * @author of snippet: Brian Krogsard
 */

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);
add_action(    'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10);

/**
 * WooCommerce Loop Product Thumbs
 **/

 if ( ! function_exists( 'woocommerce_template_loop_product_thumbnail' ) ) {
    function woocommerce_template_loop_product_thumbnail() {
        echo woocommerce_get_product_thumbnail();
    } 
 }

/**
 * WooCommerce Product Thumbnail
 **/

 if ( ! function_exists( 'woocommerce_get_product_thumbnail' ) ) {

    function woocommerce_get_product_thumbnail( $size = 'shop_catalog', $placeholder_width = 0, $placeholder_height = 0  ) {
        global $post, $wp_query, $woocommerce, $attributes;
        // if ( ! $placeholder_width ) {
        //  $placeholder_width = $woocommerce->get_image_size( 'shop_catalog_image_width' );
        // }
        // if ( ! $placeholder_height ){
        //  $placeholder_height = $woocommerce->get_image_size( 'shop_catalog_image_height' );
        // }

        $output = '<div class="imagewrapper">';
        if ( has_post_thumbnail() ) {

            $output .= get_the_post_thumbnail( $post->ID, $size ); 

        } else {

            $output .= '<img src="'. woocommerce_placeholder_img_src() .'" alt="Placeholder" width="' . $placeholder_width . '" height="' . $placeholder_height . '" />';

        }

        //$product .=  wc_get_product($post->ID ) ;
        //$name .= $product->get_name();

        $output .= '<div class="sizeAvailHover">';
        //$output .= $name;
        //$product .=  wc_get_product($post->ID ) ;
        $product = wc_get_product( $wp_query->post );


        //echo get_post_format();

            // echo 'total stock : ';
            // echo $product->is_in_stock();



        if ( $product->is_type( 'variable' ) ) {
            // $attributes .= $product->list_attributes();
            // echo print_r($attributes);
            // $attributes = $product->get_attributes();
            // echo print_r($attributes);
            // $attributes .= $product->wc_display_product_attributes();
            // echo print_r($attributes);
            // $attributes .= $product->get_variation_attributes();
            // echo print_r($attributes);


            if($product->is_in_stock()) {
                $output .= ' Available sizes: <br/>';
                $variations = $product->get_available_variations();
                $i = 0;
                foreach($variations as $variation){
                    $variation_id = $variation['variation_id'];
                    $variation_obj = new WC_Product_variation($variation_id);
                    $stock = $variation_obj->get_stock_quantity();
                    //echo $stock;
                    // echo '--';
                    //echo $variation_obj->get_sku();
                    //echo print_r($variation_obj->get_variation_attributes());
                    $dummy = $variation_obj->get_variation_attributes();
                    //echo gettype($stock);
                    if ((is_int($stock) || ctype_digit($stock)) && (int)$stock > 0 ) { // int }
                    //if ($stock > 0 ) {
                        //echo $variation_obj->get_attributes();
                        //echo 'yesssss';
                        // echo print_r($dummy['attribute_pa_size']);
                        $output .= '<span class="availSize">' . strtoupper($dummy['attribute_pa_size']) . '</span>';
                    } else {
                        // echo print_r($dummy['attribute_pa_size']);
                        $output .= '<span class="unavailSize">' . strtoupper($dummy['attribute_pa_size']) . '</span>';
                    }
                    if(++$i !== count($variations)) {
                        $output .= ', ';
                    }

                    //echo '<br/>';

                }
            } else {
                $output .= ' Sold out<br/>Notify me when the product is back in stock';
            }


            // if ( ! $attributes ) {
            //     echo "No attributes";
            // }
            // foreach ( $attributes as $attribute ) {

         //        echo $attribute['name'] . ": ";
         //        $product_attributes = array();
         //        $product_attributes = explode('|',$attribute['value']);

         //        $attributes_dropdown = '<select>';

         //        foreach ( $product_attributes as $pa ) {
         //            $attributes_dropdown .= '<option value="' . $pa . '">' . $pa . '</option>';
         //        }

         //        $attributes_dropdown .= '</select>';

         //        echo $attributes_dropdown;
            // }
        }

        $output .= '</div>';

        $output .= '</div>';

        return $output;
    }
 }