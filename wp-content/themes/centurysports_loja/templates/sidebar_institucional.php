<!-- SIDEBAR ATENDIMENTO -->
<div class="pg">
	<div class="sidebar sidebar-atendimento">
		<h4 class="titulo">Atendimento</h4>
		<nav class="nav-sidebar nav-institucional">
			<h6>Institucional</h6>
			<a href="<?php echo home_url('/termos-e-condicoes'); ?>" class="item-nav termo">Termos e condições</a>
			<a href="<?php echo home_url('/politica-de-troca'); ?>" class="item-nav troca">Política de troca</a>
			<a href="<?php echo home_url('/frete-e-entrega'); ?>" class="item-nav frete">Frete e entrega</a>
			<a href="<?php echo home_url('/perguntas-frequentes'); ?>" class="item-nav perguntas">Perguntas frequentes</a>
			<a href="<?php echo home_url('/como-comprar'); ?>" class="item-nav como-comprar">Como comprar?</a>
		</nav>
		<nav class="nav-sidebar nav-suporte">
			<h6>Ajuda e suporte</h6>
			<a href="<?php echo home_url('/minha-conta'); ?>" class="item-nav cadastro">Cadastro</a>
			<a href="<?php echo home_url('/minha-conta/orders/'); ?>" class="item-nav pedido">Status do pedido</a>
			<a href="<?php echo home_url('/perguntas-frequentes'); ?>" class="item-nav pagamento">Formas de pagamento</a>
			<a href="<?php echo home_url('/contato'); ?>" class="item-nav link-contato">Contato</a>
		</nav>
	</div>
</div>