<?php if ($configuracao['inicial_banner_promocional_promocionais_esconder'] != "1"): ?>
<div class="promocoes containerFull">
	<ul class="carrossel-promocoes">
		<?php 
			//LOOP DE POST SERVIÇOS
			$bannersPromocionais = new WP_Query( array( 'post_type' => 'promocao', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1) );
			while ( $bannersPromocionais->have_posts() ) : $bannersPromocionais->the_post();
				if (rwmb_meta('CenturySports_banner_link')){
					$banner_link = rwmb_meta('CenturySports_banner_link');
				}else{
					$banner_link = "#";
				}
		 ?>
		<li class="item-promocao">
			<a href="<?php echo $banner_link  ?>" title="<?php echo get_the_title() ?>">
				<figure class="icon-promocoes">
					<img src="<?php echo wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' )[0] ?>" alt="<?php echo get_the_title() ?>">
					<figcaption class="info"><?php echo get_the_title() ?></figcaption>
				</figure>
			</a>
		</li>
		<?php endwhile; wp_reset_query(); ?>
	</ul>
	<div class="setas-carrossel-categorias">
		<span class="setas prev4"><img src="<?php echo get_template_directory_uri(); ?>/img/prev.svg" alt="prev"></span>
		<span class="setas next4"><img src="<?php echo get_template_directory_uri(); ?>/img/next.svg" alt="next"></span>
	</div>
</div>
<?php endif; ?>
