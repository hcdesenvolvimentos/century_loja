
<?php 
	//VARIÁVEL GLOBAL DO PRODUTO 
	global $product;
	//FOTO PRODUTO
	$fotoProduto      = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'full' );
	$fotoProduto      = $fotoProduto[0];
	//FOTO ZOOM 
	$fotoProduto_zoom = rwmb_meta('CenturySports_produto_zoom');
	//PREÇO NROMAL
	$price            = str_replace('.', ',', $product->price);
	// PREÇO REGULAR
	$regular_price    = str_replace('.', ',', $product->regular_price);
	//PREÇO PROMOCIONAL
	$sale_price       = str_replace('.', ',', $product->sale_price);
	//FOREACH PARA PEGAR URL DA FOTO
	foreach ($fotoProduto_zoom as $fotoProduto_zoom) { 
		$fotoProduto_zoom =  $fotoProduto_zoom['full_url'];
	}
	if ($fotoProduto_zoom) {
		$fotoProduto_zoom =  $fotoProduto_zoom;
	}else{
		$fotoProduto_zoom =  $fotoProduto;
	}
?>
<li class="item-produto">
	<div class="produto nike">
		<a href="<?php echo get_permalink() ?>" title="<?php echo get_the_title(); ?>">
			<figure>
				<img src="<?php echo $fotoProduto ?>" alt="<?php echo get_the_title(); ?>" class="imagem-produto" data-img-zoom="<?php echo $fotoProduto_zoom ?>" data-img-padrao="<?php echo $fotoProduto ?>">
			</figure>
		</a>
		<h2 class="nome-produto" itemprop="name"><?php echo get_the_title(); ?></h2>
		
		<?php if ($sale_price):?>
		<span class="pagamento"> 
			De: 
			<strong><s>R$<?php echo $regular_price ?>,00</s></strong> -
			Por: 
			<strong>R$<?php echo $sale_price ?>,00</strong>
			<br>ou 12x de 

			<strong>
			<?php 
				$calculoParcelas     = $sale_price/12;  
				$precoParcela_format = round($calculoParcelas, 2);
				$precoParcela        = str_replace('.', ',', $precoParcela_format);
				echo $precoParcela.",00";
			?>
			</strong>

		</span>
		<?php else: if($price): ?>
		<span class="pagamento"> 
			Por: 
			<strong>R$<?php echo $price  ?>,00</strong>
			<br>ou 12x de
			 <strong>
				<?php 
					$calculoParcelas_price     = $price/12;  
					$precoParcela_format_price = round($calculoParcelas_price, 2);
					$precoParcela_price        = str_replace('.', ',', $precoParcela_format_price);
					echo $precoParcela_price.",00";
				?>
			</strong>
		</span>
		<?php endif;endif; ?>
		<div class="btn-comprar">
				<a href="<?php echo $current_url = home_url( add_query_arg( array(), $wp->request ) ) ?>/?add-to-cart=<?php echo $product->id ?>" data-quantity="1" class="button product_type_simple add_to_cart_button ajax_add_to_cart" data-product_id="<?php echo $product->id ?>" data-product_sku="" aria-label="Adicionar “Produto 4” no seu carrinho" rel="nofollow">Comprar</a>
		</div>
	</div>
</li>