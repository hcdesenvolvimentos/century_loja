<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package Century_Sports_Loja
 */
global $configuracao;
global $woocommerce;
global $wp;
global $product;
global $current_user;
$urlMinhaConta 	= get_permalink(get_option('woocommerce_myaccount_page_id'));
$urlCarrinho    = WC()->cart->get_cart_url();
$urlCheckout 	= WC()->cart->get_checkout_url();
$urlLogout 	= wp_logout_url( get_permalink( get_option( 'woocommerce_myaccount_page_id' ) ) );
$cart_subtotal = $woocommerce->cart->get_cart_subtotal();


// WOCOMMERCE: QUANTIDADE DE ITENS NO CARRINHO
$qtdItensCarrinho 		= WC()->cart->cart_contents_count;
$qtdItensCarrinhoRotulo = ($qtdItensCarrinho == 0 || $qtdItensCarrinho > 1) ? $qtdItensCarrinho . ' ' : $qtdItensCarrinho . ' ';



$totalCarrinho = str_replace('.', ',', WC()->cart->cart_contents_total);
if ($totalCarrinho != 0) {
	$totalCarrinho = $totalCarrinho ;
}else{
	$totalCarrinho = "00,00";
}
 //wc_print_notices();

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	
	<!-- FAVICON -->
	<link rel="shortcut icon" type="image/x-icon" href="img/favicon.ico" /> 
	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>

<!-- TOPO -->
<header>
	<div class="menu-topo">
		<div class="containerFull">
			<div class="row">
				<div class="col-sm-7">
					<div class="info-contato">
						 <p><a href="https://www.google.com/maps/place/" title="<?php echo $configuracao['header_endereco'] ?> " target="_blank"><strong>Localização:</strong> <?php echo $configuracao['header_endereco'] ?> </a></p>
						<p><a href="tel:<?php echo $configuracao['header_telefone'] ?>" title="<?php echo $configuracao['header_telefone'] ?>"><strong>Telefone:</strong> <?php echo $configuracao['header_telefone'] ?></a></p>
						<p class="compre-no-site"><strong><?php echo $configuracao['header_frase'] ?></strong></p>
					</div>
				</div>
				<div class="col-sm-5">
					<nav class="help-menu">
						<?php 
							foreach ( $configuracao['header_links'] as  $configuracao['header_links']):
							$itemFormatado = 	explode("|", $configuracao['header_links']);
						?>
						<a href="<?php echo $itemFormatado[1] ?>" title="<?php echo $itemFormatado[0] ?>"><?php echo $itemFormatado[0] ?></a>
						<?php endforeach; ?>
					</nav>
				</div>
			</div>
		</div>
	</div>
	<div class="main-menu">
		<div class="containerFull">
			<div class="row">
				<!-- LOGO -->
				<div class="col-md-2">
					<a href="<?php echo esc_url( home_url( '/' ) ); ?>" title="<?php bloginfo( 'name' ); ?>">
						<figure>
							<img class="img-responsive" src="<?php echo $configuracao['header_logo']['url'] ?> " alt="<?php bloginfo( 'name' ); ?>">
						</figure>
					</a>
					<div class="openaMenuMobileBtn">
						<i class="fas fa-bars"></i>
					</div>
				</div>
				<!-- MENU  -->	
				<div class="col-md-7">
					<div class="navbar" role="navigation">	
					
						<!--  MENU MOBILE-->
						<div class="row navbar-header">
							<nav class="collapse navbar-collapse" id="collapse">
								<div class="menuOptionMobile">
									<div class="nav-right">
										<div class="icons usuario">
											<i class="fas fa-times"></i>
										</div>
										<div class="icons usuario">
											<a href="<?php echo $urlMinhaConta ?>" title="Minha Conta" class="icon-usuario"><i class="far fa-user"></i></a>
										</div>
										<div class="icons carrinho">
											<a href="<?php echo $urlCarrinho  ?>" title="Meu Carrinho" class="icon-carrinho"> 
											<?php 
											if ($qtdItensCarrinhoRotulo != 0): ?>
											<small><?php echo $qtdItensCarrinhoRotulo; ?></small>
											<?php endif ?>
											<i class="fas fa-shopping-bag"></i></a>
										</div>
										<div class="icons preco">
											<span>Seu carrinho:</span>
											<p>R$<?php echo $totalCarrinho ?></p>
										</div>
										<div class="icons search">
											<form action="" role="search" method="get">
												<span class="search-icon"><i class="fas fa-search"></i></span>
												<div class="search-bar openSearch">
													<input type="text" placeholder="Digite a palavra chave...">
													<input type="submit" value="">
													<span class="close-search-bar">x</span>
												</div>
											</form>
										</div>

									</div>
								</div>
								<?php wp_nav_menu(); ?>
								<!-- <ul class="nav navbar-nav">										
									<li><a href="#" class="" title="">Calçados</a></li>
									<li>
										<a href="#" class="" title="">Confecções</a>
											<ul class="submenu">
												<li class="li-submenu"><a href="#">Feminino</a></li>
												<li class="li-submenu"><a href="#">Masculino</a></li>
												<li class="li-submenu"><a href="#">Bermudas</a></li>
												<li class="li-submenu"><a href="#">Blusas</a></li>
												<li class="li-submenu"><a href="#">Camisetas</a></li>
												<li class="li-submenu"><a href="#">Agasalhos</a></li>
												<li class="li-submenu"><a href="#">Infanto juvenil</a></li>
												<li class="li-submenu"><a href="#">Maiôs</a></li>
												<li class="li-submenu"><a href="#">Shorts</a></li>
												<li class="li-submenu"><a href="#">Sungas</a></li>
												<li class="li-submenu"><a href="#">Tops</a></li>
												<li class="li-submenu"><a href="#">Feminino</a></li>
												<li class="li-submenu"><a href="#">Masculino</a></li>
												<li class="li-submenu"><a href="#">Bermudas</a></li>
												<li class="li-submenu"><a href="#">Blusas</a></li>
												<li class="li-submenu"><a href="#">Camisetas</a></li>
												<li class="li-submenu"><a href="#">Agasalhos</a></li>
												<li class="li-submenu"><a href="#">Infanto juvenil</a></li>
												<li class="li-submenu"><a href="#">Maiôs</a></li>
												<li class="li-submenu"><a href="#">Shorts</a></li>
												<li class="li-submenu"><a href="#">Sungas</a></li>
												<li class="li-submenu"><a href="#">Tops</a></li>
											</ul>
									</li>
									<li><a href="#" class="" title="">Acessórios</a></li>
									<li><a href="#" class="" title="">Lançamentos</a></li>
									<li><a href="#" class="" title="">Outlet</a></li>
								</ul> -->
							</nav>
						</div>
					</div>
				</div>
				<div class="col-md-3">
					<div class="nav-right">
						<div class="icons search">
							<form action="" role="search" method="get">
								<span class="search-icon"><i class="fas fa-search"></i></span>
								<div class="search-bar">
										<?php //echo do_shortcode('[wcas-search-form]'); ?>
									<input type="text" placeholder="Digite a palavra chave...">
									<input type="submit" value="">
									<span class="close-search-bar">x</span>
								</div>
							</form>
						</div>
						<div class="icons usuario">
							<a href="<?php echo $urlMinhaConta ?>" title="Minha Conta" class="icon-usuario"><i class="far fa-user"></i></a>
						</div>
						<div class="icons carrinho">
							<a href="<?php echo $urlCarrinho  ?>" title="Meu Carrinho" class="icon-carrinho"> 
							<?php 
							if ($qtdItensCarrinhoRotulo != 0): ?>
							<small><?php echo $qtdItensCarrinhoRotulo; ?></small>
							<?php endif ?>
							<i class="fas fa-shopping-bag"></i></a>
							<div class="minicart">
								<?php woocommerce_mini_cart(); ?>
							</div>
						</div>
						<div class="icons preco">
							<span>Seu carrinho:</span>
							<p>R$<?php echo $totalCarrinho ?></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</header>
