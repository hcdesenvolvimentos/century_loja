<?php

/**
 * Template Name: Abas passo a passo
 * Description: Página Abas passo a passo
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Century_Sports_Loja
 */

get_header();
?>

<!-- PÁGINA PERGUNTAS FREQUENTES -->
<div class="pg pg-perguntas-frequentes">
	<div class="containerFull">
		<div class="row">
			<!-- SIDEBAR ATENDIMENTO -->
			<div class="col-sm-3">
				<div id="sidebar-atendimento">
					<?php
						//SIDEBAR
						include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
					?>
				</div>
			</div>
			<div class="col-sm-9">
				<h1 class="titulo-dois titulo-institucional"><?php echo get_the_title(); ?></h1>
				<div class="passos">
					
					<?php
						$contador = 0;

						global $wp;
						$current_url = home_url( add_query_arg( array(), $wp->request ) );
						if($current_url != home_url('/perguntas-frequentes')){
							$passos_tab = new WP_Query(array('post_type' => 'como_comprar', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1));
						} else{
							$passos_tab = new WP_Query(array('post_type' => 'perguntas_frequentes', 'orderby' => 'id', 'order' => 'asc', 'posts_per_page' => -1));
						}

						while($passos_tab->have_posts()): $passos_tab->the_post();
					?>
					<div class="passos-tab" id="<?php echo $contador; ?>">
						<h3 class="passos-tab-titulo">
							<a href="#" data-id="<?php echo $contador; ?>">
								<p><?php echo get_the_title(); ?></p>
								<figure>
									<img src="<?php echo get_template_directory_uri(); ?>/img/plus.svg" alt="" class="plus">
									<img src="<?php echo get_template_directory_uri(); ?>/img/minus.svg" alt="" class="minus">
								</figure>
							</a>
						</h3>
						<div class="passos-tab-conteudo">
							<p><?php echo get_the_content(); ?></p>
						</div>
					</div>
					<?php $contador++; endwhile; ?>
				</div>
			</div>
		</div>
	</div>
	<section class="secao-mapa">
		<h6 class="hidden">SEÇÃO MAPA</h6>
		<div class="maps" id="maps">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d450.53128144738525!2d-51.46798173240373!3d-25.39643251071809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef362dd527ed61%3A0xa6ba910955347a8d!2sCentury+Sports!5e0!3m2!1spt-BR!2sbr!4v1558361484767!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</section>
</div>

<?php get_footer();