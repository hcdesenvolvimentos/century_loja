<?php

/**
 * Template Name: Inicial
 * Description: Página Inicial
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Century_Sports_Loja
 */

get_header();
?>
<!-- PG INICIAL -->
	<div class="pg pg-inicial">
	
		<?php 
			//BANNER PRINCIPAL
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_destaque.php');

			//TEMPLATE BANNERS 
			include (TEMPLATEPATH . '/templates/minibanner.php'); 

			//TEMPLATE CARROSSEL DE CATEGORIAS
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_categorias.php');

			//TEMPLATE DE PRODUTOS POR CATEGORIAS
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_produto_categorias.php'); 

			//TEMPLATE CARROSSEL DE PRODUTO 1
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_produto_1.php');

			//TEMPLATE CARROSSEL DE PRODUTO 2
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_produto_2.php');

			//CARROSSEL PROMOCIONAL
			include (TEMPLATEPATH . '/templates/carrossel_promocional.php');

			//CARROSSEL PROMOCIONAL
			include (TEMPLATEPATH . '/templates/banner_promocional.php');

			//TEMPLATE CARROSSEL DE PRODUTO 3
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_produto_3.php');

			//TEMPLATE CARROSSEL DE PRODUTO 4
			include (TEMPLATEPATH . '/templates/carrossel_produto_home/carrossel_produto_4.php');

			//TEMPLATE CARROSSEL DE PRODUTO 4
			include (TEMPLATEPATH . '/templates/carrossel_marcas.php');
		?>

		
	

	

		<!-- <section class="secao-mapa">
			<h6 class="hidden">SEÇÃO MAPA</h6>
			<div class="maps" id="maps">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d450.53128144738525!2d-51.46798173240373!3d-25.39643251071809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef362dd527ed61%3A0xa6ba910955347a8d!2sCentury+Sports!5e0!3m2!1spt-BR!2sbr!4v1558361484767!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
			</div>
		</section> -->

	</div>
<?php get_footer();