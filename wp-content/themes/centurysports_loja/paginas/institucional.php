<?php

/**
* Template Name: Institucional
* Description: Página Institucional
*
* @link https://developer.wordpress.org/themes/basics/template-hierarchy/
*
* @package Century_Sports_Loja
*/

get_header();
?>

<!-- PÁGINA INSTITUCIONAL -->
<div class="pg pg-institucional">
	<div class="containerFull">
		<div class="row">
			<!-- SIDEBAR ATENDIMENTO -->
			<div class="col-sm-3">
				<div id="sidebar-atendimento">
					<?php
						//SIDEBAR
						include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
					?>
				</div>
			</div>
			<div class="col-sm-9">
				<h1 class="titulo-dois titulo-institucional"><?php echo get_the_title(); ?></h1>
				<div class="conteudo termos">
					<?php
						while(have_posts()){
							the_post(); 
							echo get_the_content();
						} 
					?>
				</div>
			</div>
		</div>
	</div>
</div>

<?php get_footer();