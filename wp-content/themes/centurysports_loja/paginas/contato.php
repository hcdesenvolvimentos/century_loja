<?php

/**
 * Template Name: Contato
 * Description: Página Contato
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Century_Sports_Loja
 */

get_header();
?>

<!-- PÁGINA DE CONTATO -->
<div class="pg pg-contato">
	<div class="containerFull">
		<div class="row">
			<!-- SIDEBAR ATENDIMENTO -->
			<div class="col-sm-3">
				<div id="sidebar-atendimento">
					<?php
						//SIDEBAR
						include (TEMPLATEPATH . '/templates/sidebar_institucional.php');
					?>
				</div>
			</div>
			<div class="col-sm-9">
				<div class="contato">
					<h1 class="titulo-dois titulo-institucional"><?php echo get_the_title(); ?></h1>
					<div class="formulario">
						<!-- <label for="nome">Seu nome(obrigatório)</label>
						<input type="text" name="nome" id="nome" placeholder="Digite seu nome">
						<label for="email">Seu e-mail(obrigatório)</label>
						<input type="text" name="email" id="email" placeholder="Digite seu e-mail">
						<label for="cidade">Sua cidade</label>
						<input type="text" name="cidade" id="cidade" placeholder="Digite sua cidade">
						<label for="telefone">Seu telefone</label>
						<input type="text" name="telefone" id="telefone" placeholder="Digite seu telefone">
						<label for="mensagem">Sua mensagem</label>
						<textarea name="mensagem" id="mensagem" placeholder="Fala com a gente :)"></textarea>
						<input type="submit" value="Enviar"> -->
						<?php echo do_shortcode('[contact-form-7 id="126" title="Fomulário de contato"]'); ?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<section class="secao-mapa">
		<h6 class="hidden">SEÇÃO MAPA</h6>
		<div class="maps" id="maps">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d450.53128144738525!2d-51.46798173240373!3d-25.39643251071809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef362dd527ed61%3A0xa6ba910955347a8d!2sCentury+Sports!5e0!3m2!1spt-BR!2sbr!4v1558361484767!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</section>
</div>

<?php get_footer();