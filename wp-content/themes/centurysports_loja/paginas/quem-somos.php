<?php

/**
 * Template Name: Quem somos
 * Description: Página Quem somos
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package Century_Sports_Loja
 */

get_header();
?>

<!-- PÁGINA QUEM SOMOS -->
<div class="pg pg-quem-somos">
	<div class="containerFull">
		<div class="quem-somos">
			<h1 class="titulo-dois titulo-institucional titulo-quem-somos"><?php echo get_the_title(); ?></h1>
			<p class="desc-sobre"><?php echo $configuracao['opt_descricao_inicial_quem_somos']; ?></p>
			<figure class="banner">
				<img src="<?php echo $configuracao['opt_banner_quem_somos']['url']; ?>" alt="Banner quem somos">
			</figure>
			<div class="sobre">
				<div class="background-laranja frase-destaque aqui-voce-encontra"><p><?php echo $configuracao['opt_frases_destaque_1']; ?></p></div>
				<div class="background-roxo frase-destaque atributos"><p><?php echo $configuracao['opt_frases_destaque_2']; ?></p></div>
				<div class="background-laranja frase-destaque vem-pra-century"><p><?php echo $configuracao['opt_frases_destaque_3']; ?></p></div>
			</div>
			<p class="desc-sobre"><?php echo $configuracao['opt_descricao_final_quem_somos']; ?></p>
		</div>
	</div>
	<section class="secao-mapa">
		<h6 class="hidden">SEÇÃO MAPA</h6>
		<div class="maps" id="maps">
			<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d450.53128144738525!2d-51.46798173240373!3d-25.39643251071809!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94ef362dd527ed61%3A0xa6ba910955347a8d!2sCentury+Sports!5e0!3m2!1spt-BR!2sbr!4v1558361484767!5m2!1spt-BR!2sbr" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
		</div>
	</section>
</div>

<?php get_footer();