-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 03-Jul-2019 às 13:32
-- Versão do servidor: 5.7.24
-- versão do PHP: 7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_centurysports_loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_commentmeta`
--

DROP TABLE IF EXISTS `cp_commentmeta`;
CREATE TABLE IF NOT EXISTS `cp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_comments`
--

DROP TABLE IF EXISTS `cp_comments`;
CREATE TABLE IF NOT EXISTS `cp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10)),
  KEY `woo_idx_comment_type` (`comment_type`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_comments`
--

INSERT INTO `cp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(8, 14, 'ActionScheduler', '', '', '', '2019-05-30 19:03:32', '2019-05-30 22:03:32', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(9, 14, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(10, 14, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(11, 15, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(12, 15, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(13, 15, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(14, 16, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(15, 16, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(16, 16, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(17, 17, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(18, 17, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(19, 17, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(20, 24, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(21, 24, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(22, 24, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(23, 31, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(24, 31, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(25, 31, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(26, 46, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(27, 46, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(28, 46, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(29, 49, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(30, 49, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(31, 49, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(32, 59, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(33, 59, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(34, 59, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(35, 60, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(36, 60, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(37, 60, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(38, 61, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(39, 61, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(40, 61, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(41, 62, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(42, 62, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(43, 62, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(44, 63, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(45, 63, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(46, 63, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(47, 64, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(48, 64, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(49, 64, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(50, 68, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(51, 68, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(52, 68, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(53, 69, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(54, 69, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(55, 69, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(56, 83, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(57, 83, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(58, 83, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(59, 84, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(60, 84, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(61, 84, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(62, 86, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(63, 86, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(64, 86, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(65, 93, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(66, 93, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(67, 93, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(68, 94, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(69, 94, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(70, 94, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(71, 95, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(72, 95, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(73, 95, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(74, 96, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(75, 96, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(76, 96, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(77, 110, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(78, 110, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(79, 110, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(80, 111, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(81, 111, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(82, 111, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(83, 112, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(84, 112, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(85, 112, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(86, 113, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_links`
--

DROP TABLE IF EXISTS `cp_links`;
CREATE TABLE IF NOT EXISTS `cp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_options`
--

DROP TABLE IF EXISTS `cp_options`;
CREATE TABLE IF NOT EXISTS `cp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2388 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_options`
--

INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://localhost/projetos/century_loja', 'yes'),
(2, 'home', 'http://localhost/projetos/century_loja', 'yes'),
(3, 'blogname', 'Century Sports', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '5', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '5', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:209:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"loja/?$\";s:27:\"index.php?post_type=product\";s:37:\"loja/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"loja/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"loja/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:18:\"banner-promocao/?$\";s:28:\"index.php?post_type=promocao\";s:48:\"banner-promocao/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=promocao&feed=$matches[1]\";s:43:\"banner-promocao/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=promocao&feed=$matches[1]\";s:35:\"banner-promocao/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=promocao&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\"categoria/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:43:\"categoria/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:24:\"categoria/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:36:\"categoria/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:18:\"categoria/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"produto/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"produto/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"produto/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"produto/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"produto/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"produto/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"produto/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"produto/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"produto/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"produto/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"produto/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"produto/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"produto/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"produto/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"produto/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"produto/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:34:\"destaque/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?destaque=$matches[1]&wc-api=$matches[3]\";s:40:\"destaque/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"destaque/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:43:\"banner-promocao/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:53:\"banner-promocao/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:73:\"banner-promocao/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"banner-promocao/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"banner-promocao/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:49:\"banner-promocao/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"banner-promocao/([^/]+)/embed/?$\";s:41:\"index.php?promocao=$matches[1]&embed=true\";s:36:\"banner-promocao/([^/]+)/trackback/?$\";s:35:\"index.php?promocao=$matches[1]&tb=1\";s:56:\"banner-promocao/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?promocao=$matches[1]&feed=$matches[2]\";s:51:\"banner-promocao/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?promocao=$matches[1]&feed=$matches[2]\";s:44:\"banner-promocao/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?promocao=$matches[1]&paged=$matches[2]\";s:51:\"banner-promocao/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?promocao=$matches[1]&cpage=$matches[2]\";s:41:\"banner-promocao/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?promocao=$matches[1]&wc-api=$matches[3]\";s:47:\"banner-promocao/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:58:\"banner-promocao/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:40:\"banner-promocao/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?promocao=$matches[1]&page=$matches[2]\";s:32:\"banner-promocao/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"banner-promocao/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"banner-promocao/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"banner-promocao/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"banner-promocao/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"banner-promocao/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=43&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:7:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:2;s:41:\"base-centurySports/base-centurySports.php\";i:3;s:39:\"categories-images/categories-images.php\";i:4;s:21:\"meta-box/meta-box.php\";i:5;s:37:\"woocommerce-products-filter/index.php\";i:6;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'centurysports_loja', 'yes'),
(41, 'stylesheet', 'centurysports_loja', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'file', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '43', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'cp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:115:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:5:{i:0;s:26:\"woocommerce_price_filter-2\";i:1;s:32:\"woocommerce_product_categories-2\";i:2;s:33:\"woocommerce_layered_nav_filters-3\";i:3;s:25:\"woocommerce_layered_nav-3\";i:4;s:25:\"woocommerce_widget_cart-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:15:{i:1561982567;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1561985047;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1561986143;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1561992886;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1562013846;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562013847;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1562013892;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562013894;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562014486;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562014496;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562025286;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562036400;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562059051;a:1:{s:40:\"fs_data_sync_ajax-search-for-woocommerce\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562241600;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(2228, '_transient_wc_shipping_method_count', 'a:2:{s:7:\"version\";s:10:\"1559249916\";s:5:\"value\";i:2;}', 'no'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1559249547;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(555, 'duplicate_post_copyformat', '1', 'yes'),
(556, 'duplicate_post_copyauthor', '0', 'yes'),
(557, 'duplicate_post_copypassword', '0', 'yes'),
(558, 'duplicate_post_copyattachments', '0', 'yes'),
(559, 'duplicate_post_copychildren', '0', 'yes'),
(560, 'duplicate_post_copycomments', '0', 'yes'),
(561, 'duplicate_post_copymenuorder', '1', 'yes'),
(562, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(563, 'duplicate_post_blacklist', '', 'yes'),
(564, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(565, 'duplicate_post_show_row', '1', 'yes'),
(566, 'duplicate_post_show_adminbar', '1', 'yes'),
(567, 'duplicate_post_show_submitbox', '1', 'yes'),
(568, 'duplicate_post_show_bulkactions', '1', 'yes'),
(569, 'duplicate_post_version', '3.2.2', 'yes'),
(570, 'duplicate_post_show_notice', '1', 'no'),
(430, 'woocommerce_tracker_ua', 'a:2:{i:0;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";i:1;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";}', 'yes'),
(327, 'wc_admin_last_orders_milestone', '0', 'yes'),
(323, 'wc_admin_install_timestamp', '1561902281', 'yes'),
(2374, '_site_transient_timeout_theme_roots', '1561984345', 'no'),
(2375, '_site_transient_theme_roots', 'a:1:{s:18:\"centurysports_loja\";s:7:\"/themes\";}', 'no'),
(613, 'fs_active_plugins', 'O:8:\"stdClass\":3:{s:7:\"plugins\";a:1:{s:34:\"ajax-search-for-woocommerce/fs/lib\";O:8:\"stdClass\":4:{s:7:\"version\";s:5:\"2.2.4\";s:4:\"type\";s:6:\"plugin\";s:9:\"timestamp\";i:1561337867;s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:7:\"abspath\";s:36:\"C:\\wamp64\\www\\projetos\\century_loja/\";s:6:\"newest\";O:8:\"stdClass\":5:{s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:8:\"sdk_path\";s:34:\"ajax-search-for-woocommerce/fs/lib\";s:7:\"version\";s:5:\"2.2.4\";s:13:\"in_activation\";b:0;s:9:\"timestamp\";i:1561337867;}}', 'yes'),
(155, 'woocommerce_default_country', 'BR:PR', 'yes'),
(152, 'woocommerce_store_address', 'R. Saldanha Marinho, 1301 - Centro, - PR', 'yes'),
(153, 'woocommerce_store_address_2', '', 'yes'),
(614, 'fs_debug_mode', '', 'yes'),
(477, '_site_transient_timeout_browser_bcf1814caa6afe84eeebef28ff236a7f', '1561936942', 'no'),
(478, '_site_transient_browser_bcf1814caa6afe84eeebef28ff236a7f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"75.0.3770.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(129, 'can_compress_scripts', '1', 'no'),
(154, 'woocommerce_store_city', 'Guarapuava', 'yes'),
(140, 'current_theme', 'Century Sports Loja', 'yes'),
(141, 'theme_mods_centurysports_loja', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:18;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(145, 'recently_activated', 'a:3:{s:39:\"woocommerce-admin/woocommerce-admin.php\";i:1561904612;s:41:\"yith-woocommerce-ajax-navigation/init.php\";i:1561903552;s:33:\"duplicate-post/duplicate-post.php\";i:1561335306;}', 'yes'),
(480, '_site_transient_php_check_464f4068caea2f8f3edcc5ae59429c65', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(479, '_site_transient_timeout_php_check_464f4068caea2f8f3edcc5ae59429c65', '1561936943', 'no'),
(156, 'woocommerce_store_postcode', '85010-290', 'yes'),
(157, 'woocommerce_allowed_countries', 'all', 'yes'),
(158, 'woocommerce_all_except_countries', '', 'yes'),
(159, 'woocommerce_specific_allowed_countries', '', 'yes'),
(160, 'woocommerce_ship_to_countries', '', 'yes'),
(161, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(162, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(163, 'woocommerce_calc_taxes', 'no', 'yes'),
(164, 'woocommerce_enable_coupons', 'yes', 'yes'),
(165, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(166, 'woocommerce_currency', 'BRL', 'yes'),
(167, 'woocommerce_currency_pos', 'left', 'yes'),
(168, 'woocommerce_price_thousand_sep', '.', 'yes'),
(169, 'woocommerce_price_decimal_sep', ',', 'yes'),
(170, 'woocommerce_price_num_decimals', '2', 'yes'),
(171, 'woocommerce_shop_page_id', '6', 'yes'),
(172, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(173, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(174, 'woocommerce_placeholder_image', '5', 'yes'),
(175, 'woocommerce_weight_unit', 'kg', 'yes'),
(176, 'woocommerce_dimension_unit', 'cm', 'yes'),
(177, 'woocommerce_enable_reviews', 'yes', 'yes'),
(178, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(179, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(180, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(181, 'woocommerce_review_rating_required', 'yes', 'no'),
(182, 'woocommerce_manage_stock', 'yes', 'yes'),
(183, 'woocommerce_hold_stock_minutes', '60', 'no'),
(184, 'woocommerce_notify_low_stock', 'yes', 'no'),
(185, 'woocommerce_notify_no_stock', 'yes', 'no'),
(186, 'woocommerce_stock_email_recipient', 'devhcdesenvolvimentos@gmail.com', 'no'),
(187, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(188, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(189, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(190, 'woocommerce_stock_format', '', 'yes'),
(191, 'woocommerce_file_download_method', 'force', 'no'),
(192, 'woocommerce_downloads_require_login', 'no', 'no'),
(193, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(194, 'woocommerce_prices_include_tax', 'no', 'yes'),
(195, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(196, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(197, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(198, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes'),
(199, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(200, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(201, 'woocommerce_price_display_suffix', '', 'yes'),
(202, 'woocommerce_tax_total_display', 'itemized', 'no'),
(203, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(204, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(205, 'woocommerce_ship_to_destination', 'billing', 'no'),
(206, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(207, 'woocommerce_enable_guest_checkout', 'no', 'no'),
(208, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(209, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(210, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(211, 'woocommerce_registration_generate_username', 'yes', 'no'),
(212, 'woocommerce_registration_generate_password', 'yes', 'no'),
(213, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(214, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(215, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(216, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(217, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(218, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(219, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(220, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(221, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(222, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(223, 'woocommerce_email_from_name', 'Century Sports', 'no'),
(224, 'woocommerce_email_from_address', 'devhcdesenvolvimentos@gmail.com', 'no'),
(225, 'woocommerce_email_header_image', '', 'no'),
(226, 'woocommerce_email_footer_text', '{site_title}<br/>Built with <a href=\"https://woocommerce.com/\">WooCommerce</a>', 'no'),
(227, 'woocommerce_email_base_color', '#96588a', 'no'),
(228, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(229, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(230, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(231, 'woocommerce_cart_page_id', '7', 'yes'),
(232, 'woocommerce_checkout_page_id', '8', 'yes'),
(233, 'woocommerce_myaccount_page_id', '9', 'yes'),
(234, 'woocommerce_terms_page_id', '', 'no'),
(235, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(236, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(237, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(238, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(239, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(240, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(241, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(242, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(243, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(244, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(245, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(246, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(247, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(248, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(249, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(250, 'woocommerce_api_enabled', 'no', 'yes'),
(251, 'woocommerce_allow_tracking', 'yes', 'no'),
(252, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(253, 'woocommerce_single_image_width', '600', 'yes'),
(254, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(255, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(256, 'woocommerce_demo_store', 'no', 'no'),
(257, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:8:\"/produto\";s:13:\"category_base\";s:9:\"categoria\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(258, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(259, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(1757, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(262, 'default_product_cat', '15', 'yes'),
(265, 'woocommerce_version', '3.6.4', 'yes'),
(266, 'woocommerce_db_version', '3.6.4', 'yes'),
(267, 'woocommerce_admin_notices', 'a:1:{i:1;s:20:\"no_secure_connection\";}', 'yes'),
(268, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(269, 'widget_woocommerce_widget_cart', 'a:2:{i:2;a:2:{s:5:\"title\";s:8:\"Carrinho\";s:13:\"hide_if_empty\";i:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(270, 'widget_woocommerce_layered_nav_filters', 'a:2:{i:3;a:1:{s:5:\"title\";s:14:\"Ativar filtros\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'widget_woocommerce_layered_nav', 'a:2:{i:3;a:4:{s:5:\"title\";s:11:\"Filtrar por\";s:9:\"attribute\";s:0:\"\";s:12:\"display_type\";s:4:\"list\";s:10:\"query_type\";s:3:\"and\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:\"title\";s:18:\"Filtrar por preço\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:8:{s:5:\"title\";s:0:\"\";s:7:\"orderby\";s:5:\"order\";s:8:\"dropdown\";i:0;s:5:\"count\";i:0;s:12:\"hierarchical\";i:1;s:18:\"show_children_only\";i:0;s:10:\"hide_empty\";i:0;s:9:\"max_depth\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(279, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(291, 'woocommerce_obw_last_completed_step', 'recommended', 'yes'),
(2370, '_transient_timeout_external_ip_address_::1', '1562587342', 'no'),
(299, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(300, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(294, 'woocommerce_product_type', 'physical', 'yes'),
(2371, '_transient_external_ip_address_::1', '177.82.27.37', 'no'),
(301, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(298, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";b:0;}', 'yes'),
(296, 'woocommerce_tracker_last_send', '1561332123', 'yes'),
(302, '_transient_shipping-transient-version', '1559249916', 'yes'),
(303, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:2:\"17\";}', 'yes'),
(304, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:2:\"17\";}', 'yes'),
(354, '_transient_product-transient-version', '1561919792', 'yes'),
(328, '_transient_product_query-transient-version', '1561919792', 'yes'),
(1629, 'wc_admin_version', '0.14.0', 'yes'),
(463, 'category_children', 'a:0:{}', 'yes'),
(2377, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1561982548;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(2378, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1561982549;s:7:\"checked\";a:1:{s:18:\"centurysports_loja\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(2211, '_transient_wc_term_counts', 'a:16:{i:30;s:1:\"6\";i:33;s:1:\"5\";i:22;s:1:\"7\";i:21;s:1:\"9\";i:20;s:1:\"9\";i:19;s:1:\"9\";i:34;s:1:\"4\";i:32;s:1:\"3\";i:36;s:1:\"4\";i:31;s:1:\"4\";i:29;s:1:\"3\";i:24;s:1:\"7\";i:35;s:1:\"3\";i:23;s:1:\"9\";i:27;s:1:\"7\";i:28;s:2:\"16\";}', 'no'),
(2301, '_transient_wc_related_107', 'a:1:{s:51:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=107\";a:10:{i:0;s:2:\"12\";i:1;s:2:\"52\";i:2;s:2:\"54\";i:3;s:2:\"56\";i:4;s:2:\"58\";i:5;s:2:\"65\";i:6;s:2:\"97\";i:7;s:3:\"106\";i:8;s:3:\"108\";i:9;s:3:\"109\";}}', 'no'),
(2210, '_transient_timeout_wc_term_counts', '1564511794', 'no'),
(2379, '_transient_timeout_wc_related_97', '1562068948', 'no'),
(2380, '_transient_wc_related_97', 'a:1:{s:50:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=97\";a:13:{i:0;s:2:\"12\";i:1;s:2:\"52\";i:2;s:2:\"54\";i:3;s:2:\"56\";i:4;s:2:\"58\";i:5;s:2:\"65\";i:6;s:2:\"91\";i:7;s:3:\"104\";i:8;s:3:\"105\";i:9;s:3:\"106\";i:10;s:3:\"107\";i:11;s:3:\"108\";i:12;s:3:\"109\";}}', 'no'),
(514, 'recovery_mode_email_last_sent', '1561333290', 'yes'),
(488, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(549, 'duplicate_post_copystatus', '0', 'yes'),
(550, 'duplicate_post_copyslug', '0', 'yes'),
(551, 'duplicate_post_copyexcerpt', '1', 'yes'),
(552, 'duplicate_post_copycontent', '1', 'yes'),
(553, 'duplicate_post_copythumbnail', '1', 'yes'),
(554, 'duplicate_post_copytemplate', '1', 'yes'),
(494, 'configuracao', 'a:48:{s:8:\"last_tab\";s:1:\"1\";s:11:\"header_logo\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/logo-1.png\";s:2:\"id\";s:2:\"25\";s:6:\"height\";s:2:\"90\";s:5:\"width\";s:3:\"500\";s:9:\"thumbnail\";s:83:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/logo-1-150x90.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"header_telefone\";s:13:\"(42)3622 9592\";s:12:\"header_frase\";s:32:\"COMPRE NO SITE E RETIRE NA LOJA!\";s:15:\"header_endereco\";s:35:\" Rua Saldanha Marinho, 1302, Centro\";s:12:\"header_links\";a:3:{i:0;s:13:\"MEUS PEDIDO|#\";i:1;s:8:\"AJUDA |#\";i:2;s:13:\"ATENDIMENTO|#\";}s:27:\"config_site_rodape_endereco\";s:65:\"RUA SALDANHA MARINHO, 123, CENTRO - GUARAPUAVA/PR - CEP 85010-290\";s:26:\"config_site_rodape_contato\";s:14:\"(12) 3456-7890\";s:27:\"config_site_rodape_whatsapp\";s:14:\"(12) 3456-7890\";s:38:\"config_site_rodape_horario_atendimento\";s:62:\"Segunda a Sexta das 9:00 às 18:00 Sábado das 09:00 às 14:00\";s:23:\"config_site_rodape_info\";s:32:\"CENTURY SPORTS | CNPJ 1234567890\";s:28:\"config_site_rodape_copyright\";s:29:\"Todos os direitos reservados.\";s:27:\"config_site_rodape_linkedin\";s:1:\"#\";s:26:\"config_site_rodape_twitter\";s:1:\"#\";s:27:\"config_site_rodape_facebook\";s:1:\"#\";s:33:\"inicial_mini_banner_modelo_hidden\";s:1:\"0\";s:26:\"inicial_mini_banner_modelo\";s:1:\"0\";s:21:\"inicial_mini_banner_1\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_1\";s:16:\"Título banner 1\";s:26:\"inicial_mini_banner_link_1\";s:2:\"#1\";s:21:\"inicial_mini_banner_2\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_2\";s:11:\"lo banner 2\";s:26:\"inicial_mini_banner_link_2\";s:2:\"#2\";s:21:\"inicial_mini_banner_3\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titul0_3\";s:16:\"Título banner 3\";s:26:\"inicial_mini_banner_lin0_3\";s:2:\"#3\";s:21:\"inicial_mini_banner_4\";a:9:{s:3:\"url\";s:82:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_4\";s:16:\"Título banner 4\";s:26:\"inicial_mini_banner_link_4\";s:2:\"#4\";s:38:\"inicial_sessao_categoroia_carrossel_id\";s:2:\"28\";s:32:\"inicial_sessao_categoroia_titulo\";s:23:\"VOCÊ TAMBÉM ENCONTRA:\";s:35:\"inicial_sessao_categoroia_carrossel\";s:1:\"0\";s:43:\"inicial_sessao_categoroia_carrossel_produto\";s:1:\"0\";s:28:\"inicial_sessao_categoroia_id\";s:2:\"27\";s:51:\"inicial_sessao_categoroia_carrossel_selecao_produto\";a:4:{i:0;s:4:\"nike\";i:1;s:6:\"adidas\";i:2;s:7:\"minzuno\";i:3;s:8:\"all-star\";}s:44:\"inicial_sessao_carrossel_departamento_titulo\";s:8:\"Feminino\";s:42:\"inicial_sessao_carrossel_departamento_slug\";s:8:\"bermudas\";s:46:\"inicial_sessao_carrossel_departamento_titulo_2\";s:7:\"Regatas\";s:44:\"inicial_sessao_carrossel_departamento_slug_2\";s:7:\"regatas\";s:46:\"inicial_sessao_carrossel_departamento_titulo_3\";s:6:\"Tênis\";s:44:\"inicial_sessao_carrossel_departamento_slug_3\";s:5:\"tenis\";s:46:\"inicial_sessao_carrossel_departamento_titulo_4\";s:9:\"Vestuario\";s:44:\"inicial_sessao_carrossel_departamento_slug_4\";s:9:\"vestuario\";s:26:\"inicial_banner_promocional\";a:9:{s:3:\"url\";s:76:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/banner.png\";s:2:\"id\";s:2:\"76\";s:6:\"height\";s:3:\"279\";s:5:\"width\";s:4:\"1920\";s:9:\"thumbnail\";s:84:\"http://localhost/projetos/century_loja/wp-content/uploads/2019/06/banner-150x150.png\";s:5:\"title\";s:6:\"banner\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:31:\"inicial_banner_promocional_link\";s:1:\"#\";s:23:\"inicial_marcas_esconder\";s:1:\"0\";s:48:\"inicial_banner_promocional_promocionais_esconder\";s:1:\"0\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(495, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:1:{s:26:\"inicial_mini_banner_modelo\";s:1:\"1\";}s:9:\"last_save\";i:1561843937;}', 'yes'),
(548, 'duplicate_post_copydate', '0', 'yes'),
(547, 'duplicate_post_copytitle', '1', 'yes'),
(615, 'fs_accounts', 'a:13:{s:21:\"id_slug_type_path_map\";a:1:{i:700;a:3:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:4:\"type\";s:6:\"plugin\";s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:11:\"plugin_data\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:20:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}s:17:\"install_timestamp\";i:1561337867;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.2.4\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:5:\"1.4.0\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:9:\"localhost\";s:9:\"server_ip\";s:3:\"::1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1561337867;s:7:\"version\";s:5:\"1.4.0\";}s:17:\"was_plugin_loaded\";b:1;s:15:\"prev_is_premium\";b:0;s:14:\"has_trial_plan\";b:0;s:22:\"install_sync_timestamp\";i:1561982551;s:19:\"keepalive_timestamp\";i:1561982551;s:20:\"activation_timestamp\";i:1561337893;s:9:\"sync_cron\";O:8:\"stdClass\":5:{s:7:\"version\";s:5:\"1.4.0\";s:7:\"blog_id\";i:0;s:11:\"sdk_version\";s:5:\"2.2.4\";s:9:\"timestamp\";i:1561337899;s:2:\"on\";b:1;}s:14:\"sync_timestamp\";i:1561982550;}}s:13:\"file_slug_map\";a:1:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:27:\"ajax-search-for-woocommerce\";}s:7:\"plugins\";a:1:{s:27:\"ajax-search-for-woocommerce\";O:9:\"FS_Plugin\":20:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:12:\"premium_slug\";s:35:\"ajax-search-for-woocommerce-premium\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:4:\"file\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:7:\"version\";s:5:\"1.4.0\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:14:\"premium_suffix\";s:3:\"Pro\";s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_f4f2a51dbe0aee43de0692db77a3e\";s:10:\"secret_key\";N;s:2:\"id\";s:3:\"700\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"fc8a93668c09868b3795703092a94f0d\";s:5:\"plans\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:2:{i:0;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:4:\"NzAw\";s:4:\"name\";s:8:\"ZnJlZQ==\";s:5:\"title\";s:12:\"U3RhcnRlcg==\";s:11:\"description\";s:40:\"U2VhcmNoIHByb2R1Y3RzIGluIGxpdmUgdGltZQ==\";s:17:\"is_free_localhost\";s:4:\"MQ==\";s:17:\"is_block_features\";s:4:\"MQ==\";s:12:\"license_type\";s:4:\"MA==\";s:16:\"is_https_support\";s:0:\"\";s:12:\"trial_period\";N;s:23:\"is_require_subscription\";s:0:\"\";s:10:\"support_kb\";N;s:13:\"support_forum\";s:88:\"aHR0cHM6Ly93b3JkcHJlc3Mub3JnL3N1cHBvcnQvcGx1Z2luL2FqYXgtc2VhcmNoLWZvci13b29jb21tZXJjZQ==\";s:13:\"support_email\";N;s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";s:0:\"\";s:11:\"is_featured\";s:0:\"\";s:2:\"id\";s:8:\"MjkwMg==\";s:7:\"updated\";s:28:\"MjAxOC0xMi0yOCAwMDowNDowNQ==\";s:7:\"created\";s:28:\"MjAxOC0wNC0wNyAyMTozNjo1Ng==\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}i:1;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:4:\"NzAw\";s:4:\"name\";s:4:\"cHJv\";s:5:\"title\";s:4:\"UHJv\";s:11:\"description\";s:76:\"I0luY3JlYXNlIHlvdXIgc2FsZXMgYnkgZmFzdGVyIGFuZCBtb2Rlcm4gc2VhcmNoIGVuZ2luZQ==\";s:17:\"is_free_localhost\";s:4:\"MQ==\";s:17:\"is_block_features\";s:4:\"MQ==\";s:12:\"license_type\";s:4:\"MA==\";s:16:\"is_https_support\";s:0:\"\";s:12:\"trial_period\";N;s:23:\"is_require_subscription\";s:4:\"MQ==\";s:10:\"support_kb\";N;s:13:\"support_forum\";N;s:13:\"support_email\";s:32:\"ZGdvcmFwbHVnaW5zQGdtYWlsLmNvbQ==\";s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";s:0:\"\";s:11:\"is_featured\";s:4:\"MQ==\";s:2:\"id\";s:8:\"NDc5OQ==\";s:7:\"updated\";s:28:\"MjAxOS0wNC0wOSAxNjoyMjo0Mg==\";s:7:\"created\";s:28:\"MjAxOC0xMi0yNyAyMzoxODowNg==\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}}s:14:\"active_plugins\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1561982550;s:3:\"md5\";s:32:\"b6119f087000d4defdf7e9b729023acb\";s:7:\"plugins\";a:7:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";a:5:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:7:\"version\";s:5:\"1.4.0\";s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:41:\"base-centurySports/base-centurySports.php\";a:5:{s:4:\"slug\";s:18:\"base-centurySports\";s:7:\"version\";s:3:\"0.1\";s:5:\"title\";s:19:\"Base Century Sports\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:21:\"meta-box/meta-box.php\";a:5:{s:4:\"slug\";s:8:\"meta-box\";s:7:\"version\";s:6:\"4.18.2\";s:5:\"title\";s:8:\"Meta Box\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:35:\"redux-framework/redux-framework.php\";a:5:{s:4:\"slug\";s:15:\"redux-framework\";s:7:\"version\";s:6:\"3.6.15\";s:5:\"title\";s:15:\"Redux Framework\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:27:\"woocommerce/woocommerce.php\";a:5:{s:4:\"slug\";s:11:\"woocommerce\";s:7:\"version\";s:5:\"3.6.4\";s:5:\"title\";s:11:\"WooCommerce\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:39:\"categories-images/categories-images.php\";a:5:{s:4:\"slug\";s:17:\"categories-images\";s:7:\"version\";s:5:\"2.5.4\";s:5:\"title\";s:17:\"Categories Images\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:37:\"woocommerce-products-filter/index.php\";a:5:{s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:7:\"version\";s:7:\"1.2.2.1\";s:5:\"title\";s:34:\"WOOF - WooCommerce Products Filter\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}}}s:11:\"all_plugins\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1561982550;s:3:\"md5\";s:32:\"c7b48ae79335731f42b58c4998f2e579\";s:7:\"plugins\";a:9:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";a:5:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:7:\"version\";s:5:\"1.4.0\";s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:41:\"base-centurySports/base-centurySports.php\";a:5:{s:4:\"slug\";s:18:\"base-centurySports\";s:7:\"version\";s:3:\"0.1\";s:5:\"title\";s:19:\"Base Century Sports\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:21:\"meta-box/meta-box.php\";a:5:{s:4:\"slug\";s:8:\"meta-box\";s:7:\"version\";s:6:\"4.18.2\";s:5:\"title\";s:8:\"Meta Box\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:35:\"redux-framework/redux-framework.php\";a:5:{s:4:\"slug\";s:15:\"redux-framework\";s:7:\"version\";s:6:\"3.6.15\";s:5:\"title\";s:15:\"Redux Framework\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:27:\"woocommerce/woocommerce.php\";a:5:{s:4:\"slug\";s:11:\"woocommerce\";s:7:\"version\";s:5:\"3.6.4\";s:5:\"title\";s:11:\"WooCommerce\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:39:\"woocommerce-admin/woocommerce-admin.php\";a:6:{s:4:\"slug\";s:17:\"woocommerce-admin\";s:7:\"version\";s:6:\"0.12.0\";s:5:\"title\";s:17:\"WooCommerce Admin\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;s:7:\"Version\";s:6:\"0.14.0\";}s:39:\"categories-images/categories-images.php\";a:5:{s:4:\"slug\";s:17:\"categories-images\";s:7:\"version\";s:5:\"2.5.4\";s:5:\"title\";s:17:\"Categories Images\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:37:\"woocommerce-products-filter/index.php\";a:5:{s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:7:\"version\";s:7:\"1.2.2.1\";s:5:\"title\";s:34:\"WOOF - WooCommerce Products Filter\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:41:\"yith-woocommerce-ajax-navigation/init.php\";a:5:{s:4:\"slug\";s:32:\"yith-woocommerce-ajax-navigation\";s:7:\"version\";s:5:\"3.6.6\";s:5:\"title\";s:36:\"YITH WooCommerce Ajax Product Filter\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}}}s:10:\"all_themes\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1561982551;s:3:\"md5\";s:32:\"f9887237554bc5c5b2d13d2aca67df81\";s:6:\"themes\";a:1:{s:18:\"centurysports_loja\";a:5:{s:4:\"slug\";s:18:\"centurysports_loja\";s:7:\"version\";s:3:\"1.0\";s:5:\"title\";s:15:\"Amanda Karoline\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}}}s:5:\"sites\";a:1:{s:27:\"ajax-search-for-woocommerce\";O:7:\"FS_Site\":25:{s:7:\"site_id\";s:8:\"10422296\";s:9:\"plugin_id\";s:3:\"700\";s:7:\"user_id\";s:7:\"1979899\";s:5:\"title\";s:14:\"Century Sports\";s:3:\"url\";s:38:\"http://localhost/projetos/century_loja\";s:7:\"version\";s:5:\"1.4.0\";s:8:\"language\";s:5:\"pt-BR\";s:7:\"charset\";s:5:\"UTF-8\";s:16:\"platform_version\";s:5:\"5.2.1\";s:11:\"sdk_version\";s:5:\"2.2.4\";s:28:\"programming_language_version\";s:6:\"7.2.10\";s:7:\"plan_id\";s:4:\"2902\";s:10:\"license_id\";N;s:13:\"trial_plan_id\";N;s:10:\"trial_ends\";N;s:10:\"is_premium\";b:0;s:15:\"is_disconnected\";b:0;s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;s:10:\"public_key\";s:32:\"pk_266d8c2ff927c47c8896d143b985f\";s:10:\"secret_key\";s:32:\"sk_QsGs)T$*[O^lYEJtLt1K.nh7cI0]@\";s:2:\"id\";s:7:\"2757685\";s:7:\"updated\";N;s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:5:\"users\";a:1:{i:1979899;O:7:\"FS_User\":12:{s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:5:\"first\";s:13:\"centurysports\";s:4:\"last\";s:0:\"\";s:11:\"is_verified\";b:0;s:11:\"customer_id\";N;s:5:\"gross\";i:0;s:10:\"public_key\";s:32:\"pk_e708ffe655728bcccb003c5a536ef\";s:10:\"secret_key\";s:32:\"sk_sKl=s44[b1{F<^^[7kp2qv[U-gAq{\";s:2:\"id\";s:7:\"1979899\";s:7:\"updated\";N;s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:23:\"user_id_license_ids_map\";a:1:{i:700;a:1:{i:1979899;a:0:{}}}s:12:\"all_licenses\";a:1:{i:700;a:0:{}}}', 'yes'),
(645, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(1624, '_transient_timeout__woocommerce_helper_updates', '1561945472', 'no'),
(1625, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1561902272;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(616, 'fs_api_cache', 'a:5:{s:29:\"get:/v1/plugins/700/info.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":14:{s:9:\"plugin_id\";s:3:\"700\";s:3:\"url\";N;s:11:\"description\";N;s:17:\"short_description\";N;s:10:\"banner_url\";N;s:15:\"card_banner_url\";N;s:15:\"selling_point_0\";N;s:15:\"selling_point_1\";N;s:15:\"selling_point_2\";N;s:11:\"screenshots\";N;s:2:\"id\";s:3:\"682\";s:7:\"created\";s:19:\"2019-03-01 23:26:26\";s:7:\"updated\";N;s:4:\"icon\";s:92:\"//s3-us-west-2.amazonaws.com/freemius/plugins/700/icons/b73778ae43d2248effda74c277ca3025.jpg\";}s:7:\"created\";i:1561337872;s:9:\"timestamp\";i:1561942672;}s:26:\"get:/v1/users/1979899.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":16:{s:15:\"default_card_id\";N;s:5:\"gross\";i:0;s:6:\"source\";i:0;s:13:\"last_login_at\";N;s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:5:\"first\";s:13:\"centurysports\";s:4:\"last\";s:0:\"\";s:7:\"picture\";N;s:2:\"ip\";s:14:\"191.177.186.40\";s:11:\"is_verified\";b:0;s:10:\"secret_key\";s:32:\"sk_sKl=s44[b1{F<^^[7kp2qv[U-gAq{\";s:10:\"public_key\";s:32:\"pk_e708ffe655728bcccb003c5a536ef\";s:2:\"id\";s:7:\"1979899\";s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:7:\"updated\";N;s:11:\"customer_id\";N;}s:7:\"created\";i:1561337893;s:9:\"timestamp\";i:1561424293;}s:29:\"get:/v1/installs/2757685.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":31:{s:7:\"site_id\";s:8:\"10422296\";s:9:\"plugin_id\";s:3:\"700\";s:7:\"user_id\";s:7:\"1979899\";s:3:\"url\";s:38:\"http://localhost/projetos/century_loja\";s:5:\"title\";s:14:\"Century Sports\";s:7:\"version\";s:5:\"1.4.0\";s:7:\"plan_id\";s:4:\"2902\";s:10:\"license_id\";N;s:13:\"trial_plan_id\";N;s:10:\"trial_ends\";N;s:15:\"subscription_id\";N;s:5:\"gross\";i:0;s:12:\"country_code\";s:2:\"br\";s:8:\"language\";s:5:\"pt-BR\";s:7:\"charset\";s:5:\"UTF-8\";s:16:\"platform_version\";s:5:\"5.2.1\";s:11:\"sdk_version\";s:5:\"2.2.4\";s:28:\"programming_language_version\";s:6:\"7.2.10\";s:9:\"is_active\";b:1;s:15:\"is_disconnected\";b:0;s:10:\"is_premium\";b:0;s:14:\"is_uninstalled\";b:0;s:9:\"is_locked\";b:0;s:6:\"source\";i:0;s:8:\"upgraded\";N;s:12:\"last_seen_at\";s:19:\"2019-06-24 00:58:14\";s:10:\"secret_key\";s:32:\"sk_QsGs)T$*[O^lYEJtLt1K.nh7cI0]@\";s:10:\"public_key\";s:32:\"pk_266d8c2ff927c47c8896d143b985f\";s:2:\"id\";s:7:\"2757685\";s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:7:\"updated\";N;}s:7:\"created\";i:1561337893;s:9:\"timestamp\";i:1561424293;}s:44:\"get:/v1/users/1979899/plugins/700/plans.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":1:{s:5:\"plans\";a:2:{i:0;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:3:\"700\";s:4:\"name\";s:4:\"free\";s:5:\"title\";s:7:\"Starter\";s:11:\"description\";s:28:\"Search products in live time\";s:17:\"is_free_localhost\";b:1;s:17:\"is_block_features\";b:1;s:12:\"license_type\";i:0;s:16:\"is_https_support\";b:0;s:12:\"trial_period\";N;s:23:\"is_require_subscription\";b:0;s:10:\"support_kb\";N;s:13:\"support_forum\";s:64:\"https://wordpress.org/support/plugin/ajax-search-for-woocommerce\";s:13:\"support_email\";N;s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";b:0;s:11:\"is_featured\";b:0;s:2:\"id\";s:4:\"2902\";s:7:\"updated\";s:19:\"2018-12-28 00:04:05\";s:7:\"created\";s:19:\"2018-04-07 21:36:56\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}i:1;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:3:\"700\";s:4:\"name\";s:3:\"pro\";s:5:\"title\";s:3:\"Pro\";s:11:\"description\";s:55:\"#Increase your sales by faster and modern search engine\";s:17:\"is_free_localhost\";b:1;s:17:\"is_block_features\";b:1;s:12:\"license_type\";i:0;s:16:\"is_https_support\";b:0;s:12:\"trial_period\";N;s:23:\"is_require_subscription\";b:1;s:10:\"support_kb\";N;s:13:\"support_forum\";N;s:13:\"support_email\";s:22:\"dgoraplugins@gmail.com\";s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";b:0;s:11:\"is_featured\";b:1;s:2:\"id\";s:4:\"4799\";s:7:\"updated\";s:19:\"2019-04-09 16:22:42\";s:7:\"created\";s:19:\"2018-12-27 23:18:06\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}}s:7:\"created\";i:1561982542;s:9:\"timestamp\";i:1562068942;}s:47:\"get:/v1/users/1979899/plugins/700/licenses.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":1:{s:8:\"licenses\";a:0:{}}s:7:\"created\";i:1561982542;s:9:\"timestamp\";i:1562068942;}}', 'yes'),
(617, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:2:{s:8:\"required\";b:0;s:18:\"show_opt_in_notice\";b:0;}}', 'yes'),
(620, 'widget_dgwt_wcas_ajax_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(622, 'dgwt_wcas_activation_date', '1561337872', 'yes'),
(623, 'dgwt_wcas_settings', 'a:51:{s:10:\"how_to_use\";s:0:\"\";s:17:\"suggestions_limit\";i:10;s:9:\"min_chars\";i:3;s:14:\"max_form_width\";i:600;s:18:\"show_submit_button\";s:3:\"off\";s:25:\"search_form_labels_header\";s:0:\"\";s:18:\"search_submit_text\";s:6:\"Search\";s:18:\"search_placeholder\";s:22:\"Search for products...\";s:27:\"search_see_all_results_text\";s:18:\"See all results...\";s:22:\"search_no_results_text\";s:10:\"No results\";s:23:\"product_suggestion_head\";s:0:\"\";s:18:\"show_product_image\";s:3:\"off\";s:18:\"show_product_price\";s:3:\"off\";s:17:\"show_product_desc\";s:3:\"off\";s:16:\"show_product_sku\";s:3:\"off\";s:24:\"show_matching_categories\";s:2:\"on\";s:18:\"show_matching_tags\";s:3:\"off\";s:6:\"mobile\";s:0:\"\";s:21:\"enable_mobile_overlay\";s:3:\"off\";s:9:\"preloader\";s:0:\"\";s:14:\"show_preloader\";s:2:\"on\";s:13:\"preloader_url\";s:0:\"\";s:16:\"details_box_head\";s:0:\"\";s:16:\"show_details_box\";s:3:\"off\";s:12:\"show_for_tax\";s:2:\"on\";s:15:\"orderby_for_tax\";s:2:\"on\";s:13:\"order_for_tax\";s:4:\"desc\";s:11:\"search_form\";s:0:\"\";s:14:\"bg_input_color\";s:0:\"\";s:16:\"text_input_color\";s:0:\"\";s:18:\"border_input_color\";s:0:\"\";s:15:\"bg_submit_color\";s:0:\"\";s:17:\"text_submit_color\";s:0:\"\";s:22:\"syggestions_style_head\";s:0:\"\";s:12:\"sug_bg_color\";s:0:\"\";s:15:\"sug_hover_color\";s:0:\"\";s:14:\"sug_text_color\";s:0:\"\";s:19:\"sug_highlight_color\";s:0:\"\";s:16:\"sug_border_color\";s:0:\"\";s:17:\"search_scope_head\";s:0:\"\";s:17:\"search_scope_desc\";s:0:\"\";s:25:\"search_in_product_content\";s:3:\"off\";s:25:\"search_in_product_excerpt\";s:3:\"off\";s:21:\"search_in_product_sku\";s:3:\"off\";s:28:\"search_in_product_attributes\";s:3:\"off\";s:20:\"exclude_out_of_stock\";s:3:\"off\";s:12:\"pro_features\";s:0:\"\";s:27:\"search_scope_fuzziness_head\";s:0:\"\";s:22:\"fuzziness_enabled_demo\";s:3:\"off\";s:18:\"search_engine_head\";s:0:\"\";s:19:\"search_engine_build\";s:0:\"\";}', 'yes'),
(624, 'dgwt_wcas_version', '1.4.0', 'yes'),
(625, '_site_transient_timeout_locked_1', '1876697897', 'no'),
(626, '_site_transient_locked_1', '1', 'no'),
(827, '_site_transient_timeout_php_check_03bb19de23a7f39f237dfd15fa323af5', '1562281861', 'no'),
(828, '_site_transient_php_check_03bb19de23a7f39f237dfd15fa323af5', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2300, '_transient_timeout_wc_related_107', '1562011873', 'no'),
(852, '_transient_timeout_wc_shipping_method_count_legacy', '1564270161', 'no'),
(853, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1559249916\";s:5:\"value\";i:2;}', 'no'),
(2041, '_transient_timeout_wc_tracks_blog_details', '1561998058', 'no'),
(1194, '_transient_timeout_wc_report_sales_by_date', '1561988629', 'no'),
(1195, '_transient_wc_report_sales_by_date', 'a:16:{s:32:\"2dc73c4f91bcdb64bd99461d31af3856\";a:0:{}s:32:\"af1e354afbf0df2d831567e5a2b47d74\";a:0:{}s:32:\"6d6af99c98e6098284857a9bfcd5433f\";a:0:{}s:32:\"8e535e429c58a7a9eb4083a64f0bedec\";N;s:32:\"627276d7e19f188b37f7adf20f19c5b9\";a:0:{}s:32:\"322f161064e8d8f677f1775aba910635\";a:0:{}s:32:\"3e543d3e717ecdea37d776b65e9e9d7f\";a:0:{}s:32:\"0e42f754867dae60a9701669e054fdeb\";a:0:{}s:32:\"909604197edd09270f91e5bfc5dbf6bb\";a:0:{}s:32:\"142e38897dd76621d5d003ae407659fb\";a:0:{}s:32:\"81b61faa28c0a9bba976724a355b0d32\";a:0:{}s:32:\"37ba8ad2631b09632e7fabb9fec5dbab\";N;s:32:\"71d5a998e2b3e5cf9159816919f1d4f4\";a:0:{}s:32:\"6cb9a7b2249e51d9799962977a9dae16\";a:0:{}s:32:\"16c1286ce119b71f93258845b72b7e2f\";a:0:{}s:32:\"878f784a7ce437fa2ab6d351b13419a5\";a:0:{}}', 'no'),
(1950, 'product_cat_children', 'a:2:{i:28;a:8:{i:0;i:19;i:1;i:20;i:2;i:21;i:3;i:22;i:4;i:23;i:5;i:24;i:6;i:35;i:7;i:36;}i:27;a:6:{i:0;i:29;i:1;i:30;i:2;i:31;i:3;i:32;i:4;i:33;i:5;i:34;}}', 'yes'),
(1730, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(2204, '_transient_timeout_wc_related_56', '1562006066', 'no'),
(2205, '_transient_wc_related_56', 'a:1:{s:50:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=56\";a:15:{i:0;s:2:\"12\";i:1;s:2:\"52\";i:2;s:2:\"54\";i:3;s:2:\"58\";i:4;s:2:\"65\";i:5;s:2:\"87\";i:6;s:2:\"89\";i:7;s:2:\"91\";i:8;s:2:\"97\";i:9;s:3:\"104\";i:10;s:3:\"105\";i:11;s:3:\"106\";i:12;s:3:\"107\";i:13;s:3:\"108\";i:14;s:3:\"109\";}}', 'no'),
(2227, '_transient_timeout_wc_shipping_method_count', '1564513017', 'no'),
(1619, '_transient_timeout_plugin_slugs', '1561994392', 'no'),
(1620, '_transient_plugin_slugs', 'a:9:{i:0;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:1;s:41:\"base-centurySports/base-centurySports.php\";i:2;s:39:\"categories-images/categories-images.php\";i:3;s:21:\"meta-box/meta-box.php\";i:4;s:35:\"redux-framework/redux-framework.php\";i:5;s:27:\"woocommerce/woocommerce.php\";i:6;s:39:\"woocommerce-admin/woocommerce-admin.php\";i:7;s:37:\"woocommerce-products-filter/index.php\";i:8;s:41:\"yith-woocommerce-ajax-navigation/init.php\";}', 'no'),
(2382, '_transient_timeout_wc_product_children_109', '1564574565', 'no'),
(2383, '_transient_wc_product_children_109', 'a:2:{s:3:\"all\";a:2:{i:0;i:115;i:1;i:116;}s:7:\"visible\";a:2:{i:0;i:115;i:1;i:116;}}', 'no'),
(2384, '_transient_timeout_wc_var_prices_109', '1564574565', 'no'),
(2385, '_transient_wc_var_prices_109', '{\"version\":\"1561919792\",\"f9e544f77b7eac7add281ef28ca5559f\":{\"price\":{\"115\":\"200.00\",\"116\":\"123.00\"},\"regular_price\":{\"115\":\"200.00\",\"116\":\"123.00\"},\"sale_price\":{\"115\":\"200.00\",\"116\":\"123.00\"}}}', 'no'),
(2386, '_transient_timeout_wc_related_109', '1562068965', 'no'),
(2387, '_transient_wc_related_109', 'a:1:{s:51:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=109\";a:11:{i:0;s:2:\"12\";i:1;s:2:\"52\";i:2;s:2:\"54\";i:3;s:2:\"56\";i:4;s:2:\"58\";i:5;s:2:\"65\";i:6;s:2:\"91\";i:7;s:2:\"97\";i:8;s:3:\"104\";i:9;s:3:\"106\";i:10;s:3:\"107\";}}', 'no'),
(2298, '_transient_timeout_wc_related_65', '1562011866', 'no'),
(2299, '_transient_wc_related_65', 'a:1:{s:50:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=65\";a:15:{i:0;s:2:\"12\";i:1;s:2:\"52\";i:2;s:2:\"54\";i:3;s:2:\"56\";i:4;s:2:\"58\";i:5;s:2:\"87\";i:6;s:2:\"89\";i:7;s:2:\"91\";i:8;s:2:\"97\";i:9;s:3:\"104\";i:10;s:3:\"105\";i:11;s:3:\"106\";i:12;s:3:\"107\";i:13;s:3:\"108\";i:14;s:3:\"109\";}}', 'no'),
(1636, 'yit_recently_activated', 'a:1:{i:0;s:41:\"yith-woocommerce-ajax-navigation/init.php\";}', 'yes'),
(1637, 'widget_yith-woo-ajax-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1638, 'widget_yith-woo-ajax-reset-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1639, 'yit_wcan_options', 'a:6:{s:29:\"yith_wcan_ajax_shop_container\";s:9:\".products\";s:30:\"yith_wcan_ajax_shop_pagination\";s:26:\"nav.woocommerce-pagination\";s:36:\"yith_wcan_ajax_shop_result_container\";s:25:\".woocommerce-result-count\";s:31:\"yith_wcan_ajax_scroll_top_class\";s:19:\".yit-wcan-container\";s:31:\"yith_wcan_ajax_shop_terms_order\";s:12:\"alphabetical\";s:22:\"yith_wcan_custom_style\";s:0:\"\";}', 'yes'),
(1640, '_site_transient_timeout_yith_promo_message', '3137838356', 'no'),
(1641, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithhalloween2019</promo_id>\n        <banner>halloween.jpg</banner>\n        <title><![CDATA[<strong>YITH Halloween</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid only on <strong>31st October</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#005c7d</image_bg_color>\n            <border_color>#ea5105</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-10-30 16:00:00</start_date>\n        <end_date>2019-11-01 08:00:00</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithcybermonday2019</promo_id>\n        <banner>cyber.jpg</banner>\n        <title><![CDATA[<strong>YITH Cyber Monday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>30th November</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#12fdd4</image_bg_color>\n            <border_color>#181d7b</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-12-01 00:00:00</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <banner>black.jpg</banner>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>1st December</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-11-30 23:59:59</end_date>\n    </promo>\n</promotions>', 'no'),
(2381, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1561982550;s:7:\"checked\";a:9:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:5:\"1.4.0\";s:41:\"base-centurySports/base-centurySports.php\";s:3:\"0.1\";s:39:\"categories-images/categories-images.php\";s:5:\"2.5.4\";s:21:\"meta-box/meta-box.php\";s:6:\"4.18.2\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.6.4\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:6:\"0.14.0\";s:37:\"woocommerce-products-filter/index.php\";s:7:\"1.2.2.1\";s:41:\"yith-woocommerce-ajax-navigation/init.php\";s:5:\"3.6.6\";}s:8:\"response\";a:1:{s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.18.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.18.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:2:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.18.2\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.18.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.6.4\";s:7:\"updated\";s:19:\"2019-06-17 18:34:40\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.6.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:7:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/ajax-search-for-woocommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:6:\"plugin\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:11:\"new_version\";s:5:\"1.4.0\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/ajax-search-for-woocommerce/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/ajax-search-for-woocommerce.1.4.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-256x256.png?rev=2042590\";s:2:\"1x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-128x128.png?rev=2042590\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/ajax-search-for-woocommerce/assets/banner-772x250.png?rev=2042591\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"categories-images/categories-images.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/categories-images\";s:4:\"slug\";s:17:\"categories-images\";s:6:\"plugin\";s:39:\"categories-images/categories-images.php\";s:11:\"new_version\";s:5:\"2.5.4\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/categories-images/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/categories-images.2.5.4.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:68:\"https://s.w.org/plugins/geopattern-icon/categories-images_7a8aa3.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/categories-images/assets/banner-772x250.png?rev=1803373\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.6.4\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.6.4.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"woocommerce-admin/woocommerce-admin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/woocommerce-admin\";s:4:\"slug\";s:17:\"woocommerce-admin\";s:6:\"plugin\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:11:\"new_version\";s:6:\"0.14.0\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/woocommerce-admin/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce-admin.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-256x256.jpg?rev=2057866\";s:2:\"1x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-128x128.jpg?rev=2057866\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-admin/assets/banner-1544x500.jpg?rev=2057866\";s:2:\"1x\";s:72:\"https://ps.w.org/woocommerce-admin/assets/banner-772x250.jpg?rev=2057866\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"woocommerce-products-filter/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/woocommerce-products-filter\";s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:6:\"plugin\";s:37:\"woocommerce-products-filter/index.php\";s:11:\"new_version\";s:7:\"1.2.2.1\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/woocommerce-products-filter/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/woocommerce-products-filter.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/woocommerce-products-filter/assets/icon-256x256.png?rev=1208073\";s:2:\"1x\";s:80:\"https://ps.w.org/woocommerce-products-filter/assets/icon-128x128.png?rev=1208072\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/woocommerce-products-filter/assets/banner-772x250.png?rev=2071519\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"yith-woocommerce-ajax-navigation/init.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:46:\"w.org/plugins/yith-woocommerce-ajax-navigation\";s:4:\"slug\";s:32:\"yith-woocommerce-ajax-navigation\";s:6:\"plugin\";s:41:\"yith-woocommerce-ajax-navigation/init.php\";s:11:\"new_version\";s:5:\"3.6.6\";s:3:\"url\";s:63:\"https://wordpress.org/plugins/yith-woocommerce-ajax-navigation/\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/plugin/yith-woocommerce-ajax-navigation.3.6.6.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:85:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/icon-128x128.jpg?rev=1460901\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:88:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-1544x500.jpg?rev=1460901\";s:2:\"1x\";s:87:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-772x250.jpg?rev=1460901\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(1763, 'installer_repositories_with_theme', 'a:1:{i:0;s:7:\"toolset\";}', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1663, 'wp_installer_settings', 'eJzs/etyG1mWJoj+7jKrd8CgpzOoToJ3ihLjUkNRUgSzdGGKVCjD2to4TsBJegqAI90BUcy2MqvH6DabMcsXOXbM8lHqBc4rnHXde+2LA1BkZFbOOdUz3akggO3b92Vdv/Wt4nj/+H+0x7t7x/2mnNVtNa+bqmz7XxfHe/jBwXH/fjYZ43/vyn+PinlB/02/hB9Wo/7X7fH+E/7qYH5XDu7rZjRryrYdTBbjeTWupreLYjyYjRe31RS/DeNMi0lJ/9w57n84f/2qN+hd3pW9D/DTc/xp77X5ae/c/RTnOmvq0WI4H7gxeAiax3F/0Yzpi4+P+3fz+aw93t7GmW3VzS3+fQ++DS9aXn0sH9qrSTEtbstJOZ1fye/2d9PfbRfDYb2Yzrfxh+22zqNdzGZ1M7+aF7etrFF1vGMWb+kE959mHnRTN4tJuz2vZ9VwAOPSJ/DAf/mX9hiWeFYMP8J86Wn7+LS9g/3Hjx/vw38eBfvhdq5rHrvwmqOyHTbVbF7VvLQ78HPchGE9mY3LedmTx232but61IO59YrpQ89uag9XZLNXTYfjxQj+1CvGbR1+40Ndn9aTSdkMy56s2BY+Dl6/msDouvCPcwt/PxsM6+kc9md7MRvXxajd3tvZPdjeeULfGYzr23p3sLO7NZvS7h4e9+EElU3/a9wJGBTXrIGf6x+euOOjiwgvDlMJTirMajp8oC2NtxK/GxzNE/ludknhcMusZk01LHESu4dP8btHeHyu3Zev5g8z/vhod/cJHtO9zBeu5uXn+apZxCNX05taf/NTWTTjh579nO7Ek9zDyj8sqk/FmBaP1uDx0d5ueIYPc3v2T8VoNJjXg2HRzL/lA7r7q2Iy+/p68XA1re+/pUGeoMyZlvdwYHChd47/Bx/xxey2KUal+SO8EIsOvmRHfMtg1o/5JmdEzXDS0mLit/ZFMrXzBj4dzJti2o4LfnW4QPiVIxVe/rOBlwz4tX3ZEvrapBxVRTzQgciW+7oeynEP5oTfOcTvwGGAN/xUzR/gQk3a5EuPZUZ47guQcvitwVHytSN5t+vFaPSQLgF+5QkfvWJ4M6H/poN3KC9RjMeDaoK3kVaKFnRXX7CoxsM7+BQfPrif0Td25QzBlWzLOnnY7h4/DY8OL/4+7zJ8/1NV3vPfDmQd53U9bsv5YAJXcVzKYjd8P+QQy6ZVw48PA3jORx7gsche+hS2GZTAJxCPJE5G9f2UZISeHfjz7pPobuNv9vJX+3F0qU5fX6x/r4+WXuvHR7tPV9/qrgn8la60vMWXX+ed/7jO/3Gd/32uc6yqr8EC6LrPR9F1egbfXf9C76260Guo6c4Z/J3d6MOnv+CN3v3CG730Hu6tcQ/3V9+dg+juHHbfncerrs7R8pvzJLo4TzP3ZmfFvem6AP/yL/Aqh7s7cPZiix+VR7rEP1fB/f+jL0ArLOv7OF3fo4zkiRf4CyTOoSzwddFWwx65wJ/KpoVPzRIHi4cPbP/OlitzUClSkCr51eJAv2cWFO8hLU3u2KIjD8/oryFp8mcc7wOf8QbON023N18009aEIexYW72zeQ9kZ9ubu40z4qqHy93AhYabMYLDP67vW/853AfZDt3C4V0xvS3lhOwdPTmERfvm7uC7g629raOt3W+24d/f3O1/97IsYFZlC3/Y/+4f/+GbxRj+3x78n2/G1XdnE9iTT+WoNypn5XSEXmBveFcOP/auy/l9WU57o+rmpsR91PPV9uobeV0YE8YIRhuTkQQDDhftvJ70xtV1UzQPdBxP6/G4HM5pDPzvctouGn6zyQze8RpWav7Qu6/md70a1qiJHvPNNs39m+sG/198tepzx3vhJ7CIvZtiDjteNg08DranGE3g/72/g/e6uOxVbW8xGxU42+vyBnfxFP6f6J14pP776bBY3N7Ney9wrOPe6biA7f0KT9fVW1aor0X8wvY2cKLxeW09gfNRtGXLD6XHkWxbwOLAP4ewzw/ZJxZTmbaMRr9vwVLEX/e+X8BRuC6b2+AE/b6+5tUremzf0pkpqin/CL7W3m1lnwbfa+txKY+EV21xUR7gR+PyZg4npOndNLCdeHJf4Sv0xgUe6tt4uS7Kee+fF82oau/cV3rjGt6z7M3rXn/48bqfnQGOLJO2r9SUs3ExpENFZwak2PBjy2+p3yuuYfBqOoVJyh2hK8R/4R/Eb/2e9p0eega2U++6/oz7hdv5Vau6pQf3Ha5A53THVTvn8WF3quFd775oe9N63oM7NSvRCAMJiAuJggG+b98LjJV53XRtRnIdrq9ZnuDosIwVvGWD11K2ezG/g7WZYWwvGvFkNMLbLRK29/7dqxan06Dh19Cy0qBgtPKdvB0vyo7bzUPp2sAs9H1KWYRiDvt3DScTRNgQpCJpcfgeieDfwf+F3bmpbhcNLUH87jw8yNhihr9bTKthPYLbc1c04PiA7MF5+3MfPzI72k01pl8Gk5VTBqPJYcJB2h6GplN5xtIRdw/E7QglOYo/Eusg63r1cLhoQDri8sHf6RCD8oEHl2Pad7jGL1gmwuJO4YWyO96UeCw+oZkE2wmXZbiQcwL//4dqCvsXv+Bl8bme1hMw9GGz+BjgqQAB1AODflHqJQbXAO4/vHYDR64B8YtDwl2GSzJLry9Pp2pbGIAkzgs6p9u9i7t61hMJB0NP8SCOwDAACQZjF71Z3c7zciyQwU6YVShsYGyWTGIY9DYyR//iA9p9ar0/srpA1d1jUXYHVtkdeKXwn/yJwB1C++UKJ3yFM7ialPPiCgTAFfhudYPP5WODp4aVLP1KZAP+jmbew9+R4NDfbUV6CucjGiqejFnnlncf51b32DvCRZE7KHqjxT+B+wZ+xBiMLNjVFhaVz0i71XtT32+S/cwXHOwuMCDuiwdQcHAG3E0z6wDbUn3GJ5ISLp0MwJ+CloFv1FMQYPR0ERajrd7lHejMu3I8oysFf1qAJQ7ealP07htMbtBNoyW+qmes6Uk8h1PQk9GWcHtwweXA4XY7pdHCfw7RCCB3fyu374crjBz7vlOcJ5o4I9i4tsUnFNf1Yu518vkP52rjJDuZtTX+U6CtzTuQckjeA+7dXC9t0YBbSPcdRHZucerRiCQSmIl4lcdjWtpgTfm8kB5ju0b2MBkOVhrfTV8bvknWC5ifl0YjvQg1kvlxeCnhrsLDJ85A/KkuWla4b+reKczmtgZ77xnOe4NF/6Nw0HclmBqf/PFHQaXrr8eQzp01fgtaqh4oZLoqtMxOsKaTPtg5EImDSg1uJ9wlEEJDnl7lngAb1Y+n3ZdlJnVeTnGpR9njd6BWNh0HeTc4QaBS70HA3uJSDeGOwJUa1VP4f9jaxYeCpwG2gYj4G5QAJTw5/5T8M0i7fXaLlx432GZYnQUotqotZrOyoIeTFUdmaShCck/ej5/sTR8j08EWfvvq1cnl2ds3aFGjYgAFVLEHevrDybuT08sX73oXLy6zD9lb5w47RazWB479jCwANqRAMG/0nXHQf5Q1/ta/zb15NSlRONRTJ/t7g1/dzr/ugYGLB7Ll8+hOEQnfOzgwRe9uAR9MF5NrvPM3rCbIHshe9EIu87CWvbDal55btbpXJS8qbUDVwK3DaWYHTcTRYopLIeZL+9DO4QbzpFa9Yzh+YBKB84k+bN27Basf/xDaMfAkfHM0k5wV00PLmK80zyK/KMn8raR67YLx8Pkfi2ZEh669q+9xWFq4S46Mga/yANvYqnuONxqMSZhh7qn4HLKh5GLSw59X7R8W0SQvWH7RRQY7vG4mBViAMuXAeSqa4CxwAJtsiDYWihO3rKGD5R0v70+B47F62ch2EScQLS11u9UKz4oNsrLRY8V1bNjyGZU3BRi8/stwItkwystFE3tY4aA7A6gYOwNItDEdR5ZtOAeJ+1UgdeC+5P2mX/zUVMlpkef9gL6AWhXDktWFeHmw+404aOC48sDFiGw6WPwG95kVmi5nPPg7WO4hyDQ9kcPxlQtOXbV6NdnjbFiXtn4arfgk4uKQqKcpwS8Xs8QRdsq4QOUIu4qyKI6a0JEBU3+uV+JThVODkVHt+2jXxYu3ctNWbJBqrVE5aB+mQ3AKprAl/tKdjD7hjRotMVCCE7ezviGIS/o96Cc4StN0C1TGuTCWlQcqe6flPUaKQMG0c9TrkdFon/asnN7Cw1y0OvjK+xbvW0HW9z2aBJ/4mXTh3e3FvQf15i7fYjoSYxKm4iQ1C+7vS5gUbN2MQu2BCgkPsqjCX02v29nX66lENM/ui2bqbsu8eRAHfwT3lzeuf1WqUXY1bNs+65jwtZ/XdPPALJmNC7D9pzw1/4YaxMGsljWaWOfROZYrnBOCI7wpQ7ISFy275eDxg8jdQKugN7ub9Y7QtMlZpMF9ZgFIzqJcsxFGzK9muN0wEbESm5KDvvjbyQwMZIkvwG8wSsDXEBbvH/+BLlRW3z13cz52kz0Ge3oqeWo4ciUIHIx7jcdgebN8gd/gW4JO42+1KoJuFngHMqb8NAnwVVM21j6BeShxNFAV8Ce9jrjTN+PiFj1LPEfXJb8cPBHOa3YHQn/BiGQfCaFhMSLFojD8xZBiq/gLNMH8j0SJ0wz+8R+69Xg8npkBHuLDrf2tz9avhnW8vZqg9LsCI6vLMrgpKJDfe/fi4pJc4rKdixvhJWBgq5HoBKed4y7oXqC4zGtU/Kr7XjgFvwDnmFzuvcLDZ5IRqovxJp6AKqUZlMUIT/6PVYuZh87x6t6g9wKzlO7SOWvjJZg1sGgjtO1QfnyCb7F2QDFo5DIIr+XGiNGABQlPGRXmG4THLuH+YCQkv6fXi1sbKZH8gtiwRi3ieaX4DXrRHBTNRkCy1h5epBmhUMVVfBiQB85OLt/4cLD/5uGqbVn/995rMON6X5GF9xX7CDBZChqhIJ8UM07vjFyGhD7DdK8LCpjFXcM6nsI5hD+ePj+5PMFz9LtXZy9fktnd4W90Xo6TT8WoiBINkkAowy03dtn6kQR03+YPdIfE7pOIO/gGZI/hwXp3+crdimj+7yVXg489X0x/X1xXxkgGERVb1BLvxGMmIXkfgEFpxgdz6sMD8yCmWnH0TK5xiVY0ZrLyNsMrjK6TsUVvhApj0VBUGMNooq01kBaGfYwls5t6+7ktT2K6TlLjU/DVZNs41UgSC/8Kx/Sm+qzPpeFPRXaA9KmHC7yIvIcYdJMAmQ3D+Pg7vQK8MTyhAAGBl6OctrxYeMevG/D2aMWvgweqo1P0fv/bRQnraTKPTvboj13MmoVdb+MGjZSeS8aBqVxS9F2ycjKkhCFAysLJHT1afYfwDN9KlkwvvipTDuW4g8aKbIpx2OC9rJn0hqJYGuyWzfeBY04rCQjC6jdzqjqn6oLuI5PIeFYWmKJ7tqgoOhzlBFeOSQb97bi+RhSGZmjHdRsl40AJ3GBcnKQaRx6DoS/m9WzGyUSMUHkfP4xP4oW8NrGbOG7ZOU23UmAOg85CfUV+9wTzRJzU5RWxwkjOQmgtRKrdvj9jaZxFxW4WeImLMWaJ1WrV+LickeC3X7VLNwC0D77XD5cI2UDbqzDCstuZL6bgIw3hIfbOW1UsYmUU5ED7eG77aCJiuHEC9mLHo9guDFT1FBNjxfBuwrY4idG7CvwLVFM4EzzRHSuqGr6ak9XK2dGS8wx2xqQBnhUf8db2+UcVyty2GpXXRdPPysj9ZfGFzhW8mBcowEfgXOJhfCOHESfENq0cW9paCS6yJjOhvqlkM8MxnFnsTsdqkXPy7nVJYSGxasmTRuN0R7JjxRCGdnr2fiaXaFZMy6yNWkRJfI1ekc1m5Hbon3eegpPgyiKS5yuOllCgI1geY0pbk0AXr/OBxjx09oYRb/Z4rWnBRA6GbIdOrH9aT/DfF3U97ffmkbUZHLG97BHrQplcF8OPcKiHZZTltB7HGsnR6BXwRESCnd7Kp00tHk5TqOTIhaZjgiuJXFw60RgalRAEidBq+vD69AWrZHXUYOMvy89zsNfwouYe4LJDPPL5s1B/SOL4unTAGz42U44aw/lfhosI1iqMQKCwAbsDHObyM7hHKLHMg3MTZbUumV4EjOEwml4Qea5HpLUh1GqaSTzl0ue4m7iiVb1ot6e4biBHqqm3r/JjBedwWSg1uBK/ubALYzLraHX4uwkrPJTrWXaF03a/JJxWyCUMEBZO2qCiRlhujMIJEtGalxSx0xn5Yx0uoThwRrPDZQAEeJQxfkxOWu9sQoZrTiq90ASxv4PWQUQFOMSbI3peUEgOC9KAIVvgacole/Rl0TS+4UVly1mAfos2walUPFOSwtNBAHYS4AplwQIJ0ZXqZzwDPO/Pf5JLDkuwuMYw6B9JcTkoh6IM8wPJzPHBbUlgPooGko20uB5gUSSGHSLn3FlPXvOvHt/hbUj7XosEZLeDnziqUSeJqxOiwSQKSatHsFTavU91BaYF7LFeBxR2jZjm8uKb8Cj8jxmJlmqu0ZS8JppUbZh+HdbjxYRU5MsFBoV1YzCCy1DI3JvbtKYPJsFEZzWcwOgondojfgzOEBvTr9+y0+/C1P1blw/tYUkBrBdazbkX8RrBJw9yKXgKVeCpVW2Bsg3M4DvJhwj40O7YFyRfXRj2N8WnQjw+jpnywByEwSSDWBz6RLdkX3VIt24JoYv120U953TG+WI8/gP9l9yyAHaWHRevMBq+RmTBJQBH9/eVi8nqxJaAADttRcyRkL/ns4e8BObYI64F/SA8veLwLBBc2nnn9Ag7lEmgWBPkxR1baH4lJGqPS9aRZtPIfv65fqSsOecAlZor8orCRAEjc5EkqFgYy21xQoh4uz4X4OGPKXWClrgeNsr2U3CHw0Qa0+1rQsPsiLpiiEOBzUA7URFScQi4z9uXphwjz8rFqECiT1rGoGZO6BeErrqSp88kCPPOOpwioP32jGpGa05LXCR8inVQUXlPb8eIBmbJLW+vf8zP6F2JBhm/7zfl5LutSf3NNvwvgwnwyr7/2CCs2aTwwGXEWMA9gqF+v6CQaDVvDfQYB1p8pHG61lbFsCA57Qn9pGgSeXWyGOl4honwJUvqjwVvnfc8OfzE+xXYy4i0p2Md4J7cvDqgGYtWrTI8m5T8LT/PyqbCSBmOKVBGLB8C0yUbJvbxBrZUYSMDzJqYAPHJWiNe7Y0rHjkKDjgvfL1D6rYfYTYDRGP6ZW3KG1AKd7hseMdpDY1EW7aWGqUMJBtZsi7T0OqV0KAluVEuIStBPo0x21AKpSt6ghXvEs8mMe98KjlGnIciNH/PF6bmxVyKT+HowvVi/HEwAjOJV8TNrl266pFTzbY9pjVaMWYRPZq+WycCNNhOHk0kjCi4vp9ZX0N5q0aFG9NWaAT+gYLBakGwmGfYAjoTGnihJLEDbrP5WIwrKtoo+JN6Wkou4PqBtnlJNIOCdWI9etyh6AGahBcrVYjPNT7YztaTLh+sF0ABNZg+lZBzrV5SmZQc3JTliDQQXV1C6y5mjeKN6RjTEWaEMezntLe/9QSdQR/hszkYygrjWCcNyJthXBEiBh//f6s9WHPGBHaE+zFgz9zqPFdl4UOxKDbxE/O2UVgSxz/5zcnvCEprEWsM359LcI1y3IJ/clgelvBgyM0zgzqQzDOsHmW7nY/abDZ2kSeUN1eUUvHWPTtlcfzULgTKRY8K8XKTgMBs/XXr9kAKJJETH1y0Eu6lnhCXS+qy7BhVapGxS6X0CMGNeOhuHUwq8OoysRp7s4YSd+n/UN/j2+uswGgKwOGdMuuWpbIoU/DoqhEpwuBUoLIEG6y68QFDPFV9NN0XbbKwiOBi44LqVEKrhODQ5QyhP69i1RgZHepCis9Mhb+6SA4bIwKkawxdmHm4MIH363JpdKjRdg4SfupSNfD3skXHmiITLBIwNHaHwj0y1aekW0I1SIvCeFja/7fTy7vy+/qCMkFYXFU2CJhauWcmi8TJWrwOLq+JzihYagytXHv/qY4HnVg32x8uL8+vfnh7cakxP9xHvpr6A6J7WjTjjrXPw+b4BsHxuapGfF20jJIfjZ+oWIOvbLIboXKO75dIt6LNGAkWSdUsfIYRXOAgUc/1AQzUDUsQXU7SrQVVBQ1KmMhGuXW7JQCyGbg6d0UbFurwbvDooj3nYBw/Si6JURS965rtF46kFnzj2moCLtxNhRCGuwoUbS4gv7N11KUOlwBLRdS53K5B2/hzK+gMb2Jj8H5xeycIclohOHB4EpO6kFSvZWaTc28loPHhvHexQFjCKdg4GRyoWIBJjkBxqj8iI4ADfbndu0O4P+P0+Clwg3ENH6+CzXIcrwZTupqULh3QxoENwonATZYbUwUobjf5cfmpgOPO1cRihrLTznE4tljWjPTDSkmWUMTkbYP3CnPLmuCVqgOQCHB1h72z553reboUABO9WHprJnxjsMZWMooBTH51teRfll3IaXM2f+4KY8gyhMXWbzsfjWUN/uLseSibyOgLra10HfnNVG39YVENPxIlC4lOdpLsgXEOUxVBVnOrFp1Ik5CKrVkEEWipGoVNz32B+jdw3I/gwHegtp2DgEWFWGcq0Th2FLSuia4NrtTJ5Yt4pDe2Wg62uOH40PUDVfL4c9+Rg7vGEkOOQntPtPxcINBFQ0qybBiBLnBSIIYIopgf0fmfjI4Vb8BEaZw2uSHAxXRABm/VwpLODSg1kLmPvyANRIkNQcSS4UBRIU1qMBKnV930muKeoQ8+KdBqbXcuY8eRDSRGwFpDCQnBrOvp7XcEzR23V3qRrrDqBOYoH2va466uPyaBYMJ9zVxeh0ydSMy5qVLW3Vur6+adKM7ljHY4nxSEF2QEp5ByI+F0pVCDRB0naAgVVWIQtm40PqUoKdLuuaFC/aIOr002NOUtpkuFK6D+WLnkyrBApK0rFmHJjX/AobLpGzsuQwlF0Gky5gtSAZ1OYbRHNoxMqppIYwqaTxjfgZHQqEVBxe9Cjr066CnWwGkLnxsJEN2HOztWBDu4PttCcF1anl/AWYNF1GlNlQnuBrDauY211LGW5/JsAqIFx/Or1gWuTchx3bKKuERzIsDJMB4YTDTMzaw6GVZ/21FgSRmsTOq5buNcppsSIznEjcdiCAL59W4WU47Hwe26QsFzdf2w8aifGk/I8QHSan5fNx8VrN2FDw4kDc/8ipOaV5iTkRrnKwEzOMnz5z8VHp/lTIcoKQq/J0AmAxgjoYK8MpIBE1qYcU4Joi9FiojSACxS8ZRfl4TZ0MgnxeY4U8DGUoN57ia+xn+/7oSqAaSGIrnm6Y4E8DD9w6JclFEqFuWZLGNcwafwPjwRZJk2wWYzm5hXJSZ/6qAr8/rPf9LfuAIt95OV0oRv5Iwks+aQdLituzlHaAJlVkaixr2pIkzBXis4qN4lZEK3uF1cT6q5xMRxFXjzTplrkCoA2t6Rpv5o1X26hcO8nVULnTU1ChXz5uiMgg4T9uthJeJAvRiOaaxrW3eEBuwoB4/KjfJELHgSJ7O5BscYZ+JnoHEqgu6732CubgxzHT0IGKIr12hlUFpt+8ovRPeuMckCx/wrrsjB8C2RuggnBEfneUVwSf0Ca1JxWZbB7pIxu9mE44o0vy0mEy23mlzoIPwfQlY6shM2s6EoM28zif1wH8HN4vzINXs67HpoRpivGNUVBHobXxTdgLbl0B/V6LfR4Oe8AFFuvHWBKY0auzKgefPAvDxSPMWy0JTtddRigAn3EVMC78/0YMieer+gsAnpCMPbDd+0hp1jg2K/YiqVODFzFDoNN7J0Kx4QRqaZU4CFRHBddP6ItEMJmjV3QQXiC+LecP2cIWmQlGOb48m4uAOTXXJGTGpAOXm/WO5weqSSSy+NGF3l4B+drkckNlGSjMUzs5YdXxNC9yOvIat6FGvlHIurONRModi1pCSum5x6k8+02POcMchiUHWInoArHqhDMWWko5cbHsyUINBc1VK0K68q0Ci8pJTkZsSwzLfvSItslruvuVIXGgILhMJXxMCADAqTRL1E+0IWjj+RCxFZrupMvK5PcFg+nA9OX51lHV7hk5GF8k6v/AHDezb7lhq3tkLBh3fw2i0TZS5xjygDNGw7nMkbWCUaDL4esPGFnibelUEefUUBSn09dr063i3JaWaDpwJfoHXAWJOKyzh3C3tMCf4vyA1bxAA7OQoEsvcVLSqF+gn0h7lO9MiZL/rbkpEmMSmNx8GNlKMwlfpiAWsKFSaAp+igY+TTt+8CkO7aZg7teYe5xIuaPc0H64RvnlGwe95jaBI8PtWizKUnpjeHo0ARzRVIQg7VF7v2ZnsZiW1RQ7ASsKbT0QCk/3jUlNNunqQo0WhJbVT9G9l4hmXT4Pv1XnwGNdvAQu7uLqtM0PAZiSXZYgfkkv1yMQTWKGxBerSHL+8U3AeHUVakd0Q4i1RW1BU/QW1NDCMqioxYjsajPLOInD8LBbmu5/NxOS011yvaOwAf3USypM0dsnwtTpDgt9QmAjliCIJQb3Aa3vvGycLaKumwko7uzWjBNbYx0iWXYoyOyy0WasNNg6k0c0rjWk6QdHddENsWXTk74+y5K5TRqISkmule6zNWZ+iJSeF+dnVLdA6YfVxUo4M+0eg4/MCBz+fQSK9pken6fuUCvoOiHVgRKEGSQtK3HRdYNzdfBZNe5sC/CQDESFXFLJiYQfVBGiVNgMvUlpu6heC9SpaIqlULCqoj2mHR0F9OBRvWdCI6ZQ5NRqhopsRYvWnCRAE/uuvqmAe3uN9xefsdcj+XID6GnZrf1RzLuKJiq6vX5XRxdQZ2w/Hx1dW85hjFxqPeBC6HrJi35YnkMV4H9iiC0lmX2fCgDubRcNFjvV2Y96fzT3kGsmN90X8MLwnOyVLCn+547iVBHYaFRKw8cQKaaVTlLRyI9prHZDRn8qvkTPg0T+Blat7q9616DJNilhVta1XBRC+WAbe0IcuA0ykWY52Y14ZK1V2jIrCZEJTGoAxzht3PMscDpQZCL+gmMTSuaG4Xrkwic3C+uOxgrZKDU0m0F13ooqYeJyc7Cysi6YXAzy7cJ9crS+FC1QWjDWcnBfE+uEYAGnInQIJXf8TgGMWeCZ0yFMiIm86p5bhVZgMVI/iNbR/vAQdim7WjMe8wTgtvEXsIjpGoj+jCAVijZZ+efEl+Fq5CZs0wS0GHMSV2ptlKikwKYPn2MUD0wzku2M140d7xmhLBJ7hm4zLEiHLVlAgWl3ATklWNSGooYB8XQ04druFdgSUEoBfGY+ayWmhmR5gMvtiwhHm/JtrDCwywIwMiwwRiF1oYv7n2kXcJD4j0Kxgl0gTfJ3KyXJxDasZ9gZhCBjkWXlzLN3J5QzZ8yilYBjDB7VHJ/6CtjRIiiEwQg4VscrIGhwVY8GASLreNT78MkboSSnxXzUPDMR9VdQEg8haCNcsVyVlLsX8ZZPr6vVFTz0aOCd04T5h5tvTDZtJaFamYTA0HsDoPs09FRG6RGS4zVmAxwwGICDIygyymxiATwClnz6igCl6GsahtRgb7AmgKzIAFf4t0IDFWguNhBvPmaVLio6stAZcfn2sY4CNWy1FUzk0YS7EumNXGAawtSaC4+3cVHPXci7CR5gg3omII5m3JwUTF6l7iuFEEVPQYladELAmS+WmrSUXAVDA0ckvQWWjuzfD9radbn9HiZIHS+ZapKeoguARvFLarNNabERoqLTxwyGh+pLmkdkikiLY+g9ii3JHFgrrfUYUYDUdiGO9k5nlGORXX5ZiAxxyyU0Ji5cLOnlZeAEITkdem0FQ8mXzWExrKZU6558OG/wepOuC4DMiaJ1E0QcPVzbgj0U7JULpsIJc45GGLGyK3qXI8wAxuWFotYfFkvy8+C3kzZ13RhGdtihkJcbFdJLUrPFDdmJO//IlCVsM5QKwdlaCFxotsjghuLTHtBzxHmhjHhGbmUd5Dc6RCmZRewDfSBXXA4+Bqi/xR0P4VaRTM+S9fAJO1p9gELkEBCkTKMyOnMI6EpY0tI3IQHXSov2hD1Qe75d2mqPAvLfrzR2uNvIADVjmDX/eVT41uI5Zic4GH2RzqgcN/lYjeNvWP2m4X06Zqy63Z3SyzV8RpFd1SiZFKZNTjzjrr6gq0qnx+Ol+50EWUFI76/QKlrivOSE+HgPIr48tK6g+ZVJfm4wJV6Yi03cjkNF19KsAEwMjBpOYa/im5Yu4aZy2aBRWuillrGCqRHcQFRUqQKuDuM0dvkgHArx5hSCa/RRZXLNaYrRMT7Swkjq7IbkVENSxZZDUpapokDSgP7GQzAAUjhIe5DI8FOZKqGbF7ukJ6XthyZ+/doegWaeW7+XjEPn8dPJz+CfgUztfS2Izz8FFvH3wBy0siZHEpWaLzbFwhSXDrsL7ggi5cipCKcclLrA2fadzf2t86sE/w7iScyecCxqjklgfMlNgqbjFztJSDBLkRLM7+uoh765wGjbbeznwgmeruokZOq+HzdmjXLOKGfxCsYRqHtK4uvCaMMX5g9mX4KxF+6b2Lhjr1LT9On7+hbWbnHzukkc8K/6CAs4AENGMkZMskYjv3+BxjoUmotJ4KT9Be9r1AxCymWMJLYX82HNk8jsxaf/2do8ftYSgfh1ejT7EnMTT6QQFqf0ue2wUwNsx4LRNl2YC8ke434HJ/1ambXTTEj5dT3XRIsLfZ0XFfkp3yX9LhrM9d5ubEfykt6A6RM0e6uyFKS5ti7u3sPh3sPB7sHvV2jrAX5uHjsNPlwVGu06X2Tfr28c6TJ7+SWXxrnr8LI2BaFuzPwf2MesRL88o9+EA0/yD7DfwptiRpYJcGTTmrTQfOJ8d9zKO6PnYr+9NRG9l6MZM1wQ6d2qUO+3BS9xykttGumdi8Els9XRFMXdqS0nOx1m5eTF1HYfMtEtraV+/s9NXVxdnli3MwBC+ufnzx7uLs7RvXnBRzHVe5Jqn/sqIHb6aLH/ZgTon94hZ+ywbN9u/b33/C/fsypIHKySZN+AJWHsfsrhSBLEbHQl+JN7CyeRr17MQ9QdAqSICtSS0crSj11pqGtzeQ3yCIHyPgGLlF4Dy13GYQYdBjhI881Asze7SZBppWGvEPfOk2kt5LZaQAulyyHMGAWKKAAnlTrPDbMYL68TsYCMDkP7xMrjvh7s7hnnQn3Nva3dk6/OL2hDb2JMBnJpBAF0CMaoIOSoo+X4u8Vs/AIF1CYOFfY7PAX/cuX7umTNRgiSszkW+Fiy7E4tQQYipGmcqftvlKSBQYC0xAZol6KDtwBdeVGjRVvAYSsqfKrGho5N0kL6FtcUbPn9F3gyY38ALcP4fgpsgmrj5DU2J0JdO7q4f4KrKzmzEvM9qAjLWWgLekotzIJmxt39qYdJc9Ofc97pjrCGRdwyipwJfFdDRxjl+k6u6XGGUH6Hox/+iEc23Du7p1gShn7NCRVFNwVcuumFMyubNbkhCJXxBn9CUvmdhkNM2fR33JyhbuztjFU3O9Fn9fX3fxdwTY1Y60zW/g50xObvkypwO9qo5SlZFAq4p+oj5lam2ZNgS+pKxLdHaT6ODy35WFbaWQGSSX8KR9WDcz7pYuILljPwqFCEIjlWxh7/CQuh12MMfdBP3aEnIHliQCflIoUTHR/BUnth+iL6iVuNbK912PgEnVkl9djkgqoFXa90wILpazaYxwWV2QTacsKPLruk7rGDu9AIrKYYF1p8ne4xozXN7PsJhLvW05Crsa/ObFc82Mol2wT3WiIBBEh8SjCxCTqENhxGYUo+bVYjC4iBb5NCWEV0iQnoy0/OLurDy0UaEd5fDa+RWe1ytz2q7aAoHOfyz//KdMjZ156FO9KMv5zCko8vN5rYMH5k9QfIa4FLSHbYcaIR8lY+a6KQumfLPPYgAqHjhebB9pkU+Cm6jlb9nprQUqOLdQJlb/QQFdcE1tHSO3ONQFbEvETwfmq0ZeviCt+u9BxsVmivQmsfwUWm0keYfMEj+xzDnBCUjhre4UOnwyRSmYBlBMvVc4Z+mG0g9YnUzq0+8A6fM2q00CbSel5PKwsOFuTO7BJfQIDKaK9D7XFl/XnzdhzMXUwcFXzD5sZxw9RKKLGBbORKGQe2XV2uS24ugLAmw5pipMVihHlcm6oLgWexNNGaovuPjtKwXmrqI50EYfU+mp/nmuj2LQHvPs1wvmkKrnmVYN+HKP17QBbhC+RuAmbROsCbHP80E3J2e+fwqmAxgg4iw4KmX9858UfJ0twMTAlOvWFzQfZy1/591ZRRf5Pka3XCa+qjbCm5pyPOFZt7djvV1//tOq89lhbj35ebh8hAWE9G0sxB2WFs3zLOjVdVPCcH2N4brPpuGeD152Aumj1pUuhDm/K1dOK9fEQBrvVkQalS0EDYCzHoZvkgGB/OdXI/Zhrl+jN2zjAbKb8TOD5mfSaIoq6utpxJaHW4+YMhfkJEhRkbMSn6yDOZbESNBfeCXu1vfrwJ4+IBsWyONgmnE7d3pWNA7b7WCTtIYIm5zk8LQ48XW9hrjsDJO1mr1TmGRrTFfFNDJXE97f1vX+E6K3KXO6ucrn3PTWhXFS0irBKxOQ9bypPhXDh6tThhf1aWFYSfludBRU4XJ8X0vbE8hlOq0vYCO3tTguS0wrSWHwTl/TAo9Qe7tEHUeW+M540RCIjE1xQtSZbznejW24Zgs094theVd/IRVygnhgW06BYTIN5cRCIg3BB7uCfZ+pzyKFy+56ANxN5DKfV0w9RrZYK5lnQmC5mH9L8D1f605Rw22ObQVYy3M0Cp+FdQRhqWWhtvQYLPCoKUSIWCK8PaMErqtb01015pnIaqmgO+Ybg3Zow5yn1lYJGEtUgk9DcmGbsqmAbr3lfAhVAkYqeOKlugLfmUZDpbFv2Ca7p383976cfqpAw+AbroC/S6436NVyen7JmZ+7whObreAJ6sQy0W98oReOpfhQ7ZyVpcXNVMGrmFJzbmjhs8LsFKAe1OWFtXIxdIY0BWFXI0GOViiML042+iObYWJS57V1JbilVMju5bmY5n4vgnbOceawg4fEbTvbLufMqOCvXx5qIZM9ZfSp/Ii3VMuUqK0Xl3212iC9ClO4ur7r9vAODCG7k3q9iEEK5jRAIbzx+v2jOFqyhui0z3G9GcL9XGVuN2UOq8qBS9W1ORRqRE4za8qBs9YzlVkCKoWFmMk6iDsigXswhsRE3ZpptmiloyBCM0Iqm5ltUuQIBbcT49c1mIDDuuE8VviM18XIFpcSSwRXoLnsRaaSMzgey+yKrK1ozjSxEaxzpFvnwyW9qiN+M4lHZ6f6BbYGR74EO8dL76rITPnGBt39oCDkEYV+BQrDiO8IidMRAKKwpTpvPiCoNynSOhJ4MfEJ6jgHyzxhaSqiJ62zMzeSGxi4bJv0MmB+XPm9LjlpqdXr/qVGUHQxDS1oETBqYf2YRZ/Yz8B7Rr+znt+V3hVXK0/r483infqH/LUekXOew86sLbhL1Pk5R5NuL91KmeD7JOFRcdYjdQ+tuTxCi0ozZrGYeprrY7y41fymDrJqGQcyZbwNBW9Wocz0qKkJkL/e6xuwCPlRk8F2ZPYZT9XLq/BnSYZMY/thyAAv+OjaWnHeMjHL1KJrixa1gqaJUBdjJQKdcx3VNc08NtGRVYZUYBQQBtrA9e9nVyb3vGjGGuah6XUjyBpLALd8tUK55JQ7ipxTrOKWcOeKGm5B5ZKgUgLtm6JFjE9AJMlrxVGkJUHOJZXibNezLY95mYqCYa7BHd3aoPNn15DiNzjgI0PkUypyo2ceq0W6KoIkNmCPyhkVUN1KZCKAtxo3Uz+nfR9dHx+DHYIBi2xUNKiNDLw0lrebJMLua1cqoUzUm9rZtgyqGT0ozNEtZJ+rcWWXb6rC2uaniTeGE3nPVVAGyZMdHHaW6JnwERIl8SwOXnuYXnYoGgLsIrzXVu9s2fS4/86IWBJUnRY63B1xdrI5VsYrYC1UFXpxRkFKNm7qUJzgxagyoJjsMvjqp5gR2L6J8LK5G2t6yUvA9lcuXJsTSAbpkcEUWPiDMzTEu20Nfpb0JKJqSL8FAnbJ+eFTK0xLCD0UfIvz0l0KSW20gr5ONQ9K2pcZXcQQSwMV4ZPyqqlhZGLlQaqWhuqamN2OgaaT+loqBCblo9y931t675fSZNgQhPKZcxBbHcxU61QaSbi3le4IWSIzTpMfhbHVVukZoZR+fh1Uw6GEAGtlygWxH4Lur9yRMac+JAPmcjGL6Zzrpb9Mvp9N0TNoxdYtpBuXJkGRjCsOjG8ICftnuviYP8J/P8qJeGX0khRFU9WNcCd0pPiuYW/KLIJ/flcJCODBI8mmugy547L7RWqiE7+vsSx7hHzhC4k200jUWlsJfPmH83DfnWWpVpU2B3W4Rw77x20FZWAQ6IpirqQkuJ0TzhG3ZfhRwY3Okn791kuqwPjtWoMwfucEDlIMEGJ/qa7vGjRjGlrCNX2a1BiHJeHUNCoqM+7AOj857ite8i8DOz+NwM5PVoCdn+45sLOdwN8I7bwcyvsLQZ3l2V0QZ/14JZj5SCZrZjnwTZmygGZcrTyGLgY1rxo8D2zegSE+4O18wMzzcFxRzhyLICqO+oBamJb3YL1pRCXwatGP3+TfUnAcVtPJqUr+dl1SVA7ldBccUMDLd+V41nJsgK5iKy2rZLSASBhGpiFRXiK/LyY0pmjhjIVgj7qQ609ACG0SnSEBFyUPPuGZNlhwB2bSLVk5IjtwEv/2r/8r7GfAXM1bWdjy4eODPYUtP/HaO04zYkgQ/of7e1ZipUlo9XevX4Ux7TyUYF1ANLWa9GCABy6CY0GHzIRYYtTH2rkrijf2wSCauWZwbUR/wxvRRMLuddGgQ4rgSLUDf/fq7OVLWtjTyxfMcznibrEI/Kb2Gkgc6e3QtfDWz6l9q4SYe/03BEpmq6zPWyqy+fJ173nR3l3X1GuhGn5czLDWmA2GzmqVKGACMgddJLLksQxoXsZwsljpOfC5ac8Q81adnRZTMU8wNKwsL4Vpns5+klpkYuI6OlZ+HP6fHK9znHjoDgvezwbaHz5V3zZ3owxcAkh1dfsc5FFUbmaFpmZ/lulHpvbJywTm+kHq6zuYDK2ZK2rYPbSbkMN/LAFnu71q5w9jCaYLEY923BYe7TaHYibo8mrOZBc7IIoCeqP6xsqjpWMrkCcqpV0Rng6MUcfGrn1FbzxPBkEB+koN8bOe1YXzWN39wdWwkrPXlJ4It/VFuUube6NN0OK/HtAZxGTEYjYXLISWH6QMy9TJwYGrkQSAG2HFRdmOKN1lOTZ9aI5kaLGAf5L/QhXSuV+7ugcm0LLZui+PZtvWJrUy55DUi5grcQJEbs4TRgu8xUoPjSASYx3jHlY0zcw+UruAZc+uubpeAv+l02AJwQrdXalTZcxF8ieW2q7CmM9Yay9aZ6U5d17AjnEMxO0+cV+1xHrs2lmzklDmqHUXY4nQWHbaLbJPeqWhE+4x0AXBOkv/mQuHhkdzg96awuX8XUaDEOymRBo3ztCMwfKjHh7+p49WyzrieDg5P5OyZhpQNR+uHbFoMP+3p57HwxA3B4yIhJSY7WZc3PsjIDhX3sJ+r51xBy7ay+XbMVq9HaL9OhnH7vXoWHVItVia8mCuKhdEMX2RwpcLmrW6QLPYAr4MhBvYl66VpRZm3DAegiSB38trtS7IHMkmaiWe6xUezYAUD0Nk2t4tmftSbrK7s7Oz+gi4PUd20TTtQTq2qW9YEhTjvNA1mRGmWKB4PnZVXU/f5tVp1CAFFnldetfCoKMs6zkXey8XG8sQiQm+PAWIePp1iU+RDKqpg+t04ARoHIlYPmgwGGdsne8jHaxSopGfhUyMO/IEB6LkWhs6GHbhLrzylZZPiDKXQ+z4nr78KJgLbKl+Ky/PXZzFVaojxVYN11zCG11UuSqhNMNjt86BSrg/XFTEZsst7CVYjdwQ9yG9a5nCvNTuimHdXO9HQ4kxTyLFQgYjwbKcYB0jfANruS9Tb/wookAC7SGpEg7+OINOuBjWMBXmHNKX/satr3Bpo8X+85/uHd1Y1OhsKp3O2O7IXuIvqj0TuHbauOYbAoAMx3Csv+1XU6wqGuCf+t+10lEC7jT+4TvuD4VXFuk9ubR3E08vLc5gaYXkksoG1y2346rk+gmwW2E0Svy65pi1WG1s5K4r66SluC6520PdYyRExjnKYGjJkuiqt0NTU7bfz5gBUuiWkzANAtiMF1rnXX6xK6MhDSHs7hsJ3A+MUsNxRDhZY0PxWcDMIf/guNe/L/gF7TGom372+K5XWeZ4jNmqlj5oWgmLB5KDPZhHWlluxhRJrmOiFNIJ96PE4WJKyJjkqZIC8tSvs04Q3WVHaOVgd36ucUsFiSoV0q6sXm6NT+05/apXToqKOducaRAjEH81vW5nXy/3+F5hI5dh2WDHYka+d5mfJVMRt5pTp0qmqimzrQqyMpIYbcJGtpqAKTgRxycY4y9kyEokwe6QZV7EUGpXl4rFHIHtN0GzcM8cZ7htMs00cVPXbkPnHik1F63CRR3QzgsfUqCfFW/OwmdK2RL/9ZzcP0qNtzz5mrafKKLyjJ/lKnbb247I6kuGNbTS+d5NbcmkXejsCvuDGRcTwvCicGPUu3OeaH7J1jUX2wlVLfrXY5AY82WZtqqdy+EcIxyMuRwEdcqhTvF1vgB2Gmv0S46Hst/KcLEqDcDAdn8sE5WiQPC08bdLizKG1woqh7uzVt2Xg1BdMzxTq9QuJpOicS9gj86z8AWsRJf0gA8JcRadC8MMrkcWnkuJTtHAqYarHKS/v9LiqNt1dGVwJrJlJrJrasDZEyC9R4qZ+uXJWlgtk2q+bo4FX+CQJmekfe5AEwYdkUlkhtK2IPxGN3AcOY6lGd4lsjU9Ka6FMBfa9EfgrfdRUMzrkFpXLAg7gpdNG8bzLyx22bDhPsrcgce2iDtmn7OlnsFbUBzXF0pFJvC7Gi/fqCrG9W14RPNMdGm5eFtSf6aEavFaVgzDQhtU34inaS7Yy2t4Hn/2a3YdB9cPSsLJ1cUaN82vxJfUULu+z8Zd1Wbxr4oHtAZWeBSXRYPQHZEEk3oEFhBbBNwjmUkmWO1mp7tuVXQo55dZZyJRfeG8ROcUTmby2CL40O2LCwHC06+Sm5nseFBPAIGVp357FX7zeVzd3AwwcQ5f/POfHBCHfL0//0lavdD+zwubOM3YpyfYUAhXYUHZVDZW40gCXBoxqT1gzzbH4xLZJDKguH9fXk7cz8hROibeMoXkWQcMceGsaVcHKuxehE1tA1JtPPPLTVcizHdyDXdttrgeoyRNRdwalS9y4BGah81CyHkjU35NPeC7PHXbXdLQwRScEV7d1w/a7nmudXbRSpPOjj6T/deCifDuTru4dqgO8CHVjVGW3/m9WZPgAq5RLJ9m8O6q27sx/F/nbXbr/4x1lFNwFuS6pN79hmjlJOhpr0vvDUgb5FLWeAE6xjorgtZqLG9sZ7lJ1kCAKkEoyTWpyMGsni0CkHL3BaVlIaCRLkk2pFkl6H5s9wA3N39+voBEINU/USU/N1j0ndnSfcF4QlPewoEdCwxGizUES589QF9S4J+SdviqdAJhOOM0H935TfGpuOA+11rwZUlRyW2caeOtsxcmnp+D06/bhcyxMQ/HKBYJGKNhocBDM01mT7vIednu2mNTkP1OBXdeP0hjalec3yFVpEc1u/9Kama5ETJ+ow35CbcCn2apTxZgCGWTnBerMKkvzHA8XsPjc6YYmrRWlcGfPvx0cfbhp+9d73NB0BRt2JmhnmamswQX+3fAVOCe9he1m8J3Wsvre4FtoIfmBqO4Mbm4AFdokIJSBVpLoWpfD1c/H3ujE2+NSavyXYuTr9oe9YBaWydc3KOJoZl6V2ZwjWi9cp4MAxumTV9yqA/Sr9qJlO16jUVdUwczqmRmUHHWawpVMRdAEZLhnpSFc6gy7nQ2+SwVocGwr4uPQceAfoh6y6hZFesdK9p3rShM+0dMMMO2htTTiRouUvIZdjIlfRZO3PdstHaiy+n2Ofs1K7nCuG/CwgLEwAI+6scU9grpnp+gLyTIR3kiRdwI2WpGBccJPESPu4AhBWfYlVm+ic5lDctApRcY/rDSiTrbPMD+tTVzXt8RsRyCQXXfSSPkbIdcqaWQj2CxXlNOkAMWTMbft5aGkfpLNyBsqMWDqA5cs43fXVw86n1ajJHdl/RUFT3ELnaijs61tMERp0jio5h6q7MWkiwCOsj3HVuwoU7Se5eWwfFMLv2e5sIZtscwZcMUlkP3+xMSYqidHkg8PZ19FItfjM2KFdbZX6SwgjioLItCSRiwjE5YkFbiBJSm0OP7n3WEzkMSeilzuzzvcSq5NWDL0N83s+vsO9C/AIfw49vF/H/rvUA/qndOpDDkkAi8O9yQ1tosoeiHFSDZPyzAusM1EMIaetWcB7C8hNOHWR5MRtP1taDs1MPkuh7nLr7DkERXUBDCHE24colCx+9jGndU0yUNOYxTSk7wtrrEytKKPBLKTLpwfCyLmdYXc7iMF6PrCuXCKFR7SspD2Qk6PRHf6sZ37RkOwVw3Qb2O5kxrlYhpsYc56x5AxQnRPmUirgTyndpEhx1W5xcTtwRjrkLad6hNthpfvPnx7N3bN69fvLlEqC1T5UtXNa0h1rolrERZTBVaBEsaStv684PDq/jW0FTh0490pLp4BWeLyY9z3DI+u6VHPXOb/pb8KQ7P93ZWTmMqVTViJJIsSS6x3hS0vbQBUO5xFOUxZVfOAQrLRag0nn6+tA3QvdwCF01k6AkV+FKIrJXGos4yydoEq6f9fVN8wkV9WaPJx4lcYXvQsgVNcnQBXhnW6BrCwZQleZ15/Yxjd7hmsim4DqOqRbcGu0gq06NrBDLDDjOynR6twYhsLRrM4mFtuEwLVCX6iT66b1JGCYWsw3Jdgu04GsAvB+2wnGK7OqW8FMBGaLPxlETW4R0XUJiew5xlEgE3JU/PuCQixTHr3Wl7BTEIrsSlMsFR+boiP1XjAShYfOxXpsf15xj9hTlH0vgyEwGi3AOx0933xDyO6mEzq0lH22cGW8yIkSQHr+PsOZp6I9gGCpmPqDwz1bK/JdB6Rs0aGx2L6+5dVQJZboYJM+8BhSa6ayYc+zYuREcBuaXWfzJHu1k4Lls/AR7EXbCMP9WKASuKfYJcCYQbqBDdAe5WWX4cEzFh1W76M0DxZD1/FlFYtB9jSrmE7TCCC7rt+VIj2Ea2MHTk8xnaXJnjaGFYQKsAFAPKUInfvX41eHd+mhNdmatVYmzDtMS4Lpk9QYSt8Q6reaxWOAW3EiIpuHS6apPKEtabcr2wXBdPj6sU8T6ujnTxAB7h5zB+TvR3TOsiYVBtS4IS54vSK/B9LqMZRZkWrADCzDh8h+tKKYrZNMUDdw5ZqXtSxZEcIdYkWfx0p8vwbDGGQwzWDZzhCzUa2FdYz2ggzvn55Aq3PuCcH970ubO0Mfe1YXU5NXy+hc27rTgS5ILhXrWca7C1Uowj3eRuiTdspm3iXwXpyQ9RtEAiYFqp2unoJRy0jE9OsgVYMiiFRC29qaPREL5L9ky6iJZieBGdHoX3kH89DcnuOIRmHqPETi4ZzGURgjVv55hfMXjH/Bwi78C7Ovx2Fh+DefvXDm5l+32k9svBmtyOP4MQxa2Rkvf1kB94cbsUh57EkSw7iuRdhx2VwdTFYQYPRSk8mWBT2nmJAHkSf8wNk02imP216+jvSUHU6WyHdZuHS7iqOqoHst3ZpFT6ZxEWPO3t7h/vPDk+3PvC7mxPDwxhgXv+34ivYHWV/t8ZZ8GeTJjO2Or+azAet0i+tN8MmQqWDJklKdjd38Xua8gXFzQ540uPh9Dfye+l6X2epXar91O9oCilVPhqvGYifb5BPIk05WwzXAWPCsnV/R/t7O5o2b+PQXQXH+c79LDbyGYR2zG+EFzhvULszhwJVGKBbFNiCRGhrvNPXO8ooW7LOnPr5sVCpeCFsaxZZFCSSL748fseB2VXuZHLY6x3DBAnNPxXrStHF/2ZnLOupgAHKRkDcyOYrEhhjgu9GSPfub6dYn1JSl4HXw876rWb5Y+3bZH5fRYz8D5H3dS+oX4MGp3MmNxKC765A7UZGnUGSersa6zl3P9tUZiGVcGakK4Nu917X8Go1KmEZNIu9jfFEH+M7qeQLlH2hW3A0MfAgKrvH5JZq/11EYfv4HANvQNLHEI8c0//AG+IfdwSsMeylrfd0H6CUIswc63TRCqYVCnmAdBOxYaPLnVJIgeVOTquQcVl8O5fgjE00E1jRnVCjnkT072VGBEbKJhA6OJy4uQbHoL371518qByUEMigNl3/HnARHaT+PmOSsyIxjhSOy+lStfc0Xl8pD3PKw8MlvUiLrdRT4tIyoy4QoKHcdWK5e4XZ1jM0vyONBWDo49mdq9ZCHGWmRCHyBBqiPA7irwKjT4T222GUcqoalx3tOx2T3GKWFdLxUVmu8hSyO5UhGDrYj0L0w+hOPHiVCtQcPbViHx2DrcixS61yHYblWyA0060CRg8TvYT1VgOPqG/zP6IRFoctVHrhSPj7mDl00qY9WNvV0avvb8mrGND8F4+mqvZv8D0SzIZDcVnd6KTAyRJwQiegdnLCUBo2jVlQnnKI5wC4lYIIUmU9/34Vxgy79Ne9m/L+VVLTWqvHCVB3yXDWHYQ/47gLipu8Zp9+bXbzBSwhEyj5+980LjABZoxyWOtEol1juQO61l1W2nVWDkKOeTzMTuWV1ZGc8WcHobAzuK+tQFBwbXvVOz4XU2CkHxCedPlJzTinGYPWg6CWSfBhriOssxkYalV/lPG0nK3ynEBesxWhpaRiG9CRopV1yoWVGpA1knPzODMrFsYFQPTvjpFLU99gLULmU8/Y0QMDzZt1JX4FRuPvsoD1XiTcpP7e0TNHezs9/pvYJCTxRx8Vox09nsv6LGeM8lZw7wVeEaH/Ax6/kGub9D+2pmttMM0qCpGkGGJpLhAxPwlmDs+uh05xAvMaIiYvVtMrqeYOkgCu/IFEpdVLiUWKXKK73l1b5p2a1hewAske+3x9YIyJjvgQL6NBTorHAUPdwEpJxKLd9m8QdEO/PjhoJqOwaSJ5yNLWh9m1O27clRK+akRD9cPjuyOxqTRUrnqpqOrg7A0J7auHySsELmDWch3Nqni9jxj5r4w5c4SIMylof6fuZ+8JjzGBTkVLH07wp/pzgzv6rrVtKmYw0zw2N45NofIFPrSlBVqXw0Xr6hHcczJWCSCAZZ5OfXJhzHSJwphciB3HCqB5s4cByTVOvhDiq5ulj68pUGtT1V5nzH830/hjW7v5saS/Upa6J00t8TY9UI/+SpoEemAN3KwinaJw5LYD3JcfP8S0/HqgQDjHI5mdYhhYTj+/tQYAbz3RaH5v9r75ia2HqwoWRvfgRQDVyS6k9XBc0X5QU/lRUuam0bE7rykUiewfbGb9QzDxdPhg9Jxg8C05P2buAqS8p+UDbcmg1mlttH69Si5xelGDvI5ekWUqA+9k7atCCrV11ItvoUdADMCQp2AQeQ6U/TlyXEAzfeqLiZUAo1FHZ3NgzXMOTE7kgYbFLLvpuWMU3IsPNcZCsI+in/e4n7ITR5s9c6qrU7al56iAYbNWlgfZUNJS2nkaaz3Yn9RKt8dm6psN0UL6A6OTQ4Ww0SKtqGzNSCLjtJ+lLnIXKrdrb2Dtdz2zrS3a/Q7cRqLUWTNbTGtQIdMHmQLhQd4Ajbo3YC++FAWzeCaKOClzq5vMldgSHM63tr+F+U8RCFY+jd30cl1cFc5+9oi5WbffXOtL3393TfbsyRUkc3HapMM0E6fqlueSuQ+x9g9epKhgu184MmomCnciO3ZMajCuZwD8QbFpzMN6aYj25dD/bWr1y+en51cPTu5PP3h6tXZ67NLj39Er4j7IaBJA0vW2YR+a8+nU+g/d00U3y9SZMNEbrJnhboZF7e9/nPdoPiLIQAdzgRWeYKNWftuNlaWydtED+fjGPANkTDgah6T3Q/fc8e9Z/5GBHkkcz681JG6BQUJhBK1LYNqteDZu09pcEotNSWSQpcakGtZEqDFREjOzZ7QVZXFFEMB06ikzzafx+N1UfLCu3zZ1mSkvUrYSuJsHt/CTc+YugBpruBV/jFzbofp40NMHx/+nOTxwWBvv7fzBNnuD2K2+8OlyeOjgyObPD78m6aOl6ZN/86yxrDu93U9FJqtgeWMz+aN92FEy8v1TTsrpt/ZLjtwGfBPcS556YPylPePd+BhIBnpzP/qPz/Z2338Nb4s/fPoaz2Wk+IjAkzmvlgPk66LKZ3Ph5AGH2nbZObSQ5msgm+K3l1T3nzrj1TdjJB+o6VzJaWf2+YFtvvfmWX4Zrv4ju5ZMJA5mn28s9/2pzXcIfCm4McgffFXWyjj/wEv+T+DE2JiGgffkRj5hzAgjwGlDC1a29toqZZpE6R0U2FQbJPrnLF4Ws8tt0KhEV8U7UOgKT20gYHvMu5mzE3gnQg/2D+X5cx0onHCzkJr6HwiVYWk0fyvL6gVgSBY0LmU1gZsmVBzgUr73gQEmv/g8sP4jU+Ue3ngLgGKAcIHEvkDZV11rcjQCHvQ8aJMucu2ZlSQxHeKve7tinMnEy4DVSA9IwGHlRtsm3YO9vA0bRhjB3sBOzNt2aOhHZ/BC00oX0PsgemDwYJrtfxXf7sVfC/oeIXd5ekWxCYhF803ZpRNqV+EXx7zodTjl94OFbjKM01nyF6PgR93EPgT2yb8MrA/uK5r3KV20CUlttM7ZN76mfwc75Tf0b/JzAm5McAE4aC9q2az+PP1X+KSMlnvqAeDjPTv8D4BScXPfJMLO8a/wzvIXR+Ia/Mz30JbKJ+og/TvcbYEddb8/DOlCHYzfxZQs+9e8bUhmc9FrX6KPnrJJhtJ5FiOoB77p56zIJU+5RddnOT91Oa075mbd26yodJ9TwUbZ0gkuxjOA0H8kipfyxlGmul/K/OtXu3jHWhiR8127urZJpgnZKTf1gSd6TAMovUQKg88v78HZyAwOb5g0wMdALtPZhmYKIK7mrGm4ZgfOkhj/C4a9oS24mo+1OlSxYC4H04rak05IomCED3/ppYm2+SCSOUP8xOa4COsjjBGW4YXRqJyrIYItUSpPiCQguMUfEC1HNW9x+UdArrRF6qmzD+Eg4i9qK3T1WYkqwKLrZ1Zs8mFEKFxw2cIUTegcN2TtP8SsS0SwG9z1fbCxVpM2u15PauGcJtvQxty7T00CYzFJDzCz8Xn8SeX7GVT9tQ5KlJ7tqlV8qvb+de9fU628YsjGJMwl8W4rfntv9jSxeDUbUlb6IJSmbSuhog6khGMqmk3TdofizTdSe3ahxk49ndwHdM1D1bk9PWFXBRCMvhVfl1Nq8li4su1qIpLCVBlVm4HOldcyhJc90yeBKGldGDcMEOsWnHXwXrKRrG2GSojG43Ta9GrSBMiOvu7X7uUqv9BZv3tD/YQPxf8oGNX/A8eRwrmg1jhAj+ghPhdCWJRjOzNJacTD5wU0RfereM9yUF8n+4coT+M23XgOLOMFwXr/eITLeNpMS6RyeJYi8t7TtrcCOpufjWslb6a359AGGJYIFEC2uQI/SAxqucLfRXYr+ugX4Pkr+g3mnTWXtjuwbjfygICXx9+9I++DBKFNokjzWWrGw8vQXCHCwwtJuNiIc6Ur/2Qd7B1Md9Tg2+sY3uOgdk7//BzelOCvizm9WBYjIdKtxXhEoNknbwsPnhUT0X8zgUsQx8aJ46W8A+LaviRccTqxLqp0h8UtjxsTM0l3I+z58aJ40A7ppXvh1h0pEt3pbb51QSEUD26qqZXytzAUTn7yuIpEhCCPuSyLy35civH0F2mp+y9n8FSjNvtUybywH/3GDhK72CKn/2jmLEbb/gW88szCx0mnPiIMdhCGrLovIjWlnJ0Ir3fvn32wlZk0djYhzNmTKSKKErQELEmviC1cm3dq3AQGTdsb+frN7CObxGaS/8NN5yLspF8h1xz/7AfdYdsI2c9owRaVVNDNM7+zs6v7b6asIMfVabxAwahXd8HnYvLLCXNKQxnh3HGDxypmBcKZ0KmNSzQsdatCiB8TJUSsFBgZIDTRvIDQ4kqTTCEkD0u89HYtBQam9uk3B5aGEmIOOkdQS9IKx4P4jgEkTevvGcsNSlZH/yvEAfqvjAubwj2qGe5npbhoLLmcFOv5PWudCxd+aCHUNglSn9+/v5Sv80XLnwIHLNZXUmHqtcPvZMhsx0bfJAQc4s8IW5ul6ULByNxeSXJnogbhc5VLE8URIC/s6Evc1vHoATGwWPKDDOeXhsLKlGvEbvRJkpT6U+ZgXdck+Xib0H8QNORelS13Ex5RD1KRDwwRzpZ+EqZCnPY1qQGw+od80x6hFwNKrp+dLeNbEOMoRKKmJFkh99Oez/UY3cb4Qzq1nOID34lnyXLKCt0K3AJn2yXPiQjB3ynBeRsTZDCcXlAxQDm30qndIGONfgvL0t3hnn93OlgXI8JW3jZFQ/tv09a8ROsGh0uxKoz1NFL20C8hJWe66lfPN9Mb6FXRlt8t9tMBGv7uNrM2HYaLRX0w9SQAIACcqKBRc91TZiNUBYHQlRRDl6MElde5nLErsU+/Pq/ZodcZ0DVUWz0JMoCfyNVpbzUV/fDK5agYMwt5r5FvetEGP54XNdUtBRbSr7vtQf/tWV2abILEx13wbSQqmTtrN/g9TqlZdqJXoxbnaURQkUH49/E6FKtIvUEGWMz96DgPXYy78EPcmNQktFoKy7iRzJ/FBKhTEJRhgLEv5MQM/X+65XxiK+QJw8NNF/OTev0X/0H4apES0pXVmi9Flx/ZoDWarrHVwIM1AUuq1s2J+4CQYtHz72V4JTAovO6mYHEI3Vw43MfbWjmuqC1oXkSzjAwgzDrKT2MK4ah0NQdHGRWpZmZxIee8TOerrQXN51134VbNKu9KgoT6EZDZEyV+4z7xIfHNT4WnlW1wWq2TFcPPXkYFNZ88LZFcPi9YtMKNEfQJFpCO/0YtJmzNtNLcphcEqSwlKEQ2o3gQjqzSMmiaofYDnxbhg/+8Ls3x6/HDo0DjlOpEp1X2OTaddrhIT/3rpv6I5K35S2KiI3R9jCircSecr+piepYQd5apFi1goYLB7wBB0GLYgVtL6KgMBVR99UICehRT8kM4ltIbS3x8UGJKu34h1MEwsnaWFsKhOES4S/HihPqcyZbTBVGOXInE0+klZx2FHMo0bXk80jEDkNY7sRC0GQWsyTzV/lSuU/GcrQTaaTuP/jDhafCC4yjqBtd2D6pSl4waCDAujMG5ppvz7PBBjRmYXu2M8SM0pRvOuUogA0Lj/JKOqrxy9uTQv+1uMZAuNMzSC+YN0eXWJ3KwwlTxLv0v19dvLi4OHv7xvkFoehNFYTpVkat4L57Q/9x3HuL5TR+FB/anhWtMGZT146S2qjTT/3IlpDIoD+5YyoaphMJOpo8hg0Tun5SzKjNIIV4O9XECI3WgAq3nhrIWkTyHFgmUywtxFPmTwbV70cxhkhnFFM6IBSpiywQ2TqK1HDe/0q/oMcgjs24I91jemz0Qj+W2oud65OclpbgSflZno5Gklkhqxd9/OLinlgc2aw+v6vndRthYoPMRw4RwaI2XEGB5pNSQq85A0A4ccGgCDPwU4UQF6EzcqKwuE7wBgdYQ581QrWREVoxvhC9y8Y/SAxyq6h0BiAcSUrzlXadZbDX3u24vsZaN/pG5Pr7+qPEPTa++K/Gc4pAb+1kY6qqn2gIKhyCXdlnVK59GiIwaiLVdca86/BFQqS19z1Ygswy4vYghreW8mbeIzyNuqaReowrrtLprPhBbt1PolVVCLlySTi1ue345vFau0J6F98MsLJU/E9ITwx5IcocC0ownOzyNWjRDFzYrTLySwslD9IdSBppXS+QTtnHzmB2xbiNxAZTCA0xt2NWQwPknrOLUI78vaiNk0qSxP015FRNQekGshIKITwKfDVds9z5yJh/ils3/nXb1kMk/ek9Ozm96Imh2mpiIAIOycuzoRSW/lAEYN5IAyJUJf43FB9dEgHEkNkmhlPmTD5Adfb2B4maZow5216v6ts6PWxODGGc87QeLyZTY6+JYaT48rxYIxjyaXakOJbNnsr9FYf6pC5ghWEUOy8sPr3qYp1GbbSJbB3BaxTRET7QyMyKKqRKG2M2ho2yPbFB2SyEbmMFzscEj7Ln1upkWw7F1ivyQVCUPZ7yjU8FMY8cWf1NpbxbgnvjViIU/rAVTZGfUXjKPQfOJmL7+IaFcHjvBpy/e+ulMHUZ8CTcoBEN1DAqLi9to0XzoN9h9yYi7vQZJvfCn3IZiTbZjAiqFEURu2phXsKsKLKHxXeR6DJfc8kwUZju7HH3zqiomishUmqJiPkuUEaNGEKElMfMovKCgJJZxNdAfEl2PWNLT5JhkY3EPyHZ7gS/sBmAWGHcOo5EqVswUcS8q/K+kYs2/OYPC2qPXBMag0NBItakWEH616aT/xwFbXVxxZi8Rgy1i/KCPT+obwbsUSUFu3zOhZE2Ts2poMP3uC+a+DAo/wAtI7V3iDIPya1wm4+gaFOAP75Hs1BCES/gNgcBYMk7F9QSxuEco8lQ6jB5YKB6qQePAmN7BOUPQQfmTmwg+uPwUXobqDmH97+K1kO+XJKBPzLZzk1PrTsqJXlriQF8qRj7vXMj+lQ3iGx44A4SumRe9jMiC8kwMjy/nIR27L7Eg8dH2rQ9hrG0S0Ksj+mixaX8nNXiMpr3715dveTzcHzMivuKLeArjPe0xtolszYNByayhpPBpJPwhN0PvRBJbWqcDvk8svgcDEDJF7+1fI0DxbHFFaR/tMoj3WlDIWom4C1bZ16Gb6QgEQ8oaJBjQGQf0zWDZYK8lMi4kzp9WsOXKPKgKV9vm1hg4nJI95ovNI0i/F6+CcfBzkGYsEku1BCLVfEfUr1qbj/4r9gky4qPpQ2HlfViAppXMkWEoESBrpMgzF260xWFFuI8J5x7wbyBzKtv6C4p6s34KjhZx0mSyXdHh0LiiUwoRKc4m4Iw6ASGqPPxtU4+N2Eke/C6buYqoNFA8o88jba2FdHKIaaZdK0cVc4kDq45xYjE56dcj1CLcTYn1iSWFABxYj5ubEFPvZdFi/jX3ilVoOaP9I2kHEX5OUUpXaPiJ1sO7nK+DjxIbCJjLCMm+a3dLbHYNWYkwSE6pUuej+k/fX7xwBA7Emyt51WMDFLjAynJWOwjx00YXRbZ8iY7SSMSm2SHDVIW0zhp9IwZ4XKSEmf7Cka4mJVUaIubRTC70DpUbvdgRiC+lziJhq6ZlA/eVLxElKYi8H+UkWlcJKRFgtYFpSzHzKnO7QrQVkrNfEwfrPtqGf8onLU2LMP4nCBEQwTEktyIp9SQVqMB80ZEGxsdiMfRgVArighvOBbaXC2aMRWzz7mevUfUCw2VJ8ev9NKaMIa9QW/1pmJstPrcUFT5zM6oDoPOSYhYu0L5O6xM85RYdZoKe8UwURuX3cIjHsN63VbzRE6jOuF5R5a2V1zU1fhTmU0CwJO0oten84QG7A61w0zUBgrW0bABPzYTDtxPYJAcm2znsW/rDoYaDylswEGbgogdk8NTM82MaiBHJ0YTVq1BhsmoGEivCt2w3xIYENEyqijAl80ksqnJShTHTu3gDuKOwuks6kOawnESh9N7HcIe2GXyv6rvGcLn2mmJcrbZblT2V5hjvkI7T5U1ZQhxOWUpchUp4LTSwuriOHg8HXrEda8BXbJoJ5ONYhlwXd0a+oWOIDfuexKwJLua3AuQv1OwdZ/GgXHPY4aTHYLq9f32MDrundghQWoFZ/qx8gUI4STYcbDV/oLHVQlPJDAerJf1PqIuIGJHyKBUIJMYErmtecbRzehktCqQXZp04DJ2UTx0SW4OnucTFa3ct84mppYOxw1OcQeU/S5OMARpDuc0zvS3oN/gYzPeZDEf27oMb7m6tD6WLsZ+tEoPcJtQw3OUiRtbNbf+tTkQx1mXODcSCLSDrMlBJ/AYtOOU3R3wNMcY57T37X52hcCpq2o6WyRSwgSo/c5oeZTHeCgSyH0nMlHcRrmo/fMHuN2wkir60v7ozrll0yHFkWVCCU7JWVgcW0/u5OH0JJAdASY1NqQNBNB1xW+DgWls7yEXBlEF1MdFFKJ2FoYrlZFMH7ctCNcOznQMalOva0w2XWoXUYnqjC6exGixfyQc2kkxjsPl5JcJLy41iGs5L0lfhsWcFQ3R6VJSlQJVqfXl/LUkX1HIIvquXKovSgpmbfo8AjW+Rp5vSijkwoIVoXPuFZeQpgMd1KPA+5ykc30Liaj/rpzRUcW/df2KYaZSxkzdJvACRIdhQmTERm4wuuHGWharDqVLApNpLEA7Lxi83jTx5VEObqN0H8YspqI0Tz4mgA6jvbSc5oG70TGAcZMoBdNkSOaqnuauqqSFjGgdUGGUvyyY3K/HZRD1zxd3LHM9bTzSzoUdX665nPKuJnOf13BUDX92Jb3WjFVjncuVSYagQYmFX9xr5Rst8H4zoqqrB/lW0ESpwzVxJ6FCxj+PXc7YdPQM74M0lLrn5rHqHxfjMi4iyR5ltWWTCKfEUdM4daBq4moBsTbMU9gpd1FZZrHh1MaQWqzo+bOOfNb6jTOAIoFGC2nxLHS0QjvPEZlYZ3cYAmHYoBbEMMlq182NukzmnUIrI6m9J65YoHc4PGQqOMQ8xsdkFZAitfJOqBh8tm6IzgSmCjAQmgITGcMVxC6D8KALI2tgNOdxyQo7G5urej4wdo0OK4WJfLgaIW2oB1C9gLWUmgFJ/L3Rl45A4c6t5YbzQXA+jROHphn+/g5tvUooEJfwUeChTvEhaIIPXVBTo+vSZoYE1srU+muJzXpsc5y/S9G34ayyV81dAwvG824bqgMNmC7Le70w4JcU0cfgAXDm4Z5IeP4gDc+HsRV/yEPUccQk7we5+OAOFtv7bQDJ0Y6TOaTBfrJhL5QwGXPtV5p0u7qQuOPVZTmZjUladozupYxtSvDGQRvZ1o0geNMR5eQo4ItfV/9Cgf58CUP06suEgc/gGbQBQpyWcxu8lVuMPOzCxDS9hnZG1nVOmlF9HAvAkC41lWQsMUNwSCZLlGO+K0ywYtq7KG7AFMr/TnMDqQHqto/BFY7hvoP60US0XCodH9RsVTUC5pJ4MGVV+Qy43INzEaxFQZlLJP4XHWZRCaki+zEu41RMC0m5HGbHx4RNOiSU1a8DJ8HDDyVJ4AvqfFwpdOpspixNND6Ty6mhfantdNExhqA62DCnGfwwl+YYR8l/5+VRKRv6Dq1BJbcG3Mpbj/FDxkvVGI8hZeHADY5tidEcZgKZGJy+S1j95i0sCjO78kfCBmm1rKY562luL/46AiM2sNzh4ygPuLfWIrlZCMwIg5r4BX1bPA4YCRNpPysb36npRhOPd6WN6S3xcbpDPa04Br4C27+mjZRuSEOUAGKD4ZhtWzGrnIuPUtEHijtXlpMx+2R9uZn5bic6JbjVZlbuFmLcX1IAAvBL4REBurzsSBnEP85RNx33QucoQNlqNCHOPbv0ghiUGm2IYFo9M7+c1xlLxAjqAQMspr7ENXDTEP+TmRBbeCwBSFZ7bAFPkt0kMXDbqMlg6gcEG2bKpjlqacIvmNEjRvBKrixY00s9bW3DnN+dNK1/jLqk91z8fUFQEHzCHCHHWaFfUOmXf0oAwuIHXFS3UyzZvClLUyhNQ4ljFeopK4RxT0A51lhsAhKTBzxlM4+seZ/moXARm3BsPSkZ9jJr96LClkDUIeEtzHlS/bFs+CEh9C3JCCewnkwRb2qgaiLU0M65kcNUXMfV6vHknnm/NFzRZXGBBDNWw0w4a6EVKh1xLl+H5S6fqfSGs4hprFmnlI2CMfwKb6VPGFpICB0g0y49W+/A+voEB72tOjL2eEvUIuVIS77wISeFn3bWRv5h/uCuneuRQGragXcQipT0O4z8eLHcCKmINZr7PsLjR+1SVM9ttpcAMYgda9Wr8zwGtCmu8m1JxleTTMO7uo0C+6KqXQMXpAiZFOic5gNMpXdceu9KFKccqfVxJwzt2E2tytZYcuTYZ4ongv15sqwAIgZiPc2OEP3+x6pFk/FZ8RFBiywSExwCn3tnN9bOOGuD2o+UkUO5I4STI+RPX2JNcxIFu/PZoL17Fq6TURQCiAkrHoupK7SxWB2YR4wj9mXuGEcXrlwC40Tv4+q3BKpv6URTc5sBPyg/nfUZ4KI6qnUCfSH2lyRYX4JyxX/u73y95ZhibdY5YPJQMEp6XL93FSwDTCxbFhg15HWvXc4Qzc2lVuR/YzgD+e//vRuGLG/0rhQuxmsFq9cxOVoi/g0PSywI2hQwz2aiugUqOkDSjvmozDWmgeg8hsnGuPczBwtw68ICPl2HNTbToAyaUgNmPsy9LLT+WhUoHnHe8Y09o9Ieyaqm+jVfq44OBfmV5IJUQwdUuqma1pgQHCczFlQgSo46yohCJz+oyqK+SvDLtABIHI7sc7o4g4MyTlQ/axQ7M+eevbmydhjmIsTOek7TG6kMVmQG6xw41miCoZI0JqjKCwpumNOhRHb4/G971IAkHJ5MDUQb865J8zGi0+IaIkkYFdLDz6BaN/h/4FqUj4Kz6Vw7PSU+ppTnk1lfBHRfehUnvNLG/JULqJLWnbzgoUYEgJ9pAhBlMxjXt5izdgwiHQgIx6TdlIOmvK1aKs/nLkMScuCAU9HqKbIVMcgL2ApuIgFYqb9gDmMHM492AQ0EVWgCGDstGSChbJOZyueOPAp93qCbbMTU1W2Yvnjf+/Hk0m0X/rvVDEH5mZpyMuJ8QQd6mXFthxXXBN8vyDGagiUyox0YrGr5Vm07OJiJeJNfChZ/AfK0wwZGyT3FHi6+IBfRo3gxY3+291NxV9eu4ptzzc4icFi56xJjmCUeIYR9Ek8E03IaDXWBrqJHw+Bjn4UIGPZpXawLSxCdtL6vqZEgSWUBzqAvEpYzeWqpyUMv4IYIIpCEDEL+LA8ADWRqjGXs4NUNONJhg00pkiKWzYGd+7JdM9nQ3PJhZDAAJpUgG3u/r68ZYlPn0mZt7799OCf6BpnXfzdOHLm0Aeg1hPtuctuyKXcDF24lakrsFPL4QeUGBWHWkWjaOCLEcHrRk88yvpQSny4QTRDvDCjSQ3GKYjQwCgKZGtRFRA70crPMLWPA3dg45uxrb15gJM44+gbkb6IV5Myy36NyT90z/G+qiSbXCMObIZhbzAXSpcIhFXgHSamEsWmoD8iM6OmmoC/+6CsL/dep/oTjIVTXOtSjocUg+Y0IeDzToklZR80ZCuj/JdUzUh56edhV6LidHDvh2DMKbVt2rpYA2ywIN6rqhfhBDCuCl3Ox16h+wJ5GI4tNTWGg8G3CiKVSjHlsZyhrP5VTrLQk8gZnBMEPQWfkBFCM3eVYjCJJkTXhrhoJjhTOD/b5oA7c6vdI1AbrDCSoekdMivhU+r6JcvEvTf6Iw4kCgzxXflO0lXobmIlXXIMvRn0Et5tDoiSqzDEx1yubM2g5wb+Y2iyc5g7IArFAlKjDGLHAafyCQGJkaqTOpoZmOEwqLe4JUsQeR0h9l2alvsAZPbe3EDvKgICHBSZIDpjPofA1ulECBhThMWkUhm0WUX90NUclCRLHxql53DU3j/OhCD23pr2mRUJzx1fMMiY4/3EIgLkd4/o4xzQuH0G5j+8dyCMGx7nkEoYiOzLShOkiayL0lTB2ZNeeOMdffKbIfNOBPWIRDBbwbDG3QWrfwCOoPHYwv7XKgTmY9bok9OpdNbOpLVcS1RiR9p4OOZMw+cQoi2USDAyPptwgKnvFZku6P4ZhImrF8UmnLEYu6MFHPEelYQFM5H2d9p6XFPRP1vN18ZlXHEvs2MRMUyVoREY6Tq+DxLvRNhTrWBOLuYkpSIuIWHLcF4GwjHHBjBu0d8Vss3bC83stMAQqRCJu13nxucdVykmC5CIRDdvOFmhS79ZuKUxnZJLC3PtWyI30h/Gx44LoTngX0Sik/skbRs26xgjZ3ENHVEXk7Gw2fuB7irxvmQJjd/hwSwXRK6ih4II5706NlSI1SCnJ5KuiOaMrIajUavJMA90Lk88ycj3AAzrEEtDBumZn97I2ZgfBqwAHRsAX5aJI42he+BKxqFctNbXzKs0a075t7FqmUxen8V6CUjzJ4M7UoByVFkyCcfBkpYIZCAjMtx+Pnh3Dtl57f+uZpTrjg+5i044GrZsM6b2GwxIycFPULFxyDlsalLmawU7jRFxSFsjMCwbqMhMk4n/G64+9wEofOMkUDBZJh3V3Lzzyz+iezhUPuyRcwN0tDV7o0re20L6IPsitoSriBSky4d5iiNTkK8p+PwSAxMJ+azNYoYA9iCAE+LKm6ARWz4Ff1JmfGaOklWuSfT0Wh8ijhs3YnEUL91RqhFLcKHnSpTB0u/I7P3oYCdr+L/IPsJ/n/8XTAsv2u4aw4UmJzX3PDevC3zBl7nmeIwS+0XgUNkdmdfRHQ2nO8bUIWoW4sbRTAzedMPt2GlmioVyM1uJs7o6I7Zsn/cRD2LiD82frkgvXnG9HVmnlUzLsaXSCWkWwmyghoaV0kBxiI73b7fwBnWkXScS7rQKQbx+VihSmoGNJpv7SLQdWRTC5BeGccFIqFZKsnrMmfCMaQ0+IR5lZVLg+elxhK1sNjlAbyiwkPcXuZkmnTFW52SiuYaN7p4mOT9y63RG6x24v+ryZhrpOjgv3sFqv026x5q6L0wbWOmT+UBv51f1ysL0iK3fwBdLJa2QhYFdlrCk+Q99+OXdhkqDRFVXOe+mym5b9zwV+a6LfCpZXiJJdJ14+ns+4DhFYVHHuqThCXmpzB5iYl2M+KocQEAcrS+ukcc8leMsMvg9VGq2ALByGUqZ6StkL95WbErDSI0bkvWK82roSGQ5VhTFJQ5tlg2lWkEAMiXQlSXvFAZ1HkVJ1cHpXJqMz/HAuk2wry5ZmgUAcHAq9NYwLZWxd9nqvF1JvBBOEc22yMbrr8LYjry/IrggiqEggK60wIshuF/ZMOnnmKQNLw7fFB8hrTZ/xCZiaLUXspRq2jKmJVKuRHkOiNuUCq5LB+FxbLYG18YM7ZM6/BxfKHKY6Kjq/t31hcFyaZAyulBgyGGGBFRr45NboH0mk2NAwxOkiPiS2vCTM2uXD0nRs2CoR0S2R+zoqP+nEW3I0l0FmZk4U8vTJE6Row2IGahdVNPDWcJ06t5wvNFJuVnD8P1UjDndHCVUfHzMLoLwbBp8UTr1bqyi2QHAFMclER78NCivZJFudVMxE0F8p3RQgExqRTT3OP85ygeViJAxvN9XTJNEmWEsVDOai3VUTtLJBoSOko8bzO1XSYI5eS3uPmCnsc/ya9rx62RgSwSDbl9yB8+TiI1eIweRlyAWCZIjr5EJjJ2ZaFN36OvKjyf1vgzrHQKIVQeYl3QSUMcybTDHrprjneQiPoxAj6/uwoJHGXxSnoRAt9+ki094g153wRaoByy5DUKpu5pq99dg7vaHKjhgGhFoGocpekVB7mKWUeZ0jtZ3Bgoo53uL2dy+im3IeRFolNmyq+UQpSmD9dT2GHevJjxzeWMO1M7ZL40NrYhtYrVxo0F3Q3sQNNP1UgdxzVcyctQ1DJt6+8Juh2pkRIaChNmXLpSuHj7UI97z1423AyNUpByj+1qSekZJcvj1xKd+oTUrk+DKbiAv5yJyxRpJcPhCwcuU81DGgl2YXGVmp//nFT3qk1X2RCoDwZkSILo+cJtmUs7TIh1YmRWQ1xx2E98c057i+z4DB2nw6sA0vmynNKtqORVoWXBLfaowAnmZ0jv3TPcXghnDXEHPuI5e8QyP1Co28RTMGqVDeUBmyMaDIjgxXjNfDsDfTArtGFmPiXiGJoEtj8cxJoKYzRkM7OcKYJkZxL+sFmbVJOPUkpOaj8QKwER3mCXFUUwegqZJd5uPdVtlnQcltGIcx1O5oubJi85W9IlFwWzjK6bYi7Auy8yhcZa+GwoR95FEuJ5SKvoOEAonTyRN2oVFz2y8unO+Telm653r6Mq1j9d6lKEkG9DokL8fMOM7hs56jONsUBurSvHbAQJehi+EGOkqqE0V53CItc6Mv2AGfR60vyBL/ffGpYC0cYS0UMbQyLkO4X1zscVk0YQ4joN+mwPHJ7wssPoIjSwi/cQesKbLnj+Uf3MWsDaFOhiaiCMEQ1owPBZILM09gOoJJYq3L2xnwzwUdZzAIHQSWNTmBTU0IZGRRrrZmm2qb8U45+lDNGuAhUJ5UYSW8RvMwTWXErnigLvX1pX4tvJi5+75J06NJb6Z3PyqPV4lAAnRzddRpN82tkXjjoxPV3RpcmIvMXcSRkVaKEgzjFzeBaK84VUZZuqt5DT5/Me3qBaHRYvj1aT1jPkNyr0cNbLT+ioK4FKuTGm2PIYuqWzO4PxLAvnJL2tjS/mNHqopiG7CvQ8kY0V/lBw/w8WZMRptxpIdDMCqmc/LbTPg8c/hchGMzIvdVA2qCMOLQ2S9Se9yNVzVoESAJEZ8a2LUA1+RkrQFHkr4VdFPEMWxjsrGhUU3TlSC5zx41X0yd8/Jg14U/6R0kA55QyQf9fHltNjfQIRHlTDvZfK0kTbFs7UjnMemTBFM55pS7X3n2A/aZTCAS+a2W8RB/OL8CfXp8LJ8lHWd8NX66xWwDMhJIxUdUQu1f0dfWZQjQ152562l1P7xiy+RKKJs2HnVMnV35hERXODSQGWVFbs3ByWHV09QnnDeKMRkHf8mKDVslybBmyLTusFXcsuYOQL5IgKJrZK8ebh3k0Cf5ddfuRO/dUlfwr8/Hbj4gmDoWOLdxREL6b//6P+mIYWnIv/3r/+LWyFgBqd67bdo6JWvTtqA/f51kTEVyn025kQ9x3Tj/4+Kf37tCIr/FQgxbNzY/GQCM4Hd/2Vn8cOoOn2TMnVgMwNqUEVCclDQzz21spmK7NA3CuFx7nR++mN4xW5znn5HdtGYKYjoxBOtsawH+RXn1qIgZi03c6QJZlZR/dgYznOlENZRVoE/bjIlskc/yPjFbO2KgMJKATGVRhGT1+DY0lzYcWVbEvyE2/6Yc7SZeoUdLhz45fUmgqA3kgm6XfzdborvWD897FwxOulg0n8qH9EdwvI5t8Ij9r5zfT/l4lefcJ97Hm00uTowmG7QBI3EreiTq5LgbZ76SI9IkpGIQHEuSWwxqRx+UNjqMH2zKz9CvgqcTZjm1MxTx5SNHgZnOKV0EvYdV5dkXNZXN+o7IQdim/A7wMJCcz5OKXyl7daYuilSaz+2ioQfHz31ri5u8hobBfcXff8a7c26yWDiqaU2OiNfA4MXvxA/y6W6NALh3zBkGcTDPvj1MbhOnQCRZCRtjupOG9xUt04kzTe2cnW33/t2rZaZdPDzijpEB0byP1Gd0WjnryRk3eWdS+4nLqTDJRYNBVOBI4fkj4SHtXe+2nJYNCWnfeDA5iJd3ZcR7m0P8hHVTy4qlaNBnD6aBp2nzt8mICfV7nCROYQ+8H1GRDWaxEccRP+5DuN7enVbCCnpqmERKkiSd0Slpi21Apzub4b5+irJBJkabHOzc+pv2IZhL1mRVlB1zRPpB8jP08FdtVGoh7CRO+TMwjW9walz36FHW3ItBPGAnXT3FtrOgE2Rahk3IRF70efmEhYktU3bXUxYhViygvlyZjXSPcvQAHjnNLAISQ6wbVyUgkaXbqWX/whMzHC+ERw5D6kbp8WGqmqi7mnt4QbP0HsTO1l50bjGAJge0ta33gjeNjoeeBCFoSEI+/vHZyDXvKcP2u/weikgzWgXxC4Ehq0+PkuvuqVpS422HgCI9kabOPFR+muCKSxE54vSaG2z6hG2eKeyyFJPoZlPl3XSi81CwsQWXumhbMac6Qowb9DY6TrvaYo8yD5ZdRe4zR/doXp4Aok0GYRNmwToDsp5eHmuUQkyz6oqIzymQBXmm0iBExLUUyOX8MEVsu3/IuPTju3wNcdeD33mYe1wcr1Aeudhi8zfjJuCMllSnF3ppEOcKs6qibRBYXox59t7Nh1F9nukAK+DTieb9arfOTJZVIayA41kYhDb3O2GBjGivI9i7O22k+YQYQPwtKumhAB0yGR9L+JvzRHWjOrP7eQEBlLEifNwmUPy2Ii2QPgH7aG7JuhPjpuummKplVxq0dYmbKCUzCUSrsifx+HFLI8lNx+2KnGaPMrlBjCDFSuXeg4OP64TkW5PCrlY17eU9116D6RG1nvqiVce6oFLU1nUOpgKg2WJc+Nz8uPQJyC+Yw2IGn8XSbs9OR09bZy3FTzW2ULh4gR0cy0m1mBz33klZS+tJFcKQbYi2zBXuZ1mtl8yiowL7uPfDJZYyoDoENTVDsFyttV6tidmHYYYizKM5WZ9vmEMvw/fVqRpOI9g074bxt1wHd74YObVCnuhgVjx4qJ5gwPFlDA8D4/bacnzDK1H0ruFofuQkV3yN97ee5vq+uACqEXgVdejYOmB5JrLM4NNyFRiJ2KARugh8c+0wyXbfSOuOHin/oXfBTN10QoCRQj+7cXLx8TlZgPeF4PutNyACTs5e976HcwkmWO8OG8BOM1iAYUOmCmNTBPjVYQ4TZDM6OyAgwfK0fpGjH/Hs6fnmekuuxLvyUz1e0G8vxlXAaapNjeZU2o/Fj5znDvtnBt1R8SiDhCngztqUzbjNB/GiRTaSi5UAlZcnLW4jP+0v9j0ynk4Yrqh8htOFKNZ/KpPwKw7dxiI6ClF+pkA7b/D6n00/1UlzxkS6W5rPuEbIk/o6fKDrX8WceRF5vP7A7Im0TmMwBPIkh403gpuftz2j4kDN2eRTKfPaYZDcplkKYNoBhVrONH42LBsMYi65/iZ+YtPcUZyK7Op8qmlSfHSuS9KFxNk4iWBig0xk2aicZk9gB2DFQYuCRY4tbncB3rdaK0aJOXQtlJeEulPQ4jtPnANS3Z7ipJwuVt0ydB6c25jgQW0eKEvkxI4Fsaskz0ki1ySmqnkspnxouauJghvS8w0IIJJKdshnnFRTjQeUoy53JGmX7UY+WYkic65KUvMRLDoyPG4gXgeL4oyVoKFc4aAjoKfilqXChfsFOGrIVnivjA4JTlGaaHRvk0uwRsQt3Jcg8BIVSna49ThhLKYHLpENRlvMXTDTUtBoNDOMj0kR44pAgdZMc27Ak354lBXX/0pnaAmkqJAYpWy8wV0gTImGLqJoeRTYC5RtblOy7tZwedYIE3Ybit+R6uxeOR8K8aE5Q2d6vOc+rm8ALaAv62FF22AdTBc7WVNQeN8qwgT5TOH8Lqn2i38f/VZuJpt4ZODPCxPV6a7Yy3l6tLtOn7hilxUjgZdWU1VUyG0ReJ10BSe1RPOoDktrrOuAOmH6YJy9iv2/qD/ra3IJly48MkLB2lPuukm+mt2W+5pQUQjSL4feziMcB6UO5M/HoYcd6qbcTXC2UqtFLEneEKzgmMvfcIz4wGTHRfOgcZZxWGwEr6KXzAdzSUOX98yfXI6yrrob2atyTBl6ai4KNpEsUIiRQOfkpMhyoXWci00GGMEib2KLCY7Lg87/DHbHeDjLYP+hz9jqEMG9arWM1juhUTWbGyVbw6yeGfK5KOMPFzs9yiDZXE2gbLSvBH7ZlK6f5QYLlmi4n2kjexqHL8hRhnSH3RTXnw1ku6MPb0cAYtXbBEE8HB9mRtVYYhN92L88jfXhErG0yiNYwpqVKqAnCc8dETtbjh4pv2k9OZ3L1MJkGZ+UM+cKy+cajhggorK2uXeQKco6LiRckalPzAknbhi3zDB2qfukKj0NHvo8gIMSKyV3DKvVlxhVbHJLVDRIBAQNqpxKi7td26tafkJkRTkgmEKh3VPrjxHYUyfFOFixewdhubqcL7J/c8chHzmi+gVH22jOqpRmUQTabnGeWibTNQfzMkuuS0D3hFr8oqJ2hj9mAohuybKh71XUosLD33xUcOFnbYurhngb8mFQEHHKRGjwFpl5xFjtNC5R1xhLdO6pCncGeoRHKgVxEbh7NhkPOCC/9XkyDmEkZJUcY27uQUWl7zkTrg44tLUsQeiUCoWUNWbe0itzsSZBkJwrzLU5Lr+XO1+deeuod/1IGxCrts8298l2XKxYic2tdyeRG6UBZHGWcVqepIQ6pB+sxaZ3KJP1jns6hgi2QOuikZULg0s8hl11Ox6zwjEjOHfD9DJLiuV+npKQehjHBH7qzhBW47MNJJshJHo/V4GHPF5Y6Z9UQidDr/cOp65TSVedPFlnKBfXGzAtx3InNdcsB5WUZoYd9kTsoVFTzzCroDZRbWqKW82NpCAnzvwiaHzThiQWjbW0uwEDkTjuwvTlqrZyWcLusGs4fOc1NZdT1g4PuIumuRreOJyWsywyrQCDaOHcNAysIggXyFwJiWEXLi4byZYx+JVcms+PPBvtbhu21OCbnPb+YIeDIDs2hZyZxC8qHrP+i4NSRu1xssaamvqOKObc1JZ31DqgdO2MZNIyKk9lWs/s0KLDoHlGdwbIGAOdTHeV9R00VZ9gGjNsddJggLdreFcal5Tw7DhQLuEX5io6UbL5bjTmXlqcYe5++t9GYNUgcjgv7+A4UbMySQ2YEkb3qkqFlXtQAkH0rvMqSytk/6NxtBPDcpEbyIigq15wOz3Xm1jiHAOK5Gi82Xkji3N+tNHgxd5pDoXI7hpFo6BqsQqkI82CX+PczHHA5evoyMhiYIqENuFqkCZpijWU0E1wYKTjhzM4EDLQmLx8vhorn9tbT1Xmsmb7Wzu/Xm3qd5gJSfMn2nMs6rcSL8Azongc5EgiM7HUyCk+dmLH3caEaSyQYfmw9tpvR6WzyCcjQk+AAqkwqqKOrV/yEB8q8YLdQeMMnVM2NVXf3hIgWaE3IeXyGi5cxE8al9HPc1Vjq0dZNItJ/rfd5xR/6G1DZ+LG1LbetFjM64GHYHNALawUzSHC/c4Rb8NiWv1hkfMIvXzU0BWxwKgRxBuzuJ4TJnXU29naMQ0cXxMlhHm2UOJ4yitzANxklXuTi7dBUycM2R6HhW+vTApYTfDDudNUKErgPFA9JHh3mKaZZj28hI+UanJLZEXcFPk3xtAF2mk3ZTkWmvCJQdGRKMUq8310F53so79n1j6yn835pPTfzCcUrWvm3HoXiTGPwv51UqFEDErwpKHYVta/FkedeI874uUvirYqmzhugOULNtEwgQcIIyKuQ4izK3xfCLfVEoyOCvsVVqaE77oy9mCkhHkXWIFwezeHb98jw0hbzrGOuPoj/geiORbd6GJ7xrmy/4zZzeCdXUeZ8gaGnpO9L3BdETvOmOX72IZnUhRbHpBFWR3kRJfMCyVECGif4Mc5XsE2G7H1YLdMhBsYTitpkLbaYLY9fu2iSPdb0SQUTrlFJUUVW0axvlnA9zEGcc0yQbO2fNxsRshcqqOt3ThMu6KybG/rMbjuhhGKatNB+BScuGkVc/pzXfmc0t+IsDiRu5m6mjkb2fSuypvLoY3pPZNw7zLayj0ioyh7vyoms697JzOyljnMQ6Dptme6trF+tHEfDipPgtiLew7DJjOk0N58SW2x0BQLD0EXrC8ImT47Ob1wSctbgbVZz4rxKSHFbKYoLodRF3drTaswSpCE79IBGXTvEsIUc+G7Y6W3ijO0NgMfeQ5icr8OeLGS7I5jVbZmQjj9OEzoD5eJPRRJAQW8C9ahKaloym/agSqTBBxcYsb0eBJTigJ3UxB23ejlO3ac9qGfNw/i6RgC3yznHilNaRDy5Q9m8nm3HnfUHJMIe2UtQX4l3A/wt5A4irw9n37u7O3qtaY/CAoWkLvTeqlO5W0MyY3c9CDbr74je4kEwTNAXgpQGS3ixOcf66loAz3IgYOpnWEjWiKPpQoPaJ5VwgDzHMOYq0jRNvS+jlL4iDyI/TDeaem93ZoSE86kmyUXmNBSZEMONZEV2MGvqAFqordNdC1igY6xYuGSdcKjyP/sxoUbrqKZ6zUv3kcn4VyIdLCC94Y6nZAVFVsZIfZErLvsi6uTXAR1JV7LBvgYYWO4YBDU6qrLnInkWslGEKj7EtNES8oaA/+I4fpRej+igkY3wLUDsJc+7ISozdjRtyW7dgWsysHaGOlGAem9LeN/XVDGKxwlzH+ZUoslVybS16TasMlXYcvfU8rsDtBPiA/igFD2dOfJKIqAddVPKJckFH9rA7Ub/oVIVerxddGomfcl1uTrckIxNQq5+qbqx3EeBER/M7zDwsEsoQ/Iqo5LpSTAxPWsxzIiH1B4YGvaq4f1Sr2sRKoSSjDOg+BJOJtSxE1WyIn35SmSpJZ5qQHrjRo08AuhHkuq7DctAQTfJladtNLlyL27A9Bg/mpCpCjLYLUBIdpmRrsmNaLeTPc4IQvCdLmxpYXXZcQ5FWNuQ/5OCiFE7cJE2MHTT2znkejxQV9BLeUN7eG8/3WJ0am295qaWY3AqMEmWSmBr9XFtnGOgVnX6qBSgspw4yk6Jqm+lxpqV34lB/7F9BZEyF3+4Q6hbwNTQUWjrBam0rdgGsoh22sXN1jVyfMM0kN8zr5AEHzfgNSHP72s0W6w0dm3OcjSUYKajQpR5A2CSy5E4t7K6EpyxmPVFJDhBtECjAZBPBQibyW09/eRGUQUIBW5Ydm3iSNmNvPgLUg1P5ZbBJTW0du4hSzwWJvcbtXN7aqjm5LS7G49xWZ/Co6rST5ldKA161l3qir9ucGFoJWJ5JtN+06X8/R9zfAre1l831/y4CIB+YMncHtL1v9o0ThN4opTpGZ1RaLupVvPdT2kaF4uq7dpIXawQr5n9DKY3JdhNuQIS4FceyxH3OZMvFO0PZSys3smu11WKmd0hPqzzKY0fUjhCmReYbULy/jSdDbXvpAR6W5Oa+cJBHjO1CDF9cBRk7icG7tXu1667LGmLNX4rX3YsvCMZp1ecsi71ZnLWYw/Iune8KMJN3H2eYkmvRkXtxGTai7cBi/3yaIeeeWVBwWVD9x3hlPCi3U3owgsLywSIjiPIC1cP/E8JalXPg680bFTpm/lfKmFAgYQSm0WHaaXprYVVYu9sd0zcwlANSwYOim40rua4hICSxXUXp4COSfxj5brr6RsLhRD5OtWc4ejwdd4CXOla5lNj4V1sDb2gKJpRGF+2DFKKSNScfnvO2FK8c/Mb9KeENqOdYN9eAuIjTaAQQmZtjXmOCuI29U8XqGuvOI/tJ7gr0rwjRRaNj5eGdiuhgmdp6+M/7ltzSCpKVkP2pDT76bpeQitiKOUzAQVc9MQymFFkbOUA2lUgWuDPNhZCvaCuJKmzxFdJKAibY0ovTk9ID0Hy1ozfbviiMa2cBCYciVX10Eoz5TsCQcByWcfBQm4XjihuL+1++sM7vOou2R+WfgAx4xIU/a39nobmRq6nPBVO8FTn5E7mSXYEQ3p31lJ9NWfAa35X/D2/hfVAKYOhALHli3AziJDp3nDfa/AeYF7yU46pShJ7BFx8aik1s4a9is0ENjbCDhV8Qwc954+frKb2oLRwaVm0VSRWzyc44XR4HwH+MdcHwVcWjhmYQCZcoYR8rN8Fq4m3+PytY+Ug+sUTVM8OKRbAI6zK35HHdMxKIPD5U7cqgyICcaEiY+e7Y9cpKF6V3qae2oe3xxkjFMAs5HOG2FN4yPH6A/TvkG+O44L5eLiyx/lU7vFdVAJutGRK/gFHw3rSuKe+87Wi4YKobL0UL/8+3LPeO72FjwzYin98kcTXUxaUco91f2suoZLip5SJM36RjYRfefiinhZibPdVv4IlwwV/yyuMYCRDWOnYbiMdXJXgQJuhncP7M0SiuyRMY2H03K0ZmExa0Kuo+Wu7IQO8QV+RCwjG8okM2lsjMldODidQVx2c5/HfGSSUma6BoE5ErU8VpvaFcbWKgg8HkrhE9nfNwsKajBx23qv39FhQ0P9Lvlh+aesMrNtMX+pfINB09sL4KU7VfEWaLqspjfaSGUp6R3/DQQ25lQ5dzBAZ/eqnl45109xne1VNb1yPaQt24DhvXZmANODGQa7nCDP07DFNFrq6HT0DSYvcQPcAJCmYXnKsgPxxaVbHo4YghCJbZmMdE4dxuD6DDD3i5+dgf9t5D0r2nAsMeaPhnFNyTITziQCVjVU5Itj7qBG4435YMMeEf1KII8yKHFPS6iC09V6EAEnWnOLlKn4ZkEITflRvqbE2auOITkI4th2Upb4q0jbeNMupeySbNgwPbVeiI5WiP7mcWMdu7ZK9+wqIp3kCb7mY1UIxkBv3svgkdTx0JWGPxOjGKGHi4dZiHszuoEqI3Ex7qoZy/8kCOTNukDLUevOvAKMGg+GWb2lsDCfncwGePH91lXvapyC81vzuXZOdac1mpnwSuNT44rEkJ2Uy2wF8DADy+CO8IImZEcRRpDcR2/jHX/UOi/w1395DXpnG3uLeUN4Al7+3kbjppCPtT/5BaZEThkLL6YkFQYCsQ3YZwtTBlgO9fPmJm6aC36blqQZO8E2JDVCzecqpUSZGzFRUpO/Cn4P/KPNf3FULwuqrl+KaGrKhCgAVbBHWBheYg00o8ismtaT1xa3BZf05mDlSUgHX0KrcXCsyjfZucZGacNmMbn2AsEKegqZIo0ymRLYPagacn8TEh3ThDIvBYblyr5QAOkLg1kw53igy0jGnq4n4MonORwYORM36cz45YDaTqeig5TCOzDtDfYtW1B3iCwPiipXgD6CxErO0oYHX3F4c+v3LXFD295zuGsYBBbNxa6PB6RMy3LU5bcUYdRHycmFD6TTbOiiuaB0gK/SDZRIMID7cZbomOuI9YvIhViY9LqLwWplCD7mE5cP023Frh2MEkn6ay1rB5Kr84qtAjZrED0NKya4haJHiVgueerK1KVa6HEK9fJW5VoxeM44TcN0JysXZ39s7G/tbj31rUPCCcRoHLcEp4HzEwZbYuozRkaBfeF46sTMv0AvUACDauoz90pm0c89+CjHX1V4IE7UAQ2ETw80h5c9lQl9cbHWLC92dJp39cxG0vx5823aNPbthGIaLccwMVUA/tpPlWeuh2/RjOkbmanEO9+dD9eAq6gKnKKrxHPGgC1FFpIe8kxL1ig2Qnif0xSXFvtFXE+eBsXgDFNL/Av0IE7wvByNkFy4Ixrfzh8IfMi/0lLn4Gja4sPU2Y/RsO26EjkKenm+VIVwEHVrZuUuXBaxIwLFJogkWBa3SaSrCPo6r0uqn7vdMUDFX7XoVuexpEKqEsh3c+YdAbFtd6GNyyNs8IF4D5n1Mv2B80QpLilv0GZJOJj2r50hm7/30uTSTrqga/O7hJ3tL2r2wcEfbN4UrWbcdm0c8uLy1XLtqwPcZdFk+Eg7z4P7Gas2pAoCt7Qm/A58/PzFxWnuoCTVclIljG3CudlCF9w02uatz72NZHM2PVAkp+x99yTLo2XQificbJ0P5urUuhg/CD4IllEVIb7aYe6RS+UqnbOHeuFyS4WKVS8EpFB1U4SsOFdJzb56PJ2Hf+lE4tYkfh4sGTxakueZuu0zx3LFX/nSGbjnqW9BirFMyMdtk4tVrB2r175InmtrTdI9cJOZeQ4QU5mydDZqARCs5bdzl1BjhjwV32P4tHRlLlkC+W7M8+Mk3OCNLCdh16GH8E5Sqna3euXW7RZ6Mr0f3zwXevYp/vv4xft3YV8EPEv7O5s7OzvHu7kNokCW53dJKKwz7AZW+Trbx3Z/KG5t2jNYnTxeoYO6mfvPaJI35Pk1WXCfBM89Mc6xv5CgQCxa3fEMpV/SzjDLXxuGozg6KMWfEVhVcWNJBZa7U3zm7+tcLCozjRPuMJ31eSgIAsrBb6cD3+jXWXdsJssRC/aMvsbw4XJr0l6Zm6Iac5wMzUkTZZC2xuz+6Rs7ap64v7Ez6bjih1YcozckAtg3XEbThgbbEq42jaJNa0PGpiwQ7ubmzN7FjLNv62ErY4Nzgy40fa0LYJWJhjEw1lC8xpz4MVG7XwYSga5dRL5nImeEgl6GAjuLSgETtjjj5oVY5JT2Iqptk14U2fyyG/V7juGfGIswA6LLEQrDGVxVWpD8KNN7MYcHyl1NPETwXYfiUIUzF5ikw4/GPCkZahHDAu5K5SSti2Up3l5c1p7aVrV03Q4XfdOKwEQxJM0Ys3xU67VTCNde3TyTieLch5jN7WIyQW3pWPYcjQWCuHqVliN405Lkd+sC9Jr2IrZnWJR8esfW03qZZIQZGWiyusgwgS88Lm3JhNtdorEkyynzoPeoeLvLcBLONVPrFwOUCo67BjJtw3EuKV30oyAaYCOe5Z3kMde6guG20T+ZjSDoLbLktq8KHQSp1YScJ4plYgTxM2wEhlvoHhSifZFiinUbWui+L6NYkRVngDHONqQoPz6MCR3wMIMkrYtsLK+TUfK8aNpSuz5L3zP6r83eYlp+njGM/PLq/OTk9clPZ6+v3rz45/fP317iv7GVT9gm5/HWbt6JijnIusIP1PVPpYBrYxFDFmMevK1Oa2dFLX6k1nIrt6x3x2q7L45+Sqdv0Gd0Zi+TMxs8u5MI7sRLSsons+uzzPEx8P981jCnDGP2F+Uw7+CMzTdCXpuRzajH3HMyj1BU58idGVVYXNE2K6ZlTn9ecvbmNuj9nBFi+rhEf0SRDOG46XhSvtl9yDIXqXBhrnFNFrRAcyOKH2RvGz6TKJqKEfUzrkYe3F1whISabWggPihExltxP7yChzm2NTJcO54jO9CaGDXBkJNGYxLyNmk7b+4o/0pw8XN32lrqzkQAo29G1Q7dDWOXVh3kXKRAmLfFFITWH8srRrNF2QYY+zl8ob2j5M73iLe1PQ1cuwLEZE6EXduVS85NdBzVnTkNSP7P5EZ+tOeKk+CDy2hjzuzfC2OJWLqbaFrIEdvky5+nNPbRiG4hGqZ1nNcb1ZUyDticKSWLhJf0Z3ZPgCTu2wUnNNlq979gpDK8qaOAbzPYjscJtsMWBqoXFriYMfaLzSxVEdroUyAQe5k1wZG5YxIvBQqJ1tBoMDHaOKyJXVKSWgUlqB6jWI1KdWs01Ygbmt8oDm07N50ofFXsMGBX6yJJfDmylOCL+JTbelpu9U6QmomskCA04yBZ8rymHGiep7xz42TnZ3nugwfysZ0hd5Hb+JiYh88v85xSEsJx8WT9utwEjJldBYmaHM2pSRwWQZjPocVyj2DXCedAOAYdkJWu0MN0g8apqMggHNrcI1y0P8HvdkpBjKiIGbesrMGTMbiDOcM3STVEkuhgfhFK+zPxCtydrlRKkMtxjiALlqyUVhjs5V3V+uhTq6U2qJJtmyI864tp8Qkse3zWlgBgu3SJw/EhbqIMJZfaGTY85UhZvfcaRcRcdHsJnuQkygxxEAj7L8nL7ihul/+aad3RDf10T3ldg5WHDgO2GvFOsi05FBhiwuSZuJbaPSGrQ0Q+BTh2Blhrn/Xx4jYnvDvb6Gp8SPtHaTbS9UDvIP7NmGYx2v/nZNW6F/vcszwG7PfHvZvyHnl1r3t/WFCaapO4cWENt3rvZ/jog53/AmqVCj5r5F5obkuuWDT3/iKiKBRrC4+WY9+y9Ts5CkNMAFlY7Fv38qRhrEryhiqVD9dNfMY7ojBqJTCBZZgOlynDtZ6WDtfpT497jXo2YBW6TPydEOG/gzRQCYqD46zqx5WLN+tvlaA09khd0W1uNvJqZ1MpBHbvEmRX1YdAPBTVYYXpKBfbIFOdWmB0Wb5dMKtiTJFKRlvlfnzBDJxvheSTWfxOEZLYQ4IPK93r8BC95fDpkuib9PeF6Z2dv8HwkCsDljiYtxm8t+zjgT4lrzqfuzFSnVvumQba6voBG8obXyW9ivzmg83OJEZZFNgX42wzvA1aW8e4Gk9vrl/PyLzDJL0jEypHDtVURGHiAGMW1cIkGRm7e4hX+uTnGwvESzCu89BDy8xkTOCLEiQEfkl6jCnthW3PpnxpH8sH1WaWYXOIonI8LkdXZMBdqZGsCs8WEvtRX1CkTsvSuTKLy9XpsPGaKDTd49m7CtV51//tX//nqZyzf/vX/yXIL2riuOTQ4zZpnKe4RtNDq4L5mQISppMs91IFQErEH2YIQTuMRwHUKDg3HRE8OjeFR1NKT5RUNpJDmZOB0Vqn5/Ec8/SXxAmL3SBa1l4HXfD1wyRkZobUjuiGrJdOnByVn2Da6vZRvlzPBdWqsgLu3BeuP0I3emR43WK9mEekdC9CRuCocBHXjupzMEy/bBGlDK2gNKTm6334vTM/f5gEAVcw0+xt7W99zs5kxdorzhCXeyb1wStW/JWKy2E9Xkym+bhWStKWHUuDjdTB0om8fPVfQkvt4lDVH1UNZx/yUiq7wFgjoitXnkigXCcwUFKSq7LKprAst5edR+jtlPA/iIOf36ExHkt4569002XaE3lnzPmQ6mcpJI7ydCl3W+czErMw5Nre0OoEtie0CMHXIHD5KFiEdWPxP5m6h44GlzgRxJ6DTEUhhrIVBYNpz6XdLpn/wLWqQemUjyHikM5ua2GxyCgL0b9qugpmW2zDkDksik0Lx3nkK+cfr0WXOWqjzvo8vR1wyNK4JY56kikfK7zyCeIvDkCvdmjnEegocbDpTosd0eyq7UMeVEHEPWgCQdfBLErCwfpZHqMrvpV0K82MubKSyjFmWjcyl9hEJ9Hsp5SOG0JyyyM/XEPZ9DZuXF+gJGiwqa6+aYg4Lm4fJTMwrqyHQ0QYzanPqJm+xmSv5E+oOhtpPMS04cjBc4VLhMqrOY2MxjPiut03zal7tOLpXB6azMFINMxSkFdEX914RO/q/og9IuTvmv4w2URh0+/VAu/OuF04mw/meDPbAj2ErT2ZYQg3kEvgu0vqLeCQd9BVMnPjQg2llWC31C3ggf8qWqMGYd9UcAymymihKnJZgboXQcY9/+eydPkbPVIY24anYnD7I3zsvG4MU/qmw1MK/brQyGZcBMNxfD7DS+Vh7/egBbnMKE40qLgJeyKZwG2+LajXY6ynpKdxd5pIb/DK7mp22r+cAGcmIq3xoFy/ha4IzVgc7/LZXfgv4YSDF6DSYAIoOZrFBOLWceKtB1nPFSfhBFFELLhyjKZEaRtFQ5KK3/yiGCfd+6RcWEt59c1Q32gtyVTPdfl5RsWuLTN36BHn8wV3vg1PZagffAKluV1MxI1qinvuOyW8WanSOVjm/3g4eVrbyTBVjTj69+0WGODCGivGeLFRSCcxWijaLg1I86fgwt1gvww2x4QQRlURmnZiCJJQ+iIYr8MNIZNNPH+Ot3EhrgjPsAtMrGM6FuL/+lHY+OIMPUrff/vX/9uyDibug/RlA1U9ltinM8YzHnOH2rI5Rk7fV0PZCtM2L7cVnT6RKyZw2TdfAU8ZyqpVttZM7D6+iHBaWjzX1w94UjgYeVvLZl6Pa92LJffRB495mfzhoJp4BZKpD1hpz56xOz1pxpHyY7lrlHB3DNGuqiW/m8vtWvZ/n4QKBu2kr+8yoOsJxxMRueAqCBHvQBuTdlNTGN3yLresXdFCAscT/rfIWMRxxOfEXy5rwgaecRfrbSLkUlBKrg+Sdi1k9Imr7wkkk0epmIe8dxhfX41jEzdyudNSqe4wJvthyYtIL0lL49JiOlJbfybpSttULPRF8xf7rLPppzvzUq1r7L21hwpb8xgijqRh+aocNwWb2IQQWSdK8MVnwp7yshBG2paT+mY3anQ4k2Ord+F+YoIjPFzXDFKwU8gtrxaAJ+r30Q/+qkSK1nnfy7uw6LWdN3BxkaIF+wnPxMj1FIlr1ivm4TD4wNfCZZttr+u3Svg5WWAUwcnrbaRF648QrQMKMf/MF7n+MtggzBQd06egBQPCdlR7zAMhbMCypBHdu+5UJl2RCiE2exH8aGD7uEeDFSWrjgHyV+P51729rb2t/5p7Xmw7rYx27tlo5wnC3pQFfHlTEI45kYNF7fZSVNi5CxG26c+DNoIMSi0+Yps7gtZ1nbAthTWbpyihBfEbJOg2THtvGv/9lstyrVPvo6DEfQqLBIdAkg/kiiecoEiRlyJQuR+8WttL2dMxjKcA7Vgga3pEzGtuapZAoWbmteMkjXg6Xb00j3vP8EdOhIpbroHGEArldWYHKauxsqQhG+ZgY6eeo1yrOh/knN/wsiUPT89RAqH3LmCmy3I3fUnmAaJJnenfG9WlYeIY1rOHqCIpLCAKbmq2d5KLrr7y7iIdBwfMpbB+G+q4LKzEB61l2kGNS64VrynXxu6Fgm5xNSuFKZGKIgby89cEqDwhNldy+Fzz0ID2Tcxbe4eJkhfLO6iHKD3hxgTnFq0CWLLg62D49HwKmoy0p2JqA3DrOvQh4fac4lZLdjCKfud9x46g0nPumdqlUvTGqJD79le3IPwN/ikGTp9gwMn1fDaJY/a55UJecWMNV50UyzdSE50Q+2WPoVjHFQq5K5QD7tSCuF3+bThlaBL//Cm5I+jYLKqAzMIYspKwGzUFKwK0dTRzB7vgGPx4tS/QUejgKsPcVHixvDKRIuPaywf3Ot33LrTxNETV2ZM+XxEgvXaNtl0H2NM60L+gBrFkWxBWAuVklJXEAeGCeqwYyoUMzjqlAaORNHrF1l2mo3AqhU+Z1qL3koP+aQYlVU8nHgWH8gP5Sr3jHLb7Wu4jBGI7doAvkkYV0e5FfQhsBZ3j/tNkz1Zq1zhLhmq2dV9WctLK97F12f/n//3/YolEpkQVwCbXBO6k82rjkHqMC9eZOkSRhrbd94K0kdzJcFCHXsB1clhd6S5VtjhxdYtcjCipNbls6gU2o72ra1aWme6Vks43Jjkeyrf5VvEBvrCeZViXQwt799ce+eB/6xQJUtEvsGSSQyKRNqGfic/iTxPsbbAjptjirpZwZlK1TAfGAhe6CzbtiY/Oezdks3YtvpFEjQxTSTCmiEwqy6TI3EJbKw4jugQDiqoJBv6Je2V///6Mvv86zIPXMb2afi+J1LRYBoQ+OBfsoMucGOH5vGByxzTLiN0aXB4fxOM0qDvuWDL2g05fv7q6xAqh42OnH670AVe0ihvFp7oa2e4U3JHSzPoCC8VYftuCTOmFTVGmTkP3eTlDAm9OBfE1WtINs4lYLndt70HsbGG0lKdXx1d1xqMHeFJ5hulCiRgrggoz2APEhpE8z2vuL0G2hzcUqVeUz2UxyVL6lkrlB9JwOC4LTPf1CCclVhz8neQM/G+Li2kawcAL4KdoJqBvHIShBd0QCpXX4roxwg3zjPdNhV3XvbuDfECzyXjAmBDuk0op3/Iz6sbKqMBXCMyHCTyXHXTeBU5K4shsbORNWdtz3jtASd03M4m6uu6S/eC2TNxLUXaxeIliGr6vNobclfCnyvZHCWE0H6gBzN0MFgUbk/1hQZ0SkHopDx4RKAN9i/x1U0ISBoTM9KJAatxzqEdrS33V3ZIG0TfCWmcQ7Uy0UEi2vNaiBpfHHtVGH6thF6C6zSzMEsMZEzAx1wYEEd9Ei5NVq7LpDnSDwjJdMr3LrXa+Re8VFTp45OxiGnItiJEUu4Kv4Z41rko49F+47QLdejhfzCLfaVT6IWHB5I8DFYy4PXn6h3gttoJxSNzqbOhS1+IZymzkLuAeuaEZDoFhXHJq4DNja0tPcwbCuiZPraQ7u1PhzsGXJofMCPJ/lNNHWr6GwISOdFt3RcVvFxWcP4qmhzWrsiEUE42vLrzRKYpRtJSnmvyWlfhwenXB2d+rHwqs8m6Oj0FjbjwKluFl2h4k6DZAXA6cDIGHoAyWFQtGccHvKgbkOdouVoL+pMOprMb0SVPegFpyrW6jjQfDA6Zwgqdez07hmkaUvmQU2RDNhNQ4N1kSaceYu3amHIHayOWDSmruJIFZU+JKoGQSFL4bGfynSWGdee2KL4QXi2KtgcQ7pnYCDNNk8wR2tQiaSWg5FiyEwqiiIRANCF+Ep1DiefLA+4lPrbMlZ+xlw+fuL1Sbx4GksGtOsP30tHNXFUCkt3eEU1DWzg4o+ppezPrboGaxKrFjo84wyCkyY1RiEUkUTFoFET0mEwMLpywcBVeT3roOoeYSGBXcdOAE0Ad8mLuzzr7TsTBcCIaZPYo7cAnC8lr3bW1ClmOBzjMgqPnktVB+w5VbO03WBB7YS+LLL+Z3cfW9tK8pJ+JebX1+FJ8h98oNQQtNGMiRCge0KbH6jwf8PjYP+EohvChTaXPce35ts07ScHwFMCMsnfPCPho6l1koM8kMy5oZRJXiqZYuZWdcJYJyaKy5uiEBC5qNeibka95uFHFm2yDDbiM0Dv5nK3qsT+W6m8XFmgbBUo5SaAfiXxgPFYSUvUMXViNGjeXc3DYiFz09Q7/0Op9raz4XdUpyYi7PquF95zD5xAb8qKNNUaKCIwdZdKrlyId9ycQA8IoKD26VYsu9sU+A5KwSk95ZmUmeEmWxRMtuxO1Djv3Egg2rFoZ3dYs3KXRZMg/Q08GVZHooXAZXtm/bs1+kfpIJfuwl4b6sr5OPvcnQ1TxtGfQz8r97WztbezuZKa5O+C5mMHYmLmWM9vCoCDzm2FUsBDTAM8sltMYYCde41x1+1DREFDMnUzIwSNX2Nnx7vvj6vmA7nO6jqW5NQuliRSBACTvgsuNmqtjyRTfH7IxJEz8GeKJHH4Z6V/cvYBMt8sM5gYrgJsaVGMa8bEh5ieAKIjdE9B1mLNVHtAHMuokn+N5lTbASWc6441K1YniIfB5oL+DBGVdOqTF3a36EWE5y2WtTYn1iaxo04jiO/a02SxufIXM5kmC9wX1FdtpGBZ4PB8KID0FX91Hv2YvLk/R4dnbaQ3DAGVjvrzBIh1HDjVlH6ztzZt8ujTfaq0A858ZDWWq/uqNFmHvC7VO7vjaCtpdzlLZ36N9VwhEifV1zkv84pwV9waC7PNmf2mLLW26w4vIh7KIEGZFWOP4e6EJQf79WSj1nGWwDP+Ni/kCEGnoGiWNIqaDw5U6a4roaxr9Fiww7YNoYeUBvx8599pEpO4eqNNY9XDQoXi11wwuDUBZhaIY1sPexo7rRIJR++JwaGxJlaXoFdpJLACqSe0qbnia//+0C0y6u3dIYdP5GwkBKUwqcOsegTEd9tDo02nEScGMlBXRq+PHRAXGlzXVnApXHs1AcgYni5LTNuCakzjqkskRg2hI5Mx8laEsZkRRIO6untrOxsQ8jFrQE6iBlD+PqdupyAGBwMe3SNrbUQPNLkj8xJcdKfAIvxPPk5IXHblnxQHD2qhTiwqFjQ+SSz2waNI/f2TMOKXOIwzEKSaqUWBi91/wAk79J2GfelW09ZhmzpBM0aMAWnNXrogmQFqT4jZZzbEndy0s8cBhkGRJocSPqvevJlemwJj/PX6CzhN+7t2FMIpJtQd9zydVS5RBeukc27W2+WIqEpBpO0/JRXtBImBe+8O4YWyIR1D5xqdY/fO/ERInAz2zmb2hEsVCIAYK23QMwDSKGJiHiHkWLjiJFunKwoxrAPigLtfFrZkF75C45QhIdYlJ7tpkFGA85xVRxZaLB0MJ1RRzC3axHhfj6gzMKftw8qByAWX2cYjJI6+CodBejOvBOFejSBRxzx0mFZfmd3AG9DQtH9+hwDN3hX+AxmfY+SW+fRqGYmPjEf2OK4WZc3+dtl7JoH7JVAP7rBpUSZfdTmEIeBRBPey+DUIWbAQqEeh9SAMzip7LFpMGlIsKGmcektTY1MRbGCyZcCNOUpeXEUFIXocVoe4U/72FZOv6Uw9mhC4E5QzcJzQIQ2DMqP7pAMnSDR2393UaqNmLD1ysdvqn43MXvi89uANZWvaCWKtrogPO+oTZ3zgbsbdzN57Pj7e37+/ut+7qWSk6QrNt677ev6TfbRpadIZ9jVEVhle+LVDXR5Dm+VdjSsIX07cHzmuNxp8Nh/CmufkEDQbyvMN2dO2+xg0wbUCBBn1poAh2wZKbesDVCQ2jI4XLiEBsjVEIkPWoQGY9UKdAJoDPlk1nGcdWuYgGkRTvhBrDdxCJk0lz8JZZNNlNMPKEbHDJhhDcwwTDtZTBMAoKZZkIQSMx7mDZa2Euu8btSOJWDrttTE1TkbI7S+HrkD928kBE0LAs68WWhmDUcKdJFfBtv5jA7upgIEW9JNUeLwhxibR0YA5cwG8wbnXo5hGEiN8r1/7Mk4XQkbTorYBs0zpJx6QvDkU/YOFGWXVT7zw2gUAhqfYTT19ZEbx/ArP1gCDwJsPCmvYdAKIM2AcXEBh4ssMAnplgWpSj/vPOE8HBchmnxyZWvVEEI2EVFQxmqPrIqoNNYapqzGouAcyVwcLT7mj3UvJLDiOeVjojiiQ2vGPa3uWQ+KRrCeGP4to+4manFVxGNMY/j0YiZ6zor1kaw8IEclhiLFIlSlCoDwn2F9up50bagaF2b7tSf9jAtcYl9uMVe55zFcNybYSLaViVI/w4TS4Jz755+Cx4nspnOmRgVwX1tfmCSvnGNg8tgIVSssfQJe7nzeuZhWMdMmv6bi+3TiwuW5zRW0FfT7FhseOE6heIXJxfSN+RPkS1p5B+SuYema4cqdPuB/QioQI/2hLvk2kNBcsEZCYRJ0Zl9OO2FNT944tTixu6KUZ5KbLPwBuoTXLJWyhTa3BNOFW819LlQt6C7CcsIC4aJVorpdTLssylSxujKy+KjoAXn5JUyY7wcs5aWN+3Mx6wRSXEkOUBEZsgqMeqhsMxYMCestTmClZrarUBm+/I9/cJf0j0XmCHH0lqGl1ELDEPU7DcvICaK7A70EYT0NKFy5esGSiI87WwITSwRpwMKRfBE0XI+Z+jQIlQHDdM2oyiEry0/EUO0O3PEWiv4XxPUwuO2FR1DOIXuYSEfE/GHk3EFF0IrsEU1J1bUblpdTLDEuKewCDoCeCpxpCPxsYI3eE1xppPGLC5xFppZ2d/KGsHuDTTTtPrnJGNoiMbd/5hMWir6CGLmcSQB7WpEpaN2tjeCqZ1KUi8uRnAVAfzsraRFlny6MS62L9S+YXZ05QSc1tPBEkAYvu5M87IOAuHLXTH9yebVHbWRitguaDIxu4Y+zt8uX1RbtPfleBxfVy60VtrPLHd3Z0/OaCzc2bSICL1on1reuNFOiigXH6UDtLZdNeJXFmNtGm2jz53jvCsR7ZrpCUYy7Jgjff6PVYD6iDd/kwXfdQmCC1NtRVuVTSwS3Gk3bUT4hIoRlRdFAkhjyaE++/oSyQiDjiYnhEy2tAWxr5FDo8RqgAONqAI4xBnd3uz3hW+Ja2WkgNzlvKKurqL7urSPq9Xh9JTrXZ+aNEnvLw7zYW+Foik74iniI6ksSHqW3HPM+JrLalxIPM7jpluSsF+QQr8rx7Obxbg3qodESSPbQOH2Ntb+E7Ak2GCwfcLNa4SRyvjns2Q9luyaPZFlSOBYGRBx7MDJQUunp9rVhIE7jEr4JOm+I9W/1imn0YhAMBPX2835MeUozrF2HLWMB27eaEloqEyM1Rxsir/4mwtPk5+Q7MVfli7Ot8SX6L65Ma1dz3H1yB7xrYwHeD8btCDq2+1TVGn8bw3WbSjN0nicM2WTpIHvY36fJhos/WJTzgRaZOdkNinjuvjx3xEnK6Wg52DO6aDv372KJ+Q1jPKcx/Qk6rYTSiObp7VYw4aFODxJNyeTUMsN4HbmC37KtjjVGweUjjwVM1D4AgYh6RS7pGPTHj7xyu8kzbpfUvYB9F7pG8bT1/tft8dHx33xrPG/Do/7B9jWFP/9+Lg/J9Zp+eBwC24e/vvguI9KDP+5t3vc39vZfTrYeTzY3es9Pd5/Wkx637++xA/3j/uLZkwPgX9i8Lc93t5Wxod2C+MByCbVbtXN7TZHG7ZNye7Ahjy2aF5bf6xmOOAuDHjTlOUAbK77Gfy8/3V1vIsT2oMP4LATyin5xo78FGPSzbQYD5DvDMfbOe7j/zw57uMmYCaF3g7etGs+sigE5pRFuR0v6Ge7sChNSb/CdoA6td1D/soVnaQrTDfLMzEfNgcXkn78OPgWRXDp7/CAD6dX56/ef3/25url2asX9FfYPqrCuzLmMa04zOEVxrcQTHm9wDRfEEBCQRcJTLqJaTvRrf7X/wJLQe8KtvD8AaHgbbQWBc7kf9Aq6OLtPz3uf8+/wGbCYPR+086K6XcWJvTNNv1J1g89ZVr25c/iBY7ed/cI1vGEGRCtXxTMYKv3E6yG6niF5Et314K4KfBCPjCxtSlv8bj4iuzmDCB5q/empjAOBWQUvaq2Ir3HJjmsFtG2hVOHdeLs9rjm13+yA8dRwhUiRr+522ccOfzX/nf/SHf7H/+h95+MWtZeDvT4j1jq0UjTI3I9tThODVWHMxHqDwaBqWNroMRsgf4jiwxxR+2sWM3mJia9331ltF9Glsgl1qJwREqFev8NHp/eiRZgvsRPgynA/yxZDMMATsNdFMIbM9TaV/yw6YeLos6qa1mnHK+cQNHn8yPekXzg8b8pJ99tTepvtuF/pSkYvMj7jw1sflVYkgXK9N6XEkZHSwu5k7gbDywNDrT4SONkFnx/a/dozXOAs3r+rNeC+JgUhmTN9n4MryUmaaIXFL8+PPpMkELMCV8x/YiklzillZ829435Bha4nt6awyJ/+Md/cO8A/8fGXRQfN1R4KubeieXnGUozpJMsWtaK93CGm1t6NWxLnOdtcKeHHqnz4aXsnIypZe7uS3mxaD6VD4hsGLydZhfhMFyE6KH8SFl5LiuSLqnY1EkIjWM8gvxAO6jRFEemtnazo1caGffTsnVNl3FdB6dU9MzhqIEavvFbHCx9i+A9rJtBbauxns/BGaoEdd/mD8++nHn5T/ayvpl99821Pv76u2+2Z3bj6PFEk0AitwdGj0A67yowh5M2E6QENukQDYtpxAPCIK1Qjrga+lcsR9idexTeIE2h3sQwec3FmXAf78v7aUU9YuDCNrDVYJHll2R33aPksd8OFOsTbixzbfBty56qt5R1nFRTmPa1lstkp7MT7NDT4L+euP/KT5c+zsxZwFXMAiK1xq3Y7SOBOdDuHgs3Qxhw1BaL8gP7Xkj9Q6BLRq98nqsz7n4E7vr4IY0eRK/Nspi6nIlVTeqlQGgO3US3aJzv7GAx2MJzS937XMfpTFIc3/G3i+qPcjfpRxdSRejshq3JiNWPAqkYxKTEu6pd8fUYZY63knXDlr6ViGp8z7FBv1HlOLUopaZFzRbClVj+0doEK8OS7kPdfGyN2GYqE/nKQXBI+IpbCAlSpwidDQ4AtlFdgeJq+70N6TTLTjqqnkc6zF7HczEdq2e18yu7Nq72XjlOMBRzzgI4AJyIzKRfSvRHHp8y1oeKdnfr6dbnLdw9ac1DR8QNstsxCOU7ZCQeSOQofihRDjfK+mOEP+x6OGG8grfgQJvsgdapqhG5JR6oLKY3STxwk4ErX2EC5yOa2l9xqDvri5IBvK4vuvtUfdE9cEd74OcdPjne3w890YND74niySbX85/UJ/32yZMne7+SOXzrnp71Nnd+cW9zldvzi7ib8FeqKl3ma8I+fP+Sdnupj/l4x/iY3k5kxRv7GS32Tgi9MXYsj8j/nYPSG+AvB0erXcsDePAp/4aG6h2t71uuelzeuzzcyXqX0SRu2Mk888BdV3/nPUZeHkZFmuLyTfVD+fN6yqCR05dHqRk+8QIs60Hu7jxVB3JnDQeSbz0JtVD/qNqIXpOVS2CfdNzenZ9/ew+OwQs+PIhu75Olt3d/9/Dx46dPzQXe+Zte4NVn628aMYKZfziHA3T144t3F2dv3yy9zPRKeB/hV9eL0eiBInNrhHng6c/w+5yRXPseLntM9g4ePXZXsFlMualbGNNq6yEiRaflnAhPkH1sk2+KmSBeps6Lc7C7s6c3J9/x1OrGN/XiU1ksXDvfJKdjv0xTeN4gNyYVeCfIMdQ28TMlQdAOC+4fRWYR8xuEGfE1n8w3N/fktHXZazX/vfWf/CaP7SRXlVsP+iZmOBNM/Tig/KypULzUvUNL6qQOLUkjcloELa8E5bk+DruZVo3BQJnWqt1TtZULXL48BneFEmbkt3EpEr7P786bmoM+ZJDlphUjN96nyJGuGf5YNtc1uBbMWsYcVOjSPWjaP950Guh38H9PfYFD+jJWfzEuPYdqHju60PAXk5Koc3ixqKa57jFj2KRqF202O5o2GQwcPWeLZxlJZ7LEUWJH32Yx017pJFdBgX1CEAIXCJCvyKylpplQBqGnu6Lt4JFNLPceWRpdOhcEneICndalN/XM08UODx743qUgIIgl1sI/ed7XsyvwEuLfsQgAz7S1ixMzs7qsOn1K6ExCdsJK1Z4I8romYhJ2Jlb+2DZDIP4iif7Bazfc64lKGJlaKT6aJggspDr1zQ3KAnPCcwseCxgHpHaXxp4597hn5/5u2tNL0WSWv1r5HCGLlo1kgNQabBgyzCtevO7jLPcnzAObmKeFk8nkMJhQL+azRVJwmcIZxZFucd4BLNJ94EGsmIgY1Ziq8F9z5CjH/m+vkvdwPFZK9Gn0a3xPJfaF6+cpUOWySiynE+TxxhIn8bGZ11mclUNcOuyU2ZK6npc5qJI2JKx8ExSKOpm3CWraMwQVdoaSx+cNQyYeFDkOK0b0vs98oxP4f2xN6rCpZ6jq4DKlkqqXYJ+fOdOpp6eTsskd3Z5e86njRLL0FVDNp+AzKvEz2gdXRn/Av+fbos/zLEGF4QgKEZCNFJJM6cIrQCqDb0wLZ8SAgXMMpliuSeryI1q1YitwSVYNluLQcDL95oJXPCw41eNcLC0EuwC78h7bsbslFNATdZ0RbN8z6fgjDCgrpoqbZtmSHMdTvIWtEvvZaZM0dmfFsR8ZlHLEbuEmDmKsLCbcUckbGCU2/6ZUZ1NSkxEptoVvaBYNhGhd3xrpWqIw3up9KDmMTstezamiRALQRZX8COPOPewFNe+8czDdxURa3HKRHTHnsas0pwdqPndc19zCg1CUFJOdcOyfO52WUt3PKGJ48TGT65fav6TKQm87eR3So3kSgGy2bVUslw1Q8BE0GIheu7fn/CmfonBvpYGLzUp0CFuLEEzAN8GcD+2cXyX4mnACwoUmR8CJV43Pq/RwpDdFa9gCo0qaCaKQMOeLihDT49PiU3WrEUTHhWeBdV2r3/KrJLViu1sp8kjg9bbnl3GPVhrxKamBn4PfCpzrE/Eue2/EBQ352Hge6R0IAjxEk8edDLz3Jq4rX9tM1ZGP86ycZAYulAjfZx1OH34305Or67tZGNJscQ2XQvMmW738NBPuoZ3Mvj4vP4HjPkOpKApyUySpi6TYKvPKMZI7wvmyHLGEKscI+f3floOj9jGk9fgLoVFPBrs78P/3nhzv5aFRO2tDozriJVsohf62uKjlwZu/WZx6d8fGnpaGtw6fmFh1s5guixy1cdCII9Uwo2J4M+kISSP8SEErgjN+KZ7VmiExP34+Bo35Aco5/tu//k9cDWwMKHk+Bg+DGmXIeoi18Q62sE/jhfhUjTj/r1QjFHyWnFv+RfIRs4PH+xoxc7AgE1s+5wmSW67DL6aIooL70+ay5gGsntvKosqG590qMKlwTV9YtyuRv7wLJnMrItqKcS0EcctmWX1NBR0MlBNKFm4LQ8JEcPK2afDt5PRl7/sFrDoYb7e9Z+N6+DGBEwntLKpN/PpbKYZgq8DxJAmBSD32ZAu9DS5O9lztXT1ddExUyI8yr7KXvEqEKuMHgTkt3Du85FVpe9K5DkyrwRDDhAHYsQHBHbiaFA/X5RU2hz5/e3G5kUVXWAjOYjIuFvNWOKFMI97cDwkwgKtxV80sdw9CmnSh88VnYa0ngvkUfEBt98KHnSrXLWU/idhNDHPNgeKdvIIdv6L7daWu4vExTYrF3JUg8kxfbEH2MOqqGIdP9aWE37953/v+/BX4cMNy2lKBRplbjoB7CF7fHkGy7jaqORn1iI+4JlJxrXkioZLirdYAw9C9roeEPR2FxKXCqKycEJnDumsOKz0BUZ04Q+SjkQCZ2UDEGWLqbUhR1LBfhw0IEY81R9ICNMy5qclEGlt0/WCR4NX5WdS8yuc4tYRUUzT83asfzp6/uHp59uLV86uTN2/eXp5cnr19c0F7WddUszpvFmXmbXcUMeHfVyjUSBuSo3VDu+biE8TrW03BnGiDN8lWMcQiJ/MLu3OqMnrFyL12dtb78ax5LI2NcrsCEJGcBa3rWXaUvfwona+eHSQ5MOlr0RHEkUSjqjecGy8/moaQuDCilIBpTDFL3IQMbFE+RG6xVxNB0NbW1leOKBHDopndIDJo1+wurPmqZSRzlnl8JAikHedYKzj806/mPs7gmCyC57Ew4fe4o2iPqjTwmYJv/kAMRNjck2Jg+BWfrKLYe+DVn4ohAiIPLh4Ixev6czCeRkTRGHDcBBgtxAJye2kz9/UkYHTyMsHKASk9lFUbBCr7Zlx+JsDkjcUe/ydJidDOfPjp4uzDT98bQGNKSRONGq2aO0/qJ5nzxNPlEKk9C2zZmA6D0gPWFum5MzYVLiYZs0gWpUtEMgWEnGG/Rmj/02l2n5EHT5/aB+mEHYSa34b5lkeLRmU8B4NZBEWTt8MJXwETjaGGHjAyojXr4Eq62tJzbodZHjcaXiA3RdfT2PH/sT0pTFUVn9zAXpAWKJw6cTeXfraN2+B/WXiCNHfVDdMTOEB2Zo4cyx0pZJWLhUm4/0Kyh3pZFpbewxRRBtcCDqA1nOVBG5wr2/QW3Sb3CgVR9AisTztjIvTHfksdXPwzs97mgHsg6I5AJwPh6Wx2fJH2YTh1e8nESQtBy1r6O3d6aAtwln3sktj3jXmDGTyOn3rm8fD2kg7HaNyYQFAwymGXIqkmzNuA0X9TwQrny1zl3IAdw3mL24oCbe1TYYuAG7tsmqD2AcdUa3weVzc3jBHlLCdffA9NTX8yKT5Xk8UE3GJmBh9jvMXqS9taLzyp6bsmRoxLM/p+Yg1VBZbawZVOGrUFkF6qntNpA/ZpgEY3rECD5a7up57i6FHGkOFVcwWvBMmVZs3eAs5sWod39711PNN3TkygIusfglpQg1n+D35X+pGA4a9UzQRqxtdsvXStiUMOmVSqm0CkV2Hr00AAWta7VfuWGGCvGYaNZAJkwvHBcu9ELZXRL7AakTnaJ8I6gJF2BZH1zpjvHxMO/oi23CoF059SO91GbtmmcFd5J+p3r85evry6eH9+/vbdpevpuSi5+5X0viLwsvWEzJsml1up52zlpDlOjrvXCt/Ac/Q3GJUcqpfj3jl+6y3t2ibXs7yqph83e++MltnsXYo6wL/z/qyBtjv4OWi7x4O9x73dPQyQ7T39IrTd7s7To8MnFm138LdE29lw2d8s4gjTg7O2HBX7OI+KZb8UwdObpnha5fiUS7QdjpoijjhRht+PxwNUMs28s9Tyw3kP009n9K21Y47ZJ+Thd/ArGTxkIhNTntpYSgxtC06ta9xnp/Urht794z9kUatUwylMZcFt7AW1TlT0UcwqrVYCmQPrQPkWmIRqCEwE7UrVD5diUCN0cdIoVUsMCCyV1GrkFqdfoYT4ijblKzKPvkrExV4qLnR+681tJzOkmko595SKO2gVye0lo01/daSFUeqwfWZr2RmK0hAP204FJiqpVVwXHrm3gWehqAZHTzZ7+s/dR1hipY9KLKlUKfpZEuOoZ4P3T854tcbTy66MtZXC8jxuu8BBF/3fq/vhZHzl7KGr+ubKk8CgFfuxfIgYFbNP7bBaKG/cLGZzMf6aeb7JTpZSNjEE97qjJbgq3GuFgTTIyQ/aSM4AJc7yS/lGnGYybkgf31d/RPNZ0AagOyvX2yU6wO+n1R9Ab/5zSfEu+S/hJK6cL0LFOLlX2TNnOH96mZ2P72bAIUI+E8FbGa+nVo6nU5Jn7HY8Q6skFcoTip/iGomPdAyO40iCYl7PyTNuStc9kyp2/ZXdyWrdPVWuX6Z1Dwc7R72dp5jB2n0cad2jpVr3aG9nb2/XKV33/L+N0u3QF1b/wkKw7OvQwDvdGlie2aV89eOVEPZdmSSiW4d3ME2E48M755UnzOU1fPEUv7g+fr37GfnsHS7yL5+98xMP6bzFus4Xh+yylt3JiR1Gfd+4lnWBrmeESsZ0dm6C/Bdfz+yFoad+4YV5Mth5MtjbwQtzcHC89+TLzNSDg729PW+mugn8jYpClh2Uv697g6t9j1xC9RplHzCTn2pMayFz/NrmZtcTsnfmyZ5cmT5fmP/bcfNzGYgwy9OVCXMaxOCBE9M74/NV+Tuxt7tewZTzgH2/tDCZTLhdnJhfHCEyw/TfvPxrF08dDHaeoju3vw/H+Ivuyf7h48e7R0f/XsVTS46GvSV6lv897gcMTk48XYgn0YUg+V7XYwz5XvK3YnTHXP+cOet7yPmCfP9YwxAHpTejLI+JUT+kUpowTlu9F9Pf1w+xpylRrU0ePIhsezt9Vt+XvrJ4kyyxaUlG723DUXopL8dgD7lVoMGaVulv81fs8OnhAV+yfVewHSBFTJU8cay2xCYJh+emGJaOvB9fl2MiZGqbXBC8M0kLVqdUJknJ+hbcHybQo1G5udGUP+OWmp/hO3AQm6pusci8wViqMh7DO2EcctPTGVIYlD6QwKj5TBND3LME1q6cD7dSwMW+q0fPOrhBJ6CxvLQv6/+qjRYnm2+feiIFaqln6J9tS16E/kbwDRh4jKE1Rz9gDP4c2qBQj1OpzgrwgeGZ3qXTGKviK7CPBgIaCctPkpR+5rITfDyRYm/Kri71ouB7FbxKHmnAzBW3Y6oYoLfInxjmZnYJKi1mF2wApvECBJT8aGaS5OFzL6vpw+vTF9rPh9ffgo6LMHOIC6YNA4uuOW6Qf05DjaqRtA+fzcop56ZuwbFvwu3lO96BwqCdMRkGXl6JTt0tJtdTYiZ1NYcED9G/966RExyc2dyJTs6zSbJQxYHHUJSfZ1j3E0o1KUx49+LiksDkwfR/lHZpeH+FgsdyXptuGsIYMp0tNNPOicoaPsetNS+sEkPoNxwj/KgpbgcgLQYjrBEIGgJvmoozd+v5kiKiBYTXJnOr1c0611LxQ4ILwD8xyn7KaGoFHz0EDVm94H0nv9eEOoY2ruvPvY3rxZwb627yj5gAkroFWY/70VYPCUg2kZkK8wsEbtGn8KS8kJex8fWo5KDSYixEq1bS+O+6hM+wshNv780Nv0ZAtjWqCtAJEVrqvIMjhZvd4c4j3mkirJ2UDWhKPFvM1cFVZyKriUfxDwuGpxHSCAGf2EnqEyZrkE0TMf8baD7SX7gHgvSspg8frTu7M9QfExZ6bYX/AadpMCknWBs5LBiWNylRFVbthPdTUy9xhk9iHiN2AVWYovpyNJutijNQQrCLbRXO84PdRaQtev3gq5P7ps82beoLnjn6e7rH/Q90LInijlPWlKwx8qgv+0eb7X6nlo/rfJidF4m75lNI1I2q7KvWNw3hpq2qiuTQaSRLQGKFf5Xll8w0uU7EvM2CaU2Gyn6EmbwphQJ1RvQMjpOTQJwK8qiYSWv1LDB7O3BS1WjgTSY537Qt6KXyjxdIWlXfhOJyDeFC7SVYfyPyvkO/oPyjQI6KGx+nK3qvmZBbaQSZ4qjIaUU2FwOx2GWmrVwrjdL/YVENP1IQGRE2gXKVli8BDiev8LAaLwM39MYIBVSpqChRohJVHJe3xfAhtZklYDnnAiQcLj0L7BrrZNAkOtraz7HJbYnL3HFxSBb4CkrsDzojLUjNBUDQlKYoW1HS+B15S6xUbTDIJMHYua/iJAOs9+AjO0vOFNFbSVHSyOJmSCNcl9Q3m0kUK2NGaT4zYzzJKzqIjJqMnF12vXGKCIeDlpvvn4IP4nuCWJBiOOd2HjQfdkM6TEWS/4ipdkxpqEEGvjuvzmuonRR8MbRKbb4dBTHqsQflvtxKIrq6ndZNPIdocwunL8xVwGLD76kTC4E0QQGWgp5Fw11+ir/puN3SQIEryumIEkZ6FaAarnnZiNs4ZhhUx20mUdX4z7wxp41Pox5j3Q9NJPSSp/JpxMOJIBBcC49DB+uyoOKfempHozzBpnC/3SN+jrwXgm6bX8sv4BPw/6qxtmWh9o2daxfdEvwNJfrfKa4ufTmd87ypbm9tw22rX7IHJqJ3HcH6soWA/uAE5CNmmwSWhgYlybeRZPsKbHbzMJjXA/zf8FJVrX551TtO2TMTtSFtG4yC6DiNfMEcFdKtK8TnEk2405HdpaHugjR1wReBSlhNVYWT8PfU0JBjfWGrDbEbDPzWGxboRhGwDnO/W72LsmSZ/ec/qUQuendNefOti57dwgCLa+pc6ELu27c6n20+09u7h/uPj/q9OcYT5t/2r67hInzs44p/25/WYG9MqUMNQS+bsul/9zOG/2a7+M4TvCFthVjZaxzSxVTLJcBVgo2fC1PLdXlDYRNXVdK9nWa9sSZ3PK5aB3w5+VSMCtrXd+WnerygsS/g+JfN6tMlUpcBSgVGxceebvYmmU9rhVHu4jnvkkz9dW6V9MeTXGveOMDX7FwcIR3+JHK0kla5iRGxKUyO1GXcGejLohYssbRLCRdUZhzyvRzYMnMOAmNOVJpU/Y5KSaj6IvCxhL19EbDuf5rpzhUf3FEpV8XtpfDQNdgXj/K1DNUwswBLYoGquTQOAV/+GXKODCn0gWsWPOgFNhx1DbW43BSZQtSPR51M3goVGHGtDT9w4xGHVvMfy6dh6K1NzQFyjCTOhml2Rhf0zubcnYRzF+iePjjaGiocKcDovZ1aqyq7qQkCw+JikOl9PoCzfsa1a9yQx/mUQt/cow5ptw026hLScDzy7K4LhelwXBFHabqF3ooNazn6Sg6gMk+OBcmR3DjTXt/Xf1Xwr899N6Sctr46Ynip+8mhWwI0zE68KW/x/cmKQ4sI1/NQrsEJxySx40LFX3EuKdm2bCGUU+8rKv4eFtYegOyuJVDf1yhuuJmmESzLfcCAtPhTVXQVYAXv3hlV/uturTlXzh579/J7CXLdVWB7oaoBBYNfZ7NZdPKyAb2hKEYtjakOoyvp4luIHCatxivo2KDzyHPXiNmlhEdUmtMRu2aK8XQix723Ux94c24gRwgo7CJFN9rml6VawaE+jNnM2WvRiD55R+8CzyataJFHvyYhiOFUn+i/JmNRAs6B56Rr8pviU3FBwYaVYcPcQ3/QWJITLVzEanQVbbIEQoTSqm5YdUu6xdhq/OOfEXBaeczEGeJwI/InjOO0l1BU1dMBIb0N2baYB1XD1ihIYxYHnXfksNOJT+/+QWemx0tpqe6lQN1cUUfZ0dI4u87L7HSpaDEbnLuwfXn5PkhkY3VQQ8fC5OIatqWzl6JKKRqKm956MihqeqU/8F3isV1w6Nh1ul/d5PxMWtJUf0TfZMxsZhkDclloymoNN2ZX4N02rVMubmUdX7araT7URZZFtfdnJRs7fb3uE8UamDDMhBQgsQQiBsKh7OdqeBAquXZ1TIJ79ReUzFaEQYPVpKE2okSCxw/oQddFW7Vih2Ej3emawdhMEM6HGsUsjox2H1tIM9hC6F8EzS3c/ihuj1i6s5MKGnR1baeIb+2OZtCUhSu25R7pgW42dnyaXs3uf1dlkVF6Dc1txG1q2sW1mim8kpTC4vvEaVLDoe6jDhl5ZiPlESug96qNPyZIFmnEhdRe8CNXW5N5ud2uIt6wJLwOvPTIpFbTGJvbaHjZn/rMW/Vn3FbuCrzSuukjoNR3BGWUMvEkIN6DMDsmG5B9hzRdfy5xu+sG7hUG/Wz0rlWBuULV2md0VClzAQln6/tMBPhtU9xjJAGsKmIj1LfIrYRN8Oooi2b8LdbG9F1KGV9dtDfIOqJ4YIx5MOKlx467m+aXFdEz91eLqq91jzcF6B3m5/JFajHYIFJSSQYpcB7hOPZ3aDflVdgwVjUxayqSl+wd2MUKRmNs1sm0GD+0lP5lYIsGdy/gx+Ny8Ar8kk2uW5Z/n03Iznv/7tVmHGV4KVQAoyjD33U+6VXAdhVnvoUNdI0/sTJbmEropNI3cL2pYp49S7g4UcF0O0OBrsso3oxzYWSVg3AfyapgFN8uGJVkEHBlYiV6CIcq9WCfU0zt+/dnwUi2owO+pq9Pl5QSe2oNQQHge9uiC4JBdClfSrNYp6WyteL4ftbtxYrRYck8w9RPG5lh2dFIv5bdowcQMdHf2YrBe2cyArga8I6RoSLID8m+ufkI+Qf+wGVgJgVxW1KvURk0Mx+SfNcPc9slpldPg5gU5UXIxYJjWY/8/VKNVkUl/txJuqYCPCnTQHITFK4D0mAmlxGFOiRIBS+y3Tt/9zIzY7XpY4vMRrPM+BlD/9wlRyJd4ZynINshnggjiVhfwMucnV9ipxde7ztmEKQiWLBxZouGsCimcJhL/Uu0UNtIgLLIJVezmuSsfVftbMoIFSbBsS/GjuDP01Am0i/KfZNXjNkc4kXW0Gti//JiqzjQTl+72SkfdbgU3+AJ+o5E2BW/+ZVUidIH9kHU1ZiC+B8JX9CZkK2bGZ4vPDWh18Fegab78PM0HyB8y2D7lNOFCEafGJf2MDdEWw4OPDqrxjTO9n7qzEnxWaJe9rD4psIbc6mUUTQS3t9Cpe7rAomZsRvEvkoy14lIvsCSTKQC6DG2KSdrG9Soe2UHy8tatx5IxxFstRKLQAyhZT2vyIYVwmzfZyk3bMhI4arbaWRY+Zp3DtaH9Nq6K8JKutfOEOXpBJs3PVmicq4OvJopZvIUe03aPzeoTlIxgJzGl7sXUaaAWiC3cPmWMQEWh9psKM020dqMQCr2gIcjO5kw8rWhNk7lAzxa0eYwaoQhQZ836RTWlVelEJlnUuOUv2TwmSKdegWveHcWBqwvyESkMO0NHZi5niCVDGKd0UPFlMG6rgHzTYuJ6YQc4hTqpjCIKQdlWDGp0FaNXlITnw4h4OrJWKSTY5PLd6pFYeOM4WHGLIi6oKPa6HBepbbAsP8fy6uP5YMuCafm5sa/CN7JKBlZ2xTCxIN/OL86ffvm8sWby6vnZ+9k+KycZB9fTxffCEmiWTmofjM5tuIQO2+TTzSNZJBXxJAU90DN6pbHiW5JK0Zl0dB7wPCbrpgnTMNY3SrwU8aPjb3Y+V3DkRnn/lOo9M9/0oV9/erq9IcXp//87O3vri5evHpxevniuczmz39SQq8VD2fv27tk0aGElzLhNCydgFXOrlwHO4iLp1TKxeNxFwEixT0ywBuazr6uTnVlwJ2SM8bTUzoD4z9TMY6UeqzyfcJineCYkjaNzmp0q+M4ihlZAuA4F7V8rHzmw0zx90tH9sNJgjESPmLgM6QR60xQR5ErVo1oY4vviZsbejrZPU5Dw2uaIOhhhEQ1K+KhvJFsB+oByM2ooxDZhIZEvdXTjDfZG3zHqD2yWVNPg/CBAYOikJ5/ovPckgN/Q42vwGbH7TEhB7bvMlujxVV+ms6qkFCsD1NJYiaX6sBHokUurtynilfbks/M1TaFLUAKmhyMJjPByIeaUj0X6eVsgqFz+37/W0wb957Du85gkbVF7zIg+CljyXsv1GZiM+NR8GCsFr950Jxvb1JpOESqxbGvtUQjtLW1HvJMBmHZ5eHcVMP0ELAE1JFE3GpRtySlEfEjKUi6Bb4MRaZELvYmArin6GhXHp/FEK9CLR1Y+CTO8fPzLztrUKUaG9BcGzYgMbtEKaY6n3xZDY4NvaZQNro+oqLFObhSjjrJP5cNTVkipzu70NJniISYlnjAZmO42U1vd3f1kwgGjomLZp4EmXn6YtFhVwuT8L+4eJUbnPX33XwyVtvBgab5I+2fwh96RYa44lCRdQ+Pb40yHytB/rKRmuI+O8Aqf9szwOZ2hRY2RyZqJ+DsOh/JoFawekw3TZMgq5j1/M5KYspe7nyIVdP1jBUjim1UNbGs7j5O08WEgjmEdo1517rsh1Uhlm722iqOYm5691ouYKAaFaqLWcDxwzKMJZ+A+5kCj2rtSiQHgFqarLiUTFzBh3Hf88z0VqoYTQbmrUo6EkkENnJnOd9ZU+yZlgQcKfWFs4My8GiMIL8Hwm4T+IjDHBK+add9lkPrGdhe/rHqukn7eAIlGEdRqJ4VIOkaJWenQSeHGuVEodgAKUdpVW4wjk8k9OeI6/4w/Q0zzCqbNOsEG3gslDAdhzF+jSjMRvelIIsrzaCZZ5zxURbMrCiUc7jRjDankAkSKC2EJLNdjLlc1sLnpL7tigbJwOvs5/Cx7223qZRYNXcfIzFxQwnI+p6ZntF1oOczXZzcjes0ZHDcOwFb87odIriTcP5pSawCRNCj/SpI4RduIyvmh4K/9PlhfY9WoJBUuxQlAUtqwe4FRXsdLyt3ChJZOdmbuNJADGHjMpSekHEuNczolGH8I/OoC7E/+B0DaTQssFqPqhnh1SaVrfFbBUcyT3jW1B9JWBgZT8GcjGNIWA4HnhTxr6Ybg+CaxtX9DolRG69KzEpseIb2tnYfd7OMUIn5zyXDOzw+3D3eOfwylpH9J4dPdx479gQ3gZ/JnrDbxZ4AryoXKM+J50gG4ItVO0C/Vp9oeRR2qVct64BxOWCcyJdz5v1CzApPiPsBRUqeXOHgqSdXYH+dIPoMVPm2f1fd3o3h/877372C981zjwTPyHMwYF8THv4V9X9otagTea9GDlOi4ffFeCyz4eUzWoUyblQ7iFwK8E06/WXRUuUF5trYk3vGQMOQvWer5yexCV6CcAlKf2hNdrC8uXEUJwQXdRg6ZEadPrCgq4xQyjMC7T15qsx7T7b26dB0QTYkol5524miHmmpK+fi1B4ymJSWchxEcaxtsnEIjJVKxCdz3Z9s7WWnBVqqIZQU1XtV1jZAvarTah9arBMiBxanG4YRM3aFKfiV7dZugYrc5D1i/KWsho/al0H1zlrje4lJtc2UYhYODhffLuYeKGcFf7BQIOC6dlCg3Z1oOfrYdunNPyA7vHJBeHyQ923Eu76ffSLyI6L8DzUEDNhYImGH92qjMxE8K7PFiBAiO4g3Gg5V+FON7dOrRg03s2+7072ciR9i8NynFxd0JX9zQe8Xw41bHyesGgfzzhybNR6THx+EG2OmFCr/DElaHgxKgXNqsjfqGK71cFx3TUzpfQjfoWNSt/IjLASfZ4Gg/g6dNLCxmY4OsCl5UcAXjE+adsihsLSAvk8vDQThmQFOd9/TB6yCaSjbi8wFLu4mSFbmwGGHmpNoYoZ2H9fgmvui13coCV9jZJRcCUV15uODAWl2yuQTzkRWuZ5cU50FF6MT0EO+wKa2BxAOkfiG/PBGh5K0Fx4qrosd5kk8o6l5mgI2VjVmqCoLJB9LUbI/pUciwfCnvokPf/tzvqAjeFyx0MgFFg3WuNSSgoG94wfxXaimVJdDYH2BiRcU76qmUipKlTCZk3e0dZg9exaDJGG918XM4K/AZLdxLK/gu0uXXNAYab45nO2aMAQoaaoech1RiluswZyzzc3tN8BgjMFinJPl5OvnObiJBbtZcj/5QnuWIHaS8Bs8c7xK3OvwpEOL5npsYX6xlIDP+MEexfldhdT+WNWlPRhIMIqUEzKPXOh/Vk2n2vEbo4WtmaU2NZOKxoJMIJRBrQ0k5g8UxXNGjNXRxY4uFtXz/UgdhMX10S9ShNeUw7aEKITXqOc5eXa0dbDU5jIA9EnxUffnvkFcctN7fvaj2zdNB9Cs5NCIQNZLl+aRNhVvcs10ERYPqcYJvwBlnUCjzxn0Rqu8qtAqqs2QwLgMKExTqaII0UBG4cnbMGItu5Z5+9XKYGbBZ9aXeT2Da9U4EbGZmQyd9FgvqZG3xIrM3GAv/rFVFOfAyW7uKsr+C4YXNiMyh1iwuywU7nSejflMLfaMuhKr1DrsdNCwexNL+O7J8mrbmBgXPY5Grnp4MTEFFtQybVKP2PVimAzxPcIFLMfdJX7BBb414kxn7LqV1EZ5n/zm5HdWATjVt46aY2Fj/R2sB8gKDUkO3WEMS5wG98VCzlWHBTZF/AExpQ0f/DJRub9YvI0zStIjnDurXdZdO3/QQBRjhl1oD75LKyXvJF6MwoikZw3LREOYQ8dDBvtRGq7jM9QayRTjOeOVO3Fi1+dGneX/Z77Dj1XLna9Q/GbQACi58i6u5rhhCagJfQiltcJaGm37AsZIsOOFVm46XzNTWbjZoU8YdK6vBk5dPbUsmpFHch2QZx3O9a208KBfI/SsECqwSdF8zPmYR1/ocxlfXJHqDqdLdoR13rH+j6URRiM7roThmTOcF1SG0lE/6qv7fVGkmABl0PitMNApmk46lta75JZmiXazmCo+feT2dLieeTS96kkNt8miopN8i9RCIrWiik4hjOFMGMXARBDqsaO4Wn8T2zSFs+mTdu0TMKUvT8tOyESWanmcamwP5mF8iwFfwTmvowkP0RY8lRI3ofH6C567qbUWX/T886Ip4NrO7pY9+kc9shS17FEgXfdQFnyUbkhYvcfj54ZHnTIyvaoIMMC/IaFB1Fh4iHKnfQpeAzE5oj5OPpU0SPJ3q/4zU/qy8/uqeACrJ7dydhzb2InKKBF+1I+wbPh3DE5R1QMIqLKZzfvW9rlr6sXt3Vr19ukUCkU+0XN+1TcoZ9k8RoTcfKkFmnluEElsNT/K0TTntfCXqKIRizmHBpvq3AT++kpdEKpmdXFL6sOVOTZ8vjZDNcvKlQSmdLvtYVJKdTbvNurstTS/Ci7Pbsi09coZRZvwXtgvYDHO373tU7HMotEeVdKFB6H3c5qKsyamGZRQeHeWzVET7B37S3Wlbo7O9oWb1hQ2nqhJSun/JxsaNNJAOydDcUDIxXnrYnXOFkYwgT5wrVVeel18aa4L7jFb3UylXqtzoW9xVTg1RZ3WmBGd9eVI+oMSv8wKhGbD0WaaoUbRB3HNJ8aAuRCHZvD9+7Pn1HlEYwkhZmzK20FActLfDkzoZAhlFDVYwga/hkf7F2xvEkORczT6zmPvc0LopSSW43o6G2BScH3NtUx0hRwKweWgswFdOwzrXNKAsEN9+bVnZoiJNPp4QorxJnWxBMcoEDQEr5Gx+Tz2g9oj+XHGoHm8dbDU8GW+BYJNC+LBZMbZDU4iTe7ub/J/M2PD52VpDZzG+kanES501vo/lCBOGmfM1IwOSIPx7gIFZjrY0zOs6egqQKbpLQtrTM2hYNAEM5ujftM6cTmkFDbCK/nDJYbjlL2WqdVYIHcGr1+RFyShBvR+VIGRBJcOQz7+5iZDp9b8TNQDmgOKB5SkUT5ZQwEETx8tV0u0B0YRlBo8LmBXKVO1WpNMkc/eH8sGAbwGFxx0cO/c99TqZzMfaaoEs2BYgdjoD/fenZy1xOz1uBh+VKvJuJ+tsQZYvTkiNqL/x6ePiWKbJ7zWwxgtKk/ipzj/l1Zao4xMPUYNxrgHljYEJck5cmEIHsOXfhNIZrXgJtPRq7usLtuk/VS/luFMndYGkTXIthDqRNB6wR10iggZO/FvWRN2vdn3RcH29VrSO2jiVgNCKF43DR4RJsDgS46wtpsO46eV/n/lpwvgE0VsOg/OFoZ9pFW5ESwK8/jFlIlXMdZL9evKehlncHNn8JoRRTas6Tr0tca0FJvA7t7qwV8uSAiKoSqkexRzpBuEmYgbNVzumIddnXQ12zhoiacyMeA2e9c1vgFyJJnb+wUBqw/ncl1emkChM4yQdimxtGW7KebQLm5vsRtFPXU1arQ5xNalqTWmFuAesiQq3I86opyW9jxPqN1qjIPGmxEjnvTCHtVzw6uk2XhDaU8YSxEVxIeXTZ52LRibq6H7KorB2iVBHoGMm+SQaplRrf8C6ea+rYTya82J3Bcjqg02NC/EoiR9KJI2FUI7EuRj1qd2VXLdZFUWLsq9IwgvHpN9/7Ga2d3syu5EkcjIGuND7Ibd9krFpD/LSrJ+TnYHTRZ6dHooUzt3vve8s6Jqde2McFKxsynatP8CM1JgmHqL9xIRkPTX3mkxLpHMsq+9blAE+pszrdU9DSZxylTTDgep+Cx3HeHvcaphVBK9NN0paW/gnALy6ZhjYrRoglhDYBvmU17eSA0W5RhFuayGsx5g9/KWSecg5dSTpVq/23gi1PfU4dIjw9kZx1mLzyBzirZF792bERy5zRUWkl2N4NoIk6JZYuq4Ha1/EP6OIyvBKifuAaNhuYVIKwKoYTRGKRrRroyBFhpeLmZxZQXIwRhpvASzXUxdfM/Z8+FoOhBY6aIzxw+bDG4qRp9Qco/8gOPqYxnZAXjSzGIUBECZVC6wWMmEcxVRKfJ2dwc7YiqE8Ofib3ePd57AUCH+9nB/Kf527+Bw//HOvumLaWfxS7cwWwrCjYCmIQ539+8ah4vLIS82YFLvgaQWs7Bc/Loex1A0x90yl4ybh+IewlRPmLFO+gU1DHGVdrs8ShunGzKt09TS5ftCJaUyZbRtqAVQrFiCjpwPnPlGzK7+kLV26wxDbyqy88WlC23d0+NJJNYO+itzT7unvJYPXlXXyIaQBes+eXqorQqfdBbXcxMFZx2JuyM07Y325l5GBc9tC4ThDlGzlbR+q26oXUJYVBI4dk5m4gy7itiXUNYwWQup7LiwBV20aSHQFT0J9h3SFh3ZWdmya5zUxcN0CGbGlLgkxJAsXdFLObkuaVk14OvsvXrqv0Vw8CgkHDV7jCwEjfr7b37lQ7MJA0bwAkmVdoC1AHPwrhbeAzx+rWgQxtOJQUPBF6pvIV4pW7ISPCoh78s+qubYixDISXUvP0mi0Xz6so9ImO/sKdbucWjIUh+pW7GEDH1WWc4lzU9PniTM1TqgcKzMCRx4ryEoYjGJ5uzAJpl5dzTshPv45SpvX0pO9o6Od+JO0MtLTvae7D4+OnhiGnbKBP6m2m65aP/lilD+Bu1vj6RTL/gHw48PA7Jhs81vd+GdL+hLIKrpS2GX2/xAeUX3+OCY/A47nl7Rj2U5Y3pCYtUwAXp0wXrXoFUoXEz0rkFgBhwHzNrjL8qRuP8wIPXFJRAkqigenmilpuLy8iMWMxSoeFa3eh/w7lH5CZNeIT96QS5tO28WQzQskfkaPEIwJx+4zOWTBEu40GMz/wiCdFUTAjDNy/HD1j/+Q07f7e3uPFWFd7i0Ny8rwFdivXJUmgmf4H7/5oKKyRbNkODA4wf1V4N1943gMnLqQJXZkqe/o3vBT/+mnHy3Nam/2Yb/JYVCMuX9xwb968L3xZGC3Xu02H+/aDX9Oa5hK0nR4UCLjzROdloH2Wm5icH/4akN4KIjkkHmR/lGJlKzLQI4e2CRl6qSHOma80/5V6LCggh6MMH9NdaNC/2iQusotoALcXLdYjORkjaM15YAD0l1riNPdoSxbTlcEBMk4j/zMxVlB0cbXM3vXorj9M22/CG3qs5fZWC8xTqf4+66iGPRar3xpGxuycGC90rRBLl57Ybz4lV0k8o0g9DyFofW3xSTzsvbyIzRSBzOfHBKeGk0Y6kleZub0866c8LCI6ZPaSVFxYLRlzqE3N8KAU+M+UCjg8WG3NTK80G5eil1cEwtglqqOLo3G1M7Kj44ccghFARhOIo57XGf2ijoENU7XL7ylJdTI00kDilivRLqaNqoKfgQGCa5r0ZlPLjiEJg0hY4UgYdMqyuyhrUrijPkmOZvPE63bX9r96ncRvnPJ+F/ei7I4F6SPkMdM0ChWjZbs7uZRADpIeEj2C/5AO/VmuuANbH77juHnd/Zc985COfGcuS5v2psc4pfI8QwY2LNg90aSk1kiXBE6Ss4pfppZ+y5qIY+Ya9jUntmUrud39l139np+o5+gzfBR21YJuEppRoG16jgBfb2au8MPYaOwPt2oTKNFap+eNTxfHZ+5Etdu4Rf0u907dLu1pH7Du8SSx6uZNXvPJa4FnVZ/izXetbRTNWNt2/Ga8PSWBj1KUMoKKN/6H7TtXEoqDrM9UNub/9l5vreYAfM9f3jwyfHOweRuX641Fx/vPN0x9jq8vRf2lYXYzcy0jstUmuNw0KwtP/3s8D3ZJrDSTuYFp/yxjf8+/T1Re9N8am6LXQIa34no2Qt793HT9nyDgdTIQ1LAfLE/RlNkrHLRGLIiWxiQgqLg0pZG7WZyJ2OuksXZLGPhs1ict2iiV6NN3vYYnqAh4TMK74hmN7EmhT/+GwoaG//yY43jVOSfQxI2CdSaDykwMHLpcY7ug6pzs2YSYdqjsD/9SYS/EdsHLlIgb3yWKuGvUhZPw/vStPGk5L0Q0vXFTx2tfUft5yofZqiNA7AxunFxfZvLh5xvBsV2sjMQtVD3kLMW9v/7k7AXt7I/jvyAvb2/Az9qclNMoPixfTcc62WbQ11aV/TG/aRNOSKldBApKAO/EVxRQJBSw8kByTFcle0V1KqgxeJ7K/8+0ZG+9+NM7G3tuX+N/QmArP0IDRLD5xZ2jntQP7Q3P/P/53An98Ruz8iy/5PJvTw/SRvXM8GaTQoCK62nKe5RbRhuHmNy2XToSSbGOsi+2TUS2PXZjHFPPGm8D4JMgtXEbTN4M3Jj0zrTQTfI26b7WjI4D/Sx5sAOTkUX8mt866Aa86QXV229UgfaXbS1acKKQSmT5A7bjHbwqN5UUp/OlU5WxPmEXTcH6wob2o+kXpNKQ6uYVVHnaXz8PmAL3IvOHjOS3GPG879Cm56rx/OLk5eg5S/xTrSkFCAxNFwfAV2wBWo0ivWNnSdY5/lQH2NJT7LgTogS3yWA3Uh3H/u/nV9loMlvoaf1NNul8WN82S5y3IgPkXW4ThY6kscuGes9iYOfmFv4uCLvIlOT+FncEkdDvb20VM4ODree/qlnsLjwFP4S4ikvtRTyBjPf09Owr+gmwBzbhfX7tP2CsFl5Cygr4AghPLzrGrEOaA/V8ePj3afwL/36Ct7uGMP7ZVwzNrnw+Tlr1cSf4meDr8ls0zAOviAneP/IZ9gRSNxD14NZ+EnSrY9mBT0yd6OTpYO+GuMgyuGgnZgon/ZhymdKJjDduttuTsRlqYsirFsVTG8mYw5tXDc/74BFwIuK0P942/Dub3VL8O/LTV45qv3Q/4qrhBN+QQE/RklYd3CkRgfjweV//OefFvcrK5DtrerC2GejD8Sz4e/j7VbckikFivMwjg/l19q330ttouCL9NE93TMfD9Z+4P5hLZl327UwNTfbEveF5RYfkfxBNM+bfOKDW/0u3YD9/Gm8AYSVGEwMUsTPcNsJUqX+7oeylaGv6LnmU/N73V/99ON3I7+271Zfs/39sP9jdcjs/0HsEboM89QLgdTxm9tu490jPhE7D3JRDii54aHY//A/QIPx5L9C8/Jvj7J/EBSnnhOOn6LR+ZfaJnBf7ijcWAVnj4ZHQyf7j/e3dnbeXK0d/DkprgZ7T25gf8tDkANoMgJ868i41jjsMzbxz/AYBXppYNdn4+VXDZxbt2D0uPFHZWfwJyb6aG2NHv7Hs8z6J2bTLhXmdGvjWQb6DBHbpRQ7aFuVLUnU8Qm9yp+cI+RBA7EuVvNK/kpblbmp9vFkNiK6Hy02zoh8Ryv5sVtG6q6GehINJ9x4Y5QK+wfPt19vMP/ZVZx94k7X5pJKpqHaLn2VHYPqKw/+Fom4CSzgEWgMgl9tcf7+Ve7nw2keGN7MSMUxzYYFnvbOwf41riRYJPXu1szVl9wE8kjIKDYLmvqWdHQNlXHe4c7j0H98RqIIvIKs+Nl9WzZ4NsTeeVkXXIRNhRgDOf5tRZJo33Sk13z1p02G+/9VC+4XyCCj7TbUsB7KL2cPFJI43VT352lcK28fW8SWaMZ2NgOi0C2hjEkxFSpjg8f7z4Wyyr5/AoJk6LtT1cjHrma3tSqZS7MBxrp75HF8qCiLH0qmv+fEJQ8N2cpD4+0p+if0FoDt2pYNPNv9w93Hx8e/qqYzL6+XjxcTev7b3flYsBBKe/B6fA2C/xResSbP8LsRaj02ZZyoA7hHRWU4p78YcBbQbIPDr6auGZENKrvYOnCAfeMFGthUcF2HQzvqnGAg4y+QPbZv8BZf7L79MlhcqVh6mOuGI+uMXzwyn+QOcd4Q59zJTwsf9WUAvNTzD4lHKcVtdtqitsBHMUBhnUZuXFTDB3tZnjxO2TakosvL7Dk5u+ve/Nx9XGxDncPdlL5h2a+7FywVvD3E/f3zFLhMTyVW3iP8DMPjSSyDYqYy/WsDR4lv0B/Bcm48+Xrs//0abI+sCQglkbR6sCTuNg1vzhHCGQhaloK4zH+lPPfjgNxOnJ0ha4Sn9pPYTXvoJwKmGfLcNwmy7b7xcuG77Jk0fZ+zqIdJYsGYzpUdLhqPy5h5T06cLdvHtTLa6bdEZtNezfj8jMHI8G5cy1T7xteUPDy8+ds70sWbHcb1oze45dQwG7Fdh7vpdfQ8jmHK3apf85mttCV4XAsxftMByTMWoHLXlLjar6HHO6nduGknV2aZNOQQnqKFfoOi2yjrwnubJQ21pTcleMZZRUWVDwyWUzRFTVh/cXkl9oPVjad+/HkZ2wHRQsSFWKAkKEKMRZvbksO4Ct860WF3JfXrEUcCtzb2AwcyR1fzijOYHvuMITrUovCRCP8ISSElJdmM0ZR4yYqd8pfLnaPtnce69doA7baT8n6HxzGG7CTrv0h2aIHXq/DnKbDDjv0wHsqJ/K1ZUa3tfz2QJ4vsf2ODveerLT9Op7fZfWh5/NTWVB3DvP5utYePOzwaP8osvg6zIfA4tvdefIU7KFfzuTDncjbfEHZC6zy1+FmMs0RfrIfGWNwPMSaJjWkvBUDrt4mczxrUlbHj2XNbbTDTeDoaw580qD6sQqDXYtaLmb0g6dfL0czi/WAwQ13QiUqN+ACFSf60Z2ViomBKq2BeWHSqHgobrB2E/Ffg0pbUf3NbGWegX5AtirXe+RvnKHgP7PfXffa7R6sunZHq6/dkkn8Ve/e059993b+4+79x93L3D1ccxenQzr06dznK+LLh1/Wc38efHnd2/d41eVbL97ROYm/0u1L4xxr37yjp7/czXv817p4S4/84d/HQd0nMw2z83d1Mwh3MacoYM/Om7p3Qt//ArNsuX44fPx0DbMsffbf7GCuE4A72j14svMf1th/aIQua2zdS4ZxuC+8X7vL3R64X6vtr/Cxf02L62BJ+f+y67X7dP/nXS/KhKyQdQkUOSvqsAJvj7FgZivW2SR64d0nR9nVjQThLyR58DEuynNFSQgFcvzLf4ib/98VNwRliR9E2Jh0y+125wD5Jmt8aaKVise3QcxcymAX9+ObmQN2BmNh+7OvuP8ZstZk+B1CNipTN2aqyaR+MIqwbfVOgtxN0d5d18hg6FuuSSoDy20f5neOFAobl8O+DhGm7gCoM0QpjqpP2pWOaAPgSE7Kus+0h9/2UZAOinF1Oz3uDYl/6uv+d99UdBJ6bTP81t1kAug3W/Rzus7U02Z79+jxwd6Tw6PdPrOWfdt/DKKyd1di+7tv+/uPd5DQCEa7puDbt3347/vy+mM1J0g3gjO5o2NvUv8x/lP03/BOPDP4B7wXvt7dwXenb1+fv3px+aL3/O3p+9cv3lyeXJ69fbPZO3/74cW7l+9f9U7OzzZ7F2f4LaQx7b18+6735u2bwenb5y/eXShQd/bd2Q2u8a/+85O93aOvCSCMSVBs9EJAJ8TkCdgA4aX+qxhtniE7ENYNK/FjPYG/3JXTFgPb3xS9u6a8+ba//U+YLbmqRt8+3usj8PXb/rS+qfE1+9/JiYHpfrNdIKh+xvNyMe2biqCYc+7UebuopJVQZviD3aOd3aPkEdLdopOVhCH0rpeP6cSFqR6clonBH8sMHWtFbh6HTw9295N5nDpIrMugUE0jN2Ex02vxmQZx2/WI3XUfEb+663Kx3oN2Hnc/SIamlqKgtImyY81Rn+ZHLcO+Stm5cnkBHBPcuvVPArKOh8fMiLveZDG8g0cO76ZUhNPOFzc3ROEJVtT0n5yMIaoawle7B+fwNPGzRaDi43sCgPEijqkuHbuUF5EmOyEMhHPXxsWJSn0dlAvvLy7fvu69PHvx6vkFXfrTt28uQT5s9i5Pfvf2zdvXP/UuX7x7fdE7efO89/4ikAVO1ieiXi6Ka4jW25iUBWPr/Vzpk01D0Cg/1tQgXbpHm3HbLO1IK4ksyk0jNdpUawMYbEKN2mwa2x038/onp6cvLi7ond+9fUXvz0vhX/I94QUyZ+Zw52DnycF/Ftsm3j5O47AweMjhY2y+XZYrl3YnYjbujlbeSybpvnjYdJVsyHrExQBBT2RTMIEXumywaoDH5foGPJEPZLFxn234Fnf10v4acuJfIqXK5wJPmlAb+gOIfRZAXzbcwI4an2oZnUuHRa/NfBTmZ2PUFsLcggobtbTbYv59y7lL4qN0v1NZbF7VbOy7F6/OTp6BLrt4f37+9t2lObSc5rR1SpKHm3GFxe9ryX4uu67blAhtt5N9N/LApkxJivjEqyZz9TvMG4SbRYbKovFKFCaGKVk8hLjwo3JcIY3ILYo+ue5Ynl9NpF058wUFixFc8vj6umxk0PyAKtt9QYi91VuJRhNBd0GKaUC9wdBkMlaWl+3yJ8ILr/fVDz9dnH346XsvTze8CJH+PNwP5FHmx6fCLb16LgRp7506Luplv3hHRLt005Z97bmrEr0gjt5l332JNTqcoV32tTPM+ZqliPaw6DX19QIpovB7A1ij6o/axp4brnLNTW6pnhdzO7IZ8zcX1MprwF00c799MQFza9m83yzw4i77xvkd3PxlX7j4CO+67Avv371a9vEJNppc9oUf0VZf9oUXSilGkPBl3zytx3XDy7X0nZn4jIT+0C4+g9R4c88p/d7bhrNZJRxuHnT7KLF1xMgONDIWYJGInhdIlDqK1PGACH9JI2xq10LQP8OmbttBXDaIGpecW6Say1NaFmSQM+Mw6CpQBPrqccNg9/Kp3Dp/e3HZu/zp/AUbIWKXnL3otkRAjlcYxyrni1lqx1v7vSqNGSGYVeTMN98GbdCWSG/tTA+xqaS2w3cLrR0hbMm6DLTTjFuShhKTirzVAqhRnPPXo6JYLKpt25pdJuMYWz7nhGeF2wFye7FyFD4lPAsKxaLGPc70SAeUAwVr/ez92avnPdCt5D2evAKH8XLJNozKGxTwMz3Aw+QAZ8gYTRE7box3H4NNBVnh6ANxV7o7MdLHbK64tu6kNs1Be/3+1eXZq7M337+Hd3r34uT5T4mWbKVKdvyQiV2Y0yLAZmTxk6m3QjAelEcQtSRRgOEWEOr5hi0A2tzA9DD1Zom5gVBmZ1v4tTIWlyu6NvEQixFD9ctvMS6u0RJL2qKzJac22cAhHLFfNHm9Qr01M6fkkmzpi8uTZ7Cul7nlxLaxzDF4U0xwY+HfEsKy0yOstuH/3HSW0Ug2vtJubdcP6brd32/BDO/K25o7jbdZX+vtFHy07+sL/gqv59tF42gF6eDwFTrc2dnc2dnpSXiyKuQ0bWrHbr9srRBmkot2+voCadlQy98jewGyZ0tt+Sb+5eOUOm6gTTeXphmbtkxy0zS3o+JnrehEMoQhgs3A5tyi6q7azPy2dlTcYbQFayLpJPz2JBoPRPdDW7Xaf5clk5Qfyy4LAC3gtdh9eqjEFvuO2sUSLJybmk89TB5sLZWp7Ai8I+VAJ5hK5nrfk1Mj7g/zvKLm6aGD3mvhzem7PCr9s5ryZ7z+n+E75bRoqho80TGy64OJO8FqBqK5xKKcTc84Jh4JfDDVRXKfiTGqnTk3e+V8uPUoYRLAFdhNVkAL800NknCWahm2IxQNFyekScu2kLBCKWgCjYXtXGvuCUSkga8r/udf0f0PH6UMnDfVZ+Li4obAaGbiscNgnyX7nDdMeF8rSLUgRgbimaWfzedghU6clOe+nty4iThNU4sl++ZYXg0LdguXARs/cNPy3IkhKkPfzqIaSd8BlnDoNQSVmfIjyyQYPveymj68Pn3h2nHS+nMb6aZkogI9IPxQwYjw23XMcYMq3mmoUcV0Bkz3j79qytsFHNhwe9nFf5SfI+2MIR7g5ZXIyB2Y4VMw1dsenJBipvX27u+9a2QvLzL8tfuevVZD5ffVR3Th2vlO31P4+Q6lUfsfzLe0ZaQ8Rde8ewEW3sn5WfhGP+KFXbS2q6cpFbeBSn67ajpbaLtOVAvwDfgcd9usQRvz+0i/5bAGJGCS2xS/jDo+qyDgewviuibhTKoIBM4aN1XpEaW5KHctaUGrTokIwrTaoM9AWLR35cgjjt/J77WnBJrs4LH2Nq6pUVM1Hm/yj8j65riJbTP3CLVEW5PWQaVzh56+PoUn5Z6lY+PrUduySsmBsDJRax+uS/isqolNpri54dcg1t9GWsYyg29E93jeQQJw3NOdx0442l6hZUZvPFvMw0wepYpvrIbXhvOsRpsFXXRVX/AZSJwNjTIZmj/98NG6s3M0ldQjHv8DThN4ThPstKp+tm9ASvspPcL4cAaBOkvgo/IVNRq5XqZtMGx7CR5H21bhPD/YXURuk9cPPprfNzF82lTfycE30PjATWlq5xBQAYIRUX1lYOZKgoj//LkJK6fzIgnYfCoDNiLUbl+13nNiD0S1kxw65V+U3vOmTe/ySxY3u7GSH4WrboVrcSHqAHRd7426fngwTCPpZ9Qu94WKfSLKWT0LNBgHTtAapQwXtJqji03pyDviYhEKEV4gDkZlIm+rhAuT5LqeFh0qp9JeLypubEMZKTywrZCpK2iqKNmADcRil+W2cq2o3x2M9IdFNfyIJcB4MUJ9K0XNVsHmByayEZo/c/84oersk1FJRnmR0atIFEdG4G0xzFHTk8FNYlbMqvQscLmY7eVztLWf487XgrCOi0OywFSZST9O1mbMi+MI54WKbY5Xfy5vSYxt1K6TXgn+4cjXySbrPZTzNc4UJRewAfhiVAa+LmmEa3TfkZedBJaxrC41bZPaU/KKKm8cZTxTpjoCsyKMFZAx5wnf8EF8T+4K4mbl/lA0H/ZMOqxHkv/YDgGPCR0+1CADZcqa2xo1Dmaxr2ulNt8O7tPOaRP35VZYtqpbMFLjOUSbWzh9Ya7CBfiT33Nnx7esAKnrUsFtV+Wn+JuO202yRZQ4H1FkusouhlUw3Jtamg5z3KvjNpOoavxn3pjTPJMWcGfvp31oIqGXPFUaQk2Zx0677Aml7me4R0wIaEcjQmq2zqbl/fhB+uER3Z35tfwCO3rByo59gzbsQNm1dpkefhTPfafx3PTldM7zprq9pS5mVapfsgeGs3yuddOoQIZZunCYFMVwdPMgxhvKJ5ZvMMcbbrnZ3TKkavXLq95xys6aqA2hwjIKouM08gVz4T2WsdREg8IOcKcjuwssrYq9TtTU0uKUk1Kmf71KeOpXLFRtQusc2g1iZpOOcYYFelYUOAGzETyCi7Jkmf3nP6lEjnN/tzDA4ppSfy7Es32r89nmM729e7j/+KgvDfy+7V9dw0X46MJNYG9Mif2NQv5N2fS/+xnDEyIA14MCHAjfFCt7jUO6mNJxZlcJUTIls99JrKe9R4+aRW/Xdpr1xvDWGBxAx4hw8qkYFbSv78pPSPiOn1zA8S+b1adLpC6z4mnyWnr7gSUUz6e1wih38Zx3ybCxNW4VTwNvg7xdxjigxEfX4gidhRKZV9ShrUiNCGlkecdBaTXQlwUyWGJxC1csJb0eV8OMj763dWSiTt3nIDDmRKVhzKHAxLOnOdfO6dJ8j7wl0xCwkMQIyh5H2ZFv8AbeA3esp0PXTCppZlgNI3dISB2vy4jvtqCAcTWkaAiuWdjv7vOcyMxYctPqYDzB+fGok8lbuYJ7eUUPvOIHbjzidFD+Y/k0jMa1qTlAjpGE3m7g3bg7J4b38ZgV3BKLumXia8uPsAVyK50kE98m2NTH8aa6SKL0K8VKwN6Z4lFDUtjRiMPRsGuMnFaaXTzy7K4rm6QQunR1pEQrVljWxFTro/yvpotSZZ4cC5Ij+eabfRDnlBdCKTQqP/fdkHLa+uqI4aXuJ4duCdtXduJNeYvvT1YcWkS4nodyDU4c5Yy223YuKdm2bCFgDwD1FU2faHsAsruWdNF6jeKGbr4VLMt9wIDO9lNV9Dqo0YJ37ww0/3W31pwrZ4+9e/m9BLnuKrC9UNXcIUKzmLLZLDp5ZaNWRZThFtKY6jDiesZN7zVeQcdGmTnSTq4qzW371nQix723Ux94c24gRwgo7CJAXO7brEzjBYf6MGYz7zkglvOO3gWeDQ2Qe/RrEoIYTvWJJcLlaww68Jx0TX5TfCouKNiwMmyYe+gPGktyooXs09boKtpkCYRIp426YdUtGRhjq/GPf0bAaeUxE2fI57nHDlGgUtY1ZcD1npqWDL7LMVmjII1ZHHTekcNOJz69+wedyR8vpQU5TIG6ebZXp462n4ym8zI7zc13RIhqcO5C5ZkXoxLZWB3U0LFsk7kltqWzl8IIjbTDDjh+C8JYFBHmX/C7oWPX6X5ldqL1Ga07OI1/RN9kTMDJnAG5LDRltYYbsyvwjpONAiB4LimCvWxX0xSpbYBENwXJ0GakBuW6T5DwWWG1EoZhDBDlv33ql9uLseGBWAzs38ON4hVy4S8oma2Y0wGrSUNt2FYWFeOAHoQ9bluxw24r6hS2VjA2E4TzocZZgH6yeIoIHCRXWduG4gGpfJ7J7Y+2KsK1zU+Kr0g1XbqdIr7JGtZJk9K9L1rtrUm06KFuNnZ8mnHN7n/SQDEhDdfCKiKdwrI8NVN4JR1IQjOnvFr0Ux91yMgzGyl3gEeXIxSv2vhjbYn9yilrAP8XbGr8UTmKYrD25TrbT/pTF1zgTN9H0+BYNackyTh6yQtMel66GGhmbbhExfHYKH5YXZOJHHThk9hwJrJCaBUrN3PdKn3GU+6bi43TYGHiEK+ZP+z6BbuoZmjC+G5UMIlPRUWIh0c9gvXPNdTEx4IUs/ee0GEHX3wydcIJT6r16YJnuZ0KFsD6wKRK6JhQ4qlo9XygwTCf18I+ipJbbjM2YOf0Xcfa+Z4jOLfkYRKHVl1GEGp8NmhSkHmUKr9BOkbJA7cS5hMFSwPlH+cMHH97BQXls7CoGlEqqqi5tA1WzKXih5swsKjPjCriwxrGTzPTtJfV1m9IDNDtcAYc0LPvJHqxrSbVuBBzx0lN54LW05zzvvrp1GxX28fr26klqj03WHWAndeMBujIPyT4xeViKkQxFHO2KvBJ378/e65L7eQlBwAJ+zwHVwmOpWQJhFeNYq/8ecuOVufj6WmaUDUpARIGyXFFwUlN7TjyKkTMXGu6tiAGUx7Dc8bpFcju3L4PhU/xju2oKMOHS2+55c8iqnbqHXGLEUmS7E7uozmDbu6DPlhR5O7JNLH371gjspRjOcMTzzw74PvvCPmpFLRgDTx497NPxIVMfX3HdZ27I4QN95gbumd6Ayi8g4Zlp8gL1ic0VAV+y34d9T4wdOtpvyUznCPcTfplm+C0O06Fw0n7c+WAx7n4mlG2u5kuUmLG2racdRASj+JXKgTmtddXS1+vP+MyyashQt37hHZ0BwRvO2+jP2km9Z59hxQudy5JsusGjFjMsNlUmROvK/xa+4zkCT9Ut3eDPyy4X+RPdQHve/HiLWm+2yYTUw+cdrL+XPC97Q2k/xKF5sHAgan++U+UVtA64hsEy95T0e9xDwlc+//f9t51x40jSxf9fRrod8hNwG1pN1lVrJukmrYHpZtdPZKlUUl2997YKGSRWVVpkUx2JqlSuWGg32H/OsDegJ/Fj9JPcmJdY0VkJMmyJY/mYAaDtopMRkTGZcW6fOtbXy6uqwDLHFpleCbRE4UCAOR27DTGCkK5JIV2x3l8kCpuIQf/X44OwpbOI5K5HaV+JLl64P+YlU5b1pyqKJ5D3thgMOCaDct+tNV12p8s2XokWb6o82uIpszGJMBlc6XDjObKkGaW9eSLRQ0+LYHawZZkFwbJOf5h1OTrCH2PN6fud6h9cX22LHugo6BvMJ9AgTnSU5zQmgHiNEZhRqZ6B45GfOhuJ0BaeN7Iy5B/UCT2vC7RbKT7ws5X2Bzt8mMB+ZqtjnPAOVzPylnRp6IO/G9MPOqD1O/H0ZanWpEpBD8GoqP1Mq+efsVRjcatIrmr3cvMPl+gL4nXhJ4QUk12sTuhFp9LzLPQqWS/riqMPNPBHlzYgnW8p43udhHGnmGKtVYTBalJ7LxEy8ApIWFTpyaZDt5UhPss5vUk1SDbZlUhClPxfEJZWWhJjaf49akCGbyjDQJAkg9cU3VZIbQbAezkdm0/ll6pGycF4i/IqwNn0KhDMCXuPSPHDYNjGY2kI2IlEX6giBQnRkdXYuVRo6kR4e10frMorCcP3dteCUOgCPqc3f6sxv6oiYlfRmfsGAW3O7dYnwonyO1R0h8GKCSNWh8Ffzhs515lO3v56mlqzFpdOHJS2QCf6SD0fYZ3YetKV4dygABh7ywBrulad69z8vI1WGg053it5KQAOvk8X9aIz21iSNDjArShJhKoJINRQ3RWaeKy3eHSVCkfgK3yDj9vx3eBuoFPXmWMVjQz0s5yiUcn9C002lgyUEkdrKCVGPK9Dj/rn2AXfYny7IzenIKEf9rGL2xHaI2jau5V0uTRquo5bDHYOOGlTxqoYKDg+zZIgpoAh5DUdWwMWpBNWNioGNUYRMZgwdWorf+qE6hDm2mKccMFSimnK4bZ3F7c+5MoeWddYHrTdg11rBjhprKSDjEjrLDiHtbeQzvS3ar6ypq+kHT5+YQDCuuL6ywPZBHYHosSHXt092dIwI1CNdUsnXs51a4B431xM19x+c2K7rhNZ4StPqdqjYxw83YZiVXybJzfQEjD+8ECEzYleiRXgrCNfPZCLD7cDmg4rV4yMiAp/mjji5awvB8hd1N+nyyUCcaLZYN3htmCLnMP3EdgLXg/9B5YBzbDuCHNpxwBhTVidKopwF+x5t1JGNCVgSojxq4vcMMsZAeJZBAnIyV2oVoDavxgXGGGAqmcKuTAzVDVuYGRK75zzaBC3TV6SUGDKWzSqQPgW52wSEdvbwoEJoqFDb6GmxlsavHLjytzkdMsNTlgIX7ACkEyJWQyLYwd2KW189y2cd3U+Hcvz5is5OzxyStuPiknyREru4tOBCOLrBwUDx+a7mXSpYQtGTh6aVNXV9joOwGGwyZXWdoFnjQwJsBJJzMGztVpsSBgx1pEeMKnFLv2ISXwWjDFqElg/NiZqzyxz5+dPfr6yaN/e/jiL2enT549efT6yWMezc8/ubUH2ZmEDtsrFy8bb6NFm9K9lIkxQhlEN8vJmWvhKKIgUznrcKbGXQZJGIEjhwnt16IQELFiDD+4gMLXJCITLtuYMoSsdm3Ep2sp2KZ4m0Z7NTrVcXDJtMyoACQ3Yc0nKCiBmxlBCb5EBCEnJsjdUhcc6Vo5I4lwHl2NyCLgPZehyZNc43a8fEMVBD2gQiHQ2o6d+iDpgbIBUiPaS+8646bl6w3vvtiwzAZfZj3MZUCltRfMI94dGPCpCHo4Q246kvWohThJgwb9BfwTilhjBrT3QZCGl1gcicT4gapewRFqH71jvEoKAQJdgk7OFt27kuabnNoG4QgPLLo9WIkBRobUDEuz4s2cxF10LuD3/w5ougxIS4hnQwubd+bHQY0jDB8+YbVJYCZ3g66/cWfn4kbAcBmRQqKJS7fFrCjG7J2Q5HPZ6AloxaoDRKAdrRFMrDJsX/OVi5IaoNCMzaJohKbs8pDQ1u5DZhtGwkoPXCfsey7aDrM9JcFitwem7CQgDCkZIc5uf3RIiQR/JgajqjQqZX3WUMJdqvKRFUXF2JOnpaBy2Ldt2sc8xeGWSiPD6gazAnbYfOLOdp0Nh+t7wvw4QHTUi1b0nYbPWh2GXHxQ6PT0WapxusOvFtOJ6A+aTUZfkUCQL/1lBglX4WXW3Ty8Nch9iFz8upbq/DrZwDqbW6C96VWhmtPu6ityyPtKG0eq23lvBlY/l23az2o8ksSP4C9n2b/zAvkfVhsgrNl09bGmRdaPyjqW1t3babacokcH04C8dZrUiFrO6C43S4iOsj6oMnZp9r2JzQcwDERwDhPAoxRtkTSzaQdczwWRzTOgG6CcBPZx8lC6gzISulEQWMnXSV0ygpJKa5a4JTq8sYGby/0TXdE4Jc6YEns42SghsifIDUO4CkBlk6uDXTjNpn1pGoPJZ0h3K+Yb3iXXhNY0xmJN6qBkjpDLp2sYBgcQumSDFALiI0M6KOgR02LGxJEAwBw3wuRl047fuQU8yp6u2ozxa0SuNjwvOepc7Win6eOEtjLjgPhCeelONKXhodsE4vNLSi0qmuWE8EI2r4DhSWfYSCLvwH7vvj6/ESg4X+IaXUQxcYFhcyCXA+0ezAfsn5gv+Wyct90GR9mx0zbPmxFkvWACZJs+RJCzYNV+HmAbc11IsEQh5z7PetRZz8M40S3VrISPuim1WYBE1qs4LEIxsKyc7k4V2UVV76prCATk78vpcsrqNhlm4ANJdHXK+ge9YyCNRjnQGFAAeFJOS0t+sA6nbXp4WFdvUVgYGY8OnYRxSCS1klXC4l9UN8oOqD0TljsnFFPBCn+Td0ks4U5sqzxi2qEJaFRQim4hkM3gABwJKRmdYsZUomViA8ntH4YKJGojcHIVz3supDZhrqHPVYOV5AiwBYeEZhJJtPGSwOLMzr6ySSSGIBEyQQvtAqYAJruaDcaF6yO0liHNpA3rP8qS8we02Hj82BPpxFr3pKxu4UrcL0HhUCYACeQ8NvaQOEowPA8Xm4AM1VvVeqk4+UmmgrDCkRh0Zxnw7VZPo81lIXNmc6xBUreez+3m8O6NoyiQHG2qJOlL7cwEIh5J8BHYSyYczbOKxDERBzmxd1G+L4TPL9VR8OtXeSloM5gnwRmz4YbhBTfDB1t7raO56z47aF1bIT8A5ohqDkmkhqXS7kib4VE0GWUOoRcButsn6DZy8NbLGVqN30kZeTfIHSRBkQQ+D8cl2PjdCOwRvEkLZnTsc8r48HkjEpSqasZzI/nueD7Bvi5HdGbJBEZnKrkYTHuqtZGnwJ1gW1pDeYr9OLmoJ88MVzFAMjIug7JAbwN/cQCgI656ABawFEHZ3Rk+GOwcDnYPs+HB0cHwaHg/rGoCVUGTpTukLMMXUK+bR/GF9g8VF6AM68C9yPUcyPy4oiZUsLhwkwaWwqD1xJB/Kj7FgdPiKlOI5P5RD8hvTJVZrdvg/o1XeVTQpC6QvA7Ss7SmOjzqjMQzNEy4aon/0WHwLZqT9msoegJX3FmqVMqPUZ2RNbUovjW1hqUWRfD7dKXW+4dRE0oGFBRkRhIaoQsy5Ygb9JnMkI0b+IGAYpsYtq+qqim2fv87t8WK6ZdxBpkzRtynhsiCcViq5sKPcDzhg5OS8eQkqlJ8ert7u/CidPbud7kjc4nsGWw3el/bPEQEChCbzCQMNBhrbTAk6kwPlJOuCYjZsOc5IdfuJzD+sxFJQ4TRl9Y0QfnPIyIiRobJLZJ499CsMdhRVqsE4il5AbTilBfXoq8sAlaFjdr3ChuC1hHqwmyNGmLLFz6ByeqdwRwNk0kenG3bmcCEX5ubJj3/rZaFrM9na3iHCovnAEsbqqUlFOZchFBc3h7hJgj6SiwsqGqkZeDyul0U3aYcVMS3HBduLUectRGrH/yiO12ZMqHfwyTWPjo9xdP351N8tdap9bGJstZ828Q+2aCbdPvjgvG0krP8MH8L7moPjwoyFMQRtVHnMOWigsoBCN+hY1CX/CNg5FokM/L8oTkm3H5qPTpgPbS/qvrthZipor88em0wTw9N8mr3mbwBJoIawSUVZbiQk5+zCcuZ991RzJ4t3u5NGhxpL69fgcB7DmEY9FpowYpkMIL5RehlWkZRNBKe4Gp6jrnuRAiG0DJ+gKx6n8Q1Aj5SdPnV0hRH2WE/ETfRqOzI4Q6G5qniyC6W8IRchU7KkcTEO5HqbzCjN4yHKgjQ0+/TSfVBd/lSnKRY7QimmiO+bu2oIzoGzlgDbgRMmOZU3byxeV4FshEkNt29dhDYgh45ePA8nxvQp7vlrdZMA3lmnBHYTMAcYeH4HDY7zyVKHVYhOb8hJy6+xCVQ4CzIsgelBukmIqHH6A+Cebxf5O7kBdYkHWOv3pKdBE/QyOEUIcb25XHHZRleKB7JULBbWcilBeBY1mMi1RBKdBSHLNtYfUqFGOflbFYIwP/Nq2eNGSVRqI+FUIbtkEtgBDfhivReQq8x1V7SyY7OFGLdv8XsDnawyIMYRzJsRKyI/W1ZLVJS7F4QcQ7GY1J/mQEfJVsNGaF19vjkW10yiTjigHi/sASWo9YOVvcF1HZORH0WgS3qB40dQ9uUUQcSDyc4OYNmxFFWPEfeuEGm/W3fDCHk0Nxw/DaEjE1OY0svtUL3ggaIVJuLau4OU60yoZ8YB+7v+A4SDW6Fipg4t17en98Ixqbl2Op239yqeaaQRa0nyOpBMWEqDYQOEtbEE/cTq5zWGYh7DLKfSKR3D5ZmO3CFINPMeCwQWcwF1Qt5AvkA02rsbHKF4cHlDRpiMenmVQmO7aURYjJiQUNllbmtj/98/Bcr8fWu2+ReIxFj7RjIKUyKCg48X4F/nC0CfVAKhHVoWzPANyFj9ejGTxPl5nJlSdVC2ls4tVe7NDlICzLOYhM2cM/iTPE7sYkiMEUn4Up8d5gQw1KK24MbIyHZUOrRWJhOWgwoqqjiAw+LHIzfh11UKP8Z3oFLAoH/1VnubbQRCK2OfLkGTH9ARUY4fSuiSfczhDGROIezLPTgnqOgtEjWAx+H7JxacZaqC5fny4giPgkLIBXK68slOUjx14BqzdkrPs3rtykD8t7mVpUxryUTRtH/qDNYexz8dCSDAgdd2IFxuhp6QUxCLNNUPZ5IzfPP8HVfKJc2bRAPyMThtNuSbMfUrKSvMwvSpO2Ghk2HXdkKDAcXo7ipeD7B+L0EAlcWUxFvDtNyUlgdw7Qs+WSz9WB1ev2sF4+mh9dpD5FuPe4tOSDjIqq4O7miPTqQAHMGzYmJh+GAR6DyPWIiESZL/hX99iWP61b9v8zr3B3W+dWqrr+V3YpOxQyjcrKGPOHj9oKEHCnUfqp5uETG4uFmWca/obT1ijdRaqPPnHGAfPlwAbe+5fhA6/PO3OZfsn+fUdnjNe0oepXv/R6gGXsROBY+B6cTZlI5sVTU80XPKjtcnGQTVrP2EHIBUmI/f+iZtAlePIKXXdxW5Uz0G/gFGwFbkJdMjRN6CHljgDJnZMDu3hGNj6+9AcK7WCzZYkx1ZVobgPZXP7xX6TYlvgRG+ZUYRKNLmlYbLumNrnoRXJ5D/roco2nnk4t7b3z85uWrFz3MwFvWTNDnjnzTYCFTxM4vGq8+zBKQw/DsrBqjoHU61hfZe3SMquy6k1bn1lkoiIdRNS+9d84WT6D0/DaRHBGvNOqIU+UXkEnS4UazvPK4eAIk9dwRJ/hcpF6jJDDwFHFvQdO9WQXwinmPt6TfKPHLrIF81+RFxhGKT3wQJ/uDb5cy+3AEyNEBrE3iMghDvTNaDsxMwatbsckqQzBAJz4R0vDF99k7JQUTeWDVsuipdd4jpPBTRqmYVN2WH8lTAmF2JB4hhTQpoCXprbXN0J2LNyCijOnXnv8upivswQ7JJ/0MUzOKJhA0iNXjtmk/9oJkRv5xQpc53NrvZoaA8AKmYDByyiBsyORt+ZL02Pfpb6LEe78qSAEj2EjLNCIFd1jv68IJkVpVmIoARm3/uh6bQCV3uvMcUsO6+CZwZB2Oi5klhkLIldbwqoR+i3cl+oTgDH79GtxsUhSEGKtJAnf6o5+hncPOBLBv5MZCkX1d/gBMO96vpoPBbWp+xvcBVmplNDFHf9JRF3QReI4qPkt8XYCfQIowxbxghluKSQ7Qo5n9UNSQAGDyChCwuFbWtTV8UumB/ZcRT4ZslRT8cNl102wkV88n+eitqEnGwGzM9U/3mfJbU3VdqOiNlYtowBt1Rlhz7ol6UQsXZ1pciMTojFV7l3OkXK4lrwY5/cTRQG14LgmE2K2X1Kgr+vsteXn1cT3FfCUwZKd6gRx4vCyIWWOsb3D89OaBQghYeTGls242+h7fqD05lvgOEncVlw/I037IeUbQbSYk6ytCWMhDPnLvDBcHwdoeB8X+QtY5pbgDZAJE4PMZ1bMARy6yYUgxgTgUm9qD54RHtI5LpI9iH4HqkqwE2NVb3/jTJQpB1kyZyxy9iniCIMJwIZrKFZW3EoNc9DRf872lsfWz8wreAGtm+tN7C5fUdy/5uDw1rkDVhIDNtqVa83Kjf6FZXl5C3b9qplmuuDhIgizRMiIqIWY7FBX6ow4/ZpsAK65T1Ig/A9ubI9G4m6FtMNmqhaGrlbB6xFEnogJpxpPx0K4JI/00tFf5YrCKSBAkQG2mtUklUbGSfznppk8L8GajMaG9YkR1wDCXEmJRyD0USX0B4I8ZN500ojXPtpsD2ILNqUofZpvENZR+KOd2NbtCN5HDMdLBaBNrs9v+UjFhzaLkaJ7K7qB2XYa7B4OvCzW2F505metz75jql6xLvk17TyDc5DRRr+K+Bvw0fpo9yicFcJj1pDYuiEB/cmaV2KMhfpYq+GSCosasGXsc3edxMGFcYNUersHMAEO2AtCII6qa8TLk9grUwlY8y6umwXwcgRTniVDFwS1cWinpbKSY+fIT1sY2VgcosT6hJVKXVSVOKnsGXZM3DVjqXoMgB21i4UmbBlR+BC6RwC98Hk994OCOvSjBBLdBpOpeMXqpu0ypgHE6/PbvXCVMwqO48YAXROiLS1FKugNlcfa1Gq/qwWIPu3icypTC1WEsRVEIZygd/HHzX6+Nw9itQsam3TDBfYec0PkEwaMoH4yVfuMk1PtNlYBEYBb9/dSqLU/unfkkzFqlypMbo2Uo6mohUkzwnJGQFOebCb9sGOJd74CB6Qy9L6IocCSdDDdYBDTCEltINAXWmdcOALjQaOPj//J2V9ZvFrw+vVXuMtYgmPknb/j3V2hGp68z4PbKmGGQ2LERfQO2XNuNZn1gHftxiZ49vq55c25dVZXbkd/n73JCtrgtel6b+rL2MoRVhgKf7rJwfaNNzR4XZoTxYYJ8ZUw+tb3SUidx9lAhCrJf6up7nhf4zqpXsOsSxX0Q16U0ygtTwTbhDvgFQ+jAvnWMhhRN8WEAyPw6r1HbtB1jjqtxrTtFDyNr3efnFw/f6JSxM47axAI6kAbbbntVaK19hSDID8mjCOmWIpxf3xD6VroYRjwwofum8dHOwO/AIaHVt1Trt12QgzXtUM4gEL0JJy6mn5GdWmk1jFWLE7Qnt89VMZlnQK+AsrAxWYslcWDZh4FvA2hoKUJSzSK1JcDiSa6UL0voFzLXW4DulNRih2WWRTMt6zbJaT9jyKzb8HFPEv4PWVHVlxOX0rsunA7C5Y2CV9OqduGevqjq0Ej3s0/qPSMbySen4H7AVGeWU2pFch7HfcbVCBECpO+QooB+tZIydEwiS4d8XyHymgAgKZzf7TSsritXb9zzanwTXrgBSXTXDtgMtA2BCjDCBxKFijpy35d1IfuWDUiiPMHSCwspI/TqyWOMomxuNSFohEiG2jikoLqVYSViOIMb+1R4vEaTHEOkzhSclKNyEYjspH66YrKJYyucA18ilzFouRZXhwRbon4PisgmLtuDzrBD28N2iongIchRlCauT6gwNvYpYTEpynoz58WDmisbFQ6wdQ0HvxLzpKzXIbt0wxa3IrETqG5vBm2GmBoRAOZSwZ3GzW9LWoagoiBbI/0OSQgR6wjIc+bhrC2QHvFIjxCEfrt9Dc09meU1bdTNpsDE0dHlxreQ8WhdAkVYNV8aWrZ5jkMOBY8xBwB5pIyIHeTlwR3mbhgO2jRq7LawXrZmQbeJFOFFkzpRGNwmjuHIlRp5Jil4zbkjL6Fz+QYrYatBdgeRdeAeiOzOu8nT2bK0XCvzXNHpKG8URh8zRFONT3pIo1II9UJTj1009L2fXZDZVuvZCHwJV03uq7w5NR+DJRITZlYYgPD0goRnIjoWW+6MLq4z+Cc8e6a774wMh143JEMcdN25Wuvhhpy5cDXExGwOmTfWSMA5ck8N4YdXe1++DleAZbx7YA8eaNwEzy4pqzDD5vkTZ3iAb4dZKvlDxvIA5W1fn4S7CNSRJgT0YLcSeecK18jSIcXVCgZvbgyjyV5DZbdp4YxHSqfMZ9EWke6g1JRW2cBHMZOg7ySwiR8IhoZ4FaA1nk8L+mOiHMjZ/Cv3eVGyG5Cm9gV32nD5W3a+nVcLkJgdinb6HYMsber0Kc4l1MtC1msoApg37k1oxXjGG9Kp7F3l1BIlZOBE8bgKsU03DjS6bZPUO6C2tlGjGPBPBrArBjkwANL3270vj+EvKCfMm/ZDdCrducka5CAq6IvBhPYhfk43ky53M2B+94E7lODodiODX37gkSWmg6ufFJgbr9PyFX+K6/yRpmdcvisHRrS6bh+7jz5wbzLp8J5OqkYdfkWffvxprkZOAFzPdYJfwN/fvTQdo7j8/e8iwQdRrbfFnO0tEApUfZQPy1Z2csFJ2pgpeAOezikVF9WqU+DvIHWeryzUYLkArQgfcq21wZbwaevE99HRjJ2pthGWpiKyFrLMkLmHnX8k3dXKDbSJSMqTrBAuKJUZ6CMLfwdQUfD9tmTykUpcBpvCBGgyXOiedm8a+Zyt9ASSJhLHyliyKqcWqp7QPROqN3R38NGWm8g16J+HIyBP6yM8bXB58jzAxUyuayZTYWWS5+7xsibiq3fFpJrDZu07o4E5UVupKwRdx0jjsk6pbjAot9VmNPk4xCkGzyCDU7JqvHYuuGRP2Qazy+FQX61DuFEYLULML1w/iToR7uRGgiCSu9ksEQY+h2tdCrdTnheuMPtcJNpK7kjVxCYllDz/2xLO1nkN3HQL9x6XiIzgSrNkdLjxE6xpBtTocNoeAqIVwr19ut4obEdB53O3PSgXcjIJXD6mRgtsKdQHwFFydQNrkmMF+5gpxpgHtM5b2bFrlu9P/6WI77Hfe6r3xBg7v+PU6AKUFpWxkB9p1VSzpeRnbh48SB+S2SjJ4TxHPByd+vaAgFQN3oheA2YgmMXCfCsL0MTLI44ACaOYetGU7gexPdolduOQRZqdVsH2xPUnZr2//11dAcgV+OOPQq0ndebbT+Bh3P3yITPouBO3+2WoszyXuszF+wWznJd8dog1XnNZFh7KhkwYTXPtZC3nWCGUrqvAj/TxNTgfNc+3u9Bz+OtTpNFKmKXo2EHWr5dUIlGjad4tgsKPfIrsDMEDzdU0eljtqLkqxj3mY+tjxQ1rci+plhWNl44s8H3W+QWcxDmTyoFLcomabYL2L0n7WFxSJG62nJ6bTHmcak7ONclD6SlR0WjM6hXsBolgZV98y9p3C4oOFDmel6UNQg/JGSeVOy0SzZfhsXGiRQD5TFO5+y6PqiFfkAKa4Ob5qgBcDhWEmEQhrvSGiUKQQbq9KliC5jVwUnbFSEUIPwdadk/HZZOhcwr4DAosZPd+EYZ91u8M3NoGzqmNBUwFyAYeGH90SfmcPjeVAM4EYKaOU8ucJgzfBPY3mUrhA67CC8qRVwT/6peQGMNfeGguouApgI8en5m7CC+NjzsR3XkKVtn2M0h7NQAGdfAhI0G9UKoZ/0hXgxh0lQtZ/EjncL3kGLEWfwWD37ZpH+K/e6Rw1GVTaZ7q6brwWHABwH7gHSZd42c4HFNFkX2juOIGEp1wiv2FC5ksJ289eWhbCECOMKQedKKpnG3ZXHX34+9KVDMgTXE513fwlY95j/LkMKRvHX+I8pAEAbeO9HLdRMS/CiKXiiERADv0ozrlqHaXlrw1CnPwe7F6nD6bCTgDhLfGdTUHzjCMmhkeiJKnVpE3rD0SLXS7vqdNoDC1f9qBhkTGMhPPWWJgAT/PLpdQLtbQQs0nS/CGdcU+pJhERzwDLkLY+wwwVw+3FB4Eca1hRYC/K+sJEedMi80CHN/tPcq+pfLXuJtqQ7aI4xsvJXfV7zPef75sNnv6PdMPlR3X3EB7tyXodjYaKQh4EV9OhxwDGynEmkhvMjmOwN7I8akRKvEN6ArMF8x8qBrAkIJuSicBOXCkvgIgu9C32GiMsSItYAw5D9V7H7V4WxRzwhrq3cbUphRGM1gODHIArn7lKDzd4wzLZeP68YXVe/ri1aMnZ6enz86OHz8/+UYx2pyhACPW5RXdEKnVodiCOwpfv379cqMpeFhVC3CVzQV416jCwaWQw2rxi6tCgWhcLUAmHoENyCaJpDFmXtCqB6fhEgC1oCNtuEC8RZdz+o29sQSx16rKtwqt9yE6EyyyPWJr29fFivH1RkshKnvleYGQD6agJkjYV+tDLRKBJURUVZlo33cGzmsurL4J2VFZDFrs0HReN6LEpWQ5MmaciSJkYpDNCD+DegAawFsDXjYcMZaPQ+8ImWXqI+DA99oulo6tYYcjB5InDcLXdUdkOQKT5da5P0G5+gtKSNKRUdZ03U6n6gTwgUAkRlTWIf0HqLZAxd+U5hVAuwI6IoJ6gJYv+bvWiO1WSh6FaN+AE7ElWFmmEC0lCkk3rq9qp9E6VcCjPKs6hZ9sF9AhZdHLLnFteVgh5H/FelYgNRP6cWhuUB/tGLsFbbS6CDhx+qKjks+2s/pHoN61sUHql+xSHIOMkHClssg08QabZ5khHV+0Uw5aW74ZULoFKeHmth/x65WLDV6Hdxv5SnQ++zBhiDbTLy1CubosFleE51bWq2Zk0Qjd7yeoKdVknWG3yGvjTiMPELsuBvDMjz/2rV8oXy7cSH/8EVeAPqenOp03YajcGHCNU70KQUIAjszvIXj0GdCzYzcvLi7QtcjRu5go3zKqfcsFDWUbL5Ce7jr73l0xWHQJlxKIvdpVD4OBMhaFRqpEduDUCPA7ZmYAyxRMgzfDJHEi4pRjSZd29oRUSDCkHwYEytHSYbtG90tkk0ZOMpwaSh7FMFE2KiaTDbuGm0ddyqxmPiTx5XmfOegfRhpMGqk4S1BVwBDNar9d52g+++yb4+dPPvsssIyxTG8sDLB3VVD07GBLn3326MWbb15/9tlmHpUp+yCNxcC4Ml+XFGW2N3QZzUN7xH15WS0G4vZEZJrbLixh6Blk6mw9srrs+Qa6Tu8UvZUTdwnynaO8AljvQYMJniqBisn4ApLgLOKw2zX7uawxAycL+H2RAxBOg7N0rzYZr2EC/4uzub5dTmaG7t6fM7RrvAVSWSGf4kLp7lHWEYaLAgdCINZsldRbz1q+4bwn8vyt2aBOP8sEG0XMlH+vmpznKXBJgpT9N5UXdkRJ+nXkrv6l9OvDo53Do52D29Kv7+3cV/p17f8X0q/v3I5+PaIq/1QY2IdukiUkT7RIaSL2fc+i/pAfC5jYU+2kCdl3XNfHkBY88dcmx6FbmDtv/hA78BNJNkephA6OhgLeGNe9gXLRI7KJktTp9x/cJ+L0na0Hcj74L/ZIX+19+ViaqZbulEvs2JwjRGuhRtfDcBCLQJJS6IdOJizSw9/AjMKhpj9PYf76IdiBwtp8a+JktsfwmKC2krOHUSakl4DbxM0aVwgTLWor1YORQq/BM9zRlWo7PuMgjK6LkGXl2fqpsW5UAaNrSOZjlHGJNz96o6FzkHX4OfGayS5YZdJpWiQbLExdY7P1fEw9n1wDkb8hjUBVneL76V5E/7WmOJCa7PYNQpmGy61jqhRxmeL0r6pfKwvwSvnUqVjJHZyFSyyTcXO3az16rx9m0S97QglHMw5RZNhw7Qez7ewraj7coycL/CGfKIzdWBZ7YQ5p3TPu5LTJ/ilHZw4Bz5GA3LbW3gd4JH/JfTDcyYa7Tq4cHeyG98HBcN19MNzd3bu3v6d3go7ht7kT0kLzU7ka3PcME0tfCQf+SngmzwV3gm0geRfcG8JVMK5zMtMh6qF8It6F3RhaPM1WM+XfO6X97t79w0MplJFKPSCE6FF2zJ5yH64RzVhAsmixeL4/k1iFIz/3PnhqRn6XIiiTXp9ghvc46kvaBqQyFgRBLshW16iCRb8sPbkKgRFgeBJJ1kJx9EBOBIrhOMlIBhlGunTUvl8G23Lwcg+Xl4MLrL0XoKQ5BYwVT7rftWu59MGnlZrhNFK9HUJ+WFM+xiqIPbpIu+rWJhHwBGiRFzL3wVXpXg2csk7aArekmShw2/iqygriIpSLDZuJj4etz+wRuFGllUeQMlZiTcwGGcPpQqFv41k3Y4TafUD07AQcIRD/tsSsRkzdIWqm1/k53fvHo5E7SOjGc70FiTR4K5MvP5/d4OV9XmBsjkxYKbIJdCOUeFO9pddzHVk2IEai4ZNk0pSrhk9EGk98vNBDbIilQ3fOYw6Cek/mI6Kw/g65ZgR6uaKv76h4Mm1Ip5+fFTM3WU5kkphSoMGZG/cZpXbrV3zJalmEyU1qQ6Xq7xm/70uB20dHbVWCKwDfUToqyFTOuFAYhb50mS6G7BOKCoMwhpLYXUMpnEa7uJBIL1CCFRRisz1ITIxKShxDL4qMgAmngLjmz6cGNcLbDCT54Bp+CPIPHAnqVphTbboZ6rX4dikq0peeS+coO4GbYkps9xSYJ/4ZJ4Qu63zqCZZGUAp4sJxjuRqQx6ZGTDkpWPhTYItLsVdRnnok9mzaPijmbBYrFhgBCKb4GVztENX2kaYNW0dx9tKpIuhcwnRkVkdF/AhwRsaAiZJapy5fXEn4Q2hAq1mxYee9wKHZC5CubdcSyBfB1dIy5uxsEMBz2brQokIjwc5ccZtr6MH7s9FnA5cMDgNSt4uFd27zdsUVKTGZhJ82oCGm02FhBnelBmj18UlMvaa1CmVUoO0tsCJ5hamaeSk8BheSfC5MWKp6lOC3pwpKRPQj4TWokiTbkb9qQpKf1YO5KIoxouDVPXkhMjdA2hPREBzQJWoNGIjuPnhU9ZYR7lOgfEUmG8D/ApCa5GfDugbqBVK1Jyu4JD0LBC8D4OoEEeuWFUYA+dZ3QNe5m96rz/O3mMJUF6nK2VL0PojLSkiW+lOZibciWqoSjXI/dlpsPi/G6IeAxVkAyheGSKSPi/ySXw/xto3Ht1JZl9GyjtcmNXIZg67F1p9PX3xD+6EEg3M2nuAk8QK5GXY3A7lCJQqfm4on+MOKMLV5tgVMVPhRqpzPZuP5vkmNhnbi9kcc1FOs0SkOSJt09xS30ROO+cluVgTMBJD/XF4PFDans9CGi3QsBMZQwddi63JL5NExOETROuEPSO3TP0l9KxajDSWoIpvdnbQwJV7RnUGPmFAI5bYYin19LSTUDfp8bJNxjjLJ9UdOBiaddZMTEAVthtOM9JfncLxlY0jyHGRcmpgyvsdzGCuW/AmlAt+5jGOrC2dSN1f+3j/HYHfD6T0R7mXVWNhzQee3VbeOU9jdav/P6/l0MuDz+b84CNV1EWFcvALXv6WIkgrNqn1piZ+GX2/VqEkUE8CADYR3eV0ihrb3r04+ffHtk1enJy++QaehcOXQQcKZBUJcOIlGn5BJ5WteLAF8OaZao+u+pRvLoZflQfVFTFHM7CD+sAFVUFjQhQlnGo119zmXXzKRgO3aaTvjimgRsTAueNjy7zGxYcBLoSEFeS1/ISRs6OhIXYsaj62wz4HUIQjiFCCemuybN8+eEQVtRlUNpz64IY9K7UEyLIhbbYFghNJfCkHJep5z+PthDtAYpz89d0oQahnEo4xie5WV0K44JbrMq2LAJneyc7cYgZLHgW/BB4KVRxlsXIgIzx7pL5dQYKEYa70BAHVYKkqfgLxq97ZEmgzTw5W4AgguzXdVNZLomdtN7j5OlxLcWyt4xHkiN6scSNk+PXOuWTXNiICrFhJe1lBUu2GDCIWUrhbdDLXEPcuaszfg6XZVAibXIsSKiGn35FNB1t9qLt252UZ9FZX2O6BW9KnUhGSJj+9qlBKQe2ANxFZfJz9Xx4RK9OPUacTnbvtq4TYyOIjO8Bhq1LHHAEgIpXPobNLkYhVWm3SY5MirF0vmG/iFTXj/IGi5TbadvXJmXZoUsfuCpgjvV3U5btkvMOcCrau5ngMvPCVFxKoEncldm7OOE5hytW2qP6CykBMeCwzAqjLlS6HUyLtyTDysyrqnBhlfbXLVlnXCnYigUJR9F/k7UN6FxC1qxHL6MUzdiUROUcKc/A3fSDQSKtjoT21YW8ifTktYjJFojCxxhRlcMnun6DHc4E6xtMbRz6LR8PXYKNQsoIWxFVdvtbjlDAEkdHu0WxG+GaxHsQ2qJBsylCnZIj+KOqJSj80ULu1R1UBRhpFo0uXM+oPjt09I61hWG0vzCfMRubUCXGwxFs3gLaRjlyOANQLeuxy9VdFJHBQ4taRRdlixHYFJoM8eMO1yxHmLSm9+SaWIGLoLjBNlbPERzLKjXyFRVc+1pofAxvAWqqK6SIgrVeEUCnPhNYxkzittdFhLdhpYn4E4n2+8NQZzeZvbxRbW8a4b0XQxLpuHiEJ4Oy6cYGZsRZ/MYev9s+h3JvILSM4sqU3eGG4f9Egj6NHmJi91bU8/YaVkrfDMyx/6AMjC2x2x+M7EI7ecI2ZtfkNnyx+tyqT3wzHbULKl+EWELWuez8ahYANoN6AeBxCr3KwHSHA01ScJPnBVum3kE22BOGnebNYeUfeQNkUCXs4u0/m4KQM3+Jy2bMk7RLL/lrMB/yrXawJdZpxpyVfGphexFewYGfECeI/L1d6o5TWlgAOgumej7s3QBKxYsNROEM3PK8U44UbQory8XTEDK203oVgAkVo+BcWAz3yT7e/M3xOX3tzaabxGUPyU0kB4tc6LK5CObsrAOUly4wJLnSS7fZWjJl+8L0ZLToYtq5o5KsmjSSnppCWIBPL80rLvmXwf/SFs1uGKQfrzZpP4+amPHmKOOzX9ORG0y21NWc7o3eMtQo9temoF2z1qGtWHVSCRBN8gKEm0DwFRmLiSPJwQqN3vbb1f0VxY8VorFfqYaS7sIFx6iV6FfJtSLufeH8NboKiRkqW+yMmjCqJ4TGnorsXAb5OMLXNFNME7g3sTzuW2Yc8+C8uOcYj7zUmyPYj22BsSrCaNrgxoYK8s5SUiXLeyE1QjFbk5o7gHc/W2fwUvxD5tqHwBI5JeaTlA/hp0YThIDqa1mdrAGEAmHwZ1R243TJFltijQEE68zw2ldtNWeVrRh+7tKqlvlRTpgK0KO1eLgVJpD8qtdTYfnA90tVTzYubjp0yFJD6yDbXIurgE0jSUk4DN5uSUE6y39bB6T7I0qNq3OvoUbHcxPpmaoCmc5VhNhDiHcn+M/z3r/QHKbvc2HLuYM5PIyKCIAGGRHwM7wqr2uOAJajuaEsXuRKlT1xUmEeRe8T6H7c5V7FGUuVX7a7V8vTwvAnR8QkXVY4Lg6gjpC4ED2CDIyGP4tScBFoSpRumNDXxAMCfwsdAZ4Q5PmDexTHkKiuWNe5pqcQfPkq6SVCCD5We6Jo5FmRppsMUaciAwIaFHTQvbaQe0Y/hxSPGSbHc//5TgxWvT58k3/0WTdyuavBBb1A9R4xMV6wUl0y+CqdBK3P9Fm/dftHn/RZv3K2jzmv+f0+a1KKcGWTLPJdcslxrKGxENxqUnGJas3XJ28/zRE0VhziIxRtcW9CJeIPKPQchZCltL5hRE2zyJnGTHbHUPMTIe0FS7hgvHozhRE+DQusmYK7ybDQyD3mMMtwFWjzrt2V7DDPOX33bRIkXlYkC+Ys7Cw5CLvZp13+9dvXL0H9MOOKYQpD4JfEk2FL23lP76+Se/iRjNH1Iod/b7HMj6uUciYAP4JbvqeSCoz228SDCA/Z19tp2lzn0rbN/d3oir5JyD/T0rhEl8VpBDFz7X/HkMKVM8xo/QaTFRkz//FOx1qXSF+vgpV6bRba+bre08D2YB+JeriVSFnVSXl6KKe4Cmkt1EHm9op8UkpsYvh28DVDBNM3K9CS9IYJTfC8bmHcC01vLGvEQSgywhHFa6gdcKLlJ8cHEBbiM4MeiMlvQN+Skd6s59dT3HGso//3TH70z0Q/vyyz//xKrWXY7YaF1m78kivvd4GaBTH5KPEA6AVhQQDqNXx0IwNgEqq6CG7JUBHsvNQ6vDqCSrZINFTY4qhh2AwCRHlaVxU/gbOY7RU0YF7A3m7td2knhrMXLQbNgJohCZ+z/CpXlY2gtYb/idjaCePCY0kJDzqRqqCir5n6+RnJ8jXFCKyAm6qbsA6hupbgTO0RqSXotzykgQFYCHEpmldm9BATu8cRsCxrTwJ02AZlpIXUf1eON0xzG5CEHVGofcXznlIZejrAfAvgEW6uz5mcLL/l1VIq7lwp0N4Z30blfClCjtx3Szt6+o6h6K+Qvh50CkSi/76uSpBXNQKALAc64nCuFjWC9hP+6sQIbiIVJnFBy2SSBuOIAm0AW4fSG6DadJrROiVKxmbWRWOsuj3Wc7q+I8XMt4JQGYsHmH1uXADYDqBJtUbD3xAMvbm/sFNQsuKJk5M6ssVvKW0HImSfBjHCtsDl+ekLbMh2w6b26yx+VlCdkDjznBq0n1k/Ru2FlzKm85zcXADkcgwgMjidfu+ZvstHy/cKPp698ozvATtgj6xG3cRxZW9weYifQlLGfG1wXwV4Ai71SylZ4blpfgnWQ/MKNUK0HwCLjK3QZUBAkQLITMF5lDUXoRO4At6eORL8d9KyTvhtjcDfxKz8A7xkoZ+csW7h6s3R3ecL4UV/IiMCJ7EFa7qngncJQ60B/ezMbV9qtirIWUrUaT5N2hVhpp9gIgP8kweEv372pI3ITiEKTXFqeh01hG8lmbzpNTa0Iyx5cnrlGG3UieTSUGGGLVAC3rhmzoZpcYzy5qoKJKAQ2CRgGhCX46BO0GqCzLBAuCT2bAu9BT9YO7A0K2bSn/hAEEq+UbVjh27mLpc96cGI+ijU0b5zbQwHAoj+KhUOp2aF0w1Ac1Q/zamzqkfl78MhiJ1T3w7cWfSx1OETSIfs4ZBYKbeUV+9ccvngvYwBRb/lWTMXLmtrvObgT778OMcimGyL05hPXmuBaYqcqWOHHDzgqrr6MR73P4fUGhW4BfLq/A0FSMpoeee7NFHd0oVti9yBFMe5vHKl0bAbjwRZrZ1q2YmVni3Zrk5gQZMAYFiKpFBW7aylkONGrK5pE4qshq4RfiDkWDIx1MK8oJNztWiq8IHAOfU8uuy2pU5sJlSMxDBImgfE6BJPh52XQ7ME7aI6xAdr1XLjt3jTY2MaqZlfN5QRAMD+1OJaUC8va8EMYrGixYyM0SPNYUl142xWp8L/pA8L4gOZpPZLO1sdo+yIx1ja/KS4iCOdN1uTq01WDipBOszxI6ocJZLQZGUerIZU4Q7nE5Bs8LsX67TV/khk6qr1hxs7mu88YSWhEwa21uLY0W1+6U2LFQiKAVClhtcpwk9eNhixAuCUlXFHu77hXTyZN+RCmjzDzAvGiU4wEq68yk/1FxBLquSrKPfRaeQNrJ98EQqpBOJxqmLQR+7V4Zw8lvGpkK2m9NRLMlm4VIL0Jbj6Mt/CpIBuo5hVCvQvPDZs3BzYWXhPyWDFvm90acwSRSqgN9MnzBr964O/o1kP3oDRP5FPOmLTMiJwfR9oYNR+fJkPEGsdUpZy1oF+yw9Vf46ZW7BxT3DpjzyQ1PUh5J+GTPkfdHS9gTdlLL2E+MYk+H3w+Q9Z1qhBfL+OefiI9x1oKcUhKyHGLMx+IiYbBqdLdIpnPXoCHZY0zzgWKRr28DGRHeZSVpRF1Cf6Ff+8LPuHD0dLrXb/Oa0PyhWopO7tSJbp3nWIGmAw2eTlOIzfI/QtY0MCn8gJpWiMZZ1SThN2ea1wjt/w0qsCNwntAYzJ+MfnuPJSxnRNmC0cPzGLaT7lOL15cwbgp7pwrJMfgKiRmU/h8zWbjQXPAbFN4B3jPrUvCtpNWcC0RKIn2OGCAjDIHg8UQSdtCRPFa0gpmnieeS1/wLqkzviepWyb6UcRycrEDvxtiy/5tBqfmSEwcMZWGMcLcgdrlfg5KseELdJ9dVNeBbbIA+a1OTJeOCKetep8NskICCwEGSCHZOX17Viee5ZHeMlIPG4JVnLzP0l1zSFaIe23iz4C1QQeGkqXvzW70SXplcnEnJFSh7B7KMJogXxviK3FG4z2CWg37QkOMEXiYvdCOGu6aG43abPSriDAl8MeQHV3qm1PYlcT1Mu/r3c6klsXUc8SoJs+xVXS0viaE4v0T/c8YsMWfl+IuTx73VL5v1xuPJgAs38jVxdn5zBu9LBDiYpEfPBplgyCd/wxValVoUpc7nkul1s00IrXle1quuMa1Va88Ksw764r02H+oO5pGmtjX97O6q7sBw2EZQDQJbOe3DqKcTydhoCF6WShbb4H1MJCN+uURmRUv1jq+m4daDdm5ArMStSFfKMBonKEWQp2J+0SUfKf4+W9zmfCDwy6ZoPbFZDKuuy0l17nr6c/4u58iEZs+uxmrSJcW4M7CZIORWigET/z5aiZaXK+UxRq8EnDQgkkjNercL3Po5xcHiMxYwP53wNDDg56Uo/uRGXe+ka/hV7ehbIRVrxXvo45Jxj8TY5gmZWDCFjSRn8JS5K+Va1bpl5H4BE33GKfhS255QjJJHTZ+vs5gbn7wNr3o81ti5xSPHugWyUMBloiWdywuvpgCo4k7obby7YhywKU9pU5Iu72tmhDqBeMoD39eM00oiv4jMyirwZ1Ej5LMKKU07GurTkTR5BOb3fmmNW0ZyB+rlLPBhr8zXC/QeaWwJrh9j1LR2Gj7NYbUa4tpiL/KltmobyC63JQqW6DMcU9qm2xzE2L2RjExxTnXRybTZLJSqcJa9qq77kDUTUTtB8GzTjEcGZqCJp+vqJ5LdbObusUTJsQW2aafoI2sYq8wSJ+Ha1HQKKktF6Gg8Z9dAnXKFVsiGXbKGMGIqv7BvShNzSiwkKVEQlAKQrNy0jLBhQINoadVHAaDXUtnhUl6ApEefJWn/poAnOoRTTO2t0iVTrv2ufozEj3BNRlaxBNU+lsxoWQs/jyksKRRMyeFwUBECl1LaUdBbeMgrcI224KU0YFaBvS3KsruDbx2O1hRdOZjS4DNY2jFx1aphudmM4UodcWk0bd64ulhg+bqiUVw7/OEkkNwmgRH1tbata/3iISJ2BdE8xwsoQ5J8PdMCZGjZTKE+oHaqvD4chiMC/3OKBGPyOkneFX11iXSm0Uc6UDl8FNVT076j1VYmZF/TU3pPQXgysoPFWY92NfhGmYzBZhG31s1PsMQz6H1RmRBhRe9iZyo3DgLGLrGOJdGLrjzmaAS9x3SobRJzzxcCdaOaLfsQdKdwV/auJDxAwHJG+K5ZFZ5w9f51dN1CSmFZCHY/+RxwFjNYlamyaG10zHA9wVlqKldsk5Sd0Q+KR0wrRmxjhRRNe8Qpn6cbTmXzOyVea+oGkHtW7FFx8FVEJSKOEHw69FkAhgl7dBp3nr11EgSlvIWhiSTr05Hh6N80++c//s94rPanPPXPf/xfmFz35Vn623T3EciHOd1Nl8IjfIkMz+RzAVWj43XE72z4PIIHYwCbqWcjtDneMaDptRXXycOp3YDnHtBQC5/kRHTyOLlh5RkqCUGxgfXNukWs88ztAIjvQPZ034dh4HYgn2+GLILuv7Yrlv0UYoVKXX5GC6m8Db4D1eRSo0EXMpsBvgaZyOMk+dqqyqyI8URVY8NphfSqel6O3kJhEkZQUaI52ueFAW+uXXNTJIdME9ELchWVBnT51MnBC6ioUGen+UVel+sH62W+rSIYpZFzu+tbA6io5GziIIkrygMuuVBrqqUkjEhbPdja3fBHxpVH0S+6ZkTxYQU1oR/e3xruKN88/Nnizfa5mwEnq0AU6sKwJBATAFBpAo9+PmdOVv4DbxaIb6R1YSV/zaWjhqE/jTLWcRU6gI+2LQ6/lFrcG3o3nhJNRd7Ibgv3Iu7Ep8Km4v/ykT0i5Qi3zTrshTYToBpEuKHUICA2ToxxrV1V06IdSRck65HmclNdTuYxI0yf1u9Q8CrDoyN0FqEroarJu6CqCXCLYatkVyJ3WXJr3e80IAMuxgs6VMBVyJ6SYgw8znkU0sqNR4MvAoie9MO4gCG8sZ+KzbYqnM94b/e43zPbsAWJm7amblNverjSVG5SL0YMgM5KrRiTwcS91YxwCFrwxFdg5GBNOtkeowWNngvEUDGGtZIiX1ilirU9MlLoNiwbznrngWr5A8RmU/I/lTpzOzEcRTANLVphenuonxpWWoW5ZL8GuHzwhkR8TfkO2WMo8IfV5JjjyHbXUbfl4Jfw9A8Hu/eBp3/34Gh/L+Lp31nH07+7c/jgcM+Ubjn4LWn6Qx77T4Wef9f9w63teCDWwoAu/zRZ/54n60fWgIiqv7uxJHH/oXv8Sd6UwJ7MaaaIOEIW/wIJ+gO/vUfMoRWSZOsf3h8eKFt/ixHPB5xyH2wSLnKvAszo6EwxzUf8wGjKwAe+6LWv/2WpL01PTJinz5liXqDCBu1R2TC5BpND2KTL6OU4BPdDEecKrH6XvkGaZeVUoRrFdV9423Fj96WgGOq7mEHy5tWzxLCI9EbManh5NIZhqZcSEpUbD/qdqbPVejnA4EVDoOXgV0L5uHZ0YGlabkJxSBGT5flNEE0tIRBkuaVny+l5gcxlXIrFkqF0d2gLhftSV3rpUeKk2oOEgcAfHo/fwSjH2XPjv+s2k6RopNPnSnY/jjFX8oLjfBzMKoNyMr70K+NUp8UP1QzLpYqfDfFJb14/WjuE7/56evLdX7+yu5koetSXVsmtFJB7rGu3xbrqW1bnHFUd3rR9YW6x+juqDK7NPpTHnpbkUXIbTQ6s5cnFiZaqsVQUlI6MYtK5mBlAITZbOqESoXpQKXFRzew5Udbwm+wiLyfggeIV9T/ZqGdxnpEBAroD4za59KK6k8k/S9u3Nctru0LGbo+gAwxRxfhsZdMIYrbjZeHTVsUjg4TZ7l+rugtqloMMw+X08BMC4PEOQMZjDHeBTSSil38iSCysYMSIeKeuramaHWYyMm0S9So13WfZ8Z+P/+KDBnT0gZtAUqH6VMgq8uSvLDWV7J5ZccP+6RWpcIvIY0gEmBFmdKP1RDAxRbZ9SOA9g2iiuqp+2jntmDgh6ZhJWBVmJMzemAnnDEtSxAeDikzlPkktOC0Wyzk63sAjiROJkfpqlrxeFM+cAsDttc0Cn7TaYcKbNdP0NbrDv/93BPKWQjofxGGCxgMD1q/dGEDnCuRezi/rnLKk6VWYw0orGcVlSoIXSyr6LRil3E/UgQKtiTfINdNnGDmD2BlqK6UH6vzyD6b0UnIcLX0suCvJbJtKsIhrHGx+9+li4zITP7l69dh0apEJAEiOorUqyYlZ3hdKwUyF5NKFuT5+OGh4YrzUW+9hCXMeGQXx/KRrC13T2b16NuMe07ctSjLDtPagxmhWTN3NkZpe6N9DalmDiI9SwOe78QZoF4JJLGjXyQWkhd90T94D1IriO14lEL+WMFJUm4+thXYhxbUuuKRGw7ztU1tNXkKCrJ9SwJs6TcRy/M3uYzruZ1EZGFGY42rD0OjJY9FbZQGqWnQ7nC70R9hlLq3ikOoHSRC19jyqWPwGwl5xQdq7mmlkI/QthxTWu2Zedqz+soliF+tqnolKEdB4TSDUA+NRlBhAWghsk235xTYcbo1jruo7pSXKezPFRzOnkHoE8vPYa/LP9TG/ajmXYuAI6QRVdO0NLWESQ5iBzgDKkqYh0X72arvZ7+uFAQobqgXo8+t9Z/yNPzaoQG8gY0Bh47ni80bi7bocFfawxYX41jedOqah0nuEcBxf7VrWTIIBFZgspfJWC0BACtGwbampJXxyu9Sd9tVoxRFcLuzlTLKWUE0Dtk7EtmAIE2GMbVGqG7zzLquFFibceC3M5vVnY3FdSYRrdu2OdQCZlw34C3aVXf6J5QiX4How5+2stHh5NWuOhoOxgjVH16yBSX40lgwdoSAnSLaMHf+abuL1Jt4gzMTgVwWxXfq61q13M4TovoripRbDu91rRlsbb25ZCJA56qQwkl8J2DYyA+WyN6gagLM1xkG0me2e2DG0Cf2JFbeAqvcRb6TWAaCj6+3xfMPTmrhZeA/4OcSTozx2KL+ou9AjwwFRt6esvkF3Ex9W2YBSJDmj8uZJHaOF0QsUYCKmwwJxCUcV+M9WqoUnItqC9/coVZF4XIGKHJQ3HhlTbdYNO1Ho5CWmunVldXiwErcf37/e++S9bvo0FlFRQ3bD0SYum9QvASq+oNeCGyPaEHno+SHZC5uEVp4Zcg1zf5x6mRyL0KbwssgIaJdyIJA9BKribzwFq+j1rV82dWfjsUQv/5H9lXctA1O4eJEnRcTgHq9t+C0FjMCyJaqS6xL95IqzsE+jLiN2G82SfTv4T6zTWmyPFJ8myBpPqPgH2mIsOJCkpVtYEnqLBOfEkS9D/wRpulheFMop4DUPQyhyZ4pwHK67s8jg9NNvgVh6TBETzr5r2BLnyGWJKPAbC76PtlcZ8FGtvRvk1+j75GAjt0SqDs2Dod9K7LzdrWGC6fjXl9ndTZXvZToOnDdvPFhwHARvvaurXWElMSl41FcZJ5CMDX4tksDgPhg7wXiz0d0L5w+4At8hzOWiknGpgmiJIwrJFhVo3DQwUCXDmNAeUYlgiNIl5VGdW8oO8Uzw1ZaQeum1iFfiFVcJpoPkbQ/OHWNPVgRXc9K94Jj4hKB/YjoTCiiuK5qaVGx2kp8DCZ43ca6dWJmTuCHN3HpMkF1YSg+l6KuTzjry9JGjSaplR95xTtIHLvAaZWV9Q9l5GGTnMBbVuHfvi8n/4MLYaOMIrBOlBZ9oehXOlbiqtO4FSjKCpld1yknY4f1g6Uk7ks1G6EGFVdJzoYa4oWknURak0bMo84IP/WZO3Bj7QAnuuPdkf4Saq/yJboraRLs6O2h79qRFSmAYsIBH5V2IFiz/MAd+kNciZ2QMpL0JY3V0IDV7PB+1dfPgMuhAr4JwPy+QZsaYX8cvT9ZvmPx9NaumN+YiyP0daNCP4grSH8yqDOitC+ZVXaP1G/dGIlBErOdoNnfdPzGvpRCSTIrLfHQTNCm+dngNBN8CIZGAJxsrpBdB4NVplyNWsiCpIGgyGNIroYvFRYPS5AHCKXS0eTwbRq0mN2IBJ05b4kLUnccKoGwf4hrlYBSQpHKcVTyia/17QYtXZSvAR2qNh1/lGzQoyH3btLCTY3I1C20V9sAblhh5Vx8m8Emx/Zq86cHL9kOeboPfAL7S9NA9DxaBdUndYC6sUEf0SRtRZIblPB4e2oA+4NDhTpf4GN/EEnaz5O7ep9yxbwmSwQWn6/LyahGE4Fjsr78dE8HltKFtDkpO1R28tmQd3h3vHIVzrnxBTHdiisuKIDUmmsAhd5GtjIBB+MLatwqPYtuDwm5ONPZNOgvu0RtDXhmgIzfsLWofJ5PD9HVIP9oIClRiupu8mE+IYtG9xKAu3adCw4F3/Hw+ubE+KtrIm3Qx2KALKLAGGDOMtHKcQEUe14tXrIc83CH9EvIvFv/YtpCE6ab93qODVyMdonxOujxMYNCbL36TLKoFqp0C4vyhc/QdZgfQx3jcpG5qe9GyKR9Uwalq7ySVhXicN1dYQiq9fKYbE27P9ef6piqw7KntEhQR+Ucb0SOAe/Vtk6zK/QIhtAGxzOmpa9uBQOSwCN7dLJPRFyUXLQAgAuu71t4gbeirNyfdehVWvpvxwSXQC62/zylmy0u6OSakREnlfmkaOEUgcuMGHWGW/hWiP7AXuijM6yA127Im/571u6KQCM32Fe+D+0DuE2OVsAgHYVff+FvLpi3DHzN/y4G/d/2mUM3BZ5pyTgz1SmaauEyMJQs8WHwPsIeQgGlXRFxG9EbSemrvxDvnG65PGYaKRbvQkh36bODxFS89Oqlv3PjfI8XbBK7VGGH0kECczcjdh4DNZl8yGygM8QxMHvCMgVnvw6kJ1zU3a67JC/RAdi58+gdtN2rqR8alBytA+839QjURw1aTVj9SrQb3YJ9m89x2qPyNJbb4vYDVgY4mWOPQi3cCNw6wY48DuUD3kWf/TZRdas+R23ObXXBpXzpuLNH5qYIyYrNEJ+1TNMb9ZzG6K9rRZ589ffHq+dnj49fHn33WiuOHqomfwPWHD8peFRJAAZ2aEm1+8KYuhf3+fArCCplChahusaBq7um92B2SLeZwpc9GN0JlPz2Hkm+u78UZwnPF3qILe4p6KZWjLJu0+9s79bm7JhW2oXW6wChwMVhUA/efvvx7Chw8WIbc/YO/XGHHrY8SoauBkkzZm2ah0RHog3I+VM0SBGPj4bQkg8FiXz+OUJn0kWpRVuG2x4AfU6ERUScLOx8J1OAJj8p7DVEN/C3GEUF1b9vlRcmFWSMkc6PatSrvhAuti1E5L7uiLCtozShbby9xy+xsJVxhxo6TGJ9gYjgrPhUPFKFh8QHOQi1qe02TBJLvOwbUoW2a4Le46fyhQRCX9TripOKl3fbK4IDQv1RKBlt6KG0fLyeGCXCBnbbhvRwu8yqUTrwHeec5HdkHbnQfdmy3TpZS4whgDT/lZjNhGLAEFmontG+itvswZFrQnDDKWvYRWFGWz288xaNO28ljcOZelO9Xav7EmAPnBGF8uOYtI7gjxuX38xoMVlgoAlVFjkBcVqaEk2DPVzgDeK9itcFWl+gcXeE6aYWJdVh0PRFMvc7HZRXqTyJlPEKiSxZSJoje0zSf8xytJbz1W7g6eCDlk0xtwxZ7GeSRnkOqF6Tb2T2Vdj8SZNvHNKVuNwBcIo9osOp9JkQVTxcxaZnoYKO8qRfsGmsDqEDKLxdXMWH/jIKcKI0R8EBTEhm9bggYjMA8oXCkLSWkHbvl4fU571TGpyOiml/+fLGDLFfZ26pfL3Q6rWMvBjkpaB3QBgQGS/HYgMjjto0rTSRS8bIhmATZJPyKyUQv9QyaV4/8EMa5lpv7wOtXmde2UMOyo1k/ApEccknP2STlGCBXbJ4SdW0/4XRcnVeSTm+jXGi6ChD8SwffAkrwtHWIQguCQLZfJ6EuJBWNG+dpU12zNTUtwZ7oAjqIAo7e5Y6au1C7omqxyVTAU+/yKQQ1E7bpRXTgVrw84Y6aRTGXtALCqVNYg+7f5BnvatKe4bRWvSEAg3o1UcTUoZyNEyoNuY8wxsimaHcvvu6XaE44No+cUhAHUrzZZMk1MxwmH9J1ErQtE9EIZKHC+t5V/VZqBBVE9r/qqgm7Ct8GRxxgwFqlsnE10G/nAQ1H2WgCRWSWc393Ete1PEuFJZrC3MajHEjrzwutMIAaC1Ym6VNNKsxiB3OcuBFyoZhoNFANniMysRhuAu5m99ZVPu4LL/q4XXeCixi6G+4Ki6MIxSkYv6DomZoHtw0mtE32RDSB4jGkl0pQYRaYZ2vwxisGoGrweAzRrYKMM+IuUfyvj+Dyn3ixzJ05YrV50yN1F0YzoSsiR0LcJry0KD+h5ossGvR236xzYQSZeb6SxYVPeJktp6izkR4oBboY9haletoKV50iN90ze5Qk+Q8VaWOzUwnSkpgUMN9qoZh2zF3enSJqo1nraIjrCKqOn09Ahs2rOTCZk+ecGSpxIf3x61wvnC8TuQv8v5GPAsQ289ujdgQRTs3Fq8gj26Gyt7DQKLRIppK8Td63KZxowHSkmy0hx5VvTl6LuuJUxc2bNipFohfNm6tRVYHJ4+rzsNM3NZOoKqMCy73zlwpPRO+nxUtElLZsBODvPOyEUCV1SoKwybEMkw+VsZvBc4143oGGBUpoMEFKc5Vj64CKSY9pTb4WuP1iYMmFh9Opyz3Hun6Bm8KAz+HRLnCa9BSU6gkBcozp44GUHrQOWhD9S9yYC/Fiavw6NhDWpny+IoDMrCml3iPMeA+VzGO3K8a1O9lNj8PAxMY0JgoFDgjP16cA8C4FqnN+A5NSqCLQXTL/E+g2zvCZ/2X0yzUZSG4UL0kZ6UiFysYVV6mRqICllyd1mciz4bXX3mlxVvKAU6xzK10oXmW49hrUcDmljDgo1ofnEX9E6femmXDrEDvTLErgX7sePNWT4oyW8IzDVmdubi7hZj4D2BcloCNywrXwtJyozku/Wu2Z7kQX2SAb+vLJWDC3/gSzA6f5YnSFjjK8wN1Ht9veuKDqr7RilVWDMMdklr1Al2aSETQ1kxwNdYojweQZURBkmsi2WLcq1JZPVbXpqX66kjGWQMq10qM7OUOAKiCXbIC68FKE9Uwo0fHeiZoGkCt/kwxwGBfi07pzj1Z5q8Gus4L85dcvg9do8yvtAb/S/u3ZlQ4Hu4fZcHi0c3B0sB+yK+3fW8euNHzwYPfQkCvt/3bUSqt4hz4VoqWhe2Fksx4QpjFNsLTvCZaO+bGAYandSppZyS3pK7D86CnF8ESxY59o48m2KSuPL/U0xdLe3p5SLN3rIhegzJAWrCKnU8qykJ9ieNRDwhfjc94N3wpJmE6k+DRJoy4EnnJkY9d2SGTvl/V4EHA4JHy499rkBemk5nzc6uHVk9PXiE0FiVBGt/NLI15MQWtTIZoAmsJMBJwla29AqOlXT0n++UpWjD1izkBQ/PJ5HlYQC2RSqz4A3m1XgKOfCSzS6bp279SC+0PthLEzGgiilElb4R0UCQw84RlNzvs6zgBzR4l3M14AM0J+hXfkhgDlZ2ZJKUtJ2nQ7AYxEOJhrFYPkoh9bxD05wdc2RNo7a8mW652VsB5/havR441hdXApSNpmpWrtEii3561xyhjAPQeYZfFJwYbBdIWvAvsuWJ9WULBjR699ezuDZlERhuDLipLniG5g3DQUg5KKAx36fIiFJwl7xsLRR8rJtI0WM6ScBZBlSQM7tqUlVhrspphmLA/DqGtqftuFFlPuDT2VQFOnpTQZqNaw0s/n8bpaTkC3AR5Hp5TkC/GG0JDI4zIrFuCGHODAcmO0ZknFlTqIQHMv7dsmZpB2mXf5eYI/YDpPbrd4sz3Pv0cQ1BgEwUU+4jRtrCrrrsNp+UMbIuojCb3TRTl6e5NduUUv3ImimBcuLr1BsBFA2V/n7taDhDJGOcMwmXorewmVPHEaBPxNGiSGzaVo5MIXetfyI1FLhihQ8IlSQ2crO6G0rz54kpEJq2FngTgJfKsmL4vaPS/gJubCk86EcCJInqUuj7u8MZF9acq1sNCiDgilMpm0j8MIdyVCh8QmBJzWBN6OYtDjcbsY6vqRsMbhmxd0HvVQzugS0uQB1EKUcfxCT2iB7uqU9WmJ5FQURye9LsJ58MNhpkvXGdY8HBBaV50ZHfGEVDdNRZsKxTYUH63YMX9dcqlsHy9HkWR+nO7meUuJMwzAyWmOVzxeVtA66YVR9Ut3ywGGRNEefSS+bmPkaMfy8GPtn5u1SYidw068enCx7O/sZ/aK5rtDyrWHgo9vr66ruke+Zsm29ePtkSASdVryxrExrDxAERBRbiaIiEy/VAKN/auzclsuQ4VSGmnF6Uzsw+dMj6/eiMofJf1Jcmh6J9gM2QbVbBwxZmslxneQyEUm0CiXs+ckXgz7vqd/tkfOfvHOsGLHFHISpOT4Jztr5U7NIiR+8qU6tYTgftf96DTxy0t0InquFs5pN2dXH7dU5oEqa8eQPiHO9LyYAAEeTsJfXxw7WyiqCiiarj06qSMRNvVv7qdQBrBqBFInJhIU1uIYO6/aI7Z/u8IdUdMeYswZ9tGA8zaPnB37WgWUrF6fo7FGK1xr7jVKW5EwRHhk7XJaZu3229btK/Su6G5Ev2CcfYGU329OUu2FpIO0I//UzPNZN0rmHFxk1tFFuQRYYxvHjUWt4SnsGG8HupMYYYLUtZHloJ7qCHxEjnmYCxxU+hX2W6/wxof2vGWjJFMTSZcLb4GJLwYE8T5jo6V16rAwnKXUz3qPwKya9IwEFW5S0Vir+cDdBF01JOjF9lauTQJZ5KvZh6eUMudVd9KqpQRHnA2kFlZhSl+tmPDddbaOsnFVoSPH+xRkwYOZFQzOe0KlanzMhDVxCRNaPxYUpJs70eQ4b65sLrukkgoDbXMzPXcHBteJi+KIz6WMihkHViM5cThd0u4lmtj4pCOjatEi+adJ7cDAthnQp4ZFq9Si7ngJUPvx8qd3V2sNQ0gP8lbkaiNqjzDRWAYhmuj0r90SUsXlcTCjBFTFoGnqaEVuG770JsW7YhI2Q7w+RilMHlSJBUSet5FiwANeyJPHK4cEv2YWB4PQiwNi8HVq1tsnB0803YVf9C7AsL4uAKVy5NTUnX/pGbIGTQHCLGr1xonXZVM/X3S0NxvFSkZX9FoGydKgjDipD4Bwrn1GOIoARh1AIMWx+GGGJmfSD2/93f1hes61MC1wNA3GTJGvc2WX7IN1GluJSpZDRaqdzHeToegCfw8Yf50vyaNEKVx5Wus4ZN9AxdGoTbRZSZxCrROlajNeE7I4wdwJK8LmH3Y7tgMGRdaDu6SH79CT26TnDJl35WUYi/gwQzCHsrXfWjsetuXnXGtUJQeu2hyJfsTbveoqXut0pJnx4oqxEDyGpN7xCyWU4NAXgQSyK47m8J3nsTzqZ1+DtIBpiK/LvBF0+puGvV1ysg2LRKRN3/1FyxmWN/X1isPrBsszlpPFADDg5CgjJhWuAoG0g4ET6gOMhJcvodH5ie4b6cL5BH4zfliRr4h4b2uSpAgKML4LStTJWG1NWqkuzIT2WtVeNJg81mE+3Cuw0zykW+KynRRygQtMri5MIakghwOfR+na8t0rbuHDjJIy55JChBgxkO6wGPu6DORE/Pknp4UhyITtlg8znKTpT5aSmBIhoMaIAPCpfVAlxHsBjM2FWbRcv0zqoJu4ZhKG98GmR9M1pCJSQtQqzJtcnLDX+YnIv/uBlTO6jqT2gmpnATe6cXT8ym0TY2rLmQX8+RqaMLxTEPlgqijPDF4hpsanyhivx5UfYUOFcD7FKanJQ/AeUG8wf5kIGH51bwSLaimFQBrKCahIf0Texjg0Ljj8jzcP4BZ4loi7U7RAEmw/nCJ76+lwB3sCd/9NcBVZcw7b+VDzEYo+2iVhXcnSM2R8kFWgeI2QtFFFPwgoUC4a+xIV2Wk8cJJWZYqNfKDloTRqX4MaPI2mxw/X0eoo/K9Rr5Ldqb3YiUjp1sL3NtTCk17fS4tK6KABxiZXvNGGHgTvRypqiG63HDuzcRj79IzwXN4mNd+/elxhwMaNDt3J/Zbrngbo1f5jUTw4wPMxxpZbNXytf1WwLQQhTw72YwzScuWRdHSDOafyogYf5AeDoI2PMZI6vzbVN1gYd6J4MCLxEdbMYyXcrtXEL7y1rC3Hvg+nFInOD14MgDK4V6D64KEP+MMPNQakeGWeD+WH7TTw0B4nlsOQZhODteZPfpQzdvvxWG4HE0z22bTs9zqWsj0fdsAk1ZMjZXyV2LQ0aP+IaPyQnAED/bDjgjyAnEQOUw9JPMOTwsLxm3MqmOSbguvpY8wQ2h4xRVErvqkRNsvFB04Xjrl//k2lltNyNv6czYOPPWAAZ3G4vXgvpoABV4qb9w88yf8h44Gl9oZKeBUxgIb9sBL4hiKZFKf9KFdRC0upoiz26VEAZa0997GmNbJwUshi1DJ5sWGizeL/BqOKPaaU+jrFQu7O7EKAQ1NNluhv+rjjMVuPhR45RRtfXoc0jjPQMM44MgCooCmSX6dgDR9Yh0xOYejWa+n0NQJlSbZ85FNgrDMuv96XSvGEhGAiJqJ2YBrzhsCuUd7klEj6IekKk9i5vY8yn9bsvSonYw4ChSkoqYATVeIABfMDj+uxUE/ijHoaSXNaLDmnmFUdJlsneVgi344Fqlef97fuJZA+rTsPpYhX6MhfS1yUKHiqy0sB9K9HwkNIfT66YCX67NLp0+zkOMOYtTtvV9XYGMkMiAKXRNGChnekox1AOtq9rYNfkpA2fJDtHB4d7B3tHEYJaQ/WJqQN79+zCWk8gt8mJS2VqPXJpKK5B66rasRZngO8v5P5aLsHPh/N8th9S78IU9PSrabz09xQjgmajkzYSKI+kJ/amuS2V9ji2HM6LW1nB9LeJG3iwSaA3b8t89kCTyTkWvvqS+Ni7laF6t8yDJNpUYFzgHBpnul4/Tl7l9clqshcILBJVAgsxhv1EeSG3N+osC+d8ncDZz0Af9EorxcDQVWoKZGERdrc16DEwzmkGcNHUp5BX2xFjmtrUAHhs2Kx1DdCUC2qqBhy87PjQbdGcnJauSIe3WZ3VZR5S/KNEkKconTZzyjFaM8J95CSrxPIiFYJKzSuLVrK3a1DE6boSNV/g7Uhk3mNFA4tmXLGC7zkq7dA2J0kVOcAaF0B+j4FLBWa69lLXopTp9SKrZD4+iUUu6SBuw0Xpxa0HRTxepwuz1VWNNRE0MKTmfvVKLGQNM/sLMMUf0Faysi+fql2C7ra0IAhLABGZJk0esO87L9WkIhy+uRFjDmFzYOBMdg/DabhjA1se7PWoaFH+cRdC262fyhOFv+N9+b6dqIL3i0MofvCrbI2CT1q5i8MEQxYXAQRXQKTN50ZzBCpqsWaWhQSigPB5K6NAS/SgIgeIBaBlRO8dlrNiCJPKUM8gLazgxybB8kyAMpp6ziRDjj3ONddElKEJ49XVz31ELPT3pqmRDfyVUmdohb7v+WJT1eK7kxNhuF8U1wfZeFJc6tFfATf+Suawl980dLFavYWNhISmnJb1QW0srhaTs8bwtuS0BTs9lZ2xymzB1s72R+zu6ZFBU8fZc/zt0XHAWa3M8XqOLsFbP0BGDVb6eYeV8A8JmmypAZXc92qZjorkccIxr5ZXJHJlOuvlbwe/vG2SPjaNxrDiv7F1w8U3XEaQTgSNOPE+88X7Ea9tzmH00OJ2sazq69/7jQgQAaXi89JX4Hzktx/Pqikh+EoezFrnPhyO3d8idVtIEGVLo6SSK8KyLrHu6Lw93w4+W60c3POO/OOdb++mAmQGe0iZB8F3QGk6Dix2bT4qV4LiFmcInBhESkHrdPxQkswsimEKIOw+rD739GSLprJJBgBb/QoqUfaPrUcijw5CBwi4cwr3c/Q+wu1iyBRStYfnE1WcZaQFpZpx5ktZwbx1tajVIiQku65VzuGY3vT3ETRfVo31lH23aNvw/dX4MGSVaBq5nU/U1PdtJHSv6qEaupE39MT9mr+4rEgLguyMgB8hvZJoqVHE+RHRr3VLYtpWCiSpQMo83sNHqOq5tgp55N4oEui/VdEu0nsnKAhg4sCy00qPRS7QUlvh4e9IZFo8KnPDWtSCHReALwgHhY5VPF6SJWLknIgTVQedJONl7iPWufG1LssauhIKagwk3r05tUzYjJGn0xnMq67WpjPjSm3fAHXxhKvf/dI97/XYtxZmnNFmDyRWXyUPayrtwjy16oIho47NCrc6jw6PU018jROuNf0rSDMCVkdpAthJVaoN9MiDwlOAudnpkoHPnLmy87WHl7W3zdcg9zp/gW5DFc2/tKUr6ABNeUPLMap4T8mxwQMee7edLort38jS1L5JUn9Uh0OL4+ZXeqhQLjAjU8bugS1ZTaTQhihBQFy85wP0P/DjbeEqshK0mb0xzAp41HtdBujLdpNFCTsBtt/pwXXsPTCf3LHtppdfnk9Qs/ImdvqZ3LtnHHEjJ5gx4AXAVEiTGqFzRvgUP6YTljpUvBbvxf8ii0uJcuYWL9VGHGSIbp1TCYHnO4a67128Uskm0JvClWKn7ZpLyUNC6VMV45dywsKZ4+Pv48zcGu6cilTxa+abpd//uP/bbix9Z37lzHuGUGt8IslW0FiMxA1Odox5zfJsZ4vbwZVPaDcxMRYV41TeqCt8N2jMzbxj45gfs9UVDUCdECvsHrOnJx1P2ajgHZV4tgcdnjqIgXyz6ecHFYje2frAMhVzZaWB6i1xMujTlf8w6pCcfiH7HiOrhaEor2km4oSHVd5nA473HFH2ff/jqYXjZGED9DqqvcRvVpBhB/yBGAfxOXHgxcK3uQoHUkIxnevBf5CveGL3nwokRL+oBn2VP7zqXskaFq+lQKDg29WQv3m7d+wBtoNdjsUnxX/dZDS7sU9ttK09ttSUhvZymEIAyUlAdC0SctSsc8iB4AMItw/vC1ekzE8yI6vC/TfygycwGTVN12+m1XbkRJmPSyXFBcFhXJmLZytqHE7q628bnyH18T/xcQZ8QTIBby7NfzjCgWL3JLVzGcfiRmpq5Ea0V63syLwT7p3vlwKGDTtJSJqLHSsI4tbFrC4bfMEXvGxrkKjCJtILgglAagOyTo8q4/nFWC2MeFt6gt8Vh1F5P2umTg5rxUzYFe6Ns6XmO7qJy6025ILGiR6ZtaMMY4dOhRM7xWrXwA5vHLWh2QPhpY3wwzLC95vaiU+8kay3ha2/1dajscEe6CTUzp+ciLmqtO2j7H0kOzgRFmb7Q+tenqiOkZjDVWhGa9ECfFF46iDlm64YjcV74vRUmpAMXOJHc/nJgZ2dl641oozcGOcgUfoDCjqP8cd+RuPANfgjCfkrFlOp3l90zmSE6jmNCVqOPIslLN3yIhxo1ySxkAPtw5m74zHeoexLhMG3KQkQXKPDwN/kndEqBiQYvdgactnfBUNFvl5w6XLllytaFFdFgB4aJ3KV1Shu+WJlPYB6ImnPtmJsQ8a6Ys64lyraLStWNtm4q/q6J6NGCf+iPuDOsHEmPO2P4mMDteI2wSg1JXICvqOHNAKtyf2ZyXkR9ZILH7R3IBnQiotSN0gPWBQtQ2abVqOusN2+aPWZpcLZ8lFhQDnAQ4R2oNtArcN7rDQoDnYOvjj7Vvw6tRBSpF8ZXPOHxuZ99rtGuSEzqfeDzjN35piT0ZfWc6E0DtxFg6Mdhx5fJ+SZ8m0FCbwFPlsYc8BESt7RSzlRf4FbYJY63JKM3q78Kx3VM4o5YTrigMIg7v4xEsqbBDT2QRzdt+okgdG6+3oD0NDqPhi3j4OGm5OYuGeDUJvOoQNWNKRt178VOjyT7np7X5x4pkf+ZyLkLbzEeigBWki6sJNtAthTbLUGNEqPjZM5mFosCXrmiQjROT4ZujntoeEJpTtIH9Ncx/z0VvQdbflgxOuktwWu7fvGP1pv7rf24kM9qEEO4uMlP9uZv67vJ555+K4yFEaU/bmdL644XiE151+/zv5+Wu6z4muDKoP3xBVMZUeZ7NFVN6t+dVcDyJRdAJw6HKGz+ZN9rkoWBpMgnTdrc9NhwEzK1kavFUuSshrCGYcsEXO0jA/P0kZ3o+rt06skJF8lD1BJIfV9Eo5HFjzwIzF7FonM91dBGEak3zH9C7wk4Td1RLR+5wZY9dwf2voLBheunTQ9ij7agKRIpAr13U+n8c77xHn/rrv26RfxWSSuhkeUzWdRfF+sYTgvb3V1Tpd8UOxqmO1JIQr+SJS5YV3swENJJYbGpfvyvES7p72zRfOpRNz8bzd/6NHW2VDN42tJuT+RoiSVfekIHDu10nHTeoFO+87Ncj2eL1+ZMtBSk9HGag1qGTzzJ2VzRln/5ZFc+duq8Hnpdb3IefMm5PYPmQ/M9Ud8wtI6T0dIzTRdsInk6fcTg9sipZbJ/YuESrcNfgyuk4+WodPGNbY6nE1pvPB7TGd+4Od+9lw/2hv52j33q0xnYd7BwcBpvPBb4jp7EI4fjLAzj2ugyBDTIM6dz2o82lQfzhGdLabS6I59wHNOQZr9QYNllE1mXAFWcMYSNXxUsjN3b19Bm4Ot+6vqIKUZ+cUfwOmTrBc2e18iWEEeD2UKcMIR62/l0pPOY2tHDHA3ONORcG4qOrOurlag9c73qWmp3F+o8IiAqql2A/brmLxbWqpREY5JokXbwMaXIUVHLZBkrcKFSFjX2KCbAq0rb9iKsa85M3CwM5HQeUYM8AES20n8BLnCiq67Cfa2QSz5ZSJabmcUn27V8Ul5XrAdF5MquvEZgAt5hvUYo6y7M1MKOnc7Vu8PyLozZmTOOCfSvx6w3DcgcXcIUSfdbMzJzfxnsYPnbDHD1ohmF1vv5oJ6eRr3Wjdhz6Ysx4Mq+syDMoFZkTZ1PYBDRNkUW50dMjOl6xe9167PfM2+2u1zJ4TmrlntSf3dBjVel7OkGbpEgmsU6WA2tSF0ZEHJRDeBdVc3CRwHDk5U0JvbivXl4VKQ56Fkkp8agju5584pA2B0AjOYIbUyidJqME8vfeR0gJchiUgjuqIFYUqlEmxwURXdM4eyvwwGDYAmrbCppgT4s7IrJnk4pLEzniHEncErNMAqOSlUGGid/ISheWn3KcEUG7CkrI0s7a8Lwlrd2TzcfanPLtyUvSL3va/zr/YO7x3uL/b+5Ln6CD0RvxpO3cLAW0izrScEfSDS1vhooHSyYB0Z4r8/nev4UMJauaTRjmcGy5IaHlhhS3dTybfSOR2/BNolF/+/e9YWQxArm/dJJ3xJP3445+28fuoQEWeQFDDOTDzSzqfpLV2yme/P7SAJP7Q3e75HLPEWz9dKR5G1nrejZd4R+sJJrYwzHJIouSZgEPGg33C4XODJMROuaQnTjXxTXSekj397b7sd/cz2Rb6JYkCwW+gsoF+beCAArcUuovQW9kgHEx+t2t+F/HosikRQgDRuRVVe6PKqo2OhQ7mU0zl43EmFXLUnm6pkN932jgq5Aegfe/cv61Cfv/BwQOfZKUj+I0U8pSC+sko45ABRnM2mObzjoJfRhd/jg+1y30FbSQV8AP3kyd5U04UF4pOSiloHom1r6oK4nHQXVIdf7C3t79CHSdJwLkKkhWuJeaXmpkr5C7MzwhoY5CPIXbSKKLtULUoto9PQeNP/qZVHEp+Y+6kZNHzkN56mtdvIcKLGvf4xq1OOXLDXSJ4lsouaxVH1PUf8zOn+Exjx4e9nEKNIjjPWNsFs6yd0HBTzhUN/nyajZeFwX2SyEKEafJFW3qAiKbrK2RcgNBRgUhgoTaHxqFDX0kNuw6GGcD7ed0el+CI8/hkZQEGUwuqilwhJYSplQOhDdh49LMRhA3Zb+/rdyJt9bpqLTO1pObku9axcGCCqKsAd7xg7kRVoSosZceO+/IiIw7Fhsta1/CSCBT8+vXrlwlYZkyPhSSvBNtxBp6fE+KFNvSzMoier/PRINzg9PRZq3IKpWAoxS2mYE+uIazR+6bKTrAS+lNw4feSCXTuwoatWFKppClYuox+ms0KgDvn1Do5gaBU5Ddf8d4ma3dtoiLV5aNqoij3BOip969eywfOAhjiW4HvGhcHdgia6G5/v/iGsq6prVTt08feTgEukcFyhvY0OhQhZhY+/W1eI2U7MRb4mz7QtzgBCxfGV0SyzfzbDImT4Y2P/HlsmD2eqxKDaMTJdZsMDt/QV9Ahek/M00Q4F4O18IhuZV9X16CKOIOgADesnKy6cr+kZArUj7HSE1rIM+5RFEpsh1Oo6munnfXpce2f6nyheAeVNSkrOgzmSUVUzbcREK+4ppb7ee9VAWvY89hElAsiEWbOBq6zN0kx3YmLjbpGFQvvpTTw9fTbrxLwKxHeOJF8uTl5TGS1BUdUJvTsCWyKmZMsT967HVm7EQ+HHZ2Vs3KBoRgxoNwFrJS55UzvBWaixd5pW8KlAlddMUae34Z36nx1Xbys66hL9aiE0BHS7Dt09ljaNiTk7iblIsKUQDaSqIUnV4NUsXd493F0MfCFiFIMAEXuLrhGuCieDhlvUlhLfcqkyduRvBpcOXXhtvDME5AvF5jFx9ZnnY/LJfruoRAaqUD4vVu26EpJ6QbG4KJQoQ85iHp1njPqK369Ne0J9oqKGvJIoF7ELNQzqNrHZfkOOW3KqHoitQv5lpKHO8cYVgh9dDsNwz+uZcz7aRZO3V4gEGS1RkQZY6wORLTs5zdmH8AdpOtKMUl6PMrMWKuU6aF7c3JEisA///F/3jhxOb3RDv75j/+LoJVJOXpbOLkoRxG2JoTtg6wrhgMn7rs3J7io+I4wYPtCA8jVnRUFrdqcWIbwQrTr1juGjBv5kRQNTHnY1teMVV3Db22afTssPMaQCpi/c3Zh7qsemBEm3jQ6r4Rs1vcmnnyna5APoVb0XqjvJtpF2TZFZ5r1/FBAucNhLpKQqyhEkoRLARjdt1NBbHKuUwXAjOI9x7RkzNj68Q+QGhVr5MeAajoHUP8F6xCwE0Be9+0lQeGAc8lg9w0mN/Tb4obUJAFjBa9FQBnMHBtAyCJ3F3qR3vfmbjiSwpXuY2cmwNwABzUXt0pus5YV5Cvp4VygPgMjEGaFeV6rX8SYg/DMUVBcc25zvI2mdRQ4oJwxWk0mbn2KyRc9d+H0+nDd1Jxd66a0By39UEH5YXDOoIXQc7JiRBmTxWIUTMtzvtGlDxZFFLCZRY54ehjqHgGvFqgj8rOvXz9/JuncscSjckVOZoDZRQUYZ+0bTmgKzb4KizPBBgrPnq8dJmWKWE7ho10b6ukSlNpwdSIFo/SYzHATpEJBPkHN4usI44dkFjuRmkcOSdgjsKdxgRAXAiJe6oaiuLkjacHXPiGcVIld+DmuwN1k46FPTq9XirP/CQgytrI7niwDCy+BnCmny6moNlq6HLer04Kd8XR3pdAI5I1KPyzc8cydMvJQv7i4cPOj9V/jxNU1RtiBUZJktX16E2H7sLwtEEtROSVxGcf3fvJ4b1R7Mu1sROJ41lnUyUH4XOvUcEujqz2Da6gu35WQ6ErujeSoOhxDK0cFuFE3hTC5wQ0XazuSUGQ4prAp/OFcCUY2iWq1sWanpAe1V5gftcpaMWvgRAbSDDVJNMgwM1VSe5mH1v2IdcJmlVKoO7FEo/8OlyOhsrl/8DnUoCc3d+1kbRZ2C4+8O+/ZHXe73ECCGfw3LId3N9GmESEAaZUygNV0isHl8xp8DaiJgzAY0ectIiFbwsXtp103T95CQ1Fj0KSSRAS2HANN3SZ1e3G0wMdzKXXMd+rYKXwlkBgslp2OPqeogyvPLD+obmgTzRb+Yhxkz0t3iTXVxcLcmem7Ok/dHrCDIRjEhsk5iER4CBy9XTVCxb/SFtFE5rYg4w39jdHN5Ay511qJC51O7UqR2pG/ksKNMoivKBJWDQiyRtmpkopPpDSo/0IQ95KLTEZgny8/1ByTnDdNdGOo18GoPqJPIRSUKR1QdM0nJWwXVC0mRT5LMs/Edzupq2Fc7pTSIigKRhKI3XOg/IO+NwWk5XJWhvrEo8BU8qy9wun6fR4xRykVRuBKLX1xyHfEVU73SkkgY8i3A/dT9HL4Nh4UKlWGfUZemJTwqpgU75zW3pQ83kCIwn/imYNrzV1XDeZAwd0P3Tj77Gnl9LIMw/hzMM4UcXIhXyC/Dxac8da/MgliXdlg19r4f6KgHcs6ODV0icWbEKquFxMs8HA5q5rCL4PBTxi7IFFfkGkj0EjIFdxP3stCaezlJhXfN+aCgYDxhZmVcJiEFMj0xg19mnf5thIy/Iq41h9W1QJwKHM02OqxepNtnkfiXVIQZ5x4JFoQy83T/MKu9xXOUAZw5tuKTtAdVbyfA33AjDn6Ri0XXeDuuAgcL1l84vF6Yt5JlJ50DyB23vq5ndAiHIcCL04QRO5EwZFPdHaCEgLjdFNDhS152xCmiqb3vO9UXQQHkE3XnsQKW6sz5d4m7+wddNguQXm/q6LQPWsjeyknG2Y0s6ueoW18uRTJ59u6VcCrAxfSQG8kpsozYZX4EqFWriub73EHKtEI/A1XUz5gD9Dd1QNTh4aeeMIqk1cLzvykaujSYyFxml+44SdlwPrKpRRrigUB6BXjElIFsbROaMCQ60SIRyKXA3XQi1xPvWi34LmfxxnCpoHTudMR64sbVZRIzucTDGsvuI5pxSntGJiawDqORbRsZY/ymUmzUUcmHKZAnrNkTnoyTaFx9F7hBEFGphvCZFISezeY5lherqIK2om7c3Hj1FP4ocZUUFgkb1ksO8w7yK1wXzxosJXAAzAAVJCQahILJmapCjSZnQmJtuNVNmoBF7AVtaMhQSwn8c7e1t7O1vu7qT3WccuIv/+yqHQji3k+kJDaCJiFYV47fd13OMjrDvc1CMp6OUG2BHeLN1L4idlO6Yienj6LDpgfT6zBDCD6S/uIP4ERy93EfBCdJlZXL8SF6C02eD/3nnfAmTLAiN74ruKMnI40KXSyyib2Uq1u/Oef0HSQO3UuhdsBUTvX1b2y0cQm1bKnBTL707ia8byCxeAEozInpwUsuwZhw0t+dBhMDDb+atmM4hiZBjwq151gOG9/yNhrdfJ4HQ7xNmYeEFDfu5V5KO6WPcDfxb+Dl7CHjkxVjrE+ZSYrzRIJjr44zvBEnxNlVOpNOwizGw+5rGZWE0tJerzhlBUIRAqXToQoQf6uKpFA8Hsuy+yuxRK5kpzVyIg6iD7TX3Rn9m1guenSpXA4EDV2U1EX17Bt3Oo+uipnefJFh5qUqKhOgl0G39C/vysmI7CVDO6bY9LDfnb8b8fZw3rplIRJo4hEiIAwP6wgHCMFpM9eJwx6CMizTzaZATnSkjaMqLM+3y0L1AzQaBz8BB+m9BhMWN8HusV9SsYaXl2oABHd69YHw7Uhefju/Wy4c7Rz/2j/IMS1Heysw7XtHRzeO9j7DwO2JXBfnwqubdeOrRq7G21A/NX12myT5/i4wL4jrNuqdpOot92De8oijkWq0MXjK5hk1IpnJyKna7tgTJ/doH2WIohZDrJWIFUzGj0ndDvbHJPj1RMmP3SDLS9nvmaodzrRfY9QXvAYyZZDklN00EEzPPY2Nexz/uIZaZbphJr7Q4PgO+yEXwSgUybMpY49lCDPOv2G7JAhIx5UySll1Y3B6qoQ/WWcjBlG61KpMJ18ut1oY7rvyNqI1gWZfWdEClPpVrDvgCsfjC05qphV9/RmNrpyFxtG5qi4xqTQChXF9LzAaRWtUy8JZ0DpUygjjYWISlGLGoiSYagTJHUMnvzc05aSkVMlaIHhBbrDgqixkYmN8QgEZ/FFRxQrDMLGAuxI6aSFiZNdtW7xZFeVCZYqMlCMM0BB8O5LdtFlDApLGe5fyMeBKr7l5ZITdzQ9fwpOf0SH+FBSEhoqyCbVfrkM/UV7zMSN4WRL97gTE6PUKpDQhq5ed/1GaqZ4TXhzy2lP+OmDrC8qFDytvDMO9BIWJolfx1EjOi3YQhQ47USYdSvBvpBUkLyW0OB5tzmdbFKohjkXFgRPScrCCRXRRDP00hiKb83KBGQUvhg3z22xhd5hsCCulnDA6hhL/W5VRl1KCYfGoO3nu89DSZRKgHvQIcDXlvlLptPdT69dkqU1pmeNWFhZ3Tb53J6/NWEjHSl5GXj1RVfHTSIOa3A2CYir4OgjPMmpm8lG/3waMNgyhHg6d1LWLbwt080wj6gsFy9oEKmhDjDR+lswGMBnY59j+EwxXzAyZ/CDU8wH/6Of7Qwe9LOrm7kbCqm9zTxHOgXpMtHFMygCX/7AboayZeaG43CXKw+FjyhFgciwETr7mZ/YH8o5UUGkdkMbm6mSO7pXoco3w+EaJw8hMtjhF+bRFZPCpGnlcO3doK2l8hTPsRauKhcU2xF9CgueF6v7gBawOduKa6Ak+I2biplTcW6MYreCz93Oy6GxyTrYCC2Dkc1fatxSF2MpVqWX+m14cexI9oOR7AV/7QZ/se2IziXP6Q+YYgus7P6e3lJCfBiVRDULR0FAJMmh2t+y/jP6SWPgHDQ448x447kv/RNDa/OmGtkL07FO2XgkdN/KBKluS/Lw9pbk3mD3EDKkdt3/79w2Q+re3oPhfmBIHv52huRqo+qTMSn3/Cjz8TvwV44HZM+lbco9b1Me8/OA6MbnQ6NyVctpq/KBm0J0qQhFOVmYEEVKWpnvJDEhqHfNVDHCi/v738FdNwJHf6FcvV4U/eX5M26ZTM35sp5X5MLpqs0Jgaff/w55ZEtR9Kprhk9InIonaUBwZWYiAkt1VIA6BE629znWPJn74BYo+b//nVSWvWLnGyVKSS08wRYnzc+DoRifLeq3EwLV+3RLI/DaJ3YPTuzOLc/rvcHOcLC7n+3uwOrvRp6f/fvrzuv+vYO94dAc2J3f8Liu3q6fzHl1s862/kCIrQYT0u3W1pETHZAoeIRCxBzZlY0nz+ze/pDP7HIRlPEilBZgAjA7mkl/uDEyfeELSf6Ji633xVaA613Yn3j8W0g8gMofIMYpECzVESnoTPHmnNItAmphuAXeZ2g81pOSEdDwEySUNXlFlJfH0SXvNpB22HUAXmCb9e7GflaOvzjYGe7eG/a+/BqA65W+PsH2qYWB05IHc+bSjd4QkuNTx3t/537n+X5KTmSi//dwfPIvSGmuBjxjbsq0HxPpS/+eFqhRsSorSPBUsA+dBAY1XJqmBzZomSKwTRgvmsrWTDbwCDBGDZSFyzkpi1MGSBirkx08YDSOKDkhJz8ExEfEsUOwk2R3LxAN3hScPOgBNKXgZz6OEN0bDA+znQcgSnb3bytE3dY73Dv8DxKiawTIpyJFYaovoNQAhIMHErPq8KLfM5xN8hvPdBinjXe1nJSe99xbPisWpOKoN1pxJEpIZBzX3CQBn5xIIr2B7P6kRnCwf0gyA5hjuhHNWgIPRTPlOoprIPHa5FSp2VejUT8cYuSr83mr11egWYkwpaSQhgBC+5kwgQCLSpPdKbeKLe4ETjRgUva3htGPs3E59rmh7ECd3LThBvTua3HT5IfHSwMSK33gQF+BpQayjYesGXeau34etrJvqut+4NaVkYvsoGsPDKf0aNW+hL86E/ShB8RMErjdFuTgt+FFUGcsxgLUYWS8yoEj1vuNa+LwollRZgGpogevy84TIs5Y2RaWj8TU7JwchoxQwdbC3Q7v4qcz0exMyMRwjJQqRvXHlKuUJmCDdoOZb/nuk0VU/UnAVt8WN21vxp6PMK/w12IkhYbWmk++cyx3fid0cy/oK+2rjOfCdiiltpBTxTtPUmeknJVnGKJjbBZvPPJFlolYwLr2xGHGlzYoGwjdJN789pvuhPOaEYF6ljzPCjdLyjHUY8jZ0mKvlXbZ3Z83V+cVFLNbOJ2idrYUs6AzYkCQOVxKeIO91tLhPNnmVz6dO1P/m4lewDPKX+EPmNwO0c3RbspHiK6pEJNC4luRFnhHrS8Zv8Tu1lC5gOwmx5ImYUcSD6TBtndjxHhNzmQbf0UXmIbsOqqZDJVeKOlZ1RoedjAdb9XmJFvzPt6rcF2OL4tk5eihMhWFezcVxXnz6lkjbmVBCqOIM+XNwt+naISZDJj8sWWyftxQPXnhoNoCxHpbERfD14tAWNLDYsZjgwXnjUobLtwFAEwFAo+6WmB1KvpYEPmjogbRwAEnzIDHJAxMtC64oCuk1kMvAeOIDMmGeCN5yPXvDKwRW8CxVKNqIniGi463EhVOdm7HMQxnvnuX5ajXAP9hyNS9qiSibfp+clFTO62Zl7MZkEi8emYlcilVDNPNb7RjpDyU7JpN5uSeca/T36kXkShAi4sEbs8YNcyn1Rmb5Syx65KjSPR5kpC53HKgfER77sQPD4QtwiC4LGaq58PkRe7FS838A+IT5CHAdD8y7GjqLzHXLzWlJoreTxi4pSLwDJig4L6ujxleDM9IiwmiT+2Y762WRtCuNteY3JpyNmhuZiO0EuzmLzyD7NpmjzlNUDjEJ8FdQD8NlqF9AaTfNCjBKi/MUksuHL5BUyDauLnnBRpGAPOJZm1Nc6KoAsYwqkPLwjMoJGbEJ08yq2ErW8Vl1T3P5QSL9+J6wa8l0SPgMehGK3hCeh6Is10vxYJZJfV+/glHAMIa+g17WsN1yH7wxic+72/tUEL83aAdPfI2Z0Q1VvQHQaYkWBzo6pshgwAHxlfk79CVRRKLXnuaj4tEMQOcUvZjFRKIUMUgdo5uOnb445Vur1O8eiGlE69Ards0G8yX55NylD3+5lS+W7k9KLMN/ERQHSp4yQtTdptG65EGHcPWMr1u2Ka+IoBJztzZwE1w5p5wKsgZuGB89hEbh0gfiZkcTLXhRgVvjqoVaepxeWIrATq9JUiFi1V789q7RT3r2SJoPM51yWwz7tWEHltON5BPmJ+nJtxCRqpEMexfVdR3lXRyAu5d0Wlh0OuxiR5t2aCpJwA1eJrPRjeAbuDhG9MdM0BBOpGD94IoT7jnJBhuN7K20+vlN39O1zDVCwHeVffg9w3g1sCZvGyIwQf1yeTZWlFKJT6UoY+7jGpaxwpwUG1wXZ+RfxuPm6pt260KN31D2ouKE6KX6M9NBMpqi0AM4+gWTO1fJTciORHqawju4yWxRhAsj2oz61fBK0ynT150b67WW6wdPGv+kHU9k2IXAE3V+5WiIUqZgRQDTB98fuPujIwSJgskxV4TinV/sev2FwHxD7Ph8Gh/92g3IpiFUPHaig/79zWQYMbwGyHxu13pn0Yc4UdaDSwlBKGDXYgcQIcSR3YjhjXGsMJBTD9r4rKn8lwH2N7N6jHl1+ORhZTB2iQnBxgDAr+TOarh18sCM/NqpVkMXdfOQiBoEmj+1xV20Bx5GHUY8zTu6zuzCsvmMseELYwqbqS7moAC96NaJCf8KCqhk4mhNTdKK/t6kpHQQ0U67AdYrP0gp8ciuPbE1Oa/7gd/3Qv+Ogz+Ogj+CtvcC/7aDf6yI7HP0VPImI6odOSNn0jVBnTpw1oeRXzqO8OdB/d7X0a75ig7zr5x0uupO25U1wqW/it9IFpxKheFEWYLAVs/mJ9/+rij6UCN7bPIccvNXBlnEgplAebk14VTIhc3g+H+4d7O4Hwx2N0alw1Cfpz0QamGbUCcbuQGNnOHeiHNHB6m5eD1fMAx722mUN12MvVge2d/OzrfW/PZJYtmdxs7eSGdvZi5GfiqOr1pAG0pn+J0niFSIHqTeAhX1bSIO9vujBofAPRm5/7R3v7R7oNI1q9Nutp98ODB3j0TNd7/TUV9SmiGcdXUE4CHGbY+H4yuysk4KXTh6WjDQtrgZNwheod7KHon5eXV4rqA/82wcZa1lj1GmsNvtgSh2iBQQdw0mlHERgO1ghD5YvE5RYGllhRyj2Bw9zpPY732djXTSATN7pePiwXGjej00vPueLlvfv87cnyCkKfqB6JzNVDgjise/P53TistgJvFnTb3V3mR3SHG/TMMHDZ3ss+/e/n48RlL8s+zu9kf8un8X/B/sv8mbfqnOffrbDyenE38j+5mfweNh3oZiUu2inrmqi53sM9nZ98+eXV68uKbz399nxZE0wHcGH5cuXOvA8mxudyhjT6gHfcJyKBDqDA2HGZOqxruOJXztsiVvb393fsW/vcbpn2ukCMhRK77uR9BIRy6dpvluUqR5mzqTiQKI9QOd2BE81Lgd/hxeXRw+OC+KpC7MLE3zdk1lbe0+qgTgvyplCox2igXPpiYjAboYA8adaPiYp2oqd6DTvcOHgwPd6RblZK5++ff8UD8cDVw5xL3zK7rAz1dA2SB5urA9Q1+B+uzwUPjYoOHijXd/ZiQ08kR85Tp4Pzf4yL8u2jMJJaQwfng/sFm0+L+eubRTdpX6+Nxkfy4aDWy8esduq39GFV9IGIp69B3kFOB6VkJt8y4zi8H7pIZjOtqbrjZgyF/uPboXT9ce8WHe19c3oPh/s6Gu94JU4oZhFMVfcovHH1axC1svLQH7lUfcaX3ayxzrDAkgpiPEPtCZFkzDxwPF/TXtkJv9WtbKX7tG/GS7T14sNmSOQmJmY7hXIQfjovEh0X0842X6x4UUEQwwkMg22BfGqZrSWkWZMMHKxkYv9i7hrogknUMhK2jEVRDKEw+WvsslT5a+8XHmx/dFvc23hbfSslQuy3sh7ot7IdF9PPNt8W+CiwtQB1wwsqLAnhi4jRX5K8DwkPxlVzXNCMvv34ZHu2P0jRvho/RdPFRJoS2wM7h7obCHPxtAPOKt4D9ULeA/bCIfr7xFhgeur+/U6q62ha1BFNPc4GUvhaNxSWA0gqThWvpYzxJKT5DzkdKiMCMC+S1AGYIJmEAzx+6Ad2sEn3edAokmjfWZb6chhvsP+HAaeX+Ew68+E+4VeToDe9vrCazmyRWk8OPVU0OPy5ajWx+BPfd33TvsNp4XZyT5qjOGB8lysfTcpYUNxmlJbo5vgL0s5KIvyYSm2+JMxZVhz7jTPpxFjeshPi/w+P2aQ+Sj9anPcjiE19uNM/BCmdoE5r+O7Bh95yNQE6/AVIiDcS9IGHIwcidxqbobXbWoCI4kyKTAPjnP/6356qTyOYjbtLswl/wQ9oZv+CHxS8Y6se2/NkxJimgl8CysOGcww81KZp/GJzvjq/lZHV8XXQ2/hvMxQM/F2hCU9LBphPywI/5xP7azsrKZ8bF+meK1X19/EkCN5VMEpZNZNabDWcJfq6BsODnkVNtxUPeqbbioWJNd7/BTO2h6BuQb3pgPaSbbinX1su6ghxS9m77nZT6ijdQ6qsi2eBvc6Z++QSAK6n97vGn3hnVfmPz6W/0siiufunLtgVp/Kl/2bbYNJ/+Bi9L0YRy9Eve1VmWD+G3sWFqP1TD1H5YRD//bd7zej6dMNW5euU3O8H32ak/6owgrHyET/SqR4qVHW1uNYBORirJHwU1jjaULdtGPUxK5LLzVpUxqCh1XjN3iDfRUygKmoY5S8A5yubd3BCKRabCJzgyXpdPcGTFp7iacI4AzEDOpM1OjiHdafuwur6VdUl/W3S1fAvbGmIykrRuOG+CGdkCM0mJnDGtXcFnFJEkqjZyMWgZiMatT4JX496ORxzJzxldARii2K7+ZAcomv8nO8DiE17iH0W/CALtAyRzi0PqFHW/b6PuAw2sf8jLUcLvWAQAHAlnozn04Gx8/kZMf6CVxmE+wLFZ804DiADj0D+GhscZ0NvwmfuVE0jj+AxrRMt+b3p4iCW3uYdz/WPYJgmCj93EGPKO1XTS0EuCOzolfbz0Cx/QuIt74J382778c+bjhq74nxuwc7iNEoCHdx9w/wOa4m37B0ChWguwN6TZHAi5/oDKj2zz/uffmDnfM2xSNM3b4Z+mH78MMMM84dvjsRCnyKPhatgewnmPRtWxMPs8CdvX89GFPGtXBpAu1/MBLsS2/MM8qCsE16tlTN+2fwywfFNtpkhWbn8vCe/eTnwmvw5X8kds7ipvrnAUbu5GF3s7o53D/cP794p8d3xe3B8X93fOi4Nhfj7cy/dYdABubpI3izOABGHZkLJozihPFaBDw4PD4YOdvb3hg38hDPiZM+7Phmc7Z4jBPgPc+fnR8F9+/P8AAd6klA==', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(2042, '_transient_wc_tracks_blog_details', 'a:4:{s:3:\"url\";s:38:\"http://localhost/projetos/century_loja\";s:9:\"blog_lang\";s:5:\"pt_BR\";s:7:\"blog_id\";b:0;s:14:\"products_count\";s:2:\"16\";}', 'no'),
(1664, 'widget_woof_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1665, 'woof_first_init', '1', 'yes'),
(1666, 'woof_set_automatically', '0', 'yes'),
(1667, 'woof_autosubmit', '1', 'yes'),
(1668, 'woof_show_count', '1', 'yes'),
(1669, 'woof_show_count_dynamic', '0', 'yes'),
(1670, 'woof_hide_dynamic_empty_pos', '0', 'yes'),
(1671, 'woof_try_ajax', '0', 'yes'),
(1672, 'woof_checkboxes_slide', '1', 'yes'),
(1673, 'woof_hide_red_top_panel', '0', 'yes'),
(1674, 'woof_sort_terms_checked', '0', 'yes'),
(1675, 'woof_filter_btn_txt', '', 'yes'),
(1676, 'woof_reset_btn_txt', '', 'yes'),
(1677, 'woof_settings', 'a:1:{s:10:\"use_chosen\";i:1;}', 'yes'),
(1678, 'woof_version', '1.2.2.1', 'yes'),
(1679, 'woof_alert', 'a:2:{s:29:\"woocommerce_currency_switcher\";i:1;s:23:\"woocommerce_bulk_editor\";i:1;}', 'no'),
(1931, 'z_taxonomy_image30', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1933, 'z_taxonomy_image28', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/cat_feminino_moletons.jpg', 'yes'),
(1935, 'z_taxonomy_image29', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1940, 'z_taxonomy_image32', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1938, 'z_taxonomy_image31', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1942, 'z_taxonomy_image34', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1944, 'z_taxonomy_image33', 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_postmeta`
--

DROP TABLE IF EXISTS `cp_postmeta`;
CREATE TABLE IF NOT EXISTS `cp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=678 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_postmeta`
--

INSERT INTO `cp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', 'woocommerce-placeholder.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(7, 12, '_edit_last', '1'),
(8, 12, '_edit_lock', '1561904145:1'),
(9, 13, '_wp_attached_file', '2019/05/q1.png'),
(10, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:66;s:4:\"file\";s:14:\"2019/05/q1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 12, '_thumbnail_id', '50'),
(12, 12, '_sku', '123'),
(13, 12, '_regular_price', '120'),
(14, 12, 'total_sales', '0'),
(15, 12, '_tax_status', 'taxable'),
(16, 12, '_tax_class', ''),
(17, 12, '_manage_stock', 'yes'),
(18, 12, '_backorders', 'no'),
(19, 12, '_sold_individually', 'no'),
(20, 12, '_virtual', 'no'),
(21, 12, '_downloadable', 'no'),
(22, 12, '_download_limit', '-1'),
(23, 12, '_download_expiry', '-1'),
(24, 12, '_stock', '10'),
(25, 12, '_stock_status', 'instock'),
(26, 12, '_wc_average_rating', '0'),
(27, 12, '_wc_review_count', '0'),
(28, 12, '_product_version', '3.6.4'),
(29, 12, '_price', '120'),
(30, 12, '_product_image_gallery', '5'),
(31, 14, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559257412;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(32, 15, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559760549;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(33, 16, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559764255;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(34, 17, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561335708;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(35, 23, '_wp_attached_file', '2019/06/destaque.png'),
(36, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:761;s:4:\"file\";s:20:\"2019/06/destaque.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"destaque-300x119.png\";s:5:\"width\";i:300;s:6:\"height\";i:119;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"destaque-768x304.png\";s:5:\"width\";i:768;s:6:\"height\";i:304;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"destaque-1024x406.png\";s:5:\"width\";i:1024;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"destaque-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"destaque-600x238.png\";s:5:\"width\";i:600;s:6:\"height\";i:238;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"destaque-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"destaque-600x238.png\";s:5:\"width\";i:600;s:6:\"height\";i:238;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 22, '_edit_last', '1'),
(38, 22, '_edit_lock', '1561335107:1'),
(39, 22, '_thumbnail_id', '23'),
(40, 22, 'CenturySports_destaque_link', '#'),
(41, 24, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561339351;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(42, 25, '_wp_attached_file', '2019/06/logo-1.png'),
(43, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:90;s:4:\"file\";s:18:\"2019/06/logo-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"logo-1-300x54.png\";s:5:\"width\";i:300;s:6:\"height\";i:54;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"logo-1-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"logo-1-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 26, '_menu_item_type', 'post_type'),
(45, 26, '_menu_item_menu_item_parent', '0'),
(46, 26, '_menu_item_object_id', '9'),
(47, 26, '_menu_item_object', 'page'),
(48, 26, '_menu_item_target', ''),
(49, 26, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(50, 26, '_menu_item_xfn', ''),
(51, 26, '_menu_item_url', ''),
(53, 27, '_menu_item_type', 'post_type'),
(54, 27, '_menu_item_menu_item_parent', '0'),
(55, 27, '_menu_item_object_id', '8'),
(56, 27, '_menu_item_object', 'page'),
(57, 27, '_menu_item_target', ''),
(58, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(59, 27, '_menu_item_xfn', ''),
(60, 27, '_menu_item_url', ''),
(62, 28, '_menu_item_type', 'post_type'),
(63, 28, '_menu_item_menu_item_parent', '0'),
(64, 28, '_menu_item_object_id', '7'),
(65, 28, '_menu_item_object', 'page'),
(66, 28, '_menu_item_target', ''),
(67, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(68, 28, '_menu_item_xfn', ''),
(69, 28, '_menu_item_url', ''),
(71, 29, '_menu_item_type', 'post_type'),
(72, 29, '_menu_item_menu_item_parent', '0'),
(73, 29, '_menu_item_object_id', '6'),
(74, 29, '_menu_item_object', 'page'),
(75, 29, '_menu_item_target', ''),
(76, 29, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(77, 29, '_menu_item_xfn', ''),
(78, 29, '_menu_item_url', ''),
(80, 30, '_menu_item_type', 'post_type'),
(81, 30, '_menu_item_menu_item_parent', '26'),
(82, 30, '_menu_item_object_id', '2'),
(83, 30, '_menu_item_object', 'page'),
(84, 30, '_menu_item_target', ''),
(85, 30, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(86, 30, '_menu_item_xfn', ''),
(87, 30, '_menu_item_url', ''),
(89, 31, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561342969;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(90, 32, '_menu_item_type', 'custom'),
(91, 32, '_menu_item_menu_item_parent', '26'),
(92, 32, '_menu_item_object_id', '32'),
(93, 32, '_menu_item_object', 'custom'),
(94, 32, '_menu_item_target', ''),
(95, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 32, '_menu_item_xfn', ''),
(97, 32, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/orders/'),
(129, 36, '_menu_item_object', 'custom'),
(99, 33, '_menu_item_type', 'custom'),
(100, 33, '_menu_item_menu_item_parent', '26'),
(101, 33, '_menu_item_object_id', '33'),
(102, 33, '_menu_item_object', 'custom'),
(103, 33, '_menu_item_target', ''),
(104, 33, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(105, 33, '_menu_item_xfn', ''),
(106, 33, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/downloads/'),
(128, 36, '_menu_item_object_id', '36'),
(108, 34, '_menu_item_type', 'custom'),
(109, 34, '_menu_item_menu_item_parent', '26'),
(110, 34, '_menu_item_object_id', '34'),
(111, 34, '_menu_item_object', 'custom'),
(112, 34, '_menu_item_target', ''),
(113, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(114, 34, '_menu_item_xfn', ''),
(115, 34, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/edit-address/'),
(127, 36, '_menu_item_menu_item_parent', '26'),
(117, 35, '_menu_item_type', 'custom'),
(118, 35, '_menu_item_menu_item_parent', '26'),
(119, 35, '_menu_item_object_id', '35'),
(120, 35, '_menu_item_object', 'custom'),
(121, 35, '_menu_item_target', ''),
(122, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(123, 35, '_menu_item_xfn', ''),
(124, 35, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/edit-account/'),
(126, 36, '_menu_item_type', 'custom'),
(130, 36, '_menu_item_target', ''),
(131, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(132, 36, '_menu_item_xfn', ''),
(133, 36, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/orders/'),
(135, 37, '_menu_item_type', 'custom'),
(136, 37, '_menu_item_menu_item_parent', '26'),
(137, 37, '_menu_item_object_id', '37'),
(138, 37, '_menu_item_object', 'custom'),
(139, 37, '_menu_item_target', ''),
(140, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(141, 37, '_menu_item_xfn', ''),
(142, 37, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/downloads/'),
(144, 38, '_menu_item_type', 'custom'),
(145, 38, '_menu_item_menu_item_parent', '26'),
(146, 38, '_menu_item_object_id', '38'),
(147, 38, '_menu_item_object', 'custom'),
(148, 38, '_menu_item_target', ''),
(149, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(150, 38, '_menu_item_xfn', ''),
(151, 38, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/edit-address/'),
(153, 39, '_menu_item_type', 'custom'),
(154, 39, '_menu_item_menu_item_parent', '26'),
(155, 39, '_menu_item_object_id', '39'),
(156, 39, '_menu_item_object', 'custom'),
(157, 39, '_menu_item_target', ''),
(158, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(159, 39, '_menu_item_xfn', ''),
(160, 39, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/edit-account/'),
(180, 42, '_wp_attached_file', '2019/06/selo.png'),
(162, 40, '_menu_item_type', 'custom'),
(163, 40, '_menu_item_menu_item_parent', '26'),
(164, 40, '_menu_item_object_id', '40'),
(165, 40, '_menu_item_object', 'custom'),
(166, 40, '_menu_item_target', ''),
(167, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(168, 40, '_menu_item_xfn', ''),
(169, 40, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/customer-logout/?_wpnonce=99db4bf76c'),
(181, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:254;s:6:\"height\";i:112;s:4:\"file\";s:16:\"2019/06/selo.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-150x112.png\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:14:\"selo-64x28.png\";s:5:\"width\";i:64;s:6:\"height\";i:28;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 41, '_menu_item_type', 'custom'),
(172, 41, '_menu_item_menu_item_parent', '26'),
(173, 41, '_menu_item_object_id', '41'),
(174, 41, '_menu_item_object', 'custom'),
(175, 41, '_menu_item_target', ''),
(176, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(177, 41, '_menu_item_xfn', ''),
(178, 41, '_menu_item_url', 'http://localhost/projetos/century_loja/my-account/lost-password/'),
(182, 43, '_edit_lock', '1561343348:1'),
(183, 43, '_wp_page_template', 'paginas/inicial.php'),
(184, 46, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561346607;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(185, 47, '_wp_attached_file', '2019/06/categoria1-1.png'),
(186, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:555;s:6:\"height\";i:758;s:4:\"file\";s:24:\"2019/06/categoria1-1.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"categoria1-1-220x300.png\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"categoria1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"categoria1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:22:\"categoria1-1-64x87.png\";s:5:\"width\";i:64;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(187, 48, '_wp_attached_file', '2019/06/categoria.png'),
(188, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:172;s:6:\"height\";i:172;s:4:\"file\";s:21:\"2019/06/categoria.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:19:\"categoria-64x64.png\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(189, 49, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561350272;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(190, 50, '_wp_attached_file', '2019/05/tenis-1.png'),
(191, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:212;s:4:\"file\";s:19:\"2019/05/tenis-1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:17:\"tenis-1-64x51.png\";s:5:\"width\";i:64;s:6:\"height\";i:51;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(192, 52, '_edit_last', '1'),
(193, 52, '_edit_lock', '1561904147:1'),
(194, 53, '_wp_attached_file', '2019/06/D24-1738-304_zoom1.jpg'),
(195, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:30:\"2019/06/D24-1738-304_zoom1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"D24-1738-304_zoom1-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"D24-1738-304_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(196, 52, '_thumbnail_id', '53'),
(197, 52, 'total_sales', '0'),
(198, 52, '_tax_status', 'taxable'),
(199, 52, '_tax_class', ''),
(200, 52, '_manage_stock', 'no'),
(201, 52, '_backorders', 'no'),
(202, 52, '_sold_individually', 'no'),
(203, 52, '_virtual', 'no'),
(204, 52, '_downloadable', 'no'),
(205, 52, '_download_limit', '-1'),
(206, 52, '_download_expiry', '-1'),
(207, 52, '_stock', NULL),
(208, 52, '_stock_status', 'instock'),
(209, 52, '_wc_average_rating', '0'),
(210, 52, '_wc_review_count', '0'),
(211, 52, '_product_version', '3.6.4'),
(212, 54, '_edit_last', '1'),
(213, 54, '_edit_lock', '1561904283:1'),
(214, 55, '_wp_attached_file', '2019/06/497-9505-026_zoom1.jpg'),
(215, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:544;s:6:\"height\";i:544;s:4:\"file\";s:30:\"2019/06/497-9505-026_zoom1.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"497-9505-026_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(216, 54, '_thumbnail_id', '55'),
(217, 54, 'total_sales', '0'),
(218, 54, '_tax_status', 'taxable'),
(219, 54, '_tax_class', ''),
(220, 54, '_manage_stock', 'yes'),
(221, 54, '_backorders', 'no'),
(222, 54, '_sold_individually', 'no'),
(223, 54, '_virtual', 'no'),
(224, 54, '_downloadable', 'no'),
(225, 54, '_download_limit', '-1'),
(226, 54, '_download_expiry', '-1'),
(227, 54, '_stock', '88898'),
(228, 54, '_stock_status', 'instock'),
(229, 54, '_wc_average_rating', '0'),
(230, 54, '_wc_review_count', '0'),
(231, 54, '_product_version', '3.6.4'),
(232, 56, '_edit_last', '1'),
(233, 56, '_edit_lock', '1561904152:1'),
(234, 57, '_wp_attached_file', '2019/06/B78-2495-172_zoom1.jpg'),
(235, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:30:\"2019/06/B78-2495-172_zoom1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"B78-2495-172_zoom1-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"B78-2495-172_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 56, '_thumbnail_id', '57'),
(237, 56, 'total_sales', '0'),
(238, 56, '_tax_status', 'taxable'),
(239, 56, '_tax_class', ''),
(240, 56, '_manage_stock', 'yes'),
(241, 56, '_backorders', 'no'),
(242, 56, '_sold_individually', 'no'),
(243, 56, '_virtual', 'no'),
(244, 56, '_downloadable', 'no'),
(245, 56, '_download_limit', '-1'),
(246, 56, '_download_expiry', '-1'),
(247, 56, '_stock', '4543534'),
(248, 56, '_stock_status', 'instock'),
(249, 56, '_wc_average_rating', '0'),
(250, 56, '_wc_review_count', '0'),
(251, 56, '_product_version', '3.6.4'),
(252, 58, '_edit_last', '1'),
(253, 58, '_edit_lock', '1561904155:1'),
(254, 58, '_thumbnail_id', '50'),
(255, 58, 'total_sales', '0'),
(256, 58, '_tax_status', 'taxable'),
(257, 58, '_tax_class', ''),
(258, 58, '_manage_stock', 'yes'),
(259, 58, '_backorders', 'no'),
(260, 58, '_sold_individually', 'no'),
(261, 58, '_virtual', 'no'),
(262, 58, '_downloadable', 'no'),
(263, 58, '_download_limit', '-1'),
(264, 58, '_download_expiry', '-1'),
(265, 58, '_stock', '222'),
(266, 58, '_stock_status', 'instock'),
(267, 58, '_wc_average_rating', '0'),
(268, 58, '_wc_review_count', '0'),
(269, 58, '_product_version', '3.6.4'),
(270, 59, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561679395;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(271, 58, '_regular_price', '12.50'),
(280, 56, '_sale_price', '242.00'),
(273, 58, '_price', '12.50'),
(274, 56, '_regular_price', '250.00'),
(275, 56, '_price', '242.00'),
(276, 54, '_regular_price', '24.90'),
(277, 54, '_sale_price', '11.49'),
(278, 54, '_price', '11.49'),
(279, 60, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561682995;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(281, 61, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561686613;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(282, 62, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561690324;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(283, 63, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561693946;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(284, 64, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561697570;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(285, 65, '_edit_last', '1'),
(286, 65, '_edit_lock', '1561904156:1'),
(287, 66, '_wp_attached_file', '2019/06/296485.jpg'),
(288, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:240;s:6:\"height\";i:240;s:4:\"file\";s:18:\"2019/06/296485.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:16:\"296485-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(289, 67, '_wp_attached_file', '2019/06/296485-1.jpg'),
(290, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:240;s:6:\"height\";i:240;s:4:\"file\";s:20:\"2019/06/296485-1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"296485-1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(291, 65, '_thumbnail_id', '66'),
(658, 65, 'CenturySports_produto_zoom', '67'),
(293, 65, '_regular_price', '180.00'),
(294, 65, 'total_sales', '0'),
(295, 65, '_tax_status', 'taxable'),
(296, 65, '_tax_class', ''),
(297, 65, '_manage_stock', 'yes'),
(298, 65, '_backorders', 'no'),
(299, 65, '_sold_individually', 'no'),
(300, 65, '_virtual', 'no'),
(301, 65, '_downloadable', 'no'),
(302, 65, '_download_limit', '-1'),
(303, 65, '_download_expiry', '-1'),
(304, 65, '_stock', '999'),
(305, 65, '_stock_status', 'instock'),
(306, 65, '_wc_average_rating', '0'),
(307, 65, '_wc_review_count', '0'),
(308, 65, '_product_version', '3.6.4'),
(309, 65, '_price', '180.00'),
(310, 68, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561701192;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(311, 69, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561823405;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(312, 70, '_edit_last', '1'),
(313, 70, '_edit_lock', '1561820030:1'),
(314, 71, '_wp_attached_file', '2019/06/camiseta.png'),
(315, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:65;s:6:\"height\";i:61;s:4:\"file\";s:20:\"2019/06/camiseta.png\";s:5:\"sizes\";a:1:{s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"camiseta-64x60.png\";s:5:\"width\";i:64;s:6:\"height\";i:60;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(316, 70, '_thumbnail_id', '71'),
(317, 70, 'CenturySports_banner_link', '#'),
(318, 72, '_edit_last', '1'),
(319, 72, '_edit_lock', '1561820052:1'),
(320, 72, '_thumbnail_id', '71'),
(321, 72, 'CenturySports_banner_link', '#'),
(322, 73, '_edit_last', '1'),
(323, 73, '_edit_lock', '1561820076:1'),
(324, 73, '_thumbnail_id', '71'),
(325, 73, 'CenturySports_banner_link', '#'),
(326, 74, '_edit_last', '1'),
(327, 74, '_edit_lock', '1561820096:1'),
(328, 74, '_thumbnail_id', '71'),
(329, 74, 'CenturySports_banner_link', '#'),
(330, 75, '_edit_last', '1'),
(331, 75, '_edit_lock', '1561820695:1'),
(332, 75, '_thumbnail_id', '71'),
(334, 75, 'CenturySports_banner_link', 'https://www.google.com.br'),
(335, 76, '_wp_attached_file', '2019/06/banner.png'),
(336, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:279;s:4:\"file\";s:18:\"2019/06/banner.png\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"banner-300x44.png\";s:5:\"width\";i:300;s:6:\"height\";i:44;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x112.png\";s:5:\"width\";i:768;s:6:\"height\";i:112;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x149.png\";s:5:\"width\";i:1024;s:6:\"height\";i:149;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"banner-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"banner-600x87.png\";s:5:\"width\";i:600;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"banner-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"banner-600x87.png\";s:5:\"width\";i:600;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"banner-64x9.png\";s:5:\"width\";i:64;s:6:\"height\";i:9;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(337, 77, '_wp_attached_file', '2019/06/neckties-210347_960_720.jpg'),
(338, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:35:\"2019/06/neckties-210347_960_720.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:33:\"neckties-210347_960_720-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(339, 78, '_wp_attached_file', '2019/06/tartan-track-2678544_960_720.jpg'),
(340, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:40:\"2019/06/tartan-track-2678544_960_720.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:38:\"tartan-track-2678544_960_720-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:11:\"NIKON D7100\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"44\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:5:\"0.004\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(341, 79, '_wp_attached_file', '2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg'),
(342, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:215;s:4:\"file\";s:76:\"2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x202.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:202;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x215.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x215.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:74:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 80, '_wp_attached_file', '2019/06/2000px-Adidas_klassisches_logo.svg_.png'),
(344, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1945;s:4:\"file\";s:47:\"2019/06/2000px-Adidas_klassisches_logo.svg_.png\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x292.png\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-768x747.png\";s:5:\"width\";i:768;s:6:\"height\";i:747;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:48:\"2000px-Adidas_klassisches_logo.svg_-1024x996.png\";s:5:\"width\";i:1024;s:6:\"height\";i:996;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-600x584.png\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-600x584.png\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:45:\"2000px-Adidas_klassisches_logo.svg_-64x62.png\";s:5:\"width\";i:64;s:6:\"height\";i:62;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(345, 81, '_wp_attached_file', '2019/06/asics.png'),
(346, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:184;s:6:\"height\";i:63;s:4:\"file\";s:17:\"2019/06/asics.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-150x63.png\";s:5:\"width\";i:150;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"asics-64x22.png\";s:5:\"width\";i:64;s:6:\"height\";i:22;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(347, 82, '_wp_attached_file', '2019/06/nike-logo-47A65A59D5-seeklogo.com_.png'),
(348, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:157;s:4:\"file\";s:46:\"2019/06/nike-logo-47A65A59D5-seeklogo.com_.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:44:\"nike-logo-47A65A59D5-seeklogo.com_-64x33.png\";s:5:\"width\";i:64;s:6:\"height\";i:33;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(349, 83, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561827040;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(350, 84, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561832391;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(352, 86, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561835991;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(355, 87, '_edit_last', '1'),
(356, 87, '_edit_lock', '1561904161:1'),
(357, 88, '_wp_attached_file', '2019/06/fff.jpg');
INSERT INTO `cp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(358, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:622;s:6:\"height\";i:642;s:4:\"file\";s:15:\"2019/06/fff.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"fff-291x300.jpg\";s:5:\"width\";i:291;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"fff-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"fff-600x619.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:619;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"fff-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"fff-600x619.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:619;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"fff-64x66.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:66;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822985\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(359, 87, '_thumbnail_id', '88'),
(360, 87, '_regular_price', '123'),
(361, 87, 'total_sales', '0'),
(362, 87, '_tax_status', 'taxable'),
(363, 87, '_tax_class', ''),
(364, 87, '_manage_stock', 'yes'),
(365, 87, '_backorders', 'no'),
(366, 87, '_sold_individually', 'no'),
(367, 87, '_virtual', 'no'),
(368, 87, '_downloadable', 'no'),
(369, 87, '_download_limit', '-1'),
(370, 87, '_download_expiry', '-1'),
(371, 87, '_stock', '888'),
(372, 87, '_stock_status', 'instock'),
(373, 87, '_wc_average_rating', '0'),
(374, 87, '_wc_review_count', '0'),
(375, 87, '_product_version', '3.6.4'),
(376, 87, '_price', '123'),
(377, 90, '_wp_attached_file', '2019/06/asd.jpg'),
(378, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:635;s:6:\"height\";i:614;s:4:\"file\";s:15:\"2019/06/asd.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"asd-300x290.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"asd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"asd-600x580.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:580;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"asd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"asd-600x580.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:580;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"asd-64x62.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:62;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822941\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(379, 89, '_edit_last', '1'),
(380, 89, '_edit_lock', '1561904229:1'),
(381, 89, '_thumbnail_id', '90'),
(382, 89, '_regular_price', '50.00'),
(383, 89, 'total_sales', '0'),
(384, 89, '_tax_status', 'taxable'),
(385, 89, '_tax_class', ''),
(386, 89, '_manage_stock', 'yes'),
(387, 89, '_backorders', 'no'),
(388, 89, '_sold_individually', 'no'),
(389, 89, '_virtual', 'no'),
(390, 89, '_downloadable', 'no'),
(391, 89, '_download_limit', '-1'),
(392, 89, '_download_expiry', '-1'),
(393, 89, '_stock', '7777'),
(394, 89, '_stock_status', 'instock'),
(395, 89, '_wc_average_rating', '0'),
(396, 89, '_wc_review_count', '0'),
(397, 89, '_product_version', '3.6.4'),
(398, 89, '_price', '50.00'),
(399, 92, '_wp_attached_file', '2019/06/Capturar.jpg'),
(400, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:617;s:6:\"height\";i:629;s:4:\"file\";s:20:\"2019/06/Capturar.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Capturar-294x300.jpg\";s:5:\"width\";i:294;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"Capturar-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"Capturar-600x612.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:612;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"Capturar-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"Capturar-600x612.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:612;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"Capturar-64x65.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:65;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822914\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(401, 91, '_edit_last', '1'),
(402, 91, '_edit_lock', '1561904215:1'),
(403, 91, '_thumbnail_id', '92'),
(404, 91, '_regular_price', '34'),
(405, 91, 'total_sales', '0'),
(406, 91, '_tax_status', 'taxable'),
(407, 91, '_tax_class', ''),
(408, 91, '_manage_stock', 'yes'),
(409, 91, '_backorders', 'no'),
(410, 91, '_sold_individually', 'no'),
(411, 91, '_virtual', 'no'),
(412, 91, '_downloadable', 'no'),
(413, 91, '_download_limit', '-1'),
(414, 91, '_download_expiry', '-1'),
(415, 91, '_stock', '777'),
(416, 91, '_stock_status', 'instock'),
(417, 91, '_wc_average_rating', '0'),
(418, 91, '_wc_review_count', '0'),
(419, 91, '_product_version', '3.6.4'),
(420, 91, '_price', '34'),
(421, 93, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561839608;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(422, 94, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561843307;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(423, 95, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561847098;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(424, 96, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561850752;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(425, 97, '_edit_last', '1'),
(426, 97, '_edit_lock', '1561904203:1'),
(427, 98, '_wp_attached_file', '2019/06/asasd.jpg'),
(428, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:308;s:6:\"height\";i:319;s:4:\"file\";s:17:\"2019/06/asasd.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"asasd-290x300.jpg\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"asasd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"asasd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"asasd-64x66.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:66;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836616\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(429, 99, '_wp_attached_file', '2019/06/bnb.jpg'),
(430, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:319;s:6:\"height\";i:322;s:4:\"file\";s:15:\"2019/06/bnb.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"bnb-297x300.jpg\";s:5:\"width\";i:297;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"bnb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"bnb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"bnb-64x65.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:65;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836661\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(431, 100, '_wp_attached_file', '2019/06/dfd.jpg'),
(432, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:318;s:4:\"file\";s:15:\"2019/06/dfd.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"dfd-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"dfd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"dfd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"dfd-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836636\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(433, 101, '_wp_attached_file', '2019/06/kj.jpg'),
(434, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:310;s:6:\"height\";i:325;s:4:\"file\";s:14:\"2019/06/kj.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"kj-286x300.jpg\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"kj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"kj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:12:\"kj-64x67.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:67;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836688\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 102, '_wp_attached_file', '2019/06/mnm.jpg'),
(436, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:327;s:6:\"height\";i:322;s:4:\"file\";s:15:\"2019/06/mnm.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"mnm-300x295.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"mnm-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"mnm-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"mnm-64x63.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:63;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836677\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(437, 103, '_wp_attached_file', '2019/06/vb.jpg'),
(438, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:320;s:4:\"file\";s:14:\"2019/06/vb.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:12:\"vb-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836649\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(439, 97, '_thumbnail_id', '103'),
(440, 97, '_regular_price', '300'),
(441, 97, 'total_sales', '0'),
(442, 97, '_tax_status', 'taxable'),
(443, 97, '_tax_class', ''),
(444, 97, '_manage_stock', 'yes'),
(445, 97, '_backorders', 'no'),
(446, 97, '_sold_individually', 'no'),
(447, 97, '_virtual', 'no'),
(448, 97, '_downloadable', 'no'),
(449, 97, '_download_limit', '-1'),
(450, 97, '_download_expiry', '-1'),
(451, 97, '_stock', '43534'),
(452, 97, '_stock_status', 'instock'),
(453, 97, '_wc_average_rating', '0'),
(454, 97, '_wc_review_count', '0'),
(455, 97, '_product_version', '3.6.4'),
(456, 97, '_price', '300'),
(457, 104, '_edit_last', '1'),
(458, 104, '_edit_lock', '1561904192:1'),
(459, 104, '_thumbnail_id', '102'),
(460, 104, 'total_sales', '0'),
(461, 104, '_tax_status', 'taxable'),
(462, 104, '_tax_class', ''),
(463, 104, '_manage_stock', 'yes'),
(464, 104, '_backorders', 'no'),
(465, 104, '_sold_individually', 'no'),
(466, 104, '_virtual', 'no'),
(467, 104, '_downloadable', 'no'),
(468, 104, '_download_limit', '-1'),
(469, 104, '_download_expiry', '-1'),
(470, 104, '_stock', '4564564'),
(471, 104, '_stock_status', 'instock'),
(472, 104, '_wc_average_rating', '0'),
(473, 104, '_wc_review_count', '0'),
(474, 104, '_product_version', '3.6.4'),
(475, 104, '_regular_price', '250'),
(476, 104, '_price', '250'),
(477, 105, '_edit_last', '1'),
(478, 105, '_edit_lock', '1561904181:1'),
(479, 105, '_thumbnail_id', '101'),
(480, 105, '_regular_price', '350'),
(481, 105, 'total_sales', '0'),
(482, 105, '_tax_status', 'taxable'),
(483, 105, '_tax_class', ''),
(484, 105, '_manage_stock', 'yes'),
(485, 105, '_backorders', 'no'),
(486, 105, '_sold_individually', 'no'),
(487, 105, '_virtual', 'no'),
(488, 105, '_downloadable', 'no'),
(489, 105, '_download_limit', '-1'),
(490, 105, '_download_expiry', '-1'),
(491, 105, '_stock', '56456456'),
(492, 105, '_stock_status', 'instock'),
(493, 105, '_wc_average_rating', '0'),
(494, 105, '_wc_review_count', '0'),
(495, 105, '_product_version', '3.6.4'),
(496, 105, '_price', '350'),
(497, 106, '_edit_last', '1'),
(498, 106, '_edit_lock', '1561904177:1'),
(499, 106, '_thumbnail_id', '101'),
(500, 106, '_regular_price', '450'),
(501, 106, 'total_sales', '0'),
(502, 106, '_tax_status', 'taxable'),
(503, 106, '_tax_class', ''),
(504, 106, '_manage_stock', 'yes'),
(505, 106, '_backorders', 'no'),
(506, 106, '_sold_individually', 'no'),
(507, 106, '_virtual', 'no'),
(508, 106, '_downloadable', 'no'),
(509, 106, '_download_limit', '-1'),
(510, 106, '_download_expiry', '-1'),
(511, 106, '_stock', '4545454'),
(512, 106, '_stock_status', 'instock'),
(513, 106, '_wc_average_rating', '0'),
(514, 106, '_wc_review_count', '0'),
(515, 106, '_product_version', '3.6.4'),
(516, 106, '_price', '450'),
(517, 107, '_edit_last', '1'),
(518, 107, '_edit_lock', '1561904178:1'),
(519, 107, '_thumbnail_id', '100'),
(520, 107, '_regular_price', '950'),
(521, 107, 'total_sales', '0'),
(522, 107, '_tax_status', 'taxable'),
(523, 107, '_tax_class', ''),
(524, 107, '_manage_stock', 'yes'),
(525, 107, '_backorders', 'no'),
(526, 107, '_sold_individually', 'no'),
(527, 107, '_virtual', 'no'),
(528, 107, '_downloadable', 'no'),
(529, 107, '_download_limit', '-1'),
(530, 107, '_download_expiry', '-1'),
(531, 107, '_stock', '3453453'),
(532, 107, '_stock_status', 'instock'),
(533, 107, '_wc_average_rating', '0'),
(534, 107, '_wc_review_count', '0'),
(535, 107, '_product_version', '3.6.4'),
(536, 107, '_price', '950'),
(537, 107, '_product_image_gallery', '100'),
(538, 108, '_edit_last', '1'),
(539, 108, '_edit_lock', '1561904183:1'),
(540, 108, '_thumbnail_id', '99'),
(541, 108, '_regular_price', '95'),
(542, 108, 'total_sales', '0'),
(543, 108, '_tax_status', 'taxable'),
(544, 108, '_tax_class', ''),
(545, 108, '_manage_stock', 'yes'),
(546, 108, '_backorders', 'no'),
(547, 108, '_sold_individually', 'no'),
(548, 108, '_virtual', 'no'),
(549, 108, '_downloadable', 'no'),
(550, 108, '_download_limit', '-1'),
(551, 108, '_download_expiry', '-1'),
(552, 108, '_stock', '3453453'),
(553, 108, '_stock_status', 'instock'),
(554, 108, '_wc_average_rating', '0'),
(555, 108, '_wc_review_count', '0'),
(556, 108, '_product_version', '3.6.4'),
(557, 108, '_price', '95'),
(558, 109, '_edit_last', '1'),
(559, 109, '_edit_lock', '1561920804:1'),
(560, 109, '_thumbnail_id', '98'),
(562, 109, 'total_sales', '0'),
(563, 109, '_tax_status', 'taxable'),
(564, 109, '_tax_class', ''),
(565, 109, '_manage_stock', 'yes'),
(566, 109, '_backorders', 'no'),
(567, 109, '_sold_individually', 'no'),
(568, 109, '_virtual', 'no'),
(569, 109, '_downloadable', 'no'),
(570, 109, '_download_limit', '-1'),
(571, 109, '_download_expiry', '-1'),
(572, 109, '_stock', '123'),
(573, 109, '_stock_status', 'instock'),
(574, 109, '_wc_average_rating', '0'),
(575, 109, '_wc_review_count', '0'),
(576, 109, '_product_version', '3.6.4'),
(603, 115, '_regular_price', '200'),
(578, 110, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561854354;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(579, 111, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561858686;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(580, 112, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561862290;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(581, 113, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561905026;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(582, 109, '_upsell_ids', 'a:4:{i:0;i:52;i:1;i:54;i:2;i:56;i:3;i:58;}'),
(583, 109, '_crosssell_ids', 'a:5:{i:0;i:65;i:1;i:52;i:2;i:56;i:3;i:105;i:4;i:87;}'),
(584, 109, '_product_attributes', 'a:2:{s:3:\"cor\";a:6:{s:4:\"name\";s:3:\"Cor\";s:5:\"value\";s:41:\"Amarelo | Vermelho | Verde | Rosa | Preto\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"0\";}s:7:\"tamanho\";a:6:{s:4:\"name\";s:7:\"Tamanho\";s:5:\"value\";s:14:\"P | M | G | GG\";s:8:\"position\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"0\";}}'),
(602, 115, '_sku', '99999'),
(585, 115, '_variation_description', ''),
(586, 115, 'total_sales', '0'),
(587, 115, '_tax_status', 'taxable'),
(588, 115, '_tax_class', 'parent'),
(589, 115, '_manage_stock', 'no'),
(590, 115, '_backorders', 'no'),
(591, 115, '_sold_individually', 'no'),
(592, 115, '_virtual', 'no'),
(593, 115, '_downloadable', 'no'),
(594, 115, '_download_limit', '-1'),
(595, 115, '_download_expiry', '-1'),
(596, 115, '_stock', '0'),
(597, 115, '_stock_status', 'instock'),
(598, 115, '_wc_average_rating', '0'),
(599, 115, '_wc_review_count', '0'),
(600, 115, 'attribute_cor', 'Verde'),
(601, 115, '_product_version', '3.6.4'),
(604, 115, '_weight', '12'),
(605, 115, '_length', '12'),
(606, 115, '_width', '12'),
(607, 115, '_height', '12'),
(608, 115, '_crosssell_ids', 'a:3:{i:0;i:52;i:1;i:56;i:2;i:105;}'),
(609, 115, '_price', '200'),
(669, 109, '_price', '200'),
(611, 115, 'attribute_tamanho', 'M'),
(614, 116, '_variation_description', ''),
(615, 116, 'total_sales', '0'),
(616, 116, '_tax_status', 'taxable'),
(617, 116, '_tax_class', 'parent'),
(618, 116, '_manage_stock', 'yes'),
(619, 116, '_backorders', 'no'),
(620, 116, '_sold_individually', 'no'),
(621, 116, '_virtual', 'no'),
(622, 116, '_downloadable', 'no'),
(623, 116, '_download_limit', '-1'),
(624, 116, '_download_expiry', '-1'),
(625, 116, '_stock', '12'),
(626, 116, '_stock_status', 'instock'),
(627, 116, '_wc_average_rating', '0'),
(628, 116, '_wc_review_count', '0'),
(629, 116, 'attribute_cor', 'Amarelo'),
(630, 116, 'attribute_tamanho', 'P'),
(631, 116, '_product_version', '3.6.4'),
(633, 116, '_regular_price', '123'),
(634, 116, '_weight', '12'),
(635, 116, '_length', '12'),
(636, 116, '_width', '12'),
(637, 116, '_height', '12'),
(638, 116, '_crosssell_ids', 'a:3:{i:0;i:52;i:1;i:56;i:2;i:105;}'),
(639, 116, '_price', '123'),
(668, 109, '_price', '123'),
(642, 116, '_sku', '999'),
(645, 115, '_thumbnail_id', '98'),
(646, 116, '_thumbnail_id', '98'),
(649, 108, '_sku', '56555'),
(650, 107, '_sku', '55454'),
(651, 106, '_sku', '45444554'),
(652, 105, '_sku', '56565656'),
(653, 104, '_sku', '6767667'),
(654, 97, '_sku', '234242'),
(655, 91, '_sku', '667777'),
(656, 89, '_sku', '7777'),
(657, 87, '_sku', '88888'),
(659, 65, '_sku', '9999'),
(660, 58, '_sku', '2222'),
(661, 56, '_sku', '455454'),
(662, 54, '_sku', '000999'),
(663, 118, '_wp_attached_file', '2019/06/08-06-2015-banner-esportes-kanui.jpg'),
(664, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1158;s:6:\"height\";i:150;s:4:\"file\";s:44:\"2019/06/08-06-2015-banner-esportes-kanui.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-300x39.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:39;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-768x99.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:45:\"08-06-2015-banner-esportes-kanui-1024x133.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:133;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:41:\"08-06-2015-banner-esportes-kanui-64x8.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:8;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(665, 119, '_wp_attached_file', '2019/06/cat_feminino_moletons.jpg'),
(666, 119, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1158;s:6:\"height\";i:150;s:4:\"file\";s:33:\"2019/06/cat_feminino_moletons.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-300x39.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:39;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-768x99.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"cat_feminino_moletons-1024x133.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:133;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"cat_feminino_moletons-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:30:\"cat_feminino_moletons-64x8.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:8;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(667, 109, '_product_image_gallery', '98,102,101,100,99'),
(670, 7, '_edit_lock', '1561916774:1'),
(671, 6, '_edit_lock', '1561919617:1'),
(672, 9, '_edit_last', '1'),
(673, 9, '_wp_page_template', 'default'),
(674, 9, '_edit_lock', '1561923084:1'),
(675, 8, '_edit_last', '1'),
(676, 8, '_wp_page_template', 'default'),
(677, 8, '_edit_lock', '1561923109:1');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_posts`
--

DROP TABLE IF EXISTS `cp_posts`;
CREATE TABLE IF NOT EXISTS `cp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=124 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_posts`
--

INSERT INTO `cp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://localhost/projetos/century_loja/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://localhost/projetos/century_loja/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://localhost/projetos/century_loja/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://localhost/projetos/century_loja.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://localhost/projetos/century_loja/?page_id=3', 0, 'page', '', 0),
(18, 1, '2019-06-23 20:22:23', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-06-23 20:22:23', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/century_loja/?p=18', 0, 'post', '', 0),
(5, 1, '2019-05-30 17:54:46', '2019-05-30 20:54:46', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2019-05-30 17:54:46', '2019-05-30 20:54:46', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/05/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '', 'Loja', '', 'publish', 'closed', 'closed', '', 'loja', '', '', '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 0, 'http://localhost/projetos/century_loja/shop/', 0, 'page', '', 0),
(7, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->\n[woocommerce_cart]\n<!-- /wp:shortcode -->', 'Carrinho', '', 'publish', 'closed', 'closed', '', 'carrinho', '', '', '2019-06-30 14:48:36', '2019-06-30 17:48:36', '', 0, 'http://localhost/projetos/century_loja/cart/', 0, 'page', '', 0),
(8, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Finalizar Compra', '', 'publish', 'closed', 'closed', '', 'finalizar-compra', '', '', '2019-06-30 16:31:49', '2019-06-30 19:31:49', '', 0, 'http://localhost/projetos/century_loja/checkout/', 0, 'page', '', 0),
(9, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Minha Conta', '', 'publish', 'closed', 'closed', '', 'minha-conta', '', '', '2019-06-30 16:31:24', '2019-06-30 19:31:24', '', 0, 'http://localhost/projetos/century_loja/my-account/', 0, 'page', '', 0),
(12, 1, '2019-05-30 18:15:48', '2019-05-30 21:15:48', 'Descrição do produto', 'Produto Nome', 'Descrição do produto 2', 'publish', 'open', 'closed', '', 'produto-nome', '', '', '2019-06-29 15:00:49', '2019-06-29 18:00:49', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=12', 0, 'product', '', 0),
(13, 1, '2019-05-30 18:15:17', '2019-05-30 21:15:17', '', 'q1', '', 'inherit', 'open', 'closed', '', 'q1', '', '', '2019-05-30 18:15:17', '2019-05-30 21:15:17', '', 12, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/05/q1.png', 0, 'attachment', 'image/png', 0),
(14, 0, '2019-05-30 20:03:32', '2019-05-30 23:03:32', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5cf800952af667.39044636-d39bT7RytYQyOVEfLZq0dIdBhpLS7kBT', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=14', 0, 'scheduled-action', '', 3),
(15, 0, '2019-06-05 15:49:09', '2019-06-05 18:49:09', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5cf80f0f242dc3.50022788-Uujf3BJDbjNznKR2gybnoQPeUBnPikaw', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=15', 0, 'scheduled-action', '', 3),
(16, 0, '2019-06-05 16:50:55', '2019-06-05 19:50:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d10098cb565f6.31905340-4wGIFywriQ0GV2wKKCiZfbItFT6e63jp', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=16', 0, 'scheduled-action', '', 3),
(17, 0, '2019-06-23 21:21:48', '2019-06-24 00:21:48', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1017c7383a77.41893791-2D7x9DjCjWwkWkP9UVZAGpZ8E6Zu71vk', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=17', 0, 'scheduled-action', '', 3),
(19, 1, '2019-06-23 21:10:14', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-06-23 21:10:14', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/century_loja/?post_type=destaque&p=19', 0, 'destaque', '', 0),
(20, 1, '2019-06-23 21:11:32', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-06-23 21:11:32', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/century_loja/?post_type=destaque&p=20', 0, 'destaque', '', 0),
(21, 1, '2019-06-23 21:13:01', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-06-23 21:13:01', '0000-00-00 00:00:00', '', 0, 'http://localhost/projetos/century_loja/?post_type=destaque&p=21', 0, 'destaque', '', 0),
(22, 1, '2019-06-23 21:14:01', '2019-06-24 00:14:01', '', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2019-06-23 21:14:01', '2019-06-24 00:14:01', '', 0, 'http://localhost/projetos/century_loja/?post_type=destaque&#038;p=22', 0, 'destaque', '', 0),
(23, 1, '2019-06-23 21:13:48', '2019-06-24 00:13:48', '', 'destaque', '', 'inherit', 'open', 'closed', '', 'destaque', '', '', '2019-06-23 21:13:48', '2019-06-24 00:13:48', '', 22, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/destaque.png', 0, 'attachment', 'image/png', 0),
(24, 0, '2019-06-23 22:22:31', '2019-06-24 01:22:31', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1025e9abca30.54858592-0aajfTZnStrRjAPe5d1MdpNiF3jeaZ6L', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=24', 0, 'scheduled-action', '', 3),
(25, 1, '2019-06-23 21:39:36', '2019-06-24 00:39:36', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-06-23 21:39:36', '2019-06-24 00:39:36', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/logo-1.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '26', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=26', 1, 'nav_menu_item', '', 0),
(27, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=27', 13, 'nav_menu_item', '', 0),
(28, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=28', 14, 'nav_menu_item', '', 0),
(29, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '29', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=29', 15, 'nav_menu_item', '', 0),
(30, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=30', 12, 'nav_menu_item', '', 0),
(31, 0, '2019-06-23 23:22:49', '2019-06-24 02:22:49', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d10341f0eae19.71734031-gWyvE7sxYXMU9kQUCTgOGz1Ek4HbvzlW', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=31', 0, 'scheduled-action', '', 3),
(32, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Pedidos', '', 'publish', 'closed', 'closed', '', 'pedidos', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=32', 2, 'nav_menu_item', '', 0),
(33, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Downloads', '', 'publish', 'closed', 'closed', '', 'downloads', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=33', 3, 'nav_menu_item', '', 0),
(34, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Endereços', '', 'publish', 'closed', 'closed', '', 'enderecos', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=34', 4, 'nav_menu_item', '', 0),
(35, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Detalhes da conta', '', 'publish', 'closed', 'closed', '', 'detalhes-da-conta', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=35', 5, 'nav_menu_item', '', 0),
(36, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Pedidos', '', 'publish', 'closed', 'closed', '', 'pedidos-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=36', 6, 'nav_menu_item', '', 0),
(37, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Downloads', '', 'publish', 'closed', 'closed', '', 'downloads-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=37', 7, 'nav_menu_item', '', 0),
(38, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Endereços', '', 'publish', 'closed', 'closed', '', 'enderecos-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=38', 8, 'nav_menu_item', '', 0),
(39, 1, '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 'Detalhes da conta', '', 'publish', 'closed', 'closed', '', 'detalhes-da-conta-2', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=39', 11, 'nav_menu_item', '', 0),
(40, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Sair', '', 'publish', 'closed', 'closed', '', 'sair', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://localhost/projetos/century_loja/?p=40', 9, 'nav_menu_item', '', 0),
(41, 1, '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 'Senha perdida', '', 'publish', 'closed', 'closed', '', 'senha-perdida', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://localhost/projetos/century_loja/?p=41', 10, 'nav_menu_item', '', 0),
(42, 1, '2019-06-23 23:05:38', '2019-06-24 02:05:38', '', 'selo', '', 'inherit', 'open', 'closed', '', 'selo', '', '', '2019-06-23 23:05:38', '2019-06-24 02:05:38', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/selo.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2019-06-23 23:18:56', '2019-06-24 02:18:56', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-06-23 23:21:16', '2019-06-24 02:21:16', '', 0, 'http://localhost/projetos/century_loja/?page_id=43', 0, 'page', '', 0),
(45, 1, '2019-06-23 23:19:53', '2019-06-24 02:19:53', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-06-23 23:19:53', '2019-06-24 02:19:53', '', 43, 'http://localhost/projetos/century_loja/2019/06/23/43-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2019-06-23 23:18:56', '2019-06-24 02:18:56', '<!-- wp:paragraph -->\n<p>Inicial</p>\n<!-- /wp:paragraph -->', '', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-06-23 23:18:56', '2019-06-24 02:18:56', '', 43, 'http://localhost/projetos/century_loja/2019/06/23/43-revision-v1/', 0, 'revision', '', 0),
(46, 0, '2019-06-24 00:23:27', '2019-06-24 03:23:27', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1042703e7d20.49506471-JSCP5uja4Q5u7tskO3yvEVgbXM3udAtK', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=46', 0, 'scheduled-action', '', 3),
(47, 1, '2019-06-23 23:31:55', '2019-06-24 02:31:55', '', 'categoria1', '', 'inherit', 'open', 'closed', '', 'categoria1', '', '', '2019-06-23 23:31:55', '2019-06-24 02:31:55', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria1-1.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2019-06-24 00:01:25', '2019-06-24 03:01:25', '', 'categoria', '', 'inherit', 'open', 'closed', '', 'categoria', '', '', '2019-06-24 00:01:25', '2019-06-24 03:01:25', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/categoria.png', 0, 'attachment', 'image/png', 0),
(49, 0, '2019-06-24 01:24:32', '2019-06-24 04:24:32', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d154813c9bee2.66866334-DTnUOoO3oD0yHMEnHdko4Jn6uuyJGsXh', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=49', 0, 'scheduled-action', '', 3),
(50, 1, '2019-06-24 00:43:49', '2019-06-24 03:43:49', '', 'tenis', '', 'inherit', 'open', 'closed', '', 'tenis', '', '', '2019-06-24 00:43:49', '2019-06-24 03:43:49', '', 12, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/05/tenis-1.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2019-06-29 15:00:04', '2019-06-29 18:00:04', '', 'Camiseta', '', 'inherit', 'closed', 'closed', '', '65-autosave-v1', '', '', '2019-06-29 15:00:04', '2019-06-29 18:00:04', '', 65, 'http://localhost/projetos/century_loja/2019/06/29/65-autosave-v1/', 0, 'revision', '', 0),
(86, 0, '2019-06-29 16:19:51', '2019-06-29 19:19:51', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17b9e8982104.63293545-hupbwKlmyKKeqRN34WS0hmnEaOXKlRR8', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=86', 0, 'scheduled-action', '', 3),
(52, 1, '2019-06-24 00:45:09', '2019-06-24 03:45:09', '', 'Produto 2', '', 'publish', 'open', 'closed', '', 'produto-2', '', '', '2019-06-29 15:00:46', '2019-06-29 18:00:46', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=52', 0, 'product', '', 0),
(53, 1, '2019-06-24 00:45:02', '2019-06-24 03:45:02', '', 'D24-1738-304_zoom1', '', 'inherit', 'open', 'closed', '', 'd24-1738-304_zoom1', '', '', '2019-06-24 00:45:02', '2019-06-24 03:45:02', '', 52, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/D24-1738-304_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2019-06-24 00:46:24', '2019-06-24 03:46:24', '', 'Produto 3', '', 'publish', 'open', 'closed', '', 'produto-3', '', '', '2019-06-30 11:18:00', '2019-06-30 14:18:00', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=54', 0, 'product', '', 0),
(55, 1, '2019-06-24 00:46:20', '2019-06-24 03:46:20', '', '497-9505-026_zoom1', '', 'inherit', 'open', 'closed', '', '497-9505-026_zoom1', '', '', '2019-06-24 00:46:20', '2019-06-24 03:46:20', '', 54, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/497-9505-026_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2019-06-24 00:48:36', '2019-06-24 03:48:36', '', 'Produto 4', '', 'publish', 'open', 'closed', '', 'produto-4', '', '', '2019-06-30 11:17:46', '2019-06-30 14:17:46', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=56', 0, 'product', '', 0),
(57, 1, '2019-06-24 00:48:31', '2019-06-24 03:48:31', '', 'B78-2495-172_zoom1', '', 'inherit', 'open', 'closed', '', 'b78-2495-172_zoom1', '', '', '2019-06-24 00:48:31', '2019-06-24 03:48:31', '', 56, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/B78-2495-172_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2019-06-24 00:49:24', '2019-06-24 03:49:24', '', 'Produto 5', '', 'publish', 'open', 'closed', '', 'produto-5', '', '', '2019-06-30 11:17:37', '2019-06-30 14:17:37', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=58', 0, 'product', '', 0),
(59, 0, '2019-06-27 20:49:55', '2019-06-27 23:49:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d15562394d1b6.49028689-AGAWTQMsJqALIuweTTpmmmIBdKq0mZqU', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=59', 0, 'scheduled-action', '', 3),
(60, 0, '2019-06-27 21:49:55', '2019-06-28 00:49:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d156445847d05.21805590-mBWIvRM2vIGw651aR9tyaVgSRS6VhADf', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=60', 0, 'scheduled-action', '', 3),
(61, 0, '2019-06-27 22:50:13', '2019-06-28 01:50:13', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1572c48017a5.04672575-STrQ6E2hQ9tOoeu37rfyIZfH4zIOYq1g', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=61', 0, 'scheduled-action', '', 3),
(62, 0, '2019-06-27 23:52:04', '2019-06-28 02:52:04', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1580eaada822.22985298-eiNupFLU7WITTjGr0RxnDA1QFEpgYy1f', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=62', 0, 'scheduled-action', '', 3),
(63, 0, '2019-06-28 00:52:26', '2019-06-28 03:52:26', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d158f127cb670.23503789-Jo2ApGAgY36WYKClAOLuOf41IJqIfomQ', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=63', 0, 'scheduled-action', '', 3),
(64, 0, '2019-06-28 01:52:50', '2019-06-28 04:52:50', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d159d3829fe03.08608696-COvl37Zjc1LeumA3iMmNCnbB7inFzbJ3', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=64', 0, 'scheduled-action', '', 3),
(65, 1, '2019-06-28 01:11:13', '2019-06-28 04:11:13', '', 'Camiseta', '', 'publish', 'open', 'closed', '', 'camiseta', '', '', '2019-06-30 11:17:27', '2019-06-30 14:17:27', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=65', 0, 'product', '', 0),
(66, 1, '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', '296485', '', 'inherit', 'open', 'closed', '', '296485', '', '', '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', 65, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/296485.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', '296485-1', '', 'inherit', 'open', 'closed', '', '296485-1', '', '', '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', 65, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/296485-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(68, 0, '2019-06-28 02:53:12', '2019-06-28 05:53:12', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d177a9db1abc5.10751276-tncAAvshFmaSuFccfKEL1qmZiLnOHP5n', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=68', 0, 'scheduled-action', '', 3),
(69, 0, '2019-06-29 12:50:05', '2019-06-29 15:50:05', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1788d0e25b00.02566634-qZLEEzYtCffp4bCZAhErHg8grw8oP761', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=69', 0, 'scheduled-action', '', 3),
(70, 1, '2019-06-29 11:56:04', '2019-06-29 14:56:04', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off', '', '', '2019-06-29 11:56:04', '2019-06-29 14:56:04', '', 0, 'http://localhost/projetos/century_loja/?post_type=promocao&#038;p=70', 0, 'promocao', '', 0),
(71, 1, '2019-06-29 11:56:00', '2019-06-29 14:56:00', '', 'camiseta', '', 'inherit', 'open', 'closed', '', 'camiseta-2', '', '', '2019-06-29 11:56:00', '2019-06-29 14:56:00', '', 70, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/camiseta.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2019-06-29 11:56:31', '2019-06-29 14:56:31', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off-2', '', '', '2019-06-29 11:56:31', '2019-06-29 14:56:31', '', 0, 'http://localhost/projetos/century_loja/?post_type=promocao&#038;p=72', 0, 'promocao', '', 0),
(73, 1, '2019-06-29 11:56:51', '2019-06-29 14:56:51', '', 'Camiseta pela metade do preço', '', 'publish', 'closed', 'closed', '', 'camiseta-pela-metade-do-preco', '', '', '2019-06-29 11:56:51', '2019-06-29 14:56:51', '', 0, 'http://localhost/projetos/century_loja/?post_type=promocao&#038;p=73', 0, 'promocao', '', 0),
(74, 1, '2019-06-29 11:57:09', '2019-06-29 14:57:09', '', 'Liquidção de<br>shorts esportivos', '', 'publish', 'closed', 'closed', '', 'liquidcao-deshorts-esportivos', '', '', '2019-06-29 11:57:09', '2019-06-29 14:57:09', '', 0, 'http://localhost/projetos/century_loja/?post_type=promocao&#038;p=74', 0, 'promocao', '', 0),
(75, 1, '2019-06-29 11:57:36', '2019-06-29 14:57:36', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off-3', '', '', '2019-06-29 12:05:59', '2019-06-29 15:05:59', '', 0, 'http://localhost/projetos/century_loja/?post_type=promocao&#038;p=75', 0, 'promocao', '', 0),
(76, 1, '2019-06-29 12:07:46', '2019-06-29 15:07:46', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2019-06-29 12:07:46', '2019-06-29 15:07:46', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/banner.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2019-06-29 12:38:07', '2019-06-29 15:38:07', '', 'neckties-210347_960_720', '', 'inherit', 'open', 'closed', '', 'neckties-210347_960_720', '', '', '2019-06-29 12:38:07', '2019-06-29 15:38:07', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/neckties-210347_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2019-06-29 12:38:38', '2019-06-29 15:38:38', '', 'tartan-track-2678544_960_720', '', 'inherit', 'open', 'closed', '', 'tartan-track-2678544_960_720', '', '', '2019-06-29 12:38:38', '2019-06-29 15:38:38', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/tartan-track-2678544_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2019-06-29 12:48:21', '2019-06-29 15:48:21', '', '184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe', '', 'inherit', 'open', 'closed', '', '184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe', '', '', '2019-06-29 12:48:21', '2019-06-29 15:48:21', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2019-06-29 12:48:23', '2019-06-29 15:48:23', '', '2000px-Adidas_klassisches_logo.svg', '', 'inherit', 'open', 'closed', '', '2000px-adidas_klassisches_logo-svg', '', '', '2019-06-29 12:48:23', '2019-06-29 15:48:23', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/2000px-Adidas_klassisches_logo.svg_.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2019-06-29 12:48:27', '2019-06-29 15:48:27', '', 'asics', '', 'inherit', 'open', 'closed', '', 'asics', '', '', '2019-06-29 12:48:27', '2019-06-29 15:48:27', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/asics.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2019-06-29 12:48:28', '2019-06-29 15:48:28', '', 'nike-logo-47A65A59D5-seeklogo.com', '', 'inherit', 'open', 'closed', '', 'nike-logo-47a65a59d5-seeklogo-com', '', '', '2019-06-29 12:48:28', '2019-06-29 15:48:28', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/nike-logo-47A65A59D5-seeklogo.com_.png', 0, 'attachment', 'image/png', 0),
(83, 0, '2019-06-29 13:50:40', '2019-06-29 16:50:40', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d179db73b1572.72475909-4vkrfmri52UPLErPpjOG3WQjGHb5sxLX', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=83', 0, 'scheduled-action', '', 3),
(84, 0, '2019-06-29 15:19:51', '2019-06-29 18:19:51', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17abc7636208.20251769-a37BbWttZLqbJdxFyAR8Gl2QiznQoOM0', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=84', 0, 'scheduled-action', '', 3),
(87, 1, '2019-06-29 15:47:19', '2019-06-29 18:47:19', '', 'Regatas', '', 'publish', 'open', 'closed', '', 'regatas', '', '', '2019-06-30 11:17:17', '2019-06-30 14:17:17', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=87', 0, 'product', '', 0),
(88, 1, '2019-06-29 15:47:03', '2019-06-29 18:47:03', '', 'fff', '', 'inherit', 'open', 'closed', '', 'fff', '', '', '2019-06-29 15:47:03', '2019-06-29 18:47:03', '', 87, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/fff.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2019-06-29 15:49:06', '2019-06-29 18:49:06', '', 'Tenis', '', 'publish', 'open', 'closed', '', 'tenis', '', '', '2019-06-30 11:17:06', '2019-06-30 14:17:06', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=89', 0, 'product', '', 0),
(90, 1, '2019-06-29 15:48:21', '2019-06-29 18:48:21', '', 'asd', '', 'inherit', 'open', 'closed', '', 'asd', '', '', '2019-06-29 15:48:21', '2019-06-29 18:48:21', '', 89, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/asd.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2019-06-29 15:50:43', '2019-06-29 18:50:43', '', 'Calça', '', 'publish', 'open', 'closed', '', 'calca', '', '', '2019-06-30 11:16:51', '2019-06-30 14:16:51', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=91', 0, 'product', '', 0),
(92, 1, '2019-06-29 15:50:25', '2019-06-29 18:50:25', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar', '', '', '2019-06-29 15:50:25', '2019-06-29 18:50:25', '', 91, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/Capturar.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 0, '2019-06-29 17:20:08', '2019-06-29 20:20:08', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17c85b9e09a7.72898956-Zvebtak9cVt7bBCLGx7MttJSgrAImVpH', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=93', 0, 'scheduled-action', '', 3),
(94, 0, '2019-06-29 18:21:47', '2019-06-29 21:21:47', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17d72a97d8d7.57777513-W0ytQCqA65Wa74lzJfwopIEEm2Tv2sk5', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=94', 0, 'scheduled-action', '', 3),
(95, 0, '2019-06-29 19:24:58', '2019-06-29 22:24:58', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17e570398eb3.10669841-29aFuBJSmOI3QAhqV3lbO3PvRgrCd6rS', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=95', 0, 'scheduled-action', '', 3),
(96, 0, '2019-06-29 20:25:52', '2019-06-29 23:25:52', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17f382823c98.61658026-ycd1ywdLEqunOhQRztTW40x0XjgM8iX3', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=96', 0, 'scheduled-action', '', 3),
(97, 1, '2019-06-29 19:32:43', '2019-06-29 22:32:43', '', 'Produto A', '', 'publish', 'open', 'closed', '', 'produto-a', '', '', '2019-06-30 11:16:39', '2019-06-30 14:16:39', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=97', 0, 'product', '', 0),
(98, 1, '2019-06-29 19:32:32', '2019-06-29 22:32:32', '', 'asasd', '', 'inherit', 'open', 'closed', '', 'asasd', '', '', '2019-06-29 19:32:32', '2019-06-29 22:32:32', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/asasd.jpg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2019-06-29 19:32:33', '2019-06-29 22:32:33', '', 'bnb', '', 'inherit', 'open', 'closed', '', 'bnb', '', '', '2019-06-29 19:32:33', '2019-06-29 22:32:33', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/bnb.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2019-06-29 19:32:34', '2019-06-29 22:32:34', '', 'dfd', '', 'inherit', 'open', 'closed', '', 'dfd', '', '', '2019-06-29 19:32:34', '2019-06-29 22:32:34', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/dfd.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2019-06-29 19:32:35', '2019-06-29 22:32:35', '', 'kj', '', 'inherit', 'open', 'closed', '', 'kj', '', '', '2019-06-29 19:32:35', '2019-06-29 22:32:35', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/kj.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2019-06-29 19:32:36', '2019-06-29 22:32:36', '', 'mnm', '', 'inherit', 'open', 'closed', '', 'mnm', '', '', '2019-06-29 19:32:36', '2019-06-29 22:32:36', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/mnm.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2019-06-29 19:32:37', '2019-06-29 22:32:37', '', 'vb', '', 'inherit', 'open', 'closed', '', 'vb', '', '', '2019-06-29 19:32:37', '2019-06-29 22:32:37', '', 97, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/vb.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2019-06-29 19:33:19', '2019-06-29 22:33:19', '', 'Produto B', '', 'publish', 'open', 'closed', '', 'produto-b', '', '', '2019-06-30 11:16:29', '2019-06-30 14:16:29', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=104', 0, 'product', '', 0),
(105, 1, '2019-06-29 19:34:13', '2019-06-29 22:34:13', '', 'Produto C', '', 'publish', 'open', 'closed', '', 'produto-c', '', '', '2019-06-30 11:16:18', '2019-06-30 14:16:18', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=105', 0, 'product', '', 0),
(106, 1, '2019-06-29 19:34:51', '2019-06-29 22:34:51', '', 'Produto D', '', 'publish', 'open', 'closed', '', 'produto-d', '', '', '2019-06-30 11:16:08', '2019-06-30 14:16:08', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=106', 0, 'product', '', 0),
(107, 1, '2019-06-29 19:35:51', '2019-06-29 22:35:51', '', 'Produto E', '', 'publish', 'open', 'closed', '', 'produto-e', '', '', '2019-06-30 11:15:53', '2019-06-30 14:15:53', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=107', 0, 'product', '', 0),
(108, 1, '2019-06-29 19:36:37', '2019-06-29 22:36:37', '', 'Produto F', '', 'publish', 'open', 'closed', '', 'produto-f', '', '', '2019-06-30 11:15:35', '2019-06-30 14:15:35', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=108', 0, 'product', '', 0),
(109, 1, '2019-06-29 19:37:21', '2019-06-29 22:37:21', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\r\n<p class=\"desc-detalhes\"></p>', 'Produto D', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas!</p>', 'publish', 'open', 'closed', '', 'produto-d-2', '', '', '2019-06-30 15:36:32', '2019-06-30 18:36:32', '', 0, 'http://localhost/projetos/century_loja/?post_type=product&#038;p=109', 0, 'product', '', 0),
(110, 0, '2019-06-29 21:25:54', '2019-06-30 00:25:54', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d18046e3d00d4.70656836-gMv3m6MwVUlB6FJ60B5TSDjDNUKeCzhZ', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=110', 0, 'scheduled-action', '', 3),
(111, 0, '2019-06-29 22:38:06', '2019-06-30 01:38:06', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d181282140fe7.05224689-7T0jEcnGhiAnzacGl6dXMBPwF7zcSU3R', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=111', 0, 'scheduled-action', '', 3),
(112, 0, '2019-06-29 23:38:10', '2019-06-30 02:38:10', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d18b972dc1827.56917889-uvuU45wZrwGSkuMXsA2HVwsIVWnUOJTk', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&#038;p=112', 0, 'scheduled-action', '', 3),
(113, 0, '2019-06-30 11:30:26', '2019-06-30 14:30:26', '[]', 'wc_admin_unsnooze_admin_notes', '', 'trash', 'open', 'closed', '', '', '', '', '2019-06-30 11:30:26', '2019-06-30 14:30:26', '', 0, 'http://localhost/projetos/century_loja/?post_type=scheduled-action&p=113', 0, 'scheduled-action', '', 1),
(114, 1, '2019-06-30 14:10:32', '2019-06-30 17:10:32', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\n<p class=\"desc-detalhes\"></p>', 'Produto D', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\n<p class=\"desc-detalhes\">Marca: Nike</p>\n<p class=\"desc-detalhes\">Modelo: 003624</p>', 'inherit', 'closed', 'closed', '', '109-autosave-v1', '', '', '2019-06-30 14:10:32', '2019-06-30 17:10:32', '', 109, 'http://localhost/projetos/century_loja/2019/06/30/109-autosave-v1/', 0, 'revision', '', 0),
(115, 1, '2019-06-30 11:11:41', '2019-06-30 14:11:41', '', 'Produto D - Verde, M', 'Cor: Verde, Tamanho: M', 'publish', 'closed', 'closed', '', 'produto-d', '', '', '2019-06-30 11:14:49', '2019-06-30 14:14:49', '', 109, 'http://localhost/projetos/century_loja/?post_type=product_variation&#038;p=115', 1, 'product_variation', '', 0),
(116, 1, '2019-06-30 11:13:21', '2019-06-30 14:13:21', '', 'Produto D - Amarelo, P', 'Cor: Amarelo, Tamanho: P', 'publish', 'closed', 'closed', '', 'produto-d-2', '', '', '2019-06-30 13:38:27', '2019-06-30 16:38:27', '', 109, 'http://localhost/projetos/century_loja/?post_type=product_variation&#038;p=116', 2, 'product_variation', '', 0),
(117, 1, '2019-06-30 11:18:12', '2019-06-30 14:18:12', '', 'Produto 3', '', 'inherit', 'closed', 'closed', '', '54-autosave-v1', '', '', '2019-06-30 11:18:12', '2019-06-30 14:18:12', '', 54, 'http://localhost/projetos/century_loja/2019/06/30/54-autosave-v1/', 0, 'revision', '', 0),
(118, 1, '2019-06-30 12:22:01', '2019-06-30 15:22:01', '', '08-06-2015-banner-esportes-kanui', '', 'inherit', 'open', 'closed', '', '08-06-2015-banner-esportes-kanui', '', '', '2019-06-30 12:22:01', '2019-06-30 15:22:01', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 0, 'attachment', 'image/jpeg', 0),
(119, 1, '2019-06-30 12:28:14', '2019-06-30 15:28:14', '', 'cat_feminino_moletons', '', 'inherit', 'open', 'closed', '', 'cat_feminino_moletons', '', '', '2019-06-30 12:28:14', '2019-06-30 15:28:14', '', 0, 'http://localhost/projetos/century_loja/wp-content/uploads/2019/06/cat_feminino_moletons.jpg', 0, 'attachment', 'image/jpeg', 0),
(120, 1, '2019-06-30 14:48:36', '2019-06-30 17:48:36', '<!-- wp:shortcode -->\n[woocommerce_cart]\n<!-- /wp:shortcode -->', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-06-30 14:48:36', '2019-06-30 17:48:36', '', 7, 'http://localhost/projetos/century_loja/2019/06/30/7-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 'Loja', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 6, 'http://localhost/projetos/century_loja/2019/06/30/6-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2019-06-30 16:31:24', '2019-06-30 19:31:24', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Minha Conta', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-06-30 16:31:24', '2019-06-30 19:31:24', '', 9, 'http://localhost/projetos/century_loja/2019/06/30/9-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2019-06-30 16:31:49', '2019-06-30 19:31:49', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Finalizar Compra', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-06-30 16:31:49', '2019-06-30 19:31:49', '', 8, 'http://localhost/projetos/century_loja/2019/06/30/8-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_termmeta`
--

DROP TABLE IF EXISTS `cp_termmeta`;
CREATE TABLE IF NOT EXISTS `cp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_termmeta`
--

INSERT INTO `cp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(5, 19, 'display_type', ''),
(4, 19, 'order', '0'),
(3, 15, 'product_count_product_cat', '0'),
(6, 19, 'thumbnail_id', '48'),
(7, 20, 'order', '0'),
(8, 20, 'display_type', ''),
(9, 20, 'thumbnail_id', '48'),
(10, 21, 'order', '0'),
(11, 21, 'display_type', ''),
(12, 21, 'thumbnail_id', '48'),
(13, 22, 'order', '0'),
(14, 22, 'display_type', ''),
(15, 22, 'thumbnail_id', '48'),
(16, 23, 'order', '0'),
(17, 23, 'display_type', ''),
(18, 23, 'thumbnail_id', '48'),
(19, 24, 'order', '0'),
(20, 24, 'display_type', ''),
(21, 24, 'thumbnail_id', '48'),
(64, 31, 'product_count_product_cat', '4'),
(63, 32, 'product_count_product_cat', '3'),
(62, 34, 'product_count_product_cat', '4'),
(61, 33, 'product_count_product_cat', '5'),
(28, 22, 'product_count_product_cat', '7'),
(30, 21, 'product_count_product_cat', '9'),
(31, 20, 'product_count_product_cat', '9'),
(32, 19, 'product_count_product_cat', '9'),
(33, 24, 'product_count_product_cat', '7'),
(60, 30, 'product_count_product_cat', '6'),
(35, 23, 'product_count_product_cat', '9'),
(36, 27, 'order', '0'),
(37, 27, 'display_type', ''),
(38, 27, 'thumbnail_id', '78'),
(39, 28, 'order', '0'),
(40, 28, 'display_type', ''),
(41, 28, 'thumbnail_id', '0'),
(42, 29, 'order', '0'),
(43, 29, 'display_type', ''),
(44, 29, 'thumbnail_id', '82'),
(45, 30, 'order', '0'),
(46, 30, 'display_type', ''),
(47, 30, 'thumbnail_id', '80'),
(48, 31, 'order', '0'),
(49, 31, 'display_type', ''),
(50, 31, 'thumbnail_id', '81'),
(51, 32, 'order', '0'),
(52, 32, 'display_type', ''),
(53, 32, 'thumbnail_id', '81'),
(54, 33, 'order', '0'),
(55, 33, 'display_type', ''),
(56, 33, 'thumbnail_id', '79'),
(57, 34, 'order', '0'),
(58, 34, 'display_type', ''),
(59, 34, 'thumbnail_id', '79'),
(65, 29, 'product_count_product_cat', '3'),
(66, 27, 'product_count_product_cat', '7'),
(67, 28, 'product_count_product_cat', '16'),
(68, 35, 'order', '0'),
(69, 35, 'display_type', ''),
(70, 35, 'thumbnail_id', '48'),
(71, 36, 'order', '0'),
(72, 36, 'display_type', ''),
(73, 36, 'thumbnail_id', '48'),
(74, 36, 'product_count_product_cat', '4'),
(75, 35, 'product_count_product_cat', '3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_terms`
--

DROP TABLE IF EXISTS `cp_terms`;
CREATE TABLE IF NOT EXISTS `cp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_terms`
--

INSERT INTO `cp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'wc-admin-notes', 'wc-admin-notes', 0),
(19, 'Camisetas', 'camisetas', 0),
(18, 'Menu Principal', 'menu-principal', 0),
(20, 'Calças', 'calcas', 0),
(21, 'Blusas', 'blusas', 0),
(22, 'Bermudas', 'bermudas', 0),
(23, 'Tênis', 'tenis', 0),
(24, 'Regatas', 'regatas', 0),
(35, 'Shorts', 'shorts', 0),
(27, 'Marcas', 'marcas', 0),
(28, 'Vestuário', 'vestuario', 0),
(29, 'Nike', 'nike', 0),
(30, 'Adidas', 'adidas', 0),
(31, 'Minzuno', 'minzuno', 0),
(32, 'Cross', 'cross', 0),
(33, 'ALL-STAR', 'all-star', 0),
(34, 'Converse', 'converse', 0),
(36, 'Meias', 'meias', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_term_relationships`
--

DROP TABLE IF EXISTS `cp_term_relationships`;
CREATE TABLE IF NOT EXISTS `cp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_term_relationships`
--

INSERT INTO `cp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(49, 16, 0),
(12, 2, 0),
(14, 16, 0),
(15, 16, 0),
(16, 16, 0),
(17, 16, 0),
(24, 16, 0),
(26, 18, 0),
(27, 18, 0),
(28, 18, 0),
(29, 18, 0),
(30, 18, 0),
(31, 16, 0),
(32, 18, 0),
(33, 18, 0),
(34, 18, 0),
(35, 18, 0),
(36, 18, 0),
(37, 18, 0),
(38, 18, 0),
(40, 18, 0),
(41, 18, 0),
(39, 18, 0),
(46, 16, 0),
(12, 22, 0),
(12, 32, 0),
(12, 21, 0),
(12, 20, 0),
(12, 19, 0),
(12, 24, 0),
(52, 32, 0),
(12, 23, 0),
(52, 22, 0),
(12, 34, 0),
(52, 21, 0),
(52, 20, 0),
(52, 19, 0),
(52, 24, 0),
(52, 34, 0),
(52, 23, 0),
(52, 2, 0),
(54, 22, 0),
(12, 33, 0),
(54, 21, 0),
(54, 20, 0),
(54, 19, 0),
(54, 24, 0),
(52, 33, 0),
(54, 23, 0),
(54, 2, 0),
(56, 22, 0),
(12, 30, 0),
(56, 21, 0),
(56, 20, 0),
(56, 19, 0),
(56, 24, 0),
(52, 30, 0),
(56, 23, 0),
(56, 2, 0),
(58, 22, 0),
(52, 29, 0),
(58, 20, 0),
(58, 19, 0),
(58, 24, 0),
(84, 16, 0),
(58, 23, 0),
(58, 2, 0),
(59, 16, 0),
(60, 16, 0),
(61, 16, 0),
(62, 16, 0),
(63, 16, 0),
(64, 16, 0),
(52, 31, 0),
(65, 21, 0),
(65, 20, 0),
(65, 19, 0),
(65, 24, 0),
(83, 16, 0),
(65, 23, 0),
(65, 2, 0),
(68, 16, 0),
(69, 16, 0),
(12, 31, 0),
(12, 29, 0),
(65, 30, 0),
(65, 33, 0),
(65, 34, 0),
(65, 32, 0),
(65, 31, 0),
(65, 29, 0),
(86, 16, 0),
(65, 22, 0),
(65, 36, 0),
(65, 35, 0),
(87, 24, 0),
(87, 2, 0),
(89, 23, 0),
(89, 2, 0),
(91, 20, 0),
(91, 2, 0),
(91, 30, 0),
(93, 16, 0),
(94, 16, 0),
(95, 16, 0),
(96, 16, 0),
(97, 30, 0),
(97, 33, 0),
(97, 34, 0),
(97, 21, 0),
(97, 19, 0),
(97, 36, 0),
(97, 35, 0),
(97, 2, 0),
(104, 30, 0),
(104, 33, 0),
(104, 20, 0),
(104, 19, 0),
(104, 23, 0),
(104, 2, 0),
(105, 19, 0),
(105, 2, 0),
(106, 31, 0),
(106, 21, 0),
(106, 2, 0),
(107, 22, 0),
(107, 21, 0),
(107, 36, 0),
(107, 2, 0),
(108, 36, 0),
(108, 23, 0),
(108, 2, 0),
(109, 21, 0),
(109, 20, 0),
(109, 35, 0),
(110, 16, 0),
(111, 16, 0),
(112, 16, 0),
(113, 16, 0),
(109, 4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_term_taxonomy`
--

DROP TABLE IF EXISTS `cp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `cp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_term_taxonomy`
--

INSERT INTO `cp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 15),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 1),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'action-group', '', 0, 26),
(19, 19, 'product_cat', '', 28, 9),
(18, 18, 'nav_menu', '', 0, 15),
(20, 20, 'product_cat', '', 28, 9),
(21, 21, 'product_cat', '', 28, 9),
(22, 22, 'product_cat', '', 28, 7),
(23, 23, 'product_cat', '', 28, 9),
(24, 24, 'product_cat', '', 28, 7),
(27, 27, 'product_cat', '', 0, 0),
(28, 28, 'product_cat', '', 0, 0),
(29, 29, 'product_cat', '', 27, 3),
(30, 30, 'product_cat', '', 27, 6),
(31, 31, 'product_cat', '', 27, 4),
(32, 32, 'product_cat', '', 27, 3),
(33, 33, 'product_cat', '', 27, 5),
(34, 34, 'product_cat', '', 27, 4),
(35, 35, 'product_cat', '', 28, 3),
(36, 36, 'product_cat', '', 28, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_usermeta`
--

DROP TABLE IF EXISTS `cp_usermeta`;
CREATE TABLE IF NOT EXISTS `cp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=53 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_usermeta`
--

INSERT INTO `cp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'centurysports'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"5cf1a23f826f16f67591919bcf9d2d4cf30b1459125d79c9887db401abaffcad\";a:4:{s:10:\"expiration\";i:1561992897;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561820097;}s:64:\"334ca617b5ae18cfd6cb44caf1aa7ca5784c3f63aafb8dd5e5843cbc0e00d56e\";a:4:{s:10:\"expiration\";i:1562075028;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1561902228;}}'),
(17, 1, 'cp_dashboard_quick_press_last_post_id', '18'),
(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(19, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(20, 1, '_woocommerce_tracks_anon_id', 'woo:tuOlm6PvDEuvTYmB/+vf1+uC'),
(21, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:10;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1200;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1200;s:8:\"line_tax\";i:0;}}}'),
(22, 1, 'wc_last_active', '1561852800'),
(23, 1, 'cp_user-settings', 'libraryContent=browse&unfold=1&mfold=o'),
(24, 1, 'cp_user-settings-time', '1561821946'),
(25, 1, '_order_count', '0'),
(29, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:20:\"categoriaDestaquediv\";}'),
(28, 1, 'cp_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1561418612;}'),
(30, 1, 'metaboxhidden_destaque', 'a:2:{i:0;s:20:\"categoriaDestaquediv\";i:1;s:7:\"slugdiv\";}'),
(31, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(32, 1, 'metaboxhidden_nav-menus', 'a:6:{i:0;s:21:\"add-post-type-product\";i:1;s:22:\"add-post-type-destaque\";i:2;s:22:\"add-post-type-promocao\";i:3;s:12:\"add-post_tag\";i:4;s:15:\"add-product_cat\";i:5;s:15:\"add-product_tag\";}'),
(33, 1, 'nav_menu_recently_edited', '18'),
(37, 2, 'nickname', 'agenciahcdesenvolvimentos'),
(38, 2, 'first_name', ''),
(39, 2, 'last_name', ''),
(40, 2, 'description', ''),
(41, 2, 'rich_editing', 'true'),
(42, 2, 'syntax_highlighting', 'true'),
(43, 2, 'comment_shortcuts', 'false'),
(44, 2, 'admin_color', 'fresh'),
(45, 2, 'use_ssl', '0'),
(46, 2, 'show_admin_bar_front', 'true'),
(47, 2, 'locale', ''),
(48, 2, 'cp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(49, 2, 'cp_user_level', '0'),
(51, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"2723d092b63885e0d7c260cc007e8b9d\";a:11:{s:3:\"key\";s:32:\"2723d092b63885e0d7c260cc007e8b9d\";s:10:\"product_id\";i:109;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:123;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:123;s:8:\"line_tax\";i:0;}}}'),
(52, 2, 'wc_last_active', '1561852800');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_users`
--

DROP TABLE IF EXISTS `cp_users`;
CREATE TABLE IF NOT EXISTS `cp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_users`
--

INSERT INTO `cp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'centurysports', '$P$B2G0NGM/dYPLgqXd3.KGFWtNjudq6j1', 'centurysports', 'devhcdesenvolvimentos@gmail.com', '', '2019-05-30 20:44:06', '', 0, 'centurysports'),
(2, 'agenciahcdesenvolvimentos', '$P$BrjECFE0V9J.6XKJyP77q0Es9XkA22/', 'agenciahcdesenvolvimentos', 'agenciahcdesenvolvimentos@gmail.com', '', '2019-06-30 19:34:30', '', 0, 'agenciahcdesenvolvimentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_admin_notes`
--

DROP TABLE IF EXISTS `cp_wc_admin_notes`;
CREATE TABLE IF NOT EXISTS `cp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_admin_notes`
--

INSERT INTO `cp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-05-30 21:02:14', NULL, 0),
(2, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-05-30 21:02:14', NULL, 0),
(3, 'wc-admin-store-notice-giving-feedback', 'info', 'en_US', 'Giving feedback', 'Are you enjoying the new WooCommerce experience? We\'d love to get your feedback.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-06-05 17:49:20', NULL, 0),
(4, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', 'phone', '{}', 'unactioned', 'woocommerce-admin', '2019-06-05 17:49:20', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_admin_note_actions`
--

DROP TABLE IF EXISTS `cp_wc_admin_note_actions`;
CREATE TABLE IF NOT EXISTS `cp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`),
  KEY `note_id` (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_admin_note_actions`
--

INSERT INTO `cp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', '', 0),
(2, 2, 'connect', 'Connect', '?page=wc-addons&section=helper', '', 0),
(3, 3, 'share-feedback', 'Share feedback', 'https://github.com/woocommerce/woocommerce-admin/issues/new/choose', '', 0),
(4, 4, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_customer_lookup`
--

DROP TABLE IF EXISTS `cp_wc_customer_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_download_log`
--

DROP TABLE IF EXISTS `cp_wc_download_log`;
CREATE TABLE IF NOT EXISTS `cp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  PRIMARY KEY (`download_log_id`),
  KEY `permission_id` (`permission_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_coupon_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_coupon_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`coupon_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_product_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_product_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `customer_id` (`customer_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_stats`
--

DROP TABLE IF EXISTS `cp_wc_order_stats`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `gross_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_id`),
  KEY `date_created` (`date_created`),
  KEY `customer_id` (`customer_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_tax_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_tax_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`tax_rate_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_product_meta_lookup`
--

DROP TABLE IF EXISTS `cp_wc_product_meta_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(10,2) DEFAULT NULL,
  `max_price` decimal(10,2) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `virtual` (`virtual`),
  KEY `downloadable` (`downloadable`),
  KEY `stock_status` (`stock_status`),
  KEY `stock_quantity` (`stock_quantity`),
  KEY `onsale` (`onsale`),
  KEY `min_max_price` (`min_price`,`max_price`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_product_meta_lookup`
--

INSERT INTO `cp_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`) VALUES
(12, '123', 0, 0, '120.00', '120.00', 0, 10, 'instock', 0, '0.00', 0),
(52, '', 0, 0, '0.00', '0.00', 0, NULL, 'instock', 0, '0.00', 0),
(54, '000999', 0, 0, '11.49', '11.49', 1, 88898, 'instock', 0, '0.00', 0),
(56, '455454', 0, 0, '242.00', '242.00', 1, 4543534, 'instock', 0, '0.00', 0),
(58, '2222', 0, 0, '12.50', '12.50', 0, 222, 'instock', 0, '0.00', 0),
(65, '9999', 0, 0, '180.00', '180.00', 0, 999, 'instock', 0, '0.00', 0),
(87, '88888', 0, 0, '123.00', '123.00', 0, 888, 'instock', 0, '0.00', 0),
(89, '7777', 0, 0, '50.00', '50.00', 0, 7777, 'instock', 0, '0.00', 0),
(91, '667777', 0, 0, '34.00', '34.00', 0, 777, 'instock', 0, '0.00', 0),
(97, '234242', 0, 0, '300.00', '300.00', 0, 43534, 'instock', 0, '0.00', 0),
(104, '6767667', 0, 0, '250.00', '250.00', 0, 4564564, 'instock', 0, '0.00', 0),
(105, '56565656', 0, 0, '350.00', '350.00', 0, 56456456, 'instock', 0, '0.00', 0),
(106, '45444554', 0, 0, '450.00', '450.00', 0, 4545454, 'instock', 0, '0.00', 0),
(107, '55454', 0, 0, '950.00', '950.00', 0, 3453453, 'instock', 0, '0.00', 0),
(108, '56555', 0, 0, '95.00', '95.00', 0, 3453453, 'instock', 0, '0.00', 0),
(109, '', 0, 0, '123.00', '200.00', 0, 123, 'instock', 0, '0.00', 0),
(115, '99999', 0, 0, '200.00', '200.00', 0, NULL, 'instock', 0, '0.00', 0),
(116, '999', 0, 0, '123.00', '123.00', 0, 12, 'instock', 0, '0.00', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_webhooks`
--

DROP TABLE IF EXISTS `cp_wc_webhooks`;
CREATE TABLE IF NOT EXISTS `cp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`webhook_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_api_keys`
--

DROP TABLE IF EXISTS `cp_woocommerce_api_keys`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  PRIMARY KEY (`key_id`),
  KEY `consumer_key` (`consumer_key`),
  KEY `consumer_secret` (`consumer_secret`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_attribute_taxonomies`
--

DROP TABLE IF EXISTS `cp_woocommerce_attribute_taxonomies`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_downloadable_product_permissions`
--

DROP TABLE IF EXISTS `cp_woocommerce_downloadable_product_permissions`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  KEY `order_id` (`order_id`),
  KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_log`
--

DROP TABLE IF EXISTS `cp_woocommerce_log`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`log_id`),
  KEY `level` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_order_itemmeta`
--

DROP TABLE IF EXISTS `cp_woocommerce_order_itemmeta`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_order_items`
--

DROP TABLE IF EXISTS `cp_woocommerce_order_items`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_payment_tokenmeta`
--

DROP TABLE IF EXISTS `cp_woocommerce_payment_tokenmeta`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `payment_token_id` (`payment_token_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_payment_tokens`
--

DROP TABLE IF EXISTS `cp_woocommerce_payment_tokens`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_sessions`
--

DROP TABLE IF EXISTS `cp_woocommerce_sessions`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_key` (`session_key`)
) ENGINE=MyISAM AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_sessions`
--

INSERT INTO `cp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(4, '1', 'a:12:{s:4:\"cart\";s:415:\"a:1:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:10;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1200;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1200;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:412:\"a:15:{s:8:\"subtotal\";s:7:\"1200.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:7:\"1200.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:7:\"1217.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:415:\"a:1:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:10;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1200;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1200;s:8:\"line_tax\";i:0;}}\";s:8:\"customer\";s:723:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:22:\"shipping_for_package_0\";s:380:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_b5d05d9ae90b82ee07c3c9eb38bc1c6f\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:23:\"Produto Nome &times; 10\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:10:\"wc_notices\";s:233:\"a:1:{s:7:\"success\";a:1:{i:0;s:194:\"&ldquo;Produto Nome&rdquo; removido. <a href=\"http://localhost/projetos/century_loja/cart/?undo_item=c20ad4d76fe97759aa27a0c99bff6710&#038;_wpnonce=2ab5638642\" class=\"restore-item\">Desfazer?</a>\";}}\";}', 1562020193),
(6, '846469ac7e78b8e968b7b42c9a2300e2', 'a:12:{s:4:\"cart\";s:412:\"a:1:{s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";a:11:{s:3:\"key\";s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";s:10:\"product_id\";i:65;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:180;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:180;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:409:\"a:15:{s:8:\"subtotal\";s:6:\"180.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"180.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"197.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:889:\"a:2:{s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";a:11:{s:3:\"key\";s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";s:10:\"product_id\";i:65;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:180;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:180;s:8:\"line_tax\";i:0;}s:32:\"b5e7e62a81dfe0d039321531f2c4d347\";a:11:{s:3:\"key\";s:32:\"b5e7e62a81dfe0d039321531f2c4d347\";s:10:\"product_id\";i:109;s:12:\"variation_id\";i:116;s:9:\"variation\";a:2:{s:13:\"attribute_cor\";s:7:\"Amarelo\";s:17:\"attribute_tamanho\";s:1:\"P\";}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"6aeed39936bcc132ec3f32bc69967952\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:123;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:123;s:8:\"line_tax\";i:0;}}\";s:22:\"shipping_for_package_0\";s:375:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_330645011d8ba58e9fddd78809e2f05f\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:18:\"Camiseta &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:10:\"wc_notices\";N;s:8:\"customer\";s:691:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1562077173),
(5, 'f93970dcbfb208cda1cc8eb1ace0da04', 'a:12:{s:4:\"cart\";s:818:\"a:2:{s:32:\"c7e1249ffc03eb9ded908c236bd1996d\";a:11:{s:3:\"key\";s:32:\"c7e1249ffc03eb9ded908c236bd1996d\";s:10:\"product_id\";i:87;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:6;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:738;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:738;s:8:\"line_tax\";i:0;}s:32:\"54229abfcfa5649e7003b83dd4755294\";a:11:{s:3:\"key\";s:32:\"54229abfcfa5649e7003b83dd4755294\";s:10:\"product_id\";i:91;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:3;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:102;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:102;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:409:\"a:15:{s:8:\"subtotal\";s:6:\"840.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"840.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"857.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:1228:\"a:3:{s:32:\"a684eceee76fc522773286a895bc8436\";a:11:{s:3:\"key\";s:32:\"a684eceee76fc522773286a895bc8436\";s:10:\"product_id\";i:54;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:11.49;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:11.49;s:8:\"line_tax\";i:0;}s:32:\"c7e1249ffc03eb9ded908c236bd1996d\";a:11:{s:3:\"key\";s:32:\"c7e1249ffc03eb9ded908c236bd1996d\";s:10:\"product_id\";i:87;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:123;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:123;s:8:\"line_tax\";i:0;}s:32:\"9f61408e3afb633e50cdf1b20de6f466\";a:11:{s:3:\"key\";s:32:\"9f61408e3afb633e50cdf1b20de6f466\";s:10:\"product_id\";i:56;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:484;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:484;s:8:\"line_tax\";i:0;}}\";s:22:\"shipping_for_package_0\";s:392:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_a742fbf05855e3a7e9e49af683ed35eb\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:35:\"Regatas &times; 6, Calça &times; 3\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:10:\"wc_notices\";N;s:8:\"customer\";s:691:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1562011480),
(7, 'a713fcbe3d8ba5d019c44ffee9bb1b25', 'a:13:{s:4:\"cart\";s:1298:\"a:3:{s:32:\"66f041e16a60928b05a7e228a89c3799\";a:11:{s:3:\"key\";s:32:\"66f041e16a60928b05a7e228a89c3799\";s:10:\"product_id\";i:58;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:12.5;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:12.5;s:8:\"line_tax\";i:0;}s:32:\"b5e7e62a81dfe0d039321531f2c4d347\";a:11:{s:3:\"key\";s:32:\"b5e7e62a81dfe0d039321531f2c4d347\";s:10:\"product_id\";i:109;s:12:\"variation_id\";i:116;s:9:\"variation\";a:2:{s:13:\"attribute_cor\";s:7:\"Amarelo\";s:17:\"attribute_tamanho\";s:1:\"P\";}s:8:\"quantity\";i:3;s:9:\"data_hash\";s:32:\"6aeed39936bcc132ec3f32bc69967952\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:369;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:369;s:8:\"line_tax\";i:0;}s:32:\"c9e1074f5b3f9fc8ea15d152add07294\";a:11:{s:3:\"key\";s:32:\"c9e1074f5b3f9fc8ea15d152add07294\";s:10:\"product_id\";i:104;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:250;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:250;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:409:\"a:15:{s:8:\"subtotal\";s:6:\"631.50\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"631.50\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"648.50\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:2041:\"a:5:{s:32:\"54229abfcfa5649e7003b83dd4755294\";a:11:{s:3:\"key\";s:32:\"54229abfcfa5649e7003b83dd4755294\";s:10:\"product_id\";i:91;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:3;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:102;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:102;s:8:\"line_tax\";i:0;}s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";a:11:{s:3:\"key\";s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";s:10:\"product_id\";i:65;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:8;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1440;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1440;s:8:\"line_tax\";i:0;}s:32:\"a97da629b098b75c294dffdc3e463904\";a:11:{s:3:\"key\";s:32:\"a97da629b098b75c294dffdc3e463904\";s:10:\"product_id\";i:107;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:950;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:950;s:8:\"line_tax\";i:0;}s:32:\"66f041e16a60928b05a7e228a89c3799\";a:11:{s:3:\"key\";s:32:\"66f041e16a60928b05a7e228a89c3799\";s:10:\"product_id\";i:58;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:12.5;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:12.5;s:8:\"line_tax\";i:0;}s:32:\"9f61408e3afb633e50cdf1b20de6f466\";a:11:{s:3:\"key\";s:32:\"9f61408e3afb633e50cdf1b20de6f466\";s:10:\"product_id\";i:56;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:242;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:242;s:8:\"line_tax\";i:0;}}\";s:22:\"shipping_for_package_0\";s:431:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_2124b2e094de5812fc60f46678e9545d\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:74:\"Produto 5 &times; 1, Produto D - Amarelo, P &times; 3, Produto B &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:8:\"customer\";s:691:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;s:21:\"chosen_payment_method\";s:0:\"\";}', 1562082111),
(8, '7bd09a1784ec217b5f47a00b5f1b91e2', 'a:13:{s:4:\"cart\";s:413:\"a:1:{s:32:\"2723d092b63885e0d7c260cc007e8b9d\";a:11:{s:3:\"key\";s:32:\"2723d092b63885e0d7c260cc007e8b9d\";s:10:\"product_id\";i:109;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:123;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:123;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:409:\"a:15:{s:8:\"subtotal\";s:6:\"123.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"123.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"140.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:22:\"shipping_for_package_0\";s:376:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_bba4a31836fe51407ce102a5e7eb98d1\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:19:\"Produto D &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:8:\"customer\";s:691:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:21:\"chosen_payment_method\";s:0:\"\";s:10:\"wc_notices\";N;}', 1562092933),
(10, '2', 'a:1:{s:8:\"customer\";s:727:\"a:26:{s:2:\"id\";s:1:\"2\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:35:\"agenciahcdesenvolvimentos@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";}', 1562092933),
(11, '4b96e300bcb1cbfc2bad1ab123b54204', 'a:13:{s:4:\"cart\";s:816:\"a:2:{s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";a:11:{s:3:\"key\";s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";s:10:\"product_id\";i:97;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:300;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:300;s:8:\"line_tax\";i:0;}s:32:\"54229abfcfa5649e7003b83dd4755294\";a:11:{s:3:\"key\";s:32:\"54229abfcfa5649e7003b83dd4755294\";s:10:\"product_id\";i:91;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:68;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:68;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:409:\"a:15:{s:8:\"subtotal\";s:6:\"368.00\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:6:\"368.00\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:6:\"385.00\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:2441:\"a:6:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:120;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:120;s:8:\"line_tax\";i:0;}s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";a:11:{s:3:\"key\";s:32:\"fc490ca45c00b1249bbe3554a4fdf6fb\";s:10:\"product_id\";i:65;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:180;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:180;s:8:\"line_tax\";i:0;}s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";a:11:{s:3:\"key\";s:32:\"e2ef524fbf3d9fe611d5a8e90fefdc9c\";s:10:\"product_id\";i:97;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:2;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:600;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:600;s:8:\"line_tax\";i:0;}s:32:\"9f61408e3afb633e50cdf1b20de6f466\";a:11:{s:3:\"key\";s:32:\"9f61408e3afb633e50cdf1b20de6f466\";s:10:\"product_id\";i:56;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:242;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:242;s:8:\"line_tax\";i:0;}s:32:\"a97da629b098b75c294dffdc3e463904\";a:11:{s:3:\"key\";s:32:\"a97da629b098b75c294dffdc3e463904\";s:10:\"product_id\";i:107;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:950;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:950;s:8:\"line_tax\";i:0;}s:32:\"54229abfcfa5649e7003b83dd4755294\";a:11:{s:3:\"key\";s:32:\"54229abfcfa5649e7003b83dd4755294\";s:10:\"product_id\";i:91;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:34;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:34;s:8:\"line_tax\";i:0;}}\";s:22:\"shipping_for_package_0\";s:394:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_5af59ba920c24aa112d3293c2960ecf2\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:37:\"Produto A &times; 1, Calça &times; 2\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";s:8:\"customer\";s:691:\"a:26:{s:2:\"id\";s:1:\"0\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:0:\"\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:10:\"wc_notices\";N;s:21:\"chosen_payment_method\";s:0:\"\";}', 1562098618);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zones`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zones`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zones`
--

INSERT INTO `cp_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'Brazil', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zone_locations`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zone_locations`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zone_locations`
--

INSERT INTO `cp_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'BR', 'country');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zone_methods`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zone_methods`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zone_methods`
--

INSERT INTO `cp_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'flat_rate', 1, 1),
(0, 2, 'flat_rate', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_tax_rates`
--

DROP TABLE IF EXISTS `cp_woocommerce_tax_rates`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`),
  KEY `tax_rate_state` (`tax_rate_state`(2)),
  KEY `tax_rate_class` (`tax_rate_class`(10)),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_tax_rate_locations`
--

DROP TABLE IF EXISTS `cp_woocommerce_tax_rate_locations`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woof_query_cache`
--

DROP TABLE IF EXISTS `cp_woof_query_cache`;
CREATE TABLE IF NOT EXISTS `cp_woof_query_cache` (
  `mkey` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `mvalue` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  KEY `mkey` (`mkey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
