-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3306
-- Generation Time: 05-Jul-2019 às 16:44
-- Versão do servidor: 5.7.23
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `projetos_centurysports_loja`
--

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_commentmeta`
--

DROP TABLE IF EXISTS `cp_commentmeta`;
CREATE TABLE IF NOT EXISTS `cp_commentmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `comment_id` (`comment_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_comments`
--

DROP TABLE IF EXISTS `cp_comments`;
CREATE TABLE IF NOT EXISTS `cp_comments` (
  `comment_ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `comment_post_ID` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `comment_author` tinytext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_author_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_url` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_author_IP` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `comment_content` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `comment_karma` int(11) NOT NULL DEFAULT '0',
  `comment_approved` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '1',
  `comment_agent` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`comment_ID`),
  KEY `comment_post_ID` (`comment_post_ID`),
  KEY `comment_approved_date_gmt` (`comment_approved`,`comment_date_gmt`),
  KEY `comment_date_gmt` (`comment_date_gmt`),
  KEY `comment_parent` (`comment_parent`),
  KEY `comment_author_email` (`comment_author_email`(10)),
  KEY `woo_idx_comment_type` (`comment_type`)
) ENGINE=MyISAM AUTO_INCREMENT=87 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_comments`
--

INSERT INTO `cp_comments` (`comment_ID`, `comment_post_ID`, `comment_author`, `comment_author_email`, `comment_author_url`, `comment_author_IP`, `comment_date`, `comment_date_gmt`, `comment_content`, `comment_karma`, `comment_approved`, `comment_agent`, `comment_type`, `comment_parent`, `user_id`) VALUES
(1, 1, 'Um comentarista do WordPress', 'wapuu@wordpress.example', 'https://wordpress.org/', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', 'Olá, isso é um comentário.\nPara começar a moderar, editar e excluir comentários, visite a tela de Comentários no painel.\nAvatares de comentaristas vêm a partir do <a href=\"https://gravatar.com\">Gravatar</a>.', 0, '1', '', '', 0, 0),
(8, 14, 'ActionScheduler', '', '', '', '2019-05-30 19:03:32', '2019-05-30 22:03:32', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(9, 14, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(10, 14, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(11, 15, 'ActionScheduler', '', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(12, 15, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(13, 15, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(14, 16, 'ActionScheduler', '', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(15, 16, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(16, 16, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(17, 17, 'ActionScheduler', '', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(18, 17, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(19, 17, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(20, 24, 'ActionScheduler', '', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(21, 24, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(22, 24, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(23, 31, 'ActionScheduler', '', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(24, 31, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(25, 31, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(26, 46, 'ActionScheduler', '', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(27, 46, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(28, 46, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(29, 49, 'ActionScheduler', '', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(30, 49, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(31, 49, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(32, 59, 'ActionScheduler', '', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(33, 59, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(34, 59, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(35, 60, 'ActionScheduler', '', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(36, 60, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(37, 60, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(38, 61, 'ActionScheduler', '', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(39, 61, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(40, 61, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(41, 62, 'ActionScheduler', '', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(42, 62, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(43, 62, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(44, 63, 'ActionScheduler', '', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(45, 63, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(46, 63, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(47, 64, 'ActionScheduler', '', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(48, 64, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(49, 64, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(50, 68, 'ActionScheduler', '', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(51, 68, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(52, 68, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(53, 69, 'ActionScheduler', '', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(54, 69, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(55, 69, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(56, 83, 'ActionScheduler', '', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(57, 83, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(58, 83, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(59, 84, 'ActionScheduler', '', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(60, 84, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(61, 84, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(62, 86, 'ActionScheduler', '', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(63, 86, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(64, 86, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(65, 93, 'ActionScheduler', '', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(66, 93, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(67, 93, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(68, 94, 'ActionScheduler', '', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(69, 94, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(70, 94, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(71, 95, 'ActionScheduler', '', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(72, 95, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(73, 95, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(74, 96, 'ActionScheduler', '', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(75, 96, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(76, 96, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(77, 110, 'ActionScheduler', '', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(78, 110, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(79, 110, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(80, 111, 'ActionScheduler', '', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(81, 111, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(82, 111, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(83, 112, 'ActionScheduler', '', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(84, 112, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action started', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(85, 112, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action complete', 0, '1', 'ActionScheduler', 'action_log', 0, 0),
(86, 113, 'ActionScheduler', '', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', 'action created', 0, '1', 'ActionScheduler', 'action_log', 0, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_links`
--

DROP TABLE IF EXISTS `cp_links`;
CREATE TABLE IF NOT EXISTS `cp_links` (
  `link_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `link_url` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_image` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_target` varchar(25) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_description` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_visible` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'Y',
  `link_owner` bigint(20) UNSIGNED NOT NULL DEFAULT '1',
  `link_rating` int(11) NOT NULL DEFAULT '0',
  `link_updated` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `link_rel` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `link_notes` mediumtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `link_rss` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`link_id`),
  KEY `link_visible` (`link_visible`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_options`
--

DROP TABLE IF EXISTS `cp_options`;
CREATE TABLE IF NOT EXISTS `cp_options` (
  `option_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `option_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `option_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `autoload` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'yes',
  PRIMARY KEY (`option_id`),
  UNIQUE KEY `option_name` (`option_name`)
) ENGINE=MyISAM AUTO_INCREMENT=2897 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_options`
--

INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1, 'siteurl', 'http://centurysports.hcdesenvolvimentos.com', 'yes'),
(2, 'home', 'http://centurysports.hcdesenvolvimentos.com', 'yes'),
(3, 'blogname', 'Century Sports', 'yes'),
(4, 'blogdescription', 'Só mais um site WordPress', 'yes'),
(5, 'users_can_register', '0', 'yes'),
(6, 'admin_email', 'devhcdesenvolvimentos@gmail.com', 'yes'),
(7, 'start_of_week', '0', 'yes'),
(8, 'use_balanceTags', '0', 'yes'),
(9, 'use_smilies', '1', 'yes'),
(10, 'require_name_email', '1', 'yes'),
(11, 'comments_notify', '1', 'yes'),
(12, 'posts_per_rss', '5', 'yes'),
(13, 'rss_use_excerpt', '0', 'yes'),
(14, 'mailserver_url', 'mail.example.com', 'yes'),
(15, 'mailserver_login', 'login@example.com', 'yes'),
(16, 'mailserver_pass', 'password', 'yes'),
(17, 'mailserver_port', '110', 'yes'),
(18, 'default_category', '1', 'yes'),
(19, 'default_comment_status', 'open', 'yes'),
(20, 'default_ping_status', 'open', 'yes'),
(21, 'default_pingback_flag', '0', 'yes'),
(22, 'posts_per_page', '5', 'yes'),
(23, 'date_format', 'j \\d\\e F \\d\\e Y', 'yes'),
(24, 'time_format', 'H:i', 'yes'),
(25, 'links_updated_date_format', 'j \\d\\e F \\d\\e Y, H:i', 'yes'),
(26, 'comment_moderation', '0', 'yes'),
(27, 'moderation_notify', '1', 'yes'),
(28, 'permalink_structure', '/%year%/%monthnum%/%day%/%postname%/', 'yes'),
(29, 'rewrite_rules', 'a:209:{s:24:\"^wc-auth/v([1]{1})/(.*)?\";s:63:\"index.php?wc-auth-version=$matches[1]&wc-auth-route=$matches[2]\";s:22:\"^wc-api/v([1-3]{1})/?$\";s:51:\"index.php?wc-api-version=$matches[1]&wc-api-route=/\";s:24:\"^wc-api/v([1-3]{1})(.*)?\";s:61:\"index.php?wc-api-version=$matches[1]&wc-api-route=$matches[2]\";s:7:\"loja/?$\";s:27:\"index.php?post_type=product\";s:37:\"loja/feed/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:32:\"loja/(feed|rdf|rss|rss2|atom)/?$\";s:44:\"index.php?post_type=product&feed=$matches[1]\";s:24:\"loja/page/([0-9]{1,})/?$\";s:45:\"index.php?post_type=product&paged=$matches[1]\";s:11:\"^wp-json/?$\";s:22:\"index.php?rest_route=/\";s:14:\"^wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:21:\"^index.php/wp-json/?$\";s:22:\"index.php?rest_route=/\";s:24:\"^index.php/wp-json/(.*)?\";s:33:\"index.php?rest_route=/$matches[1]\";s:11:\"destaque/?$\";s:28:\"index.php?post_type=destaque\";s:41:\"destaque/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:36:\"destaque/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=destaque&feed=$matches[1]\";s:28:\"destaque/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=destaque&paged=$matches[1]\";s:18:\"banner-promocao/?$\";s:28:\"index.php?post_type=promocao\";s:48:\"banner-promocao/feed/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=promocao&feed=$matches[1]\";s:43:\"banner-promocao/(feed|rdf|rss|rss2|atom)/?$\";s:45:\"index.php?post_type=promocao&feed=$matches[1]\";s:35:\"banner-promocao/page/([0-9]{1,})/?$\";s:46:\"index.php?post_type=promocao&paged=$matches[1]\";s:47:\"category/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:42:\"category/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:52:\"index.php?category_name=$matches[1]&feed=$matches[2]\";s:23:\"category/(.+?)/embed/?$\";s:46:\"index.php?category_name=$matches[1]&embed=true\";s:35:\"category/(.+?)/page/?([0-9]{1,})/?$\";s:53:\"index.php?category_name=$matches[1]&paged=$matches[2]\";s:32:\"category/(.+?)/wc-api(/(.*))?/?$\";s:54:\"index.php?category_name=$matches[1]&wc-api=$matches[3]\";s:17:\"category/(.+?)/?$\";s:35:\"index.php?category_name=$matches[1]\";s:44:\"tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:39:\"tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?tag=$matches[1]&feed=$matches[2]\";s:20:\"tag/([^/]+)/embed/?$\";s:36:\"index.php?tag=$matches[1]&embed=true\";s:32:\"tag/([^/]+)/page/?([0-9]{1,})/?$\";s:43:\"index.php?tag=$matches[1]&paged=$matches[2]\";s:29:\"tag/([^/]+)/wc-api(/(.*))?/?$\";s:44:\"index.php?tag=$matches[1]&wc-api=$matches[3]\";s:14:\"tag/([^/]+)/?$\";s:25:\"index.php?tag=$matches[1]\";s:45:\"type/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:40:\"type/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?post_format=$matches[1]&feed=$matches[2]\";s:21:\"type/([^/]+)/embed/?$\";s:44:\"index.php?post_format=$matches[1]&embed=true\";s:33:\"type/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?post_format=$matches[1]&paged=$matches[2]\";s:15:\"type/([^/]+)/?$\";s:33:\"index.php?post_format=$matches[1]\";s:48:\"categoria/(.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:43:\"categoria/(.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_cat=$matches[1]&feed=$matches[2]\";s:24:\"categoria/(.+?)/embed/?$\";s:44:\"index.php?product_cat=$matches[1]&embed=true\";s:36:\"categoria/(.+?)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_cat=$matches[1]&paged=$matches[2]\";s:18:\"categoria/(.+?)/?$\";s:33:\"index.php?product_cat=$matches[1]\";s:52:\"product-tag/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:47:\"product-tag/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?product_tag=$matches[1]&feed=$matches[2]\";s:28:\"product-tag/([^/]+)/embed/?$\";s:44:\"index.php?product_tag=$matches[1]&embed=true\";s:40:\"product-tag/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?product_tag=$matches[1]&paged=$matches[2]\";s:22:\"product-tag/([^/]+)/?$\";s:33:\"index.php?product_tag=$matches[1]\";s:35:\"produto/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:45:\"produto/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:65:\"produto/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:60:\"produto/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:41:\"produto/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:24:\"produto/([^/]+)/embed/?$\";s:40:\"index.php?product=$matches[1]&embed=true\";s:28:\"produto/([^/]+)/trackback/?$\";s:34:\"index.php?product=$matches[1]&tb=1\";s:48:\"produto/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:43:\"produto/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:46:\"index.php?product=$matches[1]&feed=$matches[2]\";s:36:\"produto/([^/]+)/page/?([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&paged=$matches[2]\";s:43:\"produto/([^/]+)/comment-page-([0-9]{1,})/?$\";s:47:\"index.php?product=$matches[1]&cpage=$matches[2]\";s:33:\"produto/([^/]+)/wc-api(/(.*))?/?$\";s:48:\"index.php?product=$matches[1]&wc-api=$matches[3]\";s:39:\"produto/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:50:\"produto/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:32:\"produto/([^/]+)(?:/([0-9]+))?/?$\";s:46:\"index.php?product=$matches[1]&page=$matches[2]\";s:24:\"produto/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:34:\"produto/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:54:\"produto/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:49:\"produto/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:30:\"produto/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:36:\"destaque/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:46:\"destaque/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:66:\"destaque/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:61:\"destaque/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:42:\"destaque/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:25:\"destaque/([^/]+)/embed/?$\";s:41:\"index.php?destaque=$matches[1]&embed=true\";s:29:\"destaque/([^/]+)/trackback/?$\";s:35:\"index.php?destaque=$matches[1]&tb=1\";s:49:\"destaque/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:44:\"destaque/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?destaque=$matches[1]&feed=$matches[2]\";s:37:\"destaque/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&paged=$matches[2]\";s:44:\"destaque/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?destaque=$matches[1]&cpage=$matches[2]\";s:34:\"destaque/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?destaque=$matches[1]&wc-api=$matches[3]\";s:40:\"destaque/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:51:\"destaque/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:33:\"destaque/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?destaque=$matches[1]&page=$matches[2]\";s:25:\"destaque/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:35:\"destaque/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:55:\"destaque/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:50:\"destaque/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:31:\"destaque/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:43:\"banner-promocao/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:53:\"banner-promocao/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:73:\"banner-promocao/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"banner-promocao/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:68:\"banner-promocao/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:49:\"banner-promocao/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:32:\"banner-promocao/([^/]+)/embed/?$\";s:41:\"index.php?promocao=$matches[1]&embed=true\";s:36:\"banner-promocao/([^/]+)/trackback/?$\";s:35:\"index.php?promocao=$matches[1]&tb=1\";s:56:\"banner-promocao/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?promocao=$matches[1]&feed=$matches[2]\";s:51:\"banner-promocao/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?promocao=$matches[1]&feed=$matches[2]\";s:44:\"banner-promocao/([^/]+)/page/?([0-9]{1,})/?$\";s:48:\"index.php?promocao=$matches[1]&paged=$matches[2]\";s:51:\"banner-promocao/([^/]+)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?promocao=$matches[1]&cpage=$matches[2]\";s:41:\"banner-promocao/([^/]+)/wc-api(/(.*))?/?$\";s:49:\"index.php?promocao=$matches[1]&wc-api=$matches[3]\";s:47:\"banner-promocao/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:58:\"banner-promocao/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:40:\"banner-promocao/([^/]+)(?:/([0-9]+))?/?$\";s:47:\"index.php?promocao=$matches[1]&page=$matches[2]\";s:32:\"banner-promocao/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:42:\"banner-promocao/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:62:\"banner-promocao/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"banner-promocao/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:57:\"banner-promocao/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:38:\"banner-promocao/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:48:\".*wp-(atom|rdf|rss|rss2|feed|commentsrss2)\\.php$\";s:18:\"index.php?feed=old\";s:20:\".*wp-app\\.php(/.*)?$\";s:19:\"index.php?error=403\";s:18:\".*wp-register.php$\";s:23:\"index.php?register=true\";s:32:\"feed/(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:27:\"(feed|rdf|rss|rss2|atom)/?$\";s:27:\"index.php?&feed=$matches[1]\";s:8:\"embed/?$\";s:21:\"index.php?&embed=true\";s:20:\"page/?([0-9]{1,})/?$\";s:28:\"index.php?&paged=$matches[1]\";s:27:\"comment-page-([0-9]{1,})/?$\";s:39:\"index.php?&page_id=43&cpage=$matches[1]\";s:17:\"wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:41:\"comments/feed/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:36:\"comments/(feed|rdf|rss|rss2|atom)/?$\";s:42:\"index.php?&feed=$matches[1]&withcomments=1\";s:17:\"comments/embed/?$\";s:21:\"index.php?&embed=true\";s:26:\"comments/wc-api(/(.*))?/?$\";s:29:\"index.php?&wc-api=$matches[2]\";s:44:\"search/(.+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:39:\"search/(.+)/(feed|rdf|rss|rss2|atom)/?$\";s:40:\"index.php?s=$matches[1]&feed=$matches[2]\";s:20:\"search/(.+)/embed/?$\";s:34:\"index.php?s=$matches[1]&embed=true\";s:32:\"search/(.+)/page/?([0-9]{1,})/?$\";s:41:\"index.php?s=$matches[1]&paged=$matches[2]\";s:29:\"search/(.+)/wc-api(/(.*))?/?$\";s:42:\"index.php?s=$matches[1]&wc-api=$matches[3]\";s:14:\"search/(.+)/?$\";s:23:\"index.php?s=$matches[1]\";s:47:\"author/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:42:\"author/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:50:\"index.php?author_name=$matches[1]&feed=$matches[2]\";s:23:\"author/([^/]+)/embed/?$\";s:44:\"index.php?author_name=$matches[1]&embed=true\";s:35:\"author/([^/]+)/page/?([0-9]{1,})/?$\";s:51:\"index.php?author_name=$matches[1]&paged=$matches[2]\";s:32:\"author/([^/]+)/wc-api(/(.*))?/?$\";s:52:\"index.php?author_name=$matches[1]&wc-api=$matches[3]\";s:17:\"author/([^/]+)/?$\";s:33:\"index.php?author_name=$matches[1]\";s:69:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:80:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&feed=$matches[4]\";s:45:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/embed/?$\";s:74:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&paged=$matches[4]\";s:54:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:82:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&wc-api=$matches[5]\";s:39:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/?$\";s:63:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]\";s:56:\"([0-9]{4})/([0-9]{1,2})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:51:\"([0-9]{4})/([0-9]{1,2})/(feed|rdf|rss|rss2|atom)/?$\";s:64:\"index.php?year=$matches[1]&monthnum=$matches[2]&feed=$matches[3]\";s:32:\"([0-9]{4})/([0-9]{1,2})/embed/?$\";s:58:\"index.php?year=$matches[1]&monthnum=$matches[2]&embed=true\";s:44:\"([0-9]{4})/([0-9]{1,2})/page/?([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&paged=$matches[3]\";s:41:\"([0-9]{4})/([0-9]{1,2})/wc-api(/(.*))?/?$\";s:66:\"index.php?year=$matches[1]&monthnum=$matches[2]&wc-api=$matches[4]\";s:26:\"([0-9]{4})/([0-9]{1,2})/?$\";s:47:\"index.php?year=$matches[1]&monthnum=$matches[2]\";s:43:\"([0-9]{4})/feed/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:38:\"([0-9]{4})/(feed|rdf|rss|rss2|atom)/?$\";s:43:\"index.php?year=$matches[1]&feed=$matches[2]\";s:19:\"([0-9]{4})/embed/?$\";s:37:\"index.php?year=$matches[1]&embed=true\";s:31:\"([0-9]{4})/page/?([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&paged=$matches[2]\";s:28:\"([0-9]{4})/wc-api(/(.*))?/?$\";s:45:\"index.php?year=$matches[1]&wc-api=$matches[3]\";s:13:\"([0-9]{4})/?$\";s:26:\"index.php?year=$matches[1]\";s:58:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:68:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:88:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:83:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:64:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:53:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/embed/?$\";s:91:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&embed=true\";s:57:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/trackback/?$\";s:85:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&tb=1\";s:77:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&feed=$matches[5]\";s:65:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/page/?([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&paged=$matches[5]\";s:72:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/comment-page-([0-9]{1,})/?$\";s:98:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&cpage=$matches[5]\";s:62:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)/wc-api(/(.*))?/?$\";s:99:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&wc-api=$matches[6]\";s:62:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:73:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:61:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/([^/]+)(?:/([0-9]+))?/?$\";s:97:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&name=$matches[4]&page=$matches[5]\";s:47:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:57:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:77:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:72:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:53:\"[0-9]{4}/[0-9]{1,2}/[0-9]{1,2}/[^/]+/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:64:\"([0-9]{4})/([0-9]{1,2})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:81:\"index.php?year=$matches[1]&monthnum=$matches[2]&day=$matches[3]&cpage=$matches[4]\";s:51:\"([0-9]{4})/([0-9]{1,2})/comment-page-([0-9]{1,})/?$\";s:65:\"index.php?year=$matches[1]&monthnum=$matches[2]&cpage=$matches[3]\";s:38:\"([0-9]{4})/comment-page-([0-9]{1,})/?$\";s:44:\"index.php?year=$matches[1]&cpage=$matches[2]\";s:27:\".?.+?/attachment/([^/]+)/?$\";s:32:\"index.php?attachment=$matches[1]\";s:37:\".?.+?/attachment/([^/]+)/trackback/?$\";s:37:\"index.php?attachment=$matches[1]&tb=1\";s:57:\".?.+?/attachment/([^/]+)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/(feed|rdf|rss|rss2|atom)/?$\";s:49:\"index.php?attachment=$matches[1]&feed=$matches[2]\";s:52:\".?.+?/attachment/([^/]+)/comment-page-([0-9]{1,})/?$\";s:50:\"index.php?attachment=$matches[1]&cpage=$matches[2]\";s:33:\".?.+?/attachment/([^/]+)/embed/?$\";s:43:\"index.php?attachment=$matches[1]&embed=true\";s:16:\"(.?.+?)/embed/?$\";s:41:\"index.php?pagename=$matches[1]&embed=true\";s:20:\"(.?.+?)/trackback/?$\";s:35:\"index.php?pagename=$matches[1]&tb=1\";s:40:\"(.?.+?)/feed/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:35:\"(.?.+?)/(feed|rdf|rss|rss2|atom)/?$\";s:47:\"index.php?pagename=$matches[1]&feed=$matches[2]\";s:28:\"(.?.+?)/page/?([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&paged=$matches[2]\";s:35:\"(.?.+?)/comment-page-([0-9]{1,})/?$\";s:48:\"index.php?pagename=$matches[1]&cpage=$matches[2]\";s:25:\"(.?.+?)/wc-api(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&wc-api=$matches[3]\";s:28:\"(.?.+?)/order-pay(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&order-pay=$matches[3]\";s:33:\"(.?.+?)/order-received(/(.*))?/?$\";s:57:\"index.php?pagename=$matches[1]&order-received=$matches[3]\";s:25:\"(.?.+?)/orders(/(.*))?/?$\";s:49:\"index.php?pagename=$matches[1]&orders=$matches[3]\";s:29:\"(.?.+?)/view-order(/(.*))?/?$\";s:53:\"index.php?pagename=$matches[1]&view-order=$matches[3]\";s:28:\"(.?.+?)/downloads(/(.*))?/?$\";s:52:\"index.php?pagename=$matches[1]&downloads=$matches[3]\";s:31:\"(.?.+?)/edit-account(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-account=$matches[3]\";s:31:\"(.?.+?)/edit-address(/(.*))?/?$\";s:55:\"index.php?pagename=$matches[1]&edit-address=$matches[3]\";s:34:\"(.?.+?)/payment-methods(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&payment-methods=$matches[3]\";s:32:\"(.?.+?)/lost-password(/(.*))?/?$\";s:56:\"index.php?pagename=$matches[1]&lost-password=$matches[3]\";s:34:\"(.?.+?)/customer-logout(/(.*))?/?$\";s:58:\"index.php?pagename=$matches[1]&customer-logout=$matches[3]\";s:37:\"(.?.+?)/add-payment-method(/(.*))?/?$\";s:61:\"index.php?pagename=$matches[1]&add-payment-method=$matches[3]\";s:40:\"(.?.+?)/delete-payment-method(/(.*))?/?$\";s:64:\"index.php?pagename=$matches[1]&delete-payment-method=$matches[3]\";s:45:\"(.?.+?)/set-default-payment-method(/(.*))?/?$\";s:69:\"index.php?pagename=$matches[1]&set-default-payment-method=$matches[3]\";s:31:\".?.+?/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:42:\".?.+?/attachment/([^/]+)/wc-api(/(.*))?/?$\";s:51:\"index.php?attachment=$matches[1]&wc-api=$matches[3]\";s:24:\"(.?.+?)(?:/([0-9]+))?/?$\";s:47:\"index.php?pagename=$matches[1]&page=$matches[2]\";}', 'yes'),
(30, 'hack_file', '0', 'yes'),
(31, 'blog_charset', 'UTF-8', 'yes'),
(32, 'moderation_keys', '', 'no'),
(33, 'active_plugins', 'a:8:{i:0;s:35:\"redux-framework/redux-framework.php\";i:1;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:2;s:41:\"base-centurySports/base-centurySports.php\";i:3;s:39:\"categories-images/categories-images.php\";i:4;s:36:\"contact-form-7/wp-contact-form-7.php\";i:5;s:21:\"meta-box/meta-box.php\";i:6;s:37:\"woocommerce-products-filter/index.php\";i:7;s:27:\"woocommerce/woocommerce.php\";}', 'yes'),
(34, 'category_base', '', 'yes'),
(35, 'ping_sites', 'http://rpc.pingomatic.com/', 'yes'),
(36, 'comment_max_links', '2', 'yes'),
(37, 'gmt_offset', '0', 'yes'),
(38, 'default_email_category', '1', 'yes'),
(39, 'recently_edited', '', 'no'),
(40, 'template', 'centurysports_loja', 'yes'),
(41, 'stylesheet', 'centurysports_loja', 'yes'),
(42, 'comment_whitelist', '1', 'yes'),
(43, 'blacklist_keys', '', 'no'),
(44, 'comment_registration', '0', 'yes'),
(45, 'html_type', 'text/html', 'yes'),
(46, 'use_trackback', '0', 'yes'),
(47, 'default_role', 'subscriber', 'yes'),
(48, 'db_version', '44719', 'yes'),
(49, 'uploads_use_yearmonth_folders', '1', 'yes'),
(50, 'upload_path', '', 'yes'),
(51, 'blog_public', '0', 'yes'),
(52, 'default_link_category', '2', 'yes'),
(53, 'show_on_front', 'page', 'yes'),
(54, 'tag_base', '', 'yes'),
(55, 'show_avatars', '1', 'yes'),
(56, 'avatar_rating', 'G', 'yes'),
(57, 'upload_url_path', '', 'yes'),
(58, 'thumbnail_size_w', '150', 'yes'),
(59, 'thumbnail_size_h', '150', 'yes'),
(60, 'thumbnail_crop', '1', 'yes'),
(61, 'medium_size_w', '300', 'yes'),
(62, 'medium_size_h', '300', 'yes'),
(63, 'avatar_default', 'mystery', 'yes'),
(64, 'large_size_w', '1024', 'yes'),
(65, 'large_size_h', '1024', 'yes'),
(66, 'image_default_link_type', 'file', 'yes'),
(67, 'image_default_size', '', 'yes'),
(68, 'image_default_align', '', 'yes'),
(69, 'close_comments_for_old_posts', '0', 'yes'),
(70, 'close_comments_days_old', '14', 'yes'),
(71, 'thread_comments', '1', 'yes'),
(72, 'thread_comments_depth', '5', 'yes'),
(73, 'page_comments', '0', 'yes'),
(74, 'comments_per_page', '50', 'yes'),
(75, 'default_comments_page', 'newest', 'yes'),
(76, 'comment_order', 'asc', 'yes'),
(77, 'sticky_posts', 'a:0:{}', 'yes'),
(78, 'widget_categories', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(79, 'widget_text', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(80, 'widget_rss', 'a:2:{i:1;a:0:{}s:12:\"_multiwidget\";i:1;}', 'yes'),
(81, 'uninstall_plugins', 'a:0:{}', 'no'),
(82, 'timezone_string', 'America/Sao_Paulo', 'yes'),
(83, 'page_for_posts', '0', 'yes'),
(84, 'page_on_front', '43', 'yes'),
(85, 'default_post_format', '0', 'yes'),
(86, 'link_manager_enabled', '0', 'yes'),
(87, 'finished_splitting_shared_terms', '1', 'yes'),
(88, 'site_icon', '0', 'yes'),
(89, 'medium_large_size_w', '768', 'yes'),
(90, 'medium_large_size_h', '0', 'yes'),
(91, 'wp_page_for_privacy_policy', '3', 'yes'),
(92, 'show_comments_cookies_opt_in', '1', 'yes'),
(93, 'initial_db_version', '44719', 'yes'),
(94, 'cp_user_roles', 'a:7:{s:13:\"administrator\";a:2:{s:4:\"name\";s:13:\"Administrator\";s:12:\"capabilities\";a:115:{s:13:\"switch_themes\";b:1;s:11:\"edit_themes\";b:1;s:16:\"activate_plugins\";b:1;s:12:\"edit_plugins\";b:1;s:10:\"edit_users\";b:1;s:10:\"edit_files\";b:1;s:14:\"manage_options\";b:1;s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:6:\"import\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:8:\"level_10\";b:1;s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:12:\"delete_users\";b:1;s:12:\"create_users\";b:1;s:17:\"unfiltered_upload\";b:1;s:14:\"edit_dashboard\";b:1;s:14:\"update_plugins\";b:1;s:14:\"delete_plugins\";b:1;s:15:\"install_plugins\";b:1;s:13:\"update_themes\";b:1;s:14:\"install_themes\";b:1;s:11:\"update_core\";b:1;s:10:\"list_users\";b:1;s:12:\"remove_users\";b:1;s:13:\"promote_users\";b:1;s:18:\"edit_theme_options\";b:1;s:13:\"delete_themes\";b:1;s:6:\"export\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"editor\";a:2:{s:4:\"name\";s:6:\"Editor\";s:12:\"capabilities\";a:35:{s:17:\"moderate_comments\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:12:\"upload_files\";b:1;s:15:\"unfiltered_html\";b:1;s:10:\"edit_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:10:\"edit_pages\";b:1;s:4:\"read\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:17:\"edit_others_pages\";b:1;s:20:\"edit_published_pages\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_pages\";b:1;s:19:\"delete_others_pages\";b:1;s:22:\"delete_published_pages\";b:1;s:12:\"delete_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:22:\"delete_published_posts\";b:1;s:20:\"delete_private_posts\";b:1;s:18:\"edit_private_posts\";b:1;s:18:\"read_private_posts\";b:1;s:20:\"delete_private_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"read_private_pages\";b:1;s:10:\"copy_posts\";b:1;}}s:6:\"author\";a:2:{s:4:\"name\";s:6:\"Author\";s:12:\"capabilities\";a:10:{s:12:\"upload_files\";b:1;s:10:\"edit_posts\";b:1;s:20:\"edit_published_posts\";b:1;s:13:\"publish_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;s:22:\"delete_published_posts\";b:1;}}s:11:\"contributor\";a:2:{s:4:\"name\";s:11:\"Contributor\";s:12:\"capabilities\";a:5:{s:10:\"edit_posts\";b:1;s:4:\"read\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:12:\"delete_posts\";b:1;}}s:10:\"subscriber\";a:2:{s:4:\"name\";s:10:\"Subscriber\";s:12:\"capabilities\";a:2:{s:4:\"read\";b:1;s:7:\"level_0\";b:1;}}s:8:\"customer\";a:2:{s:4:\"name\";s:8:\"Customer\";s:12:\"capabilities\";a:1:{s:4:\"read\";b:1;}}s:12:\"shop_manager\";a:2:{s:4:\"name\";s:12:\"Shop manager\";s:12:\"capabilities\";a:92:{s:7:\"level_9\";b:1;s:7:\"level_8\";b:1;s:7:\"level_7\";b:1;s:7:\"level_6\";b:1;s:7:\"level_5\";b:1;s:7:\"level_4\";b:1;s:7:\"level_3\";b:1;s:7:\"level_2\";b:1;s:7:\"level_1\";b:1;s:7:\"level_0\";b:1;s:4:\"read\";b:1;s:18:\"read_private_pages\";b:1;s:18:\"read_private_posts\";b:1;s:10:\"edit_posts\";b:1;s:10:\"edit_pages\";b:1;s:20:\"edit_published_posts\";b:1;s:20:\"edit_published_pages\";b:1;s:18:\"edit_private_pages\";b:1;s:18:\"edit_private_posts\";b:1;s:17:\"edit_others_posts\";b:1;s:17:\"edit_others_pages\";b:1;s:13:\"publish_posts\";b:1;s:13:\"publish_pages\";b:1;s:12:\"delete_posts\";b:1;s:12:\"delete_pages\";b:1;s:20:\"delete_private_pages\";b:1;s:20:\"delete_private_posts\";b:1;s:22:\"delete_published_pages\";b:1;s:22:\"delete_published_posts\";b:1;s:19:\"delete_others_posts\";b:1;s:19:\"delete_others_pages\";b:1;s:17:\"manage_categories\";b:1;s:12:\"manage_links\";b:1;s:17:\"moderate_comments\";b:1;s:12:\"upload_files\";b:1;s:6:\"export\";b:1;s:6:\"import\";b:1;s:10:\"list_users\";b:1;s:18:\"edit_theme_options\";b:1;s:18:\"manage_woocommerce\";b:1;s:24:\"view_woocommerce_reports\";b:1;s:12:\"edit_product\";b:1;s:12:\"read_product\";b:1;s:14:\"delete_product\";b:1;s:13:\"edit_products\";b:1;s:20:\"edit_others_products\";b:1;s:16:\"publish_products\";b:1;s:21:\"read_private_products\";b:1;s:15:\"delete_products\";b:1;s:23:\"delete_private_products\";b:1;s:25:\"delete_published_products\";b:1;s:22:\"delete_others_products\";b:1;s:21:\"edit_private_products\";b:1;s:23:\"edit_published_products\";b:1;s:20:\"manage_product_terms\";b:1;s:18:\"edit_product_terms\";b:1;s:20:\"delete_product_terms\";b:1;s:20:\"assign_product_terms\";b:1;s:15:\"edit_shop_order\";b:1;s:15:\"read_shop_order\";b:1;s:17:\"delete_shop_order\";b:1;s:16:\"edit_shop_orders\";b:1;s:23:\"edit_others_shop_orders\";b:1;s:19:\"publish_shop_orders\";b:1;s:24:\"read_private_shop_orders\";b:1;s:18:\"delete_shop_orders\";b:1;s:26:\"delete_private_shop_orders\";b:1;s:28:\"delete_published_shop_orders\";b:1;s:25:\"delete_others_shop_orders\";b:1;s:24:\"edit_private_shop_orders\";b:1;s:26:\"edit_published_shop_orders\";b:1;s:23:\"manage_shop_order_terms\";b:1;s:21:\"edit_shop_order_terms\";b:1;s:23:\"delete_shop_order_terms\";b:1;s:23:\"assign_shop_order_terms\";b:1;s:16:\"edit_shop_coupon\";b:1;s:16:\"read_shop_coupon\";b:1;s:18:\"delete_shop_coupon\";b:1;s:17:\"edit_shop_coupons\";b:1;s:24:\"edit_others_shop_coupons\";b:1;s:20:\"publish_shop_coupons\";b:1;s:25:\"read_private_shop_coupons\";b:1;s:19:\"delete_shop_coupons\";b:1;s:27:\"delete_private_shop_coupons\";b:1;s:29:\"delete_published_shop_coupons\";b:1;s:26:\"delete_others_shop_coupons\";b:1;s:25:\"edit_private_shop_coupons\";b:1;s:27:\"edit_published_shop_coupons\";b:1;s:24:\"manage_shop_coupon_terms\";b:1;s:22:\"edit_shop_coupon_terms\";b:1;s:24:\"delete_shop_coupon_terms\";b:1;s:24:\"assign_shop_coupon_terms\";b:1;}}}', 'yes'),
(95, 'fresh_site', '0', 'yes'),
(96, 'WPLANG', 'pt_BR', 'yes'),
(97, 'widget_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(98, 'widget_recent-posts', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(99, 'widget_recent-comments', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(100, 'widget_archives', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(101, 'widget_meta', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(102, 'sidebars_widgets', 'a:3:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:5:{i:0;s:26:\"woocommerce_price_filter-2\";i:1;s:32:\"woocommerce_product_categories-2\";i:2;s:33:\"woocommerce_layered_nav_filters-3\";i:3;s:25:\"woocommerce_layered_nav-3\";i:4;s:25:\"woocommerce_widget_cart-2\";}s:13:\"array_version\";i:3;}', 'yes'),
(103, 'cron', 'a:15:{i:1562345047;a:1:{s:34:\"wp_privacy_delete_old_export_files\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:6:\"hourly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:3600;}}}i:1562345087;a:1:{s:26:\"action_scheduler_run_queue\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:12:\"every_minute\";s:4:\"args\";a:0:{}s:8:\"interval\";i:60;}}}i:1562348586;a:1:{s:32:\"woocommerce_cancel_unpaid_orders\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:2:{s:8:\"schedule\";b:0;s:4:\"args\";a:0:{}}}}i:1562359446;a:1:{s:32:\"recovery_mode_clean_expired_keys\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562359447;a:3:{s:16:\"wp_version_check\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:17:\"wp_update_plugins\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}s:16:\"wp_update_themes\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1562359492;a:2:{s:19:\"wp_scheduled_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}s:25:\"delete_expired_transients\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562359494;a:1:{s:30:\"wp_scheduled_auto_draft_delete\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562360086;a:1:{s:33:\"woocommerce_cleanup_personal_data\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562360096;a:1:{s:30:\"woocommerce_tracker_send_event\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562370886;a:1:{s:24:\"woocommerce_cleanup_logs\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562381686;a:1:{s:28:\"woocommerce_cleanup_sessions\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:10:\"twicedaily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:43200;}}}i:1562382000;a:1:{s:27:\"woocommerce_scheduled_sales\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1562404651;a:1:{s:40:\"fs_data_sync_ajax-search-for-woocommerce\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:5:\"daily\";s:4:\"args\";a:0:{}s:8:\"interval\";i:86400;}}}i:1564876800;a:1:{s:25:\"woocommerce_geoip_updater\";a:1:{s:32:\"40cd750bba9870f18aada2478b24840a\";a:3:{s:8:\"schedule\";s:7:\"monthly\";s:4:\"args\";a:0:{}s:8:\"interval\";i:2635200;}}}s:7:\"version\";i:2;}', 'yes'),
(2228, '_transient_wc_shipping_method_count', 'a:2:{s:7:\"version\";s:10:\"1559249916\";s:5:\"value\";i:2;}', 'no'),
(104, 'widget_pages', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(105, 'widget_calendar', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(106, 'widget_media_audio', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(107, 'widget_media_image', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(108, 'widget_media_gallery', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(109, 'widget_media_video', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(110, 'widget_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(111, 'widget_nav_menu', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(112, 'widget_custom_html', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(114, 'recovery_keys', 'a:0:{}', 'yes'),
(116, 'theme_mods_twentynineteen', 'a:2:{s:18:\"custom_css_post_id\";i:-1;s:16:\"sidebars_widgets\";a:2:{s:4:\"time\";i:1559249547;s:4:\"data\";a:2:{s:19:\"wp_inactive_widgets\";a:0:{}s:9:\"sidebar-1\";a:6:{i:0;s:8:\"search-2\";i:1;s:14:\"recent-posts-2\";i:2;s:17:\"recent-comments-2\";i:3;s:10:\"archives-2\";i:4;s:12:\"categories-2\";i:5;s:6:\"meta-2\";}}}}', 'yes'),
(555, 'duplicate_post_copyformat', '1', 'yes'),
(556, 'duplicate_post_copyauthor', '0', 'yes'),
(557, 'duplicate_post_copypassword', '0', 'yes'),
(558, 'duplicate_post_copyattachments', '0', 'yes'),
(559, 'duplicate_post_copychildren', '0', 'yes'),
(560, 'duplicate_post_copycomments', '0', 'yes'),
(561, 'duplicate_post_copymenuorder', '1', 'yes'),
(562, 'duplicate_post_taxonomies_blacklist', 'a:0:{}', 'yes'),
(563, 'duplicate_post_blacklist', '', 'yes'),
(564, 'duplicate_post_types_enabled', 'a:2:{i:0;s:4:\"post\";i:1;s:4:\"page\";}', 'yes'),
(565, 'duplicate_post_show_row', '1', 'yes'),
(566, 'duplicate_post_show_adminbar', '1', 'yes'),
(567, 'duplicate_post_show_submitbox', '1', 'yes'),
(568, 'duplicate_post_show_bulkactions', '1', 'yes'),
(569, 'duplicate_post_version', '3.2.2', 'yes'),
(570, 'duplicate_post_show_notice', '1', 'no'),
(430, 'woocommerce_tracker_ua', 'a:2:{i:0;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36\";i:1;s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";}', 'yes'),
(327, 'wc_admin_last_orders_milestone', '0', 'yes'),
(323, 'wc_admin_install_timestamp', '1561902281', 'yes'),
(2820, '_site_transient_timeout_theme_roots', '1562335279', 'no'),
(2821, '_site_transient_theme_roots', 'a:1:{s:18:\"centurysports_loja\";s:7:\"/themes\";}', 'no'),
(613, 'fs_active_plugins', 'O:8:\"stdClass\":3:{s:7:\"plugins\";a:1:{s:34:\"ajax-search-for-woocommerce/fs/lib\";O:8:\"stdClass\":4:{s:7:\"version\";s:5:\"2.2.4\";s:4:\"type\";s:6:\"plugin\";s:9:\"timestamp\";i:1561337867;s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:7:\"abspath\";s:36:\"C:\\wamp64\\www\\projetos\\century_loja/\";s:6:\"newest\";O:8:\"stdClass\":5:{s:11:\"plugin_path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:8:\"sdk_path\";s:34:\"ajax-search-for-woocommerce/fs/lib\";s:7:\"version\";s:5:\"2.2.4\";s:13:\"in_activation\";b:0;s:9:\"timestamp\";i:1561337867;}}', 'yes'),
(155, 'woocommerce_default_country', 'BR:PR', 'yes'),
(152, 'woocommerce_store_address', 'R. Saldanha Marinho, 1301 - Centro, - PR', 'yes'),
(153, 'woocommerce_store_address_2', '', 'yes'),
(614, 'fs_debug_mode', '', 'yes'),
(2400, '_site_transient_timeout_browser_bcf1814caa6afe84eeebef28ff236a7f', '1562769332', 'no'),
(2401, '_site_transient_browser_bcf1814caa6afe84eeebef28ff236a7f', 'a:10:{s:4:\"name\";s:6:\"Chrome\";s:7:\"version\";s:13:\"75.0.3770.100\";s:8:\"platform\";s:7:\"Windows\";s:10:\"update_url\";s:29:\"https://www.google.com/chrome\";s:7:\"img_src\";s:43:\"http://s.w.org/images/browsers/chrome.png?1\";s:11:\"img_src_ssl\";s:44:\"https://s.w.org/images/browsers/chrome.png?1\";s:15:\"current_version\";s:2:\"18\";s:7:\"upgrade\";b:0;s:8:\"insecure\";b:0;s:6:\"mobile\";b:0;}', 'no'),
(129, 'can_compress_scripts', '1', 'no'),
(154, 'woocommerce_store_city', 'Guarapuava', 'yes'),
(140, 'current_theme', 'Century Sports Loja', 'yes'),
(141, 'theme_mods_centurysports_loja', 'a:3:{i:0;b:0;s:18:\"nav_menu_locations\";a:1:{s:6:\"menu-1\";i:18;}s:18:\"custom_css_post_id\";i:-1;}', 'yes'),
(142, 'theme_switched', '', 'yes'),
(145, 'recently_activated', 'a:2:{s:39:\"woocommerce-admin/woocommerce-admin.php\";i:1561904612;s:41:\"yith-woocommerce-ajax-navigation/init.php\";i:1561903552;}', 'yes'),
(156, 'woocommerce_store_postcode', '85010-290', 'yes'),
(157, 'woocommerce_allowed_countries', 'all', 'yes'),
(158, 'woocommerce_all_except_countries', '', 'yes'),
(159, 'woocommerce_specific_allowed_countries', '', 'yes'),
(160, 'woocommerce_ship_to_countries', '', 'yes'),
(161, 'woocommerce_specific_ship_to_countries', '', 'yes'),
(162, 'woocommerce_default_customer_address', 'geolocation', 'yes'),
(163, 'woocommerce_calc_taxes', 'no', 'yes'),
(164, 'woocommerce_enable_coupons', 'yes', 'yes'),
(165, 'woocommerce_calc_discounts_sequentially', 'no', 'no'),
(166, 'woocommerce_currency', 'BRL', 'yes'),
(167, 'woocommerce_currency_pos', 'left', 'yes'),
(168, 'woocommerce_price_thousand_sep', '.', 'yes'),
(169, 'woocommerce_price_decimal_sep', ',', 'yes'),
(170, 'woocommerce_price_num_decimals', '2', 'yes'),
(171, 'woocommerce_shop_page_id', '6', 'yes'),
(172, 'woocommerce_cart_redirect_after_add', 'no', 'yes'),
(173, 'woocommerce_enable_ajax_add_to_cart', 'yes', 'yes'),
(174, 'woocommerce_placeholder_image', '5', 'yes'),
(175, 'woocommerce_weight_unit', 'kg', 'yes'),
(176, 'woocommerce_dimension_unit', 'cm', 'yes'),
(177, 'woocommerce_enable_reviews', 'yes', 'yes'),
(178, 'woocommerce_review_rating_verification_label', 'yes', 'no'),
(179, 'woocommerce_review_rating_verification_required', 'no', 'no'),
(180, 'woocommerce_enable_review_rating', 'yes', 'yes'),
(181, 'woocommerce_review_rating_required', 'yes', 'no'),
(182, 'woocommerce_manage_stock', 'yes', 'yes'),
(183, 'woocommerce_hold_stock_minutes', '60', 'no'),
(184, 'woocommerce_notify_low_stock', 'yes', 'no'),
(185, 'woocommerce_notify_no_stock', 'yes', 'no'),
(186, 'woocommerce_stock_email_recipient', 'devhcdesenvolvimentos@gmail.com', 'no'),
(187, 'woocommerce_notify_low_stock_amount', '2', 'no'),
(188, 'woocommerce_notify_no_stock_amount', '0', 'yes'),
(189, 'woocommerce_hide_out_of_stock_items', 'no', 'yes'),
(190, 'woocommerce_stock_format', '', 'yes'),
(191, 'woocommerce_file_download_method', 'force', 'no'),
(192, 'woocommerce_downloads_require_login', 'no', 'no'),
(193, 'woocommerce_downloads_grant_access_after_payment', 'yes', 'no'),
(194, 'woocommerce_prices_include_tax', 'no', 'yes'),
(195, 'woocommerce_tax_based_on', 'shipping', 'yes'),
(196, 'woocommerce_shipping_tax_class', 'inherit', 'yes'),
(197, 'woocommerce_tax_round_at_subtotal', 'no', 'yes'),
(198, 'woocommerce_tax_classes', 'Reduced rate\r\nZero rate', 'yes'),
(199, 'woocommerce_tax_display_shop', 'excl', 'yes'),
(200, 'woocommerce_tax_display_cart', 'excl', 'yes'),
(201, 'woocommerce_price_display_suffix', '', 'yes'),
(202, 'woocommerce_tax_total_display', 'itemized', 'no'),
(203, 'woocommerce_enable_shipping_calc', 'yes', 'no'),
(204, 'woocommerce_shipping_cost_requires_address', 'no', 'yes'),
(205, 'woocommerce_ship_to_destination', 'billing', 'no'),
(206, 'woocommerce_shipping_debug_mode', 'no', 'yes'),
(207, 'woocommerce_enable_guest_checkout', 'no', 'no'),
(208, 'woocommerce_enable_checkout_login_reminder', 'yes', 'no'),
(209, 'woocommerce_enable_signup_and_login_from_checkout', 'yes', 'no'),
(210, 'woocommerce_enable_myaccount_registration', 'yes', 'no'),
(211, 'woocommerce_registration_generate_username', 'yes', 'no'),
(212, 'woocommerce_registration_generate_password', 'yes', 'no'),
(213, 'woocommerce_erasure_request_removes_order_data', 'no', 'no'),
(214, 'woocommerce_erasure_request_removes_download_data', 'no', 'no'),
(215, 'woocommerce_allow_bulk_remove_personal_data', 'no', 'no'),
(216, 'woocommerce_registration_privacy_policy_text', 'Your personal data will be used to support your experience throughout this website, to manage access to your account, and for other purposes described in our [privacy_policy].', 'yes'),
(217, 'woocommerce_checkout_privacy_policy_text', 'Your personal data will be used to process your order, support your experience throughout this website, and for other purposes described in our [privacy_policy].', 'yes'),
(218, 'woocommerce_delete_inactive_accounts', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(219, 'woocommerce_trash_pending_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(220, 'woocommerce_trash_failed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(221, 'woocommerce_trash_cancelled_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:4:\"days\";}', 'no'),
(222, 'woocommerce_anonymize_completed_orders', 'a:2:{s:6:\"number\";s:0:\"\";s:4:\"unit\";s:6:\"months\";}', 'no'),
(223, 'woocommerce_email_from_name', 'Century Sports', 'no'),
(224, 'woocommerce_email_from_address', 'devhcdesenvolvimentos@gmail.com', 'no'),
(225, 'woocommerce_email_header_image', '', 'no'),
(226, 'woocommerce_email_footer_text', '{site_title}<br/>Built with <a href=\"https://woocommerce.com/\">WooCommerce</a>', 'no'),
(227, 'woocommerce_email_base_color', '#96588a', 'no'),
(228, 'woocommerce_email_background_color', '#f7f7f7', 'no'),
(229, 'woocommerce_email_body_background_color', '#ffffff', 'no'),
(230, 'woocommerce_email_text_color', '#3c3c3c', 'no'),
(231, 'woocommerce_cart_page_id', '7', 'yes'),
(232, 'woocommerce_checkout_page_id', '8', 'yes'),
(233, 'woocommerce_myaccount_page_id', '9', 'yes'),
(234, 'woocommerce_terms_page_id', '', 'no'),
(235, 'woocommerce_force_ssl_checkout', 'no', 'yes'),
(236, 'woocommerce_unforce_ssl_checkout', 'no', 'yes'),
(237, 'woocommerce_checkout_pay_endpoint', 'order-pay', 'yes'),
(238, 'woocommerce_checkout_order_received_endpoint', 'order-received', 'yes'),
(239, 'woocommerce_myaccount_add_payment_method_endpoint', 'add-payment-method', 'yes'),
(240, 'woocommerce_myaccount_delete_payment_method_endpoint', 'delete-payment-method', 'yes'),
(241, 'woocommerce_myaccount_set_default_payment_method_endpoint', 'set-default-payment-method', 'yes'),
(242, 'woocommerce_myaccount_orders_endpoint', 'orders', 'yes'),
(243, 'woocommerce_myaccount_view_order_endpoint', 'view-order', 'yes'),
(244, 'woocommerce_myaccount_downloads_endpoint', 'downloads', 'yes'),
(245, 'woocommerce_myaccount_edit_account_endpoint', 'edit-account', 'yes'),
(246, 'woocommerce_myaccount_edit_address_endpoint', 'edit-address', 'yes'),
(247, 'woocommerce_myaccount_payment_methods_endpoint', 'payment-methods', 'yes'),
(248, 'woocommerce_myaccount_lost_password_endpoint', 'lost-password', 'yes'),
(249, 'woocommerce_logout_endpoint', 'customer-logout', 'yes'),
(250, 'woocommerce_api_enabled', 'no', 'yes'),
(251, 'woocommerce_allow_tracking', 'yes', 'no'),
(252, 'woocommerce_show_marketplace_suggestions', 'yes', 'no'),
(253, 'woocommerce_single_image_width', '600', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(254, 'woocommerce_thumbnail_image_width', '300', 'yes'),
(255, 'woocommerce_checkout_highlight_required_fields', 'yes', 'yes'),
(256, 'woocommerce_demo_store', 'no', 'no'),
(257, 'woocommerce_permalinks', 'a:5:{s:12:\"product_base\";s:8:\"/produto\";s:13:\"category_base\";s:9:\"categoria\";s:8:\"tag_base\";s:11:\"product-tag\";s:14:\"attribute_base\";s:0:\"\";s:22:\"use_verbose_page_rules\";b:0;}', 'yes'),
(258, 'current_theme_supports_woocommerce', 'yes', 'yes'),
(259, 'woocommerce_queue_flush_rewrite_rules', 'no', 'yes'),
(1757, '_transient_wc_attribute_taxonomies', 'a:0:{}', 'yes'),
(262, 'default_product_cat', '15', 'yes'),
(265, 'woocommerce_version', '3.6.4', 'yes'),
(266, 'woocommerce_db_version', '3.6.4', 'yes'),
(267, 'woocommerce_admin_notices', 'a:1:{i:1;s:20:\"no_secure_connection\";}', 'yes'),
(268, '_transient_woocommerce_webhook_ids_status_active', 'a:0:{}', 'yes'),
(269, 'widget_woocommerce_widget_cart', 'a:2:{i:2;a:2:{s:5:\"title\";s:8:\"Carrinho\";s:13:\"hide_if_empty\";i:1;}s:12:\"_multiwidget\";i:1;}', 'yes'),
(270, 'widget_woocommerce_layered_nav_filters', 'a:2:{i:3;a:1:{s:5:\"title\";s:14:\"Ativar filtros\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(271, 'widget_woocommerce_layered_nav', 'a:2:{i:3;a:4:{s:5:\"title\";s:11:\"Filtrar por\";s:9:\"attribute\";s:0:\"\";s:12:\"display_type\";s:4:\"list\";s:10:\"query_type\";s:3:\"and\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(272, 'widget_woocommerce_price_filter', 'a:2:{i:2;a:1:{s:5:\"title\";s:18:\"Filtrar por preço\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(273, 'widget_woocommerce_product_categories', 'a:2:{i:2;a:8:{s:5:\"title\";s:0:\"\";s:7:\"orderby\";s:5:\"order\";s:8:\"dropdown\";i:0;s:5:\"count\";i:0;s:12:\"hierarchical\";i:1;s:18:\"show_children_only\";i:0;s:10:\"hide_empty\";i:0;s:9:\"max_depth\";s:0:\"\";}s:12:\"_multiwidget\";i:1;}', 'yes'),
(274, 'widget_woocommerce_product_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(275, 'widget_woocommerce_product_tag_cloud', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(276, 'widget_woocommerce_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(277, 'widget_woocommerce_recently_viewed_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(278, 'widget_woocommerce_top_rated_products', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(279, 'widget_woocommerce_recent_reviews', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(280, 'widget_woocommerce_rating_filter', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(291, 'woocommerce_obw_last_completed_step', 'recommended', 'yes'),
(2370, '_transient_timeout_external_ip_address_::1', '1562587342', 'no'),
(299, 'woocommerce_cheque_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(300, 'woocommerce_bacs_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(294, 'woocommerce_product_type', 'physical', 'yes'),
(2371, '_transient_external_ip_address_::1', '177.82.27.37', 'no'),
(301, 'woocommerce_cod_settings', 'a:1:{s:7:\"enabled\";s:2:\"no\";}', 'yes'),
(298, 'woocommerce_ppec_paypal_settings', 'a:2:{s:16:\"reroute_requests\";b:0;s:5:\"email\";b:0;}', 'yes'),
(296, 'woocommerce_tracker_last_send', '1562164536', 'yes'),
(302, '_transient_shipping-transient-version', '1559249916', 'yes'),
(303, 'woocommerce_flat_rate_1_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:2:\"17\";}', 'yes'),
(304, 'woocommerce_flat_rate_2_settings', 'a:3:{s:5:\"title\";s:9:\"Flat rate\";s:10:\"tax_status\";s:7:\"taxable\";s:4:\"cost\";s:2:\"17\";}', 'yes'),
(354, '_transient_product-transient-version', '1561919792', 'yes'),
(328, '_transient_product_query-transient-version', '1562246506', 'yes'),
(1629, 'wc_admin_version', '0.14.0', 'yes'),
(463, 'category_children', 'a:0:{}', 'yes'),
(2826, '_site_transient_update_core', 'O:8:\"stdClass\":4:{s:7:\"updates\";a:1:{i:0;O:8:\"stdClass\":10:{s:8:\"response\";s:6:\"latest\";s:8:\"download\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:6:\"locale\";s:5:\"pt_BR\";s:8:\"packages\";O:8:\"stdClass\":5:{s:4:\"full\";s:65:\"https://downloads.wordpress.org/release/pt_BR/wordpress-5.2.2.zip\";s:10:\"no_content\";b:0;s:11:\"new_bundled\";b:0;s:7:\"partial\";b:0;s:8:\"rollback\";b:0;}s:7:\"current\";s:5:\"5.2.2\";s:7:\"version\";s:5:\"5.2.2\";s:11:\"php_version\";s:6:\"5.6.20\";s:13:\"mysql_version\";s:3:\"5.0\";s:11:\"new_bundled\";s:3:\"5.0\";s:15:\"partial_version\";s:0:\"\";}}s:12:\"last_checked\";i:1562333483;s:15:\"version_checked\";s:5:\"5.2.2\";s:12:\"translations\";a:0:{}}', 'no'),
(2827, '_site_transient_update_themes', 'O:8:\"stdClass\":4:{s:12:\"last_checked\";i:1562333483;s:7:\"checked\";a:1:{s:18:\"centurysports_loja\";s:3:\"1.0\";}s:8:\"response\";a:0:{}s:12:\"translations\";a:0:{}}', 'no'),
(2403, '_site_transient_timeout_php_check_464f4068caea2f8f3edcc5ae59429c65', '1562769333', 'no'),
(2404, '_site_transient_php_check_464f4068caea2f8f3edcc5ae59429c65', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(2211, '_transient_wc_term_counts', 'a:16:{i:30;s:1:\"6\";i:33;s:1:\"5\";i:22;s:1:\"7\";i:21;s:1:\"9\";i:20;s:1:\"9\";i:19;s:1:\"9\";i:34;s:1:\"4\";i:32;s:1:\"3\";i:36;s:1:\"4\";i:31;s:1:\"4\";i:29;s:1:\"3\";i:24;s:1:\"7\";i:35;s:1:\"3\";i:23;s:1:\"9\";i:27;s:1:\"7\";i:28;s:2:\"16\";}', 'no'),
(2210, '_transient_timeout_wc_term_counts', '1564511794', 'no'),
(514, 'recovery_mode_email_last_sent', '1561333290', 'yes'),
(488, 'redux_version_upgraded_from', '3.6.15', 'yes'),
(549, 'duplicate_post_copystatus', '0', 'yes'),
(550, 'duplicate_post_copyslug', '0', 'yes'),
(551, 'duplicate_post_copyexcerpt', '1', 'yes'),
(552, 'duplicate_post_copycontent', '1', 'yes'),
(553, 'duplicate_post_copythumbnail', '1', 'yes'),
(554, 'duplicate_post_copytemplate', '1', 'yes'),
(494, 'configuracao', 'a:54:{s:8:\"last_tab\";s:1:\"1\";s:11:\"header_logo\";a:9:{s:3:\"url\";s:76:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/logo-1.png\";s:2:\"id\";s:2:\"25\";s:6:\"height\";s:2:\"90\";s:5:\"width\";s:3:\"500\";s:9:\"thumbnail\";s:83:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/logo-1-150x90.png\";s:5:\"title\";s:4:\"logo\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:15:\"header_telefone\";s:13:\"(42)3622 9592\";s:12:\"header_frase\";s:32:\"COMPRE NO SITE E RETIRE NA LOJA!\";s:15:\"header_endereco\";s:35:\" Rua Saldanha Marinho, 1302, Centro\";s:12:\"header_links\";a:3:{i:0;s:13:\"MEUS PEDIDO|#\";i:1;s:8:\"AJUDA |#\";i:2;s:13:\"ATENDIMENTO|#\";}s:27:\"config_site_rodape_endereco\";s:65:\"RUA SALDANHA MARINHO, 123, CENTRO - GUARAPUAVA/PR - CEP 85010-290\";s:26:\"config_site_rodape_contato\";s:14:\"(12) 3456-7890\";s:27:\"config_site_rodape_whatsapp\";s:14:\"(12) 3456-7890\";s:38:\"config_site_rodape_horario_atendimento\";s:62:\"Segunda a Sexta das 9:00 às 18:00 Sábado das 09:00 às 14:00\";s:23:\"config_site_rodape_info\";s:32:\"CENTURY SPORTS | CNPJ 1234567890\";s:28:\"config_site_rodape_copyright\";s:29:\"Todos os direitos reservados.\";s:27:\"config_site_rodape_linkedin\";s:1:\"#\";s:26:\"config_site_rodape_twitter\";s:1:\"#\";s:27:\"config_site_rodape_facebook\";s:1:\"#\";s:33:\"inicial_mini_banner_modelo_hidden\";s:1:\"0\";s:26:\"inicial_mini_banner_modelo\";s:1:\"0\";s:21:\"inicial_mini_banner_1\";a:9:{s:3:\"url\";s:82:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_1\";s:16:\"Título banner 1\";s:26:\"inicial_mini_banner_link_1\";s:2:\"#1\";s:21:\"inicial_mini_banner_2\";a:9:{s:3:\"url\";s:82:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_2\";s:11:\"lo banner 2\";s:26:\"inicial_mini_banner_link_2\";s:2:\"#2\";s:21:\"inicial_mini_banner_3\";a:9:{s:3:\"url\";s:82:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titul0_3\";s:16:\"Título banner 3\";s:26:\"inicial_mini_banner_lin0_3\";s:2:\"#3\";s:21:\"inicial_mini_banner_4\";a:9:{s:3:\"url\";s:82:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1.png\";s:2:\"id\";s:2:\"47\";s:6:\"height\";s:3:\"758\";s:5:\"width\";s:3:\"555\";s:9:\"thumbnail\";s:90:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1-150x150.png\";s:5:\"title\";s:10:\"categoria1\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:28:\"inicial_mini_banner_titulo_4\";s:16:\"Título banner 4\";s:26:\"inicial_mini_banner_link_4\";s:2:\"#4\";s:38:\"inicial_sessao_categoroia_carrossel_id\";s:2:\"28\";s:32:\"inicial_sessao_categoroia_titulo\";s:23:\"VOCÊ TAMBÉM ENCONTRA:\";s:35:\"inicial_sessao_categoroia_carrossel\";s:1:\"0\";s:43:\"inicial_sessao_categoroia_carrossel_produto\";s:1:\"0\";s:28:\"inicial_sessao_categoroia_id\";s:2:\"27\";s:51:\"inicial_sessao_categoroia_carrossel_selecao_produto\";a:4:{i:0;s:4:\"nike\";i:1;s:6:\"adidas\";i:2;s:7:\"minzuno\";i:3;s:8:\"all-star\";}s:44:\"inicial_sessao_carrossel_departamento_titulo\";s:8:\"Feminino\";s:42:\"inicial_sessao_carrossel_departamento_slug\";s:8:\"bermudas\";s:46:\"inicial_sessao_carrossel_departamento_titulo_2\";s:7:\"Regatas\";s:44:\"inicial_sessao_carrossel_departamento_slug_2\";s:7:\"regatas\";s:46:\"inicial_sessao_carrossel_departamento_titulo_3\";s:6:\"Tênis\";s:44:\"inicial_sessao_carrossel_departamento_slug_3\";s:5:\"tenis\";s:46:\"inicial_sessao_carrossel_departamento_titulo_4\";s:9:\"Vestuario\";s:44:\"inicial_sessao_carrossel_departamento_slug_4\";s:9:\"vestuario\";s:26:\"inicial_banner_promocional\";a:9:{s:3:\"url\";s:76:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner.png\";s:2:\"id\";s:2:\"76\";s:6:\"height\";s:3:\"279\";s:5:\"width\";s:4:\"1920\";s:9:\"thumbnail\";s:84:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner-150x150.png\";s:5:\"title\";s:6:\"banner\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:31:\"inicial_banner_promocional_link\";s:1:\"#\";s:23:\"inicial_marcas_esconder\";s:1:\"0\";s:48:\"inicial_banner_promocional_promocionais_esconder\";s:1:\"0\";s:32:\"opt_descricao_inicial_quem_somos\";s:223:\"Bem- vindo ao site da Century Sports! A nossa loja localize-se na rua Saldanha Marinho, n° 1301, na cidade de Guarapuava/Paraná, fundada em 18 de agosto de 2004, atuando a mais de 10 anos no mercado de artigos esportivos.\";s:30:\"opt_descricao_final_quem_somos\";s:213:\"Aqui você encontra produtos para iniciar uma atividade física, praticar seus treinos favoritos e incrementar seu look na academia. Nossa missão é inspirar e motivar a vida das pessoas com mais esporte e lazer!\";s:21:\"opt_banner_quem_somos\";a:9:{s:3:\"url\";s:86:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/07/banner-quemsomos.png\";s:2:\"id\";s:3:\"130\";s:6:\"height\";s:3:\"896\";s:5:\"width\";s:4:\"1230\";s:9:\"thumbnail\";s:94:\"http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/07/banner-quemsomos-150x150.png\";s:5:\"title\";s:16:\"banner-quemsomos\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}s:21:\"opt_frases_destaque_1\";s:24:\"Aqui você vai encontrar\";s:21:\"opt_frases_destaque_2\";s:31:\"Confiança, qualidade, esporte!\";s:21:\"opt_frases_destaque_3\";s:16:\"Vem pra century!\";s:19:\"opt-customizer-only\";s:1:\"2\";}', 'yes'),
(495, 'configuracao-transients', 'a:2:{s:14:\"changed_values\";a:3:{s:32:\"opt_descricao_inicial_quem_somos\";s:0:\"\";s:30:\"opt_descricao_final_quem_somos\";s:0:\"\";s:21:\"opt_banner_quem_somos\";a:9:{s:3:\"url\";s:0:\"\";s:2:\"id\";s:0:\"\";s:6:\"height\";s:0:\"\";s:5:\"width\";s:0:\"\";s:9:\"thumbnail\";s:0:\"\";s:5:\"title\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:3:\"alt\";s:0:\"\";s:11:\"description\";s:0:\"\";}}s:9:\"last_save\";i:1562172700;}', 'yes'),
(548, 'duplicate_post_copydate', '0', 'yes'),
(547, 'duplicate_post_copytitle', '1', 'yes'),
(615, 'fs_accounts', 'a:13:{s:21:\"id_slug_type_path_map\";a:1:{i:700;a:3:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:4:\"type\";s:6:\"plugin\";s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}}s:11:\"plugin_data\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:20:{s:16:\"plugin_main_file\";O:8:\"stdClass\":1:{s:4:\"path\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";}s:17:\"install_timestamp\";i:1561337867;s:16:\"sdk_last_version\";N;s:11:\"sdk_version\";s:5:\"2.2.4\";s:16:\"sdk_upgrade_mode\";b:1;s:18:\"sdk_downgrade_mode\";b:0;s:19:\"plugin_last_version\";N;s:14:\"plugin_version\";s:5:\"1.4.0\";s:19:\"plugin_upgrade_mode\";b:1;s:21:\"plugin_downgrade_mode\";b:0;s:21:\"is_plugin_new_install\";b:1;s:17:\"connectivity_test\";a:6:{s:12:\"is_connected\";b:1;s:4:\"host\";s:9:\"localhost\";s:9:\"server_ip\";s:3:\"::1\";s:9:\"is_active\";b:1;s:9:\"timestamp\";i:1561337867;s:7:\"version\";s:5:\"1.4.0\";}s:17:\"was_plugin_loaded\";b:1;s:15:\"prev_is_premium\";b:0;s:14:\"has_trial_plan\";b:0;s:22:\"install_sync_timestamp\";i:1562333486;s:19:\"keepalive_timestamp\";i:1562333486;s:20:\"activation_timestamp\";i:1561337893;s:9:\"sync_cron\";O:8:\"stdClass\":5:{s:7:\"version\";s:5:\"1.4.0\";s:7:\"blog_id\";i:0;s:11:\"sdk_version\";s:5:\"2.2.4\";s:9:\"timestamp\";i:1561337899;s:2:\"on\";b:1;}s:14:\"sync_timestamp\";i:1562333486;}}s:13:\"file_slug_map\";a:1:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:27:\"ajax-search-for-woocommerce\";}s:7:\"plugins\";a:1:{s:27:\"ajax-search-for-woocommerce\";O:9:\"FS_Plugin\":20:{s:16:\"parent_plugin_id\";N;s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:12:\"premium_slug\";s:35:\"ajax-search-for-woocommerce-premium\";s:4:\"type\";s:6:\"plugin\";s:20:\"affiliate_moderation\";b:0;s:19:\"is_wp_org_compliant\";b:1;s:4:\"file\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:7:\"version\";s:5:\"1.4.0\";s:11:\"auto_update\";N;s:4:\"info\";N;s:10:\"is_premium\";b:0;s:14:\"premium_suffix\";s:3:\"Pro\";s:7:\"is_live\";b:1;s:10:\"public_key\";s:32:\"pk_f4f2a51dbe0aee43de0692db77a3e\";s:10:\"secret_key\";N;s:2:\"id\";s:3:\"700\";s:7:\"updated\";N;s:7:\"created\";N;s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:9:\"unique_id\";s:32:\"fc8a93668c09868b3795703092a94f0d\";s:5:\"plans\";a:1:{s:27:\"ajax-search-for-woocommerce\";a:2:{i:0;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:4:\"NzAw\";s:4:\"name\";s:8:\"ZnJlZQ==\";s:5:\"title\";s:12:\"U3RhcnRlcg==\";s:11:\"description\";s:40:\"U2VhcmNoIHByb2R1Y3RzIGluIGxpdmUgdGltZQ==\";s:17:\"is_free_localhost\";s:4:\"MQ==\";s:17:\"is_block_features\";s:4:\"MQ==\";s:12:\"license_type\";s:4:\"MA==\";s:16:\"is_https_support\";s:0:\"\";s:12:\"trial_period\";N;s:23:\"is_require_subscription\";s:0:\"\";s:10:\"support_kb\";N;s:13:\"support_forum\";s:88:\"aHR0cHM6Ly93b3JkcHJlc3Mub3JnL3N1cHBvcnQvcGx1Z2luL2FqYXgtc2VhcmNoLWZvci13b29jb21tZXJjZQ==\";s:13:\"support_email\";N;s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";s:0:\"\";s:11:\"is_featured\";s:0:\"\";s:2:\"id\";s:8:\"MjkwMg==\";s:7:\"updated\";s:28:\"MjAxOC0xMi0yOCAwMDowNDowNQ==\";s:7:\"created\";s:28:\"MjAxOC0wNC0wNyAyMTozNjo1Ng==\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}i:1;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:4:\"NzAw\";s:4:\"name\";s:4:\"cHJv\";s:5:\"title\";s:4:\"UHJv\";s:11:\"description\";s:76:\"I0luY3JlYXNlIHlvdXIgc2FsZXMgYnkgZmFzdGVyIGFuZCBtb2Rlcm4gc2VhcmNoIGVuZ2luZQ==\";s:17:\"is_free_localhost\";s:4:\"MQ==\";s:17:\"is_block_features\";s:4:\"MQ==\";s:12:\"license_type\";s:4:\"MA==\";s:16:\"is_https_support\";s:0:\"\";s:12:\"trial_period\";N;s:23:\"is_require_subscription\";s:4:\"MQ==\";s:10:\"support_kb\";N;s:13:\"support_forum\";N;s:13:\"support_email\";s:32:\"ZGdvcmFwbHVnaW5zQGdtYWlsLmNvbQ==\";s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";s:0:\"\";s:11:\"is_featured\";s:4:\"MQ==\";s:2:\"id\";s:8:\"NDc5OQ==\";s:7:\"updated\";s:28:\"MjAxOS0wNC0wOSAxNjoyMjo0Mg==\";s:7:\"created\";s:28:\"MjAxOC0xMi0yNyAyMzoxODowNg==\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}}s:14:\"active_plugins\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1562333486;s:3:\"md5\";s:32:\"13a8beae5d055c20bc2b22f3d88b7374\";s:7:\"plugins\";a:8:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";a:5:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:7:\"version\";s:5:\"1.4.0\";s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:41:\"base-centurySports/base-centurySports.php\";a:5:{s:4:\"slug\";s:18:\"base-centurySports\";s:7:\"version\";s:3:\"0.1\";s:5:\"title\";s:19:\"Base Century Sports\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:21:\"meta-box/meta-box.php\";a:5:{s:4:\"slug\";s:8:\"meta-box\";s:7:\"version\";s:6:\"4.18.2\";s:5:\"title\";s:8:\"Meta Box\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:35:\"redux-framework/redux-framework.php\";a:5:{s:4:\"slug\";s:15:\"redux-framework\";s:7:\"version\";s:6:\"3.6.15\";s:5:\"title\";s:15:\"Redux Framework\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:27:\"woocommerce/woocommerce.php\";a:5:{s:4:\"slug\";s:11:\"woocommerce\";s:7:\"version\";s:5:\"3.6.4\";s:5:\"title\";s:11:\"WooCommerce\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:39:\"categories-images/categories-images.php\";a:5:{s:4:\"slug\";s:17:\"categories-images\";s:7:\"version\";s:5:\"2.5.4\";s:5:\"title\";s:17:\"Categories Images\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:37:\"woocommerce-products-filter/index.php\";a:5:{s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:7:\"version\";s:7:\"1.2.2.1\";s:5:\"title\";s:34:\"WOOF - WooCommerce Products Filter\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}s:36:\"contact-form-7/wp-contact-form-7.php\";a:5:{s:4:\"slug\";s:14:\"contact-form-7\";s:7:\"version\";s:5:\"5.1.3\";s:5:\"title\";s:14:\"Contact Form 7\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}}}s:11:\"all_plugins\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1562245030;s:3:\"md5\";s:32:\"48681b9cf822a6232250bed435c1c2b3\";s:7:\"plugins\";a:10:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";a:5:{s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:7:\"version\";s:5:\"1.4.0\";s:5:\"title\";s:27:\"AJAX Search for WooCommerce\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:41:\"base-centurySports/base-centurySports.php\";a:5:{s:4:\"slug\";s:18:\"base-centurySports\";s:7:\"version\";s:3:\"0.1\";s:5:\"title\";s:19:\"Base Century Sports\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:21:\"meta-box/meta-box.php\";a:6:{s:4:\"slug\";s:8:\"meta-box\";s:7:\"version\";s:6:\"4.18.2\";s:5:\"title\";s:8:\"Meta Box\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;s:7:\"Version\";s:6:\"4.18.3\";}s:35:\"redux-framework/redux-framework.php\";a:5:{s:4:\"slug\";s:15:\"redux-framework\";s:7:\"version\";s:6:\"3.6.15\";s:5:\"title\";s:15:\"Redux Framework\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:27:\"woocommerce/woocommerce.php\";a:5:{s:4:\"slug\";s:11:\"woocommerce\";s:7:\"version\";s:5:\"3.6.4\";s:5:\"title\";s:11:\"WooCommerce\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:39:\"woocommerce-admin/woocommerce-admin.php\";a:6:{s:4:\"slug\";s:17:\"woocommerce-admin\";s:7:\"version\";s:6:\"0.12.0\";s:5:\"title\";s:17:\"WooCommerce Admin\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;s:7:\"Version\";s:6:\"0.14.0\";}s:39:\"categories-images/categories-images.php\";a:5:{s:4:\"slug\";s:17:\"categories-images\";s:7:\"version\";s:5:\"2.5.4\";s:5:\"title\";s:17:\"Categories Images\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:37:\"woocommerce-products-filter/index.php\";a:5:{s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:7:\"version\";s:7:\"1.2.2.1\";s:5:\"title\";s:34:\"WOOF - WooCommerce Products Filter\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:41:\"yith-woocommerce-ajax-navigation/init.php\";a:5:{s:4:\"slug\";s:32:\"yith-woocommerce-ajax-navigation\";s:7:\"version\";s:5:\"3.6.6\";s:5:\"title\";s:36:\"YITH WooCommerce Ajax Product Filter\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}s:36:\"contact-form-7/wp-contact-form-7.php\";a:5:{s:4:\"slug\";s:14:\"contact-form-7\";s:7:\"version\";s:5:\"5.1.3\";s:5:\"title\";s:14:\"Contact Form 7\";s:9:\"is_active\";b:0;s:14:\"is_uninstalled\";b:0;}}}s:10:\"all_themes\";O:8:\"stdClass\":3:{s:9:\"timestamp\";i:1562333486;s:3:\"md5\";s:32:\"f9887237554bc5c5b2d13d2aca67df81\";s:6:\"themes\";a:1:{s:18:\"centurysports_loja\";a:5:{s:4:\"slug\";s:18:\"centurysports_loja\";s:7:\"version\";s:3:\"1.0\";s:5:\"title\";s:15:\"Amanda Karoline\";s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;}}}s:5:\"sites\";a:1:{s:27:\"ajax-search-for-woocommerce\";O:7:\"FS_Site\":25:{s:7:\"site_id\";s:8:\"10422296\";s:9:\"plugin_id\";s:3:\"700\";s:7:\"user_id\";s:7:\"1979899\";s:5:\"title\";s:14:\"Century Sports\";s:3:\"url\";s:38:\"http://centurysports.hcdesenvolvimentos.com\";s:7:\"version\";s:5:\"1.4.0\";s:8:\"language\";s:5:\"pt-BR\";s:7:\"charset\";s:5:\"UTF-8\";s:16:\"platform_version\";s:5:\"5.2.1\";s:11:\"sdk_version\";s:5:\"2.2.4\";s:28:\"programming_language_version\";s:6:\"7.2.10\";s:7:\"plan_id\";s:4:\"2902\";s:10:\"license_id\";N;s:13:\"trial_plan_id\";N;s:10:\"trial_ends\";N;s:10:\"is_premium\";b:0;s:15:\"is_disconnected\";b:0;s:9:\"is_active\";b:1;s:14:\"is_uninstalled\";b:0;s:10:\"public_key\";s:32:\"pk_266d8c2ff927c47c8896d143b985f\";s:10:\"secret_key\";s:32:\"sk_QsGs)T$*[O^lYEJtLt1K.nh7cI0]@\";s:2:\"id\";s:7:\"2757685\";s:7:\"updated\";N;s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:5:\"users\";a:1:{i:1979899;O:7:\"FS_User\":12:{s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:5:\"first\";s:13:\"centurysports\";s:4:\"last\";s:0:\"\";s:11:\"is_verified\";b:0;s:11:\"customer_id\";N;s:5:\"gross\";i:0;s:10:\"public_key\";s:32:\"pk_e708ffe655728bcccb003c5a536ef\";s:10:\"secret_key\";s:32:\"sk_sKl=s44[b1{F<^^[7kp2qv[U-gAq{\";s:2:\"id\";s:7:\"1979899\";s:7:\"updated\";N;s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}s:23:\"user_id_license_ids_map\";a:1:{i:700;a:1:{i:1979899;a:0:{}}}s:12:\"all_licenses\";a:1:{i:700;a:0:{}}}', 'yes'),
(645, 'nav_menu_options', 'a:2:{i:0;b:0;s:8:\"auto_add\";a:0:{}}', 'yes'),
(2822, '_transient_timeout__woocommerce_helper_updates', '1562376679', 'no'),
(2823, '_transient__woocommerce_helper_updates', 'a:4:{s:4:\"hash\";s:32:\"d751713988987e9331980363e24189ce\";s:7:\"updated\";i:1562333479;s:8:\"products\";a:0:{}s:6:\"errors\";a:1:{i:0;s:10:\"http-error\";}}', 'no'),
(616, 'fs_api_cache', 'a:5:{s:29:\"get:/v1/plugins/700/info.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":14:{s:9:\"plugin_id\";s:3:\"700\";s:3:\"url\";N;s:11:\"description\";N;s:17:\"short_description\";N;s:10:\"banner_url\";N;s:15:\"card_banner_url\";N;s:15:\"selling_point_0\";N;s:15:\"selling_point_1\";N;s:15:\"selling_point_2\";N;s:11:\"screenshots\";N;s:2:\"id\";s:3:\"682\";s:7:\"created\";s:19:\"2019-03-01 23:26:26\";s:7:\"updated\";N;s:4:\"icon\";s:92:\"//s3-us-west-2.amazonaws.com/freemius/plugins/700/icons/b73778ae43d2248effda74c277ca3025.jpg\";}s:7:\"created\";i:1561337872;s:9:\"timestamp\";i:1561942672;}s:26:\"get:/v1/users/1979899.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":16:{s:15:\"default_card_id\";N;s:5:\"gross\";i:0;s:6:\"source\";i:0;s:13:\"last_login_at\";N;s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:5:\"first\";s:13:\"centurysports\";s:4:\"last\";s:0:\"\";s:7:\"picture\";N;s:2:\"ip\";s:14:\"191.177.186.40\";s:11:\"is_verified\";b:0;s:10:\"secret_key\";s:32:\"sk_sKl=s44[b1{F<^^[7kp2qv[U-gAq{\";s:10:\"public_key\";s:32:\"pk_e708ffe655728bcccb003c5a536ef\";s:2:\"id\";s:7:\"1979899\";s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:7:\"updated\";N;s:11:\"customer_id\";N;}s:7:\"created\";i:1561337893;s:9:\"timestamp\";i:1561424293;}s:29:\"get:/v1/installs/2757685.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":31:{s:7:\"site_id\";s:8:\"10422296\";s:9:\"plugin_id\";s:3:\"700\";s:7:\"user_id\";s:7:\"1979899\";s:3:\"url\";s:38:\"http://centurysports.hcdesenvolvimentos.com\";s:5:\"title\";s:14:\"Century Sports\";s:7:\"version\";s:5:\"1.4.0\";s:7:\"plan_id\";s:4:\"2902\";s:10:\"license_id\";N;s:13:\"trial_plan_id\";N;s:10:\"trial_ends\";N;s:15:\"subscription_id\";N;s:5:\"gross\";i:0;s:12:\"country_code\";s:2:\"br\";s:8:\"language\";s:5:\"pt-BR\";s:7:\"charset\";s:5:\"UTF-8\";s:16:\"platform_version\";s:5:\"5.2.1\";s:11:\"sdk_version\";s:5:\"2.2.4\";s:28:\"programming_language_version\";s:6:\"7.2.10\";s:9:\"is_active\";b:1;s:15:\"is_disconnected\";b:0;s:10:\"is_premium\";b:0;s:14:\"is_uninstalled\";b:0;s:9:\"is_locked\";b:0;s:6:\"source\";i:0;s:8:\"upgraded\";N;s:12:\"last_seen_at\";s:19:\"2019-06-24 00:58:14\";s:10:\"secret_key\";s:32:\"sk_QsGs)T$*[O^lYEJtLt1K.nh7cI0]@\";s:10:\"public_key\";s:32:\"pk_266d8c2ff927c47c8896d143b985f\";s:2:\"id\";s:7:\"2757685\";s:7:\"created\";s:19:\"2019-06-24 00:58:09\";s:7:\"updated\";N;}s:7:\"created\";i:1561337893;s:9:\"timestamp\";i:1561424293;}s:44:\"get:/v1/users/1979899/plugins/700/plans.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":1:{s:5:\"plans\";a:2:{i:0;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:3:\"700\";s:4:\"name\";s:4:\"free\";s:5:\"title\";s:7:\"Starter\";s:11:\"description\";s:28:\"Search products in live time\";s:17:\"is_free_localhost\";b:1;s:17:\"is_block_features\";b:1;s:12:\"license_type\";i:0;s:16:\"is_https_support\";b:0;s:12:\"trial_period\";N;s:23:\"is_require_subscription\";b:0;s:10:\"support_kb\";N;s:13:\"support_forum\";s:64:\"https://wordpress.org/support/plugin/ajax-search-for-woocommerce\";s:13:\"support_email\";N;s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";b:0;s:11:\"is_featured\";b:0;s:2:\"id\";s:4:\"2902\";s:7:\"updated\";s:19:\"2018-12-28 00:04:05\";s:7:\"created\";s:19:\"2018-04-07 21:36:56\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}i:1;O:14:\"FS_Plugin_Plan\":21:{s:9:\"plugin_id\";s:3:\"700\";s:4:\"name\";s:3:\"pro\";s:5:\"title\";s:3:\"Pro\";s:11:\"description\";s:55:\"#Increase your sales by faster and modern search engine\";s:17:\"is_free_localhost\";b:1;s:17:\"is_block_features\";b:1;s:12:\"license_type\";i:0;s:16:\"is_https_support\";b:0;s:12:\"trial_period\";N;s:23:\"is_require_subscription\";b:1;s:10:\"support_kb\";N;s:13:\"support_forum\";N;s:13:\"support_email\";s:22:\"dgoraplugins@gmail.com\";s:13:\"support_phone\";N;s:13:\"support_skype\";N;s:18:\"is_success_manager\";b:0;s:11:\"is_featured\";b:1;s:2:\"id\";s:4:\"4799\";s:7:\"updated\";s:19:\"2019-04-09 16:22:42\";s:7:\"created\";s:19:\"2018-12-27 23:18:06\";s:22:\"\0FS_Entity\0_is_updated\";b:0;}}}s:7:\"created\";i:1562333478;s:9:\"timestamp\";i:1562419878;}s:47:\"get:/v1/users/1979899/plugins/700/licenses.json\";O:8:\"stdClass\":3:{s:6:\"result\";O:8:\"stdClass\":1:{s:8:\"licenses\";a:0:{}}s:7:\"created\";i:1562333478;s:9:\"timestamp\";i:1562419878;}}', 'yes'),
(617, 'fs_gdpr', 'a:1:{s:2:\"u1\";a:2:{s:8:\"required\";b:0;s:18:\"show_opt_in_notice\";b:0;}}', 'yes'),
(620, 'widget_dgwt_wcas_ajax_search', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(622, 'dgwt_wcas_activation_date', '1561337872', 'yes'),
(623, 'dgwt_wcas_settings', 'a:51:{s:10:\"how_to_use\";s:0:\"\";s:17:\"suggestions_limit\";i:10;s:9:\"min_chars\";i:3;s:14:\"max_form_width\";i:600;s:18:\"show_submit_button\";s:3:\"off\";s:25:\"search_form_labels_header\";s:0:\"\";s:18:\"search_submit_text\";s:6:\"Search\";s:18:\"search_placeholder\";s:22:\"Search for products...\";s:27:\"search_see_all_results_text\";s:18:\"See all results...\";s:22:\"search_no_results_text\";s:10:\"No results\";s:23:\"product_suggestion_head\";s:0:\"\";s:18:\"show_product_image\";s:3:\"off\";s:18:\"show_product_price\";s:3:\"off\";s:17:\"show_product_desc\";s:3:\"off\";s:16:\"show_product_sku\";s:3:\"off\";s:24:\"show_matching_categories\";s:2:\"on\";s:18:\"show_matching_tags\";s:3:\"off\";s:6:\"mobile\";s:0:\"\";s:21:\"enable_mobile_overlay\";s:3:\"off\";s:9:\"preloader\";s:0:\"\";s:14:\"show_preloader\";s:2:\"on\";s:13:\"preloader_url\";s:0:\"\";s:16:\"details_box_head\";s:0:\"\";s:16:\"show_details_box\";s:3:\"off\";s:12:\"show_for_tax\";s:2:\"on\";s:15:\"orderby_for_tax\";s:2:\"on\";s:13:\"order_for_tax\";s:4:\"desc\";s:11:\"search_form\";s:0:\"\";s:14:\"bg_input_color\";s:0:\"\";s:16:\"text_input_color\";s:0:\"\";s:18:\"border_input_color\";s:0:\"\";s:15:\"bg_submit_color\";s:0:\"\";s:17:\"text_submit_color\";s:0:\"\";s:22:\"syggestions_style_head\";s:0:\"\";s:12:\"sug_bg_color\";s:0:\"\";s:15:\"sug_hover_color\";s:0:\"\";s:14:\"sug_text_color\";s:0:\"\";s:19:\"sug_highlight_color\";s:0:\"\";s:16:\"sug_border_color\";s:0:\"\";s:17:\"search_scope_head\";s:0:\"\";s:17:\"search_scope_desc\";s:0:\"\";s:25:\"search_in_product_content\";s:3:\"off\";s:25:\"search_in_product_excerpt\";s:3:\"off\";s:21:\"search_in_product_sku\";s:3:\"off\";s:28:\"search_in_product_attributes\";s:3:\"off\";s:20:\"exclude_out_of_stock\";s:3:\"off\";s:12:\"pro_features\";s:0:\"\";s:27:\"search_scope_fuzziness_head\";s:0:\"\";s:22:\"fuzziness_enabled_demo\";s:3:\"off\";s:18:\"search_engine_head\";s:0:\"\";s:19:\"search_engine_build\";s:0:\"\";}', 'yes'),
(624, 'dgwt_wcas_version', '1.4.0', 'yes'),
(625, '_site_transient_timeout_locked_1', '1876697897', 'no'),
(626, '_site_transient_locked_1', '1', 'no'),
(827, '_site_transient_timeout_php_check_03bb19de23a7f39f237dfd15fa323af5', '1562281861', 'no'),
(828, '_site_transient_php_check_03bb19de23a7f39f237dfd15fa323af5', 'a:5:{s:19:\"recommended_version\";s:3:\"7.3\";s:15:\"minimum_version\";s:6:\"5.6.20\";s:12:\"is_supported\";b:1;s:9:\"is_secure\";b:1;s:13:\"is_acceptable\";b:1;}', 'no'),
(852, '_transient_timeout_wc_shipping_method_count_legacy', '1564270161', 'no'),
(853, '_transient_wc_shipping_method_count_legacy', 'a:2:{s:7:\"version\";s:10:\"1559249916\";s:5:\"value\";i:2;}', 'no'),
(2876, '_transient_timeout_wc_report_sales_by_date', '1562424864', 'no'),
(2877, '_transient_wc_report_sales_by_date', 'a:8:{s:32:\"76a147de20a9edf13ee922682067369a\";a:0:{}s:32:\"dc9b9dbec4af2fa5140bdd15819e1d1e\";a:0:{}s:32:\"a5caa2762908c6aa6dd8097566081630\";a:0:{}s:32:\"0436186a040745b3de60662fe8fe4883\";N;s:32:\"3f9f8f04993e2d0ed4916c5d7d80bcaa\";a:0:{}s:32:\"e10b4bfecbd1e24d8e5437337a45a932\";a:0:{}s:32:\"bb97b0f3a1b1f2e305aebea806c70c8b\";a:0:{}s:32:\"7b97ce9872e7818ae61ff863e39e406a\";a:0:{}}', 'no'),
(2878, '_transient_timeout_wc_admin_report', '1562424864', 'no'),
(2879, '_transient_wc_admin_report', 'a:1:{s:32:\"4ae1b0eb3ac6fecaa7d52565b4af4a7e\";a:0:{}}', 'no'),
(2410, '_transient_timeout_wc_low_stock_count', '1564756535', 'no'),
(2411, '_transient_wc_low_stock_count', '0', 'no'),
(1950, 'product_cat_children', 'a:2:{i:28;a:8:{i:0;i:19;i:1;i:20;i:2;i:21;i:3;i:22;i:4;i:23;i:5;i:24;i:6;i:35;i:7;i:36;}i:27;a:6:{i:0;i:29;i:1;i:30;i:2;i:31;i:3;i:32;i:4;i:33;i:5;i:34;}}', 'yes'),
(1730, 'woocommerce_meta_box_errors', 'a:0:{}', 'yes'),
(2227, '_transient_timeout_wc_shipping_method_count', '1564513017', 'no'),
(2382, '_transient_timeout_wc_product_children_109', '1564574565', 'no'),
(2383, '_transient_wc_product_children_109', 'a:2:{s:3:\"all\";a:2:{i:0;i:115;i:1;i:116;}s:7:\"visible\";a:2:{i:0;i:115;i:1;i:116;}}', 'no'),
(2384, '_transient_timeout_wc_var_prices_109', '1564574565', 'no'),
(2385, '_transient_wc_var_prices_109', '{\"version\":\"1561919792\",\"f9e544f77b7eac7add281ef28ca5559f\":{\"price\":{\"115\":\"200.00\",\"116\":\"123.00\"},\"regular_price\":{\"115\":\"200.00\",\"116\":\"123.00\"},\"sale_price\":{\"115\":\"200.00\",\"116\":\"123.00\"}}}', 'no'),
(2795, '_transient_timeout_wc_upgrade_notice_3.6.5', '1562358258', 'no'),
(2796, '_transient_wc_upgrade_notice_3.6.5', '', 'no'),
(1636, 'yit_recently_activated', 'a:1:{i:0;s:41:\"yith-woocommerce-ajax-navigation/init.php\";}', 'yes'),
(1637, 'widget_yith-woo-ajax-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1638, 'widget_yith-woo-ajax-reset-navigation', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1639, 'yit_wcan_options', 'a:6:{s:29:\"yith_wcan_ajax_shop_container\";s:9:\".products\";s:30:\"yith_wcan_ajax_shop_pagination\";s:26:\"nav.woocommerce-pagination\";s:36:\"yith_wcan_ajax_shop_result_container\";s:25:\".woocommerce-result-count\";s:31:\"yith_wcan_ajax_scroll_top_class\";s:19:\".yit-wcan-container\";s:31:\"yith_wcan_ajax_shop_terms_order\";s:12:\"alphabetical\";s:22:\"yith_wcan_custom_style\";s:0:\"\";}', 'yes'),
(1640, '_site_transient_timeout_yith_promo_message', '3137838356', 'no'),
(1641, '_site_transient_yith_promo_message', '<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<!-- Default border color: #acc327 -->\n<!-- Default background color: #ecf7ed -->\n\n<promotions>\n    <expiry_date>2019-12-10</expiry_date>\n    <promo>\n        <promo_id>yithhalloween2019</promo_id>\n        <banner>halloween.jpg</banner>\n        <title><![CDATA[<strong>YITH Halloween</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid only on <strong>31st October</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#005c7d</image_bg_color>\n            <border_color>#ea5105</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-10-30 16:00:00</start_date>\n        <end_date>2019-11-01 08:00:00</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithcybermonday2019</promo_id>\n        <banner>cyber.jpg</banner>\n        <title><![CDATA[<strong>YITH Cyber Monday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>28th November</strong> to <strong>30th November</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#12fdd4</image_bg_color>\n            <border_color>#181d7b</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-12-01 00:00:00</start_date>\n        <end_date>2019-12-03 08:00:00</end_date>\n    </promo>\n    <promo>\n        <promo_id>yithblackfriday2019</promo_id>\n        <banner>black.jpg</banner>\n        <title><![CDATA[<strong>YITH Black Friday</strong>]]></title>\n        <description><![CDATA[\n            Don\'t miss our <strong>30% discount</strong> on all our products! No coupon needed in cart. Valid from <strong>1st December</strong> to <strong>2nd December</strong>.\n        ]]></description>\n        <link>\n            <label>Get your deals now!</label>\n            <url><![CDATA[https://yithemes.com]]></url>\n        </link>\n        <style>\n            <image_bg_color>#272121</image_bg_color>\n            <border_color>#272121</border_color>\n            <background_color>#ffffff</background_color>\n        </style>\n        <start_date>2019-11-27 23:59:59</start_date>\n        <end_date>2019-11-30 23:59:59</end_date>\n    </promo>\n</promotions>', 'no'),
(2828, '_site_transient_update_plugins', 'O:8:\"stdClass\":5:{s:12:\"last_checked\";i:1562333485;s:7:\"checked\";a:10:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:5:\"1.4.0\";s:41:\"base-centurySports/base-centurySports.php\";s:3:\"0.1\";s:39:\"categories-images/categories-images.php\";s:5:\"2.5.4\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:5:\"5.1.3\";s:21:\"meta-box/meta-box.php\";s:6:\"4.18.3\";s:35:\"redux-framework/redux-framework.php\";s:6:\"3.6.15\";s:27:\"woocommerce/woocommerce.php\";s:5:\"3.6.4\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:6:\"0.14.0\";s:37:\"woocommerce-products-filter/index.php\";s:7:\"1.2.2.1\";s:41:\"yith-woocommerce-ajax-navigation/init.php\";s:5:\"3.6.6\";}s:8:\"response\";a:1:{s:27:\"woocommerce/woocommerce.php\";O:8:\"stdClass\":12:{s:2:\"id\";s:25:\"w.org/plugins/woocommerce\";s:4:\"slug\";s:11:\"woocommerce\";s:6:\"plugin\";s:27:\"woocommerce/woocommerce.php\";s:11:\"new_version\";s:5:\"3.6.5\";s:3:\"url\";s:42:\"https://wordpress.org/plugins/woocommerce/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce.3.6.5.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-256x256.png?rev=2075035\";s:2:\"1x\";s:64:\"https://ps.w.org/woocommerce/assets/icon-128x128.png?rev=2075035\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:67:\"https://ps.w.org/woocommerce/assets/banner-1544x500.png?rev=2075035\";s:2:\"1x\";s:66:\"https://ps.w.org/woocommerce/assets/banner-772x250.png?rev=2075035\";}s:11:\"banners_rtl\";a:0:{}s:6:\"tested\";s:5:\"5.2.2\";s:12:\"requires_php\";b:0;s:13:\"compatibility\";O:8:\"stdClass\":0:{}}}s:12:\"translations\";a:3:{i:0;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:14:\"contact-form-7\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"5.1.3\";s:7:\"updated\";s:19:\"2019-06-13 22:13:44\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/translation/plugin/contact-form-7/5.1.3/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:1;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:8:\"meta-box\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:6:\"4.18.2\";s:7:\"updated\";s:19:\"2018-12-05 02:06:02\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/translation/plugin/meta-box/4.18.2/pt_BR.zip\";s:10:\"autoupdate\";b:1;}i:2;a:7:{s:4:\"type\";s:6:\"plugin\";s:4:\"slug\";s:11:\"woocommerce\";s:8:\"language\";s:5:\"pt_BR\";s:7:\"version\";s:5:\"3.6.4\";s:7:\"updated\";s:19:\"2019-06-17 18:34:40\";s:7:\"package\";s:78:\"https://downloads.wordpress.org/translation/plugin/woocommerce/3.6.4/pt_BR.zip\";s:10:\"autoupdate\";b:1;}}s:9:\"no_update\";a:8:{s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/ajax-search-for-woocommerce\";s:4:\"slug\";s:27:\"ajax-search-for-woocommerce\";s:6:\"plugin\";s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";s:11:\"new_version\";s:5:\"1.4.0\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/ajax-search-for-woocommerce/\";s:7:\"package\";s:76:\"https://downloads.wordpress.org/plugin/ajax-search-for-woocommerce.1.4.0.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-256x256.png?rev=2042590\";s:2:\"1x\";s:80:\"https://ps.w.org/ajax-search-for-woocommerce/assets/icon-128x128.png?rev=2042590\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/ajax-search-for-woocommerce/assets/banner-772x250.png?rev=2042591\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"categories-images/categories-images.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/categories-images\";s:4:\"slug\";s:17:\"categories-images\";s:6:\"plugin\";s:39:\"categories-images/categories-images.php\";s:11:\"new_version\";s:5:\"2.5.4\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/categories-images/\";s:7:\"package\";s:66:\"https://downloads.wordpress.org/plugin/categories-images.2.5.4.zip\";s:5:\"icons\";a:1:{s:7:\"default\";s:68:\"https://s.w.org/plugins/geopattern-icon/categories-images_7a8aa3.svg\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:72:\"https://ps.w.org/categories-images/assets/banner-772x250.png?rev=1803373\";}s:11:\"banners_rtl\";a:0:{}}s:36:\"contact-form-7/wp-contact-form-7.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:28:\"w.org/plugins/contact-form-7\";s:4:\"slug\";s:14:\"contact-form-7\";s:6:\"plugin\";s:36:\"contact-form-7/wp-contact-form-7.php\";s:11:\"new_version\";s:5:\"5.1.3\";s:3:\"url\";s:45:\"https://wordpress.org/plugins/contact-form-7/\";s:7:\"package\";s:63:\"https://downloads.wordpress.org/plugin/contact-form-7.5.1.3.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-256x256.png?rev=984007\";s:2:\"1x\";s:66:\"https://ps.w.org/contact-form-7/assets/icon-128x128.png?rev=984007\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:69:\"https://ps.w.org/contact-form-7/assets/banner-1544x500.png?rev=860901\";s:2:\"1x\";s:68:\"https://ps.w.org/contact-form-7/assets/banner-772x250.png?rev=880427\";}s:11:\"banners_rtl\";a:0:{}}s:21:\"meta-box/meta-box.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:22:\"w.org/plugins/meta-box\";s:4:\"slug\";s:8:\"meta-box\";s:6:\"plugin\";s:21:\"meta-box/meta-box.php\";s:11:\"new_version\";s:6:\"4.18.3\";s:3:\"url\";s:39:\"https://wordpress.org/plugins/meta-box/\";s:7:\"package\";s:58:\"https://downloads.wordpress.org/plugin/meta-box.4.18.3.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:61:\"https://ps.w.org/meta-box/assets/icon-128x128.png?rev=1100915\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:63:\"https://ps.w.org/meta-box/assets/banner-772x250.png?rev=1929588\";}s:11:\"banners_rtl\";a:0:{}}s:35:\"redux-framework/redux-framework.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:29:\"w.org/plugins/redux-framework\";s:4:\"slug\";s:15:\"redux-framework\";s:6:\"plugin\";s:35:\"redux-framework/redux-framework.php\";s:11:\"new_version\";s:6:\"3.6.15\";s:3:\"url\";s:46:\"https://wordpress.org/plugins/redux-framework/\";s:7:\"package\";s:65:\"https://downloads.wordpress.org/plugin/redux-framework.3.6.15.zip\";s:5:\"icons\";a:3:{s:2:\"2x\";s:67:\"https://ps.w.org/redux-framework/assets/icon-256x256.png?rev=995554\";s:2:\"1x\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";s:3:\"svg\";s:59:\"https://ps.w.org/redux-framework/assets/icon.svg?rev=995554\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:69:\"https://ps.w.org/redux-framework/assets/banner-772x250.png?rev=793165\";}s:11:\"banners_rtl\";a:0:{}}s:39:\"woocommerce-admin/woocommerce-admin.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:31:\"w.org/plugins/woocommerce-admin\";s:4:\"slug\";s:17:\"woocommerce-admin\";s:6:\"plugin\";s:39:\"woocommerce-admin/woocommerce-admin.php\";s:11:\"new_version\";s:6:\"0.14.0\";s:3:\"url\";s:48:\"https://wordpress.org/plugins/woocommerce-admin/\";s:7:\"package\";s:60:\"https://downloads.wordpress.org/plugin/woocommerce-admin.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-256x256.jpg?rev=2057866\";s:2:\"1x\";s:70:\"https://ps.w.org/woocommerce-admin/assets/icon-128x128.jpg?rev=2057866\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:73:\"https://ps.w.org/woocommerce-admin/assets/banner-1544x500.jpg?rev=2057866\";s:2:\"1x\";s:72:\"https://ps.w.org/woocommerce-admin/assets/banner-772x250.jpg?rev=2057866\";}s:11:\"banners_rtl\";a:0:{}}s:37:\"woocommerce-products-filter/index.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:41:\"w.org/plugins/woocommerce-products-filter\";s:4:\"slug\";s:27:\"woocommerce-products-filter\";s:6:\"plugin\";s:37:\"woocommerce-products-filter/index.php\";s:11:\"new_version\";s:7:\"1.2.2.1\";s:3:\"url\";s:58:\"https://wordpress.org/plugins/woocommerce-products-filter/\";s:7:\"package\";s:70:\"https://downloads.wordpress.org/plugin/woocommerce-products-filter.zip\";s:5:\"icons\";a:2:{s:2:\"2x\";s:80:\"https://ps.w.org/woocommerce-products-filter/assets/icon-256x256.png?rev=1208073\";s:2:\"1x\";s:80:\"https://ps.w.org/woocommerce-products-filter/assets/icon-128x128.png?rev=1208072\";}s:7:\"banners\";a:1:{s:2:\"1x\";s:82:\"https://ps.w.org/woocommerce-products-filter/assets/banner-772x250.png?rev=2071519\";}s:11:\"banners_rtl\";a:0:{}}s:41:\"yith-woocommerce-ajax-navigation/init.php\";O:8:\"stdClass\":9:{s:2:\"id\";s:46:\"w.org/plugins/yith-woocommerce-ajax-navigation\";s:4:\"slug\";s:32:\"yith-woocommerce-ajax-navigation\";s:6:\"plugin\";s:41:\"yith-woocommerce-ajax-navigation/init.php\";s:11:\"new_version\";s:5:\"3.6.6\";s:3:\"url\";s:63:\"https://wordpress.org/plugins/yith-woocommerce-ajax-navigation/\";s:7:\"package\";s:81:\"https://downloads.wordpress.org/plugin/yith-woocommerce-ajax-navigation.3.6.6.zip\";s:5:\"icons\";a:1:{s:2:\"1x\";s:85:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/icon-128x128.jpg?rev=1460901\";}s:7:\"banners\";a:2:{s:2:\"2x\";s:88:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-1544x500.jpg?rev=1460901\";s:2:\"1x\";s:87:\"https://ps.w.org/yith-woocommerce-ajax-navigation/assets/banner-772x250.jpg?rev=1460901\";}s:11:\"banners_rtl\";a:0:{}}}}', 'no'),
(2412, '_transient_timeout_wc_outofstock_count', '1564756535', 'no'),
(2413, '_transient_wc_outofstock_count', '0', 'no'),
(1763, 'installer_repositories_with_theme', 'a:1:{i:0;s:7:\"toolset\";}', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1663, 'wp_installer_settings', 'eJzs/elyG1uWJoj+rjTLd0CjuuJIlQRnihLPkE1R0glGamCKVCjCysrYDsBJegiAI9wBUYyyNMvHqDLrNosXuXbN4lHyBe4r3DXuvfbgAHTiRGT0vVndmalDANu372GN3/pWcXJw8j/ak739k35Tzuu2WtRNVbb9b4uTffzg8KR/P59O8L/35L/HxaKg/6Zfwg+rcf/b9uTgKX91sLgrB/d1M543ZdsOpsvJoppUs9tlMRnMJ8vbaobfhnFmxbSkf+6e9D9evHndG/Su7sreR/jpBf6098b8tHfhfopznTf1eDlaDNwYPATN46S/bCb0xScn/bvFYt6e7OzgzLbr5hb/vg/fhhctrz+VD+31tJgVt+W0nC2u5XcHe+nvdorRqF7OFjv4w3ZH59Eu5/O6WVwvittW1qg62TWLt3KCB88yD7qpm+W03VnU82o0gHHpE3jgv/xLewJLPC9Gn2C+9LQDfNr+4cGTJ08O4D+Pg/1wO9c1jz14zXHZjppqvqhqXtpd+DluwqieziflouzJ47Z6t3U97sHcesXsoWc3tYcrstWrZqPJcgx/6hWTtg6/8bGuz+rptGxGZU9WbBsfB69fTWF0XfgnuYW/nw9G9WwB+7OznE/qYtzu7O/uHe7sPqXvDCb1bb032N3bns9od49O+nCCyqb/Le4EDIpr1sDP9Q9P3fHRRYQXh6kEJxVmNRs90JbGW4nfDY7mqXw3u6RwuGVW86YalTiJvaNn+N1jPD5D9+XrxcOcPz7e23uKx3Q/84XrRfllsW4W8cjV7KbW3/y2LJrJQ89+Tnfiae5h5e+X1ediQotHa/DkeH8vPMNHuT37x2I8HizqwahoFt/zAd37RTGdfztcPlzP6vvvaZCnKHNm5T0cGFzo3ZP/wUd8Ob9tinFp/ggvxKKDL9kx3zKY9RO+yRlRM5q2tJj4rQORTO2igU8Hi6aYtZOCXx0uEH7lWIWX/2zgJQN+7UC2hL42LcdVEQ90KLLlvq5HctyDOeF3jvA7cBjgDT9Xiwe4UNM2+dITmRGe+wKkHH5rcJx87Vjebbgcjx/SJcCvPOWjV4xupvTfdPCO5CWKyWRQTfE20krRgu7pCxbVZHQHn+LDB/dz+saenCG4km1ZJw/b2+en4dHhxT/gXYbvf67Ke/7boazjoq4nbbkYTOEqTkpZ7Ibvhxxi2bRq9OlhAM/5xAM8EdlLn8I2gxL4DOKRxMm4vp+RjNCzA3/eexrdbfzNfv5qP4ku1dmby83v9fHKa/3keO/Z+lvdNYG/0JWWt/j667z7H9f5P67zv891jlX1ECyArvt8HF2n5/DdzS/0/roLvYGa7pzB39iNPnr2M97ova+80Svv4f4G9/Bg/d05jO7OUffdebLu6hyvvjlPo4vzLHNvdtfcm64L8C//Aq9ytLcLZy+2+FF5pEv8UxXc/z/6ArTCsr5P0vU9zkieeIG/QuIcyQIPi7Ya9cgF/lw2LXxqljhYPHxg+ze2XJmDSpGCVMmvFwf6PbOgeA9paXLHFh15eEZ/A0mTP+N4H/iMN3C+abq9xbKZtSYMYcfa7p0veiA7297CbZwRVz1c7gYuNNyMMRz+SX3f+s/hPsh26BaO7orZbSknZP/46REs2nd3hz8cbu9vH2/vfbcD//7u7uCHV2UBsypb+MPBD3//d98tJ/C/e/D/vptUP5xPYU8+l+PeuJyXszF6gb3RXTn61BuWi/uynPXG1c1Nifuo56vt1TfyujAmjBGMNiEjCQYcLdtFPe1NqmFTNA90HM/qyaQcLWgM/O9y1i4bfrPpHN5xCCu1eOjdV4u7Xg1r1ESP+W6H5v7dsMH/ja9Wfel4L/wEFrF3Uyxgx8umgcfB9hTjKfzv+zt4r8urXtX2lvNxgbMdlje4i2fwv6J34pH6H2ajYnl7t+i9xLFOemeTArb3Gzxd1+9Yob4R8Qvb28CJxue19RTOR9GWLT+UHkeybQmLA/8cwT4/ZJ9YzGTaMhr9vgVLEX/d+3EJR2FYNrfBCfpdPeTVK3ps39KZKaoZ/wi+1t5tZ58G32vrSSmPhFdtcVEe4EeT8mYBJ6Tp3TSwnXhyX+Mr9CYFHurbeLkuy0Xvn5bNuGrv3Fd6kxres+wt6l5/9GnYz84AR5ZJ21dqyvmkGNGhojMDUmz0qeW31O8VQxi8ms1gknJH6ArxX/gH8Vt/oH2nh56D7dQb1l9wv3A7v2lVt/TgvsMV6JzupGoXPD7sTjW6690XbW9WL3pwp+YlGmEgAXEhUTDA9+17gbGyqJuuzUiuw3DI8gRHh2Ws4C0bvJay3cvFHazNHGN70Yin4zHebpGwvQ/vX7c4nQYNv4aWlQYFo5Xv5O1kWXbcbh5K1wZmoe9TyiIUC9i/IZxMEGEjkIqkxeF7JIJ/A/8Du3NT3S4bWoL43Xl4kLHFHH+3nFWjegy3565owPEB2YPz9uc+fmR2tJtqQr8MJiunDEaTw4SDtD0MTafyjKUj7h6I2zFKchR/JNZB1vXq0WjZgHTE5YO/0yEG5QMPLie073CNX7JMhMWdwQtld7wp8Vh8RjMJthMuy2gp5wT+/4/VDPYvfsGr4ks9q6dg6MNm8THAUwECqAcG/bLUSwyuAdx/eO0GjlwD4heHhLsMl2SeXl+eTtW2MABJnJd0Tnd6l3f1vCcSDoae4UEcg2EAEgzGLnrzul3k5Vggg50wq1DYwNgsmcQw6D3KHP3Lj2j3qfX+2OoCVXdPRNkdWmV36JXCf/InAncI7ZdrnPA1zuB6Wi6KaxAA1+C71Q0+l48NnhpWsvQrkQ34O5p5D39HgkN/tx3pKZyPaKh4MmadW959nFvdY+8IF0XuoOiNFv8E7hv4ERMwsmBXW1hUPiPtdu9tfb9F9jNfcLC7wIC4Lx5AwcEZcDfNrANsS/UFn0hKuHQyAH8KWga+Uc9AgNHTRViMt3tXd6Az78rJnK4U/GkJljh4q03Ru28wuUE3jZb4up6zpifxHE5BT0Zbwu3BBZcDh9vtlEYL/zlCI4Dc/e3cvh+tMXLs+85wnmjijGHj2hafUAzr5cLr5ItfXqiNk+xk1tb4T4G2Nu9AyiF5D7h3C720RQNuId13ENm5xanHY5JIYCbiVZ5MaGmDNeXzQnqM7RrZw2Q4WGl8N31t+CZZL2B+XhmN9DLUSObH4aWEuwoPnzoD8bd10bLCfVv3zmA2tzXYe89x3o9Y9D8OB31fgqnx2R9/FFS6/noM6dxZ47egpeqBQqarQsvsBGs66cPdQ5E4qNTgdsJdAiE04ulV7gmwUf142n1ZZlLn5QyXepw9fodqZdNxkHeDEwQq9R4E7C0u1QjuCFypcT2D/8XWLj4UPA2wDUTE36AEKOHJ+afkn0Ha7YtbvPS4wTbD6ixBsVVtMZ+XBT2crDgyS0MRknvyQfxkb/oYmQ628LvXr0+vzt+9RYsaFQMooIo90LNfnr4/Pbt6+b53+fIq+5D9Te6wU8RqfeDYz8kCYEMKBPOjvjMO+o+zxt/mt7m3qKYlCod65mR/b/CL28W3PTBw8UC2fB7dKSLhewcHpujdLeGD2XI6xDt/w2qC7IHsRS/kMo9q2Qurfem5Vat7VfKi0gZUDdw6nGZ20EQcLWe4FGK+tA/tAm4wT2rdO4bjByYROJ/ow9a9W7D68Q+hHQNPwjdHM8lZMT20jPlK8yzyi5LM30qqNy4YD5//oWjGdOjau/oeh6WFu+LIGPgqD7CNrbrneKPBmIQZ5p6KzyEbSi4mPfxF1f5+GU3ykuUXXWSww+tmWoAFKFMOnKeiCc4CB7DJhmhjoTh1yxo6WN7x8v4UOB7rl41sF3EC0dJSt1ut8KzYICsbPVZcx4Ytn3F5U4DB678MJ5INo7xcNLGHNQ66M4CKiTOARBvTcWTZhnOQuF8FUgfuS95v+tlPTZWcFnneL9EXUKtiVLK6EC8Pdr8RBw0cVx64GJNNB4vf4D6zQtPljAd/D8s9ApmmJ3I0uXbBqetWryZ7nA3r0tZPoxWfRFwcEvU0Jfjlcp44wk4ZF6gcYVdRFsVREzoyYOov9Ep8rnBqMDKqfR/tunz5Tm7amg1SrTUuB+3DbAROwQy2xF+60/FnvFHjFQZKcOJ2NzcEcUl/BP0ER2mWboHKOBfGsvJAZe+svMdIESiYdoF6PTIa7dOel7NbeJiLVgdf+dDifSvI+r5Hk+AzP5MuvLu9uPeg3tzlW87GYkzCVJykZsH9YwmTgq2bU6g9UCHhQRZV+IvZsJ1/u5lKRPPsvmhm7rYsmgdx8Mdwf3nj+telGmXXo7bts44JX/tFTTcPzJL5pADbf8ZT82+oQRzMalmjiXUenWO5wjkhOMabMiIrcdmyWw4eP4jcR2gV9OZ3894xmjY5izS4zywAyVmUazbGiPn1HLcbJiJWYlNy0Bd/O52DgSzxBfgNRgn4GsLi/f3f0YXK6rsXbs4nbrInYE/PJE8NR64EgYNxr8kELG+WL/AbfEvQafytVkXQzRLvQMaUnyUBvmrGxtpnMA8ljgaqAv6k1xF3+mZS3KJniedoWPLLwRPhvGZ3IPQXjEj2kRAaFiNSLArDX4wotoq/QBPM/0iUOM3g7/+uW4/H45kZ4CE+2j7Y/mL9aljH2+spSr9rMLK6LIObggL5vfcvL6/IJS7bhbgRXgIGthqJTnDaOe6C7gWKy7xGxa+674VT8Atwgcnl3ms8fCYZoboYb+IpqFKaQVmM8eT/umox89A5Xt0b9F5iltJdOmdtvAKzBhZtjLYdyo/P8C3WDigGjVwG4bXaGDEasCDhKaPCfIPw2BXcH4yE5Pd0uLy1kRLJL4gNa9QinleK36AXzUHRbAQka+3hRZoTClVcxYcBeeDs5PKNDwf7bx6u2pb1f++9ATOu9w1ZeN+wjwCTpaARCvJpMef0zthlSOgzTPe6oIBZ3A2s4xmcQ/jj2YvTq1M8R795ff7qFZndHf5G5+U4/VyMiyjRIAmEMtxyY5dtHklA923xQHdI7D6JuINvQPYYHqz3V6/drYjm/0FyNfjYi+Xsd8WwMkYyiKjYopZ4Jx4zCcn7AAxKMz6YMx8eWAQx1YqjZ3KNS7SiMZOVtxleY3SdjC16I1QYy4aiwhhGE22tgbQw7GMsmb3U289teRLTdZIan4KvJtvGqUaSWPhXOKY31Rd9Lg1/JrIDpE89WuJF5D3EoJsEyGwYxsff6RXgjeEJBQgIvBzlrOXFwjs+bMDboxUfBg9UR6fo/e6flyWsp8k8OtmjP3YxaxZ2vUc3aKT0XDIOTOWSou+SlZMhJQwBUhZO7vjx+juEZ/hWsmR68VWZcijHHTRWZDOMwwbvZc2ktxTF0mC3bL4PHHNaSUAQVr+ZU9U5VRd0H5tExvOywBTd82VF0eEoJ7h2TDLobyf1EFEYmqGd1G2UjAMlcINxcZJqHHkMhr5c1PM5JxMxQuV9/DA+iRdyaGI3cdyyc5pupcAcBp2F+or87inmiTipyytihZGchdBaiFS7fX/G0jiLit0s8BKXE8wSq9Wq8XE5I8Fvv2lXbgBoH3yvX14hZANtr8IIy25nvpiBjzSCh9g7b1WxiJVxkAPt47nto4mI4cYp2Isdj2K7MFDVM0yMFaO7KdviJEbvKvAvUE3hTPBEd6yoavhqQVYrZ0dLzjPYGZMGeF58wlvb5x9VKHPbalwOi6aflZEHq+ILnSt4uShQgI/BucTD+FYOI06IbVo5trS1ElxkTWZCfTPJZoZjOLPYnY71Iuf0/ZuSwkJi1ZInjcbprmTHihEM7fTs/Vwu0byYlVkbtYiS+Bq9IpvNyO3QP+88BafBlUUkzzccLaFAR7A8xpS2JoEuXucDjXno7A0j3uzx2tCCiRwM2Q6dWP+snuK/L+t61u8tImszOGL72SPWhTIZFqNPcKhHZZTltB7HBsnR6BXwRESCnd7Kp00tHk5TqOTIhaZjgiuJXFw60RgalRAEidBq9vDm7CWrZHXUYOOvyi8LsNfwouYe4LJDPPLF81B/SOJ4WDrgDR+bGUeN4fyvwkUEaxVGIFDYgN0BDnP5BdwjlFjmwbmJslqXTC8CxnAYTS+IPNcj0toQajXLJJ5y6XPcTVzRql62OzNcN5Aj1czbV/mxgnO4KpQaXIlfXdqFMZl1tDr83YQVHsn1LLvCaXtfE04r5BIGCAsnbVBRIyw3RuEEiWjNS4rY6Yz8sQ6XUBw4o9nhMgACPMoYPyYnrXc+JcM1J5VeaoLY30HrIKICHOHNET0vKCSHBWnAkC3wNOWSPfqyaBrf8KKy5SxAv2Wb4FQqnilJ4dkgADsJcIWyYIGE6Er1M54BnvenP8olhyVYDjEM+gdSXA7KoSjD/EAyc3xwWxKYj6KBZCMthwMsisSwQ+ScO+vJa/714zu8DWnfoUhAdjv4ieMadZK4OiEaTKKQtHoES6Xd+1xXYFrAHut1QGHXiGkuL74Fj8L/mJNoqRYaTclromnVhunXUT1ZTklFvlpiUFg3BiO4DIXMvblNa/pgEkx0XsMJjI7SmT3iJ+AMsTH95h07/S5M3b91+dAelhTAeqHVnHsRrxF88iCXgqdQBZ5a1RYo28AMvpN8iIAP7Y59RfLVhWF/VXwuxOPjmCkPzEEYTDKIxaFPdEv2TYd065YQulj/vKwXnM64WE4mv6f/klsWwM6y4+IVRsPXiCy4BODo/q5yMVmd2AoQYKetiDkS8vd89pCXwBx7xLWgH4SnVxyeJYJLO++cHmGHMgkUa4K8uGMLza+ERO1xyTrSbBrZzz/Xj5Q15xygUnNFXlGYKGBkLpIEFQtjtS1OCBFv1+cCPPwxpU7QEtfDRtl+Cu5wmEhjun1NaJgdUVcMcSiwGWgnKkIqDgH3efvSlGPkWbkYFUj0acsY1MwJ/YrQVVfy9LkEYd5bh1MEtN+ecc1ozVmJi4RPsQ4qKu/Z7QTRwCy55e31j/kZvS/RIOP3/a6c/rA9rb/bgf/LYAK8sh8+NQhrNik8cBkxFnCPYKjfLSkkWi1aAz3GgZafaJyutVUxLEhOe0I/K5pEXp0sRjqeYSJ8xZL6Y8Fb5z1PDj/xfgX2MiLt6VgHuCc3rw5oxrJVqwzPJiV/yy/zsqkwUoZjCpQRy4fAdMmGiX28gS1V2MgAsyYmQHyyNohXe+OKR46CA84L3+yQuu1HmM0A0Zh+WZvyBpTCHS4b3nFaQyPRVq2lRikDyUaWrMs0tHolNGhJbpRLyEqQT2PMNpRC6YqeYMW7xLNJzDufSo4R56EIzd/zhal5MZfiUzi6MFxOPg3GYCbxirjZtStXPXKq2bbHtEYrxiyiR9N360SABtvJo4mEEQXX9zPrayhv3ahwY9oKjcDfUzBYLQgW8wxbQGdCAy+UJHbAbTYfi0lFRRsFf1LPSskFDB9om1dEMyhYJ9ajxx2KHqBJeLFShfhc44Ptbj/t8sF6ARRQg+kzCTnX6iWVScnBTVmOSQPR1SW07nLeKN6YjjEdYUYYw37OegfbT9EZ9BE+m4OhrDCOddqAvBnFFSFi8PH/t96DNWdMYEe4HwP2zK3Oc1UWPhSLYhM/MW8bhSVx/NNfnf6GoLQWscbw/YUE1yjHLfgnh+VhCQ+G3CIzqAPJPMfqUbbb+ajN5xMXeUJ5c00pFW/ds1MWx0/tQqBc9KgQLzcJCMzWX7duD6RAEjnxwUUr4V7pCXG5pC7LjlGlFhm7UkqPEdyIh+7WwaQCry4Tq7E3ayRxl/4v63t8e50VGE0BOLxTZt2yVBZlCh5dNSZFGJwKVJZgg1U3PmCIp6qPpvuyTRYWEVxsXFCdSmiVEBy6nCP053WsGiOjQ11I8Zmp8FcXyWFjRIB0jaELswgXJvB+XS6NDjXazkHCT12qBv5etuhYU2SCRQKGxu5QuEem+ox0S6gGaVEYD0v7/252dVf+WF9SJgiLq8oGAVNr98xkkThZi9fB5TXRGQVLjaGVG+8/1fGgE+tm+8urq4vrX767vNKYH+4jX039AdE9LZtJx9rnYXN8g+D4XFdjvi5aRsmPxk9UrMFXttiNUDnH90ukW9FmjASLpGqWPsMILnCQqOf6AAbqhiWILifp1oKqggYlTORRuX27LQCyObg6d0UbFurwbvDooj0XYBw/Ti6JURS9Yc32C0dSC75xbTUFF+6mQgjDXQWKNheQ390+7lKHK4ClIupcbtegbfy5FXSGN7ExeL+8vRMEOa0QHDg8iUldSKrXMrPJubcS0Ph40btcIizhDGycDA5ULMAkR6A41V8jI4ADfbndu0O4P+P0+Clwg3ENn6yDzXIcrwZTupqWLh3QxoENwonATZYbUwUobjf5Sfm5gOPO1cRihrLTznE4tlg2jPTDSkmWUMTkbYP3CnPLmuCVqgOQCHB1R73zF53rebYSABO9WHprpnxjsMZWMooBTH59teSfl13IaXM2f+4KY8gyhMXWbzsfjWUN/uL8RSibyOgLra10HfnNVG39flmNPhElC4lOdpLsgXEOUxVBVnOrFp1Ik5CKrVkEEWipGoVNL3yB+ndw3I/hwHegtp2DgEWFWGcq0Th2FLSuia4NrtTp1ct4pLe2Wg62uOH40PCBKnn8ue/IwQ2xxJCj0N4TLb8UCHTRkJIsG0agC5wUiCGCKOZHdP4no2PFGzBRGqdNbghwMRuQwVu1sKQLA0oNZO6Tr0gDUWJDELFkOFBUSJMajMTpVTe9prhn6INPCrRa253L2HFkA4kRsNZQQkIw63p2+wNBcyfttV6ka6w6gTnKx5r2uKvrT0kgmHBfc5fXIVMnEnNuqpR199bqpnkninM5ox3OJwXhBRnBKaTcSDhdKdQgUccJGkJFlRiErRuNTylKirR7bqhQv6jDa5MNTXmL6VLhCqg/VS65MioQaeuKRVhy4x9wqGz6xo7LUEIRdJqM+YpUQKdTGO2RDSOTqibSmILmE8Z3YCQ0alFQ8buQY68Oeoo1cNrC50YCRPfR7q4VwQ6uz7YQXJeW5xdw1mARdVpTZYK7Aax2YWMtdazluTybgGjB8fymdYFrE3LctKwiLtGcCnAyjAcGEw1zM+tOhtXfdhRYUgYrk3qu2ziX6abESA5x47EYgkB+vZvljONxcLuuUfBcDx8ePe6nxhNyfIC0WtzXzScFa3fhgwNJwzO/5qTmNeZkpMb5WsAMTvL86Y+Fx2c50yFKisLvCZDJAMZIqCCvjGTAhBZmklOC6EuRIqI0AItUPOXDkjAbGvmk2BxnCthYajDP3cTX+G/XnVA1gNRQJNc83ZEAHma/X5bLMkrFojyTZYwr+BTehyeCLNMm2GxmE/OqxORPHXRlUf/pj/obV6DlfrJWmvCNnJNk1hySDrd9t+AITaDMykjUuDdVhCnYawUH1buETOgWt8vhtFpITBxXgTfvjLkGqQKg7R1r6o9W3adbOMzbWbXQWVOjUDFvjs4p6DBlvx5WIg7Ui+GYxrp2dEdowI5y8KjcKE/EgidxOl9ocIxxJn4GGqci6L77DebqJjDX8YOAIbpyjVYGpdW2r/1CdO8akyxwzL/iihwM3xKpi3BCcHSeVwSX1C+wJhVXZRnsLhmzm004rkjz22Iy0XKryYUOwv8hZKUjO2EzG4oy8zaT2A/3Edwszo8M2dNh10MzwnzFqK4g0Nv4ougGtC2H/qhGv40Gv+AFiHLjrQtMadTYlQEtmgfm5ZHiKZaFpmyvoxYDTLhPmBL4cK4HQ/bU+wWFTUhHGN5u+KY17BwbFPsVM6nEiZmj0Gm4kaVb84AwMs2cAiwkguui80ekHUrQrLkLKhBfEPeG6+cMSYOkHNscT8blHZjskjNiUgPKyfvFcofTI5VcemnM6CoH/+h0PSKxiZJkIp6Ztez4mhC6H3kNWdWjWCsXWFzFoWYKxW4kJXHd5NSbfKbFnueMQRaDqkP0BFzzQB2KKSMdvdzwYKYEgeaqlqJdeV2BRuElpSQ3I4Zlvn1HWmSz3H3NlbrQEFggFL4iBgZkUJgm6iXaF7Jw/IlcishyVWfidX2Gw/LxYnD2+jzr8AqfjCyUd3rlDxjes9m31Li1FQo+vIPXbpUoc4l7RBmgYdvhTN7AKtFg8PWAjS/0NPGuDPLoKwpQ6uux69XxbklOMxs8FfgCrQPGmlRcxrlb2GNK8H9FbtgiBtjJUSCQva9oUSnUT6A/zHWiR8580d+WjDSJSWk8Dm6sHIWp1BcLWFOoMAE8RYcdI5+9ex+AdDc2c2jPO8wlXtTsaT7cJHzznILdix5Dk+DxqRZlLj0xvTkcBYpooUAScqi+2rU328tIbIsagpWANZ2NByD9J+OmnHXzJEWJRktqo+rfyMZzLJsG36/38guo2QYWcm9vVWWChs9ILMkWOyCX7JeLIbBGYQvSoz18eafgPjiMsia9I8JZpLKirvgJamtiGFFRZMRyNBnnmUXk/FkoyLBeLCblrNRcr2jvAHx0E8mSNnfI8rU4QYLfUpsI5IghCEK9wWl47xsnC2urpMNKOro34yXX2MZIl1yKMTout1ioDTcNptIsKI1rOUHS3XVBbFt05eyM8xeuUEajEpJqpnutz1ifoScmhfv59S3ROWD2cVmND/tEo+PwA4c+n0MjvaFFpuv7jQv4Dop2YEWgBEkKSd92XGDd3HwVTHqZA/8mABAjVRWzYGIG1QdplDQBLlNbbukWgvcqWSKqVi0oqI5oh2VDfzkTbFjTieiUOTQZoaKZEmP1pgkTBfzorqtjHtzifsfl7XfI/VyC+AR2anFXcyzjmoqtrt+Us+X1OdgNJyfX14uaYxSPHvemcDlkxbwtTySP8TqwRxGUzrrMhgd1MI+Gix7r7cK8P51/yjOQHeuL/mN4SXBOVhL+dMdzrwjqMCokYuWJE9BMoypv4UC01zwmozmXXyVnwqd5Ai9T81a/a9VjmBbzrGjbqAomerEMuKUNWQacTrEY68S8NlSq7hoVgc2EoDQGZZgz7H6WOR4oNRB6QTeJoXFFc7t0ZRKZg/PVZQcblRycSaK96EIXNfUkOdlZWBFJLwR+duE+uV5ZCheqLhhtODspiPfBNQLQkDsBErz6AwbHKPZM6JSRQEbcdM4sx60yG6gYwW/s+HgPOBA7rB2NeYdxWniL2ENwjER9RBcOwBot+/TkK/KzcBUya4ZZCjqMKbEzzVZSZFIAy7ePAaIfL3DBbibL9o7XlAg+wTWblCFGlKumRLC4hJuQrGpEUkMBB7gYcupwDe8KLCEAvTCZMJfVUjM7wmTw1YYlzPsN0R5eYoAdGRAZJhC70ML4zbWPvEt4QKRfwTiRJvg+kZPl4hxSM+4LxBQyyLHwYijfyOUN2fApZ2AZwAR3xiX/g7Y2SoggMkEMFrLJyRocFWDBg0m42jY++zpE6loo8V21CA3HfFTVBYDIWwjWLFckZy3F/lWQ6ev3xk09HzsmdOM8YebZ0g+bSWtVpGIyNRzA6jzMPhURuUVmuMxYgcUMByAiyMgMspwZg0wAp5w9o4IqeBnGorYZGewLoCkwAxb8LdKBxFgJjocZzJunSYmPrrYEXH18hjDAJ6yWo6icmzCWYl0yq40DWFuSQHH37yo46rkXYSPNEW5ExRDM25KDiYrVvcJxowio6DEqT4lYEiTz01bTioCpYGjklqCz0Nyb4Qfbz7a/oMXJAqXzLVNT1EFwCd4obFdprDcjNFRaeOCQ0fxIc0ntkEgRbX8BsUW5I4sFdb+jCjEajsQw3snM84xyKoblhIDHHLJTQmLlws6eVl4AQhOR16bQVDyZfNYTGspVTrnnw4b/hVQdcFwGZM2TKJqi4epm3JFop2QoXTaQSxzysMUNkdtUOR5gBjesrJaweLLfFV+EvJmzrmjCszbFjIS42C6S2hUeqG7MyV/9RCGr4Rwg1o5K0ELjRTZHBLeWmPYDniNNjGNCM/Mo76E5UqFMSi/gG+mCOuBxcLVF/iho/4o0Cub8l6+AydpTbAKXoAAFIuWZkVMYR8LSxpYROYgOOtRftqHqg93yblNU+JcW/fmjtUFewAGrnMGv+8qnRrcRS7G5wMNsDvXA4b9KRG+H+kfttMtZU7Xl9vxuntkr4rSKbqnESCUy6nFnnXV1BVpVPj+dr1zoIkoKR/1xiVLXFWekp0NA+ZXxZSX1h0yqK/Nxgap0RNpuZHKarj8XYAJg5GBacw3/jFwxd42zFs2SClfFrDUMlcgO4oIiJUgVcPeZozfJAOBXjzEkk98iiysWa8zWiYl2FhJHV2S3JqIaliyymhQ1TZIGlAd2shmAghHCw1yGx4IcSdWM2T1dIz0vbbmz9+5QdIu08t18PGKfvw4eTv8UfArna2lsxnn4qLcPv4LlJRGyuJQs0Xk2rpAkuHVYX3BJFy5FSMW45BXWhs80HmwfbB/aJ3h3Es7kCwFjVHLLA2ZKbBW3nDtaykGC3AgW52BTxL11ToNGW+/mPpBMdXdRI6f18Hk7tGsWccM/CNYwjUNaVxdeE8aYPDD7MvyVCL/03kVDnfmWH2cv3tI2s/OPHdLIZ4V/UMBZQAKaMRKyZRKxnXt8gbHQJFRaz4QnaD/7XiBiljMs4aWwPxuObB5HZq2//s7R4/YwlI/Dq9Gn2JMYGv2gALW/Lc/tAhgbZryWibJsQN5I9xtwub/p1M0uGuLHy6luOiTY2+z4pC/JTvkv6XDW5y5zC+K/lBZ0R8iZI93dEKWlTTH3d/eeDXafDPaOe7vH2Avz6EnY6fLwONfpUvsmff9k9+nTX8gsvjfP34MRMC0L9ufgfk494qV55T58IJp/kP0G/hRbkjSwS4OmnNemA+fTkz7mUV0fu7X96aiNbL2cy5pgh07tUod9OKl7DlLbaNdMbF6JrZ6uCaYubUnpuVhrtyhmrqOw+RYJbe2rd372+vry/OrlBRiCl9e/fvn+8vzdW9ecFHMd17kmqf+ypgdvposf9mBOif3iFn6rBs327zs4eMr9+zKkgcrJJk34AlYex+yuFIEsRidCX4k3sLJ5GvXsxD1B0CpIgO1pLRytKPU2moa3N5DfIIgfI+AYuUXgPLXcZhBh0BOEjzzUSzN7tJkGmlYa8w986TaS3ktlpAC6XLIcwYBYooACeUus8NsJgvrxOxgIwOQ/vEyuO+He7tG+dCfc397b3T766vaENvYkwGcmkEAXQIxqgg5Kij5fi7xRz8AgXUJg4X/AZoH/0Lt645oyUYMlrsxEvhUuuhCLU0OIqRhlKn/a5mshUWAsMAGZJeqh7MAVXFdq0FTxGkjIniqzoqGRd5O8hLbFGb14Tt8NmtzAC3D/HIKbIpu4+gxNidGVTO+uHuKryM5uJrzMaAMy1loC3pKKciObsLV9a2PSXfXk3Pe4Y64jkHUNo6QCXxbT0cQ5fpGqu19ilB2g68X8o1POtY3u6tYFopyxQ0dSTcF1LbtiTsnkzm5LQiR+QZzR17xkYpPRNH8a9SUrW7g7ExdPzfVa/F097OLvCLCrHWmbX8HPmZzc8mXOBnpVHaUqI4HWFf1EfcrU2jJtCHxJWZfo7CbRweW/KwvbSiEzSC7hSfuwaWbcLV1Acsd+FAoRhEYq2cL+0RF1O+xgjrsJ+rUl5A4sSQT8pFCiYqr5K05sP0RfUCtxo5Xvux4B06olv7ock1RAq7TvmRBcLGfLGOGyuiCbzlhQ5Nd1k9YxdnoBFJXDAptOk73HDWa4up9hsZB623IcdjX41csXmhlFu+CA6kRBIIgOiUcXICZRh8KIzThGzavFYHARLfJpSgivkCA9GWn5xd1de2ijQjvK4bWLazyv1+a0XbcFAp3/UP7pj5kaO/PQZ3pRVvOZU1Dkp/NaBw/Mn6D4DHEpaA/bDjVCPkrGzLApC6Z8s89iACoeOF5sH2mRT4KbqOVv2eltBCq4sFAmVv9BAV1wTW0dI7c41AVsS8RPB+arRl6+Iq3670HGxWaK9Cax/BRabSR5h8wSP7XMOcEJSOGt7hQ6fDJFKZgGUEy91zhn6YbSD1idTOrT7wDp8zarTQJtJ6Xk8rCw4W5M7sEl9AgMpor0PtcWD+svWzDmcubg4GtmH7Yzjh4i0UUMC2eiUMi9sm5tcltx/BUBthxTFSYrlKPKZF1QXIu9iaYM1Rdc/vNrBeauoznQRh8z6an+ZaGPYtAe8+zXS+aQqheZVg34ck82tAFuEL5G4CZtE6wJsS+LQTcnZ75/CqYDGCDiLDgqZf3THxV8nS3AxMCU69YXNB9nLX/n3VlFF/k+RrdcJr6uNsKbmnI84Vm3txO9XX/647rz2WFuPf1puHyEBYT0bSzEHZYWzfMs6NV1U8JwfY3hui+m4Z4PXnYC6aPWlS6Eubgr104r18RAGu9WRBqVLQQNgLMehm+SAYH851cj9mGuX6M3bOMBspvxE4Pm59Joiirq61nElodbj5gyF+QkSFGRsxKfboI5lsRI0F94Le7W9+vAnj4gG5bI42CacTt3el40DtvtYJO0hgibnObwtDjxTb2GuOwMk7WavVOYZGtMV8U0MlcT3t/W9f4TorcZc7q5yufc9DaFcVLSKsErE5D1oqk+F6OH6zOGF/VpYVhJ+W50FFThcnxfS9sTyGU6ra9gI7e1OC5LTCtJYfBOX9MCj1B7u0QdR5b4znjREIiMLXFC1JlvOd6NbbjmSzT3i1F5V38lFXKCeGBbToFhMg3lxEIiDcEHu4J9n6nPIoXL7noA3E3kMl9UTD1GtlgrmWdCYLmYf0vwPV/rTlHDHY5tBVjLCzQKn4d1BGGpZaG29AQs8KgpRIhYIrw9owSG1a3prhrzTGS1VNAd861BO7RhzlNrqwSMJSrBpyG5sE3ZVEC33nI+hCoBIxU89VJdge9Mo6HS2Ddsk93Tv5t7X84+V6Bh8A3XwN8l1xv0ajm7uOLMz13hic3W8AR1YpnoN77QC8dSfKh2zsrS4maq4FVMqTk3svBZYXYKUA/q8sJauRg6Q5qCsKuRIMdrFMZXJxv9kc0wManz2roS3FIqZPfzXEwLvxdBO+c4c9jBQ+K2nW2XC2ZU8NcvD7WQyZ4x+lR+xFuqZUrU1ovLvlptkF6FKVxd3017eAeGkN1JvV7EIAVzGqAQfvTmw+M4WrKB6LTPcb0Zwv1cZ243ZQ6ryoFL1bU5FGpETjNvyoGz1jOVWQIqhYWYyzqIOyKBezCGxETdnmu2aK2jIEIzQiqbmW1R5AgFtxPjwxpMwFHdcB4rfMabYmyLS4klgivQXPYiU8kZHI9VdkXWVjRnmtgINjnSrfPhkl7VEb+ZxKOzU/0KW4MjX4Kd46V3VWSmfOMR3f2gIOQxhX4FCsOI7wiJ0xEAorClOm8+IKg3KdI6Engx8QnqOAfLPGVpKqInrbMzN5IbGLhsm/QyYH5c+b0uOWmp9ev+tUZQdDENLWgRMGph/ZhFn9jPwHtGv7Ne3JXeFVcrT+vjzeKd+Yf8pR6Rc57DzqwtuEvU+TlHk24v3VqZ4Psk4VFx1iN1D625PEKLSjNmsZh6mutjvLjV/KYOsmoZBzJjvA0Fb9ahzPSoqQmQv96bG7AI+VGTwXZk9hlP1cvr8GdJhkxj+2HIAC/4eGitOG+ZmGVq0bVFi1pB00Soi7ESgc65juqaZp6Y6Mg6QyowCggDbeD69/Nrk3teNhMN89D0uhFkjSWAW71aoVxyyh1FzhlWcUu4c00Nt6BySVApgfZN0SLGJyCS5LXiKNKKIOeKSnG269mWx7xMRcEw1+CObm3Q+bNrSPEbHPCRIfIpFbnRM0/UIl0XQRIbsEfljAqobiUyEcBbjZupn9O+j4cnJ2CHYMAiGxUNaiMDL43l7RaJsPvalUooE/WWdrYtg2pGDwpzdAvZ52pc2eWbqrC2+VnijeFEPnAVlEHyZAeHnSV6JnyEREk8i4PXHqaXHYqGALsI77XdO181Pe6/MyaWBFWnhQ53R5ydbI6V8QpYC1WFXpxRkJKNmzoUJ3gxqgwoJrsMvvopZgS2byK8bO7Gml7yErD9hQvX5gSSQXpkMAUW/uAMDfFuW4OfJT2JqBrSb4GAXXF++NQK0xJCDwXf4rx0l0JSG62gr1PNg5L2ZUYXMcTSQEX4tLxuahiZWHmQqqWhuiZmt2Og6bQeSoXAtHycu/f7K+/9SpoMG4JQPnMOYquDmWqdSiMJ97bSHSFLZMZp8qMwtto6PSOU0i+GQTUcSgiwVmZcEPsx6P7KHRlz6kMyYC4Xs5wtuF766+T7+Qw9g1Zs3UK6cWkSFMm44sD4IyFh/0IXH/NH+O/HORGvjF6SomiquhHuhI4U3xD2pswi+Bd3lYAAHjySbKbLkDsue1+lJjrx+xrLskfIF76QaDONRK21lcCXf3kR7ruzLNWq0uagDvfIYf+4raAMDAJdUcyVlAS3C8I54raMPim40VnSb955SRUYv11rEMbvnMBBigFC7K/U9V2DZkxDS7imT5Ma47AknJpGRWXGHVjnpyd9xUv+eWDnZxHY+ekasPOzfQd2thP4K6GdV0N5fyaoszy7C+KsH68FMx/LZM0sB74pUxbQjKuVx9DFoOZ1g+eBzbswxEe8nQ+YeR5NKsqZYxFExVEfUAuz8h6sN42oBF4t+vFb/FsKjsNqOjlVyd+GJUXlUE53wQEFvHxXTuYtxwboKrbSskpGC4iEYWQaEuUl8vtiQmOGFs5ECPaoC7n+BITQFtEZEnBR8uBTnmmDBXdgJt2SlSOyAyfxb//6v8J+BszVvJ2FLR89OdxX2PJTr73jNCOGBOH/cH/PSqw0Ca3+5s3rMKadhxJsCoimVpMeDPDARXAs6JCZEEuM+lg7d03xxj4YRHPXDK6N6G94I5pI2L0pGnRIERypduBvXp+/ekULe3b1knkux9wtFoHf1F4DiSO9HboR3voFtW+VEHOv/5ZAyWyV9XlLRTZfvem9KNq7YU29FqrRp+Uca43ZYOisVokCJiBz0EUiSx7LgBZlDCeLlZ4Dn5v2DDFv1flZMRPzBEPDyvJSmObp7CepRSYmrqNj5cfh/8vxOseJh+6w4P18oP3hU/VtczfKwCWAVFe3z0EeReVmVmhm9meVfmRqn7xMYK4fpL6+g8nQmrmihr0juwk5/McKcLbbq3bxMJFguhDxaMdt4dFucyhmgi6v50x2sQOiKKA3qm+sPFo5tgJ5olLaNeHpwBh1bOzaV/TG82QQFKCv1BA/6VldOI/13R9cDSs5e03piXBbX5S7srk32gQt/usBnUFMRiznC8FCaPlByrBMnRwcuBpJALgRVlyU7YjSXZZjy4fmSIYWS/gn+S9UIZ37tat7YAItm637+mi2bW1SK3MOSb2IuRInQOTmPGG0wFus9NAIIjHWMe5hTdPM7CO1C1j27Jqr6yXwnzsNlhCs0N2VOlPGXCR/YqntKoz5jLX2onVWmnPnBewYx0Dc7hP3TUusx66dNSsJZY7adDFWCI1Vp90i+6RXGjrhHgNdEKyz9J+5cGh4NB/RW1O4nL/LaBCC3ZRI48YZmglYftTDw//08XpZRxwPpxfnUtZMA6rmw7UjFg3m//bU83gY4uaAEZGQErPdTIp7fwQE58pb2O+1c+7ARXu5ejvG67dDtF8n49i9Hh2rDqkWS1MezFXlgiimL1L4ckGzVhdoFlvAl4FwA/vStbLUwowbxkOQJPB7OVTrgsyRbKJW4rle4dEMSPEwRKbt3ZK5L+Ume7u7u+uPgNtzZBdN0x6kY5v6hiVBMckLXZMZYYoFiudjV9XN9G1enUYNUmCRN6V3LQw6yrKec7H3arGxCpGY4MtTgIinX5f4FMmgmjq4zgZOgMaRiNWDBoNxxtb5PtLBKiUa+UnIxLgjT3AgSq61oYNhF+7SK19p+YQocznEju/p64+CucCW6rfy8tzFWVylOlJs1XDNJbzRRZWrEkozPHbrHKiE+8NFRWy23MJegvXIDXEf0ruWKcxL7a4Y1s31fjSUGPMkUixkMBIsqwnWMcI3sJb7KvXGjyIKJNAekirh4I8z6ISLYQNTYcEhfelv3PoKlzZa7D/98d7RjUWNzmbS6Yztjuwl/qraM4Frp41rviMAyGgCx/r7fjXDqqIB/qn/QysdJeBO4x9+4P5QeGWR3pNLe7fw9NLiDFZWSK6obHDdcjuuSq6fALsVRqPEr2uOWYvVxkbuurJOWophyd0e6h4jITLOUQZDS5ZEV70dmpqy/X7GDJBCt5yEaRDAZrzQJu/ys10ZDWkIYXffSOB+YJQajiPCyRobis8CZg75Bye9/n3BL2iPQd30s8d3s8oyx2PMVrX0QdNKWDyQHOzBPNLacjOmSHIdE6WQTrgfJQ4XU0LGJE+VFJCnfp11guguO0IrB7vzc41bKkhUqZB2ZfVqa3xmz+k3vXJaVMzZ5kyDGIH4i9mwnX+72uN7jY1cRmWDHYsZ+d5lfpZMRdxqTp0qmaqmzLYqyMpIYrQJG9lqAqbgRByfYIy/kCErkQS7Q5Z5EUOpXV0qlgsEtt8EzcI9c5zhtsk008RN3bgNnXuk1Fy0Chd1QDsvfEiBflG8OQufGWVL/Ndzcv84Nd7y5GvafqKIyjN+kqvYbW87IquvGdbQSud7N7Ulk3ahsyvsD2ZcTAjDi8KNUe/OeaL5JdvUXGynVLXoX49BYsyXZdqqdi6Hc4xwMOZyENQphzrF1/kK2Gms0a84Hsp+K8PFqjQAA9v9qUxUigLB08bfLi3KGF4rqBzuzlp1Xw9Cdc3wTK1Su5xOi8a9gD06z8MXsBJd0gM+JMRZdC4MM7geWXguJTpDA6carXOQ/vZKi6Nu19GVwZnIlpnIrqkBZ0+A9B4pZuqXJ2thtUyq+bo5FnyBQ5qckfa5A00YdEQmkRlK24LwG93AceQ4lmZ4V8jW9KS4FsJcaNMfg7feR0GxqENqXbEg7AheNj0ynn9hscuGDfdx5g48sUXcMfucLfUM3oLiuL5QKjKB39d4+cZVMalvwyOaZ6JLy8XbkvozJVSLQ1kxDAs9ovpGPE0LwV4O4Xn82T+w6zgYPigJJ1cXa9w0vxJfU0Pt+j4bd1Wbxb8uHtAaWONRXBUNQndEEkzrMVhAbBFwj2QmmWC1m53uplXRoZxfZZ2JRPWF8xKdUziZyWOL4EO3Ly4ECE+/Sm5msuNBPQEEVp767VX4zZdJdXMzwMQ5fPFPf3RAHPL1/vRHafVC+78obOI0Y5+eYkMhXIUlZVPZWI0jCXBpxKT2gD3bHI9LZJPIgOL+fXk5cT8jR+mEeMsUkmcdMMSFs6ZdH6iwexE2tQ1ItfHMrzZdiTDfyTXctflyOEFJmoq4DSpf5MAjNA+bhZDzRqb8hnrAd3nqtrukoYMpOCO8uq8ftN3zXOvsopUmnR19JvtvBBPh3Z12OXSoDvAh1Y1Rlt/FvVmT4AJuUCyfZvDuqtu7CfyP8za79X/GOsopOAtyXVHvfkO0chL0tNel9xakDXIpa7wAHWOdFUFrNZY3sbPcImsgQJUglGRIKnIwr+fLAKTcfUFpWQhopEuSDWlWCbof2z3Azc2fn68gEUj1T1TJzw0WfWe2dF8wntCUt3BgJwKD0WINwdJnD9DXFPinpB2+Kp1AGM44zUd3flV8Li65z7UWfFlSVHIb59p46/yliefn4PSbdiFzbMyjCYpFAsZoWCjw0EyT2bMucl62u/bZFGS/U8GdwwdpTO2K8zukivSoZvdfSc0sN0LGb7QhP+FW4NMs9ckCDKFskvNiFSb1lRmOJxt4fM4UQ5PWqjL408ffXp5//O2Prve5IGiKNuzMUM8y01mBi/0bYCpwT/uz2k3hO23k9b3ENtAjc4NR3JhcXIArNEhBqQKtpVC1r4ern4+90Ym3xqRV+a7FyTdtj3pAbawTLu/RxNBMvSszGCJar1wkw8CGadOXHOqD9Kt2ImW7XmNRQ+pgRpXMDCrOek2hKuYCKEIy3JOycA5Vxp3OJp+lIjQY9k3xKegY0A9Rbxk1q2K9Y0X7rhWFaf+ICWbY1pB6OlHDRUo+w06mpM/CifuejdZOdDndPme/5iVXGPdNWFiAGFjAR/2Ywl4h3fMT9IUE+ShPpIgbIVvNqOA4gYfocRcwpOAMuzKrN9G5rGEZqPQCwx9WOlFnmwfYv7Zmzus7IpZDMKjuO2mEnO2QK7UU8hEs1mvKKXLAgsn4u9bSMFJ/6QaEDbV4ENWBa/boN5eXj3uflxNk9yU9VUUPsYudqKMLLW1wxCmS+Chm3uqshSSLgA7yfccWbKiT9N6lZXA8kyu/p7lwhu0xTNkwheXQ/f6MhBhqpwcST09nH8XiV2OzYoV1/mcprCAOKsuiUBIGLKMTFqSVOAGlKfT4/mcdoYuQhF7K3K4uepxKbg3YMvT3zew6+w70L8Eh/PRuufjfei/Rj+pdECkMOSQC7w43pLU2Syj6YQVI9o8KsO5wDYSwhl415wGsLuH0YZYHk9F0fS0oO/UwHdaT3MV3GJLoCgpCmKMJ1y5R6Ph9TOOOaraiIYdxSskJ3lGXWFlakUdCmUmXjo9lOdf6Yg6X8WJ0XaFcGIVqT0l5KDtBpyfiW934rj2jEZjrJqjX0ZxpoxIxLfYwZ90DqDgh2qdMxLVAvlOb6KjD6vxq4pZgzHVI+w61yVbjy7e/Pn//7u2bl2+vEGrLVPnSVU1riLVuCStRljOFFsGShtK2/vLg8Cq+NTRV+PQjHakuXsHZYvLjHLeMz27pUc/cpr8mf4rD872bl7OYSlWNGIkkS5JLrDcFba9sAJR7HEV5TNmVc4DCchEqjaefr2wDdC+3wEUTGXpCBb4UImulsaizTLI2wfpp/9gUn3FRX9Vo8nEiV9getGxBkxxdgFeGNbqGcDBlSV5nXj/j2B1tmGwKrsO4atGtwS6SyvToGoHMscOMbKdHazAiW4sGs3hYGy7TAlWJfqKP7puUUUIh67AMS7AdxwP45aAdlTNsV6eUlwLYCG02npLIOrzjAgrTc5izTCLgpuTpGZdEpDhmvTttryAGwZW4VCY4Lt9U5KdqPAAFi4/9yvS4/hyjvzDnSBpfZSJAlHsgdrr7npjHUT1sZjXpaPvMYIsZMZLk4HWcv0BTbwzbQCHzMZVnplr2nwm0nlGzxkbH4rp7V5VAlpthwsx7QKGJ7poJx76NC9FRQG6l9Z/M0W4WjsvWT4AHcRcs40+1YsCKYp8iVwLhBipEd4C7VZafJkRMWLVb/gxQPFnPn0UUFu2nmFIuYTuM4IJue77WCLaRLQwd+XyGNlfmOFoYFtAqAMWAMlTiN29eD95fnOVEV+ZqlRjbMC0xhiWzJ4iwNd5htYjVCqfg1kIkBZdOV21aWcJ6U64Xluvi6XGVIt7H1ZEuH8Aj/BLGz4n+jmldJAyqbUlQ4nxVegW+z2U04yjTghVAmBmH73BdKUUxm6Z44M4ha3VPqjiSI8SaJIuf7nQZni8ncIjBuoEzfKlGA/sKmxkNxDm/mF7j1gec86ObPneWNua+NqwuZ4bPt7B5tzVHglww3KuWcw22VopxpFvcLfGGzbQt/KsgPfkhihZIBEwrVTsdvYSDlvHJSbYASwalkKilN3U0GsJ3yZ5JF9FSDC+i06PwHvKvZyHZHYfQzGOU2Mklg7ksQrDm7QLzKwbvmJ9D5B14V4ffzuJjMG//xsGtbL+P1H453JDb8ScQorg1UvK+HvIDL29X4tCTOJJlR5G866ijMpi6OMzhoSiFp1NsSrsoESBP4o+5YbJJFLO/dh39PSmIOp3tsG7zcAVXVUf1QLY7m5RK/yTCgme9vYOT3acnR/tf2Z3t2aEhLHDP/yvxFayv0v8b4yzYlwnTGVvffw3G4xbJV/abIVPBiiGzJAV7B3vYfQ354oImZ3zp8RD6O/mjNL3Ps9Ru935bLylKKRW+Gq+ZSp9vEE8iTTnbDFfBo0Jydf/Hu3u7WvbvYxDdxcf5Dj3sNrJZxHaMLwRXeK8QuzNHApVYINuUWEJEqOv8E9c7Sqjbss7cpnmxUCl4YSxrFhmUJJIvf/1jj4Oy69zI1THWOwaIExr+m9aVo4v+TM5ZV1OAw5SMgbkRTFakMMeF3oyR71zfTrG+JCWvg2+GHfXazfLH27bI/D7LOXif425q31A/Bo1O5kxupQXf3IHaDI06gyR19jU2cu7/uihMw6pgTUjXht3uva9gVOpUQjJpF/ubYoQ/RvdTSJco+8I2YOhjYEDV9w/JrNXBpojD93C4Rt6BJQ4hnrmnf4A3xD5uCdhjVcvbbmg/QahFmLnWaSIVTKoU8wBop2LDR5e6JJGDyhwd16DiMnj3r8EYGuimMaM6Ice8ieneSoyIDRRMIHRxOXHyDQ/Bh/evO3lQOaghEcDsO/40YCK7Sfx8RyVmRGMcqV2UUqVr7ugiPtKe55UHBst6GZfbqKdFJGVGXCHBw6RqxXL3izMq5ml+R5qKwdFHM7vXLIU4y0yIQ2QINUT4HUVehUafie22wihlVDWuO1p2u6c4RayrpeIis11kKWR3KkKwdbGehemHUJx4caoVKDj7akw+O4dbkWKXWmS7jUo2wGkn2gQMHif7iWosB5/QX2Z/RCItjtqo9cKRcXew8mklzPqxtyuj195fE9axEXgvn8zV7F9i+iWZjIbiszvRyQGSpGAEz8Ds5QQgNO2aMqE85RFOAXFrhJAkyvt+/GsMmfdpL/u35eK6pSa1146SoO+SYSw7iH9HcBcVt3jNvvzGbWYKWEKm0fN3Pmhc4ALNmOSxVonEOsdyh/Wsuq20aqwchxzy+Zgdyysro7liTg9DYGdx39qAoGDoOxU7fleTICSfUN509QmNOKfZg5aDYNZJsCGuoywzWVhqlf+UsbTcrXJcgB6zlaFlJOKbkJFi3bWKBZUakHXSMzM4M5sWRsXAtG/OUMtTH2DtQubTzxgRw4NNG3UtfsWjx9/kgWq8SbnJ/S2i5g53D3r9tzDI6XIBPitGOvu9l/RYz5nkrGHeCjyjI34GPf8w1zfoYOPMVtphGlQVI8iwRFJcIGL+EswdH92OHOIlZjREzN4tp8MZpg6SwK58gcRllUuJRYqc4nte3Zum3RqWF/ACyV57fL2gjMkOOJBvY4HOCkfBw11AyqnE4l02b1C0Az9+OKimYzBp4vnIktaHGXX7vhyXUn5qxMPwwZHd0Zg0WipX3XR0dRCW5sTW8EHCCpE7mIV8Z5Mqbs8zZu5LU+4sAcJcGur/mfvJa8JjXJJTwdK3I/yZ7szorq5bTZuKOcwEj+2dY3OITKGvTVmh9tVw8Zp6FMecjEUiGGBZlDOffJggfaIQJgdyx6ESaO7McUBSrYM/pOjqZunDWxrU+lyV9xnD/8MM3uj2bmEs2W+khd5pc0uMXS/1k2+CFpEOeCMHq2hXOCyJ/SDHxfcvMR2vHggwzuFoVocYFobj70+NEcD7XxWa/4u9b25im8GKkrXxHUgxcEWiO1kdPFeUH/RUXrSkuWlE7M4rKnUC2xe7Wc8xXDwbPSgdNwhMS96/hasgKf9p2XBrMphVahttXo+SW5xu5CCfo9dEifrQO23biqBSfS3V4lvYATAjINQpGESuM0VfnhwH0Hyv6mJKJdBY1NHZPFjDnFOzI2mwQSH7blrOOCXHwnOdoSDso/jnLe6H3OTBVu+u2+qkfekZGmDYrIX1UTaUtJJGnsb6IPYXpfLdsanKdku0gO7gxORgMUykaBs6WwOy6CjtR5mLzKXa294/3Mht70x7u0a/U6exGEXW3BazCnTI9EG2UHiAp2CD3g3oiw9l0QyGRAEvdXZ9k7kCQ5rT8db2vywXIQrB0r+5i06ug7vK2dcWKTf/4buhvvTwh+925kmoIpuP1SYZoJ0+V7c8lch9jrF79CRDBdv5wNNxMVe4EduzE1CFCzkH4g2KT2ca0s3Gti+H+mvXb16+OD+9fn56dfbL69fnb86vPP4RvSLuh4AmDSxZZxP67X2fTqH/3DNRfL9IkQ0TucmeFepmUtz2+i90g+IvhgB0OBNY5Qk2Zu272VhZJm8TPZyPY8A3RMKAq3lMdj98z133nvkbEeSRzPnwUkfqFhQkEErUtgyq1YJn7z2jwSm11JRICl1qQK5lSYAWEyE5t3pCV1UWMwwFzKKSPtt8Ho/XZckL7/Jl29Ox9iphK4mzeXwLtzxj6hKkuYJX+cfMuR2mj48wfXz0U5LHh4P9g97uU2S7P4zZ7o9WJo+PD49t8vjor5o6Xpk2/RvLGsO639f1SGi2BpYzPps3PoARLS/Xd+28mP1gu+zAZcA/xbnklQ/KU94/2YWHgWSkM/+L//x0f+/Jt/iy9M/jb/VYTotPCDBZ+GI9TLouZ3Q+H0IafKRtk5lLD2WyCr4rendNefO9P1J1M0b6jZbOlZR+7pgX2On/YJbhu53iB7pnwUDmaPbxzn7fn9Vwh8Cbgh+D9MVfbaOM/zu85P8EToiJaRz+QGLk78KAPAaUMrRobe9RS7VMWyClmwqDYltc54zF03puuRUKjfiyaB8CTemhDQx8l3G3Ym4C70T4wf6pLOemE40TdhZaQ+cTqSokjeZ/fUmtCATBgs6ltDZgy4SaC1Ta9yYg0Pw7lx/Gb3ym3MsDdwlQDBA+kMgfKOuqa0WGRtiDjhdlxl22NaOCJL4z7HVvV5w7mXAZqALpGQk4qtxgO7RzsIdnacMYO9hL2JlZyx4N7fgcXmhK+RpiD0wfDBZcq+W/+tvt4HtBxyvsLk+3IDYJuWi+MaNsSf0i/PKED6Uev/R2qMBVnmk6Q/Z6DPy4g8Cf2DHhl4H9wbCucZfaQZeU2EnvkHnr5/JzvFN+R/8qMyfkxgAThIP2rprP4883f4krymS9px4MMtK/w/sEJBU/8U0u7Rj/Du8gd30grs1PfAttoXyqDtK/x9kS1Fnz08+UItjN/FlAzX94zdeGZD4Xtfop+uglm2wkkWM5gnrsH3vOglT6lJ91cZL3U5vTvmdu3rnJhkr3AxVsnCOR7HK0CATxK6p8LecYaab/W5lv9Wof70ATO2q2c1fPt8A8ISP9tiboTIdhEK2HUHng+f0dOAOByfEVmx7oANh9MsvARBHc1Zw1Dcf80EGa4HfRsCe0FVfzoU6XKgbE/XBaUWvKEUkUhOj5N7U02SYXRCp/mJ/QBB9hdYQx2jK8MBKVYzVEqCVK9QGBFByn4AOq5ajuPa7uENCNvlA1Y/4hHETsRW2drjYjWRVYbO3Mmi0uhAiNGz5DiLoBheuepP2XiG2RAH5b67YXLtZy2u4s6nk1gtt8G9qQG++hSWAsp+ERfiE+jz+5ZC+bsqfOUZHas02tkl/cLr7tHXCyjV8cwZiEuSwmbc1v/9WWLganbkvaQheUyqR1NUTUkYxgVE27ZdL+WKTpTmrXPszBsb+D65iuebAiZ28u5aIQksGv8ptqVk2XU1+uRVVcSoAqs3I70LniUpbgumfyJAgtpQPjhhli1Yq7DtYzNoq1zVAZ2WicXoteRZoQ0dnf+9alVP0PMutvf7CP+LngBx274n/wJFIwH8UKF/gBJcTvShCLYmRvrTideOCkiL7wbh3vSQ7i+2z3GP1h3K5Dx5llvChY75efaRnPikmJTBYnWlzec9LmRlB3i+tRrfTV/P4EwhDDAokS0CZH6AeJUT1f6KvAfg2Dfg2Sv6LfaNJZe2G7B+N+KwsIfH30yT/6KkgU2iSONJetbjy8BMEdLjC0nE6KpThTvvZD3sHWxfxIDb6xju0FBmbv/MMv6E0J+rJc1INRMRkp3VaESwySdfKy+OBxPRPxuxCwDH1onDhawt8vq9EnxhGrE+umSn9Q2PKoMTWXcD/OXxgnjgPtmFa+H2HRkS7dtdrm11MQQvX4uppdK3MDR+XsK4unSEAI+pDLvrTky60cQ3eZnrL3YQ5LMWl3zpjIA//dY+AovYMpfvaPYsZuvOHbzC/PLHSYcOIjxmALacii8yJaW8rRifR+9+75S1uRRWNjH86YMZEqoihBQ8Sa+ILUyrV1r8JBZNyw/d1v38I6vkNoLv033HAuykbyHXLN/cN+rTtkGznrGSXQqpoaonEOdnf/we6rCTv4UWUav8QgtOv7oHNxmaWkOYXh7DDO+KEjFfNC4VzItEYFOta6VQGEj6lSAhYKjAxw2kh+YChRpQmGELLHZT4am5ZCY3OblNtDCyMJESe9I+gFacXjQRyHIPLmlfeMpSYl64P/FeJA3Rcm5Q3BHvUs17MyHFTWHG7qtbzetY6lKx/0EAq7ROnPLz5c6bf5woUPgWM2ryvpUPXmoXc6YrZjgw8SYm6RJ8TN7bJ04WAkLq8l2RNxo9C5iuWJggjwdzb0ZW7rBJTAJHhMmWHG02tjQSXqNWI32kRpKv0pM/BOarJc/C2IH2g6Uo+rlpspj6lHiYgH5kgnC18pU2EOO5rUYFi9Y55Jj5CrQUXXj+62kW2IMVRCETOS7PC7We+X9cTdRjiDuvUc4oNfyWfJMsoK3QpcwifbpQ/J2AHfaQE5WxOkcFweUDGA+bfSKV2iYw3+y6vSnWFeP3c6GNdjwhZedsVD+++TVvwMq0aHC7HqDHX00jYQL2Gl52bqF88301voldEW3+0OE8HaPq42M7aTRksF/TAzJACggJxoYNEzrAmzEcriQIgqysGLUeLKy1yO2LU4gF//1+yQmwyoOoqNnkRZ4G+kqpSX+vp+dM0SFIy55cK3qHedCMMfT+qaipZiS8n3vfbgv7bMLk12YaLjLpgWUpWsnfUbvF5ntEy70Ytxq7M0QqjoYPybGF2qVaSeIGNs5h4UvMdu5j34QW4MSjIabcVF/Ejmj0IilEkoylCA+HcSYqbef702HvE18uShgebLuWmd/qv/IFyVaEnpygqt15LrzwzQWk33+EqAgbrEZXXL5sRdIGjx6Lm3EpwSWHReNzOQeKwObnzuow3NXBe0NjRPwhkGZhBmPaWHcc0wFJq6g4PMqjQzk/jQM37G05X24qaz7rtwi+a1V0VhAt1oiIypcp9xn/jwuMbHwrOqNljNlun6oacPg8KaD962CA6/V2xageYImkRLaKcfgzZz1mZ6SY6SS4IUljIUQrsRXEhnFilZVO0Q24Fvy/DRH3735vj12KFxwHEqVaLzCptcu047POSX3rCpPyF5W96iiNgYbQ8j2krsKfermqiOFeStRYpVK2i4cMAbcBC0KFbQ9iIKClMRdV+NkYAe9ZTMIL6F1NYSHx+UqNKOfzxDIJysjbWlQBiuEP5yrDihvmCyxVRhlGN3MvFEWslpRzGHEl1LPo9E7DCC5U4sBE1mMUsyf5UvlftkIkc7kUbq/oM/XHgqvMA4irrRhe2TquQFgwYCrDtjYK759iIbbEBjFrZnJ0PMKE35ZjOOAtiw8DivpKMav7w9KfRfyyEGwp2eQXrBvDm6wupUHk6YIt6l//368uXl5fm7t84vCEVvqiBMtzJqBffDW/qPk947LKfxo/jQ9rxohTGbunaU1EadfupHtoREBv3JHVPRMJ1K0NHkMWyY0PWTYkZtBinE26kmRmi0BlS49cxA1iKS58AymWFpIZ4yfzKofj+KMUQ6o5jRAaFIXWSByNZRpIbz/tf6BT0GcWzGHeke02OjF/qp1F7sXJ/ktLQET8ov8nQ0kswKWb3o4xeX98TiyGb1xV29qNsIExtkPnKICBa14QoKNJ+UEnrNGQDCqQsGRZiB31YIcRE6IycKi2GCNzjEGvqsEaqNjNCK8YXoXTb+YWKQW0WlMwDhSFKar7TrLIO99m4n9RBr3egbkevv648S99j44r+YLCgCvb2bjamqfqIhqHAIduWAUbn2aYjAqIlU1xnzrsMXCZHW3vdgCTLLiNuDGN5aypt5j/A06ppG6jGuuEqns+YHuXU/jVZVIeTKJeHU5o7jm8dr7QrpXXwzwMpS8T8hPTHkhShzLCjBcLLL16BFM3Bht8rILy2UPEx3IGmkNVwinbKPncHsikkbiQ2mEBphbseshgbIPWcXoRz5e1EbJ5UkiftryKmagtINZCUUQngU+Gq6ZrnzkTH/FLdu/Ou2rUdI+tN7fnp22RNDtdXEQAQckpdnQyks/aEIwKKRBkSoSvxvKD66IgKIIbMtDKcsmHyA6uztDxI1zRhztr1e17d1eticGMI451k9WU5nxl4Tw0jx5XmxRjDks+xIcSybPZX7aw71SV3AGsModl5YfHrVxTqN2mgT2TqC1yiiI3ygkZkVVUiVNsZsDBtle2KDslkK3cYanI8JHmXPrdXJthyKrVfkg6AoezzlG58KYh45svqbSnm3BPfGrUQo/GErmiI/o/CUew6cTcT28Q0L4fDeDbh4/85LYeoy4Em4QSMaqGFUXF7aRovmQb/B7k1E3OkzTO6FP+cyEm2yGRFUKYoidtXCvIJZUWQPi+8i0WW+5pJhojDd2ePunVFRNVdCpNQSEfNdoIwaMYQIKY+ZReUFASWzjK+B+JLsesaWniTDIhuJf0Ky3Ql+YTMAscK4dRyJUrdgooh5V+V9Ixdt+NXvl9QeuSY0BoeCRKxJsYL0r00n/yUK2uriijE5RAy1i/KCPT+obwbsUSUFu3zOhZE2Ts2poMP3uC+a+DAo/wAtI7V3iDIPya1wm4+gaFOAP7lHs1BCES/hNgcBYMk7F9QSxuEco8lQ6jB5YKB6qQePAmN7BOUPQQfmTjxC9MfR4/Q2UHMO738VrYd8uSQDf2SynVueWndcSvLWEgP4UjH2exdG9KluENnwwB0kdMm87GdEFpJhZHh+OQnt2H2JB4+PtGl7DGNpl4RYH9NFi0v5OavFZTQf3r++fsXn4eSEFfc1W8DXGO9pjbVLZm0aDkxkDSeDSSfhCbsfeSGS2tQ4HfJ5ZPE5GICSL35r+RoHimOLK0j/aJVHutOGQtRMwFu2zrwM30hBIh5Q0CDHgMg+pmsGywR5KZFxJ3X6tIYvUeRBU77eDrHAxOWQ7jVfahpF+L18E47D3cMwYZNcqBEWq+I/pHrV3H7wX7FJlhUfKxsOK+vFFDSvZIoIQYkCXSdBmLt0pysKLcR5Tjj3gnkDmVff0F1S1JvxVXCyjpMkk++ODoXEE5lQiE5xNgVh0AkMUefja518bsJI9uCwbhYqoNFA8o88i7a2FdHKIaa5dK0cV84kDq45xYjE56dcj1CLcTYn1iSWFABxYj5ubEFPvVdFi/jX3hlVoOaP9I2kHEX5OUUpXaPiJ1sO7nKxCTxIbCJjLCMm+Z3dLbHYNWYkwSE6pSuej+k/fX7xwBA7Emyt51WMDFLjAynJWOwjx00YXRbZ8iY7SSMSm2SHDVIWszhp9JwZ4XKSEmf7Gka4nJdUaIubRTC70DpUbvdgRiC+VziJhq6ZlA/eVLxElKYi8H+UkWlcJKRFgtYlpSwnzKnO7QrQVkrNfEwfbPpqGf8onLU2LMP4nCBEQwTEityIp9SQVqMB80ZEGxsdiCfRgVArighvOBbaXC+bCRWzL7ievUfUCw2VJ8ev9MqaMIa9QW/1lmJstPrcUFT5zM64DoPOSYhYu0L5O6xM85RYdZoKe8UwURuX3cIjnsB63VaLRE6jOuF5R5a2V1zU1fhzmU0CwJO0oten84QG7A61w1zUBgrW8agBPzYTDjxIYJAcm2wXsW/rDoYaDylswEGbgogdk8NTM82MaiBHJ0YTVq1BhsmoGEivCt2wfyYwIKJlVFGAL5tJZFOTlSiOndrBHcQdhdNZ1Ic0heMkDqf3OoQ9sMvkf13fM4TPtdMS5Wyz3ajsrzHHfI12nipryhDicspS5CpSwGmlhdXFcfB4OvSI694AumTRTiYbxTJgWN0a+oWOIDfuexKwJLua3AuQvzOwdZ/FgXHPY4aTHYHq9f32MDrundgRQWoFZ/qp8gUI4STYcbDV/oLHVQlPJDAerJf1PqIuIGJHyKBUIJMYErmtec7RzehktCqQXZp04DJ2UTx0RW4OnucTFa3ct84mppYOxw1OcQeU/S5OMAJpDuc0zvS3oN/gYzPedLmY2LoMb7m6tD6WLsZ+tEoPcJtQw3OUiRtbNbf+tTkQx1mXODcSCLTDrMlBJ/AEtOOM3R3wNCcY57T37X5+jcCp62o2XyZSwgSo/c5oeZTHeCgSyH0nMlHcRrmo/YsHuN2wkir60v7ozrll0yHFkWVCCU7JWVgcW0/u5OH0JJAdASY1NqQNBNB1xW+DgWls7xEXBlEF1KdlFKJ2FoYrlZFMH7ctCNcOznQMalOva0I2XWoXUYnqnC6exGixfyQc2mkxicPl5JcJLy41iGs5L0lfhsWcFw3R6VJSlQJVqfXl/LUkX1HIIvquXKovSgpmbfk8AjW+Rp5vSijkwoIVoXPuFZeQpgMd1KPA+5ykc30Liaj/rpzRccW/df2KYaZSxkzdJvACRIdhSmTERm4wuuHGWhbrDqVLApNpLEA7Lxi83jTx5XEObqN0H8YspqI0Tz4mgA6jvbSc5oG70TGAcYsoBdNkSOaqnuWuqqSFjGgdUGGUvyyY3K8nZRD1zxd3rHI9bTzSzoUdX665nPGuJnNf1HBUDX92Jb3WjFVjncu1SYagQYmFX9xr5Rst8EEzpqqrB/lW0ESpwzVxJ6FCxj+PXc7YdPQM74M0lLrn5rHqHxeTMi4iyR5ltWWTCKfEUdM4daBq4moBsTbMU9gpd1FZZrHh1MaIWqzo+bOOfNb6jTOAIoHGS2nxLHS0QjvPEZlYZ3cYAmHYoBbEMMlq182NukzmnUIrI6m9J65YoHc4PGQqOMQ8xsdkFZAitfJOqBh8tm6IzgSmCjAQmgITGcMVxC6D8KALI2tgNOdxyQo7G5urej4ydo0OK4WJfLgaIW2oB1C9gLWUmgFJ/L3Rl45A4c6t5YbzQXA+jROHphn+/g5tvUooEFfwUeChTvEhaIKPXFBTo+vSZoYE1trU+huJzXpsc5y/S9G34ayyV81dAwvG824bqgMNmK7Ke7004JcU0cfgAXDm4Z5IeP4wDc+HsRV/yEPUccQk7we5/OgOFtv7bQDJ0Y6TOaTBQbJhL5UwGXPt15p0u76UuOP1VTmdT0hadozupYxtSvDWQRvZ1o0geLMx5eQo4ItfV/9Cgf58CUP06quEgc/gGbQBQpyWcxu8nVuMPOzCxDS9hnZG1jAnzag+jgVgSJeaSjKWmCE4JJMlyjHfFSZYMetdFjdgCuV/p7mB1AB128fgCsdw30H9aCJaLpWOD2q2qxoBc0k8mLKqfAZc7sG5CNaioMwlEv+LDrOohFSR/Tou41RMC0m5HGbHx4RNOiSU1W8CJ8HDDyVJ4AvqfFwpdOpspixNND6Xy6mhfantdNExhqA62DCnGfwwV+YYR8l/5+VRKRv6Dq1BJbcG3Mpbj/FDxkvVGI8hZeHADY5tidEcZgKZGJy+S1j95i0sCjO78kfCBmm1rKY561luL/4yAiM2sNzh4ygPuLfWIrlZCswIg5r4BX1bPA4YCRNpPy8b36npRhOPd6WN6a3wcbpDPa04Br4C27+mjZQ+koYoAcQGwzE7tmJWORcfp6IPFHeuLCdj9sn6cjPzvU50SnCrzazcLcS4v6QABOCXwiMCdHnZkTKIf5yjbjrphc5RgLLVaEKce3bpBTEoNdoQwbR6Zn45rzOWiBHUAwZYznyJa+CmIf4nMyG28FgCkKz22AKeJLtJYuC2UZPB1A8INsyUTXPU0oRfMKNHjOCVXFmwpld62tqGOb87aVr/BHVJ74X4+4KgIPiEOUKOs0K/oNIv/5QAhMUPuKxuZ1iyeVOWplCahhLHKtRTVgjjnoByrLHYBCQmD3jGZh5Z8z7NQ+EiNuHYelIy7FXW7mWFLYGoQ8I7mPO0+kPZ8ENC6FuSEU5gPZki3tRA1USooZ1zI4epuI6r1ePJPfd+abiiq+ICCWashplw1kIrVDriXL4Oy10+U+kNZxHTWPNOKRsFY/gV3kmfMLSQEDpApl16tt6D9fUZDnpbdWTs8ZaoRcqRlnzhQ04KP+usjfz94sFdO9cjgdS0A+8gFCnpdxj58WK5EVIRazQPfITHj9qlqF7YbC8BYhA71qpX53kMaFNc5duKjK8mmUZ3dRsF9kVVuwYuSBEyLdA5zQeYSu+49N6XKE45UuvjThjasZtala2x5MixzxRPBPvzdFUBRAzEepYdIfr9r6sWTcbnxScELbJITHAIfO6d3Vg746wNaj9SRg7ljhBOjpA/fYU1zUkU7M5ng/buWbhORlEIICaseCxmrtDGYnVgHjGO2Je5YxxduHIJjBO9j6vfEqi+pRNNzW0G/KD8dNZngIvqqNYJ9IXYX5JgfQXKFf95sPvttmOKtVnngMlDwSjpcf3RVbAMMLFsWWDUkNe9djlDNDdXWpH/jeEM5L//924YsrzR+1K4GIcKVq9jcrRE/BsellgQtClgns1EdQtUdICknfBRWWhMA9F5DJONce/nDhbg1oUFfLoOG2ymQRk0pQbMfJh7VWj9jSpQPOK844/2jUp7LKua6td8rTo6FORXkgtSjRxQ6aZqWmNCcJzMWFCBKDnuKCMKnfygKov6KsEv0wIgcTiyz+niDA7KOFH9bFDszJx79ubK2mGYixA7mzlNb6UyWJEZrHPgWKMJhkrSmKAqLyi4YU6HEtnh87/vUQOScHgyNRBtzLsmzceITotriCRhVEgPP4NqfcT/B65F+Tg4m86101PiY0p5PpnNRUD3pVdxwittzF+5gCpp3ckLHmpEAPiZJgBRNoNJfYs5a8cg0oGAcEzaTTloytuqpfJ87jIkIQcOOBWtniJbEYO8gK3gJhKAlfoL5jB2MPNoF9BAUIUmgLHTkgESyjaZqXzuyKPQ5w26yUZMXd2G6csPvV+fXrntwn+3miEov1BTTkacL+lArzKu7bDimuD7BTlGU7BEZrQDg1Ut36odBwczEW/yS8HiL0CedtjAKLln2MPFF+QiehQvZuzP9n5b3NW1q/jmXLOzCBxWblhiDLPEI4SwT+KJYFpOo6Eu0VX0aBh87PMQAcM+rYt1YQmik9b3NTUSJKkswBn0RcJyJk8tNX3oBdwQQQSSkEHIn+UBoIFMjbGMHby6AUc6bLApRVLEsjmwC1+2ayYbmls+jAwGwLQSZGPvd/WQITZ1Lm3W9v7bxwuib5B5/XfjxJFLG4BeQ7jvFrctm3E3cOFWoqbETiFPHlRuUBBmE4mmjSNCDKcXPfks4ysp8ekC0QTxzoAiPRSnKEYDoyCQqUFdRORArzbL3DIG3I2NY84eevMCI3HG0TcgfxOtIGeW/R6Ve+qe4X9TTTS5RhjeDMHcYi6QLhUOqcA7SEoljE1DfUDmRE83A33xB19Z6L9O9SccD6G61pEeDS0GyW9EwOOZFk3KOmrOUED/r6iekfLQq8OuQsft5Ngpx55RaNuyc7UE2GZBuFFVL8UPYlgRvJyLvUb1A/Y0GllsagoDhW8TRiyVYsxjO0dZ+7mcYaUlkTc4Iwh+CDojJ4Bi7C7HYhRJiqwJd9VYcKRwfrDPB3XgVr9HojZYZyBB1TtiUsSn0vdNlIt/afJHHE4UGOSF8puirdR7hJl4xTX4YtTHcLs5JEqiyhwTc72yOYOWE/zLmc3Cae6ALBALRIk6jBELnMYvCCRGpkbqbGpohsOk0uKeIEXscYTUd2lW6iuc0Qt7C7GjDAh4WGCC5ID5HApfoxslYEARHpNGYdhmEfVHV3NUkiBxbJyaxw25eZwPRei5Ne01LRKaO75iljHB+U9CAMztBNfHOaZx+QjKfXzvQB4xOM4llzAU2ZGRJkwXWROhr4SxI7v2xDn+8gtF5psO7BGLYLCA58uFDVL7Bh5B5bGD+W1UDszBrDcloVfvqrlNbbmSqMaItA90yJmEySdGWSyTYGB4NOUGUdkrNlvS/TEME1Erjk86ZTFyQQ8+4jkqDQtgIu/rrPeipKB/sp5vii+84lhixyZmmipBIzLScXodJN6NtqFYx5pYzE1MQVpExJLjvgiEZYwLZtygvStmm7UTnt9rgSFQIRJxuy6KLz2uUk4SJJeJaNhxtkCTerd2S2E6Y5MU5t63Qm6kP4yPHRdEd8K7iEYh9U/eMmrWNUbI5h46oioiZ+fzyQPfU+R9yxQYu8OHWyqIXkENBRfMeXdqrBSpQUpJJl8VzRldCUGlVpNnGuhemHyWkesBHtAhloAO1jU7u5e1MTsIXgU4MAK+KBdFGkfz0peIRb1qqamdV2nWmPZtYzcynbo4jfcTlOJpBnemBuW4tGASjIMnKxXMQEBgvv149OwYtvXG+1vPLdUZH3QXm3Y0aN1kSB80HJaQgZuiZuGSc9jSoMzVDHYWJ+KSskBmXjBQl7kgEf8zXn/sBVb6wEmmYLBIOqy7e+GRf0b3dK542CXhEu5uafBCV761hfZF9EFuDVURL0iRCfcWI6QmX1P2+zEAJBb2W1vBCgXsQQQhwJc1RSeweg78os783BglrVyT7OuxOEQeNWzG5ixauKdSI5TiRsmTLoWh25Xf+dHDSNDOf5F/gP28+C+eFli23zWEDU9KbO57blgX/oYpc8/zHCHwjcajsDkyq6M/GEpzjq9F0CrEjaWdGrjphNm3s8gSDeVitBbnC3dEbN886ScewsYdnD9bl1y45ny7skprn5JhT6MT1CqC3UQJCS2lg+QQG+ndbhcP6Ey7SCLebRWAfPuoVKQwBR0rMvVXbjmwKoLJLQjnhJNSqZBk9Zw14RvRGHpCPMrMosL10ZMKW9lqcITaUGYh6Sl2N0s6ZarKzUZxDRvdO010fObW7Y7QPXZ70efNNNR1cly4h9V6nXWLNXddnDaw1iHzh9rIr+6Xg+0VWbmDL5BOXiMLAbsqY03xGfr2q7kLkwSNrqhy3kuX3bTsfyHwWxP9VrC8QpTsOvHy8XwmdYjAoopzT8UR8lKbO8DEvBzzUTmEgDhYWVonjXuuwFtm8H2o0mgFZOEwlDLTU8peuK/clICVHjEi7xXj1daVyHCoKoxJGtosj5hmBQnEkEhXkrTXHNB5HClVB6d3ZTI6w48XMsm2smxpFgjEwaHQW8O4UMbWZa93uJR6I5ggnGuTjdFdh7cde31BdkUQQUUCWWmFEUF2u7Bn0skzTxlYGr4tPkBea/qMT8DUbClir9SwZUxNpFqN9BgRtSkXWJUMxufaagmsTR7cIXP+PbhQ5jDVUdH5ve0Lg+PSJGNwpcSQwQgLrNDAJ7dG/1gixYaGIU4X8SGx5SVh1i4flqZjw1aJiG6J3NdR+Ukn3pKjuQwyM3OikKdPniBFGxYzULuoooG3huvUueV8oZFys4Lj/7kac7g7Sqj6+JhZAOXdMPikcOrdWkWxBYIriEkmOvptUFjJJtnqpGImgv5K6aYAmdCIbOpJ/nGWCywXI2F4u6meJok2xVqqYDAX7a6aoJUNCh0hHTWe35mSBnP0Wtp7xExhX+LXtOfVy8aQCAbZvuQOXCQXH7lCDCYvQy4QJENcJxcaOzHToujWt5EfTe5/G9Q5BhKtCDIv6SagjGHeZIpZN8U9z0N4HIUYWd+HBY00/qI4DYVouU8XmfYGue6EL1INWHYZglJ1M9fsb8be6Q1VdsQwINQyCFX2ioTawzylzOscqe0MFlTM8Ra3v3sZ3ZSLINIqsWFTzSdKUQLrb+oJ7FhPfuTwxhqunbNdGh9aE9vAauVCg+6C9iZuoNnnCuSeq2LmrG0YMvH2hd8M1c6MCAENtSVbLl05fKxFuOetH28DRq5OOUDxtyb1jJTk8u2pS/lGbVIix5fZRFzIR+aMNZLk8oGAlSvnoY4BvTS7yMhK/U8vf6tHWt0XqQAIb0aE6PLIaZJNOUuLfGhlUkRWc9xBeH9Mc07q+wwYrM2nA9vwspnSrKLtWKRVwSXxrSYI4GnGF9g/3VMMPhLuGmLOfeySd2ikXqORt2wmIBXKGypDNgYU2ZHhivF6GPZmWmDXyGJC3CskEXRpLJ45CdR0xmhoJ8cY08Qo7lW9JLM2CaeehtR8NF4ANqLDPCWOauoANFOyy3y82yr7LCi5DeMwhtodLVdWbL6yVyQKbgtHOd1WhH1Bdh+Hq+zVUJiwjzzK1YRS0XeQUCBxOnnCLjRqbvvlpfN9Ui9L91xPX6Z1rN67FCXJgF6H5OWYGcc5fNZzHGebwkBdmtcOGOgydDHcQEdJdaIoj1ukVW70JTvgi6j1BVnivys+F6yFI6yFIobWxmUI94uLPSmLJsxhBPTbFDg+/V2BxUdwZAnhN+mANUX2/In8g7uYtSHUydBEFCEYwprxoUByYeYpTEcwSax1eTsD/rmg4wwGoYPAsiYnsKkJgYwsytXWbFNtM94pRx+qWQM8BMqTKqyEQzQP01RG7IoH6lJfX+rXwouZu+9bND2a9FZ696PyeJUIJEC31ked9tLcGok3PjpR3a3BhbnI3GUcGWmlKMEwfnETiPaaU2WUpbte1ODzF7OuXhAaLYZfn9Vz5jMk93rcwEbrryiIS7E6qdH2GLKoujWD+yMB7Cu3pI0t7T92pKootgH7OpKMEf1VfvAAH2/FZLQZR3o0AqNitiC/zYTPM4fPRTi2InJfNaCmCCMOnf0itcfdeFWDFgGSEPGpgV0LcE1O1hpwJOlbQTdFHMM2JhsbGtUsXQmS++xR88XUOa8Odl36k95BMuAJlXzQz5fXZnMDHRJRzrSTzUMlaYpla0c6j0mfJJjKMafc/cqzH7DPZAKRyG+1iof448U16NOTE/ks6Tjjq/HTLWYbkJFAKj6iEmr/ir62LkOAvunMXU+r+9E1WybXQtn06HHH1NmVT0h0hUMDmVHW5NYcnBxWPU19wnmjGJNx8Fes2KhVkgxrhszqDlvFLWvuAOSLBCi6Rvbq0fZhDn2SX3ftTvTBLXUF//py4uYDgqljgXMbRySk//av/5OOGJaG/Nu//i9ujYwVkOq926atM7I2bQv6izdJxlQk9/mMG/kQ143zPy7/6YMrJPJbLMSwdWPzkwHACH73553Fj2fu8EnG3InFAKxNGQHFSUkz89zGZiq2S9MgjMu1N/nhy9kds8V5/hnZTWumIKYTQ7DOthbgX5RXj4qYsdjEnS6QVUn5Z2cww5lOVENZBfq0zZjIFvks7xOztSMGCiMJyFQWRUjWj29Dc2nDkVVF/I/E5t+So93EK/R45dCnZ68IFPUIuaDb1d/Nluhu9MOL3iWDky6XzefyIf0RHK8TGzxi/yvn91M+XuU594n38WaTixOjyQZtwEjcjh6JOjnuxpmv5Ig0CakYBMeS5BaD2tEHpY0O4web8jP0q+DphFlO7QxFfPnIUWCmc0oXQe9hVXn2RU1ls74jchC2Kb8DPAwk54uk4lfKXp2piyKV5nO7bOjB8XPf2eImr6FhcF/x95/x7lyYLBaOalqTI+I1MHjxO/GDfLpbIwDuHXOGQRzMs28Pk9vCKRBJVsLGmO6k4X1Fy3TqTFM7Z2fbfXj/epVpFw+PuGNkQDTvI/UZnVbOZnLGTd6Z1H7icipMctFgEBU4Unj+SHhIe9e7LWdlQ0LaNx5MDuLVXRnx3uYQP2Hd1KpiKRr0+YNp4Gna/G0xYkL9HieJU9gD70dUZINZbMRxxI/7GK63d6eVsIKeGiaRkiRJZ3RK2mIb0OnuVrivn6NskInRJgc7t/6mfQjmkjVZFWXHHJF+kPwMPfx1G5VaCLuJU/4cTOMbnBrXPXqUNfdiEA/YSVdPse0s6ASZlmETMpEXfV4+YWFiy5Td9ZRFiBULqC/XZiPdoxw9gEdOM4uAxBDrxlUJSGTpdmbZv/DEjCZL4ZHDkLpRenyYqibqruYeXtAsvQexu70fnVsMoMkBbW3rveBNo+OhJ0EIGpKQj398NnLNe8qw/S6/hyLSjFZB/EJgyOrTo+S6e6qW1HjbIaBIT6SpMw+Vnya44lJEjji95gabPmGbZwq7rMQkutlUeTed6DwUbGzBpS7aViyojhDjBr1HHaddbbHHmQfLriL3maN7NC9PANEmg7AJs2CdAVlPL481SiGmWXVFxOcUyII8U2kQIuJaCuRyfpghtt0/ZFL68V2+hrjrwe88yj0ujlcoj1xssfmbcRNwRkuq0wu9NIhzjVlV0TYILC8mPHvv5sOoPs90iBXw6UTzfrVbZybLqhBWwPEsDEKb+52wQEa01xHs3Z020nxCDCD+FpX0UIAOmYxPJPzNeaK6UZ3Z/byAAMpYET5uEyh+W5EWSJ+AfTS3ZN2JcdN1U0zVsisN2rrETZSSmQaiVdmTePy4pZHkpuN2RU6zR5ncIEaQYqVy78HBx01C8q1JYVfrmvbynmuvwfSIWk992apjXVApaus6B1MB0Hw5KXxuflL6BORXzGE5h89iabdvp6OnrbOW4rc1tlC4fIkdHMtptZye9N5LWUvrSRXCkG2ItswV7mdZrVfMoqMC+6T3yyssZUB1CGpqjmC5Wmu9WhOzD8MMRZhHc7I+3zCHXobvq1M1nEawad5Hxt9yHdz5YuTUCnmig3nx4KF6ggHHlzE8DIzba8vJDa9E0RvC0fzESa74Gh9sP8v1fXEBVCPwKurQsX3I8kxkmcGn5SowErFBI3QR+ObaYZLt/iitO3qs/IfeBTN10wkBRgr97MbJxcfndAneF4Lvt9+CCDg9f9P7Ec4lmGC9O2wAO8tgAUYNmSqMTRHgV4c5TJDN6OyAgATL0/pFjn7Es6fnm+utuBLvy8/1ZEm/vZxUAaepNjVaUGk/Fj9ynjvsnxl0R8WjDBKmgDtrUzaTNh/EixbZSC5WAlRenrS4jfy0P9v3yHg6Ybii8hlOF6LY/KlMwq84dBuL6ChE+YkC7aLB638++1wnzRkT6W5pPuMaIU/q6/CBrn8Vc+ZF5PH6A7Mn0jqNwRDIkxw23ghuft72jIoDNWeTT6UsaodBcptmKYBpBxRqOdf42ahsMIi54vqb+IlNc0dxKrKr86mmafHJuS5JFxJn4ySCiQ0ykWXjcpY9gR2AFQctChY5trjdBfjQaq0YJebQtVBeEupOQYvvPHEOSHV7itNytlx3y9B5cG5jgge1eaAskRM7FsSukjwniVyTmKoWsZjyoeWuJgpuSM83IIBIKtkhn3FazTQeUI673JGkXbYb+XQtisy5KknNR7DoyPD4CPE6WBRnrAQN5QoHHQE9FbcsFS7cL8BRQ7bCe2V0SHCK0kSje5tcgjUibuG+BIGXqFCyo+0nCWMxPXCFbDDaYuGCmZaCRqOZYXxMihjXBAq0ZppzA570w6OsuP5XOkNLIEWFxDhl4w3uAmFKNHQRRcujwF6gbHObknW3RquzRpiwe6T4HanO7pWLkRAfmjN0rsd74eP6BtAC+rIeVbQN1sF0sZMNBYX3rSJMkM8ULu6Sar/499Fv5WayiUcG/qIwUZ3uir2cp0e76/SJK3ZZMxJ4aTVVRYXcFoHXSVdwWks0j+qwtMa6DqgTZg/G2avY/4v6s74hl3DlwiMjFKw95a6b5KvZbbmvCRWFIP1y5O08wnFQ6kD+fBJ62KFuyt0EZyu1WsSS5A3BCo65/A3HiA9Mdlw0DxpnGYfFRvAqesl8MJc0dHnP/MnlOOuqu5G9KseUoafmomATyQKFGAl0Tk6KLBdax7nYZIARLPImtpjguDzo/M9hd4yHswr2H/qMrQ4R3KtWy2i9ExpVs7lRsjXM6pkhn4sy/nCx0+MMks3VBMpG+0rgV03p+lk+YsESDfcTbWRP4/AVOcqQ7rCb4vqLgWx39OHtCECse5sgiIfjw8yoGktsoo8HV2exPlwhltZ5BCtYs1IF9DThuSNiZ8vRI+U3rSenc5lamCzjk3LmXGH5XMMRA0RU1jb3DjJFWSeFhCsy9Yk54cQN41YZxi51n1Slp8FDnwdwUGKl5I5htfoS44pNbomKBomAoEGVU2lxt2t7VcvPiKwoBwRTKLR7av0pAnvqpBgHK3bvICxXl/NF9m/uOOQjR1S/4GgbzVmV0iyKQNstzlPLZLrmYF5mxXUJ6J5Qi19W1M7w15kAoluybOh7HbWo8PA3nxRc+EXb4qoh3oZ8GBREnDERGrxFZh4xVjuNS9Q1xhKde6rCnYEe4ZFKQVwE7p5PJwMOyG9/mU5CGAlZJSeYm3tQUel7zoSrAw5tLUsQOqVCIWWNmXf0ylysSRAk5wpzbY7L7+XOV2feOupdP9YGxKrts819sh0XK1ZiC+vdSeRGaQBZnGWclqcpoQ7pB2ux6R3KZL3jno4hgi3Qumhk5cLgEo9hV92Ox6xwzAjO3TC9zJJiuZ+mJKQexjGBn7kzhNX4bAPJZgiJ3k9V4CGPF1b6J5XQydCbvcOZ61TSVSdP1hnKxc0GTMux3EnNNctBJaWZYYc9EXto3NRzzCqoTVSbmuJWcyMpyIkzvwga37IhiWVjLe1uwEAkjrswfbmqrVyWsDvsGg7feU3N5ZS1wwPuommuhjcOp+Usi0wrwCBauDANA6sIwgUyV0Ji2IWLy0ayZQx+JVfm8yPPRrvbhi01+CanvT/Y4SDIjk0hZybxs4rHrP/ioJRRe5yssaamviOKuTC15R21DihdOyOZtIzKU5nWMzu06ChontGdATLGQCfTXWV9B03VJ5jGDFudNBjg7RrdlcYlJTw7DpRL+IW5ik6UbL4bjbmXFmeYu5/+txFYNYgcLso7OE7UrExSA6aE0b2qUmHlHpRAEL3rvM7SCtn/aBztxLBa5AYyIuiqF9xOz/UmljjHgCI5Gm923sjinB9tNHixd5pDIbK7RtEoqFqsAulIs+DXODdzEnD5OjoyshiYIqFNuBqkSZpiDSV0ExwY6fjhDA6EDDQmL5+vxsrn9jZTlbms2cH27j+sN/U7zISk+RPtORb1W4kX4BlRPA5yJJGZWGrkFJ84seNuY8I0FsiwfFh747ej0lnkkxGhJ0CBVBhVUcfWr3mID5V4we6gcYbOKZuaqm9vCZCs0JuQcnkDFy7iJ43L6Be5qrH1oyyb5TT/2+5zij/0tqEzcWNqW29aLBf1wEOwOaAWVormEOF+54i3YTmrfr/MeYRePmroilhg1AjijVkOF4RJHfd2t3dNA8c3RAlhni2UOJ7yyhwAN1nl3uTibdDUCUO2x2Hh2yuTAlYT/PLCaSoUJXAeqB4SvDtM08yyHl7CR0o1uSWyIm6J/Jtg6ALttJuynAhN+NSg6EiUYpX5AbqLTvbR3zNrH9nP5nxS+m/uE4rWNXNuvYvEmEdh/zqpUCIGJXjSSGwr61+Lo068xx3x8pdFW5VNHDfA8gWbaJjCA4QREdchxNkVvi+E22oJRkeF/QorU8J3XRl7MFLCvEusQLi9W8C375FhpC0XWEdc/QH/A9Ecy250sT3jXNl/zuxm8M6uo0x5A0MvyN4XuK6IHWfM8n1swzMpii0PyKKsDnKiS+aFEiIEtE/w4xyvYJuN2HqwWybCDQynlTRIW28w2x6/dlGk+61oEgqn3KKSoooto1jfLuH7GIMYskzQrC0fN5sRMpfqeHsvDtOuqSzb334CrrthhKLadBA+BSduWsWc/lRXPqf0H0VYnMjdTF3NnI1selflzeXQxvSeSbh3GW3lHpFRlL1fFNP5t73TOVnLHOYh0HTbM13bWD/auA8HladB7MU9h2GTGVJob76ktlhoioWHoAvWF4RMn5+eXbqk5a3A2qxnxfiUkGI2UxSXw6iLu7WhVRglSMJ36YAMuncJYYq58N2J0lvFGVqbgY88BzG53wS8WEl2x7EqWzMhnH4cJvSHy8QeiqSAAt4F69CUVDTlN+1AlUkCDi4xY3o8iSlFgbspCLtu9OodO0n70C+aB/F0DIFvlnOPlKY0CPn6BzP5vFuPO2qOSYS9spYgvxLuB/hbSBxF3p5PP3f2dvVa0x8EBQvI3Wm9VKfyNobkRm56kO1X35G9RILgGSAvBaiMFnHi8w/1TLSBHuTAwdTOsBEtkcdShQc0zyphgHmOYcxVpGgbel9HKXxEHsR+FO+09N5uTYkJZ9LNkgtMaCWyIYeayArs4FfUADXR2ya6FrFAx1ixcMk64VHkf3bjwg1X0dz1mhfvo5NwLkQ6WMF7Q51OyIqKrYwQeyLWXfbF1UkugroSr2UDfIywMVwyCGp91WXORHKtZCMI1H2JaaIVZY2Bf8Rw/Si9H1FBoxvg2gHYSx92QtRm7Ojbkl27BlblYG2MdKOA9P628b8uKeMVjhLmv0ypxYorE+lrUm3Y5Kuw5e8pZXYH6CfEB3FAKHu682QURcC66ieUSxKKv/UItRv+hUhV6smwaNTM+xpr8k05pZgahVx9U/WTOA8Cor8Z3WHhYJbQB2RVx6VSEmDietZjGZEPKDywNe3Vw3qlXlYiVQklGOdB8CSczyjiJivkxPvqFElSy7zSgPVGDRr4hVCPJVX2W5YAgm8Tq05a6XLs3t0BaDB/NSVSlFWw2oAQbSujXZMaUW+me5yQBWG63NjKwusy4pyKMbchfyeFEKJ2YSLs4OmntvNI9Pigr6CW8ob2cN7/usLoVNt7Q82sxmDUYJOslMDX6mLbOMfArGt1UClBZbjxFB2TVN9LDbUrv5ID/3J2CyLkLv9wh9C3gamgolFWC1Pp2zAN5ZDttcsbrOrkeQbpIT5nXyEIfmxA6sOfXtVoN9jo7LscZOk4Qc1GhSjyBsElFyJxb2V0JTnjsWoKyHCDaAFGgyAeCZG3Etr7+8gMIgqQityw7NvEETObefAWpJofqy0CSuvobdxGFnisTW636+Z23dFNSWn2tp9hsz8Fx9UknzI60Jr1rDtVlf7U4ELQykTyzaZ9p8t5+r5m+JX9LL7vz3lwkYD8wRO4vSXrf7xsnCZxxSlSs7omUffKreemHlI0L5fV27IQO1gh3zN6FUzu6zAbcoSlQK49kSNucybeKdoZSdnZPZPdriqVMzpC/VlmU5o9pHAFMq+w2oVlfGk6m2tfyIh0N6e18wQCPGdqkOJ64KhJXC6M3atdL132WFOWavzWPmxZeEazTi855N3qzOUsJ5+QdG/0yYSbOPu8QpPeTIrbiEk1F26Dl/tsUY+88sqDgsoH7jvDKeHFuptRBJYXFgkRnEeQFq6feJ6S1CsfB97o2CnTt3Kx0kIBAwilNosO00tT24qqxd7Y7pm5BKAaFgydFFzpXU1xCYGlCmovT4Gck/jHq/VXUjYXiiHydauFw9Hga7yCudK1zKbHwjpYG3tA0TSmMD/sGKWUEam4+vedMKX4Z+Y3aU8Ibcf6iH14C4iNNoBBCZm2NeY4K4jb1Txeo6685j+0nuCvSvCNFFo2Pl4Z2K6GCZ2nr4z/uW3NIKkpWQ/akNPvpul5CK2Io5TMBBVz0xDKYU2Rs5QDaVSBa4M82FkK9oK4kqbPEV0koCJtjSi9OT0gPQfL2jB9u+aIxrZwEJhyJVfDIJRnSvaEg4Dks4+CBFwvnFA82N77hwzu87i7ZH5V+ADHjEhTDrb3e48yNXQ54at2gqc+I3cyS7AjGtK/s5Loqz8DWvO/4O39L6oBTB0IBY4tW4CdRYZO84b7XoHzAveSnXRKUZLYI+LicUmtnTXsV2ggsPco4FTFM3DSe/bk6V5qC0YHl5pFU0Vu8XCBF0aD8x3gH3N9FHBp4ZiFAWTKGUbIz+pZuJp8j8vXPlIOrlM0TfHgkG4BOM6u+B11TMegDA6XO3HrMiAmGBMmPnq2P3KRhupd6WnuqXl8c5AxTgHMRjo/CmsaHztGf5j2DfLdcVwoFxdf/Sif2i2GQSXoo45cwc/4aFhXEvfcd7ZeNlQIlaWH+vnfl3vGc7e34JkRS+nXP5roYtKKUu6p7mfVNVxS9JQiaTY3sonoOxdXxMtKnO228ke4ZKj4ZznEAEY2jJ2G4TLWyV0FCrgZ3T2wN0sossfGNB7NyvGGhcWsCbmOlruyEzrEF/gRsYxsKJPMpLExJnfh4HQGcdnNfR7zkUlKmekaBOZI1PJYbWpXGFurIPB4JIVPZH/fLCmowcRtm71+R4cNDfW75Ifln7LKzLbF/LnyDQZNby+Al+5UxVug6bKe3uhRKktJ7/hvILAxp8q5gwE6u9f17Nq5forrbK+r2bXrIW3ZBgzvtTMDmB7MMNjlBHmehi2m0VJHp6NvMHmJj8ANAGkalqesOhBfXbrl4YghCJHYlslI59RhDK7PAHO/+tkZ+N+jvGdFG44lxvzRKK4pWWXCmUTAuoaKfHHMHdRovDEfbNgjol8J5FEGJe5pCVVwuloPIuBEa26ZMhXfLAmhKT/K15Q4e9UxJAdBHNtOyhJ/FWkbb9qllF2SDRump9YL0dEK0d88bqxj11bpnl1FpJM8wdd8rArBGOjNexk8ljoeutLwZ2IUI/Rw8TAPcW9GN1BlJC7GXTVn+Z8EgbxZF2g5at2ZV4BR48Ewq7cSFuazk9kAL77fpupdjVNwfms+186p7rRGMxNea3xqXJEYspNyme0AHmZgGdwRXtCE7CjCCJL76D16zx+1zgv8hz+/Br2zjb3FvCE8AS9/71HjppCPtT/9GaZEThkLL6YkFQYCsQ3YZwtTBlgO9dPmJm6aC36blqQZO8E2JDVCzecqpUSZGzFRUpO/Cn4P/KPNf3Fcrwqqbl6KaGrKhCgAVbBHWBheYg00o8ismtaT1xa3BZf05mDlSUgHX0KrcXCsyjfZGWKjtFGznA69QLCCnkKmSKNMpgR2D6pG3N+ERMcsocxLgWG5si8UQPrCYBYsOB7oMpKxp+sJuPJJDgdGzsRNOjN+OaC206noIKXwDkx7g33LFtQdIsuDoso1oI8gsZKztOHB1xze3P5dS9zQtvcc7hoGgUVzsevjASmzshx3+S1FGPVRcnLhA+k0G7poLigd4Kt0AyUSDOB+nCU65jpi/SJyIRYmve5isFoZgo/5zOXDdFuxawejRJL+WqvageTqvGKrgM0aRE/DigluoehRIpZLnroydakWepJCvbxVuVEMnjNOszDdycrF2R+PDrb3tp/51iHhBGI0jluCs8D5CYMtMfUZI6PAvnA8dWLmX6IXKIBBNfWZeyWz6BcefJTjryo8ECfqgAbCpweaw8ueyoS+uFhrnhc7Os27em4jaf68+TZtGvt2QjGNlmOYmCoA/8FPlWeuh2/ZTOgbmanEO9+dD9eAq6gKnKKrxHPGgC1FFpIe8kxL1ig2Qnif0xRXFvtFXE+eBsXgDFNL/Cv0IE7wohyPkVy4IxrfLh4IfMi/0lLn4Gja4sPU2Y/RsO2mEjkKenm+VIVwEHVrZuUuXRaxIwLFJogkWJa3SaSrCPo6b0qqn7vdMUDFX7XoVuexpEKqEsh3c+YdAbFtd6GNyyNs8KF4D5n1Mv2B80QpLilv0GZJOJj2r50jm7/30uTSTruga4u7hJ3tz2r2wcEfbN4UrWbcdm0S8uLy1XLtqwPcZdFk+Eg7z4P7Gas2pAoCt7Qm/A58/OLl5VnuoCTVclIljG3CudlCF9w02ubtL71HyeZseaBITtn77kmWR8ugE/E52TofzNWpdTF5EHwQLKMqQny1o9wjV8pVOmcP9dLllgoVq14ISKHqlghZca6Smn31eDoP/8qJxK1J/DxYMni0JM8zddvnjuWKv/K1M3DPU9+CFGOZkI/bJhfrWDvWr32RPNfWmqR74CYz9xwgpjJl5WzUAiBYyz8vXEKNGfJUfE/g09KVuWQJ5Lsxz0+ScIM3spyE3YQewjtJqdrd7pXbt9voyfR+/faF0LPP8N8nLz+8D/si4Fk62N3a3d092cttEAWyPL9LQmGdYTewytfZPrb7Q3Fr057B6uTxCh3Uzdx/RpO8Ic+vyYL7JHjuiXGO/aUEBWLR6o5nKP2SdoZZ/towHMXRQSn+jMCqihtLKrDcneIzf1/nYlGZaZxyh+msz0NBEFAOfjsd+Ea/zrpjK1mOWLBn9DWGD1dbk/bK3BTVhONkaE6aKIO0NWb3T9/YUfPE/Y2dSccVP7TiGL0hEcC+4SqaNjTYVnC1aRRtVhsyNmWBcDc3Z/Yu55x92wxbGRucj+hC09e6AFaZaBgDYw3Fa8yJHxO1+2UgEejaReR7JnJGKOhlKLCzqBQwYYszbl6IRU5pL6LaNulFkc0vu1F/5Bj+qbEIMyC6HKEwnMF1pQXJjzK9F3N4oNzVxEME33UoDlU4C4FJOvxozJOSoRYxLOCuVE7SuliW4u3FVe2pbVVL1+1w0TetCEwUQ9KMMctHtVk7hXDt1c0zmSjOfYjZ3C6nU9SWjmXP0VggiKtXaTmCNy1JfrcuQK9pL2J7hkXJp3dsPa2XSUaYkYEmq4sME/jCk9KWTLjdJRpLspwyD/qAire7DCfhXDO1fjFAqeC4ayDTHjnOJaWLfhxEA2zEs7yTPOZGVzDcNvonsxEEvUVW3PZ1oYMgtZqQ80SxTIwgfoGNwHAL3YNCtC9STLFuQwvd92UUK7LiDDDG2UYU5ceHMaEDHmaQpHWRjeV1MkpeFE1batdn6XtG/7XVW87KL3OGkV9dX5yevjn97fmb67cv/+nDi3dX+G9s5RO2yXmyvZd3omIOsq7wA3X9Uyng2ljEkMWYB2+709pZU4sfqbXcyq3q3bHe7oujn9LpG/QZndmr5MwGz+4kgjv1kpLyyez6rHJ8DPw/nzXMKcOY/UU5zDs4Y/ONkDdmZDPqMfeczCMU1Tl2Z0YVFle0zYtZmdOfV5y9uQ16P2eEmD4u0R9RJEM4bjqelG92H7LMRSpcmGtckwUt0HwUxQ+ytw2fSRRNxZj6GVdjD+4uOEJCzTY0EB8UIuOtuB9dw8Mc2xoZrh3PkR1oTYyaYMhJozEJeZu0nTd3lH8luPi5O20tdWcigNE3p2qH7oaxK6sOci5SIMzbYgZC6w/lNaPZomwDjP0CvtDeUXLnR8Tb2p4Grl0BYjKnwq7tyiUXJjqO6s6cBiT/Z3IjP9oLxUnwwWW0MWf274WxRCzdLTQt5Iht8eXPUxr7aES3EA3TOs7rjepKGQdszpSSRcJL+jO7L0AS9+2CE5pstftfMFIZ3tRRwLcZbMeTBNthCwPVCwtczBj7xWaWqght9CkQiP3MmuDI3DGJlwKFRGtoNJgYbRLWxK4oSa2CElSPUazGpbo1mmrEDc1vFIe2nZtOFL4qdhiwq3WRJL4cWUrwRXzKbT0rt3unSM1EVkgQmnGQLHleUw40z1PeuXGy87M898ED+djOkbvIbXxMzMPnl3lOKQnhuHiyfl1uAsbMroJETY7m1CQOiyDM59BiuUew64RzIByDDshKV+hhukHjVFRkEA5t7hEu2p/gdzulIEZUxIxbVdbgyRjcwZzjm6QaIkl0ML8Ipf2ZeAXuTlcqJcjlOEeQBUtWSisM9uquan30qdVSG1TJtk0RnvXlrPgMlj0+a1sAsF26xOH4EDdRhpJL7QwbnnKkrN57jSJiLrq9Ak9yGmWGOAiE/ZfkZXcVt8t/zbTu6IZ+uqe8qcHKQ4cBW414J9mWHAoMMWHyTFxL7Z6Q1SEinwIcOwOstc/6ZHmbE96dbXQ1PqT9ozQb6XqgdxD/ZkyzGO3/U7Jq3Yt94VkeA/b7k95NeY+8usPe75eUptoiblxYw+3ehzk++nD3v4BapYLPGrkXmtuSKxbNvb+MKArF2sKj5di3bP1OjsIQE0AWFvvOvTxpGKuSvKFK5cN1E5/xjiiMWglMYBmmw2XKcK1npcN1+tPjXqOeD1iFrhJ/p0T47yANVILi4Djr+nHl4s36WyUojT1SV3Sbm4282vlMCoHduwTZVfUhEA9FdVhhOsrFNshUpxYYXZZvF8yqmFCkktFWuR9fMgPnOyH5ZBa/M4Qk9pDgw0r3OjxE7zh8uiL6Jv19YXrnF28xPOTKgCUO5m0G7y37eKBPyavO526MVOeWe6aBtrp+wIbyxldJryO/+WizM4lRFgX2xTjbCm+D1tYxrsbTm+vXMzLvKEnvyITKsUM1FVGYOMCYRbUwSUbG7h7ilT77+cYC8QqM6zz00DIzGRP4sgQJgV+SHmNKe2Hbsylf2qfyQbWZZdgcoaicTMrxNRlw12okq8KzhcR+1JcUqdOydK7M4nJ1Omy8JgpN93j2rkJ13vV/+9f/eSbn7N/+9X8J8ouaOK449LhNGucphmh6aFUwP1NAwnSS5V6qAEiJ+MMMIWiHyTiAGgXnpiOCR+em8GhK6YmSykZyKHMyMFrr9DxeYJ7+ijhhsRtEy9rrsAu+fpSEzMyQ2hHdkPXSiZOj8luYtrp9lC/Xc0G1qqyAO/eF64/QjR4bXrdYL+YRKd2LkBE4KlzEtaP6HAzTr1pEKUMrKA2p+Xoffu/Mzx8lQcA1zDT72wfbX7IzWbP2ijPE5Z5LffCaFX+t4nJUT5bTWT6ulZK0ZcfSYCN1sHQiL1/9l9BSuzhU9QdVw9mHvJLKLjDWiOjKlScSKNcJDJSU5Kqssyksy+1V5xF6NyP8D+LgF3dojMcS3vkr3XSZ9kTeGXM+pPpZCYmjPF3K3db5jMQsDLm2H2l1AtsTWoTgaxC4fBQswrqx+J9M3UNHg0ucCGLPQaaiEEPZioLBtOfSbpfMf+Ba1aB0yscQcUhnt7WwWGSUhehfNV0Fsy22YcgcFsWmheM88pXzj9eiyxy1UWd9nt4OOGRp3BJHPc2UjxVe+QTxFwegVzu08wh0lDjYdKfFjmh21fYhD6og4h40gaDrYBYl4WD9LI/RFd9KupVmxlxbSeUYM60bmUtsopNo9lNKxw0hueWRH22gbHqPblxfoCRosKWuvmmIOCluHyczMK6sh0NEGM2Zz6iZvsZkr+RPqDobaTzEtOHIwXOFS4TKqzmNjMYz4rrdN82pe7zm6VwemszBSDTMUpBXRF999Jje1f0Re0TI3zX9YbKJwqbfqwXenXG7cDYfzfFmtgV6CFt7MsMQbiCXwHeX1FvAIe+gq2TmxoUaSivBbqlbwAP/VbRGDcK+qeAYzJTRQlXkqgJ1L4KMe/5PZenyN3qkMLYNT8Xg9if42HndGKb0TYdnFPp1oZGtuAiG4/h8hlfKw97vQAtymVGcaFBxE/ZEMoHbfFtQr8dYT0lP4+40kd7gtd3V7LR/PgHOTERa40G5fgtdEZqxON7ls7vwX8IJBy9ApcEEUHI0iwnErePEWw+yXihOwgmiiFhw7RhNidI2ioYkFb/5RTFOuvdJubCW8upbob7RWpKZnuvyy5yKXVtm7tAjzucL7nwbnspQP/gESnO7nIob1RT33HdKeLNSpXO4yv/xcPK0tpNhqhpx9O/bLTDAhTVWjPFio5BOYrRQtF0akOZPwaW7wX4ZbI4JIYyqIjTtxBAkofRFMF6HG0Imm3j+HG/jQlwRnmEXmFjHdCzE//VrYeOLM/Qoff/tX/9vyzqYuA/Slw1U9URin84Yz3jMHWrL5hg5fV+NZCtM27zcVnT6RK6YwGXffAU8ZSirVtlaM7H7+CLCaWnxXA8f8KRwMPK2ls0cTmrdixX30QePeZn84aCaeAWSqQ9Yac+eiTs9acaR8mO5a5Rwd4zQrqolv5vL7Vr2f5+ECgbtpK/vMqDrKccTEbngKggR70Abk3ZTUxjd6i63rF3RQgLHE/5vkbGI44jPqb9c1oQNPOMu1ttEyKWglFwfJO1ayOgTV98TSCaPUjEP+eAwvr4axyZu5HKnpVLdYUz2w5IXkV6SlsalxXSktv5M0pW2qVjoi+Yv9nln00935qVa19h7Gw8VtuYxRBxJw/J1OW4KNrEJIbJOlODLL4Q95WUhjLQtJ/XNbtTocCbHdu/S/cQER3i4rhmkYKeQW14tAE/U76Mf/FWJFG3yvld3YdFru2jg4iJFC/YTnouR6ykSN6xXzMNh8IFvhMs2217Xb5Xwc7LAKIKT13uUFq0/RrQOKMT8M1/m+stggzBTdEyfghYMCNtR7TEPhLABy5JGdO+6U5l0RSqE2OxF8KOB7eMeDdaUrDoGyF9MFt/29rf3t/9r7nmx7bQ22rlvo52nCHtTFvDVTUE45kQOFrXbS1FhFy5E2KY/D9oIMii1+IRt7gha13XCthXWbJ6ihBbEb5Cg2zDtvWX891suy7VOvY+CEvcpLBIcAkk+kCuecIIiRV6KQOV+8Gptr2RPxzCeArRjgazpETGvualZAoWam9eOkzTi6XT10jzpPccfOREqbrkGGkMolNeZHaSsxsqShmyYg42deo5yret8kHN+w8uWPDw9RwmE3ruAmS7L3fQlmQeIJnWmf29cl4aJY1TPH6KKpLCAKLip2d5JLrr62ruLdBwcMJfC+m2o47KwEh+0lmkHNS65VrymXBu7Fwq6xdWsFKZEKooYyM/fEKDylNhcyeFzzUMD2jcxb+0dJkpeLO+gHqL0hBsTnFu2CmDJgq+D4dPzKWgy0p6KqQ3ArZvQh4Tbc4ZbLdnBKPqd9x07gkovuGdql0rRG6NC7vtf3ILwN/inGDh9igEn1/PZJI7Z55YLec2NNVx1UizfSE10QuxXPYZiHdco5K5RDrhTC+J29bfhlKFJ/NOn5I6gY7OoAjILY8hKwm7cFKwI0NbRzB3sgmPw49W+REehg6sMc1PhxfLKRIqMay8f3Ot037vQxtMQVWdP+nxFgPTaNdp2E2BP60D/ghrEkm1BWAmUk1FWEgeEC+qxYigXMjjrlAaMRtLoFVt3mY7CqRQ+Y1qL3isO+qcZlFQ9nXoUHMoP5Cv1jnPY7mu1jxCI7dgBvkwaVUS7F/UhsBV0jvtPkz3bqV3jLBmq2dZ9WctJK9/H1mX/n//3/4slEpkSVQCb3BC4k86rjUPqMS5cZ+oQRRradt8L0kZyJ8NBHXoB18lhdaW7VNnixNUtcjGipNbkqqmX2Iz2rq5ZWWa6V0o635jkeCjf5VvFB/jCep5hXQ4t7L1/8MgH/1unSJCKfoklkxwSibQJ/Ux8Fn+aYG+DHTHFFne1hDOTqmU6MBa40F2waU98dN67IZu1a/GNJGpkmEqCMUVkUlkmReaW2lpxFNElGFBUTTDwz9wr+8cP5/T9N2EevI7p1fR7SaSmxTIg9MG5YAdd5sQIz+cFkzumWUbs1uDy+CAeZ0HdcceSsR909ub19RVWCJ2cOP1wrQ+4plV8VHyuq7HtTsEdKc2sL7FQjOW3LciUXtgUZeo0dF+UcyTw5lQQX6MV3TCbiOVyz/YexM4WRkt5enV8VWc8eoAnlWeYLpSIsSKoMIM9QGwYyfOi5v4SZHt4Q5F6RflcFpMspW+pVH4gDUeTssB0X49wUmLFwd9JzsD/bXExTSMYeAH8FM0E9I2DMLSgG0Kh8kZcN0a4YZ7xvqmw67p3d5APaD6dDBgTwn1SKeVbfkHdWBkV+BqB+TCBF7KDzrvASUkcmY2NvClre857Byip+2YmUVfXXbIf3JaJeynKLhYvUUzD99XGkLsS/lTZ/ighjOYjNYC5m8OiYGOy3y+pUwJSL+XBIwJloG+Rv25KSMKAkJleFEiNew71aG2pr7pb0iD6RljrDKKdiRYKyZbXWtTg8tjj2uhjNewCVLeZhVliOGMCJubagCDim2hxsmpVNt2BblBYpkumd7nVzrfovaZCB4+cXc5CrgUxkmJX8A3cs8ZVCYf+C7ddoFsP54tZ5DuNSj8kLJj8caCCEbcnT/8Qr8V2MA6JW50NXepaPEOZjdwF3CM3NMMhMIxLTg18Zmxt6WnOQFjX5KmVdGd3Ktw5+NLkkBlB/o9y9ljL1xCY0JFu666o+OdlBeePoulhzapsCMVE46sLb3SGYhQt5Zkmv2UlPp5dX3L29/qXBVZ5NycnoDEfPQ6W4VXaHiToNkBcDpwMgYegDJYVC0Zxwe8qBuQ52i5Wgv6kw6msJvRJU96AWnKtbqONB8MDpnCKp17PTuGaRpS+ZBTZEM2E1Dg3WRJpx5i7dqYcgdrI5YNKau4kgVlT4kqgZBIUvhsZ/KdJYZ177YovhBeLYq2BxDuhdgIM02TzBHa1CJpJaDkWLITCqKIhEA0IX4SnUOJ5+sD7iU+tsyVn7GXD5+4vVJvHgaSwa06w/fS0C1cVQKS3d4RTUNbODij6hl7M5tugZrEqsROjzjDIKTJjXGIRSRRMWgcRPSETAwunLBwFV5Peug6h5hIYFdx04ATQB3yYu7POvtOxMFwIhpk9ijtwCcLyWvdtbUKWY4HOMyCo+eS1UH7DlVs7TdYEHtgr4ssvFndx9b20rymn4l5tf3kcnyH3yg1BC00YyJEKB7QpsfqPB/wxNg/4SiG8KFNpc9J7MbRZJ2k4vgaYEZbOeWEfDZ3LLJSZZIZlzQyiSvFUS5eyM64SQTk01lzdkIAFzUY9E/I1bzeKOLNtkGG3ERoH/2c7eqxP5bqbxcWaBsFSjlNoB+JfGA8VhJS9QxdWI0aN5dzcHkUuenqGfu51vtDWfC7qlOTEXJ5Vw/vOYfKJDfhRR5uiRAVHDrLoVMuRD/uSiQHgFRUe3CrFlntjnwDJWSUmvbMykzwjymKJlt2I24cc+4kFG1YtjO7qFm9S6LJkHqCngyvJ9FC4DK5s345nv0j9JBP82E/CfVlfJx97k6GrRdoy6Cfkf/e3d7f3dzNTXJ/wXc5h7Excyhjt4VEReMyJq1gIaIDnlktogzESrnGvO/yoaYgoZk6mZGCQqu098u354uv7ku1wuo+mujUJpYsVgQAl7IDLjpupYssX3ZywMyZN/BjgiR59GOpd37+ATbTID+cEKoKbGFdiGPOyIeUVgiuI3BDRd5ixVB/RBjDrJp7gB5c1wUpkOeOOS9WK4RHyeaC9gAdnUjmlxtyt+RFiOcllr02J9YmtadCI4zj2t9osbXyGzOVIgvUG9xXZaY8q8Hw4EEZ8CLq6j3vPX16dpsezs9MeggPOwXp/jUE6jBo+mne0vjNn9t3KeKO9CsRzbjyUlfarO1qEuSfcPrXrayNoe7lAaXuH/l0lHCHS1zUn+U9yWtAXDLrLk/2pLba85QYrLh/CLkqQEWmF4++BLgT192ul1HOewTbwMy4XD0SooWeQOIaUCgpf7rQphtUo/i1aZNgB08bIA3o7du6zj0zZOVSlse7hokHxaqkbXhiEsghDM6yBvU8c1Y0GofTDF9TYkChL0yuwm1wCUJHcU9r0NPndPy8x7eLaLU1A5z9KGEhpSoFT5xiU6aiP14dGO04CbqykgM4MPz46IK60ue5MoPJ4FoojMFGcnLYZ14TUeYdUlghMWyJn5uMEbSkjkgJp5/XMdjY29mHEgpZAHaTsYVLdzlwOAAwupl3awZYaaH5J8iem5FiLT+CFeJGcvPDYrSoeCM5elUJcOHRsiFzymU2D5vE7e84hZQ5xOEYhSZUSC6P3mh9g8jcJ+8z7sq0nLGNWdIIGDdiCszosmgBpQYrfaDnHltS9vMQDh0GWEYEWH0W9dz25Mh3W5Of5C3Se8Hv3HhmTiGRb0PdccrVUOYSX7rFNe5svliIhqYbTtHyUFzQS5qUvvDvBlkgEtU9cqs0P33sxUSLwM5v5jzSiWCjEAEHb7gGYBhFDkxBxj6NFR5EiXTnYUQ1gH5SFevQPzIL22F1yhCQ6xKT2bDMLMBlxiqniykSDoYXrijiEu3mPCvH1B+cU/Lh5UDkAs/o0w2SQ1sFR6S5GdeCdKtClSzjmjpMKy/I7uQN6jywc3aPDMXSHf4HHZNr7JL19GoViYuIT/40phptJfZ+3XcqifchWAfivG1RKlN1PYQp5FEA87f0MQhVuBigQ6n1IATCLn8oWkwaXiggb5h6T1trUxEQYL5hwIUxTlpYTQ0ldhBaj7RX+vIdl6fhTDmeHLgTmDN0kNAtAYM+o/OgSydANHrX1dxup2ogNX690+Kbicxe/K764AVhb9YJaqmijA877htrcORuw9+husZif7Ozc399v39e1VHKCZN3Re78zpN/sGFl2jnyOURWFVb4vU9VEk+f4VmFLw5bStwfPa47HnQ6H8ae4+gUNBPG+wnR37rzFDjJtQIEEfWqhCXTAkpl6w9YIDaEhh8uJQzwaoxIi6VGDyHisSoFOAJ0pn8wyjqt2FQsgLdoJN4DtJhYhk+biL7Fssplh4gnd4JAJI7yBCYZpP4NhEhDMLBOCQGLeo7TRwn5yjd+XwqkcdN2emaAiZ3OUxtcjf+jmhYygYVnQqS8LxazhWJEu4tt4M4fZ0cVEiHhLqgVaFOYQa+vAGLiE2WDe6NTLIQwTuVGu/58lCacjadNZAdugcZaMS18YjnzCxomy7KLaf2EAhUJQ6yOcvrYmevsAZu0HQ+BJgIU37T0EQhm0CSimNvBggQU+McWyKEX5550nhIfjMsyKz658pQpCwC4qGspQ9ZFVAZ3FUtOc1VgEXCiBg6Pd1+yh5pUcRjyvdEQUT214xbC/LSTzSdEQxhvDt33EzUwtvopojHkcj0bMXNdZsTaChQ/ksMRYpEiUolQZEO5rtFcvirYFRevadKf+tIdpiUvswy32OucshpPeHBPRtipB+neYWBKce/f0W/A4kc10wcSoCO5r8wOT9I1rHFwGC6FijaVP2M+d13MPwzph0vRfXe6cXV6yPKexgr6aZsdiwwvXKRS/OLmQviF/imxJI/+QzD00XTtUodsP7EdABXq0J9wl1x4KkgvOSCBMis7s41kvrPnBE6cWN3ZXjPJUYpuFN1Cf4JK1UqbQ5p5wpnirkc+FugXdS1hGWDBMtVJMr5Nhn02RMkZXXhWfBC24IK+UGePlmLW0vGlnPmaNSIojyQEiMkNWiVEPhVXGgjlhrc0RrNXUbgUy25fv6Rf+ku65wAw5ltYyvIxaYBiiZr95ATFRZHegjyCkpwmVK183UBLhaWdDaGqJOB1QKIInipbzOUOHFqE6aJi2GUUhfG35mRii3Zkj1lrB/5qgFh637egYwil0Dwv5mIg/nIwruBBagS2qObGi9tLqYoIlxj2FRdARwFOJIx2JjxW8wWuKM500ZnGJs9DMyv5W1gh2b6CZpvU/JxlDQzTu/sdk0lLRRxAzjyMJaFcjKh21s70RTO1UknpxMYKrCOBnbyUtsuTTjXGxc6n2DbOjKyfgrJ4NVgDC8HXnmpd1EAhf7orpTzav7qiNVMR2QZOJ2TX0cf52+aLaor0vJ5P4unKhtdJ+Zrm7O3tyRmPhzqZFROhF+9TyoxvtpIhy8XE6QGvbVSN+ZTnRptE2+tw5zvsS0a6ZnmAkw0440uf/WAWoj3jzt1jwDUsQXJhqK9qqbGKR4E67aSPCJ1SMqLwoEkAaSw712TeXSEYYdDQ5IWSypS2IfY0cGiVWAxxoRBXAIc7o9ma/L3xLXCsjBeQu5xV1dRXd16V9XK0Op6dc7/rUpEl6f3GYD3srFE3ZEU8RH0llQdKz5J5jxkMuq3Eh8TiPm25Jwn5BCv2unMxvlpPeuB4RJY1sA4Xb21j7T8GSYIPB9gk3rxFGKuOfz5P1WLFr9kSWIYFjZUDEsQMnBy2dnmpXEwbuMCrhk6T7jlT/WqecRiMCwUxcby/nx5TjOMfacdQyHrh5oxWhoTIxVnOwKf7iry49TX5Cshd/Wbo43xJfovvmo1nteo6rR/aYb2U8wIf5oAVR3+6coUrjf2uw7pHSLE0mOVM2SRr4Pub3aaLB0i825VygRXZOZpMyrosf/z1xslIKegHmnA764f3reEJewyjPeUxPom47oTSyeVqLNWxYiMOTdHMyCbXcAG5nvuKnbItTvXFA6chTMQOFL2AQkk6xSzo27eETr/xu0qz7FWUfQO+VvmE8fb3/bXtyfNIXzxr/6+ikf4htTfHfT076C2Kdlg+OtuHm4b8PT/qoxPCf+3sn/f3dvWeD3SeDvf3es5ODZ8W09+ObK/zw4KS/bCb0EPgnBn/bk50dZXxotzEegGxS7Xbd3O5wtGHHlOwObMhjm+a1/YdqjgPuwYA3TVkOwOa6n8PP+99WJ3s4oX34AA47oZySb+zKTzEm3cyKyQD5znC83ZM+/p+nJ33cBMyk0NvBm3bNRxaFwJyyKLeTJf1sDxalKelX2A5Qp7Z3xF+5ppN0jelmeSbmwxbgQtKPnwTfoggu/R0e8PHs+uL1hx/P316/On/9kv4K20dVeNfGPKYVhzm8xvgWgimHS0zzBQEkFHSRwKSbmLYT3e5/+y+wFPSuYAsvHhAK3kZrUeBM/getgi7ewbOT/o/8C2wmDEbvd+28mP1gYULf7dCfZP3QU6ZlX/0sXuDoffeOYR1PmQHR+kXBDLZ7v4XVUB2vkHzp7loQNwVeyAcmtjblLR4XX5HdnAEkb/fe1hTGoYCMolfVVqT32CKH1SLatnHqsE6c3Z7U/PpPd+E4SrhCxOh3dweMI4f/Ovjh7+lu//3f9f6TUcvay4Ee/wlLPRppekSupxbHqaHqcCZC/cEgMHVsDZSYLdC/Z5Eh7qidFavZ3MSk97uvjPbLyBK5xFoUjkipUO+/xePTO9UCzFf4aTAF+D8rFsMwgNNwl4Xwxoy09hU/bPrhoqiz6lrWKccrJ1D0+fyI9yQfePzvyukP29P6ux34v9IUDF7kw6cGNr8qLMkCZXrvSwmjo6WF3EncjQeWBgdafqJxMgt+sL13vOE5wFm9eN5rQXxMC0OyZns/htcSkzTRC4pfHx59Jkgh5oRvmH5E0kuc0spPm/vGfAcLXM9uzWGRP/z937l3gP9n4y6KjxspPBVz78Ty8xylGdJJFi1rxXs4w80tvRq2Jc7zNrjTQ4/U+fBSdk7G1DJ396W8XDafywdENgzezbKLcBQuQvRQfqSsPJcVSZdUbOokhMYxHkF+oB3UaIpjU1u71dErjYz7Wdm6psu4roMzKnrmcNRADd/4LQ5XvkXwHtbNoLbVWM/n4AxVgrpv84fnQM68/Cd7Wd/Nf/huqI8f/vDdztxuHD2eaBJI5PbA6BFI510F5nDSZoKUwBYdolExi3hAGKQVyhFXQ/+a5Qi7c4/DG6Qp1JsYJq+5OBPu4335MKuoRwxc2Aa2Giyy/JLsbXqUPPbbgWJ9wo1lrg2+bdtT9Y6yjtNqBtMearlMdjq7wQ49C/7rqfuv/HTp48ycBVzFLCBSa9yK3T4WmAPt7olwM4QBR22xKD+w74XUPwS6ZPTKl4U64+5H4K5PHtLoQfTaLIupy5lY1aReCoTm0E10i8b5zg4Wg208t9S9z3WcziTF8R3/eVn9Qe4m/ehSqgid3bA9HbP6USAVg5iUeFe1K74eo8zxVrJu2Na3ElGN7zkx6DeqHKcWpdS0qNlGuBLLP1qbYGVY0n2sm0+tEdtMZSJfOQwOCV9xCyFB6hShs8EBwDaqK1Bcbb/3SDrNspOOquexDrPf8VxMx+pZ7fzKno2rfVCOEwzFXLAADgAnIjPplxL9kcenjPWhot3bfrb9ZRt3T1rz0BFxg+x1DEL5DhmJBxI5ih9KlMONsvkY4Q+7Hk4Yr+AtONAme6B1qmpEbosHKovpTRIP3GTgyjeYwPmEpvY3HOrO+qJkAG/qi+49U190H9zRHvh5R09PDg5CT/TwyHuieLLJ9fxH9Um/f/r06f4vZA7fu6dnvc3dn93bXOf2/CzuJvyVqkpX+ZqwDz++ot1e6WM+2TU+prcTWfHGfkaLvRNCb4wdy2Pyfxeg9Ab4y8HxetfyEB58xr+hoXrHm/uW6x6X9y6PdrPeZTSJG3Yyzz1w19XfeY+Rl4dRkaa4fEv9UP68njFo5OzVcWqGT70Ay3qQe7vP1IHc3cCB5FtPQi3UP6o2otdk5RLYJx23d/en397DE/CCjw6j2/t05e092Dt68uTZM3OBd/+qF3j92fqrRoxg5h8v4ABd//rl+8vzd29XXmZ6JbyP8Kvhcjx+oMjcBmEeePpz/D5nJDe+h6sek72Dx0/cFWyWM27qFsa02nqESNFZuSDCE2Qf2+KbYiaIl6nz4hzu7e7rzcl3PLW68W29/FwWS9fON8np2C/TFF40yI1JBd4Jcgy1TfxMSRC0o4L7R5FZxPwGYUZ8wyfzzc09OW1d9kbNf2/9J7/JYzvJVeXWg76JGc4EUz8OKD9vKhQvde/IkjqpQ0vSiJwWQcsrQXmuj8NeplVjMFCmtWr3VG3lApcvT8BdoYQZ+W1cioTv85uLpuagDxlkuWnFyI0PKXKka4a/LpthDa4Fs5YxBxW6dA+a9o83nQb6DfzPmS9wSF/G6i/GpedQzRNHFxr+YloSdQ4vFtU01z1mDJtW7bLNZkfTJoOBo+ds8Swj6VyWOErs6Nss59orneQqKLDPCELgAgHyFZm11DQTyiD0dFe0HTyyieXeI0ujS+eCoFNcoNO69KaeebrY4cED37sUBASxxFr4J897OL8GLyH+HYsA8ExbuzgxM6vLqtOnhM4kZCesVO2JIIc1EZOwM7H2x7YZAvEXSfQPXrvhXk9UwsjUSvHRNEFgIdWpb25QFpgTnlvwWMA4ILW7NPbMucc9v/B3055eiiaz/NXK5whZtGokA6TWYMOIYV7x4nUfZ7k/YR7YxDwtnEwmh8GEermYL5OCyxTOKI50i/MOYJHuAw9ixUTEuMZUhf+aI0c58X97nbyH47FSok+jX+N7KrEvXD9PgSqXVWI5nSCPt5Y4iY/Nos7irBzi0mGnzJbU9aLMQZW0IWHlm6BQ1Mm8TVDTniGosDOUPD5vGDLxoMhxWDGi933uG53A/7I1qaOmnqOqg8uUSqpegn1+7kynnp5OyiZ3dHt6w6eOE8nSV0A1n4LPqMTPaB9cGf0B/55viz7PswQVhiMoREA2UkgyowuvAKkMvjEtnBEDBs4xmGK5Jqmrj2jViq3AJVk1WIojw8n0q0te8bDgVI9zsbIQ7BLsyntsx+6WUEBP1HVGsH3PpeOPMKCsmSpummVLchxP8Ra2Suxnp03S2J0Vx35kUMoRu4WbOIixsphyRyVvYJTY/JtSnU1JTUak2Ba+oVk0EKJ1fWuka4nCeLv3seQwOi17taCKEglAF1XyI4w797AX1KLzzsF0l1NpcctFdsScx67Sgh6o+dxJXXMLD0JRUkx2yrF/7nRaSnU/o4jhxSdMrl9q/5IqC73t5HVIj+ZpALLZsVWxXDZAwUfQYCB67d5e8Kd8isK9lQYuNivRIWwtQjAB3wRzPrJzfp3ga8IJCBeaHAEnXjU+r9LDkd4UrWELjCpppohCwpwvKkJMj8+Kz9WtRhAdF54F1nWtfsuvktSK7W2nyCOB19ueX8Y9WmvEp6QGfg5+K3CuT8W77L0VFzTkY+N5pHcgCPAQTR53MvDem7iufG0zVUc+zrN2khm4UCJ8n3c4ffjdTE+uru9mYUjz5RAuheZNtnv5aSbcQ7uZfX1RfgbHfY5SURTklkhSF0mxVeaVYyR3hPNlOWYJVU4Q8vu/rQZHHWBI68lXQqOeDvZ24f/vPT3Zz0OjdjeGRnXES7ZRCv11cVGrgzd/tTj13q6NPa0Mbx09NbHqZjlbFTlq46ARR6phRsXoZtoRkkb4kYJWBGf8SjyrDUNifvx8DBrzA5Rz/Ld//Z+4GtgYUPJ8DB4GNcqQ9RBr4x1sYZ/GC/G5GnP+X6lGKPgsObf8i+QjZodPDjRi5mBBJrZ8wRMkt1yHX84QRQX3p81lzQNYPbeVRZUNz7tVYFLhmr6wblcif3kXTOZWRLQV41oI4pbNsvqaCjoYKCeULNwWhoSJ4ORt0+Db6dmr3o9LWHUw3m57zyf16FMCJxLaWVSb+PV3UgzBVoHjSRICkXriyRZ6j7g42XO1d/V00TFRIT/OvMp+8ioRqowfBOa0cO/wklel7UnnOjCtB0OMEgZgxwYEd+B6WjwMy2tsDn3x7vLqURZdYSE4y+mkWC5a4YQyjXhzPyTAAK7GXTW33D0IadKFzhefhbWeCOZT8AG13QsfdqZct5T9JGI3Mcw1B4p38hp2/Jru17W6iicnNCkWc9eCyDN9sQXZw6irYhI+1ZcS/vj2Q+/Hi9fgw43KWUsFGmVuOQLuIXh9ewTJuntULcioR3zEkEjFteaJhEqKt9oADEP3uh4R9nQcEpcKo7JyQmQO6545rPQERHXiDJGPRgJkZgMRZ4iptxFFUcN+HTYgRDzWHEkL0DAXpiYTaWzR9YNFglfnZ1HzKp/j1BJSTdHwd69/ef7i5fWr85evX1yfvn377ur06vzd20vay7qmmtVFsywzb7uriAn/vkKhRtqQHK0b2jUXnyBe32oG5kQbvEm2iiEWOZlf2J1TldErxu61s7M+iGfNY2lslNsVgIjkLGhdz7Oj7OdH6Xz17CDJgUlfi44gjiQaVb3h3Hj50TSExIURpQRMY4pZ4iZkYIvyIXKLvZoIgra3t79xRIkYFs3sBpFBu2Z3Yc1XLSOZs8zjI0Eg7TjHWsHhn32z8HEGx2QRPI+FCb/HHUV7VKWBzxR885fEQITNPSkGhl/xySqKvQde/ZkYIiDy4OKBUBzWX4LxNCKKxoDjJsBoIRaQ20ubua+nAaOTlwlWDkjpoazaIFDZN5PyCwEmbyz2+D9JSoR25uNvL88//vZHA2hMKWmiUaNVc+dJ/SRznni6HCK1Z4EtG9NhUHrA2iI9d8ZmwsUkYxbJonSJSKaAkDPs1wjtfzrN7jPy4OlT+yCdsINQ89sw3/J42aiM52Awi6Bo8nY44StgojHU0ANGRrRmHVxJV1t6zu0wy+NGwwvkpuh6Gjv+P7Ynhamq4pMb2AvSAoVTJ+7m0s92cBv8LwtPkOauumF6AgfIzsyRY7kjhaxysTAJ919I9lAvy8LSe5giyuBawAG0hrM86BHnyra8RbfFvUJBFD0G69POmAj9sd9SBxf/3Ky3OeAeCLor0MlAeDqbHV+kfRjN3F4ycdJS0LKW/s6dHtoCnGUfuyT2fWPeYAZP4qeeezy8vaSjCRo3JhAUjHLUpUiqKfM2YPTfVLDC+TJXOTdgx3De4raiQFv7VNgi4MYumyaofcAx1RpfJtXNDWNEOcvJF99DU9OfTIsv1XQ5BbeYmcEnGG+x+tK21gtPavquiRHj0oy+n1hDVYGldnClk0ZtAaSXqud0egT7NECjG1agwXJX91NPcfQ4Y8jwqrmCV4LkSrNmbwFnNq3Du/vROp7pOycmUJH1D0EtqMEs/w+/K/1IwPBXqmYCNeNrtl661sQhh0wq1U0g0quw9WkgAC3r3bp9SwywNwzDRjIBMuH4YLl3opbK6BdYjcgc7VNhHcBIu4LIeufM948JB39EW26VgulPqZ1uI7dsS7irvBP1m9fnr15dX364uHj3/sr19FyW3P1Kel8ReNl6QuZNk8ut1HO2ctIcJ8fda4Vv4Dn6G4xKDtXLSe8Cv/WOdm2L61leV7NPW733Rsts9a5EHeDfeX82QNsd/hS03ZPB/pPe3j4GyPaffRXabm/32fHRU4u2O/xrou1suOyvFnGE6cFZW42KfZJHxbJfiuDpLVM8rXJ8xiXaDkdNEUecKMPvJ5MBKplm0Vlq+fGih+mnc/rWxjHH7BPy8Dv4lQweMpGJKU9tLCWGtg2n1jXus9P6BUPv/v7vsqhVquEUprLgNvaCWicq+ijmlVYrgcyBdaB8C0xCNQQmgvak6odLMagRujhplKolBgSWSmo1covTb1BCfEOb8g2ZR98k4mI/FRc6v83mtpsZUk2lnHtKxR20iuT2ktGmvzrWwih12L6wtewMRWmIh22nAhOV1CquC4/ce4RnoagGx0+3evrPvcdYYqWPSiypVCn6WRLjqGeD90/OeLXG08uujLWVwvI8brvAQRf9v9f3o+nk2tlD1/XNtSeBQSv2U/kQMSpmn9phtVDeuFnOF2L8NYt8k50spWxiCO53R0twVbjXCgNpkJMftJGcAUqc5ZfyrTjNZNyQPr6v/oDms6ANQHdWrrdLdIA/zKrfg978p5LiXfJfwklcOV+EinFyr7JvznD+9DI7H9/NgEOEfCaCtzJeT60cT6ckz9jreIZWSSqUJxQ/xRCJj3QMjuNIgmJRL8gzbkrXPZMqdv2V3c1q3X1Vrl+ndY8Gu8e93WeYwdp7Emnd45Va93h/d39/zyld9/y/jtLt0BdW/8JCsOzr0MC73RpYntmlfPXjtRD2PZkkoltHdzBNhOPDO+eVJ8zlDXzxDL+4OX69+xn57B0u8s+fvfMTD+m8xbrOF4fssZbdzYkdRn3fuJZ1ga5nhErGdHZugvwXX8/shaGnfuWFeTrYfTrY38ULc3h4sv/068zUw8P9/X1vproJ/JWKQlYdlL+te4OrfY9cQvUGZR8wk9/WmNZC5viNzc2uJ2TvzNN9uTJ9vjD/t+Pm5zIQYZanKxPmNIjBAyemd8bnq7J34gjLAqNsQRe3BV2T3osa4+cIJCD8tusVG+WWCcbLnacX33iMndW6Per7ie47EjZh9uaOuhgS9nJY3nA/8nFMVqf5YMxUT4t5MOJ7ZfBV+gdcKdeYi7ctdGodN0K+ToyHPSWcP+JMTSRGGIx9OqZwSEak01hOxkRDwPCCwO7rdWRheH5ipyDCpJwtYpGzrrDNRSp8X7vMxuCy+EMshHOYpl2UX1Xk9hMMgGOwAXq7xycgjI+Ov0qeHRw9ebJ3fBwUuf01LYDuK2ylmcqcfw85BoNTsIUE19NIcJEerusJhuav+FsxCmehf87IpH3k5sG+DFhrEicPtqJsnMklPKTalLBo272Xs9/VD3FEQKKPWzx4kIHw/tS8vi99BfgWWcyzkpyT24azKUIDgEE5cn/B0mhapSnOmwdHz44OWRgeuML6ANFj2AyIC7cl1k84PDfFqHRNFvB1OXZFLpHJ2cE7k1Rns4fKWQlU0YKbykSHNCo3oZrxZ9z69At8Bw5iU9UtkgE0GPNWZmp4J4wXb3naSQpX0wcSwDafaQKPe8vA2pWL0XYKjDlwvAHZQETQsWkiL+3pF75po8XJ4iJmnvCCWh8amm7bOhkh2hHMBgaeYAjU0UQYxyyHCik0MqCUdCCq0ePyrrfGwhUHg/1OEHhKNRckSelnLovExxOpEGcckqCeIXyvglfJI0KYYeR2QpUd9Bb5E8Mc2i6RqKQDguHAdGuAVJMfzQ2YIXzuVTV7eHP2Uvsu8fpbcHgRZnhVC/HbdczxEcVRaKhxNZY27/N5OeMc4u0SDmy4vXzHO9AytDMmE8TLK1HEu+V0OCMGWVcbSjAe/XtviNztRfOQO9HJeTbJMKoM8ViX8ssc67NCqSYFJO9fXl4R6D+Y/q+lrR3eX6FKstzkpuuJMLvM5ktFRHBCuYbPcWvNC6vEEJoUx9w/borbAUiLwRhrOYLGzVumMtDder6kiDwC4bXFHHh1s8m1VJyX4DfwT1wNMWPUu4LEHoLGuV7wvpffK/ABQ1DD+kvv0XC54AbIW/wjJuqkrk42MvJ4u4dEMVvIIIZ5IAIh6VN4Ul7Iy9j4elQaUmnRHKKKK2nQOCzhM6zAxdt7c8OvEZCijasCdEKEarvo4LLhpoS484hLmwq7KmVtwBxcjpivRqoDRVYT3+XvlwwjJEQY2tPY8eszJtWQ9RRrMx6hmU9/4V4V0lucPny86ezOUX9MWei1Ff4HnKbBtJySzV4wfHJaoiqs2invp6bI4kysxKbG7KqrMEX15ehQWxVnoIRgF9sqnOdHu4tIL/XmwVeR900/dNrUlzxz9Mt1j/sf6VgSFSFDCyipZuRRX/aPNtv9Ti0f16EyOy8Sd83nkFAdVdk3rW/uws11VRXJodOIo4D5Cv8qqy+ZaUaeiHmbrdTaGZX9CAd6WwpV7ZxoNBx3KoFtFYxTMePZ+llgln3gpKrRwFtMRg8mGTa0vCNmJanQ5AWSluI3objcQLhQGxDW31gh0aFfUP5RwE3FjY+nFr03TJyudI9MRVXktCKbi4FY7DLT1q6VZlPA2xx9omA/IqEC5SqteQK8VF7hYdVkBhbqjREKfFPxV6JEJfo7KW+L0UNqM0tgecGFYjhcehY4hKGTQZPoePsgx/q3LaGNjotDssBXumIf1zlpQWoCAYKmNMXzimbH78hbYkVxg8FACZovfLUtGWC9Bx+BW3GmiIZMisfGFt9EGmFYUn9zJrusjBmleeeM8SSv6KBMajIyCsD1MCoivBRabr7PDT6I7wkFOUYLbrtC82E3pMNUJPmP2HfHaIcaZOC7KOu8Rtrxwhetq9Tm21EQ8yF7UO7LrQAGqttZ3cRziDa3cPrCXAUsCv2ROuYQmBYUYCkoZzTc5af4m47bLY0uuPKfjihh2dcB3+Gal424jROGq3XcZhJVjf/MG3PaoDbqBdf90ERCr3gqn0Y8nAjWwbXw9QJgXRZUpFXP7GgUCtoSjr57xDmS90IQe/Nr+QV8Av5fNdH2ORQ56ly76JbgbwiQ8V7xj+nL6ZwXTXV7axujW/2SPTARDe8Y1pctBPQHpyAfMSso8EE0KEm+jSUrW2BToofBoh7g/w0vVdXql9e944w9M1Eb0l7DKIiO08gXzFFW3TrCBC6lhTsd2V2akihIUxd8EajU2FS/OAl/T40nOSYbtkQRu8HApL1hgW4UASAxR7/duyxLltl/+qNK5KJ315Q337vo2S0MsBxSh0mXGtm51fns8Jne2Ts6eHLc7y0wnrD4vn89hIvwqY8r/n1/VoO9MaNOQgSRbcqm/8NPGP67neIHT8SH9CJiZW9wSJczLWsBVwk2fiGMOhIb9tU/3dtp1htrpyeTqnUApdPPxbigfX1ffq4nSxr7Eo5/2aw/XSJ1GUhWYPZi4mmBb5L5tFYY5S6e8y7J1N/kVkkfQ8mJ540DfM3OxRFy6M8iRytpaZwYEVvCuEnd4J2BvipqwRJLu8lw4WvGId/PgWIz5yAw5kSlSXX2uJTEty/Wn0jY2xdr6/6niIRckcgdldxV3AYMD12D/Qspr86QGjMLH+f3DgFf/jlyw4wo9IFrFjzoJTaGdY3PuCwYGV3Uj0edTN4KFYJxTRQ/8NFjDq3mP5ZPw9Bbm5oD5BhJnA3hEIwC6Z0vuIsM55jQPX1w9EJU4FOA0Xs7s1ZVdlMTpIzFLyEj/2IAZ/2cawy5cZLzKYVmu0ed7G4bbKgm5O545NldF6rZ0aQiLtl0C70VG9bc9JXEQWWeHAuSI7lxZr2+r9Or4F9f+m5IOW19dcTwUveTQ7cCEJqdeFPe4vuTFYcWEa7nkVyDU45JYmeMir/iXFKybdlCKGfeV9Q6CVhYewCyu5ZAst+guOGmp0awrPYBA3Lpz1XRVSgXvHtnVPkvu7XmXDl77P2rHyXIdVeB7YWqBhQMfp3NZtHJqwb0hqIYtTSmOoyu9I5vIXLNtBqvoGODziPPXSNmVxIeUWlOR2zIVPDpRE5672Y+8ObcQI4QUNhFiqO0HTNLtYJDfRizWbDXohF98o7eB55NWnkkj35DQhDDqR6QMSRjUQLOgeeka/Kr4nNxScGGtWHD3EN/qbEkJ1q42NjoKtpkCYQI9VjdsOqWdIux1fjHPyHgtPaYiTPE4UbkuZjEaS+hEqtnA0LkG1J0MQ+qhq1RkMYsDjrvyFGnE5/e/cPOTI+X0pJ1p0DdQtFh2dHSOLvOy+x0qag+G5y7tP2T+T5IZGN9UEPHwuTiBrals5eiijYailP7nrSLmpPpD1xeEy2DyLHrdL+6mygwuUxT/QF9kwmzzmUMyFWhKas13JhdgXfbXFA505UdftWupvlQF1kW1d6fl2zs9PW6TxVrYMIwU1KAxOaIWBVXDbFQw4PQ47WrNxN8sr+gZLYiXB2sJg21EXUVPH5ADxoWbdWKHYYNj2cbBmMzQTgfahSzODLafWwhzWBL44UiaELi9kdhJMSmnp1U0EitaztFfGsXO4N6LVxRNPeyD3SzsePT9Gp2/7sqwIzSa2huY24n1C6HaqbwSlIKi+8Tp0kN172POmTkmY2UR+yN3qs2/pggWaRhGlKwwY9cDVTm5fa6iq3D0v068NIjk1pNY2xCpOFlf+ozb9Wfc/u/a/BK66aPwF/fuZXR5MRngXgPwuyYbED2HdJ0/YXE7YYN3CsM+tnoXasCc42qtc/oqCbnQh/O1veZsPH7prjHSAJYVcQaqW+RWwmb4NVRls3ke6xh6ruUMr66aG+QdUTFwbUAwYhXHuPvbppfVkTP3F8vq77Wp94UoHeYR80XE8Zgg0hJJRmkwHmE49jfpd2UV2HDWNXEvKlIXrJ3YBcrGI2xWaezYvLQUvqXgS0a3L2EH0/KwWvwS7a4vlz+fT4lO+/D+9dbcZThlVA2jKMMf9f5pFcB21Wc+RY20DVoRQSgMMrQSaVv4HoTswF7lnBxosL2do4CXZdRvBnnwsgqB+E+klXBKL6tMyrJIODKBFj0EA5V6sG+oJjajx/Og5Fs5w18Tc8jICkl9tQaggLA93ZEFwSD6FK+kqa+Tktla/rx/azbi5W9o5L5oKnvOTL4sqORfi27Rw8gYqK/sxWD985kBHA14B0jQ0WQH5J9c/MRkhb8gcvATAviIKWesDJoZj4k+YYPC9vNp1fPgpgU5UXIxYJjWY/9/VKNVkVUDNzxu6ZCSSmnQRIaFK4D0mAmlxGFOiRIBS+y07t4/yozY7XpY4vMRrPM+BlD/8IlRyJd4ZynINshnggjiVhfwMucX1xhRx5e7ztmeqRiZbBx5suGsCimwJspGUq0UNtIgLLIJVezmuasfVeVbso9FSbBsS/GjuDP01Am0mTKfZNXjFk34kXW0Gti//JiqzjQjmx72Skfd7gU3+EJ+oFE2DW/+bVU89IH9kHUfZqC+J8IX9CZkK2bOZ4vPDWh1yGAX0n34edpPkB4scH2KWdLEYw+MS5tfG6IXh4ceHRWjWmc7dHVmZPiszSlENnCVuJjLpUyikbC+1uoFItdIDEzdoPYV0nmOhHJF1iSiVSoPsF28mRtgxp1r+xgeVnr1gPpOIKtVmIRiCG0rBcV2bBCbO77YeWGDZlDHAsBjQwrX/POwfqQXtt0RVhJ99o5ojydYPOmJ0tUztWBVzPDTJ5ir0n75wbVSSoGkNP4cvciahtQC+QWrt4yJirjUJsNpdlmZ1sRSMUe8HBkJxPGvobXxql8gEcrDx1GjTAkjMGPOrp15VUpROYZ7zjlLxl8prKnns5r3p2FAesLMhEpTHtDB2ahJ0glg1hn9FAxZbD+bsC84GJiOiGHOIW6KQxiykEZ1kwqtFWjl9TEp0MIuLo/Funk2OTynWpR2DhjeJgxC6Iu6Lg2OpxXqS0w7P+H8vpT+aBLwqm5hfEvgncySkbWNoUw8eAfL67P3r29evn26vrF+XsZPisn2cfX08U3QpJoVg6q30yOrTjEztvkE00jGeQVMVnFvWqzuuVJolvSyl5ZNPQeMPymK+aJ7TBWtw78lPFjYy92cddwZMa5/xQq/dMfdWHfvL4+++XLs396/u4315cvX788u3r5Qmbzpz8q8dqah7P37V2y6FDCS5lwGpZOwCpnV66DxcXFUyrlTPK4iwCR4h4Z4A1NB2ZXT7w24E7JGePpKe2E8Z+paEpKPdb5PmFRVXBMSZtGZzW61XEcxYwsAXCci1o+Vj7zYab4+5UjZeIkwQSJOTHwGdK9dSaoo8gVq0a0scX3xM0NPZ3sHqeh4Q1NEPQwQkKhNfFQ3ki2A/UA5GbUUTBuQkOi3upZxpvsDX5g1B7ZrKmnQfjAgOlSyOk/03luyYG/oQZlYLPj9piQA9t3ma3R4io/TWdVSCjWh6kkMZNLdeAj0SIXV+5zxattSYIWapvCFiBVUA5Gk5lg5EPNqJ6L9HI2wdC5fb/7Z0wb917Au85hkbWV8iog+BljyXsv1WZiM+Nx8GCs6r950Jxvb1ppOESq5bD/uEQjtAW5HvJMBmHV5eHcVKOFhO+pc4y41aJuSUoj4kdSkHQLfBmKTIlc7C0EcM/Q0a48PoshXoVaOrDwSZzjp+dfdjegtDU2oLk2bEBidolSTHU++bIeHBt6TaFsdP1eRYtzcKUcd5K0rhqaskROd3ahpc8RCTEr8YDNJ3Czm97e3vonEQwcExfNIgky8/TFosOqUJPwv7x8nRuc9ffdYjpR28GBpvkj7XPDH3pFhrjiUJF1D49vjTIfK0H+vJGa4j47wDp/2zP15naFFjZH+mon4Ow6H8mglr16TLdMMyermPX8zktiNF/tfIhV0/WMNSOKbVQ1sazuPk6z5ZSCOYR2jfnxuuyHdSGWbpbhKo5ibnn3Wi5goBoVqotZwMnDKowln4D7uQKPau0eJQeAWs+suZRMMMKH8cDzAfXWqhhNBuatSjoSSQQ2cmc531lT7JmWBBwp9YWzgzLwaIIgvwfCbhP4iMMcEr5pN32WQ+sZ2F7+seq6kS65Z1CCcRSFklsBkq6hdXYadHKoqD4KxQZIOUqrciN4fCKhP8dc94fpb5hhVtmkWSfYwBOh7uk4jPFrRGE2ui8FWVxpBs0845yPsmBmRaFcwI1mtDmFTJDoailkpu1ywuWyFj4n9W3XNEgGXmc/h499D8ItpS6ruUsciYkbSkDW98zIja4DPZ9p/eRuDNOQwUnvFGzNYTtCcCfh/NOSWAWIoEf7TZDCL9xGVszjBX/p88P6Hq1AIal2JUoCltSC3QuK9jr+XO7oJLJyuj91pYEYwsZlKD1x5kJqmNEpw/hH5lGXYn/wOwbSaFRgtR5VM8KrTStb47cOjmSe8LypP5GwMDKegjkZx5CwHA48KeJfTTcGwTWNq/sdEfM5XpWYPdrwQe1v7z3pZoOhEvOfSlp4dHK0d7J79HVsMAdPj57tPnHsCW4CP5E9Ya+LPQFeVS5QnrvQkQzAF6t2gH6tPtHyKOxRT2HWAZNywDiRr+c2/JmYFZ4S9wOKlDy5wuEzT67A/jpB9Bmo8n3/rrq9m8D/LPo/vIb3zXPEBM/IczBg/xke/jX16Wi1qBP5ycYOU6Lh9+VkIrPh5TNahTJuVDuIXArwTTr9ZdFS5QXm2tiTe85Aw5BlabvnJ7EFXoJwPkofb012sLy5cVQ0BBd1GDpksJ09sKCrjFDKEyTuIvsE36un24d0aOIwQICq/uXVm9fIwudgjEIWoXHM2sXrUDyBTif/TCPYhBpAEX9fjRFzifXEJW6ft4ZDvzh49o8fzjkt7kqUMmBOL36okpaqSLyxJ+HMWmGaa+NfCmbLtt9kM+xW4vSUZaeXMa4Lq3q4VMNqZjhcT391+hsJvvdqat4pH68NP6GVdEdecopjLUK8jIJr4y9mIw8BbTKfQi2k1hckDuWJtkzmpMwVqo7PFDs4KyYlVgFEkdoVxqc+U81PXEEJytX1XPkFNdCBn/ROqWD7tM2x6egxPsgeY52CJIYq7wJQ8C6t2OaUspr1BlrVUqqOGNUXYL2S2oYhMOQvgcvstPaz0wJjqyGwH5UtVtbERfNQp9U+tFjuRnEYnG5mjUPz2NSti9TS5qR6c3mlGUYsq+GTT2VQhLbR+MHNY+Mgkg7ElFBYAZFdKNDTXTsoFQqdoE/62DYFzz8gO7xSmniYm3fRJUh0P/9MXGvUYSQ0dGDAxvKWO9hiG52J4FmZLUagG5nzvNFwqMKfaoqKXjXq75t9293u5UzEnSlLOLu8pKv3q0t6v1iItD7cXTWuWiFzbDZ4TH58UBgsyrTi4zlyDT0YsA2nhmVvNL6x0cNx3TW/qvchfIeOSaksRD6DRRbP7O/QKbOv5TYlLwr4gvFJ04ZclF2R2oWzK4OkeW7w/9339AGLuRoCLSABhwsft043ubiQqCOW7t3HNbjmvnb7PUrCNxjgJ49Ywcl5NRAom5SQKpyJrDKpUE1YsmKTL7DH6HGwI+RvonBSo0NJ9hYPFZd3j/KcwdHUPNsG+1yq5tXyAsnHUpTcKGnJStUkM98zjL/9JV+XFDyuWGoADq2WGpdaMomwd/wgvgvVjMrLqOZEqh2K1lgRvZIKujIn73j7KHv2LJROotNvirmBEYLnacOx3k7tttdc7gO7CnBWxvV8CcD+VATnGP+KWywlXrDryN1+wO+JMY8MLWAMwZdFAXeQowXGGLNkV+zr4zd45niVuLXqaYcWzbX0QyuwlLjl5MEexcVdhZ1EsDhRW76QYBQpJ6ZULoM1r2ZYDszmM1iOrZml9lBU0kay5FEGtTYenj9QFJYcM+RMFzu6WFSWSmaVOOPui5SoMFXdLQFj4TXqRU6eHXe4Dpk6imnxSffnvkF4fdN7cf5rt2+BsSeHRgSyXro0HbqlsKkhs55YWK8aJ/wClDwFjb5g7CatcnYZzYyjEiPJ78iAQpiWKooQ1GYUnrwNAy+za5m3X60MvuFZki28AEt5yP2U6XW2MpOhkx7rJTXyVliRmRvsxT92pmMoB9nNXdwCf8bwQspF5hALdpdMxZ3Ok7+fq8WeUVdildq4Ex00cjdIwndPllfbhna5dnc8dkXwy6mpE6IOjdN6zBEERnsRvSxcwHLSXakaXGDrW+qMnRtaG+VNDqVRAE71baLmWNhYfwfLWrJCQ5zQOwzFitPgvihss12gkhnCaIjwb/Tgl4lYK8TibZxRkh7h3Fntsu7axYPGUxn67iLU8F3reosXo2g4aZHFMtHwPtHxkMFYXLb0DLVGMr64M1658S82mW805vP/zHf4ddVyoz0UvxlQC0quvIurUA1YAkTgRYhwK6zZHjR1uJFgxwutFIu+9KuyqMkjn/daF9TxtACyaEYeyXXAtg5wrm+lYxD9GhGUhTDaTYvmU87HPP5Kn8v44lpw4eDmZEdY5x3LWFkaYVC940oYukRD3ULRoY7ImSep8LW9YgKUQZ/JwiAAaTrpWBqGyi3NCu1moYF8+sjt6XA980Uhqic1aiyL6ui0RWpFhckSVOSELoVyRRDqsaPwcH8Lu8KFs+mTdu0TvqovT8tOyESWanmcamyPSWOYlsEQwjmvowmP0BY8k0pNYaP7M567pSVDX/X8i6Ip4NrO71Y9+td6ZCn43qN8kO6hLPg43ZCwCJXHzw2POmVsWuMR7oV/Q0KDGN7wEOVO+wy8BiIkRX2cfCrZvOTvVv1npvR15/d18QBWT27l7Di2jxxVAyOKrh9BMvHvGJyi4h0QUGUzX/St7XPX1Mvbu41oI9IpFArgo+f8om/A+rJ5DGy6+VoLNPPcIJLYapqfo2nOa+EvUWEu1iSPDMTauQn89bW6IFTNJmxPlPnJAeDztRWqWVauJDCluTZx66vO5t1Gnb2R5lfB5Uk6uUuGUp/RJnwQEhdYjIv37/pU87VstCWeNP3CCpIFTcVZE7MM2C28O6vmqIH6jv2l8mg3R2f7wk1rChtP1Fy7tBuVDQ369qCdk8lwcN+D1sXqnC2MuQ994EarvPK6+ApzF9xj0sW5Sr1W50LfYnID6sE8qzGxP+/LkfQHZZMsTEil4GaoUfRBXLqMMWCuJ6MZ/Pjh/AU1OtJYQgh9nPF2UD0E6W+HiXUyhBLjGixhg1/Do/1LtjeJaMs5Gn3nsfc5r/lK8BFxWagNMGmNSM0leXSFHJjGQSmyAV07DOtc0oCwQ335tScYiflg+nhCiskWNc0FxygQNIQSk7H5PPaDEjr5ccagebJ9uNLwZdoQQv8LcMcAPNgNTiJN7u5v8X8z8ciXVWkNnMbmRqcRLnTW+r8sQZw0zpipGeSSBuPdBQrMdLCn51ia1FVHT9NbFdaYmUPB2B8m6Ef9pnQHckgpbIRXknLRpZIwM0MgC+TO4PVr8oIk1IDejyowm3D08Tc3GTq15meiHtAcUFirJI3yyRoKIHgWdLlaoj0wiqAM9zEPg0qZqtXSeop89v5QNohDN/B2Qs6tFX2p1c9mPrKtCfTGkFux0R/uvTs5G4nZ4aQYfVKrybifrbEGWL05PkHqYoFPnxBTPE94o4cx6FmexE9x/i+ttEYZmUGP+hlyyz3tFEOSc+zCEDyGZzAgrNd6wU2mo1d3WV22Rfupfi2j8jqtDcrsy7YQeEpAp8EddIoIiWfxb1kTdrPZ90XB9vVa0jto4lYDQihetwysFibAGGKOsLZbDqqqhBV/4acLbhlFbDoPzhaGbetVuRG6D/P4xYz5gzHWSzQMSt4aZ3BzZ3DIwDgb1nQNQdsAEUI2gd299YO/WpIQFENVuCMp5tgKnAbmL4bLHbcTUCddzTYOWuKpTAy4rd6wxjdAqi9ze78iYPXxQq7LKxModIYRsocllnZjkTnL21tsqlLPXKklbQ6RzmlqjRkyuGU1iQr3o44op2Xvz/PCtxrjoPHmROwIK7SDHly9MPRgmo03nRkIKiyigmgds8nTrgVjczV0X0UxWLskyCOQcZMcUq2Wq/Vf0peLvq19ETaaE7kvRlQbiHNeiEVJ+lAkbSkSfCwA3qxP7Yo9uznXLOqZW6AQHinmrP9DNbe72ZXdiSKRkTXGh9gNu+OVikl/lpVk/ZzsDnqF9Oj0UKZ24Xxv/HK+MHB9CZhQq7GzKdq0z7irvrF4M2isvrZsQhHob86sVvc0mMQZM6azP0b0BjfhdYS/x6mGcUks6XSnpEuHcwrIp2OqlPGyCWINgW2YT3l5IzVYlBPqXleohmfrAXYvb5l0DlLOPOev9buNJ0Jtll15RWQ4O+M4a/EZZE7Rtui9ezOCI7e5+liyqxEjHmFSNEuMf4/XPwh/x5GVYJUT94BB3dwJpxUB1PhWfioFQgAuIWQNvRyTEbMC5GCM9A+D2S5nLr7n7PlwNB0IrHTRmZOHLQY3FePPKLnHfsBJ9amM7AA8aWYxCgKgTCsXWKxkwrnCvhRAvreLDXgVCfuTmvDt9/b2T3YPT3afhTDyo4OVMPL9w6ODJ7sHpg2vncXP3YlvJZY8wkuHcPK9v2k4OS6HvNiAuekHklrMosvx63ocQ9EcN+ddMW4eUX4EUz1l4kVpe9UwUlu6e/MobZxuyHQAVEuX7wtVRsuU0bahTlaxYgkaAD9w5huh5/pD1tqtMwy9qcjOF1fgtHVPjydxsTsEu8w9bQL0Rj54XQ2R1COLOX/6zHVGfdrJEcG9QJx1JO6OdBuQGuJ2ZUcD7r4hRI2Imq2kg2F1Q10/wtqowLEzPUGfdnIxrGBeYs4hUtlxfRa6aLNCoCt6Euw7pJ1msrOy7AE4qcuH2QjMjBlRooghWbrarXI6LGlZXYtVtffqmf8WVTVEIeEIZR1ZCBr199/8xodmEyKX4AUSsoEAawHm4F0t9B14/FrRIIynE4OGgi9UpkX0aLbyKnhUwkGZfVTNsRfhQZQidX6SRKP59GUfkRA42lOsTRDRkKV2aLdiCRkWuLJcSJqfnjxNCNh1QKEKWhA48F5DUETGE83ZgU0y8+7oOwv38etV3oFUTu0fn+zGjedXV07tP917cnz41PSdlQn8VbXdatH+89VS/RW6bR9LY3DwD0afHgZkw2Z7bYOF0r+kL4Gopi+FTbXzA+UV3ZPDE/I77Hh6RT+V5ZxZNokcxgTo0QXrDUGrULiYWIqDwAw4Dpi1x1+UY3H/YUBqw00gSFRRPDyxo83E5eVHLOcoUPGsbvc+4t2jKirmbkOa/4Jc2nbRLEdoWCKBO3iEYE4+cLXWZwmWcKHHVv4RBOmqpgRgWpSTh+2//7ucvtvf232mCu9oZYtpVoCvxXrlqDTzlsH9/tUl1UQumxHBgScP6q8G6+77GWbk1KEqsxVPf0/3gp/+XTn9YXtaf7cD/5cUCsmUD58a9K8L395J6s7v0WL/3bLV9Oekhq0kRYcDLT/RONlpHWan5SYG/4+nNoCLjkgGmR/lG5kP0Ha64OyBRV6qSnLcgc4/5V+JCgsi6MEEDzZYN65XjfgCotgCLsTpsMWeOCVtGK8tAR6SInPHAe54j9tytCRCU8R/5mcqyg6ONriapt28/CG3qs5fZWC8xTpf4O66iGPRatn8tGxuycGC90rRBLl57YXz4lV0k8r0NNHyFofW3xKTzsvbyIzRSBzOfHBGeGk0Y8fYQ6jNzWl30zlh4ZFpUE9JGLp0rtQhpLBXCHhizAcaHSw2pFhXuhrK1UupgyMcEtRSxdG9+YS6qvHBiUMOoSAIw1HcmgH3qY2CDlG9w9Vrz9w6M9JE4pAi1ithQKeNmoEPgWGS+2pcxoMrDoG5f+hIEXjIdGwja1ib+zhDjtkqJ5N02w62957JbZT/fBr+p6c0De4l6TPUMQMUqmWzPb+bSwSQHhI+gv2Sj/BerbkOWNp94L5z1Pmdffedw3BuLEde+KvGNqf4NcJvNCHyR9itkZT2lghHlPaYM6IBcMaei2roE/Y7JrVvJrXX+Z09953dru/oN3gTfNSGZRKeUqphcP02XmKLuvbOsLzoCLxvlyrTWKHqh8cdz2fnR77UtUv4Jf1O1y7tbR+77/AuseThgmz9zhOJa1Gz8C9yrecdPYHdeAdmvDas8IZRnzGEgjL6R+43XRuHgqrDXAdr4uvN9f3BLpjrBydHT092DyNz/Wiluf5k99musdXl6T+3rS7GbmSkd1qk1hqHhWBp/+9nge/LNEfTdjArPueNb/j32ZvL3tvic3Vb6BDW/E5GyVree0+eseUdDqZCGpYC5In7M5okE5eJxJAT2cSEFBYHlbI2ajOROx01SS/IYh+PmuV02KKJXk22etgpfYCHhMwrviGY3sSaFP/4bCho/+DprjeN014RGJCwT6TQeMjkhJdLjXd0HVKdmzGTjtQcgf/xJhL8R2wcuUiBvfJYq4YtdVk/j+5K042WkvQjyzoXPHa99R93Tql9mqI0DsCjs8vLnV9dPuZ4Nyq0sZmFqoe8hZi3tv/dnYD9vJH9N+QF7O/7GfpTk5tkBsWL6bkXWi3bGgbevqY37CNpyDUroYFIQR34i+KKBILONMhxSYrlrmivpVQHLxLZX/n3jYz2vxlnYn9jy/2v6E0EZulhaJYeOrO0c9qB/KG5/5//O4E/f6AmFYgs+z+Zl8a3Rb1xrUekX6YguNpykeYW0YbhHkwul02HkmxirIvsk1Ev/Ymb5QzzxFtCXybILFxF0DaDt6e/ZnZ64qkfc/d3x6YH/5E+3gTIyaH4Rm6ddwVcj5Hs6rKtR/pIs5OuPlVIITB9ghSIy/k2Hs3LUtosqsrZnjIdpuP+YEV5U/OJdIw2qHY0rOoY4HQePh/wVe4FB895KYhHhdtu3PTePJxfnr4BKX+LdaQhoQCJo9HkGuyAa1Cl16xt6DrHPsuh+horfJZDdUBW+CyH6kK4/9z7y/oshyt8DT+pZ90uixvn6WqX5VB8iqzDcbjSlzh0z1jvTRz+zN7E4Vd5E52ewk+gRDsa7B+gp3B4fLL/7Gs9hSeBp/Dn8KF9raeQMZ7/lpyEf0E3AebcLofu0/YawWXkLKCvgCCE8su8asQ5oD9XJ0+O957Cv/fpK/u4Yw/ttVAl2+fD5OWv1xJ/iZ4OvyWzTMA6+IDdk/8hn2BFI1FoXo/m4SfKGT+YFvTJ/q5Olg74G4yDK4aCdmCqfzmAKZ0qmMM2nW65yRaWpiyLiWxVMbqZTji1cNL/sQEXAi4rQ/3jb8O5vdUvw78tw33mq/cj/iquEE35FAT9OSVh3cKRGJ9MBpX/8758W9ysrkO2v6cLYZ6MPxLPh7+PtVtySKQWK8zCOD+XX+rAfS22i4Iv00T3dcx8W2T7g8WUtuXAbtTA1N/sSN4XlFh+R/EE0z7t8IqNbvS7dgMP8KbwBhJUYTA1SxM9w2wlSpf7uh7JVoa/oueZT83vdX8P0o3cif7bvVl+z/cPwv2N1yOz/YewRugzz1EuB1PGb+24j3SM+ETsP81EOKLnhofj4ND9Ag/Hiv0Lz8mBPsn8QFKeeE46fotH5l9omcF/uKNxYBWe7e0ejkfPnoxvdp8eDsdPbw4Obvb2j4un+08ODoY3xyhywvyryDjWOCzzDvAPMFhFeulwz+djJZdNnFv3oPR4ccflZzDn5nqoLVvkgcfzDHoXJhPuVWb0ayPZBjrMsRslVHuoG1XtyRS34Ryq+ME9RhI4EOduNa/lp7hZmZ/uFCNiK6Lz0e7ohMRzvF4Ut22o6uagI9F8xoU7Rq1wcPRs78ku/5dZxb2n7nxpJqloHqLl2lfZPaCy/uBrmYCTzAIWgcok9NWeHORf7X4+kOKNneWcUBw7YFjs7+we4lvjRoJNXu9tz1l9wU0kj4CAYnusqedFQ9tUnewf7T4B9cdrIIrIK8yOl9WzZYNvT+WVk3XJRdhQgDGc5x+0SBrtk57smrfupH/Cdu+39ZLbXiL4SJuGBfSd0pLMI4U0XjfzTYYK15Het9iRNZqDje2wCGRrGENCTJXq5OjJ3hOxrJLPr5EwKdr+dDXikavZTa1a5tJ8oJH+HlksDyrK0qei+f8ZQckLc5by8Eh7iv4RrTVwq0ZFs/j+4GjvydHRL4rp/Nvh8uF6Vt9/vycXAw5KeQ9Oh7dZ4I9LrpI1f4TZi1Dpsy3lQB1CnysoxX35w4C3gmQfHHw1cc2IaFTfwdKFA+4bKdbCooLtOhjdVZMABxl9geyzf4Gz/nTv2dOj5ErD1CdcMR5dY/jgtf8gc47xhr7gSnhY/qopBeanmH1KOM4q6hrXFLcDOIoDDOsycuOmGDn22PDid8i0FRdfXmDFzT/Y9Obj6uNiHe0d7qbyD8182blgreDvp+7vmaXCY3gmt/Ae4WceGklkGxQxl+tZGzxKfoH+ApJx9+vX5+DZs2R9YElALI2j1YEncbFrfnGOEchCDMsUxmP8Kee/HQfibOzoCl0lPnVRw2reQTkTMM+2oWpOlm3vq5cN32XFou3/lEU7ThYNxnSo6HDVfr2CXPr40N2+RVAvr5l2R2w2691Myi8cjATnznX+vW94QcHLz5+z/a9ZsL0dWDN6j59DAbsV232yn15DS0sertiV/jmb2UJXhsOxFO8zjbwwawUue0n91/kecrifut6TdnZpki1DCukpVug7LLKNvia4s1HaWFNyV07mlFVYUvHIdDlDV9SE9ZfTn2s/WNl07sfTn7AdFC1IVIgBQoYqxFi8uS05hK/wrRcVcl8OWYs4FLi3sRk4kju+nFGcw/bcYQjXpRaFiUb4Q0gIKS/NVoyixk1U7pQ/X+we7+w+0a/RBmy3n5P1PzyKN2A3XfsjskUPvV6HOc1GHXboofdUTuVrq4xua/ntgzxfYfsdH+0/XWv7dTy/y+pDz+e3ZUFNZsznm1p78LCj44PjyOLrMB8Ci29v9+kzsId+PpMPdyJv8wVlL7DK34abyTRH+MlBZIzB8RBrmtSQ8lYMuHqbzPGsSVmdPJE1t9EON4HjbznwSYPqxyoM9ixquZjTD559uxrNLNYDBjfcCZWo3IALVJzoR3dWKiYGqrQG5oVJo+KhuMHaTcR/DSrtqPZXs5V5BvoB2apc75G/caaTxLn97qbXbu9w3bU7Xn/tVkziL3r34mK0ze/e7n/cvf+4e5m7h2vu4nRIhz5b+HxFfPnwy3ruL4Ivb3r7nqy7fJvFOzon8Re6fWmcY+Obd/zs57t5T/5SF2/lkT/62zioB2SmYXb+rm4G4S7mFAXs2UVT907p+19hlq3WD0dPnm1glqXP/qsdzE0CcMd7h093/8Ma+w+N0GWNbXrJMA73lfdrb7XbA/drvf0VPvYvaXEd7v6067X37OCnXS/KhKyRdQkUOSvqsAJvn7FgZis22SR64b2nx9nVjQThzyR58DEuynNNSQgFcvzLf4ib/98VNwRliR9E2Jh0y+125wD5Jmt8ZaKVise3QcxcymAP9+O7uQN2BmNhF79vuI0fstZk+B1CNipTN2aqyaR+MIqwbfdOg9xN0d4Na2Qw9J0DJZWB5bYPiztHClXPEP6IiR0DQJ0jSnFcfdbmikQbAEdyWtZ9pj38vo+CdFBMqtvZSW9E/FPf9n/4rqKT0Gub0ffuJhNAv9mmn9N1pp42O3vHTw73nx4d7/WZtez7/hMQldIG8Pv+wZNdJDSC0YYUfPu+D/99Xw4/VQuCdCM4kxuT9qb1H+I/Rf8N78Qzg3/Ae+Hr3R3+cPbuzcXrl1cvey/enX148/Lt1enV+bu3W72Ldx9fvn/14XXv9OJ8q3d5jt+inoOv3r3vvX33dnD27sXL95cK1J3/cH6Da/yL//x0f+/4WwIIYxIUG70Q0AkxeQI2QHip/ypGm+fIDoR1w0r8WE/hL3flrMXA9ndF764pb77v7/wjZkuuq/H3T/b7CHz9vj+rb2p8zf4PcmJgut/tFAiqn/O8XEz7piIo5oIbzt4uK2kllBn+cO94d+84eYR0t+hkJWEIvevlYzpxYaoHp2Vi8CcyQ8dakZvH0bPDvYNkHmcOEusyKFTTyE1YzPRafKZB3HY9Ym/TR8Sv7rpcbPag3SfdD5KhqTMuKG2i7Nhw1Gf5Ucuwr1J2rlxeAMcEt27zk4Cs4+ExM+KuN12O7uCRo7sZFeG0i+XNDVF4ghU1+0cnY4iqhvDV7sE5PE38bBGo+PieAGC8iGOqS8cu5UWkyU4IA+HCtXFxolJfB+XCh8urd296r85fvn5xSZf+7N3bK5APW72r09+8e/vuzW97Vy/fv7nsnb590ftwGcgCJ+sTUS8XxTVE6z2algVj6/1c6ZMtQ9AoP9bUIF26x1tx2yxtrCyJLMpNIzXaTGsDGGxCjdpsGtsdN/P6p2dnLy8v6Z3fv3tN789L4V/yA+EFMmfmaPdw9+nhfxbbJt4+TuOwMHjI4WNsvl2WK5d2J2I27o5W3ksm6b542HKVbMh6xMUAQWtvUzCBF7pssGqAx+X6BjyRD2Sxcbt4+BZ39dL+GnLiXyGlypcCT5pQG/oDiH0WQF823MCO+vdqGZ1Lh0WvzXwU5mcT1BbC3IIKG7W022L+fcu5S+KjdL9TWWxe1Wzs+5evz0+fgy67/HBx8e79lTm0nOa0dUqSh5tzhcXvasl+rrquO5QIbXeSfTfywKZMSYr4xKsmc/U7zBuEm0WGyrLxShQmhilZPIS48ONyUiGNyC2KPrnuWJ5fTUtBUzU8VNclj6+vy0YGzQ+ost0XhNhbvZ1oNBF0l6SYBtQbDE0mY2V52S5/IrzwZl/9+NvL84+//dHL00dehEh/Hu4H8jjz4zPhll4/F4K0984cF/WqX7wnol26aau+9sJViV4SR++q777CGh3O0K762jnmfM1SRHtY9Jp6uESKKPzeANYIG1JzORM3XOWam9xSvSgWdmQz5q8uqZXXgLto5n77cgrm1qp5v13ixV31jYs7uPmrvnD5Cd511Rc+vH+96uNTbDS56gu/Rlt91RdeKqUYQcJXffOsntQNL9fKd2biMxL6I7v4DFLjzb2g9HtvB85mlXC4edDt48TWESM70MhYgEUielEgUeo4UscDIvwljbClXQtB/4yaum0Hcdkgalxybqmbd5bSsiCDnBmHQVeBItBXjxsGu5dP5dbFu8ur3tVvL16yESJ2yfnLbksE5HiFcaxysZyndry136vSmBGCWUXOfPNt0AZtifTWzvQQm0pqO3y30NoRwpasy0A7zbklaSgxqchbLYAaxTl/PSqKxaLatq3ZZTKOseVzTnhWuB0gtxcrx+FTwrOgUCxq3ONMj3RAOVCw1s8/nL9+0QPdSt7j6WtwGK9WbMO4vEEBP9cDPEoOcIaM0RSx48Z49zHYVJAVjj4Qd6W7EyN9zObKvL4vG7B7WG2ag/bmw+ur89fnb3/8AO/0/uXpi98mWrKVKtnJQyZ2YU6LAJuRxU+m3grBeFAeQdSSRAGGW0Co5xu2AGhzA9PD1Jsl5gZCmZ1t4dfKWFyu6NrEQyxGDNUvv8WkGKIllrRFZ0tObbKBQzhiv2jyeoV6a25OyRXZ0pdXp89hXa9yy4ltY5lj8KaY4sbCvyWEZadHWG3D/7nlLKOxbHyl3dqGD+m63d9vwwzvytuaO423WV/r3Qx8tB/rS/4Kr+e7ZeNoBeng8BU62t3d2t3d7Ul4sirkNG1px26/bK0QZpKLdvbmEmnZUMvfI3sBsmdLbfkW/uXTjDpuoE23kKYZW7ZMcss0t6PiZ63oRDKEEYLNwObcpuqu2sz8tnZU3GG0BWsi6ST882k0Hojuh7Zqtf8uSyYpP5ZdFgBawGux9+xIiS0OHLWLJVi4MDWfepg82FoqU9kReE/KgU4wlcz1fiSnRtwf5nlFzdNDB73XwpvTd3lU+mc14894/b/Ad8pZ0VQ1eKITZNcHE3eK1QxEc4lFOVuecUw8EvhgpovkPhNjVDtzbvXKxWj7ccIkgCuwl6yAFuabGiThLNUybEcoGi5OSJOWbSFhhVLQBBoL27nW3BOISANfV/zPv6L7Hz5KGThvqi/ExcUNgdHMxGOHwT5L9rlomPC+VpBqQYwMxDNLP1sswAqdOinPfT25cRNxmqYWS/bNsbwaFuwWLgM2fuCm5bkTQ1SGvp1FNZa+Ayzh0GsIKjPlR5ZJMHzuVTV7eHP20rXjpPXnNtJNyUQFekD4oYIR4bfrmOMjqninocYV0xkw3T/+qilvl3Bgw+1lF/9xfo60M4Z4gJdXIiN3YIbPwFRve3BCirnW27u/94bIXl5k+GsPPHuthsrvq0/owrWL3b6n8PMdSqP2P5hvactIeYquef8SLLzTi/PwjX6NF3bZ2q6eplTcBir57arZfKntOlEtwDfgc9xtswZtzO8j/ZbDGpCASW5L/DLq+KyCgO8tiOuahDOpIhA4G9xUpUeU5qLctaQFrTojIgjTaoM+A2HR3pVjjzh+L7/XnhJosoPH2ns0pEZN1WSyxT8i65vjJrbN3GPUEm1NWgeVzh16+voUnpR7lo6Nr0dtyyolB8LKRK19GJbwWVUTm0xxc8OvQay/jbSMZQbfiO7xooME4KSnO4+dcLS9QsuM3ni2mIeZPEoV31gNrw3nWY02S7roqr7gM5A4jzTKZGj+9MPHm87O0VRSj3j8DzhN4DlNsdOq+tm+ASntp/QI48MZBOosgY/KV9Ro5HqZtsGw7SV4HG1bhfP8aHcRuU3ePPhoft/E8GlTfScH30DjIzelqZ1DQAUIRkT1lYGZKwki/vMXJqyczoskYPO5DNiIULt903rPiT0Q1U5y6JR/UXrPmza9qy9Z3OzGSn4UrroVrsWFqAPQdb236vrhwTCNpJ9Tu9yXKvaJKGf9LNBgHDhBa5QyXNBqgS42pSPviItFKER4gTgYlYm8rRMuTJLrelp0qJxKe72ouLENZaTwwLZCpq6gqaJkAzYQi12W29q1on53MNLvl9XoE5YA48UI9a0UNVsFmx+YyEZo/sz944Sqs0/GJRnlRUavIlEcGYG3xShHTU8GN4lZMavSs8DlYraXz/H2QY47XwvCOi4OyQJTZSb9OFmbMS+OI5wXKrYFXv2FvCUxtlG7Tnol+IcjXyebrPdQLjY4U5RcwAbgy3EZ+LqkEYboviMvOwksY1ldadomtafkFVXeOMp4pkx1BGZFGCsgY84TvuGD+J7cFcTNyv2haD7smXRYjyT/sR0CHhM6fKhBBsqUtbA1ahzMYl/XSm2+HdynndMm7sutsGxVt2CkxnOINrdw+sJchUvwJ3/kzo7vWAFS16WC267KT/E3HbebZIsocT6iyHSVXQyrYLg3tTQd5rhXx20mUdX4z7wxp3kmLeDO3k/70ERCr3iqNISaMY+ddtkTSt0vcI+YENCORoTUbJ3NyvvJg/TDI7o782v5BXb0gpWd+AZt2IGya+0yPfwonvte47npy+mcF011e0tdzKpUv2QPDGf5XOumcYEMs3ThMCmK4ejmQYw3lE8s32CON9xys7tlSNXql9e944ydNVEbQoVlFETHaeQL5sJ7LGOpiQaFHeBOR3YXWFoVe52oqaXFKSelTP96lfDUr1io2oTWObQbxMwmHeMMC/SsKHACZiN4BJdlyTL7T39UiRzn/m5hgOWQUn8uxLNzq/PZ4TO9s3d08OS4Lw38vu9fD+EifHLhJrA3ZsT+RiH/pmz6P/yE4QkRgOtBAQ6Eb4qVvcEhXc7oOLOrhCiZktnvJNbT3qNHzaK3azvNemN4awIOoGNEOP1cjAva1/flZyR8x08u4fiXzfrTJVKXWfE0eS29/cASiufTWmGUu3jOu2TY2Aa3iqeBt0HeLmMcUOKja3GEzkKJzCvq0FakRoQ0srzjoLQa6KsCGSyxuIUrlpIOJ9Uo46Pvbx+bqFP3OQiMOVFpGHMoMPHsac61c7o03yNvyTQELCQxgrLHUXbkG7yB98Ad6+nQNdNKmhlWo8gdElLHYRnx3RYUMK5GFA3BNQv73X1ZEJkZS25aHYwnOD8edTJ5K9dwL6/pgdf8wEePOR2U/1g+DaNxbWoOkGMkobcbeDfuzonhfTxmBbfEom6Z+NryI2yB3EonycS3CTb1SbypLpIo/UqxErB3rnjUkBR2POZwNOwaI6eVZhePPLvryiYphC5dHSnRihWWNTHV+ij/q9myVJknx4LkSL75Zh/EOeWFUAqNyy99N6Sctr46Ynip+8mhW8H2lZ14U97i+5MVhxYRrueRXINTRzmj7badS0q2LVsI2ANAfUXTJ9oegOyuJV203qC4oZtvBctqHzCgs/1cFb0OarTg3TsDzX/ZrTXnytlj71/9KEGuuwpsL1Q1d4jQLGZsNotOXtuoVRFluIU0pjqMuJ5x03uNV9CxUWaOtJOrSnPbvjWdyEnv3cwH3pwbyBECCrsIEJf7NivTeMGhPozZLHoOiOW8o/eBZ0MD5B79hoQghlN9Yolw+RqDDjwnXZNfFZ+LSwo2rA0b5h76S40lOdFC9mlrdBVtsgRCpNNG3bDqlgyMsdX4xz8h4LT2mIkz5PPcE4coUCnrmjLges9MSwbf5ZisUZDGLA4678hRpxOf3v3DzuSPl9KCHKZA3SLbq1NHO0hG03mZnebmOyJENTh3qfLMi1GJbKwPauhYtsncCtvS2UthhEbaYQccvwVhLIoI8y/43dCx63S/MjvR+ozWHZzGP6BvMiHgZM6AXBWaslrDjdkVeMfJRgEQPJcUwV61q2mK1DZAopuCZGhzUoNy3adI+KywWgnDMAaI8t8+9cvtxdjwQCwG9u/hRvEKufAXlMxWzOmA1aShNmwri4pxQA/CHret2GG3FXUK2ygYmwnC+VDjPEA/WTxFBA6Sq6xtQ/GAVD7P5PZHWxXh2uYnxVekmq3cThHfZA3rpEnp3het9tYkWvRQNxs7Ps24Zvc/aaCYkIZrYRWRTmFZnpopvJIOJKGZU14t+qmPOmTkmY2UO8CjyxGKV238sbbEfuWUNYD/AZsaf1SOoxisfbnO9pP+1AUXONP30TQ4Vs0pSTKOXvICk56XLgaaWRutUHE8NoofVtdkIgdd+CQ2nImsEFrFys1ct0qf8ZT75mLjNFiYOMRr5g+7fsEuqhmaML6PKpjE56IixMPjHsH6Fxpq4mNBitl7T+iwgy8+nTnhhCfV+nTBs9xOBQtgfWBSJXRMKPFUtHo+0GBYLGphH0XJLbcZG7Bz+q5j7XzPEZxb8jCJQ6suIwg1Phs0Kcg8SpXfIB2j5IFbCfOJgqWB8o9zBo6/vYKC8llYVI0oFVXUXNkGK+ZS8cNNGFjUZ0YV8WEN46eZadrLaus3JAbodjgDDujZdxK92FbTalKIueOkpnNB61nOeV//dGq2q+3j9e3UEtWeG6w6wM5rxgN05B8S/OJqMRWiGIoFWxX4pB8/nL/QpXbykgOAhH1egKsEx1KyBMKrRrFX/rxlR6vz8fQ0TaialAAJg+S4ouCkpnYceRUiZq413VgQgymP4Tnj9Apk9//b3rvuuHFk6aK/TwP9DrkJuC3tJquKdZNU0/agdLOrR7I0Ksnu3hsbhSwyqyotksnOJFUqNwz0O+xfB9gb8LP4UfpJTqxrrIiMJFm25NEczGDQVpHJiMi4rFiXb31rYd8H3adwxnZElEHnXFtudV9I1Y61Iy7BI4mSXeU+qDNg5t5Ix4Ii155xYG9e0Y1IUo7kDA080XfA99/h8hMpaMEasPGu5++QCxnr+k6qKnVGEBvuMTd4zuQEoHsHFMtOkRfMT6ioMvyW7DqsfWDo1tv1lkxzSrjbqpdtnNO6nXLFSft9pcDjlH/NXLbDRBUpVmNtWc4qcIlH/isRAovK31crX683pzTJsxFA3XuIdtQNAqedltHvNBN6T75DGy73koNk57VTYiHCZkNlKl7X2LW2j1YPX5eXV4O/Lale5F+r3L3v6ZMXePNd1gmfemC0o/anzvcmG3D9JXTNOwXHDfXnnzCsIHnEFwCWvcak36MMCFx7Xy6uqwDLHFpleCbRE4UCAOR27DTGCkK5JIV2x3l8kCpuIQf/X44OwpbOI5K5HaV+JLl64P+YlU5b1pyqKJ5D3thgMOCaDct+tNV12p8s2XokWb6o82uIpszGJMBlc6XDjObKkGaW9eSLRQ0+LYHawZZkFwbJOf5h1OTrCH2PN6fud6h9cX22LHugo6BvMJ9AgTnSU5zQmgHiNEZhRqZ6B45GfOhuJ0BaeN7Iy5B/UCT2vC7RbKT7ws5X2Bzt8mMB+ZqtjnPAOVzPylnRp6IO/G9MPOqD1O/H0ZanWpEpBD8GoqP1Mq+efsVRjcatIrmr3cvMPl+gL4nXhJ4QUk12sTuhFp9LzLPQqWS/riqMPNPBHlzYgnW8p43udhHGnmGKtVYTBalJ7LxEy8ApIWFTpyaZDt5UhPss5vUk1SDbZlUhClPxfEJZWWhJjaf49akCGbyjDQJAkg9cU3VZIbQbAezkdm0/ll6pGycF4i/IqwNn0KhDMCXuPSPHDYNjGY2kI2IlEX6giBQnRkdXYuVRo6kR4e10frMorCcP3dteCUOgCPqc3f6sxv6oiYlfRmfsGAW3O7dYnwonyO1R0h8GKCSNWh8Ffzhs515lO3v56mlqzFpdOHJS2QCf6SD0fYZ3YetKV4dygABh7ywBrulad69z8vI1WGg053it5KQAOvk8X9aIz21iSNDjArShJhKoJINRQ3RWaeKy3eHSVCkfgK3yDj9vx3eBuoFPXmWMVjQz0s5yiUcn9C002lgyUEkdrKCVGPK9Dj/rn2AXfYny7IzenIKEf9rGL2xHaI2jau5V0uTRquo5bDHYOOGlTxqoYKDg+zZIgpoAh5DUdWwMWpBNWNioGNUYRMZgwdWorf+qE6hDm2mKccMFSimnK4bZ3F7c+5MoeWddYHrTdg11rBjhprKSDjEjrLDiHtbeQzvS3ar6ypq+kHT5+YQDCuuL6ywPZBHYHosSHXt092dIwI1CNdUsnXs51a4B431xM19x+c2K7rhNZ4StPqdqjYxw83YZiVXybJzfQEjD+8ECEzYleiRXgrCNfPZCLD7cDmg4rV4yMiAp/mjji5awvB8hd1N+nyyUCcaLZYN3htmCLnMP3EdgLXg/9B5YBzbDuCHNpxwBhTVidKopwF+x5t1JGNCVgSojxq4vcMMsZAeJZBAnIyV2oVoDavxgXGGGAqmcKuTAzVDVuYGRK75zzaBC3TV6SUGDKWzSqQPgW52wSEdvbwoEJoqFDb6GmxlsavHLjytzkdMsNTlgIX7ACkEyJWQyLYwd2KW189y2cd3U+Hcvz5is5OzxyStuPiknyREru4tOBCOLrBwUDx+a7mXSpYQtGTh6aVNXV9joOwGGwyZXWdoFnjQwJsBJJzMGztVpsSBgx1pEeMKnFLv2ISXwWjDFqElg/NiZqzyxz5+dPfr6yaN/e/jiL2enT549efT6yWMezc8/ubUH2ZmEDtsrFy8bb6NFm9K9lIkxQhlEN8vJmWvhKKIgUznrcKbGXQZJGIEjhwnt16IQELFiDD+4gMLXJCITLtuYMoSsdm3Ep2sp2KZ4m0Z7NTrVcXDJtMyoACQ3Yc0nKCiBmxlBCb5EBCEnJsjdUhcc6Vo5I4lwHl2NyCLgPZehyZNc43a8fEMVBD2gQiHQ2o6d+iDpgbIBUiPaS+8646bl6w3vvtiwzAZfZj3MZUCltRfMI94dGPCpCHo4Q246kvWohThJgwb9BfwTilhjBrT3QZCGl1gcicT4gapewRFqH71jvEoKAQJdgk7OFt27kuabnNoG4QgPLLo9WIkBRobUDEuz4s2cxF10LuD3/w5ougxIS4hnQwubd+bHQY0jDB8+YbVJYCZ3g66/cWfn4kbAcBmRQqKJS7fFrCjG7J2Q5HPZ6AloxaoDRKAdrRFMrDJsX/OVi5IaoNCMzaJohKbs8pDQ1u5DZhtGwkoPXCfsey7aDrM9JcFitwem7CQgDCkZIc5uf3RIiQR/JgajqjQqZX3WUMJdqvKRFUXF2JOnpaBy2Ldt2sc8xeGWSiPD6gazAnbYfOLOdp0Nh+t7wvw4QHTUi1b0nYbPWh2GXHxQ6PT0WapxusOvFtOJ6A+aTUZfkUCQL/1lBglX4WXW3Ty8Nch9iFz8upbq/DrZwDqbW6C96VWhmtPu6ityyPtKG0eq23lvBlY/l23az2o8ksSP4C9n2b/zAvkfVhsgrNl09bGmRdaPyjqW1t3babacokcH04C8dZrUiFrO6C43S4iOsj6oMnZp9r2JzQcwDERwDhPAoxRtkTSzaQdczwWRzTOgG6CcBPZx8lC6gzISulEQWMnXSV0ygpJKa5a4JTq8sYGby/0TXdE4Jc6YEns42SghsifIDUO4CkBlk6uDXTjNpn1pGoPJZ0h3K+Yb3iXXhNY0xmJN6qBkjpDLp2sYBgcQumSDFALiI0M6KOgR02LGxJEAwBw3wuRl047fuQU8yp6u2ozxa0SuNjwvOepc7Win6eOEtjLjgPhCeelONKXhodsE4vNLSi0qmuWE8EI2r4DhSWfYSCLvwH7vvj6/ESg4X+IaXUQxcYFhcyCXA+0ezAfsn5gv+Wyct90GR9mx0zbPmxFkvWACZJs+RJCzYNV+HmAbc11IsEQh5z7PetRZz8M40S3VrISPuim1WYBE1qs4LEIxsKyc7k4V2UVV76prCATk78vpcsrqNhlm4ANJdHXK+ge9YyCNRjnQGFAAeFJOS0t+sA6nbXp4WFdvUVgYGY8OnYRxSCS1klXC4l9UN8oOqD0TljsnFFPBCn+Td0ks4U5sqzxi2qEJaFRQim4hkM3gABwJKRmdYsZUomViA8ntH4YKJGojcHIVz3supDZhrqHPVYOV5AiwBYeEZhJJtPGSwOLMzr6ySSSGIBEyQQvtAqYAJruaDcaF6yO0liHNpA3rP8qS8we02Hj82BPpxFr3pKxu4UrcL0HhUCYACeQ8NvaQOEowPA8Xm4AM1VvVeqk4+UmmgrDCkRh0Zxnw7VZPo81lIXNmc6xBUreez+3m8O6NoyiQHG2qJOlL7cwEIh5J8BHYSyYczbOKxDERBzmxd1G+L4TPL9VR8OtXeSloM5gnwRmz4YbhBTfDB1t7raO56z47aF1bIT8A5ohqDkmkhqXS7kib4VE0GWUOoRcButsn6DZy8NbLGVqN30kZeTfIHSRBkQQ+D8cl2PjdCOwRvEkLZnTsc8r48HkjEpSqasZzI/nueD7Bvi5HdGbJBEZnKrkYTHuqtZGnwJ1gW1pDeYr9OLmoJ88MVzFAMjIug7JAbwN/cQCgI656ABawFEHZ3Rk+GOwcDnYPs+HB0cHwaHg/rGoCVUGTpTukLMMXUK+bR/GF9g8VF6AM68C9yPUcyPy4oiZUsLhwkwaWwqD1xJB/Kj7FgdPiKlOI5P5RD8hvTJVZrdvg/o1XeVTQpC6QvA7Ss7SmOjzqjMQzNEy4aon/0WHwLZqT9msoegJX3FmqVMqPUZ2RHMrRd9ei+NbUGpZaFMHv05Va7x9GTSgZUFCQGUlohC7IlCNu0GcyQzZu4AcCim1i2L6qqqbY+v3v3BYrpl/GGWTOGHGfGiILxmGpmgs/wvGED05KxpOTqErx6e3u3bs3JD693a37gYMUz16Q8vr16+fP3KV3qUh55haUeEqlcQOEcjYVvS9H0hDMBGomFqcgsidEqXmLPBSIQd9yqXn+iESmnVeBUBpgir+XdBxWqSSHbq0fXjKNIFAjURdOTQSjGTSmS76lEfiDL2PcJ2RuuGNwLikGlL/+5+O/cBAQOW01A2GtGxwstSv01LWTDPMQXymZj/GDSf8nUxrQ8MKNLS94DbS5kF6IajgFh1+D+voOb8xHUKJpnNdRxGiFASx9igkMM8jBgaoCvMsPgAIXdyt8kx0jm9ax0J8m7sH7XQ71XGLTJjsB4wdtJi2CtYhXwaS8NIgWaDCo74xnvOldExB15NhJckTtLJXZiO5zTAQprXGNGgyPiKhEGei5SGZshIa5QT+zYSAgZTmvNL+U2dkiYC0CXpCN2g/OG5klkUzAHJTcioXkHA2TaUqcL96ZgodfG10pPf+tloVu0ucbeZcgKxgBGjw0rEooLbsIweS8PcJNEPSVWFgwNkhPxuV1uyjSBzksjm85LtxajjjvKFag+UV3unK9QqlmUsMfnZ7iCfvzKb5a697x0bWy1ozxxD7ZoJt0++5eIIklWfcP87cQcPEAvyDHRlypG3UOUy5GlByA8B06BiUiDzjlFsmcUn9ojinzJLUeHcA02l9V/fZCHC2igT96bVB7D036dfeZvAEujRrhURXlaFGYqtHbR73PfOGQ/O7epMGR9hrHKxB4zyGQiH43Lbmy/jppmfXRSHiC8ZIUaARdXfwA+aV8GuIIGHXRaV1LU4wTgf1E7FqjsoOFIBiaJzskz45c5KLMOSlHEhO1Oqogw5z0MB6qgUFPv0/TQgTd5Utx82O9Lphqxiy4taOO6BiUM2T3wJR/TjbPG5upWCCfRmLT3WvDGCxsl8Nfz/O5gS07PdXafTSQZ2WkKgTKmE0o4cDveS44i7COjlMTUJ/Hl7gEEqcF+aZALUfClEjoMX6JgErvF7k7eYE/hI6xN9DI0ocnaORwihAl/vK447IMLxSPxSk4MCL06ALRLesx0cIIqT+KQ5ZtrCelguTzcjYrJEXFqYWNGSUVARgLJRJb0pfAaW8Cbum9hHEPqh6mkx2dKczWQJ2JvX36IEZCDZ8WmxJ/W1aLlBS71zYJEsnrXMMBJVsNOc119vjkW12yQInj/cISWI5aG27RF1jmOVFN2hwCUT9o7AjOoJxQkHg4wckZNCOOeB04dswNMnF1+2YIQbPmhuO3IWx3chpbeqkVuhc0QFRvF075Pc9rlQn9xDhwf8d3kGhwK1TExLn18v78RlBiLddstwPyVs0zCTJqPUFeGooJUysjdPGxJp64n1jltO5s3GNoQaBI7x4szXbgzEOupPFYQN6YzawX8gQyWqbVuLwoFUgKlzdoiMWkmxkoOLbWXJQRq2VZmdsabUQj8fWu2+ReIxFj7RjIik2KCrYrryDCwxaBPigl7jq0rRkg9JBzfXTjp4myy7k2qmoh7S2c2qtdmhwktplwhwl8uWetNc0migBtnYQr8d1hQgzPLm4PboyEZEPJc2Ph6mmZ16qo4gMPixzcNw+7yHz+M7wDF7WCCELVJPByILQ6Mj4bcF4BrjfKNLEimnQ/Q3kUiXM4y0Jw71k2SovFPvCR9HUuGh+E4PkyoohPwgJosfL6ckkufvw14LJzjutM8/ptyoC8t7lVZcxryeXS/BXUGaw9Dp5mkkGBiznswIQNDEEmunk6XGCeCtAzKPF1XygbPG0QDynG4bTbEn9SalbS15mFGdN2Q8Omw65sQRuCi1EcrTyfYPxeAgUxi6mI+YkdgwQMQaABSz7ZbD1YnV4/68Wj6eF12kOsZo97Sw7IuIgq7k6uaI9vJcinwSNj6mw44BGofI+YCofpvn9Fv33JRLxV/y/zOneHdX61qutvZbeiWzzDuLKsIU/4uL0gIcsPtZ9qHi6RscRoWJbxb4h4oeJNlNroM2ccYMUHuIBb33KEq/V5Z3b+L9m/z6hw95p2FH/N934P8Li9CN4Nn4PTCXMBnVgq6vmiZ5UdLq+zCS9fewi5QIGxnz/0TOIPLx4BJC9uq3Im+g38go3AhchLpsYJPYTMR0D6NDLpGj6Ugo+vvQHCu9i43pGIo7UBaH/1w3uVblNi/GCcaolhYLqkabXhkt7oqhfB5asgXJdjNO18enzvjY9Avnz1ooc5pMuaKSbdkW8aLMWL2R+LxqsPswRoNjw7q8YozvaO9UX+KR2jKrvupNW5dRYKZmdUzUvvnbPlP4hgoh2lIOqgRh1xqvxC/EI63GiWVx4XT+GlnjtitZ+L1GuUxgieIvY4aLo3qwAgNO/xlvQbZZNISshVpyMUn/ggpqsA3y7lpuIIkGUGeMfEZRCCFWa0HJhbhVe3outVhmCIWXwipOGL77N3SgomMhmrZdFT67xHWPenjLMyyeYtP5IntcL8XjxCCspTSFbSW2uboTsXb0DEydOvPYNjTLjZgx2ST/oZJhcVTSBoEG3KbdN+7AXpuPzjhC5zuLXfzW0C4QVMImLsn8GIkcnb8iXpse/T30Tq+H5VkAJGsJGWaUQK7rDe14UTIrWqMBVB5Nr+dT02gUrudOc5JDd2MabgyDocFzNLbYagQa1CVwmBHO9K9AnBGcQAciFlbYhznSRwpz/6Gdo57EwA+0ZuLBsl9H41HQxuU/Mzvg+w1jDj4Tn6k466oIvAs6zxWeLrAvwEUkYsZrYz7GhM04EezeyHooYUFpMZg5DbtbKureGTSg/81YzZM3TBpOCHy66bZiO5ej7JR29FTTIGZmOuf7rPlKGd6kNDTXqsvUUD3qgzypbgnqgXtXBxpsWFSJzkWHd6OUfS8Foyw5CVUhwN1IZnQ0GQ6HpJjbqiv9+Sl1cf11PMV4LzdqoXGI7nZUHUJaPVg+OnNw+U8sDaoSmddbPR9/hG7cmxxHeQuKu4fECe9kPWPko+YEq9vmLchf7mI/fOCQ8gWNvjoNhfyJuoJI2ArYEIfD6jiizgyEU+FymHEYdiU3vwnBC11nGJBGjsIzAwDlQC7Oqtb/zpEoUga6bMxo9exYYxMG78rKlcUYE2MchFTyO3JOzKlsbWz84reAOs+upP7y1cUt+95OPy1LgCVRMCPuaWal1bOM3y8hIqV1YzzdPGxUEab4mWEdUOcTOiqNAfdfgx2xRucaWtRvwZ2N4cqfLdDG2DyVYtDOGyhNUjlkURFUiUn4yHdk0Y6aehvcoXg1VEgiABajOtTSqptpX8y0k3fVqgYxuNCe0VI6oDjsSUEItC7qFI6ksKyZiR/0kjWjPFu1msbboE1ZlEEFFcBeyHcm5Xsyt0EzkcIx2MNrE2u+0vFRPWLEqO5qnsDqovZrh7MPi6UGMbHk5nFa/PHmWyarIu+TbtEViqZ1TcBISqJ9WdQQT6kzOrxB4NEeBUg4oMMORGuQiPo/s8DiaMC6w7xVXEGSLLVgAacUS2NF6G7HSBWtiKZ3nVNJiPI5DiPBGqOLiFSyslnY0UM19AxdrYxuoAJdanZEXqsqrESWXPoGvypgFL3WsQ5KBNLDxp05BXEoFLJPALn8dTHzi4Yy9KMMFtGLS6V4xe6i5TKsGdDr/9O9e5k/AobjxgthEC7lKUku5AWcwfoMarh23SuojHqUwpXB3GUhSFcIbSwR83//XaOIzdKmRs2g0T3HfIap5PEP6M8sFY6TdOQr3fVAlIBGbR30+tLgzFn3fmkzCLyWrTG6NlKOpqIVJMgJuRkBTnmwm/bBjiXe+AgekMvS+iKHAknQw3WAQ0whJbSDQF1pnXDgDY/Gjj4//ydlfeeha8PkFb7jLWIJi7Km/491doRqevM2Cny5gjk/jdEX0DtlzbjWZ9YB37cYmePb6ueXNuXVWV25Hf5+9yQra4LXpemwrJ9jKEVYYSte6ycH2jTc0eF+Y08mGCfGVMPrW90lIncfZQIQryt+rqe54X+M6qV7DrEuWpENelROALU4M54Q74BUPowL51jIYUTfFhQJrEdV6jtmk7xixt41p3ih5G1rrPzy8evtEpY2cctYkloCCRu932qtBa+wpBkB/SnxHSLVUyYX1D6Fvp4sjxwITum8ZHOwO/A4eEVt9Srd92QQ7WtENZr0BVKKzOmEBJdmql9VxWLU7Qntw+V8VkngFBCMrCxuTdlsTiZh8GxhggUqYISTW7XWKEX8hcbwG6U1KLHRYKF820rNs0vf2MIbNuw8c9Sfg/5PVVX05cDPK6cDoIF+gKXk3rMoZ7+qKqQyPdzz6p94xsJJ+cgvsBU51ZVrQV6aUc9xlXI0QIkL5DigL61UrKMTOpWB3yfYXIawKApLDWtxMJu65cvXHPq/FNeOEGNOddO2Az0DYEKsAIH0gUKurIfV/WhexbNiCJtAeLhywkPefVk8cYRdncakLQCNFktXFIQX02w6vFcAY39qkw0Y0mOYZInSk4KUflIhDZSf10xWQTS1w4B77IM2PQcuHKwRRxKl4QlEFOXLYHnWGHtoftFKkMQpCjKE1cYVNhbOxTwnJolLdpzosHNVc2Khxg6xoOfiXmSXnbQ370hi1uRWInUN0bJjopYmpEAJhLBXcaN78tyhqCioJsjfQ7JCFErCMgU5+Hs7ZAesSEPkIQ+u32NTT3ZJbXtFE3mwITR/cZbFVtPFqXQHJXzZeGWHCe45BDwWPMAUAeKadnB/1+cIe5G4aDNo0auy2sl6260W0iRXjRpE4UBreJJTtypUaeSQpec+7IS+hcvsFa7mqQ3UFkHbgHIrvzbvJ0tiwt18o8V3Q6yhuF0ccc51Sllh7SqBRCvdDUYxcNfe9nF2S21Xo2Al/CVZP7OoVOzcdgicSEmdcIIDy9IGWfqLrFljuji+sM/gnPnunuOyPDodcNyRAHXXeu1nq4IWcuXA2RWoBD5o01EnCO3FND+OHV3pevwxVgGe8e2IMHmgWkLFJebIbN8yfO8ADfDvOs8oeM5QHS5r4+KTmkTQjowW4l8s412pFnRsoDFgze3BhGk72G2oTTwhmPlBCcz6ItIt1BsTStE4OPYiZBH7JtffxAMDTEDAKt8Xxa0B9TPUHW8V+5z4uS3YA0tS+404bzZtn5dl4tQGJ2KNrpdwx4BqjTpziXUPENeduhjGXeuDehFeMZp7zh4K5yaolSijDVQVxH2ybMBxrdtklLH1Bb26hRDPgnA9gVgxw4LOn77d6Xx/AXFMTmTfshOpXu3GQNchAV9MVgQvsQP6ebSZe7GXCFgoE7lODodiODX37gkSWmg+v3FMjuoNPyFX+K6/yRpmdcvisHRrS6bh+7jz5wbzLp8J5OqkYdfkWffvxprkZOAFzPdYJfwN/fvTQdo7j8/e8iwQdRrbfFnO0tEAqUpM6HZSs7uWCaAcwUvAFP55TK42rdNPB3kDrPVxZqsFxCWYQPudbaYEv4tHXi++hoxs5U2wiLqxHdEFlmyD3Fzj+S7mrlBtpEJOVJVgibmcoM9JGFvwOoKPh+WzL5SCUug01hAjQZLnRPuzeNfM5WegLNGIlj5dxZlVMLdXvongnVG7o7+GjLTeQa9M/DEZCn9RGeNrg8eR7gYibXNdMBsTLJc/d4iTbS2N3dk2oOm7XvjAZm9W2lrhB0HSONyzqlusGg3FYjlgL8C/yxywmEqjWrxmvngkv2pIMwuxwO9fVmhN2H0SLEXcQVwKgTYf9uJAgiuZvNEmHgc7jWOc+O87xwhdnnItFWckeqJjYp37rH/7aEs3VeA7viwr3HJSIjuFYyGR1u/ARrmgG5P5y2h4BohXBvn643CttR0PncbQ/KhZxMApePqTIEWwr1AXCUXN3AmsAoyhbXkTEPaJ23smPXLN+f/ksR32O/91TviTF2fsep0QUoLSrEIj/Sur9mS8nP3Dx4kD4ks1GSw3mOeDg69e0BAS0gvBG9BsxAMIuF+VYWoImXRxwBEkYxFc8p3Q9ie7RL7MYhizQ7rYLtietP3JB//7u6ApDt8scfhRySUElN1n4CD+Pulw+ZA8qduN0vQ53luVQWL94vmKe/5LNDdQ80l2XhoWzI5dI0107Wco4VQum6SlRJH1+D81HzfLtLlYe/PkUiuIRZio4d5K17SUU+NZrm3SIo/MinyM4QPNBcD6aH9bqaq2LcY0bBPtaMsSb3kqqx0XjpyAJjbZ1fwEmcMy0iuCSXqNkmiCuTxKXFJUXiZsvpucmUx6nm5FyTPJSeEhWNxqxewW6QCFb2xbesfbeg6EDy5JmF2iD0kF50UrnTItF8GR4bJ1rGks803PPjcZdH1ZAvSAlYcPN8VQAuh0qaTKIQV3rDRCHIIN1eFSxB8xo4KbtipKaJnwMtHKnjssnQOQV8BgWWYny/CMM+63cGbm0D59TGAqYC5LMPjD+6pHxOn5tKAGcCMFPHqYV6E4ZvAvubTKXwAVdhtuXIK4J/9UtIjOEvPDQXUfAUwEePz8xdhJfGx52I7jwFq2z7GaS9GgCDOviQkaBeKNWMf6SrQQy6yoUsfqRzuF5yjFiLv4LBb9u0D/HfPVI46rKpNE/1dF14LLgAYD/wDpOu8TMcjqkDyr5RXHEDiU44xf7CpXiWk7ee/rYtBCBHGFIPOtFUzrZsrrr78XclqhmQpricewYurd3Ne5QnhyF96/hDlIckCLh1pJfrJiIGYRC5VM6LANihH9UpR7W7tOStUZiD34vV4/TZTMAZILw1rqs5sN5h1MzwQJQ8tYq8Ye2RiM3bFWptAoWpXtUONCQylpk60VJbC/h5drmEgseGFmo+WYI3rCv2IeVQOuIZcBHC3meAuXq4pXQmiGsNKwL8XVlPiDhnWmwW4Phu71H2LRVwx91UG7pQHN94Kbmrfp/x/vOF39nT75l+QEib3EB7tyXodjYaKQh4EV9OhxwDny7EmkhvMjmOwD/K8akRKvEN6ArMeM2MvhrAkJKESicBOXCkvgIgu9C32GiMsSItYAw5D9V7H7V4WxRzwhrq3cbkvBRGM1gODHIArn7lKDxh6QwLvuP68YXVe/ri1aMnZ6enz86OHz8/+UYx2pyhACPW5RXdEIsDQLkQdxS+fv365UZT8LCqFuAqmwvwrlGFg4t5AxIUHegaGRYgGte7kIlHYAPyoSJpjJkXtOrBabgEQC3oSBsuEG/R5Zx+Y28sQey16kquQut9iM4Ei2yP2Nr2dbFifL3RUqgYg/K8QMgHU1ATZQRW60MtEoElRFRVmWjfdwbOay6svgnZUWEXWuzQdF43osSlZDkyZpyJImRikM0IPwNKRg3grQEvG44Yy8ehd4TMMvURVHHw2i4WP65hhyMHkicNwtd1R2Q5ApPl1rk/tmAHnQjD60FZ03U7naoTwAcCkTh9WYf0H6DaAjWrU5pXAO0K6IgI6gFavuTvWiO2Wyl5FKJ9A07ElmBlmULEqigk3bi+qp1G61QBj/Ks6hR+sl0CipRFL7vEteVhhZD/FetZgdRM6MehuUF9tGPsFrTR6iLgxOmLjko+2876NYF6lyBNFb9kl+IYZISEK5VFpok32DzLDOn4op1y0NryzYDSLUgJN7f9iF+vXGzwOrzbyFei89mHCUO0mX5pEcrVZbG4Ijy3sl41I4tG6H4/QU2pJusMu0VeG3caeYDYdTGAZ378sW/9Qvly4Ub644+4AvQ5PdXpvAlD5caAa5zqVQgSAnBkfg/Bo8+gwAB28+LiAl2LHL2LSz1YRrVvuSSnbOMF0tNdZ9+7KwbLhuFSArFXu25nMFDGotBIlcgOnBoBfsfMDGCZgmnwZpgkTkSccizp0s6ekAoJhvTDgEA5Wvxu1+h+iWzSyEmGU0PJoxgmykbFZLJh13DzqEuZ1cyHJL48czkH/cNIg0kjFWcJqgoYolntt+sczWeffXP8/MlnnwWWMRaajoUB9q4Kip4dbOmzzx69ePPN688+28yjMmUfpLEYGFfmK+uizPaGLqN5aI+4Ly+rxUDcnohMc9uFJQw9g0ydrUe6DtbGuk7vFL2VE3cJ8p2jvAJYsUSDCZ4qgcoh+RKo4CzisNs1+7msMQMnC/h9kQMQToOzdK82Ga/hsv+Ls7m+XU5mpmCDP2do13gLpAp4lhNcKN09yjrCcFHgQAjEmq2Seut59zec90SevzUb1OlnmWCjiJny71WT8zwElyTp+pHr/PZ0/fcGO7vZcPdoZ/9o7+C2dP17O/eVrl/7/4V0/Tu3o+uPqO0/CcZ+HBUcjVkxIcJ+5us/LxY5/H0Af8d89dAmKXIPBvAganPtJb4PS0xP8JfcE7zY7trlPTjaPwyX92Dndssrff/444+uB9ebBPqJbAnf915coGDfVxd4yI8FFQpS7aQLFey44R5DsvHEX8Yc3W4h+bxRRZzDTySFHWUduk0aCqNjtPgGyqiPyNJKlhS4/+A+rdHO1gMBuPJf7Oe+2vvysTRTLZ3skIi08YAjBgz1xB4GmViwkuxD73YyDZIe/gZmFEQF/XkK89cPIRQULOe7GCezPYbHBOCVTECMXSFpBdxRbta4cp7oZlupHoxsew3+5o6uVIfyeQxhzF5EN6vk1vuN9dQKGF1DNwnGLpeoT6CPGzoHCYqfE1ua7IJVhqImW7IZxIQ4NgfQR+rzyTUUuDBUFGgAEGog3Yto1dbAB6qU3b7BPdNwuXVMwCKGVJz+VXWdZQFeKUs7FfG5g7NwieVjbu52rUfv9cMs+mVPiOZoxiE2DRuu/WC2nX1FzYd79GSBP+QThREhy40vfCQto9WdnHYJAcr8mUMYdSTQufX3Hh7JX1KmZrgDgnG4c3SwGwnG4TrBONzd3bu3v6fCUcfw29x9aaH5SVyBP+IKMfgsfSUc+CvhmTwX3Am2geRdAOVdjiFsTsY/xFKUpcQ7xhtDtqc5cP5m6Jb2u3v3Dw/lTk4lNBDu9Cg7Zv+7DwKJvi3QW7SDPIugSdfCkZ97zz41I79L0Z5Jr08wb3wc9SVtA/4ZC+Ugw2Sra9Sjo1+WnrKFIA4wPIlPawFFeiAnWsZwnGR6gwwjDT1q3y+DbTl4uYfLy8EF1qQMsNecWMZWH93v2rVc+uApS81wGv/eDkw/rCnLYxVwHx2vXfWck7h6gsnIC5n74Kp0rwauXidtgbHSTBQ4g3y1cYWGEXbGBuPEc8Q2bfYInLPSyiNIRCuxVmyDPOR0odC38aybMUJNS6CPdgKOcI1/W2KuJCYEEeHT6/yc7v3j0cgdJHQOut6C9By8lSlCkM9u8PI+LzDiR4axFJ8FEhNK56ne0uu5jizHEOPb8EkylMpVwyd6jic+CumBO8T9oTvnMYdWvX/0ERFjf4cMNgLoXNHXd1RUnDaks0POipmbLCcySUwpfOHMjfuMEsb1K75ktdjC5Ca1oVJ1KY03+aWA+KOjtiptFuD0KB0VuipnXIiRQg+9TBcnAhA2C0M7hujYXUMp9Ee7ZJFIL1CCFWpic0hITIxKSkdD34yMgGmsgA7nz6cGi8LbDCT54Bp+CPIP3BPqrJhTzcYZ6rX4dimC05eeoecoO4GbYkoc+hTuJ1YbJ4Qu63zqaZtGUCJ7sJxjERyQx6byTDkpWPhTuCwjIV9F2e+R2LNkAKCYs3NOEcYIazBFAeFqh1i5j19t2DqKs5dOFUGXFSY5szoq4kfgODIGTL/U+o354kqCKkIuWs2KDTvvBW7SXoCfbTusQL4IWpeWMWeXp8Coy9aFFpUvCXbmittcAxreS46eILhkcBiQEF4svMuctyuuSIkpKvy0gSIxSQ8LM7grNeyrj09iQjet4SmjAm1vATlvcEe/dzNTCjvChaS0C7+Wqh4lRAOoLhPRB0nQDmovyXbkr5qQOmj1YC6KYozYenV6XojMDfD7RF8EB3SJWgOGt7sPHlWDZtz8FIhkkR8HUMUAzyb52bCugXqB1ALKwM8LPbJA8DIArk4QsW5ZYQSQxX0HdJ276b36PH+LiVF1kaoon/PtFkR7JdBL/anMxFsRLVWJcbkfOy02nxdj9EPA4iwAOwxDJCrJRX7Jr4co3sajZqlYzGhZx2uTGrmMQddi68+nL76h/VCCwTkbT3CSeIHcDLubgRysEtvPTR0V/GFFSN082wJ+K/woVSRos/F836RGQztx+yMO6inWrpUwiE3le4rb6AlHEmU3K65mAvkEXHYSFDans9CGi3QshNtQIeRi63JL5NExhGXQOuEPSO3TP0l9KxajDSWo4qXdnbQwpY/RnUGPmAALZcwY4n59LaTpDfp8bFN8jjJhEECmB6aydZMT0A9thv6M9JfncLxlY0hKHuRxmkg1vsdzGCsWEgqlAt+5jI6rC2dSN1f+3j/HEHojlS1DAMqqsbDngs5vqxoeJ8a71f6f1/PpZMDn839xaKvrIsJoewUBBUs8JZXLVfvSwkENv96qUZMoJtgCGwjv8rpEZG7vX518+uLbJ69OT158g05DYeChg4QzCzS7cBKNPiGTyte8WAL4ckzgRtd9SzeWQy/Lg+qLmKKYL0KsZAOqy7CgCxPONBrr7nMu6mTikdu103bGFZEtYsFo8LDl32O6xICXQgOb8lr+QkjY0NGRuhY1HlthnwOpQxAaKkA8Ndk3b549I2LbjGolTn2IVR6VioZkWBBj2wIhDqW/FGxwSuYc/n6YA+DG6U/PnRKEWgaxM6PYXmUltOtYiS7zqhiwyZ3s3C1GoORxOF1Qh2DlUV4clzfCs0f6yyWUbSjGWsUAoCKW4NKnNa/avS2RJsP0ICiuK4JL811VjSQm53aTu4/TBQr31goecZ7IzSoHUrZPz5xrVk0zovWqhdqXNRTVbtggQiGlq0U3Qy3R1LLmnBB4ul3rgCm7CAcjYto9+VTw+reaS3dutlFfRaX9DqgVfSpgIbnn47sa+wQ8IFgDsdXXyfrVMaES/Th1GvG5275aDo4MDiJJPIbKd+wxAGpD6Rw6mzS5WIXVJh0mmffqxZJZDH5hE94/CFpuk21nr5xZl6Za7L6gKW78VV2OW/YLzLkA9mquEsELT6kWsSpBZ3LXZsLjBKZcbZvqD6gs5ITyKrA6sSmKCgVM3pVjYndVLj81yPhqk6u2rBPuRISaouy7yN+B8i7UcFEjlimQwe9OJHLiE2b6b/hGopFQGUh/asOKRf50BtWsc0GqcN0aXDJ7p+gx3OBOsWTJ0c+i0fD12CiALSCbsXVcb7W45QxhKXR7tFsRFhuscrENqiQbMpR/2aJUijqiApLNFC7tUdVAqYeRaNLlzPqD47dPSOtYVhtL8wmzHLm1ArRtMRbN4C0keZcjAEsCirwcvVXRScwWOLWkUXZYsR2BSSDlHjCZc8Ski0pvfkkFjhgQDDwWZWzxEXizo1+hZlXPtSadwMbwFqpixUiIKwHiFMp94TWMFNErbXRYS3YaWJ+BOJ9vDMmLe5nb3C62XI933Yimi3HZPMQpwttxOQYzYyv6ZGZc759FvzNRakDKZ0lt8sZw+6BHGkGPNjd5qWt7+gmBJWuFZ17+0AdAFt7uiMV3Jh655RyRcPMbOlv+aFWGNACO2YaSLcVaIhxc83w2DgUbAMYBSzmAWOVmPUDapKlpSfCBq9JtI5++C3RM82az9ogQiLQpEvBydpkkyE0ZuMHntGVL3iGSU7icDfhXuV4T6DLj/E2+Mja9iK1gx8iIF8B7XAT3Ri2vKQUcACs+G3Vvhibg2oKldoJofl4p0hI3gpb65e2KeV1puwnFAojU8ikoBnzmm2x/Z/6eGPrm1k7jNYKSqpRcwqt1XlyBdHRTBs5JkhsXWEAl2e2rHDX54n0xWnKKbVnVzHxJHk1KdCctQSSQZ62Wfc+U/ugPYbMOVwySqjebxM9PffQQM+ep6c+J9l1ua8qdRu8ebxF6bNNTK4jxUdOoPqwCiST4BkFJIpMI6MfEleRBikAYf2/r/YrmwjraWv/Qx0xz4Rzhgk70KuTblCI89/4Y3gJFjUQv9UVOHlUQxWNKbnctBn6bZGyZ66wJihrcm3Autw0n91lYzIxD3G9Oku1BtMfekGA1aXRlQAN7ZYk0ETe7lZ2gGql40BnFPZgBuP0reCH2aUM9DRiR9ErLAfIX+k8OkoNpbf43MAaQH4ih4pHbDRNvmYMKNIQT73NDqd20VZ5W9KF7u0pCXSWlP2Crws7VEqNUMIQydp3NB+cDXS3VvJj5+CkTLImPbEMtsi4ugYoN5SQgvjnl5QSreD2s3pMsDWoBro4+BdtdjE8mPGgKZzlWE6HjoYwi43/Pen+AYt69Dccu5swkMjIoIkAI58fAubCqPS6jgtqOJlqxO1Gq33WFSQS5V7zPYbtnsLsqEmVu1f5aLV8vz4sAc59QUfWYIGQ7wg9D4AA2CPL8GNbuSYAFYQJTemMDHxDMCXwsJEm4wxPmTSxTnoJieeOepgrfwbOkqyQVyGD5mQSKY1Gm8hpssYYcCExz6LHYwqHaAe0YfhyqvSSH3s8/Jdj22qR88s1/ke/dinwvxBb1w9yViYr1glL0F8FUaH3v/yLj+y8yvv8i4/sVZHzN/8/J+FpEVoMsmWSWa65dDUWTiFzj0tMWSy5wObt5/uiJojBnkRijawt6ES8Q+ccg5CzlsiUfC6JtnppOcvS2uocYGQ9oql3DheNRnKgJcGjd5OEV3s0GhkHvMYbbAKtHnfZsr2He+stvu8iWoiI0IF8xZ+FhyPBezbrv965eOfqPaQccUwgSqgS+JBuK3lsKiv38k99EjOYPiZk7+30OJQC4R6J1A/glu+p5IKjPbbxIMID9nX22nbGaTjUbtML23e2NuPbOOdjfkHDEaNCCHLrwuWblY0iZ4jF+hE6LiZr8+adgr0v9LNTHT7nejW573Wxt53kwC8DqXE2k1uykurwUVdwDNJVCJ/J4QzstfjI1fjl8G6CCaZqRQU7YRgKj/F4wNu8AprWWN+YlkhhkCeGw0g28VnCR4oOLC3AbwYlBZ7Skb8hP6VB37qvrOVZm/vmnO35noh/aF3X++SdWte5yxEarPXtPFrHIx8sAnfqQfIRwALSigHAYvToW2rIJEGQFlWmvDPBYbh5aHUYlWSUbLGpyVDHsAAQmOaosOZzC38hxjJ6yGe4Bg7n7tZ0k3lqMHDQbdoIoROb+j3BpHpb2AtYbfmcjqCePCQ0klH+qhqqCSv7na6T85wgXFDhygm7qLoD6RmomgXO0hlTa4pwyEkQF4KFEZqndW1AWD2/choAxLfxJE6CZFlItUj3eON1xTC5CULXGIfdXTtnN5SjrAbBvgOU/e36m8LJ/V5WIa7lwZ0PYLL3blTAlSiYy3eztK6rlh2L+Qlg/EKnSy746eWrBHBSKAPCc64lC+BjWS9iPOyuQoXiI1BkFh20SiBsOoAl0AW5fiG7DaVLrhIgaq1kbmZXO8mj32c6qOA/XMl5JACZs3qF1OXADoDrBJhVbTzzA8vbmfkHNgstUZs7MKouVbCi0nElq/RjHCpvDFz2kLfMhm86bm+xxeVlC9sBjTvBqUv0kvRt21pzKW05zMbDDEYjwwEjitXv+Jjst3y/caPr6N4oz/IQtgj4xJveR29X9AWYifQnLmfF1AawYoMg7lWyl54blJXgn2Q/MKNVKEDwCrnK3AZVWAgQLIfNF5lCUXsQOYEv6eOTLcd8KybshNncDv9Iz8I6xUkb+soW7B2t3hzecL8X1wQiMyB6E1a4q3gkcpQ70hzezcbX9qhhreWar0STZfKiVRpq9AMhPMgze0v27GhI3oTgE6bXFaeg0lpF81iYJ5dSakCLy5YlrlGE3kmdTiQGGWDVAy7ohGxLbJcazixoIrlJAg6BRQGiCnw5BuwEqy/LLguCTGfAu9FRV4u6AkG1bikphAMFq+YZrjp27WFCdNyfGo2hj08a5DTQwHMqjeCiUuh1aFwz1Qc0Qv/amDqmfF78MRmJ1D3x78edSh1MEDaKfc0aB4GZekV/98YvnAjYwJZx/1WSMnLntrrMbwf77MKNciiFybw5hvTmuBWaqsiVOjLOzwurraMT7HH5fpugW4JfLKzA0FaPpoefebFFHN4oVdi9yBNPe5rFK10YALnzpZ7Z1K+Z7lni3Jrk5QQY8RAGialGBm7ZylgONmrJ5JI4qslpYi7hD0eBIB9M6dcL4jvXnKwLHwOfUsuuyGpW5MCQSnxFBIiifUyAJfl423Q6Mk/YIK5Bd75Uhz12jjU2MamblfF4QBMNDu1NJqYC8PS+ER4sGCxZyswSPNcWll02xGt+LPhC8L0iO5hPZbG2stg8yY7Xkq/ISomDOdF2uDm01mDjpBOuzhE6ocFaLgVGUOjKkE4R7XI7B80Jc4m7TF7khqeorVtxsruu8sTRZBMxam1tLo8W1OyXOLRQiaIUCVpscJ0n9eNiimUtC0hXF3q6mxST1pB9RyigzDzDbGuV4gMo6M+l/VHKBrquS7GOfhSeQdvJ9MIQqJOmJhmnLi1+7V8Zw8ptGpoL2WxORd8lmIdKL0NbjaAu/ClKMeqYi1KvQ/LBZc3Bz4SUhvyXDllnDEWcwiZTqQJ8MX/CrN+6Ofg0UQnrDRD7FvGnLjMjJQWTAYcPReTIUv0FsdcpZC9oFO2z9FX565e4Bxb0D5nxyw5OURxI+2XPk/aGZUfK/9/LXxCj2dPj9AFnfqUZ4sYx//olYHmctyCklIcshxnwsLj0Gq0Z3i2Q6dw0akj3GNB8oFvn6NpARYXNW6kfUJfQX+rUvJ40LR0+ne/02rwnNH6ql6OROnejWeY4VaDrQ4Ok05d0sqyRkTQOTwg+oaYVonFVNEn5zpnmN0P7foK47AucJjcGszOi391jCckaULRg9PI9hO+k+xWPh5nQBuXyIPEiUp2PwFRIzaFEBzGTh8nXBb1B4B3jPrEvBt5JWcy4QKYn0OWKAjDAEgscTqd1BR/JY0QpmniaeC2nzL6jevae/WyX7UsZxcLICvRtjy/5vBqXmS04cMESIMcLdgtjlfg0KveIJdZ9cV9WAb7EB+qxNpZeMy7Cse50Os0ECCgIHSSLYOX15VSeePZPdMVJkGoNXnkPRkGpyoViIemzjzYK3QAXlmKbuzW/1SnhlcsknJVeg7B3IMpogXhjjK3JH4T6DWQ76QUOOE3iZEtGNGO6aGo7bbfaoiDOkBcaQH1zpmRLml8T1MO3q38+lFtrWccSrJHy1V3W1vCTe4/wS/c8Zs8ScleMvTh73Vr9s1huPJwMuB8nXxNn5zRm8LxHgYJIePRtkgiFL/Q3XfVXCUpQ6n0um1802IbTmeVmvusa0Aq49K8xl6EsC23yoO5hHmtrW9LO7q7oDw2EbQTUIbOW0D6OeTiRjoyF4WSpZbIP3MZGM+OUSmRUt1Tu+moZbD9q5AbEStyJdKcNonKAUQZ6K+UWXfKT4+2xxm/OBwC+bovXEZjGsui4n1bnr6c/5u5wjE5o9uxqrSZcU487AZoKQWykGTPz7aCVaXq6Uxxi9EnDSgEgiNevdLnDr5xQHi89YwPx0wtPAgJ+XoviTG3W9k67hV7Wjb4VUrBXvoY9Lxj0SY5snZGLBFDaSnMFTZtCVa1WroZH7BUz0Gafg0wD4jGoeNX2+zmJufPI2vOrxWGPnFo8c6xbIQgGXiRaKLi+8mgKgijuht/HuinHApjylTUm6vK/EEeoE4ikPfF8zTiuJ/CIyK6vAn0WNkM8qJErtaKhPR9LkEZjf+6U1bhnJHaiXs8CHvTJfL9B7pLEluH6MUdPaafg0h9VqiGuLvciX2qptILvcFj5Yos9wTGmbbnMQD/hGMjLFOdVFJ9Nms1Cqwln2qrruQ9ZMRO0EwbNNMx4ZmIEmnq6rn0h2s5m7x9IvxxbYpp2ij6xhrDJLnIRrU9MpqNgVoaPxnF0DdcoVWiEbdskawoip/MK+KU3MKbGQpERBUApAsnLTMsKGAQ2iJWsfBYBeS2WHS3kBkh59lqT9m7Kg6BBO8b+3CqJMuaK8+jESP8I1GVnFElT7WDKjZS38PKZcpVAwJYfDQUUIXErBSEFv4SGvwDXagpfSgFkF9rYoy+4OFnc4WlN05WBKg89gacfEVauG5WYzhut/xAXXtHnj6mKB5auVRnHt8IeTQHKbBEbU19q2rvWLh4jYFfT1HC+gDEny9UwLkKFlM4Wqg9qp8vpwGI7KApxTJBiT10nyruirS6QzOT/Sgcrho6iemvYdrbYyIfuantJ7CsKTkR0sznq0q8E3ymQMNou4tW5+giWeQe+LyoQIK3oXO1O5cRAwdol1LIledOUxRyPoPaZDbZOYe768qBvVbNmHoDuFu7J3JeEBApYzwnfNqvCEq/evo+sWUgqLTbD7yeeAs5jBWk+VRWujY4arFM5SU7lim6TsjH5QkmJaMWIb665o2iNO+TzdcCqb3ynxWqk3gNyzYo+Kg69NKhFxhODToc8CMEzYo9O48+ytkyAo5S0MTSRZn44MR/+m2T//8X/GY7U/5al//uP/wuS6L8/S36a7j0A+zBRvuhQe4UtkeCafC6gaHa8jfmfD5xE8GAPYTJUcoc3xjgFNr624+h5O7Qbs+YCGWvgkJypqgZMb1rOhQhMUG1jfrFvEOs/cDoD4DmRP930YBm4H8vlmyCLo/mu7YtlPIVao/+VntJB63uA7UE0uNRp0IbMZ4CubiTxOkq+tqveKGE9UNTacVkivqufl6C2UO2EEFSWao31eGPDm2jU3pXfINBG9IFdRaUCXT50cvIA6DXV2ml/kdbl+sF7m29qEURo5t7u+NYCKSs4mDpK4ojzgksu/plpKwoi01YOt3Q1/ZFx5FP2ia0YUH1ZQE/rh/a3hjvLNw58t3myfuxlwsgpEoS4MSwIxAQCVJvDo53PmZOU/8GaB+EZaF1by11w6ahj60yhjHde2A/ho2+LwS6klw6F34ynRVOSN7LZwL+JOfCpsKv4vH9kjUo5w26zDXmgzAapBhBtKDQJi48QY19pVNS3akXRBsh5pLjdV+2QeM8L0aVUQBa8yPDpCZxG6EmqlvAtqpQC3GLZKdiVylyW31v1OAzLgYrygQwVchewpKcbA45xHIa3ceDT4IoDoST+MCxjCG/up2GyrwvmM93aP+z2zDVuQuGlr6jb1pocrTeUm9WLEAOis1IoxGUzcW80Ih6Bll3xdRw7WpJPtMVrQ6LlADBVjWCspHYa1r1jbIyOFbsOy4ax3HqiWP0BsNiX/UwE1txPDUQTT0KIVpreHqqxh/VaYS/ZrgMsHb0jE15TvkD2GAn9Yo445jmx3HfVpDn4JT/9wsHsfePp3D472925bwGR35/DB4Z6pYXLwW9L0hzz2nwo9/677h1vb8UCshQFd/mmy/j1P1o+sARFVf3djSeL+Q/f4k7wpgT2Z00wRcYQs/gUS9Ad+e4+YQyskydY/vD88ULb+FiOeDzjlPtgkXOReBZjR0Zlimo/4gdGUgQ98KW1fVcxSX5qemDBPnzMlwkCFDdqjYmRyDSaHsEmX0ctxCO6HIs4VWP0ufYM0y8qpQjWK677wtuPG7kuZMtR3MYPkzatniWER6Y2Y1fDyaAzDUi8lJCo3HvQ7U2er9XKAwYuGQMvBr4TycUXqwNK03ITikCImy/ObIJpaQiDIckvPltPzApnLuBSLJUPp7tCWH/cF9/TSo8RJtQcJA4E/PB6/g1GOs+fGf9dtJkkpSqfPlex+HGOu5AXH+TiYVQblZHxBWcapTosfqhkWYRU/G+KT3rx+tHYI3/319OS7v35ldzNR9KgvrZJbKSD3WNdui3XVt6zOOaplvGn7wtxi9XdUGVybfSi6PS3Jo+Q2mhxYy5OLEy21aKnUKB0ZxaRzSUWAQmy2dEIlQvWgUuKimtlzoqzhN9lFXk7AA8Ur6n+yUc/iPCMDBHQHxm1yQUd1J5N/lrZva5bXdoWM3R5BBxiiivHZyqYRxGzHy8KnrYpHBgmz3b9WdRdUQgcZhsvp4ScEwOMdgIzHGO4Cm0hEL/9EkFhYwYgR8U5dW1OLO8xkZNok6lUqxc+y4z8f/8UHDejoAzeBpEL1qZBV5MlfWWoq2T2z4ob90ytS4RaRx5AIMCPM6EbriWBiimz7kMB7BtFE1Vr9tHPaMXFC0jGTsCrMSJi9MRPOGZakiA8GFZmKiJJacFoslnN0vIFHEicSI/XVLHm9KJ45BYDba5sFPmm1w4Q3a6bpa3SHf//vCOQthXQ+iMMEjQcGrF+7MYDOFci9nF/WOWVJ06swh5VWMorLlAQvllT0WzBKuZ+oAwVaE2+Qa6bPMHIGsTPUVkoP1PnlH0zppeQ4WvpYcFeS2TaVYBHXONj87tPFxmUmfnL16rHp1CITAJAcRWtVkhOzvC+UgpkKyaULc338cNDwxHipt97Dwug8Mgri+UnXFrqms3v1bMY9pm9blGSGae1B5dKsmLqbIzW90L+H1LIGER+lgM934w3QLgSTWNCukwtIC7/pnrwHqBXFd7xKIH4tYaSoNh9bC+1CimtdcEmNhnnbp7ZGvYQEWT+lgDd1mojl+Jvdx3Tcz6IyMKIwxzWModGTx6K3ygJUteh2OF3oj7DLXFrFIdUPkiBqRXtUsfgNhL3igrR3NdPIRuhbDimsos287Fj9ZRPFLtbVPBOVIqDxmkCoB8ajKDGAtBDYJtvyi2043BrHXNV3SkuU92aKj2ZOIfUI5Oex1+Sf62N+1XIuJcYR0gmq6NobWsIkhjADnQGUJU1Dov3s1Xaz39cLAxQ2VAvQ59f7zvgbf2xQgd5AxoDCxnPF543E23U5KuxhiwvxrW86dUxDpfcI4Ti+hrasmQQDKjBZSuWtFoCAFKJh21JTS/jkdqk77avRiiO4XNjLmWQtoZoGbJ2IbcEQJsIY26JUN3jnXVYLLUy48VqYzevPxuK6kgjX7Nod6wAyLxvwF+wqu/wTyxEuwfVgzttZafHyatYcDQdjBWuOrlkDk/xoLBk6QkFOkGwZO/413cTrTbxBmInBrwpiuyyaLvXHEqL7KoqXWgzvdq8ZbW28uWUhQOaok8JIfiVg28gMlMveoGoAztYYB9Fmtntix9Am9CdW3AKq3ke8kVoHgI6ut8fzDU9r4mbhPeDnEE+O8tih/KLuQo8MB0TdnrL6Bt1NfFhlA0qRZDeGH/I6RSqRqEcRKMBETIcF4hKOKvCfrVQLT0S0Be/vUaoi8bgCFTkobzwyptqsG3ai0MlLTHXryurwYCVuP75/vffJe930aSyioobshqNNXDapXwJUfEGvBTdGtCHy0PNDshc2Ca08M+Qa5v449TI5FqFN4WWREdAu5UAgewhUxd94ClbR61u/bOrOxmOJXv4j+yvvWgamcPEiT4qIwT1e2/BbChiBZUtUJdcl+skVZ2GfRl1G7DaaJft28J9Yp7XYHik+TZA1nlDxD7TFWHAgSUu3sCT0FgnOiSNfhv4J0nSxvCiUU8BrHoZQ5M4U4Thcd2eRwemn3wKx9JgiJpx917AlzpHLElHgNxZ8H22vMuCjWns3yK/R98nBRm6JVB2aB0O/ldh5u1vDBNPxry+zu5sq38t0HDhv3niw4DgI3npXV7vCSmJS8KivMk4gGRv8WiSBwX0wdoLxZqO7F84fcAW+Q5jLRSXjUgXREkcUki0q0LhpYKBKhjGhPaISwRClS8qjOreUHeKZ4KstIfXSaxGvxCuuEkwHydsenDvGnqwIruake8Ex8QlB/8R0JhRQXFc0NanY7CQ/BxI8b+JcO7EyJ3FDmrn1mCC7sJQeStFXJ5115OkjR5NUy46845ykD1zgNcrK+oay8zDIzmEsqnHv3heT/8GFsdHGEVgnSgs+0fQqnCtxVWndC5RkBE2v6pSTsMP7wdKTdiSbjdCDCquk50INcUPTTqIsSKNnUeYFH/rNnLgx9oES3HHvyf4INVf5E90UtYl2dXbQ9uxJi5TAMGABj8q7EC1Y/mEO/CCvRc7IGEh7E8bq6EBq9ng+auvmwWXQgV4F4X5eIM2MMb+OX56s3zD5+2pWTW/MRZD7O9CgH8UVpD+YVRnQWxfMq7pG6zfujUSgiFjP0Wzuun9iXkshJJkUl/noJmhSfO3wGgi+BUIiAU82VkgvgsCr0y5HrGRBUkHQZDCkV0IXi4sGpckDhFPoaPN4NoxaTW7EAk6ctsSFqDuPFUDZPsQ1ysEoIEnlOKt4RNf694IWr8pWgI/UGg+/yjdoUJD7tmlhJ8fkahbaKuyBNywx8q4+TOCTYvs1edODl+2HPN0GvwF8pemhex4sAuuSusFcWKGO6JM2osgMy3k8PLQBfcChw50u8TG+iSXsZsndvU+5Y98SJIMLTtfl5dUiCMGx2F9/OyaCy2lD2xyUnKo7eG3JOrw73jkK51z5gpjuxBSXFUFqTDSBQ+4iWxkBg/CFtW8VHsW2B4XdnGjsm3QW3KM3hrwyQEdu2FvUPk4mh+nrkH60ERSoxHQ3eTGfEMWie4lBXbpPhYYD7/j5fHJjfVS0kTfpYrBBF1BgDTBmGGnlOIGKPK4Xr1gPebhD+iXkXyz+sW0hCdNN+71HB69GOkT5nHR5mMCgN1/8JllUC1Q7BcT5Q+foO8wOoI/xuEnd1PaiZVM+qIJT1d5JKgvxOG+usIRUevlMNybcnuvP9U1VYNlT2yUoIvKPNqJHAPfq2yZZlfsFQmgDYpnTU9e2A4HIYRG8u1kmoy9KLloAQATWd629QdrQV29OuvUqrHw344NLoBdaf59TzJaXdHNMSImSyv3SNHCKQOTGDTrCLP0rRH9gL3RRmNdBarZlTf4963dFIRGa7SveB/eB3CfGKmERDsKuvvG3lk1bhj9m/pYDf+/6TaGag8805ZwY6pXMNHGZGEsWeLD4HmAPIQHTroi4jOiNpPXU3ol3zjdcnzIMFYt2oSU79NnA4yteenRS37jxv0eKtwlcqzHC6CGBOJuRuw8Bm82+ZDZQGOIZmDzgGQOz3odTE65rbtZckxfogexc+PQP2m7U1I+MSw9WgPab+4VqIoatJq1+pFoN7sE+zea57VD5G0ts8XsBqwMdTbDGoRfvBG4cYMceB3KB7iPP/psou9SeI7fnNrvg0r503Fii81MFZcRmiU7ap2iM+89idFe0o88+e/ri1fOzx8evjz/7rBXHD1UTP4HrDx+UvSokgAI6NSXa/OBNXQr7/fkUhBUyhQpR3WJB1dzTe7E7JFvM4UqfjW6Eyn56DiXfXN+LM4Tnir1FF/YU9VIqR1k2afe3d+pzd00qbEPrdIFR4GKwqAbuP3359xQ4eLAMufsHf7nCjlsfJUJXAyWZsjfNQqMj0AflfKiaJQjGxsNpSQaDxb5+HKEy6SPVoqzCbY8BP6ZCI6JOFnY+EqjBEx6V9xqiGvhbjCOC6t62y4uSC7NGSOZGtWtV3gkXWhejcl52RVlW0JpRtt5e4pbZ2Uq4wowdJzE+wcRwVnwqHihCw+IDnIVa1PaaJgkk33cMqEPbNMFvcdP5Q4MgLut1xEnFS7vtlcEBoX+plAy29FDaPl5ODBPgAjttw3s5XOZVKJ14D/LOczqyD9zoPuzYbp0spcYRwBp+ys1mwjBgCSzUTmjfRG33Yci0oDlhlLXsI7CiLJ/feIpHnbaTx+DMvSjfr9T8iTEHzgnC+HDNW0ZwR4zL7+c1GKywUASqihyBuKxMCSfBnq9wBvBexWqDrS7RObrCddIKE+uw6HoimHqdj8sq1J9EyniERJcspEwQvadpPuc5Wkt467dwdfBAyieZ2oYt9jLIIz2HVC9It7N7Ku1+JMi2j2lK3W4AuEQe0WDV+0yIKp4uYtIy0cFGeVMv2DXWBlCBlF8urmLC/hkFOVEaI+CBpiQyet0QMBiBeULhSFtKSDt2y8Prc96pjE9HRDW//PliB1musrdVv17odFrHXgxyUtA6oA0IDJbisQGRx20bV5pIpOJlQzAJskn4FZOJXuoZNK8e+SGMcy0394HXrzKvbaGGZUezfgQiOeSSnrNJyjFArtg8JerafsLpuDqvJJ3eRrnQdBUg+JcOvgWU4GnrEIUWBIFsv05CXUgqGjfO06a6ZmtqWoI90QV0EAUcvcsdNXehdkXVYpOpgKfe5VMIaiZs04vowK14ecIdNYtiLmkFhFOnsAbdv8kz3tWkPcNprXpDAAb1aqKIqUM5GydUGnIfYYyRTdHuXnzdL9GccGweOaUgDqR4s8mSa2Y4TD6k6yRoWyaiEchChfW9q/qt1AgqiOx/1VUTdhW+DY44wIC1SmXjaqDfzgMajrLRBIrILOf+7iSua3mWCks0hbmNRzmQ1p8XWmEANRasTNKnmlSYxQ7mOHEj5EIx0WigGjxHZGIx3ATcze6tq3zcF170cbvuBBcxdDfcFRZHEYpTMH5B0TM1D24bTGib7IloAsVjSC+VoMIsMM/W4I1XDEDV4PEYolsFGWfEXaL4Xx/B5T/xYpk7c8Rq86ZH6i6MZkJXRI6EuE14aVF+Qs0XWTTo7b5Z58IIMvN8JYsLn/AyW05RZyM9UAp0MewtSvW0Fa46RW66Z/YoSfIfKtLGZqcSpCUxKWC+1UIx7Zi7vDtF1Eaz1tEQ1xFUHT+fgAybV3NgMifPOTNU4kL649e5XjhfJnIX+H8jHwWIbea3R+0IIpyai1eRR7ZDZW9hoVFokUwleZu8b1M40YDpSDdbQo4r35y8FnXFqYqbN21UikQvmjdXo6oCk8fV52Gnb2omUVVGBZZ75y8VnojeT4uXiCht2QjA33nYCaFK6pQEYZNjGSYfKmM3g+ca8bwDDQuU0GCClOYqx9YBFZMe05p8LXD7xcCSCw+nU5d7jnX9AjeFAZ/Do13gNOkpKNUTAuQY08cDKT1oHbQg+pe4MRfixdT4dWwgrE35fEUAmVlTSr1HmPEeKpnHbleMa3eymx6HgYmNaUwUChwQnq9PAeBdClTn/AYmpVBFoLtk/ifQbZzhM//L6JdrMpDcKF6SMtKRCpWNK65SI1EBSy9P6jKRZ8Nrr73T4qzkAadY51a6ULzKcO01qOFyShlxUKwPzyP+iNLvTTPh1iF2plmUwL92PXiqJ8UZLeEZh63O3Nxcws18BrAvSkBH5IRr4Wk5UZ2XfrXaM92JLrJBNvTlk7Fgbv0JZgdO88XoCh1leIG7j263vXFB1V9pxSqrBmGOySx7gS7NJCNoaiY5GuoUR4LJM6IgyDSRbbFuVagtn6pq01P9dCVjLIGUa6VHd3KGAFVALtkAdeGlCOuZUKLjvRM1DSBX/iYZ4DAuxKd15x6t8laDXWcF+cuvXwav0eZX2gN+pf3bsysdDnYPs+HwaOfg6GA/ZFfav7eOXWn44MHuoSFX2v/tqJVW8Q59KkRLQ/fCyGY9IExjmmBp3xMsHfNjAcNSu5U0s5Jb0ldg+dFTiuGJYsc+0caTbVNWHl/qaYqlvb09pVi610UuQJkhLVhFTqeUZSE/xfCoh4Qvxue8G74VkjCdSPFpkkZdCDzlyMau7ZDI3i/r8SDgcEj4cO+1yQvSSc35uNXDqyenrxGbChKhjG7nl0a8mILWpkI0ATSFmQg4S9begFDTr56S/POVrBh7xJyBoPjl8zysIBbIpFZ9ALzbrgBHPxNYpNN17d6pBfeH2gljZzQQRCmTtsI7KBIYeMIzmpz3dZwB5o4S72a8AGaE/ArvyA0Bys/MklKWkrTpdgIYiXAw1yoGyUU/toh7coKvbYi0d9aSLdc7K2E9/gpXo8cbw+rgUpC0zUrV2iVQbs9b45QxgHsOMMvik4INg+kKXwX2XbA+raBgx45e+/Z2Bs2iIgzBlxUlzxHdwLhpKAYlFQc69PkQC08S9oyFo4+Uk2kbLWZIOQsgy5IGdmxLS6w02E0xzVgehlHX1Py2Cy2m3Bt6KoGmTktpMlCtYaWfz+N1tZyAbgM8jk4pyRfiDaEhkcdlVizADTnAgeXGaM2Siit1EIHmXtq3Tcwg7TLv8vMEf8B0ntxu8WZ7nn+PIKgxCIKLfMRp2lhV1l2H0/KHNkTURxJ6p4ty9PYmu3KLXrgTRTEvXFx6g2AjgLK/zt2tBwlljHKGYTL1VvYSKnniNAj4mzRIDJtL0ciFL/Su5UeilgxRoOATpYbOVnZCaV998CQjE1bDzgJxEvhWTV4WtXtewE3MhSedCeFEkDxLXR53eWMi+9KUa2GhRR0QSmUyaR+HEe5KhA6JTQg4rQm8HcWgx+N2MdT1I2GNwzcv6DzqoZzRJaTJA6iFKOP4hZ7QAt3VKevTEsmpKI5Oel2E8+CHw0yXrjOseTggtK46MzriCalumoo2FYptKD5asWP+uuRS2T5ejiLJ/DjdzfOWEmcYgJPTHK94vKygddILo+qX7pYDDImiPfpIfN3GyNGO5eHH2j83a5MQO4edePXgYtnf2c/sFc13h5RrDwUf315dV3WPfM2SbevH2yNBJOq05I1jY1h5gCIgotxMEBGZfqkEGvtXZ+W2XIYKpTTSitOZ2IfPmR5fvRGVP0r6k+TQ9E6wGbINqtk4YszWSozvIJGLTKBRLmfPSbwY9n1P/2yPnP3inWHFjinkJEjJ8U921sqdmkVI/ORLdWoJwf2u+9Fp4peX6ET0XC2c027Orj5uqcwDVdaOIX1CnOl5MQECPJyEv744drZQVBVQNF17dFJHImzq39xPoQxg1QikTkwkKKzFMXZetUds/3aFO6KmPcSYM+yjAedtHjk79rUKKFm9PkdjjVa41txrlLYiYYjwyNrltMza7bet21foXdHdiH7BOPsCKb/fnKTaC0kHaUf+qZnns26UzDm4yKyji3IJsMY2jhuLWsNT2DHeDnQnMcIEqWsjy0E91RH4iBzzMBc4qPQr7Lde4Y0P7XnLRkmmJpIuF94CE18MCOJ9xkZL69RhYThLqZ/1HoFZNekZCSrcpKKxVvOBuwm6akjQi+2tXJsEsshXsw9PKWXOq+6kVUsJjjgbSC2swpS+WjHhu+tsHWXjqkJHjvcpyIIHMysYnPeEStX4mAlr4hImtH4sKEg3d6LJcd5c2Vx2SSUVBtrmZnruDgyuExfFEZ9LGRUzDqxGcuJwuqTdSzSx8UlHRtWiRfJPk9qBgW0zoE8Ni1apRd3xEqD24+VP767WGoaQHuStyNVG1B5horEMQjTR6V+7JaSKy+NgRgmoikHT1NGK3DZ86U2Kd8UkbIZ4fYxSmDyoEguIPG8jxYAHvJAnj1cOCX7NLA4GoRcHxODr1Ky3Tw6eaLoLv+hdgGF9XQBK5cipqTv/0jNkDZoChFnU6o0Tr8umfr7oaG82ipWMrui1DJKlQRlxUh8A4Vz7jHAUAYw6gECKY/HDDE3OpB/e+rv7w/Sca2Fa4GgajJkiX+fKLtkH6zS2EpUsh4pUO5nvJkPRBf4eMP46X5JHiVK48rTWcci+gYqjUZtos5I4hVonStVmvCZkcYK5E1aEzT/sdmwHDIqsB3dJD9+hJ7dJzxky78rLMBbxYYZgDmVrv7V2PGzLz7nWqEoOXLU5Ev2It3vVVbzW6Ugz48UVYyF4DEm94xdKKMGhLwIJZFcczeE7z2N51M++BmkB0xBfl3kj6PQ3DXu75GQbFolIm777i5YzLG/q6xWH1w2WZywniwFgwMlRRkwqXAUCaQcDJ9QHGAkvX0Kj8xPdN9KF8wn8ZvywIl8R8d7WJEkRFGB8F5Sok7HamrRSXZgJ7bWqvWgweazDfLhXYKd5SLfEZTsp5AIXmFxdmEJSQQ4HPo/SteW7V9zChxklZc4lhQgxYiDdYTH2dRnIifjzT04LQ5AJ2y0fZjhJ058sJTElQkCNEQHgU/ugSoj3AhibC7NouX6Z1EE3cc0kDO+DTY+ma0hFpISoVZg3uThhr/MTkX/3AytndB1J7QXVzgJudOPo+JXbJsbUljML+PM1NGF4pyDywVRRnhm8QkyNT5UxXo8rP8KGCuF8ilNSk4fgPaDeYP4yETD86t4IFtVSCoE0lBNQkf6IvI1xaFxw+B9vHsAt8CwRd6dogSTYfjhF9tbT4Q72BO7+m+AqsuYctvOh5iMUfbRLwrqSpWfI+CCrQPEaIWmjin4QUKBcNPYlKrLTeOAkrcoUG/lAy0Np1L4GNXgaTY8frqPVUfhfo14lu1N7sROR0q2F722ohSe9vpcWldBBA4xNrnijDT0I3o9U1BDdbjl2ZuMw9ukZ4bm8TWq+f/W4woCNGx26k/st1z0N0Kv9x6J4cIDnY4wtt2r4Wv+qYFsIQp4c7McYpOXKI+noBnNO5UUNPsgPBkEbH2MkdX5tqm+wMO5E8WBE4iOsmcdKuF2riV94a1lbjn0fTikSnR+8GABlcK9A9cFDH/CHH2oMSPHKPB/KD9tp4KE9TiyHIc0mBmvNn/woZ+z247HcDiaY7LNp2e91LGV7PuyASaonR8r4KrFpadD+EdH4ITkDBvphxwV5ADmJHKYekniGJ4WF4zfnVDDJNwXX08eYIbQ9YoqiVnxTI2yWiw+cLhxz//ybSi2n5Wz8OZsHH3vAAM7icHvxXkwBA64UN+8feJL/Q8YDS+0NlfAqYgAN+2El8A1FMilO+1GuohaWUkVZ7NOjAMpae+5jTWtk4aSQxahl8mLDRJvF/w1GFXtMKfV1ioXcndmFAIemmizR3/Rxx2O2Hgs9coo2vrwOaRxnoGGccWQAUEFTJL9OwRo+sA6ZnMLQrdfS6WsEypJs+cinwFhnXH69L5XiCQnBRExE7cA05g2BXaO8ySmR9EPSFSaxc3sfZT6t2XtVTsYcBApTUFIBJ6rEAQrmBx7XY6GexBn1NJLmtFhyTjGrOky2TvKwRL4dC1SvPu9v3UsgfVp3HkoRr9CRv5a4KFHwVJeXAuhfj4SHkPp8dMFK9Nml06fZyXGGMWt33q6qsTGSGRAFLomiBQ3vSEc7gHS0e1sHvyQhbfgg2zk8Otg72jmMEtIerE1IG96/ZxPSeAS/TUpaKlHrk0lFcw9cV9WIszwHeH8n89F2D3w+muWx+5Z+EaampVtN56e5oRwTNB2ZsJFEfSA/tTXJba+wxbHndFrazg6kvUnaxINNALt/W+azBZ5IyLX21ZfGxdytCtW/ZRgm06IC5wDh0jzT8fpz9i6vS1SRuUBgk6gQWIw36iPIDbm/UWFfOuXvBs56AP6iUV4vBoKqUFMiCYu0ua9BiYdzSDOGj6Q8g77YihzX1qACwmfFYqlvhKBaVFEx5OZnx4NujeTktHJFPLrN7qoo85bkGyWEOEXpsp9RitGeE+4hJV8nkBGtElZoXFu0lLtbhyZM0ZGq/wZrQybzGikcWjLljBd4yVdvgbA7SajOAdC6AvR9ClgqNNezl7wUp06pFVsh8fVLKHZJA3cbLk4taDso4vU4XZ6rrGioiaCFJzP3q1FiIWme2VmGKf6CtJSRff1S7RZ0taEBQ1gAjMgyafSGedl/rSAR5fTJixhzCpsHA2OwfxpMwxkb2PZmrUNDj/KJuxbcbP9QnCz+G+/N9e1EF7xbGEL3hVtlbRJ61MxfGCIYsLgIIroEJm86M5ghUlWLNbUoJBQHgsldGwNepAERPUAsAisneO20mhFFnlKGeABtZwc5Ng+SZQCU09ZxIh1w7nGuuySkCE8er6566iFmp701TYlu5KuSOkUt9n/LE5+uFN2ZmgzD+aa4PsrCk+ZWi/gIvvNXNIW/+KKli9XsLWwkJDTltqoLaGVxtZyeN4S3JaEp2O2t7I5TZg+2drI/ZndNiwqePsqe52+LjgPMbmeK1XF2C9j6AzBqttLNPa6AeUzSZEkNrua6Vc10ViKPEYx9s7gikynXXyt5PfzjbZHwtW80hhX9i68fKLrjNIJwJGjGifefL9iNem9zDqeHErWNZ1df/9xpQIAMLhefk74C5yW5/3xQSQ/DUfZi1jjx5Xbu+BKr20CCKl0cJZFeFZB1j3dF4e/5cPLdaOfmnHfmHet+fTETIDPaRcg+CroDSNFxYrNp8VO9FhCzOEXgwiJSDlqn44WWYGRTCFEGYfVh97+jJV00k0kwAt7oUVKPtH1qORR5chA4RMKZV7qfofcXahdBopSsPzibrOIsIS0s044zW84M4q2tR6kQISXdc692DMf2prmJovu0bqyj7LtH34bvr8CDJatA1czrfqamumkjpX9VCdXUib6nJ+zV/MVjQVwWZGUA+Aztk0RLjybIj4x6q1sW07BQJEsHUOb3GjxGVc2xU84n8UCXRPuviHaT2DlBQwYXBZabVHoodoOS3g4Pe0Mi0eBTnxvWpBDovAB4QTwscqji9ZAqFyXlQJqoPOgmGy9xH7XOjal3WdTQkVJQYSb16M2rZ8RkjD6ZzmRcd7UwnxtTbvkCro0lXv/uke5/r8W4szTnijB5IrP4KHtYV28R5K9VEQwdd2hUuNV5dHqaauRpnHCv6VtBmBOyOkgXwkqsUG+mRR4SnATOz0yVDnzkzJedrT28rL9vuAa50/0LchmubPylKV9BA2rKH1iMU8N/TI4JGPLcvel0V27/Rpak8kuS+qU6HF4eM7vUQ4FwgRufNnQJastsJoUwQgsC5OY5H6D/hxtvCVWRlaTN6I9hUsaj2uk2Rlu0myhI2A22/04LrmHphf/kjm01u/zyeoSekTO31c/k2jnjiBk9wY4BLwKiRJjUCps3wKH8MZ2w0qXgt34v+BVbXEqWMbF+qzDiJEN065hMDjjdNdZ77eKXSDaF3hSqFD9t015KGhZKma4cu5YXFM4eH38fZ+DWdOVSpopfNd0u//zH/9twY+s79y9j3DOCWuEXS7aCxGYganK0Y85vkmM9X94MqnpAuYmJsa4ap/RAW+G7R2ds4h8dwfyeqahqBOiAXmH1nDk5637MRgHtqsSxOezw1EUK5J9POTmsRvbO1gGQq5otLQ9Qa4mXR52u+IdVheLwD9nxHF0tCEV7STcVJTqu8jgddrjjjrLv/x1NLxojCR+g1VXvI3q1ggg/5AnAPojLjwcvFLzJUTqSEIzvXgv8hXrDF735UCIl/EEz7Kn851P3SNC0fCsFBgffrIT6zdu/YQ20G+x2KD4r/usgpd2Le2ylae23paQ2spXDEAZKSgKgaZOWpWKfRQ4AGUS4f3hbvCZjeJAdXxfov5UZOIHJqm+6fDertiMlzHpYLikuCgrlzFo4W1HjdlZbed34Dq+J/4uJM+IJkAt4d2v4xxUKFrklq5nPPhIzUlcjNaK9bmdF4J9073y5FDBo2ktE1FjoWEcWtyxgcdvmCbziY12FRhE2kVwQSgJQHZJ1eFYfzyvAbGPC29QX+Kw6isj7XTNxcl4rZsCudG2cLzHd1U9caLclFzRI9MysGWMcO3QomN4rVr8AcnjlrA/JHgwtb4YZlhe839RKfOSNZL0tbP+vtByPCfZAJ6d0/OREzFWnbR9j6SHZwYmyNtsfWvX0RHWMxhqqQjNeiRLii8ZRBy3dcMVuKt4Xo6XUgGLmEjuez00M7Oy8cK0VZ+DGOAOP0BlQ1H+OO/I3HgGuwRlPyFmznE7z+qZzJCdQzWlK1HDkWShn75AR40a5JI2BHm4dzN4Zj/UOY10mDLhJSYLkHh8G/iTviFAxIMXuwdKWz/gqGizy84ZLly25WtGiuiwA8NA6la+oQnfLEyntA9ATT32yE2MfNNIXdcS5VtFoW7G2zcRf1dE9GzFO/BH3B3WCiTHnbX8SGR2uEbcJQKkrkRX0HTmgFW5P7M9KyI+skVj8orkBz4RUWpC6QXrAoGobNNu0HHWH7fJHrc0uF86SiwoBzgMcIrQH2wRuG9xhoUFzsHXwx9u34NWpg5Qi+crmnD82Mu+12zXICZ1PvR9wmr81xZ6MvrKcCaF34iwcGO048vg+Jc+SaSlM4Cny2cKeAyJW9opYyov8C9oEsdbllGb0duFZ76icUcoJ1xUHEAZ38YmXVNggprMJ5uy+USUPjNbb0R+GhlDxxbx9HDTcnMTCPRuE3nQIG7CkI2+9+KnQ5Z9y09v94sQzP/I5FyFt5yPQQQvSRNSFm2gXwppkqTGiVXxsmMzD0GBL1jVJRojI8c3Qz20PCU0o20H+muY+5qO3oOtuywcnXCW5LXZv3zH60351v7cTGexDCXYWGSn/3cz8d3k9887FcZGjNKbszel8ccPxCK87/f538vPXdJ8TXRlUH74hqmIqPc5mi6i8W/OruR5EougE4NDlDJ/Nm+xzUbA0mATpulufmw4DZlayNHirXJSQ1xDMOGCLnKVhfn6SMrwfV2+dWCEj+Sh7gkgOq+mVcjiw5oEZi9m1Tma6uwjCNCb5juld4CcJu6slovc5M8au4f7W0FkwvHTpoO1R9tUEIkUgV67rfD6Pd94jzv1137dJv4rJJHUzPKZqOovi/WIJwXt7q6t1uuKHYlXHakkIV/JFpMoL72YDGkgsNzQu35XjJdw97ZsvnEsn5uJ5u/9Hj7bKhm4aW03I/Y0QJavuSUHg3K+TjpvUC3bed2qQ7fF6/ciWg5SejjJQa1DJ5pk7K5szzv4ti+bO3VaDz0ut70POmTcnsX3IfmaqO+YXkNJ7OkZoou2ETyZPuZ0e2BQtt07sXSJUuGvwZXSdfLQOnzCssdXjakzng9tjOvcHO/ez4f7R3s7R7r1bYzoP9w4OAkzng98Q09mFcPxkgJ17XAdBhpgGde56UOfToP5wjOhsN5dEc+4DmnMM1uoNGiyjajLhCrKGMZCq46WQm7t7+wzcHG7dX1EFKc/OKf4GTJ1gubLb+RLDCPB6KFOGEY5afy+VnnIaWzligLnHnYqCcVHVnXVztQavd7xLTU/j/EaFRQRUS7Eftl3F4tvUUomMckwSL94GNLgKKzhsgyRvFSpCxr7EBNkUaFt/xVSMecmbhYGdj4LKMWaACZbaTuAlzhVUdNlPtLMJZsspE9NyOaX6dq+KS8r1gOm8mFTXic0AWsw3qMUcZdmbmVDSudu3eH9E0JszJ3HAP5X49YbhuAOLuUOIPutmZ05u4j2NHzphjx+0QjC73n41E9LJ17rRug99MGc9GFbXZRiUC8yIsqntAxomyKLc6OiQnS9Zve69dnvmbfbXapk9JzRzz2pP7ukwqvW8nCHN0iUSWKdKAbWpC6MjD0ogvAuqubhJ4DhycqaE3txWri8LlYY8CyWV+NQQ3M8/cUgbAqERnMEMqZVPklCDeXrvI6UFuAxLQBzVESsKVSiTYoOJruicPZT5YTBsADRthU0xJ8SdkVkzycUliZ3xDiXuCFinAVDJS6HCRO/kJQrLT7lPCaDchCVlaWZteV8S1u7I5uPsT3l25aToF73tf51/sXd473B/t/clz9FB6I3403buFgLaRJxpOSPoB5e2wkUDpZMB6c4U+f3vXsOHEtTMJ41yODdckNDywgpbup9MvpHI7fgn0Ci//PvfsbIYgFzfukk640n68cc/beP3UYGKPIGghnNg5pd0Pklr7ZTPfn9oAUn8obvd8zlmibd+ulI8jKz1vBsv8Y7WE0xsYZjlkETJMwGHjAf7hMPnBkmInXJJT5xq4pvoPCV7+tt92e/uZ7It9EsSBYLfQGUD/drAAQVuKXQXobeyQTiY/G7X/C7i0WVTIoQAonMrqvZGlVUbHQsdzKeYysfjTCrkqD3dUiG/77RxVMgPQPveuX9bhfz+g4MHPslKR/AbKeQpBfWTUcYhA4zmbDDN5x0Fv4wu/hwfapf7CtpIKuAH7idP8qacKC4UnZRS0DwSa19VFcTjoLukOv5gb29/hTpOkoBzFSQrXEvMLzUzV8hdmJ8R0MYgH0PspFFE26FqUWwfn4LGn/xNqziU/MbcScmi5yG99TSv30KEFzXu8Y1bnXLkhrtE8CyVXdYqjqjrP+ZnTvGZxo4PezmFGkVwnrG2C2ZZO6HhppwrGvz5NBsvC4P7JJGFCNPki7b0ABFN11fIuAChowKRwEJtDo1Dh76SGnYdDDOA9/O6PS7BEefxycoCDKYWVBW5QkoIUysHQhuw8ehnIwgbst/e1+9E2up11VpmaknNyXetY+HABFFXAe54wdyJqkJVWMqOHfflRUYcig2Xta7hJREo+PXr1y8TsMyYHgtJXgm24ww8PyfEC23oZ2UQPV/no0G4wenps1blFErBUIpbTMGeXENYo/dNlZ1gJfSn4MLvJRPo3IUNW7GkUklTsHQZ/TSbFQB3zql1cgJBqchvvuK9Tdbu2kRFqstH1URR7gnQU+9fvZYPnAUwxLcC3zUuDuwQNNHd/n7xDWVdU1up2qePvZ0CXCKD5QztaXQoQswsfPrbvEbKdmIs8Dd9oG9xAhYujK+IZJv5txkSJ8MbH/nz2DB7PFclBtGIk+s2GRy+oa+gQ/SemKeJcC4Ga+ER3cq+rq5BFXEGQQFuWDlZdeV+SckUqB9jpSe0kGfcoyiU2A6nUNXXTjvr0+PaP9X5QvEOKmtSVnQYzJOKqJpvIyBecU0t9/PeqwLWsOexiSgXRCLMnA1cZ2+SYroTFxt1jSoW3ktp4Ovpt18l4FcivHEi+XJz8pjIaguOqEzo2RPYFDMnWZ68dzuydiMeDjs6K2flAkMxYkC5C1gpc8uZ3gvMRIu907aESwWuumKMPL8N79T56rp4WddRl+pRCaEjpNl36OyxtG1IyN1NykWEKYFsJFELT64GqWLv8O7j6GLgCxGlGACK3F1wjXBRPB0y3qSwlvqUSZO3I3k1uHLqwm3hmScgXy4wi4+tzzofl0v03UMhNFKB8Hu3bNGVktINjMFFoUIfchD16jxn1Ff8emvaE+wVFTXkkUC9iFmoZ1C1j8vyHXLalFH1RGoX8i0lD3eOMawQ+uh2GoZ/XMuY99MsnLq9QCDIao2IMsZYHYho2c9vzD6AO0jXlWKS9HiUmbFWKdND9+bkiBSBf/7j/7xx4nJ6ox388x//F0Erk3L0tnByUY4ibE0I2wdZVwwHTtx3b05wUfEdYcD2hQaQqzsrClq1ObEM4YVo1613DBk38iMpGpjysK2vGau6ht/aNPt2WHiMIRUwf+fswtxXPTAjTLxpdF4J2azvTTz5TtcgH0Kt6L1Q3020i7Jtis406/mhgHKHw1wkIVdRiCQJlwIwum+ngtjkXKcKgBnFe45pyZix9eMfIDUq1siPAdV0DqD+C9YhYCeAvO7bS4LCAeeSwe4bTG7ot8UNqUkCxgpei4AymDk2gJBF7i70Ir3vzd1wJIUr3cfOTIC5AQ5qLm6V3GYtK8hX0sO5QH0GRiDMCvO8Vr+IMQfhmaOguObc5ngbTesocEA5Y7SaTNz6FJMveu7C6fXhuqk5u9ZNaQ9a+qGC8sPgnEELoedkxYgyJovFKJiW53yjSx8siihgM4sc8fQw1D0CXi1QR+RnX79+/kzSuWOJR+WKnMwAs4sKMM7aN5zQFJp9FRZngg0Unj1fO0zKFLGcwke7NtTTJSi14epECkbpMZnhJkiFgnyCmsXXEcYPySx2IjWPHJKwR2BP4wIhLgREvNQNRXFzR9KCr31COKkSu/BzXIG7ycZDn5xerxRn/xMQZGxldzxZBhZeAjlTTpdTUW20dDluV6cFO+Pp7kqhEcgblX5YuOOZO2XkoX5xceHmR+u/xomra4ywA6MkyWr79CbC9mF5WyCWonJK4jKO7/3k8d6o9mTa2YjE8ayzqJOD8LnWqeGWRld7BtdQXb4rIdGV3BvJUXU4hlaOCnCjbgphcoMbLtZ2JKHIcExhU/jDuRKMbBLVamPNTkkPaq8wP2qVtWLWwIkMpBlqkmiQYWaqpPYyD637EeuEzSqlUHdiiUb/HS5HQmVz/+BzqEFPbu7aydos7BYeeXfeszvudrmBBDP4b1gO726iTSNCANIqZQCr6RSDy+c1+BpQEwdhMKLPW0RCtoSL20+7bp68hYaixqBJJYkIbDkGmrpN6vbiaIGP51LqmO/UsVP4SiAxWCw7HX1OUQdXnll+UN3QJpot/MU4yJ6X7hJrqouFuTPTd3Weuj1gB0MwiA2TcxCJ8BA4ertqhIp/pS2iicxtQcYb+hujm8kZcq+1Ehc6ndqVIrUjfyWFG2UQX1EkrBoQZI2yUyUVn0hpUP+FIO4lF5mMwD5ffqg5JjlvmujGUK+DUX1En0IoKFM6oOiaT0rYLqhaTIp8lmSeie92UlfDuNwppUVQFIwkELvnQPkHfW8KSMvlrAz1iUeBqeRZe4XT9fs8Yo5SKozAlVr64pDviKuc7pWSQMaQbwfup+jl8G08KFSqDPuMvDAp4VUxKd45rb0pebyBEIX/xDMH15q7rhrMgYK7H7px9tnTyullGYbx52CcKeLkQr5Afh8sOOOtf2USxLqywa618f9EQTuWdXBq6BKLNyFUXS8mWODhclY1hV8Gg58wdkGiviDTRqCRkCu4n7yXhdLYy00qvm/MBQMB4wszK+EwCSmQ6Y0b+jTv8m0lZPgVca0/rKoF4FDmaLDVY/Um2zyPxLukIM448Ui0IJabp/mFXe8rnKEM4My3FZ2gO6p4Pwf6gBlz9I1aLrrA3XEROF6y+MTj9cS8kyg96R5A7Lz1czuhRTgOBV6cIIjciYIjn+jsBCUExummhgpb8rYhTBVN73nfqboIDiCbrj2JFbZWZ8q9Td7ZO+iwXYLyfldFoXvWRvZSTjbMaGZXPUPb+HIpks+3dauAVwcupIHeSEyVZ8Iq8SVCrVxXNt/jDlSiEfgbrqZ8wB6gu6sHpg4NPfGEVSavFpz5SdXQpcdC4jS/cMNPyoD1lUsp1hQLAtArxiWkCmJpndCAIdeJEI9ELgfqoBe5nnrRbsFzP48zhE0Dp3OnI9YXN6ookZzPJxjWXnAd04pT2jEwNYF1HIto2coe5TOTZqOOTDhMgTxnyZz0ZJpC4+i9wgmCjEw3hMmkJPZuMM2xvFxFFbQTd+fixqmn8EONqaCwSN6yWHaYd5Bb4b540GArgQdgAKggIdUkFkzMUhVoMjsTEm3Hq2zUAi5gK2pHQ4JYTuKdva29na33d1N7rOOWEX//ZVHpRhbzfCAhtREwC8O8dvq673CQ1x3uaxCU9XKCbAnuFm+k8BOzndIRPT19Fh0wP55YgxlA9Jf2EX8CI5a7ifkgOk2srl6IC9FbbPB+7j3vgDNlgBG98V3FGTkdaVLoZJVN7KVa3fjPP6HpIHfqXAq3A6J2rqt7ZaOJTaplTwtk9qdxNeN5BYvBCUZlTk4LWHYNwoaX/OgwmBhs/NWyGcUxMg14VK47wXDe/pCx1+rk8Toc4m3MPCCgvncr81DcLXuAv4t/By9hDx2ZqhxjfcpMVpolEhx9cZzhiT4nyqjUm3YQZjceclnNrCaWkvR4wykrEIgULp0IUYL8XVUigeD3XJbZXYslciU5q5ERdRB9pr/ozuzbwHLTpUvhcCBq7KaiLq5h27jVfXRVzvLkiw41KVFRnQS7DL6hf39XTEZgKxncN8ekh/3s+N+Os4f10ikJk0YRiRABYX5YQThGCkifvU4Y9BCQZ59sMgNypCVtGFFnfb5bFqgZoNE4+Ak+TOkxmLC+D3SL+5SMNby6UAEiutetD4ZrQ/Lw3fvZcOdo5/7R/kGIazvYWYdr2zs4vHew9x8GbEvgvj4VXNuuHVs1djfagPir67XZJs/xcYF9R1i3Ve0mUW+7B/eURRyLVKGLx1cwyagVz05ETtd2wZg+u0H7LEUQsxxkrUCqZjR6Tuh2tjkmx6snTH7oBlteznzNUO90ovseobzgMZIthySn6KCDZnjsbWrY5/zFM9Is0wk194cGwXfYCb8IQKdMmEsdeyhBnnX6DdkhQ0Y8qJJTyqobg9VVIfrLOBkzjNalUmE6+XS70cZ035G1Ea0LMvvOiBSm0q1g3wFXPhhbclQxq+7pzWx05S42jMxRcY1JoRUqiul5gdMqWqdeEs6A0qdQRhoLEZWiFjUQJcNQJ0jqGDz5uactJSOnStACwwt0hwVRYyMTG+MRCM7ii44oVhiEjQXYkdJJCxMnu2rd4smuKhMsVWSgGGeAguDdl+yiyxgUljLcv5CPA1V8y8slJ+5oev4UnP6IDvGhpCQ0VJBNqv1yGfqL9piJG8PJlu5xJyZGqVUgoQ1dve76jdRM8Zrw5pbTnvDTB1lfVCh4WnlnHOglLEwSv46jRnRasIUocNqJMOtWgn0hqSB5LaHB825zOtmkUA1zLiwInpKUhRMqoolm6KUxFN+alQnIKHwxbp7bYgu9w2BBXC3hgNUxlvrdqoy6lBIOjUHbz3efh5IolQD3oEOAry3zl0ynu59euyRLa0zPGrGwsrpt8rk9f2vCRjpS8jLw6ouujptEHNbgbBIQV8HRR3iSUzeTjf75NGCwZQjxdO6krFt4W6abYR5RWS5e0CBSQx1govW3YDCAz8Y+x/CZYr5gZM7gB6eYD/5HP9sZPOhnVzdzNxRSe5t5jnQK0mWii2dQBL78gd0MZcvMDcfhLlceCh9RigKRYSN09jM/sT+Uc6KCSO2GNjZTJXd0r0KVb4bDNU4eQmSwwy/MoysmhUnTyuHau0FbS+UpnmMtXFUuKLYj+hQWPC9W9wEtYHO2FddASfAbNxUzp+LcGMVuBZ+7nZdDY5N1sBFaBiObv9S4pS7GUqxKL/Xb8OLYkewHI9kL/toN/mLbEZ1LntMfMMUWWNn9Pb2lhPgwKolqFo6CgEiSQ7W/Zf1n9JPGwDlocMaZ8cZzX/onhtbmTTWyF6ZjnbLxSOi+lQlS3Zbk4e0tyb3B7iFkSO26/9+5bYbUvb0Hw/3AkDz87QzJ1UbVJ2NS7vlR5uN34K8cD8ieS9uUe96mPObnAdGNz4dG5aqW01blAzeF6FIRinKyMCGKlLQy30liQlDvmqlihBf397+Du24Ejv5CuXq9KPrL82fcMpma82U9r8iF01WbEwJPv/8d8siWouhV1wyfkDgVT9KA4MrMRASW6qgAdQicbO9zrHky98EtUPJ//zupLHvFzjdKlJJaeIItTpqfB0MxPlvUbycEqvfplkbgtU/sHpzYnVue13uDneFgdz/b3YHV3408P/v3153X/XsHe8OhObA7v+FxXb1dP5nz6madbf2BEFsNJqTbra0jJzogUfAIhYg5sisbT57Zvf0hn9nlIijjRSgtwARgdjST/nBjZPrCF5L8Exdb74utANe7sD/x+LeQeACVP0CMUyBYqiNS0JnizTmlWwTUwnALvM/QeKwnJSOg4SdIKGvyiigvj6NL3m0g7bDrALzANuvdjf2sHH9xsDPcvTfsffk1ANcrfX2C7VMLA6clD+bMpRu9ISTHp473/s79zvP9lJzIRP/v4fjkX5DSXA14xtyUaT8m0pf+PS1Qo2JVVpDgqWAfOgkMarg0TQ9s0DJFYJswXjSVrZls4BFgjBooC5dzUhanDJAwVic7eMBoHFFyQk5+CIiPiGOHYCfJ7l4gGrwpOHnQA2hKwc98HCG6NxgeZjsPQJTs7t9WiLqtd7h3+B8kRNcIkE9FisJUX0CpAQgHDyRm1eFFv2c4m+Q3nukwThvvajkpPe+5t3xWLEjFUW+04kiUkMg4rrlJAj45kUR6A9n9SY3gYP+QZAYwx3QjmrUEHopmynUU10DitcmpUrOvRqN+OMTIV+fzVq+vQLMSYUpJIQ0BhPYzYQIBFpUmu1NuFVvcCZxowKTsbw2jH2fjcuxzQ9mBOrlpww3o3dfipskPj5cGJFb6wIG+AksNZBsPWTPuNHf9PGxl31TX/cCtKyMX2UHXHhhO6dGqfQl/dSboQw+ImSRwuy3IwW/Di6DOWIwFqMPIeJUDR6z3G9fE4UWzoswCUkUPXpedJ0ScsbItLB+Jqdk5OQwZoYKthbsd3sVPZ6LZmZCJ4RgpVYzqjylXKU3ABu0GM9/y3SeLqPqTgK2+LW7a3ow9H2Fe4a/FSAoNrTWffOdY7vxO6OZe0FfaVxnPhe1QSm0hp4p3nqTOSDkrzzBEx9gs3njkiywTsYB17YnDjC9tUDYQukm8+e033QnnNSMC9Sx5nhVulpRjqMeQs6XFXivtsrs/b67OKyhmt3A6Re1sKWZBZ8SAIHO4lPAGe62lw3myza98Onem/jcTvYBnlL/CHzC5HaKbo92UjxBdUyEmhcS3Ii3wjlpfMn6J3a2hcgHZTY4lTcKOJB5Ig23vxojxmpzJNv6KLjAN2XVUMxkqvVDSs6o1POxgOt6qzUm25n28V+G6HF8WycrRQ2UqCvduKorz5tWzRtzKghRGEWfKm4W/T9EIMxkw+WPLZP24oXrywkG1BYj1tiIuhq8XgbCkh8WMxwYLzhuVNly4CwCYCgQedbXA6lT0sSDyR0UNooEDTpgBj0kYmGhdcEFXSK2HXgLGERmSDfFG8pDr3xlYI7aAY6lG1UTwDBcdbyUqnOzcjmMYznz3LstRrwH+w5Cpe1VJRNv0/eSipnZaMy9nMyCRePXMSuRSqhimm99ox0h5KNk1m8zJPeNep79TLyJRgBYXCdyeMWqYT6szNstZYtclR5Ho8yQhc7nlQPmI9tyJHx4IW4RBcFnMVM+HyYvci5ea+QfEJ8hDgOl+ZNjR1F9irl9qSk0UvZ8wcEtF4BkwQcF9XR8zvBiekRYTRJ/aMd9bLY2gXW2uMbk15WzQ3MxGaCXYzV94Btm1zR5zmqBwiE+Cu4B+GixD+wJIv2lQglVemKWWXDh8g6ZAtHFzzws0jADmE83amuZEUQWMYVSHloVnUEjMiE+eZFbDVraKy6p7nssJFu/F9YJfS6JHwGPQjVbwhPQ8EGe7XooFs0rq/fwTjgCENfQb9rSG65D94I1PfN7f2qGE+LtBO3rkbc6IaqzoD4JMSbA40NU3QwYBDoyvyN+hK4skFr32NB8XiWIGOKXsxyokEKGKQewc3XTs8Mcr3V6nePVCSidegVq3aTaYL88n5Sh7/M2pfLdye1BmG/iJoDpU8JIXpuw2jdYjDTqGrWV63bBNfUUAk5y5s4Gb4Mw94VSQM3DB+OwjNg6RPhIzOZhqw40K3hxVK9LU4/LEVgJ0ekuQCher9ua1d4t61rNF0Hic65LZZtyrCT22nG4gnzA/T024hYxUiWLYv6qo7yrp5ATcu6LTwqDXYxM92rJBU08AavA0n41uAN3AwzemO2aAgnQiB+8FUZ5wz0kw3G5kbafXy2/+nK5hqhcCvKvuwe8bwK2BM3nZEIMP6pPJs7WilEp8KEMfdxnVtI4V4KDa4Lo+I/82HjdV27ZbFW76hrQXFSdEL9GfmwiU1RaBGMbRLZjav0puRHIi1NcQ3MdLYo0gWB7VZtavgleYTp+86N5crbdYO3jW/CHreibFLgCaqvcrRUOUMgMpBpg++PzG3RkZJUwWSIq9JhTr/mLX7S8C4h9mw+HR/u7RbkQwC6HitRUf9u9rIMGM4TdC4ne70j+NOMKPtBpYSghCB7sQOYAOJY7sRgxrjGGFg5h+1sRlT+W5DrC9m9Vjyq/HIwspg7VJTg4wBgR+J3NUw6+XBWbm1UqzGLqunYVA0CTQ/K8r7KA58jDqMOZp3Nd3ZhWWzWWOCVsYVdxIdzUBBe5HtUhO+FFUQicTQ2tulFb29SQjoYeKdNgPsFj7QU6PRXDtianNf90P/roX/HUY/HUQ/BW2uRf8tRv8ZUdin6OnkDEdUenIGz+Rqg3o0oe1PIr41HeGOw/u976Mds1Rdpx946TXU3fcqK4VLP1X+kC04lQuCiPMFgK2fjA///RxR9OBGttnkeOWm7kyziQUygLMya8Lp0QubgbD/cO9ncH5YrC7NS4bhPw46YNSDduAON3IDWzmDvVCmjk8TMvB6/mAY97bTKG67WTqwfbO/nZ0vrfms0sWze42dvJCOnsxczPwVXV60wDaUj7F6TxDpED0JvEQrqppEXe23Rk1PgDozc79o739o90Hkaxfm3S1++DBg717Jmq8/5uK+pTQDOOqqScADzNsfT4YXZWTcVLowtPRhoW0wcm4Q/QO91D0TsrLq8V1Af+bYeMsay17jDSH32wJQrVBoIK4aTSjiI0GagUh8sXic4oCSy0p5B7B4O51nsZ67e1qppEImt0vHxcLjBvR6aXn3fFy3/z+d+T4BCFP1Q9E52qgwB1XPPj975xWWgA3iztt7q/yIrtDjPtnGDhs7mSff/fy8eMzluSfZ3ezP+TT+b/g/2T/Tdr0T3Pu19l4PDmb+B/dzf4OGg/1MhKXbBX1zFVd7mCfz86+ffLq9OTFN5//+j4tiKYDuDH8uHLnXgeSY3O5Qxt9QDvuE5BBh1BhbDjMnFY13HEq522RK3t7+7v3LfzvN0z7XCFHQohc93M/gkI4dO02y3OVIs3Z1J1IFEaoHe7AiOalwO/w4/Lo4PDBfVUgd2Fib5qzaypvafVRJwT5UylVYrRRLnwwMRkN0MEeNOpGxcU6UVO9B53uHTwYHu5Ityolc/fPv+OB+OFq4M4l7pld1wd6ugbIAs3Vgesb/A7WZ4OHxsUGDxVruvsxIaeTI+Yp08H5v8dF+HfRmEksIYPzwf2DzabF/fXMo5u0r9bH4yL5cdFqZOPXO3Rb+zGq+kDEUtah7yCnAtOzEm6ZcZ1fDtwlMxjX1dxwswdD/nDt0bt+uPaKD/e+uLwHw/2dDXe9E6YUMwinKvqUXzj6tIhb2HhpD9yrPuJK79dY5lhhSAQxHyH2hciyZh44Hi7or22F3urXtlL82jfiJdt78GCzJXMSEjMdw7kIPxwXiQ+L6OcbL9c9KKCIYISHQLbBvjRM15LSLMiGD1YyMH6xdw11QSTrGAhbRyOohlCYfLT2WSp9tPaLjzc/ui3ubbwtvpWSoXZb2A91W9gPi+jnm2+LfRVYWoA64ISVFwXwxMRprshfB4SH4iu5rmlGXn79MjzaH6Vp3gwfo+nio0wIbYGdw90NhTn42wDmFW8B+6FuAfthEf184y0wPHR/f6dUdbUtagmmnuYCKX0tGotLAKUVJgvX0sd4klJ8hpyPlBCBGRfIawHMEEzCAJ4/dAO6WSX6vOkUSDRvrMt8OQ032H/CgdPK/SccePGfcKvI0Rve31hNZjdJrCaHH6uaHH5ctBrZ/Ajuu7/p3mG18bo4J81RnTE+SpSPp+UsKW4ySkt0c3wF6GclEX9NJDbfEmcsqg59xpn04yxuWAnxf4fH7dMeJB+tT3uQxSe+3GiegxXO0CY0/Xdgw+45G4GcfgOkRBqIe0HCkIORO41N0dvsrEFFcCZFJgHwz3/8b89VJ5HNR9yk2YW/4Ie0M37BD4tfMNSPbfmzY0xSQC+BZWHDOYcfalI0/zA43x1fy8nq+LrobPw3mIsHfi7QhKakg00n5IEf84n9tZ2Vlc+Mi/XPFKv7+viTBG4qmSQsm8isNxvOEvxcA2HBzyOn2oqHvFNtxUPFmu5+g5naQ9E3IN/0wHpIN91Srq2XdQU5pOzd9jsp9RVvoNRXRbLB3+ZM/fIJAFdS+93jT70zqv3G5tPf6GVRXP3Sl20L0vhT/7JtsWk+/Q1elqIJ5eiXvKuzLB/Cb2PD1H6ohqn9sIh+/tu85/V8OmGqc/XKb3aC77NTf9QZQVj5CJ/oVY8UKzva3GoAnYxUkj8KahxtKFu2jXqYlMhl560qY1BR6rxm7hBvoqdQFDQNc5aAc5TNu7khFItMhU9wZLwun+DIik9xNeEcAZiBnEmbnRxDutP2YXV9K+uS/rboavkWtjXEZCRp3XDeBDOyBWaSEjljWruCzygiSVRt5GLQMhCNW58Er8a9HY84kp8zugIwRLFd/ckOUDT/T3aAxSe8xD+KfhEE2gdI5haH1Cnqft9G3QcaWP+Ql6OE37EIADgSzkZz6MHZ+PyNmP5AK43DfIBjs+adBhABxqF/DA2PM6C34TP3KyeQxvEZ1oiW/d708BBLbnMP5/rHsE0SBB+7iTHkHavppKGXBHd0Svp46Rc+oHEX98A7+bd9+efMxw1d8T83YOdwGyUAD+8+4P4HNMXb9g+AQrUWYG9IszkQcv0BlR/Z5v3PvzFzvmfYpGiat8M/TT9+GWCGecK3x2MhTpFHw9WwPYTzHo2qY2H2eRK2r+ejC3nWrgwgXa7nA1yIbfmHeVBXCK5Xy5i+bf8YYPmm2kyRrNz+XhLevZ34TH4druSP2NxV3lzhKNzcDQ+GexfnO+ODg/H44uLwwYMH9w4O9vfu37+fH47OR4LRAdzcJG8WZwAJwrIhZdGcUZ4qQIeGB4e7e/v7gAIhDPiZM+7Phmc7Z4jBPgPc+fnR8F9+/P8AeOEzRA==', 'yes');
INSERT INTO `cp_options` (`option_id`, `option_name`, `option_value`, `autoload`) VALUES
(1664, 'widget_woof_widget', 'a:1:{s:12:\"_multiwidget\";i:1;}', 'yes'),
(1665, 'woof_first_init', '1', 'yes'),
(1666, 'woof_set_automatically', '0', 'yes'),
(1667, 'woof_autosubmit', '1', 'yes'),
(1668, 'woof_show_count', '1', 'yes'),
(1669, 'woof_show_count_dynamic', '0', 'yes'),
(1670, 'woof_hide_dynamic_empty_pos', '0', 'yes'),
(1671, 'woof_try_ajax', '0', 'yes'),
(1672, 'woof_checkboxes_slide', '1', 'yes'),
(1673, 'woof_hide_red_top_panel', '0', 'yes'),
(1674, 'woof_sort_terms_checked', '0', 'yes'),
(1675, 'woof_filter_btn_txt', '', 'yes'),
(1676, 'woof_reset_btn_txt', '', 'yes'),
(1677, 'woof_settings', 'a:1:{s:10:\"use_chosen\";i:1;}', 'yes'),
(1678, 'woof_version', '1.2.2.1', 'yes'),
(1679, 'woof_alert', 'a:2:{s:29:\"woocommerce_currency_switcher\";i:1;s:23:\"woocommerce_bulk_editor\";i:1;}', 'no'),
(1931, 'z_taxonomy_image30', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1933, 'z_taxonomy_image28', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/cat_feminino_moletons.jpg', 'yes'),
(1935, 'z_taxonomy_image29', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1940, 'z_taxonomy_image32', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1938, 'z_taxonomy_image31', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1942, 'z_taxonomy_image34', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(1944, 'z_taxonomy_image33', 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 'yes'),
(2418, '_transient_timeout_plugin_slugs', '1562358256', 'no'),
(2419, '_transient_plugin_slugs', 'a:10:{i:0;s:59:\"ajax-search-for-woocommerce/ajax-search-for-woocommerce.php\";i:1;s:41:\"base-centurySports/base-centurySports.php\";i:2;s:39:\"categories-images/categories-images.php\";i:3;s:36:\"contact-form-7/wp-contact-form-7.php\";i:4;s:21:\"meta-box/meta-box.php\";i:5;s:35:\"redux-framework/redux-framework.php\";i:6;s:27:\"woocommerce/woocommerce.php\";i:7;s:39:\"woocommerce-admin/woocommerce-admin.php\";i:8;s:37:\"woocommerce-products-filter/index.php\";i:9;s:41:\"yith-woocommerce-ajax-navigation/init.php\";}', 'no'),
(2390, '_transient_wc_count_comments', 'O:8:\"stdClass\":7:{s:14:\"total_comments\";i:80;s:3:\"all\";i:80;s:8:\"approved\";s:2:\"80\";s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(2391, '_transient_as_comment_count', 'O:8:\"stdClass\":7:{s:8:\"approved\";s:1:\"1\";s:14:\"total_comments\";i:1;s:3:\"all\";i:1;s:9:\"moderated\";i:0;s:4:\"spam\";i:0;s:5:\"trash\";i:0;s:12:\"post-trashed\";i:0;}', 'yes'),
(2436, 'wpcf7', 'a:2:{s:7:\"version\";s:5:\"5.1.3\";s:13:\"bulk_validate\";a:4:{s:9:\"timestamp\";d:1562154611;s:7:\"version\";s:5:\"5.1.3\";s:11:\"count_valid\";i:1;s:13:\"count_invalid\";i:0;}}', 'yes'),
(2891, '_transient_timeout_wc_related_12', '1562425688', 'no'),
(2892, '_transient_wc_related_12', 'a:1:{s:50:\"limit=5&exclude_ids%5B0%5D=0&exclude_ids%5B1%5D=12\";a:15:{i:0;s:2:\"52\";i:1;s:2:\"54\";i:2;s:2:\"56\";i:3;s:2:\"58\";i:4;s:2:\"65\";i:5;s:2:\"87\";i:6;s:2:\"89\";i:7;s:2:\"91\";i:8;s:2:\"97\";i:9;s:3:\"104\";i:10;s:3:\"105\";i:11;s:3:\"106\";i:12;s:3:\"107\";i:13;s:3:\"108\";i:14;s:3:\"109\";}}', 'no');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_postmeta`
--

DROP TABLE IF EXISTS `cp_postmeta`;
CREATE TABLE IF NOT EXISTS `cp_postmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `post_id` (`post_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=739 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_postmeta`
--

INSERT INTO `cp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 2, '_wp_page_template', 'default'),
(2, 3, '_wp_page_template', 'default'),
(3, 5, '_wp_attached_file', 'woocommerce-placeholder.png'),
(4, 5, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:27:\"woocommerce-placeholder.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"woocommerce-placeholder-768x768.png\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:37:\"woocommerce-placeholder-1024x1024.png\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(7, 12, '_edit_last', '1'),
(8, 12, '_edit_lock', '1561904145:1'),
(9, 13, '_wp_attached_file', '2019/05/q1.png'),
(10, 13, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:67;s:6:\"height\";i:66;s:4:\"file\";s:14:\"2019/05/q1.png\";s:5:\"sizes\";a:0:{}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(11, 12, '_thumbnail_id', '50'),
(12, 12, '_sku', '123'),
(13, 12, '_regular_price', '120'),
(14, 12, 'total_sales', '0'),
(15, 12, '_tax_status', 'taxable'),
(16, 12, '_tax_class', ''),
(17, 12, '_manage_stock', 'yes'),
(18, 12, '_backorders', 'no'),
(19, 12, '_sold_individually', 'no'),
(20, 12, '_virtual', 'no'),
(21, 12, '_downloadable', 'no'),
(22, 12, '_download_limit', '-1'),
(23, 12, '_download_expiry', '-1'),
(24, 12, '_stock', '10'),
(25, 12, '_stock_status', 'instock'),
(26, 12, '_wc_average_rating', '0'),
(27, 12, '_wc_review_count', '0'),
(28, 12, '_product_version', '3.6.4'),
(29, 12, '_price', '120'),
(30, 12, '_product_image_gallery', '5'),
(31, 14, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559257412;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(32, 15, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559760549;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(33, 16, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1559764255;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(34, 17, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561335708;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(35, 23, '_wp_attached_file', '2019/06/destaque.png'),
(36, 23, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:761;s:4:\"file\";s:20:\"2019/06/destaque.png\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"destaque-300x119.png\";s:5:\"width\";i:300;s:6:\"height\";i:119;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:20:\"destaque-768x304.png\";s:5:\"width\";i:768;s:6:\"height\";i:304;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:21:\"destaque-1024x406.png\";s:5:\"width\";i:1024;s:6:\"height\";i:406;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"destaque-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"destaque-600x238.png\";s:5:\"width\";i:600;s:6:\"height\";i:238;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"destaque-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"destaque-600x238.png\";s:5:\"width\";i:600;s:6:\"height\";i:238;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"destaque-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(37, 22, '_edit_last', '1'),
(38, 22, '_edit_lock', '1561335107:1'),
(39, 22, '_thumbnail_id', '23'),
(40, 22, 'CenturySports_destaque_link', '#'),
(41, 24, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561339351;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(42, 25, '_wp_attached_file', '2019/06/logo-1.png'),
(43, 25, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:500;s:6:\"height\";i:90;s:4:\"file\";s:18:\"2019/06/logo-1.png\";s:5:\"sizes\";a:6:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-150x90.png\";s:5:\"width\";i:150;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"logo-1-300x54.png\";s:5:\"width\";i:300;s:6:\"height\";i:54;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"logo-1-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"logo-1-300x90.png\";s:5:\"width\";i:300;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"logo-1-100x90.png\";s:5:\"width\";i:100;s:6:\"height\";i:90;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(44, 26, '_menu_item_type', 'post_type'),
(45, 26, '_menu_item_menu_item_parent', '0'),
(46, 26, '_menu_item_object_id', '9'),
(47, 26, '_menu_item_object', 'page'),
(48, 26, '_menu_item_target', ''),
(49, 26, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(50, 26, '_menu_item_xfn', ''),
(51, 26, '_menu_item_url', ''),
(53, 27, '_menu_item_type', 'post_type'),
(54, 27, '_menu_item_menu_item_parent', '0'),
(55, 27, '_menu_item_object_id', '8'),
(56, 27, '_menu_item_object', 'page'),
(57, 27, '_menu_item_target', ''),
(58, 27, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(59, 27, '_menu_item_xfn', ''),
(60, 27, '_menu_item_url', ''),
(62, 28, '_menu_item_type', 'post_type'),
(63, 28, '_menu_item_menu_item_parent', '0'),
(64, 28, '_menu_item_object_id', '7'),
(65, 28, '_menu_item_object', 'page'),
(66, 28, '_menu_item_target', ''),
(67, 28, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(68, 28, '_menu_item_xfn', ''),
(69, 28, '_menu_item_url', ''),
(71, 29, '_menu_item_type', 'post_type'),
(72, 29, '_menu_item_menu_item_parent', '0'),
(73, 29, '_menu_item_object_id', '6'),
(74, 29, '_menu_item_object', 'page'),
(75, 29, '_menu_item_target', ''),
(76, 29, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(77, 29, '_menu_item_xfn', ''),
(78, 29, '_menu_item_url', ''),
(80, 30, '_menu_item_type', 'post_type'),
(81, 30, '_menu_item_menu_item_parent', '26'),
(82, 30, '_menu_item_object_id', '2'),
(83, 30, '_menu_item_object', 'page'),
(84, 30, '_menu_item_target', ''),
(85, 30, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(86, 30, '_menu_item_xfn', ''),
(87, 30, '_menu_item_url', ''),
(89, 31, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561342969;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(90, 32, '_menu_item_type', 'custom'),
(91, 32, '_menu_item_menu_item_parent', '26'),
(92, 32, '_menu_item_object_id', '32'),
(93, 32, '_menu_item_object', 'custom'),
(94, 32, '_menu_item_target', ''),
(95, 32, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(96, 32, '_menu_item_xfn', ''),
(97, 32, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/orders/'),
(129, 36, '_menu_item_object', 'custom'),
(99, 33, '_menu_item_type', 'custom'),
(100, 33, '_menu_item_menu_item_parent', '26'),
(101, 33, '_menu_item_object_id', '33'),
(102, 33, '_menu_item_object', 'custom'),
(103, 33, '_menu_item_target', ''),
(104, 33, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(105, 33, '_menu_item_xfn', ''),
(106, 33, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/downloads/'),
(128, 36, '_menu_item_object_id', '36'),
(108, 34, '_menu_item_type', 'custom'),
(109, 34, '_menu_item_menu_item_parent', '26'),
(110, 34, '_menu_item_object_id', '34'),
(111, 34, '_menu_item_object', 'custom'),
(112, 34, '_menu_item_target', ''),
(113, 34, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(114, 34, '_menu_item_xfn', ''),
(115, 34, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/edit-address/'),
(127, 36, '_menu_item_menu_item_parent', '26'),
(117, 35, '_menu_item_type', 'custom'),
(118, 35, '_menu_item_menu_item_parent', '26'),
(119, 35, '_menu_item_object_id', '35'),
(120, 35, '_menu_item_object', 'custom'),
(121, 35, '_menu_item_target', ''),
(122, 35, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(123, 35, '_menu_item_xfn', ''),
(124, 35, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/edit-account/'),
(126, 36, '_menu_item_type', 'custom'),
(130, 36, '_menu_item_target', ''),
(131, 36, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(132, 36, '_menu_item_xfn', ''),
(133, 36, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/orders/'),
(135, 37, '_menu_item_type', 'custom'),
(136, 37, '_menu_item_menu_item_parent', '26'),
(137, 37, '_menu_item_object_id', '37'),
(138, 37, '_menu_item_object', 'custom'),
(139, 37, '_menu_item_target', ''),
(140, 37, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(141, 37, '_menu_item_xfn', ''),
(142, 37, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/downloads/'),
(144, 38, '_menu_item_type', 'custom'),
(145, 38, '_menu_item_menu_item_parent', '26'),
(146, 38, '_menu_item_object_id', '38'),
(147, 38, '_menu_item_object', 'custom'),
(148, 38, '_menu_item_target', ''),
(149, 38, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(150, 38, '_menu_item_xfn', ''),
(151, 38, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/edit-address/'),
(153, 39, '_menu_item_type', 'custom'),
(154, 39, '_menu_item_menu_item_parent', '26'),
(155, 39, '_menu_item_object_id', '39'),
(156, 39, '_menu_item_object', 'custom'),
(157, 39, '_menu_item_target', ''),
(158, 39, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(159, 39, '_menu_item_xfn', ''),
(160, 39, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/edit-account/'),
(180, 42, '_wp_attached_file', '2019/06/selo.png'),
(162, 40, '_menu_item_type', 'custom'),
(163, 40, '_menu_item_menu_item_parent', '26'),
(164, 40, '_menu_item_object_id', '40'),
(165, 40, '_menu_item_object', 'custom'),
(166, 40, '_menu_item_target', ''),
(167, 40, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(168, 40, '_menu_item_xfn', ''),
(169, 40, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/customer-logout/?_wpnonce=99db4bf76c'),
(181, 42, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:254;s:6:\"height\";i:112;s:4:\"file\";s:16:\"2019/06/selo.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-150x112.png\";s:5:\"width\";i:150;s:6:\"height\";i:112;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"selo-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:14:\"selo-64x28.png\";s:5:\"width\";i:64;s:6:\"height\";i:28;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(171, 41, '_menu_item_type', 'custom'),
(172, 41, '_menu_item_menu_item_parent', '26'),
(173, 41, '_menu_item_object_id', '41'),
(174, 41, '_menu_item_object', 'custom'),
(175, 41, '_menu_item_target', ''),
(176, 41, '_menu_item_classes', 'a:1:{i:0;s:0:\"\";}'),
(177, 41, '_menu_item_xfn', ''),
(178, 41, '_menu_item_url', 'http://centurysports.hcdesenvolvimentos.com/my-account/lost-password/'),
(182, 43, '_edit_lock', '1561343348:1'),
(183, 43, '_wp_page_template', 'paginas/inicial.php'),
(184, 46, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561346607;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(185, 47, '_wp_attached_file', '2019/06/categoria1-1.png'),
(186, 47, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:555;s:6:\"height\";i:758;s:4:\"file\";s:24:\"2019/06/categoria1-1.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:24:\"categoria1-1-220x300.png\";s:5:\"width\";i:220;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:24:\"categoria1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:24:\"categoria1-1-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:24:\"categoria1-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:22:\"categoria1-1-64x87.png\";s:5:\"width\";i:64;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(187, 48, '_wp_attached_file', '2019/06/categoria.png'),
(188, 48, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:172;s:6:\"height\";i:172;s:4:\"file\";s:21:\"2019/06/categoria.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:21:\"categoria-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:19:\"categoria-64x64.png\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(189, 49, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561350272;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(190, 50, '_wp_attached_file', '2019/05/tenis-1.png'),
(191, 50, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:267;s:6:\"height\";i:212;s:4:\"file\";s:19:\"2019/05/tenis-1.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:19:\"tenis-1-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:17:\"tenis-1-64x51.png\";s:5:\"width\";i:64;s:6:\"height\";i:51;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(192, 52, '_edit_last', '1'),
(193, 52, '_edit_lock', '1561904147:1'),
(194, 53, '_wp_attached_file', '2019/06/D24-1738-304_zoom1.jpg'),
(195, 53, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:30:\"2019/06/D24-1738-304_zoom1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"D24-1738-304_zoom1-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"D24-1738-304_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"D24-1738-304_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(196, 52, '_thumbnail_id', '53'),
(197, 52, 'total_sales', '0'),
(198, 52, '_tax_status', 'taxable'),
(199, 52, '_tax_class', ''),
(200, 52, '_manage_stock', 'no'),
(201, 52, '_backorders', 'no'),
(202, 52, '_sold_individually', 'no'),
(203, 52, '_virtual', 'no'),
(204, 52, '_downloadable', 'no'),
(205, 52, '_download_limit', '-1'),
(206, 52, '_download_expiry', '-1'),
(207, 52, '_stock', NULL),
(208, 52, '_stock_status', 'instock'),
(209, 52, '_wc_average_rating', '0'),
(210, 52, '_wc_review_count', '0'),
(211, 52, '_product_version', '3.6.4'),
(212, 54, '_edit_last', '1'),
(213, 54, '_edit_lock', '1561904283:1'),
(214, 55, '_wp_attached_file', '2019/06/497-9505-026_zoom1.jpg'),
(215, 55, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:544;s:6:\"height\";i:544;s:4:\"file\";s:30:\"2019/06/497-9505-026_zoom1.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"497-9505-026_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"497-9505-026_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(216, 54, '_thumbnail_id', '55'),
(217, 54, 'total_sales', '0'),
(218, 54, '_tax_status', 'taxable'),
(219, 54, '_tax_class', ''),
(220, 54, '_manage_stock', 'yes'),
(221, 54, '_backorders', 'no'),
(222, 54, '_sold_individually', 'no'),
(223, 54, '_virtual', 'no'),
(224, 54, '_downloadable', 'no'),
(225, 54, '_download_limit', '-1'),
(226, 54, '_download_expiry', '-1'),
(227, 54, '_stock', '88898'),
(228, 54, '_stock_status', 'instock'),
(229, 54, '_wc_average_rating', '0'),
(230, 54, '_wc_review_count', '0'),
(231, 54, '_product_version', '3.6.4'),
(232, 56, '_edit_last', '1'),
(233, 56, '_edit_lock', '1561904152:1'),
(234, 57, '_wp_attached_file', '2019/06/B78-2495-172_zoom1.jpg'),
(235, 57, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1200;s:6:\"height\";i:1200;s:4:\"file\";s:30:\"2019/06/B78-2495-172_zoom1.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-768x768.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:768;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:32:\"B78-2495-172_zoom1-1024x1024.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:1024;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-600x600.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:600;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:30:\"B78-2495-172_zoom1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:28:\"B78-2495-172_zoom1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(236, 56, '_thumbnail_id', '57'),
(237, 56, 'total_sales', '0'),
(238, 56, '_tax_status', 'taxable'),
(239, 56, '_tax_class', ''),
(240, 56, '_manage_stock', 'yes'),
(241, 56, '_backorders', 'no'),
(242, 56, '_sold_individually', 'no'),
(243, 56, '_virtual', 'no'),
(244, 56, '_downloadable', 'no'),
(245, 56, '_download_limit', '-1'),
(246, 56, '_download_expiry', '-1'),
(247, 56, '_stock', '4543534'),
(248, 56, '_stock_status', 'instock'),
(249, 56, '_wc_average_rating', '0'),
(250, 56, '_wc_review_count', '0'),
(251, 56, '_product_version', '3.6.4'),
(252, 58, '_edit_last', '1'),
(253, 58, '_edit_lock', '1561904155:1'),
(254, 58, '_thumbnail_id', '50'),
(255, 58, 'total_sales', '0'),
(256, 58, '_tax_status', 'taxable'),
(257, 58, '_tax_class', ''),
(258, 58, '_manage_stock', 'yes'),
(259, 58, '_backorders', 'no'),
(260, 58, '_sold_individually', 'no'),
(261, 58, '_virtual', 'no'),
(262, 58, '_downloadable', 'no'),
(263, 58, '_download_limit', '-1'),
(264, 58, '_download_expiry', '-1'),
(265, 58, '_stock', '222'),
(266, 58, '_stock_status', 'instock'),
(267, 58, '_wc_average_rating', '0'),
(268, 58, '_wc_review_count', '0'),
(269, 58, '_product_version', '3.6.4'),
(270, 59, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561679395;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(271, 58, '_regular_price', '12.50'),
(280, 56, '_sale_price', '242.00'),
(273, 58, '_price', '12.50'),
(274, 56, '_regular_price', '250.00'),
(275, 56, '_price', '242.00'),
(276, 54, '_regular_price', '24.90'),
(277, 54, '_sale_price', '11.49'),
(278, 54, '_price', '11.49'),
(279, 60, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561682995;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(281, 61, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561686613;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(282, 62, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561690324;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(283, 63, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561693946;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(284, 64, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561697570;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(285, 65, '_edit_last', '1'),
(286, 65, '_edit_lock', '1561904156:1'),
(287, 66, '_wp_attached_file', '2019/06/296485.jpg'),
(288, 66, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:240;s:6:\"height\";i:240;s:4:\"file\";s:18:\"2019/06/296485.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"296485-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:16:\"296485-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(289, 67, '_wp_attached_file', '2019/06/296485-1.jpg'),
(290, 67, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:240;s:6:\"height\";i:240;s:4:\"file\";s:20:\"2019/06/296485-1.jpg\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"296485-1-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"296485-1-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(291, 65, '_thumbnail_id', '66'),
(658, 65, 'CenturySports_produto_zoom', '67'),
(293, 65, '_regular_price', '180.00'),
(294, 65, 'total_sales', '0'),
(295, 65, '_tax_status', 'taxable'),
(296, 65, '_tax_class', ''),
(297, 65, '_manage_stock', 'yes'),
(298, 65, '_backorders', 'no'),
(299, 65, '_sold_individually', 'no'),
(300, 65, '_virtual', 'no'),
(301, 65, '_downloadable', 'no'),
(302, 65, '_download_limit', '-1'),
(303, 65, '_download_expiry', '-1'),
(304, 65, '_stock', '999'),
(305, 65, '_stock_status', 'instock'),
(306, 65, '_wc_average_rating', '0'),
(307, 65, '_wc_review_count', '0'),
(308, 65, '_product_version', '3.6.4'),
(309, 65, '_price', '180.00'),
(310, 68, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561701192;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(311, 69, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561823405;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(312, 70, '_edit_last', '1'),
(313, 70, '_edit_lock', '1561820030:1'),
(314, 71, '_wp_attached_file', '2019/06/camiseta.png'),
(315, 71, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:65;s:6:\"height\";i:61;s:4:\"file\";s:20:\"2019/06/camiseta.png\";s:5:\"sizes\";a:1:{s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"camiseta-64x60.png\";s:5:\"width\";i:64;s:6:\"height\";i:60;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(316, 70, '_thumbnail_id', '71'),
(317, 70, 'CenturySports_banner_link', '#'),
(318, 72, '_edit_last', '1'),
(319, 72, '_edit_lock', '1561820052:1'),
(320, 72, '_thumbnail_id', '71'),
(321, 72, 'CenturySports_banner_link', '#'),
(322, 73, '_edit_last', '1'),
(323, 73, '_edit_lock', '1561820076:1'),
(324, 73, '_thumbnail_id', '71'),
(325, 73, 'CenturySports_banner_link', '#'),
(326, 74, '_edit_last', '1'),
(327, 74, '_edit_lock', '1561820096:1'),
(328, 74, '_thumbnail_id', '71'),
(329, 74, 'CenturySports_banner_link', '#'),
(330, 75, '_edit_last', '1'),
(331, 75, '_edit_lock', '1561820695:1'),
(332, 75, '_thumbnail_id', '71'),
(334, 75, 'CenturySports_banner_link', 'https://www.google.com.br'),
(335, 76, '_wp_attached_file', '2019/06/banner.png'),
(336, 76, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1920;s:6:\"height\";i:279;s:4:\"file\";s:18:\"2019/06/banner.png\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"banner-300x44.png\";s:5:\"width\";i:300;s:6:\"height\";i:44;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:18:\"banner-768x112.png\";s:5:\"width\";i:768;s:6:\"height\";i:112;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:19:\"banner-1024x149.png\";s:5:\"width\";i:1024;s:6:\"height\";i:149;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:18:\"banner-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:17:\"banner-600x87.png\";s:5:\"width\";i:600;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:18:\"banner-300x279.png\";s:5:\"width\";i:300;s:6:\"height\";i:279;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:17:\"banner-600x87.png\";s:5:\"width\";i:600;s:6:\"height\";i:87;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:18:\"banner-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"banner-64x9.png\";s:5:\"width\";i:64;s:6:\"height\";i:9;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(337, 77, '_wp_attached_file', '2019/06/neckties-210347_960_720.jpg'),
(338, 77, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:35:\"2019/06/neckties-210347_960_720.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:35:\"neckties-210347_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:33:\"neckties-210347_960_720-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(339, 78, '_wp_attached_file', '2019/06/tartan-track-2678544_960_720.jpg'),
(340, 78, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:960;s:6:\"height\";i:640;s:4:\"file\";s:40:\"2019/06/tartan-track-2678544_960_720.jpg\";s:5:\"sizes\";a:10:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x200.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:200;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-768x512.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:512;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-600x400.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:400;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:40:\"tartan-track-2678544_960_720-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:38:\"tartan-track-2678544_960_720-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"8\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:11:\"NIKON D7100\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:2:\"44\";s:3:\"iso\";s:3:\"100\";s:13:\"shutter_speed\";s:5:\"0.004\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(341, 79, '_wp_attached_file', '2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg'),
(342, 79, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:215;s:4:\"file\";s:76:\"2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x202.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:202;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x215.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-300x215.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:215;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:76:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:74:\"184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe-64x43.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:43;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(343, 80, '_wp_attached_file', '2019/06/2000px-Adidas_klassisches_logo.svg_.png'),
(344, 80, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:2000;s:6:\"height\";i:1945;s:4:\"file\";s:47:\"2019/06/2000px-Adidas_klassisches_logo.svg_.png\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x292.png\";s:5:\"width\";i:300;s:6:\"height\";i:292;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-768x747.png\";s:5:\"width\";i:768;s:6:\"height\";i:747;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:48:\"2000px-Adidas_klassisches_logo.svg_-1024x996.png\";s:5:\"width\";i:1024;s:6:\"height\";i:996;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-600x584.png\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-600x584.png\";s:5:\"width\";i:600;s:6:\"height\";i:584;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:47:\"2000px-Adidas_klassisches_logo.svg_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:45:\"2000px-Adidas_klassisches_logo.svg_-64x62.png\";s:5:\"width\";i:64;s:6:\"height\";i:62;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(345, 81, '_wp_attached_file', '2019/06/asics.png'),
(346, 81, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:184;s:6:\"height\";i:63;s:4:\"file\";s:17:\"2019/06/asics.png\";s:5:\"sizes\";a:4:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-150x63.png\";s:5:\"width\";i:150;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:16:\"asics-100x63.png\";s:5:\"width\";i:100;s:6:\"height\";i:63;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"asics-64x22.png\";s:5:\"width\";i:64;s:6:\"height\";i:22;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(347, 82, '_wp_attached_file', '2019/06/nike-logo-47A65A59D5-seeklogo.com_.png'),
(348, 82, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:300;s:6:\"height\";i:157;s:4:\"file\";s:46:\"2019/06/nike-logo-47A65A59D5-seeklogo.com_.png\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-300x157.png\";s:5:\"width\";i:300;s:6:\"height\";i:157;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:46:\"nike-logo-47A65A59D5-seeklogo.com_-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:44:\"nike-logo-47A65A59D5-seeklogo.com_-64x33.png\";s:5:\"width\";i:64;s:6:\"height\";i:33;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(349, 83, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561827040;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(350, 84, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561832391;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(352, 86, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561835991;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(355, 87, '_edit_last', '1'),
(356, 87, '_edit_lock', '1561904161:1'),
(357, 88, '_wp_attached_file', '2019/06/fff.jpg');
INSERT INTO `cp_postmeta` (`meta_id`, `post_id`, `meta_key`, `meta_value`) VALUES
(358, 88, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:622;s:6:\"height\";i:642;s:4:\"file\";s:15:\"2019/06/fff.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"fff-291x300.jpg\";s:5:\"width\";i:291;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"fff-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"fff-600x619.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:619;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"fff-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"fff-600x619.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:619;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"fff-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"fff-64x66.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:66;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822985\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(359, 87, '_thumbnail_id', '88'),
(360, 87, '_regular_price', '123'),
(361, 87, 'total_sales', '0'),
(362, 87, '_tax_status', 'taxable'),
(363, 87, '_tax_class', ''),
(364, 87, '_manage_stock', 'yes'),
(365, 87, '_backorders', 'no'),
(366, 87, '_sold_individually', 'no'),
(367, 87, '_virtual', 'no'),
(368, 87, '_downloadable', 'no'),
(369, 87, '_download_limit', '-1'),
(370, 87, '_download_expiry', '-1'),
(371, 87, '_stock', '888'),
(372, 87, '_stock_status', 'instock'),
(373, 87, '_wc_average_rating', '0'),
(374, 87, '_wc_review_count', '0'),
(375, 87, '_product_version', '3.6.4'),
(376, 87, '_price', '123'),
(377, 90, '_wp_attached_file', '2019/06/asd.jpg'),
(378, 90, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:635;s:6:\"height\";i:614;s:4:\"file\";s:15:\"2019/06/asd.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"asd-300x290.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:290;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"asd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:15:\"asd-600x580.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:580;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"asd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:15:\"asd-600x580.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:580;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"asd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"asd-64x62.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:62;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822941\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(379, 89, '_edit_last', '1'),
(380, 89, '_edit_lock', '1561904229:1'),
(381, 89, '_thumbnail_id', '90'),
(382, 89, '_regular_price', '50.00'),
(383, 89, 'total_sales', '0'),
(384, 89, '_tax_status', 'taxable'),
(385, 89, '_tax_class', ''),
(386, 89, '_manage_stock', 'yes'),
(387, 89, '_backorders', 'no'),
(388, 89, '_sold_individually', 'no'),
(389, 89, '_virtual', 'no'),
(390, 89, '_downloadable', 'no'),
(391, 89, '_download_limit', '-1'),
(392, 89, '_download_expiry', '-1'),
(393, 89, '_stock', '7777'),
(394, 89, '_stock_status', 'instock'),
(395, 89, '_wc_average_rating', '0'),
(396, 89, '_wc_review_count', '0'),
(397, 89, '_product_version', '3.6.4'),
(398, 89, '_price', '50.00'),
(399, 92, '_wp_attached_file', '2019/06/Capturar.jpg'),
(400, 92, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:617;s:6:\"height\";i:629;s:4:\"file\";s:20:\"2019/06/Capturar.jpg\";s:5:\"sizes\";a:9:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:20:\"Capturar-294x300.jpg\";s:5:\"width\";i:294;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:20:\"Capturar-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:20:\"Capturar-600x612.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:612;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:20:\"Capturar-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:20:\"Capturar-600x612.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:612;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:20:\"Capturar-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:18:\"Capturar-64x65.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:65;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561822914\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(401, 91, '_edit_last', '1'),
(402, 91, '_edit_lock', '1561904215:1'),
(403, 91, '_thumbnail_id', '92'),
(404, 91, '_regular_price', '34'),
(405, 91, 'total_sales', '0'),
(406, 91, '_tax_status', 'taxable'),
(407, 91, '_tax_class', ''),
(408, 91, '_manage_stock', 'yes'),
(409, 91, '_backorders', 'no'),
(410, 91, '_sold_individually', 'no'),
(411, 91, '_virtual', 'no'),
(412, 91, '_downloadable', 'no'),
(413, 91, '_download_limit', '-1'),
(414, 91, '_download_expiry', '-1'),
(415, 91, '_stock', '777'),
(416, 91, '_stock_status', 'instock'),
(417, 91, '_wc_average_rating', '0'),
(418, 91, '_wc_review_count', '0'),
(419, 91, '_product_version', '3.6.4'),
(420, 91, '_price', '34'),
(421, 93, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561839608;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(422, 94, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561843307;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(423, 95, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561847098;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(424, 96, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561850752;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(425, 97, '_edit_last', '1'),
(426, 97, '_edit_lock', '1561904203:1'),
(427, 98, '_wp_attached_file', '2019/06/asasd.jpg'),
(428, 98, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:308;s:6:\"height\";i:319;s:4:\"file\";s:17:\"2019/06/asasd.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:17:\"asasd-290x300.jpg\";s:5:\"width\";i:290;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:17:\"asasd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:17:\"asasd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:17:\"asasd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:15:\"asasd-64x66.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:66;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836616\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(429, 99, '_wp_attached_file', '2019/06/bnb.jpg'),
(430, 99, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:319;s:6:\"height\";i:322;s:4:\"file\";s:15:\"2019/06/bnb.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"bnb-297x300.jpg\";s:5:\"width\";i:297;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"bnb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"bnb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"bnb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"bnb-64x65.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:65;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836661\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(431, 100, '_wp_attached_file', '2019/06/dfd.jpg'),
(432, 100, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:318;s:4:\"file\";s:15:\"2019/06/dfd.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"dfd-300x298.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:298;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"dfd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"dfd-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"dfd-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"dfd-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836636\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(433, 101, '_wp_attached_file', '2019/06/kj.jpg'),
(434, 101, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:310;s:6:\"height\";i:325;s:4:\"file\";s:14:\"2019/06/kj.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"kj-286x300.jpg\";s:5:\"width\";i:286;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"kj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"kj-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"kj-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:12:\"kj-64x67.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:67;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836688\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(435, 102, '_wp_attached_file', '2019/06/mnm.jpg'),
(436, 102, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:327;s:6:\"height\";i:322;s:4:\"file\";s:15:\"2019/06/mnm.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:15:\"mnm-300x295.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:295;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:15:\"mnm-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:15:\"mnm-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:15:\"mnm-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:13:\"mnm-64x63.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:63;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836677\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(437, 103, '_wp_attached_file', '2019/06/vb.jpg'),
(438, 103, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:320;s:6:\"height\";i:320;s:4:\"file\";s:14:\"2019/06/vb.jpg\";s:5:\"sizes\";a:7:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:14:\"vb-300x300.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:14:\"vb-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:12:\"vb-64x64.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:64;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:15:\"Hudson Carolino\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:10:\"1561836649\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(439, 97, '_thumbnail_id', '103'),
(440, 97, '_regular_price', '300'),
(441, 97, 'total_sales', '0'),
(442, 97, '_tax_status', 'taxable'),
(443, 97, '_tax_class', ''),
(444, 97, '_manage_stock', 'yes'),
(445, 97, '_backorders', 'no'),
(446, 97, '_sold_individually', 'no'),
(447, 97, '_virtual', 'no'),
(448, 97, '_downloadable', 'no'),
(449, 97, '_download_limit', '-1'),
(450, 97, '_download_expiry', '-1'),
(451, 97, '_stock', '43534'),
(452, 97, '_stock_status', 'instock'),
(453, 97, '_wc_average_rating', '0'),
(454, 97, '_wc_review_count', '0'),
(455, 97, '_product_version', '3.6.4'),
(456, 97, '_price', '300'),
(457, 104, '_edit_last', '1'),
(458, 104, '_edit_lock', '1561904192:1'),
(459, 104, '_thumbnail_id', '102'),
(460, 104, 'total_sales', '0'),
(461, 104, '_tax_status', 'taxable'),
(462, 104, '_tax_class', ''),
(463, 104, '_manage_stock', 'yes'),
(464, 104, '_backorders', 'no'),
(465, 104, '_sold_individually', 'no'),
(466, 104, '_virtual', 'no'),
(467, 104, '_downloadable', 'no'),
(468, 104, '_download_limit', '-1'),
(469, 104, '_download_expiry', '-1'),
(470, 104, '_stock', '4564564'),
(471, 104, '_stock_status', 'instock'),
(472, 104, '_wc_average_rating', '0'),
(473, 104, '_wc_review_count', '0'),
(474, 104, '_product_version', '3.6.4'),
(475, 104, '_regular_price', '250'),
(476, 104, '_price', '250'),
(477, 105, '_edit_last', '1'),
(478, 105, '_edit_lock', '1561904181:1'),
(479, 105, '_thumbnail_id', '101'),
(480, 105, '_regular_price', '350'),
(481, 105, 'total_sales', '0'),
(482, 105, '_tax_status', 'taxable'),
(483, 105, '_tax_class', ''),
(484, 105, '_manage_stock', 'yes'),
(485, 105, '_backorders', 'no'),
(486, 105, '_sold_individually', 'no'),
(487, 105, '_virtual', 'no'),
(488, 105, '_downloadable', 'no'),
(489, 105, '_download_limit', '-1'),
(490, 105, '_download_expiry', '-1'),
(491, 105, '_stock', '56456456'),
(492, 105, '_stock_status', 'instock'),
(493, 105, '_wc_average_rating', '0'),
(494, 105, '_wc_review_count', '0'),
(495, 105, '_product_version', '3.6.4'),
(496, 105, '_price', '350'),
(497, 106, '_edit_last', '1'),
(498, 106, '_edit_lock', '1561904177:1'),
(499, 106, '_thumbnail_id', '101'),
(500, 106, '_regular_price', '450'),
(501, 106, 'total_sales', '0'),
(502, 106, '_tax_status', 'taxable'),
(503, 106, '_tax_class', ''),
(504, 106, '_manage_stock', 'yes'),
(505, 106, '_backorders', 'no'),
(506, 106, '_sold_individually', 'no'),
(507, 106, '_virtual', 'no'),
(508, 106, '_downloadable', 'no'),
(509, 106, '_download_limit', '-1'),
(510, 106, '_download_expiry', '-1'),
(511, 106, '_stock', '4545454'),
(512, 106, '_stock_status', 'instock'),
(513, 106, '_wc_average_rating', '0'),
(514, 106, '_wc_review_count', '0'),
(515, 106, '_product_version', '3.6.4'),
(516, 106, '_price', '450'),
(517, 107, '_edit_last', '1'),
(518, 107, '_edit_lock', '1561904178:1'),
(519, 107, '_thumbnail_id', '100'),
(520, 107, '_regular_price', '950'),
(521, 107, 'total_sales', '0'),
(522, 107, '_tax_status', 'taxable'),
(523, 107, '_tax_class', ''),
(524, 107, '_manage_stock', 'yes'),
(525, 107, '_backorders', 'no'),
(526, 107, '_sold_individually', 'no'),
(527, 107, '_virtual', 'no'),
(528, 107, '_downloadable', 'no'),
(529, 107, '_download_limit', '-1'),
(530, 107, '_download_expiry', '-1'),
(531, 107, '_stock', '3453453'),
(532, 107, '_stock_status', 'instock'),
(533, 107, '_wc_average_rating', '0'),
(534, 107, '_wc_review_count', '0'),
(535, 107, '_product_version', '3.6.4'),
(536, 107, '_price', '950'),
(537, 107, '_product_image_gallery', '100'),
(538, 108, '_edit_last', '1'),
(539, 108, '_edit_lock', '1561904183:1'),
(540, 108, '_thumbnail_id', '99'),
(541, 108, '_regular_price', '95'),
(542, 108, 'total_sales', '0'),
(543, 108, '_tax_status', 'taxable'),
(544, 108, '_tax_class', ''),
(545, 108, '_manage_stock', 'yes'),
(546, 108, '_backorders', 'no'),
(547, 108, '_sold_individually', 'no'),
(548, 108, '_virtual', 'no'),
(549, 108, '_downloadable', 'no'),
(550, 108, '_download_limit', '-1'),
(551, 108, '_download_expiry', '-1'),
(552, 108, '_stock', '3453453'),
(553, 108, '_stock_status', 'instock'),
(554, 108, '_wc_average_rating', '0'),
(555, 108, '_wc_review_count', '0'),
(556, 108, '_product_version', '3.6.4'),
(557, 108, '_price', '95'),
(558, 109, '_edit_last', '1'),
(559, 109, '_edit_lock', '1561920804:1'),
(560, 109, '_thumbnail_id', '98'),
(562, 109, 'total_sales', '0'),
(563, 109, '_tax_status', 'taxable'),
(564, 109, '_tax_class', ''),
(565, 109, '_manage_stock', 'yes'),
(566, 109, '_backorders', 'no'),
(567, 109, '_sold_individually', 'no'),
(568, 109, '_virtual', 'no'),
(569, 109, '_downloadable', 'no'),
(570, 109, '_download_limit', '-1'),
(571, 109, '_download_expiry', '-1'),
(572, 109, '_stock', '123'),
(573, 109, '_stock_status', 'instock'),
(574, 109, '_wc_average_rating', '0'),
(575, 109, '_wc_review_count', '0'),
(576, 109, '_product_version', '3.6.4'),
(603, 115, '_regular_price', '200'),
(578, 110, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561854354;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(579, 111, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561858686;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(580, 112, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561862290;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(581, 113, '_action_manager_schedule', 'O:32:\"ActionScheduler_IntervalSchedule\":2:{s:49:\"\0ActionScheduler_IntervalSchedule\0start_timestamp\";i:1561905026;s:53:\"\0ActionScheduler_IntervalSchedule\0interval_in_seconds\";i:3600;}'),
(582, 109, '_upsell_ids', 'a:4:{i:0;i:52;i:1;i:54;i:2;i:56;i:3;i:58;}'),
(583, 109, '_crosssell_ids', 'a:5:{i:0;i:65;i:1;i:52;i:2;i:56;i:3;i:105;i:4;i:87;}'),
(584, 109, '_product_attributes', 'a:2:{s:3:\"cor\";a:6:{s:4:\"name\";s:3:\"Cor\";s:5:\"value\";s:41:\"Amarelo | Vermelho | Verde | Rosa | Preto\";s:8:\"position\";s:1:\"0\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"0\";}s:7:\"tamanho\";a:6:{s:4:\"name\";s:7:\"Tamanho\";s:5:\"value\";s:14:\"P | M | G | GG\";s:8:\"position\";s:1:\"1\";s:10:\"is_visible\";s:1:\"1\";s:12:\"is_variation\";s:1:\"1\";s:11:\"is_taxonomy\";s:1:\"0\";}}'),
(602, 115, '_sku', '99999'),
(585, 115, '_variation_description', ''),
(586, 115, 'total_sales', '0'),
(587, 115, '_tax_status', 'taxable'),
(588, 115, '_tax_class', 'parent'),
(589, 115, '_manage_stock', 'no'),
(590, 115, '_backorders', 'no'),
(591, 115, '_sold_individually', 'no'),
(592, 115, '_virtual', 'no'),
(593, 115, '_downloadable', 'no'),
(594, 115, '_download_limit', '-1'),
(595, 115, '_download_expiry', '-1'),
(596, 115, '_stock', '0'),
(597, 115, '_stock_status', 'instock'),
(598, 115, '_wc_average_rating', '0'),
(599, 115, '_wc_review_count', '0'),
(600, 115, 'attribute_cor', 'Verde'),
(601, 115, '_product_version', '3.6.4'),
(604, 115, '_weight', '12'),
(605, 115, '_length', '12'),
(606, 115, '_width', '12'),
(607, 115, '_height', '12'),
(608, 115, '_crosssell_ids', 'a:3:{i:0;i:52;i:1;i:56;i:2;i:105;}'),
(609, 115, '_price', '200'),
(669, 109, '_price', '200'),
(611, 115, 'attribute_tamanho', 'M'),
(614, 116, '_variation_description', ''),
(615, 116, 'total_sales', '0'),
(616, 116, '_tax_status', 'taxable'),
(617, 116, '_tax_class', 'parent'),
(618, 116, '_manage_stock', 'yes'),
(619, 116, '_backorders', 'no'),
(620, 116, '_sold_individually', 'no'),
(621, 116, '_virtual', 'no'),
(622, 116, '_downloadable', 'no'),
(623, 116, '_download_limit', '-1'),
(624, 116, '_download_expiry', '-1'),
(625, 116, '_stock', '12'),
(626, 116, '_stock_status', 'instock'),
(627, 116, '_wc_average_rating', '0'),
(628, 116, '_wc_review_count', '0'),
(629, 116, 'attribute_cor', 'Amarelo'),
(630, 116, 'attribute_tamanho', 'P'),
(631, 116, '_product_version', '3.6.4'),
(633, 116, '_regular_price', '123'),
(634, 116, '_weight', '12'),
(635, 116, '_length', '12'),
(636, 116, '_width', '12'),
(637, 116, '_height', '12'),
(638, 116, '_crosssell_ids', 'a:3:{i:0;i:52;i:1;i:56;i:2;i:105;}'),
(639, 116, '_price', '123'),
(668, 109, '_price', '123'),
(642, 116, '_sku', '999'),
(645, 115, '_thumbnail_id', '98'),
(646, 116, '_thumbnail_id', '98'),
(649, 108, '_sku', '56555'),
(650, 107, '_sku', '55454'),
(651, 106, '_sku', '45444554'),
(652, 105, '_sku', '56565656'),
(653, 104, '_sku', '6767667'),
(654, 97, '_sku', '234242'),
(655, 91, '_sku', '667777'),
(656, 89, '_sku', '7777'),
(657, 87, '_sku', '88888'),
(659, 65, '_sku', '9999'),
(660, 58, '_sku', '2222'),
(661, 56, '_sku', '455454'),
(662, 54, '_sku', '000999'),
(663, 118, '_wp_attached_file', '2019/06/08-06-2015-banner-esportes-kanui.jpg'),
(664, 118, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1158;s:6:\"height\";i:150;s:4:\"file\";s:44:\"2019/06/08-06-2015-banner-esportes-kanui.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-300x39.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:39;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-768x99.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:45:\"08-06-2015-banner-esportes-kanui-1024x133.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:133;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:43:\"08-06-2015-banner-esportes-kanui-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:44:\"08-06-2015-banner-esportes-kanui-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:41:\"08-06-2015-banner-esportes-kanui-64x8.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:8;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(665, 119, '_wp_attached_file', '2019/06/cat_feminino_moletons.jpg'),
(666, 119, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1158;s:6:\"height\";i:150;s:4:\"file\";s:33:\"2019/06/cat_feminino_moletons.jpg\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-150x150.jpg\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:6:\"medium\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-300x39.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:39;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-768x99.jpg\";s:5:\"width\";i:768;s:6:\"height\";i:99;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:5:\"large\";a:4:{s:4:\"file\";s:34:\"cat_feminino_moletons-1024x133.jpg\";s:5:\"width\";i:1024;s:6:\"height\";i:133;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:33:\"cat_feminino_moletons-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-300x150.jpg\";s:5:\"width\";i:300;s:6:\"height\";i:150;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:32:\"cat_feminino_moletons-600x78.jpg\";s:5:\"width\";i:600;s:6:\"height\";i:78;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:33:\"cat_feminino_moletons-100x100.jpg\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:10:\"image/jpeg\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:30:\"cat_feminino_moletons-64x8.jpg\";s:5:\"width\";i:64;s:6:\"height\";i:8;s:9:\"mime-type\";s:10:\"image/jpeg\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(667, 109, '_product_image_gallery', '98,102,101,100,99'),
(670, 7, '_edit_lock', '1561916774:1'),
(671, 6, '_edit_lock', '1561919617:1'),
(672, 9, '_edit_last', '1'),
(673, 9, '_wp_page_template', 'default'),
(674, 9, '_edit_lock', '1561923084:1'),
(675, 8, '_edit_last', '1'),
(676, 8, '_wp_page_template', 'default'),
(677, 8, '_edit_lock', '1561923109:1'),
(678, 124, '_edit_lock', '1562165370:1'),
(679, 124, '_wp_page_template', 'paginas/contato.php'),
(680, 126, '_form', '<label for=\"seu-nome\">Seu nome(obrigatório)</label>[text* seu-nome id:seu-nome class:seu-nome placeholder \"Digite seu nome\"]<label for=\"seu-email\">Seu e-mail(obrigatório)</label>[email* seu-email id:seu-email class:seu-email placeholder \"Digite seu email\"]<label for=\"sua-cidade\">Sua cidade</label>[text* sua-cidade id:sua-cidade class:sua-cidade placeholder \"Digite sua cidade\"]<label for=\"seu-telefone\">Seu telefone</label>[tel* seu-telefone id:seu-telefone class:seu-telefone placeholder \"Digite seu telefone\"]<label for=\"sua-mensagem\">Sua mensagem</label>[textarea* sua-mensagem id:sua-mensagem class:sua-mensagem placeholder \"Fala com a gente :)\"]\n[submit id:btn-enviar class:btn-enviar \"Enviar\"]'),
(681, 126, '_mail', 'a:9:{s:6:\"active\";b:1;s:7:\"subject\";s:14:\"Century Sports\";s:6:\"sender\";s:48:\"Century Sports <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:4:\"body\";s:206:\"De: [seu-nome] <[seu-email]>\nTelefone: [seu-telefone]\n\nCidade: [sua-cidade]\n\nMessagem:\n[sua-mensagem]\n\n-- \nThis e-mail was sent from a contact form on Century Sports (http://centurysports.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:21:\"Reply-To: [seu-email]\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(682, 126, '_mail_2', 'a:9:{s:6:\"active\";b:0;s:7:\"subject\";s:31:\"Century Sports \"[your-subject]\"\";s:6:\"sender\";s:48:\"Century Sports <devhcdesenvolvimentos@gmail.com>\";s:9:\"recipient\";s:12:\"[your-email]\";s:4:\"body\";s:133:\"Message Body:\n[your-message]\n\n-- \nThis e-mail was sent from a contact form on Century Sports (http://centurysports.hcdesenvolvimentos.com)\";s:18:\"additional_headers\";s:41:\"Reply-To: devhcdesenvolvimentos@gmail.com\";s:11:\"attachments\";s:0:\"\";s:8:\"use_html\";b:0;s:13:\"exclude_blank\";b:0;}'),
(683, 126, '_messages', 'a:23:{s:12:\"mail_sent_ok\";s:45:\"Thank you for your message. It has been sent.\";s:12:\"mail_sent_ng\";s:71:\"There was an error trying to send your message. Please try again later.\";s:16:\"validation_error\";s:61:\"One or more fields have an error. Please check and try again.\";s:4:\"spam\";s:71:\"There was an error trying to send your message. Please try again later.\";s:12:\"accept_terms\";s:69:\"You must accept the terms and conditions before sending your message.\";s:16:\"invalid_required\";s:22:\"The field is required.\";s:16:\"invalid_too_long\";s:22:\"The field is too long.\";s:17:\"invalid_too_short\";s:23:\"The field is too short.\";s:12:\"invalid_date\";s:29:\"The date format is incorrect.\";s:14:\"date_too_early\";s:44:\"The date is before the earliest one allowed.\";s:13:\"date_too_late\";s:41:\"The date is after the latest one allowed.\";s:13:\"upload_failed\";s:46:\"There was an unknown error uploading the file.\";s:24:\"upload_file_type_invalid\";s:49:\"You are not allowed to upload files of this type.\";s:21:\"upload_file_too_large\";s:20:\"The file is too big.\";s:23:\"upload_failed_php_error\";s:38:\"There was an error uploading the file.\";s:14:\"invalid_number\";s:29:\"The number format is invalid.\";s:16:\"number_too_small\";s:47:\"The number is smaller than the minimum allowed.\";s:16:\"number_too_large\";s:46:\"The number is larger than the maximum allowed.\";s:23:\"quiz_answer_not_correct\";s:36:\"The answer to the quiz is incorrect.\";s:17:\"captcha_not_match\";s:31:\"Your entered code is incorrect.\";s:13:\"invalid_email\";s:38:\"The e-mail address entered is invalid.\";s:11:\"invalid_url\";s:19:\"The URL is invalid.\";s:11:\"invalid_tel\";s:32:\"The telephone number is invalid.\";}'),
(684, 126, '_additional_settings', ''),
(685, 126, '_locale', 'pt_BR'),
(687, 127, '_edit_lock', '1562167368:1'),
(688, 128, '_edit_lock', '1562171790:1'),
(689, 128, '_wp_page_template', 'paginas/quem-somos.php'),
(690, 130, '_wp_attached_file', '2019/07/banner-quemsomos.png'),
(691, 130, '_wp_attachment_metadata', 'a:5:{s:5:\"width\";i:1230;s:6:\"height\";i:896;s:4:\"file\";s:28:\"2019/07/banner-quemsomos.png\";s:5:\"sizes\";a:11:{s:9:\"thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-150x150.png\";s:5:\"width\";i:150;s:6:\"height\";i:150;s:9:\"mime-type\";s:9:\"image/png\";}s:6:\"medium\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-300x219.png\";s:5:\"width\";i:300;s:6:\"height\";i:219;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"medium_large\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-768x559.png\";s:5:\"width\";i:768;s:6:\"height\";i:559;s:9:\"mime-type\";s:9:\"image/png\";}s:5:\"large\";a:4:{s:4:\"file\";s:29:\"banner-quemsomos-1024x746.png\";s:5:\"width\";i:1024;s:6:\"height\";i:746;s:9:\"mime-type\";s:9:\"image/png\";}s:21:\"woocommerce_thumbnail\";a:5:{s:4:\"file\";s:28:\"banner-quemsomos-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";s:9:\"uncropped\";b:0;}s:18:\"woocommerce_single\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-600x437.png\";s:5:\"width\";i:600;s:6:\"height\";i:437;s:9:\"mime-type\";s:9:\"image/png\";}s:29:\"woocommerce_gallery_thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:12:\"shop_catalog\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-300x300.png\";s:5:\"width\";i:300;s:6:\"height\";i:300;s:9:\"mime-type\";s:9:\"image/png\";}s:11:\"shop_single\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-600x437.png\";s:5:\"width\";i:600;s:6:\"height\";i:437;s:9:\"mime-type\";s:9:\"image/png\";}s:14:\"shop_thumbnail\";a:4:{s:4:\"file\";s:28:\"banner-quemsomos-100x100.png\";s:5:\"width\";i:100;s:6:\"height\";i:100;s:9:\"mime-type\";s:9:\"image/png\";}s:28:\"dgwt-wcas-product-suggestion\";a:4:{s:4:\"file\";s:26:\"banner-quemsomos-64x47.png\";s:5:\"width\";i:64;s:6:\"height\";i:47;s:9:\"mime-type\";s:9:\"image/png\";}}s:10:\"image_meta\";a:12:{s:8:\"aperture\";s:1:\"0\";s:6:\"credit\";s:0:\"\";s:6:\"camera\";s:0:\"\";s:7:\"caption\";s:0:\"\";s:17:\"created_timestamp\";s:1:\"0\";s:9:\"copyright\";s:0:\"\";s:12:\"focal_length\";s:1:\"0\";s:3:\"iso\";s:1:\"0\";s:13:\"shutter_speed\";s:1:\"0\";s:5:\"title\";s:0:\"\";s:11:\"orientation\";s:1:\"0\";s:8:\"keywords\";a:0:{}}}'),
(692, 3, '_edit_lock', '1562173076:1'),
(693, 131, '_edit_lock', '1562267705:1'),
(694, 131, '_wp_page_template', 'paginas/institucional.php'),
(695, 134, '_edit_lock', '1562249390:1'),
(696, 134, '_wp_page_template', 'paginas/institucional.php'),
(697, 141, '_edit_lock', '1562268611:1'),
(698, 141, '_wp_page_template', 'paginas/abas-passo-a-passo.php'),
(699, 143, '_edit_lock', '1562259116:1'),
(700, 143, '_edit_last', '1'),
(701, 144, '_edit_lock', '1562259216:1'),
(702, 144, '_edit_last', '1'),
(703, 145, '_edit_lock', '1562265926:1'),
(704, 145, '_edit_last', '1'),
(705, 147, '_edit_lock', '1562259331:1'),
(706, 147, '_edit_last', '1'),
(707, 148, '_edit_lock', '1562259618:1'),
(708, 149, '_edit_lock', '1562270342:1'),
(709, 149, '_wp_page_template', 'paginas/abas-passo-a-passo.php'),
(710, 151, '_edit_lock', '1562260631:1'),
(711, 151, '_edit_last', '1'),
(712, 152, '_edit_lock', '1562263172:1'),
(713, 152, '_edit_last', '1'),
(714, 154, '_edit_lock', '1562263320:1'),
(715, 154, '_edit_last', '1'),
(716, 156, '_edit_lock', '1562263353:1'),
(717, 156, '_edit_last', '1'),
(718, 157, '_edit_lock', '1562263527:1'),
(719, 157, '_edit_last', '1'),
(720, 158, '_edit_lock', '1562264412:1'),
(721, 158, '_edit_last', '1'),
(722, 159, '_edit_lock', '1562266442:1'),
(723, 159, '_edit_last', '1'),
(724, 160, '_edit_lock', '1562266329:1'),
(725, 160, '_edit_last', '1'),
(726, 161, '_edit_lock', '1562266363:1'),
(727, 161, '_edit_last', '1'),
(728, 162, '_edit_lock', '1562266385:1'),
(729, 162, '_edit_last', '1'),
(730, 163, '_edit_lock', '1562266965:1'),
(731, 163, '_edit_last', '1'),
(732, 164, '_edit_lock', '1562266991:1'),
(733, 164, '_edit_last', '1'),
(734, 165, '_edit_lock', '1562267336:1'),
(735, 165, '_edit_last', '1'),
(736, 166, '_edit_lock', '1562267433:1'),
(737, 167, '_edit_lock', '1562268600:1'),
(738, 167, '_wp_page_template', 'paginas/institucional.php');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_posts`
--

DROP TABLE IF EXISTS `cp_posts`;
CREATE TABLE IF NOT EXISTS `cp_posts` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_title` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_excerpt` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `to_ping` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `pinged` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `post_parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `post_name` (`post_name`(191)),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`)
) ENGINE=MyISAM AUTO_INCREMENT=170 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_posts`
--

INSERT INTO `cp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(1, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:paragraph -->\n<p>Boas-vindas ao WordPress. Esse é o seu primeiro post. Edite-o ou exclua-o, e então comece a escrever!</p>\n<!-- /wp:paragraph -->', 'Olá, mundo!', '', 'publish', 'open', 'open', '', 'ola-mundo', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=1', 0, 'post', '', 1),
(2, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:paragraph -->\n<p>Esta é uma página de exemplo. É diferente de um post no blog porque ela permanecerá em um lugar e aparecerá na navegação do seu site na maioria dos temas. Muitas pessoas começam com uma página que as apresenta a possíveis visitantes do site. Ela pode dizer algo assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>Olá! Eu sou um mensageiro de bicicleta durante o dia, ator aspirante à noite, e este é o meu site. Eu moro em São Paulo, tenho um grande cachorro chamado Rex e gosto de tomar caipirinha (e banhos de chuva).</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>...ou alguma coisa assim:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:quote -->\n<blockquote class=\"wp-block-quote\"><p>A Companhia de Miniaturas XYZ foi fundada em 1971, e desde então tem fornecido miniaturas de qualidade ao público. Localizada na cidade de Itu, a XYZ emprega mais de 2.000 pessoas e faz coisas grandiosas para a comunidade da cidade.</p></blockquote>\n<!-- /wp:quote -->\n\n<!-- wp:paragraph -->\n<p>Como um novo usuário do WordPress, você deveria ir ao <a href=\"http://centurysports.hcdesenvolvimentos.com/wp-admin/\">painel</a> para excluir essa página e criar novas páginas para o seu conteúdo. Divirta-se!</p>\n<!-- /wp:paragraph -->', 'Página de exemplo', '', 'publish', 'closed', 'open', '', 'pagina-exemplo', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=2', 0, 'page', '', 0),
(3, 1, '2019-05-30 17:44:06', '2019-05-30 20:44:06', '<!-- wp:heading --><h2>Quem somos</h2><!-- /wp:heading --><!-- wp:paragraph --><p>O endereço do nosso site é: http://centurysports.hcdesenvolvimentos.com.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais dados pessoais coletamos e porque</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Comentários</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Quando os visitantes deixam comentários no site, coletamos os dados mostrados no formulário de comentários, além do endereço de IP e de dados do navegador do visitante, para auxiliar na detecção de spam.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Uma sequência anonimizada de caracteres criada a partir do seu e-mail (também chamada de hash) poderá ser enviada para o Gravatar para verificar se você usa o serviço. A política de privacidade do Gravatar está disponível aqui: https://automattic.com/privacy/. Depois da aprovação do seu comentário, a foto do seu perfil fica visível publicamente junto de seu comentário.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Se você envia imagens para o site, evite enviar as que contenham dados de localização incorporados (EXIF GPS). Visitantes podem baixar estas imagens do site e extrair delas seus dados de localização.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Formulários de contato</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Cookies</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Ao deixar um comentário no site, você poderá optar por salvar seu nome, e-mail e site nos cookies. Isso visa seu conforto, assim você não precisará preencher seus  dados novamente quando fizer outro comentário. Estes cookies duram um ano.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você tem uma conta e acessa este site, um cookie temporário será criado para determinar se seu navegador aceita cookies. Ele não contém nenhum dado pessoal e será descartado quando você fechar seu navegador.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Quando você acessa sua conta no site, também criamos vários cookies para salvar os dados da sua conta e suas escolhas de exibição de tela. Cookies de login são mantidos por dois dias e cookies de opções de tela por um ano. Se você selecionar &quot;Lembrar-me&quot;, seu acesso será mantido por duas semanas. Se você se desconectar da sua conta, os cookies de login serão removidos.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Se você editar ou publicar um artigo, um cookie adicional será salvo no seu navegador. Este cookie não inclui nenhum dado pessoal e simplesmente indica o ID do post referente ao artigo que você acabou de editar. Ele expira depois de 1 dia.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Mídia incorporada de outros sites</h3><!-- /wp:heading --><!-- wp:paragraph --><p>Artigos neste site podem incluir conteúdo incorporado como, por exemplo, vídeos, imagens, artigos, etc. Conteúdos incorporados de outros sites se comportam exatamente da mesma forma como se o visitante estivesse visitando o outro site.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Estes sites podem coletar dados sobre você, usar cookies, incorporar rastreamento adicional de terceiros e monitorar sua interação com este conteúdo incorporado, incluindo sua interação com o conteúdo incorporado se você tem uma conta e está conectado com o site.</p><!-- /wp:paragraph --><!-- wp:heading {\"level\":3} --><h3>Análises</h3><!-- /wp:heading --><!-- wp:heading --><h2>Com quem partilhamos seus dados</h2><!-- /wp:heading --><!-- wp:heading --><h2>Por quanto tempo mantemos os seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você deixar um comentário, o comentário e os seus metadados são conservados indefinidamente. Fazemos isso para que seja possível reconhecer e aprovar automaticamente qualquer comentário posterior ao invés de retê-lo para moderação.</p><!-- /wp:paragraph --><!-- wp:paragraph --><p>Para usuários que se registram no nosso site (se houver), também guardamos as informações pessoais que fornecem no seu perfil de usuário. Todos os usuários podem ver, editar ou excluir suas informações pessoais a qualquer momento (só não é possível alterar o seu username). Os administradores de sites também podem ver e editar estas informações.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Quais os seus direitos sobre seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Se você tiver uma conta neste site ou se tiver deixado comentários, pode solicitar um arquivo exportado dos dados pessoais que mantemos sobre você, inclusive quaisquer dados que nos tenha fornecido. Também pode solicitar que removamos qualquer dado pessoal que mantemos sobre você. Isto não inclui nenhuns dados que somos obrigados a manter para propósitos administrativos, legais ou de segurança.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Para onde enviamos seus dados</h2><!-- /wp:heading --><!-- wp:paragraph --><p>Comentários de visitantes podem ser marcados por um serviço automático de detecção de spam.</p><!-- /wp:paragraph --><!-- wp:heading --><h2>Suas informações de contato</h2><!-- /wp:heading --><!-- wp:heading --><h2>Informações adicionais</h2><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Como protegemos seus dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais são nossos procedimentos contra violação de dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>De quais terceiros nós recebemos dados</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Quais tomadas de decisão ou análises de perfil automatizadas fazemos com os dados de usuários</h3><!-- /wp:heading --><!-- wp:heading {\"level\":3} --><h3>Requisitos obrigatórios de divulgação para sua categoria profissional</h3><!-- /wp:heading -->', 'Política de privacidade', '', 'draft', 'closed', 'open', '', 'politica-de-privacidade', '', '', '2019-05-30 17:44:06', '2019-05-30 20:44:06', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=3', 0, 'page', '', 0),
(124, 1, '2019-07-03 11:48:16', '2019-07-03 14:48:16', '', 'Contato', '', 'publish', 'closed', 'closed', '', 'contato', '', '', '2019-07-03 11:48:16', '2019-07-03 14:48:16', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=124', 0, 'page', '', 0),
(5, 1, '2019-05-30 17:54:46', '2019-05-30 20:54:46', '', 'woocommerce-placeholder', '', 'inherit', 'open', 'closed', '', 'woocommerce-placeholder', '', '', '2019-05-30 17:54:46', '2019-05-30 20:54:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/05/woocommerce-placeholder.png', 0, 'attachment', 'image/png', 0),
(6, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '', 'Loja', '', 'publish', 'closed', 'closed', '', 'loja', '', '', '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 0, 'http://centurysports.hcdesenvolvimentos.com/shop/', 0, 'page', '', 0),
(7, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->\n[woocommerce_cart]\n<!-- /wp:shortcode -->', 'Carrinho', '', 'publish', 'closed', 'closed', '', 'carrinho', '', '', '2019-06-30 14:48:36', '2019-06-30 17:48:36', '', 0, 'http://centurysports.hcdesenvolvimentos.com/cart/', 0, 'page', '', 0),
(8, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Finalizar Compra', '', 'publish', 'closed', 'closed', '', 'finalizar-compra', '', '', '2019-06-30 16:31:49', '2019-06-30 19:31:49', '', 0, 'http://centurysports.hcdesenvolvimentos.com/checkout/', 0, 'page', '', 0),
(9, 1, '2019-05-30 17:56:34', '2019-05-30 20:56:34', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Minha Conta', '', 'publish', 'closed', 'closed', '', 'minha-conta', '', '', '2019-06-30 16:31:24', '2019-06-30 19:31:24', '', 0, 'http://centurysports.hcdesenvolvimentos.com/my-account/', 0, 'page', '', 0),
(12, 1, '2019-05-30 18:15:48', '2019-05-30 21:15:48', 'Descrição do produto', 'Produto Nome', 'Descrição do produto 2', 'publish', 'open', 'closed', '', 'produto-nome', '', '', '2019-06-29 15:00:49', '2019-06-29 18:00:49', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=12', 0, 'product', '', 0),
(13, 1, '2019-05-30 18:15:17', '2019-05-30 21:15:17', '', 'q1', '', 'inherit', 'open', 'closed', '', 'q1', '', '', '2019-05-30 18:15:17', '2019-05-30 21:15:17', '', 12, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/05/q1.png', 0, 'attachment', 'image/png', 0),
(14, 0, '2019-05-30 20:03:32', '2019-05-30 23:03:32', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5cf800952af667.39044636-d39bT7RytYQyOVEfLZq0dIdBhpLS7kBT', '', '', '2019-06-05 14:49:09', '2019-06-05 17:49:09', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=14', 0, 'scheduled-action', '', 3),
(15, 0, '2019-06-05 15:49:09', '2019-06-05 18:49:09', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5cf80f0f242dc3.50022788-Uujf3BJDbjNznKR2gybnoQPeUBnPikaw', '', '', '2019-06-05 15:50:55', '2019-06-05 18:50:55', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=15', 0, 'scheduled-action', '', 3),
(16, 0, '2019-06-05 16:50:55', '2019-06-05 19:50:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d10098cb565f6.31905340-4wGIFywriQ0GV2wKKCiZfbItFT6e63jp', '', '', '2019-06-23 20:21:48', '2019-06-23 23:21:48', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=16', 0, 'scheduled-action', '', 3),
(17, 0, '2019-06-23 21:21:48', '2019-06-24 00:21:48', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1017c7383a77.41893791-2D7x9DjCjWwkWkP9UVZAGpZ8E6Zu71vk', '', '', '2019-06-23 21:22:31', '2019-06-24 00:22:31', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=17', 0, 'scheduled-action', '', 3),
(126, 1, '2019-07-03 11:50:11', '2019-07-03 14:50:11', '<label for=\"seu-nome\">Seu nome(obrigatório)</label>[text* seu-nome id:seu-nome class:seu-nome placeholder \"Digite seu nome\"]<label for=\"seu-email\">Seu e-mail(obrigatório)</label>[email* seu-email id:seu-email class:seu-email placeholder \"Digite seu email\"]<label for=\"sua-cidade\">Sua cidade</label>[text* sua-cidade id:sua-cidade class:sua-cidade placeholder \"Digite sua cidade\"]<label for=\"seu-telefone\">Seu telefone</label>[tel* seu-telefone id:seu-telefone class:seu-telefone placeholder \"Digite seu telefone\"]<label for=\"sua-mensagem\">Sua mensagem</label>[textarea* sua-mensagem id:sua-mensagem class:sua-mensagem placeholder \"Fala com a gente :)\"]\r\n[submit id:btn-enviar class:btn-enviar \"Enviar\"]\n1\nCentury Sports\nCentury Sports <devhcdesenvolvimentos@gmail.com>\ndevhcdesenvolvimentos@gmail.com\nDe: [seu-nome] <[seu-email]>\r\nTelefone: [seu-telefone]\r\n\r\nCidade: [sua-cidade]\r\n\r\nMessagem:\r\n[sua-mensagem]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Century Sports (http://centurysports.hcdesenvolvimentos.com)\nReply-To: [seu-email]\n\n\n\n\nCentury Sports \"[your-subject]\"\nCentury Sports <devhcdesenvolvimentos@gmail.com>\n[your-email]\nMessage Body:\r\n[your-message]\r\n\r\n-- \r\nThis e-mail was sent from a contact form on Century Sports (http://centurysports.hcdesenvolvimentos.com)\nReply-To: devhcdesenvolvimentos@gmail.com\n\n\n\nThank you for your message. It has been sent.\nThere was an error trying to send your message. Please try again later.\nOne or more fields have an error. Please check and try again.\nThere was an error trying to send your message. Please try again later.\nYou must accept the terms and conditions before sending your message.\nThe field is required.\nThe field is too long.\nThe field is too short.\nThe date format is incorrect.\nThe date is before the earliest one allowed.\nThe date is after the latest one allowed.\nThere was an unknown error uploading the file.\nYou are not allowed to upload files of this type.\nThe file is too big.\nThere was an error uploading the file.\nThe number format is invalid.\nThe number is smaller than the minimum allowed.\nThe number is larger than the maximum allowed.\nThe answer to the quiz is incorrect.\nYour entered code is incorrect.\nThe e-mail address entered is invalid.\nThe URL is invalid.\nThe telephone number is invalid.', 'Fomulário de contato', '', 'publish', 'closed', 'closed', '', 'contact-form-1', '', '', '2019-07-03 12:08:21', '2019-07-03 15:08:21', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=wpcf7_contact_form&#038;p=126', 0, 'wpcf7_contact_form', '', 0),
(125, 1, '2019-07-03 11:48:16', '2019-07-03 14:48:16', '', 'Contato', '', 'inherit', 'closed', 'closed', '', '124-revision-v1', '', '', '2019-07-03 11:48:16', '2019-07-03 14:48:16', '', 124, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/124-revision-v1/', 0, 'revision', '', 0),
(22, 1, '2019-06-23 21:14:01', '2019-06-24 00:14:01', '', 'Destaque 1', '', 'publish', 'closed', 'closed', '', 'destaque-1', '', '', '2019-06-23 21:14:01', '2019-06-24 00:14:01', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=destaque&#038;p=22', 0, 'destaque', '', 0),
(23, 1, '2019-06-23 21:13:48', '2019-06-24 00:13:48', '', 'destaque', '', 'inherit', 'open', 'closed', '', 'destaque', '', '', '2019-06-23 21:13:48', '2019-06-24 00:13:48', '', 22, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/destaque.png', 0, 'attachment', 'image/png', 0),
(24, 0, '2019-06-23 22:22:31', '2019-06-24 01:22:31', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1025e9abca30.54858592-0aajfTZnStrRjAPe5d1MdpNiF3jeaZ6L', '', '', '2019-06-23 22:22:49', '2019-06-24 01:22:49', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=24', 0, 'scheduled-action', '', 3),
(25, 1, '2019-06-23 21:39:36', '2019-06-24 00:39:36', '', 'logo', '', 'inherit', 'open', 'closed', '', 'logo', '', '', '2019-06-23 21:39:36', '2019-06-24 00:39:36', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/logo-1.png', 0, 'attachment', 'image/png', 0),
(26, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '26', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=26', 1, 'nav_menu_item', '', 0),
(27, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '27', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=27', 13, 'nav_menu_item', '', 0),
(28, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '28', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=28', 14, 'nav_menu_item', '', 0),
(29, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '29', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=29', 15, 'nav_menu_item', '', 0),
(30, 1, '2019-06-23 22:19:48', '2019-06-24 01:19:48', ' ', '', '', 'publish', 'closed', 'closed', '', '30', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=30', 12, 'nav_menu_item', '', 0),
(31, 0, '2019-06-23 23:22:49', '2019-06-24 02:22:49', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d10341f0eae19.71734031-gWyvE7sxYXMU9kQUCTgOGz1Ek4HbvzlW', '', '', '2019-06-23 23:23:27', '2019-06-24 02:23:27', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=31', 0, 'scheduled-action', '', 3),
(32, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Pedidos', '', 'publish', 'closed', 'closed', '', 'pedidos', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=32', 2, 'nav_menu_item', '', 0),
(33, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Downloads', '', 'publish', 'closed', 'closed', '', 'downloads', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=33', 3, 'nav_menu_item', '', 0),
(34, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Endereços', '', 'publish', 'closed', 'closed', '', 'enderecos', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=34', 4, 'nav_menu_item', '', 0),
(35, 1, '2019-06-23 22:27:50', '2019-06-24 01:27:50', '', 'Detalhes da conta', '', 'publish', 'closed', 'closed', '', 'detalhes-da-conta', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=35', 5, 'nav_menu_item', '', 0),
(36, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Pedidos', '', 'publish', 'closed', 'closed', '', 'pedidos-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=36', 6, 'nav_menu_item', '', 0),
(37, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Downloads', '', 'publish', 'closed', 'closed', '', 'downloads-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=37', 7, 'nav_menu_item', '', 0),
(38, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Endereços', '', 'publish', 'closed', 'closed', '', 'enderecos-2', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=38', 8, 'nav_menu_item', '', 0),
(39, 1, '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 'Detalhes da conta', '', 'publish', 'closed', 'closed', '', 'detalhes-da-conta-2', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=39', 11, 'nav_menu_item', '', 0),
(40, 1, '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 'Sair', '', 'publish', 'closed', 'closed', '', 'sair', '', '', '2019-06-23 22:32:07', '2019-06-24 01:32:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=40', 9, 'nav_menu_item', '', 0),
(41, 1, '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 'Senha perdida', '', 'publish', 'closed', 'closed', '', 'senha-perdida', '', '', '2019-06-23 22:32:08', '2019-06-24 01:32:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=41', 10, 'nav_menu_item', '', 0),
(42, 1, '2019-06-23 23:05:38', '2019-06-24 02:05:38', '', 'selo', '', 'inherit', 'open', 'closed', '', 'selo', '', '', '2019-06-23 23:05:38', '2019-06-24 02:05:38', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/selo.png', 0, 'attachment', 'image/png', 0),
(43, 1, '2019-06-23 23:18:56', '2019-06-24 02:18:56', '', 'Inicial', '', 'publish', 'closed', 'closed', '', 'inicial', '', '', '2019-06-23 23:21:16', '2019-06-24 02:21:16', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=43', 0, 'page', '', 0),
(45, 1, '2019-06-23 23:19:53', '2019-06-24 02:19:53', '', 'Inicial', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-06-23 23:19:53', '2019-06-24 02:19:53', '', 43, 'http://centurysports.hcdesenvolvimentos.com/2019/06/23/43-revision-v1/', 0, 'revision', '', 0),
(44, 1, '2019-06-23 23:18:56', '2019-06-24 02:18:56', '<!-- wp:paragraph -->\n<p>Inicial</p>\n<!-- /wp:paragraph -->', '', '', 'inherit', 'closed', 'closed', '', '43-revision-v1', '', '', '2019-06-23 23:18:56', '2019-06-24 02:18:56', '', 43, 'http://centurysports.hcdesenvolvimentos.com/2019/06/23/43-revision-v1/', 0, 'revision', '', 0),
(46, 0, '2019-06-24 00:23:27', '2019-06-24 03:23:27', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1042703e7d20.49506471-JSCP5uja4Q5u7tskO3yvEVgbXM3udAtK', '', '', '2019-06-24 00:24:32', '2019-06-24 03:24:32', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=46', 0, 'scheduled-action', '', 3),
(47, 1, '2019-06-23 23:31:55', '2019-06-24 02:31:55', '', 'categoria1', '', 'inherit', 'open', 'closed', '', 'categoria1', '', '', '2019-06-23 23:31:55', '2019-06-24 02:31:55', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria1-1.png', 0, 'attachment', 'image/png', 0),
(48, 1, '2019-06-24 00:01:25', '2019-06-24 03:01:25', '', 'categoria', '', 'inherit', 'open', 'closed', '', 'categoria', '', '', '2019-06-24 00:01:25', '2019-06-24 03:01:25', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/categoria.png', 0, 'attachment', 'image/png', 0),
(49, 0, '2019-06-24 01:24:32', '2019-06-24 04:24:32', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d154813c9bee2.66866334-DTnUOoO3oD0yHMEnHdko4Jn6uuyJGsXh', '', '', '2019-06-27 19:49:55', '2019-06-27 22:49:55', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=49', 0, 'scheduled-action', '', 3),
(50, 1, '2019-06-24 00:43:49', '2019-06-24 03:43:49', '', 'tenis', '', 'inherit', 'open', 'closed', '', 'tenis', '', '', '2019-06-24 00:43:49', '2019-06-24 03:43:49', '', 12, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/05/tenis-1.png', 0, 'attachment', 'image/png', 0),
(85, 1, '2019-06-29 15:00:04', '2019-06-29 18:00:04', '', 'Camiseta', '', 'inherit', 'closed', 'closed', '', '65-autosave-v1', '', '', '2019-06-29 15:00:04', '2019-06-29 18:00:04', '', 65, 'http://centurysports.hcdesenvolvimentos.com/2019/06/29/65-autosave-v1/', 0, 'revision', '', 0),
(86, 0, '2019-06-29 16:19:51', '2019-06-29 19:19:51', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17b9e8982104.63293545-hupbwKlmyKKeqRN34WS0hmnEaOXKlRR8', '', '', '2019-06-29 16:20:08', '2019-06-29 19:20:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=86', 0, 'scheduled-action', '', 3),
(52, 1, '2019-06-24 00:45:09', '2019-06-24 03:45:09', '', 'Produto 2', '', 'publish', 'open', 'closed', '', 'produto-2', '', '', '2019-06-29 15:00:46', '2019-06-29 18:00:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=52', 0, 'product', '', 0),
(53, 1, '2019-06-24 00:45:02', '2019-06-24 03:45:02', '', 'D24-1738-304_zoom1', '', 'inherit', 'open', 'closed', '', 'd24-1738-304_zoom1', '', '', '2019-06-24 00:45:02', '2019-06-24 03:45:02', '', 52, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/D24-1738-304_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(54, 1, '2019-06-24 00:46:24', '2019-06-24 03:46:24', '', 'Produto 3', '', 'publish', 'open', 'closed', '', 'produto-3', '', '', '2019-06-30 11:18:00', '2019-06-30 14:18:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=54', 0, 'product', '', 0),
(55, 1, '2019-06-24 00:46:20', '2019-06-24 03:46:20', '', '497-9505-026_zoom1', '', 'inherit', 'open', 'closed', '', '497-9505-026_zoom1', '', '', '2019-06-24 00:46:20', '2019-06-24 03:46:20', '', 54, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/497-9505-026_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(56, 1, '2019-06-24 00:48:36', '2019-06-24 03:48:36', '', 'Produto 4', '', 'publish', 'open', 'closed', '', 'produto-4', '', '', '2019-06-30 11:17:46', '2019-06-30 14:17:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=56', 0, 'product', '', 0),
(57, 1, '2019-06-24 00:48:31', '2019-06-24 03:48:31', '', 'B78-2495-172_zoom1', '', 'inherit', 'open', 'closed', '', 'b78-2495-172_zoom1', '', '', '2019-06-24 00:48:31', '2019-06-24 03:48:31', '', 56, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/B78-2495-172_zoom1.jpg', 0, 'attachment', 'image/jpeg', 0),
(58, 1, '2019-06-24 00:49:24', '2019-06-24 03:49:24', '', 'Produto 5', '', 'publish', 'open', 'closed', '', 'produto-5', '', '', '2019-06-30 11:17:37', '2019-06-30 14:17:37', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=58', 0, 'product', '', 0),
(59, 0, '2019-06-27 20:49:55', '2019-06-27 23:49:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d15562394d1b6.49028689-AGAWTQMsJqALIuweTTpmmmIBdKq0mZqU', '', '', '2019-06-27 20:49:55', '2019-06-27 23:49:55', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=59', 0, 'scheduled-action', '', 3),
(60, 0, '2019-06-27 21:49:55', '2019-06-28 00:49:55', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d156445847d05.21805590-mBWIvRM2vIGw651aR9tyaVgSRS6VhADf', '', '', '2019-06-27 21:50:13', '2019-06-28 00:50:13', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=60', 0, 'scheduled-action', '', 3),
(61, 0, '2019-06-27 22:50:13', '2019-06-28 01:50:13', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1572c48017a5.04672575-STrQ6E2hQ9tOoeu37rfyIZfH4zIOYq1g', '', '', '2019-06-27 22:52:04', '2019-06-28 01:52:04', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=61', 0, 'scheduled-action', '', 3),
(62, 0, '2019-06-27 23:52:04', '2019-06-28 02:52:04', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1580eaada822.22985298-eiNupFLU7WITTjGr0RxnDA1QFEpgYy1f', '', '', '2019-06-27 23:52:26', '2019-06-28 02:52:26', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=62', 0, 'scheduled-action', '', 3),
(63, 0, '2019-06-28 00:52:26', '2019-06-28 03:52:26', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d158f127cb670.23503789-Jo2ApGAgY36WYKClAOLuOf41IJqIfomQ', '', '', '2019-06-28 00:52:50', '2019-06-28 03:52:50', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=63', 0, 'scheduled-action', '', 3),
(64, 0, '2019-06-28 01:52:50', '2019-06-28 04:52:50', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d159d3829fe03.08608696-COvl37Zjc1LeumA3iMmNCnbB7inFzbJ3', '', '', '2019-06-28 01:53:12', '2019-06-28 04:53:12', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=64', 0, 'scheduled-action', '', 3),
(65, 1, '2019-06-28 01:11:13', '2019-06-28 04:11:13', '', 'Camiseta', '', 'publish', 'open', 'closed', '', 'camiseta', '', '', '2019-06-30 11:17:27', '2019-06-30 14:17:27', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=65', 0, 'product', '', 0),
(66, 1, '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', '296485', '', 'inherit', 'open', 'closed', '', '296485', '', '', '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', 65, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/296485.jpg', 0, 'attachment', 'image/jpeg', 0),
(67, 1, '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', '296485-1', '', 'inherit', 'open', 'closed', '', '296485-1', '', '', '2019-06-28 01:10:49', '2019-06-28 04:10:49', '', 65, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/296485-1.jpg', 0, 'attachment', 'image/jpeg', 0),
(68, 0, '2019-06-28 02:53:12', '2019-06-28 05:53:12', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d177a9db1abc5.10751276-tncAAvshFmaSuFccfKEL1qmZiLnOHP5n', '', '', '2019-06-29 11:50:05', '2019-06-29 14:50:05', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=68', 0, 'scheduled-action', '', 3),
(69, 0, '2019-06-29 12:50:05', '2019-06-29 15:50:05', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d1788d0e25b00.02566634-qZLEEzYtCffp4bCZAhErHg8grw8oP761', '', '', '2019-06-29 12:50:40', '2019-06-29 15:50:40', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=69', 0, 'scheduled-action', '', 3),
(70, 1, '2019-06-29 11:56:04', '2019-06-29 14:56:04', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off', '', '', '2019-06-29 11:56:04', '2019-06-29 14:56:04', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=promocao&#038;p=70', 0, 'promocao', '', 0),
(71, 1, '2019-06-29 11:56:00', '2019-06-29 14:56:00', '', 'camiseta', '', 'inherit', 'open', 'closed', '', 'camiseta-2', '', '', '2019-06-29 11:56:00', '2019-06-29 14:56:00', '', 70, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/camiseta.png', 0, 'attachment', 'image/png', 0),
(72, 1, '2019-06-29 11:56:31', '2019-06-29 14:56:31', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off-2', '', '', '2019-06-29 11:56:31', '2019-06-29 14:56:31', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=promocao&#038;p=72', 0, 'promocao', '', 0),
(73, 1, '2019-06-29 11:56:51', '2019-06-29 14:56:51', '', 'Camiseta pela metade do preço', '', 'publish', 'closed', 'closed', '', 'camiseta-pela-metade-do-preco', '', '', '2019-06-29 11:56:51', '2019-06-29 14:56:51', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=promocao&#038;p=73', 0, 'promocao', '', 0),
(74, 1, '2019-06-29 11:57:09', '2019-06-29 14:57:09', '', 'Liquidção de<br>shorts esportivos', '', 'publish', 'closed', 'closed', '', 'liquidcao-deshorts-esportivos', '', '', '2019-06-29 11:57:09', '2019-06-29 14:57:09', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=promocao&#038;p=74', 0, 'promocao', '', 0),
(75, 1, '2019-06-29 11:57:36', '2019-06-29 14:57:36', '', 'Camisetas<br>com 50% off', '', 'publish', 'closed', 'closed', '', 'camisetascom-50-off-3', '', '', '2019-06-29 12:05:59', '2019-06-29 15:05:59', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=promocao&#038;p=75', 0, 'promocao', '', 0),
(76, 1, '2019-06-29 12:07:46', '2019-06-29 15:07:46', '', 'banner', '', 'inherit', 'open', 'closed', '', 'banner', '', '', '2019-06-29 12:07:46', '2019-06-29 15:07:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/banner.png', 0, 'attachment', 'image/png', 0),
(77, 1, '2019-06-29 12:38:07', '2019-06-29 15:38:07', '', 'neckties-210347_960_720', '', 'inherit', 'open', 'closed', '', 'neckties-210347_960_720', '', '', '2019-06-29 12:38:07', '2019-06-29 15:38:07', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/neckties-210347_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(78, 1, '2019-06-29 12:38:38', '2019-06-29 15:38:38', '', 'tartan-track-2678544_960_720', '', 'inherit', 'open', 'closed', '', 'tartan-track-2678544_960_720', '', '', '2019-06-29 12:38:38', '2019-06-29 15:38:38', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/tartan-track-2678544_960_720.jpg', 0, 'attachment', 'image/jpeg', 0),
(79, 1, '2019-06-29 12:48:21', '2019-06-29 15:48:21', '', '184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe', '', 'inherit', 'open', 'closed', '', '184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe', '', '', '2019-06-29 12:48:21', '2019-06-29 15:48:21', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/184-1848165_taylor-adidas-all-stars-converse-chuck-sneakers-shoe.jpg', 0, 'attachment', 'image/jpeg', 0),
(80, 1, '2019-06-29 12:48:23', '2019-06-29 15:48:23', '', '2000px-Adidas_klassisches_logo.svg', '', 'inherit', 'open', 'closed', '', '2000px-adidas_klassisches_logo-svg', '', '', '2019-06-29 12:48:23', '2019-06-29 15:48:23', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/2000px-Adidas_klassisches_logo.svg_.png', 0, 'attachment', 'image/png', 0),
(81, 1, '2019-06-29 12:48:27', '2019-06-29 15:48:27', '', 'asics', '', 'inherit', 'open', 'closed', '', 'asics', '', '', '2019-06-29 12:48:27', '2019-06-29 15:48:27', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/asics.png', 0, 'attachment', 'image/png', 0),
(82, 1, '2019-06-29 12:48:28', '2019-06-29 15:48:28', '', 'nike-logo-47A65A59D5-seeklogo.com', '', 'inherit', 'open', 'closed', '', 'nike-logo-47a65a59d5-seeklogo-com', '', '', '2019-06-29 12:48:28', '2019-06-29 15:48:28', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/nike-logo-47A65A59D5-seeklogo.com_.png', 0, 'attachment', 'image/png', 0),
(83, 0, '2019-06-29 13:50:40', '2019-06-29 16:50:40', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d179db73b1572.72475909-4vkrfmri52UPLErPpjOG3WQjGHb5sxLX', '', '', '2019-06-29 14:19:51', '2019-06-29 17:19:51', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=83', 0, 'scheduled-action', '', 3),
(84, 0, '2019-06-29 15:19:51', '2019-06-29 18:19:51', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17abc7636208.20251769-a37BbWttZLqbJdxFyAR8Gl2QiznQoOM0', '', '', '2019-06-29 15:19:51', '2019-06-29 18:19:51', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=84', 0, 'scheduled-action', '', 3),
(87, 1, '2019-06-29 15:47:19', '2019-06-29 18:47:19', '', 'Regatas', '', 'publish', 'open', 'closed', '', 'regatas', '', '', '2019-06-30 11:17:17', '2019-06-30 14:17:17', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=87', 0, 'product', '', 0),
(88, 1, '2019-06-29 15:47:03', '2019-06-29 18:47:03', '', 'fff', '', 'inherit', 'open', 'closed', '', 'fff', '', '', '2019-06-29 15:47:03', '2019-06-29 18:47:03', '', 87, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/fff.jpg', 0, 'attachment', 'image/jpeg', 0),
(89, 1, '2019-06-29 15:49:06', '2019-06-29 18:49:06', '', 'Tenis', '', 'publish', 'open', 'closed', '', 'tenis', '', '', '2019-06-30 11:17:06', '2019-06-30 14:17:06', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=89', 0, 'product', '', 0),
(90, 1, '2019-06-29 15:48:21', '2019-06-29 18:48:21', '', 'asd', '', 'inherit', 'open', 'closed', '', 'asd', '', '', '2019-06-29 15:48:21', '2019-06-29 18:48:21', '', 89, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/asd.jpg', 0, 'attachment', 'image/jpeg', 0),
(91, 1, '2019-06-29 15:50:43', '2019-06-29 18:50:43', '', 'Calça', '', 'publish', 'open', 'closed', '', 'calca', '', '', '2019-06-30 11:16:51', '2019-06-30 14:16:51', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=91', 0, 'product', '', 0),
(92, 1, '2019-06-29 15:50:25', '2019-06-29 18:50:25', '', 'Capturar', '', 'inherit', 'open', 'closed', '', 'capturar', '', '', '2019-06-29 15:50:25', '2019-06-29 18:50:25', '', 91, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/Capturar.jpg', 0, 'attachment', 'image/jpeg', 0),
(93, 0, '2019-06-29 17:20:08', '2019-06-29 20:20:08', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17c85b9e09a7.72898956-Zvebtak9cVt7bBCLGx7MttJSgrAImVpH', '', '', '2019-06-29 17:21:47', '2019-06-29 20:21:47', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=93', 0, 'scheduled-action', '', 3),
(94, 0, '2019-06-29 18:21:47', '2019-06-29 21:21:47', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17d72a97d8d7.57777513-W0ytQCqA65Wa74lzJfwopIEEm2Tv2sk5', '', '', '2019-06-29 18:24:58', '2019-06-29 21:24:58', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=94', 0, 'scheduled-action', '', 3),
(95, 0, '2019-06-29 19:24:58', '2019-06-29 22:24:58', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17e570398eb3.10669841-29aFuBJSmOI3QAhqV3lbO3PvRgrCd6rS', '', '', '2019-06-29 19:25:52', '2019-06-29 22:25:52', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=95', 0, 'scheduled-action', '', 3),
(96, 0, '2019-06-29 20:25:52', '2019-06-29 23:25:52', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d17f382823c98.61658026-ycd1ywdLEqunOhQRztTW40x0XjgM8iX3', '', '', '2019-06-29 20:25:54', '2019-06-29 23:25:54', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=96', 0, 'scheduled-action', '', 3),
(97, 1, '2019-06-29 19:32:43', '2019-06-29 22:32:43', '', 'Produto A', '', 'publish', 'open', 'closed', '', 'produto-a', '', '', '2019-06-30 11:16:39', '2019-06-30 14:16:39', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=97', 0, 'product', '', 0),
(98, 1, '2019-06-29 19:32:32', '2019-06-29 22:32:32', '', 'asasd', '', 'inherit', 'open', 'closed', '', 'asasd', '', '', '2019-06-29 19:32:32', '2019-06-29 22:32:32', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/asasd.jpg', 0, 'attachment', 'image/jpeg', 0),
(99, 1, '2019-06-29 19:32:33', '2019-06-29 22:32:33', '', 'bnb', '', 'inherit', 'open', 'closed', '', 'bnb', '', '', '2019-06-29 19:32:33', '2019-06-29 22:32:33', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/bnb.jpg', 0, 'attachment', 'image/jpeg', 0),
(100, 1, '2019-06-29 19:32:34', '2019-06-29 22:32:34', '', 'dfd', '', 'inherit', 'open', 'closed', '', 'dfd', '', '', '2019-06-29 19:32:34', '2019-06-29 22:32:34', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/dfd.jpg', 0, 'attachment', 'image/jpeg', 0),
(101, 1, '2019-06-29 19:32:35', '2019-06-29 22:32:35', '', 'kj', '', 'inherit', 'open', 'closed', '', 'kj', '', '', '2019-06-29 19:32:35', '2019-06-29 22:32:35', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/kj.jpg', 0, 'attachment', 'image/jpeg', 0),
(102, 1, '2019-06-29 19:32:36', '2019-06-29 22:32:36', '', 'mnm', '', 'inherit', 'open', 'closed', '', 'mnm', '', '', '2019-06-29 19:32:36', '2019-06-29 22:32:36', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/mnm.jpg', 0, 'attachment', 'image/jpeg', 0),
(103, 1, '2019-06-29 19:32:37', '2019-06-29 22:32:37', '', 'vb', '', 'inherit', 'open', 'closed', '', 'vb', '', '', '2019-06-29 19:32:37', '2019-06-29 22:32:37', '', 97, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/vb.jpg', 0, 'attachment', 'image/jpeg', 0),
(104, 1, '2019-06-29 19:33:19', '2019-06-29 22:33:19', '', 'Produto B', '', 'publish', 'open', 'closed', '', 'produto-b', '', '', '2019-06-30 11:16:29', '2019-06-30 14:16:29', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=104', 0, 'product', '', 0),
(105, 1, '2019-06-29 19:34:13', '2019-06-29 22:34:13', '', 'Produto C', '', 'publish', 'open', 'closed', '', 'produto-c', '', '', '2019-06-30 11:16:18', '2019-06-30 14:16:18', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=105', 0, 'product', '', 0),
(106, 1, '2019-06-29 19:34:51', '2019-06-29 22:34:51', '', 'Produto D', '', 'publish', 'open', 'closed', '', 'produto-d', '', '', '2019-06-30 11:16:08', '2019-06-30 14:16:08', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=106', 0, 'product', '', 0),
(107, 1, '2019-06-29 19:35:51', '2019-06-29 22:35:51', '', 'Produto E', '', 'publish', 'open', 'closed', '', 'produto-e', '', '', '2019-06-30 11:15:53', '2019-06-30 14:15:53', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=107', 0, 'product', '', 0),
(108, 1, '2019-06-29 19:36:37', '2019-06-29 22:36:37', '', 'Produto F', '', 'publish', 'open', 'closed', '', 'produto-f', '', '', '2019-06-30 11:15:35', '2019-06-30 14:15:35', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=108', 0, 'product', '', 0),
(109, 1, '2019-06-29 19:37:21', '2019-06-29 22:37:21', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\r\n<p class=\"desc-detalhes\"></p>', 'Produto D', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas!</p>', 'publish', 'open', 'closed', '', 'produto-d-2', '', '', '2019-06-30 15:36:32', '2019-06-30 18:36:32', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product&#038;p=109', 0, 'product', '', 0),
(110, 0, '2019-06-29 21:25:54', '2019-06-30 00:25:54', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d18046e3d00d4.70656836-gMv3m6MwVUlB6FJ60B5TSDjDNUKeCzhZ', '', '', '2019-06-29 21:38:06', '2019-06-30 00:38:06', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=110', 0, 'scheduled-action', '', 3),
(111, 0, '2019-06-29 22:38:06', '2019-06-30 01:38:06', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d181282140fe7.05224689-7T0jEcnGhiAnzacGl6dXMBPwF7zcSU3R', '', '', '2019-06-29 22:38:10', '2019-06-30 01:38:10', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=111', 0, 'scheduled-action', '', 3),
(112, 0, '2019-06-29 23:38:10', '2019-06-30 02:38:10', '[]', 'wc_admin_unsnooze_admin_notes', '', 'publish', 'open', 'closed', '', 'scheduled-action-5d18b972dc1827.56917889-uvuU45wZrwGSkuMXsA2HVwsIVWnUOJTk', '', '', '2019-06-30 10:30:26', '2019-06-30 13:30:26', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&#038;p=112', 0, 'scheduled-action', '', 3),
(113, 0, '2019-06-30 11:30:26', '2019-06-30 14:30:26', '[]', 'wc_admin_unsnooze_admin_notes', '', 'trash', 'open', 'closed', '', '', '', '', '2019-06-30 11:30:26', '2019-06-30 14:30:26', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=scheduled-action&p=113', 0, 'scheduled-action', '', 1),
(114, 1, '2019-06-30 14:10:32', '2019-06-30 17:10:32', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\n<p class=\"desc-detalhes\"></p>', 'Produto D', '<p class=\"descricao-texto\">Com a nova Chuteira Nike Mercurial Victory IV IC suas jogadas ficarão muito mais belas e perfeitas! Para manter um acabamento digno de uma chuteira TOP de linha, a Chuteira Nike Mercurial Victory IV IC recebe um revestimento em sintético, proporcionando maior durabilidade e conforto ao pé. A entressola feita em EVA garante a maciez, e o solado em borracha permite máxima estabilidade nas quadras.</p>\n<p class=\"desc-detalhes\">Marca: Nike</p>\n<p class=\"desc-detalhes\">Modelo: 003624</p>', 'inherit', 'closed', 'closed', '', '109-autosave-v1', '', '', '2019-06-30 14:10:32', '2019-06-30 17:10:32', '', 109, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/109-autosave-v1/', 0, 'revision', '', 0),
(115, 1, '2019-06-30 11:11:41', '2019-06-30 14:11:41', '', 'Produto D - Verde, M', 'Cor: Verde, Tamanho: M', 'publish', 'closed', 'closed', '', 'produto-d', '', '', '2019-06-30 11:14:49', '2019-06-30 14:14:49', '', 109, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product_variation&#038;p=115', 1, 'product_variation', '', 0),
(116, 1, '2019-06-30 11:13:21', '2019-06-30 14:13:21', '', 'Produto D - Amarelo, P', 'Cor: Amarelo, Tamanho: P', 'publish', 'closed', 'closed', '', 'produto-d-2', '', '', '2019-06-30 13:38:27', '2019-06-30 16:38:27', '', 109, 'http://centurysports.hcdesenvolvimentos.com/?post_type=product_variation&#038;p=116', 2, 'product_variation', '', 0),
(117, 1, '2019-06-30 11:18:12', '2019-06-30 14:18:12', '', 'Produto 3', '', 'inherit', 'closed', 'closed', '', '54-autosave-v1', '', '', '2019-06-30 11:18:12', '2019-06-30 14:18:12', '', 54, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/54-autosave-v1/', 0, 'revision', '', 0),
(118, 1, '2019-06-30 12:22:01', '2019-06-30 15:22:01', '', '08-06-2015-banner-esportes-kanui', '', 'inherit', 'open', 'closed', '', '08-06-2015-banner-esportes-kanui', '', '', '2019-06-30 12:22:01', '2019-06-30 15:22:01', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/08-06-2015-banner-esportes-kanui.jpg', 0, 'attachment', 'image/jpeg', 0),
(119, 1, '2019-06-30 12:28:14', '2019-06-30 15:28:14', '', 'cat_feminino_moletons', '', 'inherit', 'open', 'closed', '', 'cat_feminino_moletons', '', '', '2019-06-30 12:28:14', '2019-06-30 15:28:14', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/06/cat_feminino_moletons.jpg', 0, 'attachment', 'image/jpeg', 0),
(120, 1, '2019-06-30 14:48:36', '2019-06-30 17:48:36', '<!-- wp:shortcode -->\n[woocommerce_cart]\n<!-- /wp:shortcode -->', 'Carrinho', '', 'inherit', 'closed', 'closed', '', '7-revision-v1', '', '', '2019-06-30 14:48:36', '2019-06-30 17:48:36', '', 7, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/7-revision-v1/', 0, 'revision', '', 0),
(121, 1, '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 'Loja', '', 'inherit', 'closed', 'closed', '', '6-revision-v1', '', '', '2019-06-30 14:48:56', '2019-06-30 17:48:56', '', 6, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/6-revision-v1/', 0, 'revision', '', 0),
(122, 1, '2019-06-30 16:31:24', '2019-06-30 19:31:24', '<!-- wp:shortcode -->[woocommerce_my_account]<!-- /wp:shortcode -->', 'Minha Conta', '', 'inherit', 'closed', 'closed', '', '9-revision-v1', '', '', '2019-06-30 16:31:24', '2019-06-30 19:31:24', '', 9, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/9-revision-v1/', 0, 'revision', '', 0),
(123, 1, '2019-06-30 16:31:49', '2019-06-30 19:31:49', '<!-- wp:shortcode -->[woocommerce_checkout]<!-- /wp:shortcode -->', 'Finalizar Compra', '', 'inherit', 'closed', 'closed', '', '8-revision-v1', '', '', '2019-06-30 16:31:49', '2019-06-30 19:31:49', '', 8, 'http://centurysports.hcdesenvolvimentos.com/2019/06/30/8-revision-v1/', 0, 'revision', '', 0),
(127, 1, '2019-07-03 12:18:38', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-07-03 12:18:38', '0000-00-00 00:00:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=127', 0, 'page', '', 0),
(128, 1, '2019-07-03 12:23:00', '2019-07-03 15:23:00', '', 'Quem somos', '', 'publish', 'closed', 'closed', '', 'quem-somos', '', '', '2019-07-03 12:23:00', '2019-07-03 15:23:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=128', 0, 'page', '', 0),
(129, 1, '2019-07-03 12:23:00', '2019-07-03 15:23:00', '', 'Quem somos', '', 'inherit', 'closed', 'closed', '', '128-revision-v1', '', '', '2019-07-03 12:23:00', '2019-07-03 15:23:00', '', 128, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/128-revision-v1/', 0, 'revision', '', 0),
(130, 1, '2019-07-03 13:51:35', '2019-07-03 16:51:35', '', 'banner-quemsomos', '', 'inherit', 'open', 'closed', '', 'banner-quemsomos', '', '', '2019-07-03 13:51:35', '2019-07-03 16:51:35', '', 0, 'http://centurysports.hcdesenvolvimentos.com/wp-content/uploads/2019/07/banner-quemsomos.png', 0, 'attachment', 'image/png', 0);
INSERT INTO `cp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(131, 1, '2019-07-03 14:10:58', '2019-07-03 17:10:58', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Sedex:</strong> Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>PAC encomenda normal:</strong> Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.</p>\n<!-- /wp:paragraph -->', 'Termos e condicoes', '', 'publish', 'closed', 'closed', '', 'termos-e-condicoes', '', '', '2019-07-04 16:16:46', '2019-07-04 19:16:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=131', 0, 'page', '', 0),
(132, 1, '2019-07-03 14:10:58', '2019-07-03 17:10:58', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Sedex: Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>PAC encomenda normal: Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.</p>\n<!-- /wp:paragraph -->', 'Termos e condições', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2019-07-03 14:10:58', '2019-07-03 17:10:58', '', 131, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/131-revision-v1/', 0, 'revision', '', 0),
(133, 1, '2019-07-03 14:26:49', '2019-07-03 17:26:49', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Sedex: Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>PAC encomenda normal: Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.</p>\n<!-- /wp:paragraph -->', 'Termos e condicoes', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2019-07-03 14:26:49', '2019-07-03 17:26:49', '', 131, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/131-revision-v1/', 0, 'revision', '', 0),
(134, 1, '2019-07-03 14:33:47', '2019-07-03 17:33:47', '<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Requisitos Básicos para Troca ou Devolução. </strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"ordered\":true} -->\n<ol><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ol><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ol></li></ol>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'publish', 'closed', 'closed', '', 'politica-de-troca', '', '', '2019-07-04 10:51:32', '2019-07-04 13:51:32', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=134', 0, 'page', '', 0),
(135, 1, '2019-07-03 14:33:47', '2019-07-03 17:33:47', '<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Requisitos Básicos para Troca ou Devolução.</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"ordered\":true} -->\n<ol><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ol><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ol></li></ol>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2019-07-03 14:33:47', '2019-07-03 17:33:47', '', 134, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/134-revision-v1/', 0, 'revision', '', 0),
(136, 1, '2019-07-03 14:34:49', '2019-07-03 17:34:49', '<!-- wp:heading {\"level\":3} -->\n<h3> Requisitos Básicos para Troca ou Devolução. </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"ordered\":true} -->\n<ol><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ol><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ol></li></ol>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2019-07-03 14:34:49', '2019-07-03 17:34:49', '', 134, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/134-revision-v1/', 0, 'revision', '', 0),
(137, 1, '2019-07-03 14:35:08', '2019-07-03 17:35:08', '<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Requisitos Básicos para Troca ou Devolução. </strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"ordered\":true} -->\n<ol><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ol><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ol></li></ol>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2019-07-03 14:35:08', '2019-07-03 17:35:08', '', 134, 'http://centurysports.hcdesenvolvimentos.com/2019/07/03/134-revision-v1/', 0, 'revision', '', 0),
(138, 1, '2019-07-04 10:21:46', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'open', 'open', '', '', '', '', '2019-07-04 10:21:46', '0000-00-00 00:00:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?p=138', 0, 'post', '', 0),
(139, 1, '2019-07-04 10:51:16', '2019-07-04 13:51:16', '<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Requisitos Básicos para Troca ou Devolução. </strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list -->\n<ul><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ul><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ul></li></ul>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2019-07-04 10:51:16', '2019-07-04 13:51:16', '', 134, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/134-revision-v1/', 0, 'revision', '', 0),
(140, 1, '2019-07-04 10:51:32', '2019-07-04 13:51:32', '<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Requisitos Básicos para Troca ou Devolução. </strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Somente serão aceitas trocas e devoluções que cumpram, plenamente os seguintes requisitos básicos, como segue: </p>\n<!-- /wp:paragraph -->\n\n<!-- wp:list {\"ordered\":true} -->\n<ol><li> O CLIENTE deve comunicar à Loja sua intenção de devolver ou trocar o produto adquirido dentro de um prazo máximo de 7 dias corridos contados da data da entrega.</li><li> O produto deve estar no mesmo estado em que foi entregue, sem uso, nem lavagem, sem odores, sem manchas ou quaisquer alterações realizadas pelo CLIENTE (ex: ajustes de tamanho, cortes ou deformações, etc.); </li><li> O produto deve estar acompanhado de todos os seus acessórios (se aplicável). </li><li> O produto deve estar com a etiqueta da Loja afixada ao mesmo, sem qualquer tipo de violação; </li><li> O envio deve ser feito usando a mesma caixa protetora de papelão em que foi recebido o produto, para protegê-lo. Caso não possa ser utilizada a caixa protetora com a qual o produto foi entregue, o CLIENTE deverá devolvê-lo em uma caixa padrão dos Correios;</li><li> Deve ser enviada na caixa protetora, juntamente com o produto, uma cópia do DANFE (Documento Auxiliar da Nota Fiscal Eletrônica) que foi recebido com o mesmo;</li><li> No verso da cópia do DANFE deverá constar:<ol><li> O motivo da troca ou devolução;</li><li> Data e local;</li><li> Assinatura do remetente ou responsável;</li><li> Nome legível do remetente ou responsável;</li><li> Número do RG e CPF do remetente ou responsável.</li></ol></li></ol>\n<!-- /wp:list -->\n\n<!-- wp:paragraph -->\n<p> A Loja realizará a análise do produto devolvido ou a ser trocado pautando-se no princípio da boa-fé, sempre com o objetivo de satisfação de seus clientes. Na hipótese da Loja verificar que o produto devolvido não atende aos critérios necessários para a troca ou devolução acima, a Loja fica dispensada da obrigação de aceitar a devolução ou de fazer a troca, podendo reenviar o produto ao CLIENTE, sem consulta prévia. O produto reenviado estará acompanhado da justificativa do motivo da recusa.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3> <strong>Em caso de devolução involuntária- Correios</strong> </h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> Em caso de devolução por parte dos Correios, em pedidos com validação “endereço inválido”, “destinatário desconhecido”, “mudou-se”, “proprietário não encontrado” ou situação similar, o valor do frete de reenvio será de responsabilidade do cliente.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Ressarcimento de valores</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O valor do produto será devolvido de acordo com a forma de pagamento utilizada na compra, desde que observadas as condições descritas acima.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Cartão de Crédito</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno do seu cartão poderá ocorrer em até duas faturas subsequentes. Esse procedimento é de responsabilidade da administradora do cartão utilizado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:heading {\"level\":3} -->\n<h3><strong>Para pagamentos com Boleto Bancário ou Débito Online</strong></h3>\n<!-- /wp:heading -->\n\n<!-- wp:paragraph -->\n<p> O estorno será feito na conta corrente em até 10 dias úteis. Não concedemos créditos a terceiros.</p>\n<!-- /wp:paragraph -->', 'Política de troca', '', 'inherit', 'closed', 'closed', '', '134-revision-v1', '', '', '2019-07-04 10:51:32', '2019-07-04 13:51:32', '', 134, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/134-revision-v1/', 0, 'revision', '', 0),
(141, 1, '2019-07-04 11:10:24', '2019-07-04 14:10:24', '', 'Perguntas frequentes', '', 'publish', 'closed', 'closed', '', 'perguntas-frequentes', '', '', '2019-07-04 16:32:11', '2019-07-04 19:32:11', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=141', 0, 'page', '', 0),
(142, 1, '2019-07-04 11:10:24', '2019-07-04 14:10:24', '', 'Perguntas frequentes', '', 'inherit', 'closed', 'closed', '', '141-revision-v1', '', '', '2019-07-04 11:10:24', '2019-07-04 14:10:24', '', 141, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/141-revision-v1/', 0, 'revision', '', 0),
(143, 1, '2019-07-04 13:38:32', '2019-07-04 16:38:32', '<p>Sim, as peças oferecidas em nossa loja são todos produtos originais, com peças iguais às vendidas em qualquer loja do país das respectivas marcas comercializadas.</p>', 'Os produtos da loja são originais?', '', 'publish', 'closed', 'closed', '', 'como-funciona-a-entrega', '', '', '2019-07-04 13:54:14', '2019-07-04 16:54:14', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=143', 0, 'perguntas_frequentes', '', 0),
(146, 1, '2019-07-04 13:54:31', '2019-07-04 16:54:31', 'Os valores serão restituídos, de forma total ou parcial, da mesma forma de pagamento escolhida na compra:', 'Recebi um e-mail informando que o meu pagameto está pendente. E agora?', '', 'inherit', 'closed', 'closed', '', '144-autosave-v1', '', '', '2019-07-04 13:54:31', '2019-07-04 16:54:31', '', 144, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/144-autosave-v1/', 0, 'revision', '', 0),
(147, 1, '2019-07-04 13:57:26', '2019-07-04 16:57:26', 'As ações de frete grátis são informadas na barra central da página inicial do site.', 'O site possui Frete Grátis?', '', 'publish', 'closed', 'closed', '', 'o-site-possui-frete-gratis', '', '', '2019-07-04 13:57:26', '2019-07-04 16:57:26', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=147', 0, 'perguntas_frequentes', '', 0),
(148, 1, '2019-07-04 13:58:33', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-07-04 13:58:33', '0000-00-00 00:00:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=148', 0, 'page', '', 0),
(144, 1, '2019-07-04 13:39:30', '2019-07-04 16:39:30', 'A Loja garante a proteção de seus usuários não efetuando o armazenamento das informações de pagamento em nossa loja. Toda a operação de pagamento é efetuada em ambiente seguro por um gateway de pagamento qualificado com todos os requisitos de segurança necessários. Os dados dos clientes como nome, e-mail e outras informações pessoais são armazenadas e não compartilhadas.', 'O site da Loja é seguro?', '', 'publish', 'closed', 'closed', '', 'recebi-um-e-mail-informando-que-o-meu-pagameto-esta-pendente-e-agora', '', '', '2019-07-04 13:55:52', '2019-07-04 16:55:52', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=144', 0, 'perguntas_frequentes', '', 0),
(145, 1, '2019-07-04 13:39:59', '2019-07-04 16:39:59', 'O prazo para entrega varia de acordo com o local solicitado para entrega, forma de pagamento escolhida, disponibilidade do produto adquirido e forma de envio. Contudo estipulamos a média de cinco dias úteis para ter seu produto recebido via SEDEX; as demais formas de envio devem ser consideradas pelo prazo estipulado pelos Correios.', 'Em quanto tempo eu recebo meu pedido?', '', 'publish', 'closed', 'closed', '', 'como-recebo-o-dinheiro-de-volta-quando-cancelar-uma-compra', '', '', '2019-07-04 13:56:52', '2019-07-04 16:56:52', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=145', 0, 'perguntas_frequentes', '', 0),
(149, 1, '2019-07-04 14:03:08', '2019-07-04 17:03:08', '', 'Como comprar', '', 'publish', 'closed', 'closed', '', 'como-comprar', '', '', '2019-07-04 16:31:16', '2019-07-04 19:31:16', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=149', 0, 'page', '', 0),
(150, 1, '2019-07-04 14:03:08', '2019-07-04 17:03:08', '', 'Como comprar', '', 'inherit', 'closed', 'closed', '', '149-revision-v1', '', '', '2019-07-04 14:03:08', '2019-07-04 17:03:08', '', 149, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/149-revision-v1/', 0, 'revision', '', 0),
(151, 1, '2019-07-04 14:18:30', '2019-07-04 17:18:30', '<strong>Navegação</strong><br>\r\nPara encontrar um produto que deseja através da navegação, basta clicar na categoria apropriada no menu. Também é possível navegar pelas subcategorias dos produtos.<br>\r\n\r\n<strong>Busca</strong><br>\r\nCaso você queira procurar por algum produto específico, através das características ou modelo, basta utilizar a barra de busca, localizada no topo direito do site. Serão listados os produtos mais relevantes como resultado da busca realizada. Ao iniciar uma busca o sistema irá lhe indicar alguns produtos que são relacionados com o termo buscado.<br>\r\n\r\n<strong>Busca/Refino de busca</strong><br>\r\nAo efetuar a busca com alguma palavra-chave, a barra irá auxiliá-lo com filtros para que você possa ter uma visualização mais completa do resultado de busca. Assim, poderá chegar mais rápido ao produto procurado.<br>', '1º PASSO: ESCOLHENDO O(S) PRODUTO(S)', '', 'publish', 'closed', 'closed', '', '1o-passo-escolhendo-os-produtos', '', '', '2019-07-04 14:19:17', '2019-07-04 17:19:17', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=151', 0, 'como_comprar', '', 0),
(152, 1, '2019-07-04 14:21:48', '2019-07-04 17:21:48', 'Ao escolher o produto, basta selecionar seu tamanho e as cores (se houver) disponíveis neste tamanho serão exibidas. Após a seleção do tamanho e cor, escolha a quantidade e clique em COMPRAR. O produto será adicionado a seu carrinho de compras. Após o produto estar em seu carrinho você pode:\r\n<ul>\r\n 	<li>Alterar a quantidade dos produtos</li>\r\n 	<li>Acrescentar novos produtos ao carrinho</li>\r\n 	<li>Excluir um produto</li>\r\n 	<li>Simular o custo com o frete</li>\r\n</ul>\r\nAo optar por escolher outros produtos, você irá voltar à loja. Para adicionar outro produto ao carrinho, basta repetir o passo anterior. A maneira mais rápida de você saber o que tem na no seu carrinho é acessar o link que tem o nome \"Meu carrinho\", no alto da página à direita.<br>\r\n\r\n<strong>Calculando o valor do frete</strong><br>\r\nNa página do carrinho de compras você pode calcular o valor do frete do seu pedido. Basta colocar o seu CEP no campo relacionado ao frete e clicar em \"Calcular\".<br>\r\n\r\n<strong>Removendo produtos da seu carrinho</strong><br>\r\nA qualquer momento você poderá remover um item do seu carrinho. Basta clicar em \"Meu carrinho\" e você irá visualizar o que tem acumulado nele. Nesta página você poderá excluir o item da sua sacola. Para excluir um item, basta clicar no botão \"Remover\" correspondente ao mesmo.<br>', '2º PASSO: ADICIONANDO AO CARRINHO DE COMPRAS', '', 'publish', 'closed', 'closed', '', '2o-passo-adicionando-ao-carrinho-de-compras', '', '', '2019-07-04 15:01:41', '2019-07-04 18:01:41', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=152', 0, 'como_comprar', '', 0),
(153, 1, '2019-07-04 14:23:55', '2019-07-04 17:23:55', 'Ao escolher o produto, basta selecionar seu tamanho e as cores (se houver) disponíveis neste tamanho serão exibidas. Após a seleção do tamanho e cor, escolha a quantidade e clique em COMPRAR. O produto será adicionado a seu carrinho de compras. Após o produto estar em seu carrinho você pode:\n<ul>\n 	<li>Alterar a quantidade dos produtos</li>\n 	<li>Acrescentar novos produtos ao carrinho</li>\n 	<li>Excluir um produto</li>\n 	<li>Simular o custo com o frete</li>\n</ul>\nAo optar por escolher outros produtos, você irá voltar à loja. Para adicionar outro produto ao carrinho, basta repetir o passo anterior. A maneira mais rápida de você saber o que tem na no seu carrinho é acessar o link que tem o nome \"Meu carrinho\", no alto da página à direita.\n\n<strong>Calculando o valor do frete</strong>\nNa página do carrinho de compras você pode calcular o valor do frete do seu pedido. Basta colocar o seu CEP no campo relacionado ao frete e clicar em \"Calcular\".\n\n<strong>Removendo produtos da seu carrinho</strong>\nA qualquer momento você poderá remover um item do seu carrinho. Basta clicar em \"Meu carrinho\" e você irá visualizar o que tem acumulado nele. Nesta página você poderá excluir o item da sua sacola. Para excluir um item, basta clicar no botão \"Remover\" correspondente ao mesmo.', '2º PASSO: ADICIONANDO AO CARRINHO DE COMPRAS', '', 'inherit', 'closed', 'closed', '', '152-autosave-v1', '', '', '2019-07-04 14:23:55', '2019-07-04 17:23:55', '', 152, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/152-autosave-v1/', 0, 'revision', '', 0),
(154, 1, '2019-07-04 15:03:16', '2019-07-04 18:03:16', '<p>Ao concluir sua compra, basta clicar em “Finalizar compra”. Você será encaminhado para a página de identificação. Caso você já tenha cadastro, basta inserir seu e-mail e sua senha para prosseguir para o passo 4. Caso ainda não seja cadastrado, basta realizar um cadastro rápido em nosso site preenchendo um formulário com suas informações.</p>\r\n\r\n<p>Se você já é cadastrado mas esqueceu sua senha, basta clicar no link “Esqueceu sua senha?” abaixo do campo de email.</p>\r\n\r\n<p>OBS.: as informações fornecidas são de uso exclusivo para realizar análise de pedidos, emissão de notas fiscais e para contato para o cliente, não são fornecidos a terceiros. Nossa loja mantém sigilo de dados fornecidos pelo cliente.</p>', '3º PASSO: IDENTIFICAÇÃO', '', 'publish', 'closed', 'closed', '', '3o-passo-identificacao', '', '', '2019-07-04 15:04:09', '2019-07-04 18:04:09', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=154', 0, 'como_comprar', '', 0),
(155, 1, '2019-07-04 15:04:08', '2019-07-04 18:04:08', '<p>Ao concluir sua compra, basta clicar em “Finalizar compra”. Você será encaminhado para a página de identificação. Caso você já tenha cadastro, basta inserir seu e-mail e sua senha para prosseguir para o passo 4. Caso ainda não seja cadastrado, basta realizar um cadastro rápido em nosso site preenchendo um formulário com suas informações.</p>\n\n<p>Se você já é cadastrado mas esqueceu sua senha, basta clicar no link “Esqueceu sua senha?” abaixo do campo de email.</p>\n\n<p>OBS.: as informações fornecidas são de uso exclusivo para realizar análise de pedidos, emissão de notas fiscais e para contato para o cliente, não são fornecidos a terceiros. Nossa loja mantém sigilo de dados fornecidos pelo cliente.</p>', '3º PASSO: IDENTIFICAÇÃO', '', 'inherit', 'closed', 'closed', '', '154-autosave-v1', '', '', '2019-07-04 15:04:08', '2019-07-04 18:04:08', '', 154, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/154-autosave-v1/', 0, 'revision', '', 0),
(156, 1, '2019-07-04 15:04:50', '2019-07-04 18:04:50', '<p>Após se identificar, você deverá confirmar o endereço de entrega dos produtos. O site permite que você cadastre mais de um endereço de entrega para seus pedidos.</p>\r\n\r\n<p>Para usar o endereço de cadastro para entrega, clique em “Usar este endereço”.</p>\r\n\r\n<p>Se você optar por inserir um novo endereço de entrega para seu pedido, basta preencher o formulário e clicar no botão “Salvar e usar este endereço”. Você também pode editar um endereço já existente ou excluí-lo.</p>', '4º PASSO: ENDEREÇOS', '', 'publish', 'closed', 'closed', '', '4o-passo-enderecos', '', '', '2019-07-04 15:04:50', '2019-07-04 18:04:50', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=156', 0, 'como_comprar', '', 0),
(157, 1, '2019-07-04 15:05:26', '2019-07-04 18:05:26', '<p>Ao definir o endereço de entrega, será solicitado que você escolha a modalidade da entrega. Avalie o valor e o prazo de entrega de cada modalidade, escolha o que melhor atende sua necessidade e clique em “Continuar”. Caso queira alterar o endereço de entrega, basta clicar em “Alterar endereço de entrega”. Você será direcionado para o passo anterior.</p>', '5º PASSO: SERVIÇO DE ENTREGA', '', 'publish', 'closed', 'closed', '', '5o-passo-servico-de-entrega', '', '', '2019-07-04 15:05:26', '2019-07-04 18:05:26', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=157', 0, 'como_comprar', '', 0),
(158, 1, '2019-07-04 15:06:31', '2019-07-04 18:06:31', '<p>Você verá um breve resumo de seu pedido, com os produtos selecionados, os preços definidos e o endereço de entrega escolhido. Agora basta definir o plano de pagamento que mais se adequar ao seu objetivo:</p>\r\n \r\n<p><strong>Cartão de crédito</strong>\r\nDispomos de várias opções de Cartão de Crédito. Você deve preencher as informações do cartão conforme o indicado na tela finalização de compra e escolher a condição de pagamento. Após o despacho da compra, você receberá a Nota Fiscal Eletrônica por email. Ao definir as informações de pagamento, revise todas as informações de seu pedido. Caso estejam de acordo, clique em “Finalizar compra”.</p>\r\n \r\n<p><strong>Boleto Bancário</strong>\r\nÉ possível também efetuar seu pagamento utilizando boleto bancário. O boleto será gerado com a data de vencimento de um dia a mais que a data da compra. Lembramos que o produto será despachado assim que o banco repassar a informação que o boleto está pago.</p>\r\n \r\n<p>Você receberá por email o número e um breve resumo de seu pedido.</p>\r\n \r\n<p>Pronto! Seu pedido foi realizado com sucesso.</p>\r\n \r\n<p>Imediatamente, você receberá um e-mail de confirmação de pedido. Você será comunicado de todo o processo de sua compra por e-mail e também poderá consultar a qualquer momento o processo do pedido através da sua conta. Caso tenha dúvidas acesse nosso Atendimento Online ou nos envie um e-mail: <strong>contato@centurysports.com.br</strong></p>\r\n', '6º PASSO: PAGAMENTO', '', 'publish', 'closed', 'closed', '', '6o-passo-pagamento', '', '', '2019-07-04 15:06:31', '2019-07-04 18:06:31', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=como_comprar&#038;p=158', 0, 'como_comprar', '', 0),
(159, 1, '2019-07-04 15:51:06', '2019-07-04 18:51:06', '<p>Você pode se cadastrar no serviço de “avise-me quando este produto estiver disponível” localizado no item desejado. Dessa forma, se o produto retornar ao nosso estoque, você será imediatamente comunicado.</p>', 'Se o produto que procuro não estiver disponível, o que fazer?', '', 'publish', 'closed', 'closed', '', 'se-o-produto-que-procuro-nao-estiver-disponivel-o-que-fazer', '', '', '2019-07-04 15:54:02', '2019-07-04 18:54:02', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=159', 0, 'perguntas_frequentes', '', 0),
(160, 1, '2019-07-04 15:54:29', '2019-07-04 18:54:29', '<p>O cupom de desconto é uma ferramenta promocional, a qual é ativada somente quando o site oferecer uma promoção especial.</p>', 'Como funciona o Cupom de Desconto?', '', 'publish', 'closed', 'closed', '', 'como-funciona-o-cupom-de-desconto', '', '', '2019-07-04 15:54:29', '2019-07-04 18:54:29', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=160', 0, 'perguntas_frequentes', '', 0),
(161, 1, '2019-07-04 15:54:57', '2019-07-04 18:54:57', '<p>Não. Após o fechamento do pedido, não é possível fazer estas alterações. Para cancelamento de pedidos, entre em contato com nosso setor de atendimento.</p>', 'É possível acrescentar produto ou trocar endereço de entrega, modelo e cor depois do pedido fechado?', '', 'publish', 'closed', 'closed', '', 'e-possivel-acrescentar-produto-ou-trocar-endereco-de-entrega-modelo-e-cor-depois-do-pedido-fechado', '', '', '2019-07-04 15:54:57', '2019-07-04 18:54:57', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=161', 0, 'perguntas_frequentes', '', 0),
(162, 1, '2019-07-04 15:55:22', '2019-07-04 18:55:22', '<p>Não. O pedido será despachado após a aprovação do pagamento pela Administradora do Cartão, que pode demorar até 24 horas, após a confirmação de “pedido realizado”.</p>', 'Só receberei o pedido após quitar todas as parcelas do cartão?', '', 'publish', 'closed', 'closed', '', 'so-receberei-o-pedido-apos-quitar-todas-as-parcelas-do-cartao', '', '', '2019-07-04 15:55:22', '2019-07-04 18:55:22', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=162', 0, 'perguntas_frequentes', '', 0),
(163, 1, '2019-07-04 15:55:46', '2019-07-04 18:55:46', '<p>Se você ainda não recebeu seu pedido, consulte seu histórico através da opção “Meus pedidos”, localizada no menu “minha conta”. Para visualizar este menu faça seu login e verifique informações atualizadas sobre o seu pedido. Caso as informações não sejam suficientes, entre em contato pelo e-mail contato@centurysports.com.br</p>', 'Meu pedido já deveria ter chegado! Como proceder?', '', 'publish', 'closed', 'closed', '', 'meu-pedido-ja-deveria-ter-chegado-como-proceder', '', '', '2019-07-04 15:55:46', '2019-07-04 18:55:46', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=163', 0, 'perguntas_frequentes', '', 0),
(164, 1, '2019-07-04 16:05:28', '2019-07-04 19:05:28', '<p>Não. O sistema de estoque e distribuição do site é independente e as entregas serão feitas somente no endereço de entrega.</p>', 'É possível comprar no site e retirar na Loja física?', '', 'publish', 'closed', 'closed', '', 'e-possivel-comprar-no-site-e-retirar-na-loja-fisica', '', '', '2019-07-04 16:05:28', '2019-07-04 19:05:28', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=164', 0, 'perguntas_frequentes', '', 0),
(165, 1, '2019-07-04 16:05:57', '2019-07-04 19:05:57', '<p>Não, todas as trocas só serão efetuadas pela mesma forma da compra. Para maiores informações consulte \"Trocas e Devoluções\".</p>', 'É possível comprar no site e trocar na Loja física?', '', 'publish', 'closed', 'closed', '', 'e-possivel-comprar-no-site-e-trocar-na-loja-fisica', '', '', '2019-07-04 16:05:57', '2019-07-04 19:05:57', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?post_type=perguntas_frequentes&#038;p=165', 0, 'perguntas_frequentes', '', 0),
(166, 1, '2019-07-04 16:10:06', '0000-00-00 00:00:00', '', 'Rascunho automático', '', 'auto-draft', 'closed', 'closed', '', '', '', '', '2019-07-04 16:10:06', '0000-00-00 00:00:00', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=166', 0, 'page', '', 0),
(167, 1, '2019-07-04 16:13:29', '2019-07-04 19:13:29', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Sedex:</strong>&nbsp;Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>PAC encomenda normal:</strong>&nbsp;Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.<br></p>\n<!-- /wp:paragraph -->', 'Frete e entrega', '', 'publish', 'closed', 'closed', '', 'frete-e-entrega', '', '', '2019-07-04 16:31:51', '2019-07-04 19:31:51', '', 0, 'http://centurysports.hcdesenvolvimentos.com/?page_id=167', 0, 'page', '', 0);
INSERT INTO `cp_posts` (`ID`, `post_author`, `post_date`, `post_date_gmt`, `post_content`, `post_title`, `post_excerpt`, `post_status`, `comment_status`, `ping_status`, `post_password`, `post_name`, `to_ping`, `pinged`, `post_modified`, `post_modified_gmt`, `post_content_filtered`, `post_parent`, `guid`, `menu_order`, `post_type`, `post_mime_type`, `comment_count`) VALUES
(168, 1, '2019-07-04 16:13:29', '2019-07-04 19:13:29', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Sedex:</strong>&nbsp;Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>PAC encomenda normal:</strong>&nbsp;Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.<br></p>\n<!-- /wp:paragraph -->', 'Frete e entrega', '', 'inherit', 'closed', 'closed', '', '167-revision-v1', '', '', '2019-07-04 16:13:29', '2019-07-04 19:13:29', '', 167, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/167-revision-v1/', 0, 'revision', '', 0),
(169, 1, '2019-07-04 16:16:46', '2019-07-04 19:16:46', '<!-- wp:paragraph -->\n<p>As postagens são feitas no prazo de 1 a 2 dias úteis, contados após a confirmação do pagamento pelo cliente em nosso site, independente do tipo de frete contratado.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Em 90% dos casos a postagem é feita no dia seguinte ou até no mesmo dia da confirmação.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Estipulamos este prazo para a segurança e comodidade de nossos clientes. É importante ressaltar que este prazo começa a contar após a confirmação do pagamento em nosso sistema (compensação/confirmação bancária) e não após o pagamento feito por você.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>O Prazo de entrega varia de acordo com a forma de envio escolhida, enviamos pelos correios que tem as seguintes formas de envio:</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>Sedex:</strong> Prazo de entrega de 2 a 4 dias úteis.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p><strong>PAC encomenda normal:</strong> Prazo de entrega de 4 a 15 dias úteis dependendo da região.</p>\n<!-- /wp:paragraph -->\n\n<!-- wp:paragraph -->\n<p>Observe que os correios não trabalham dias de sábados domingos e feriados então estes dias não contam como dias úteis para o prazo de entrega.</p>\n<!-- /wp:paragraph -->', 'Termos e condicoes', '', 'inherit', 'closed', 'closed', '', '131-revision-v1', '', '', '2019-07-04 16:16:46', '2019-07-04 19:16:46', '', 131, 'http://centurysports.hcdesenvolvimentos.com/2019/07/04/131-revision-v1/', 0, 'revision', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_termmeta`
--

DROP TABLE IF EXISTS `cp_termmeta`;
CREATE TABLE IF NOT EXISTS `cp_termmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `term_id` (`term_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=76 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_termmeta`
--

INSERT INTO `cp_termmeta` (`meta_id`, `term_id`, `meta_key`, `meta_value`) VALUES
(5, 19, 'display_type', ''),
(4, 19, 'order', '0'),
(3, 15, 'product_count_product_cat', '0'),
(6, 19, 'thumbnail_id', '48'),
(7, 20, 'order', '0'),
(8, 20, 'display_type', ''),
(9, 20, 'thumbnail_id', '48'),
(10, 21, 'order', '0'),
(11, 21, 'display_type', ''),
(12, 21, 'thumbnail_id', '48'),
(13, 22, 'order', '0'),
(14, 22, 'display_type', ''),
(15, 22, 'thumbnail_id', '48'),
(16, 23, 'order', '0'),
(17, 23, 'display_type', ''),
(18, 23, 'thumbnail_id', '48'),
(19, 24, 'order', '0'),
(20, 24, 'display_type', ''),
(21, 24, 'thumbnail_id', '48'),
(64, 31, 'product_count_product_cat', '4'),
(63, 32, 'product_count_product_cat', '3'),
(62, 34, 'product_count_product_cat', '4'),
(61, 33, 'product_count_product_cat', '5'),
(28, 22, 'product_count_product_cat', '7'),
(30, 21, 'product_count_product_cat', '9'),
(31, 20, 'product_count_product_cat', '9'),
(32, 19, 'product_count_product_cat', '9'),
(33, 24, 'product_count_product_cat', '7'),
(60, 30, 'product_count_product_cat', '6'),
(35, 23, 'product_count_product_cat', '9'),
(36, 27, 'order', '0'),
(37, 27, 'display_type', ''),
(38, 27, 'thumbnail_id', '78'),
(39, 28, 'order', '0'),
(40, 28, 'display_type', ''),
(41, 28, 'thumbnail_id', '0'),
(42, 29, 'order', '0'),
(43, 29, 'display_type', ''),
(44, 29, 'thumbnail_id', '82'),
(45, 30, 'order', '0'),
(46, 30, 'display_type', ''),
(47, 30, 'thumbnail_id', '80'),
(48, 31, 'order', '0'),
(49, 31, 'display_type', ''),
(50, 31, 'thumbnail_id', '81'),
(51, 32, 'order', '0'),
(52, 32, 'display_type', ''),
(53, 32, 'thumbnail_id', '81'),
(54, 33, 'order', '0'),
(55, 33, 'display_type', ''),
(56, 33, 'thumbnail_id', '79'),
(57, 34, 'order', '0'),
(58, 34, 'display_type', ''),
(59, 34, 'thumbnail_id', '79'),
(65, 29, 'product_count_product_cat', '3'),
(66, 27, 'product_count_product_cat', '7'),
(67, 28, 'product_count_product_cat', '16'),
(68, 35, 'order', '0'),
(69, 35, 'display_type', ''),
(70, 35, 'thumbnail_id', '48'),
(71, 36, 'order', '0'),
(72, 36, 'display_type', ''),
(73, 36, 'thumbnail_id', '48'),
(74, 36, 'product_count_product_cat', '4'),
(75, 35, 'product_count_product_cat', '3');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_terms`
--

DROP TABLE IF EXISTS `cp_terms`;
CREATE TABLE IF NOT EXISTS `cp_terms` (
  `term_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `slug` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `term_group` bigint(10) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_id`),
  KEY `slug` (`slug`(191)),
  KEY `name` (`name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_terms`
--

INSERT INTO `cp_terms` (`term_id`, `name`, `slug`, `term_group`) VALUES
(1, 'Sem categoria', 'sem-categoria', 0),
(2, 'simple', 'simple', 0),
(3, 'grouped', 'grouped', 0),
(4, 'variable', 'variable', 0),
(5, 'external', 'external', 0),
(6, 'exclude-from-search', 'exclude-from-search', 0),
(7, 'exclude-from-catalog', 'exclude-from-catalog', 0),
(8, 'featured', 'featured', 0),
(9, 'outofstock', 'outofstock', 0),
(10, 'rated-1', 'rated-1', 0),
(11, 'rated-2', 'rated-2', 0),
(12, 'rated-3', 'rated-3', 0),
(13, 'rated-4', 'rated-4', 0),
(14, 'rated-5', 'rated-5', 0),
(15, 'Uncategorized', 'uncategorized', 0),
(16, 'wc-admin-notes', 'wc-admin-notes', 0),
(19, 'Camisetas', 'camisetas', 0),
(18, 'Menu Principal', 'menu-principal', 0),
(20, 'Calças', 'calcas', 0),
(21, 'Blusas', 'blusas', 0),
(22, 'Bermudas', 'bermudas', 0),
(23, 'Tênis', 'tenis', 0),
(24, 'Regatas', 'regatas', 0),
(35, 'Shorts', 'shorts', 0),
(27, 'Marcas', 'marcas', 0),
(28, 'Vestuário', 'vestuario', 0),
(29, 'Nike', 'nike', 0),
(30, 'Adidas', 'adidas', 0),
(31, 'Minzuno', 'minzuno', 0),
(32, 'Cross', 'cross', 0),
(33, 'ALL-STAR', 'all-star', 0),
(34, 'Converse', 'converse', 0),
(36, 'Meias', 'meias', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_term_relationships`
--

DROP TABLE IF EXISTS `cp_term_relationships`;
CREATE TABLE IF NOT EXISTS `cp_term_relationships` (
  `object_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `term_order` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`object_id`,`term_taxonomy_id`),
  KEY `term_taxonomy_id` (`term_taxonomy_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_term_relationships`
--

INSERT INTO `cp_term_relationships` (`object_id`, `term_taxonomy_id`, `term_order`) VALUES
(1, 1, 0),
(49, 16, 0),
(12, 2, 0),
(14, 16, 0),
(15, 16, 0),
(16, 16, 0),
(17, 16, 0),
(24, 16, 0),
(26, 18, 0),
(27, 18, 0),
(28, 18, 0),
(29, 18, 0),
(30, 18, 0),
(31, 16, 0),
(32, 18, 0),
(33, 18, 0),
(34, 18, 0),
(35, 18, 0),
(36, 18, 0),
(37, 18, 0),
(38, 18, 0),
(40, 18, 0),
(41, 18, 0),
(39, 18, 0),
(46, 16, 0),
(12, 22, 0),
(12, 32, 0),
(12, 21, 0),
(12, 20, 0),
(12, 19, 0),
(12, 24, 0),
(52, 32, 0),
(12, 23, 0),
(52, 22, 0),
(12, 34, 0),
(52, 21, 0),
(52, 20, 0),
(52, 19, 0),
(52, 24, 0),
(52, 34, 0),
(52, 23, 0),
(52, 2, 0),
(54, 22, 0),
(12, 33, 0),
(54, 21, 0),
(54, 20, 0),
(54, 19, 0),
(54, 24, 0),
(52, 33, 0),
(54, 23, 0),
(54, 2, 0),
(56, 22, 0),
(12, 30, 0),
(56, 21, 0),
(56, 20, 0),
(56, 19, 0),
(56, 24, 0),
(52, 30, 0),
(56, 23, 0),
(56, 2, 0),
(58, 22, 0),
(52, 29, 0),
(58, 20, 0),
(58, 19, 0),
(58, 24, 0),
(84, 16, 0),
(58, 23, 0),
(58, 2, 0),
(59, 16, 0),
(60, 16, 0),
(61, 16, 0),
(62, 16, 0),
(63, 16, 0),
(64, 16, 0),
(52, 31, 0),
(65, 21, 0),
(65, 20, 0),
(65, 19, 0),
(65, 24, 0),
(83, 16, 0),
(65, 23, 0),
(65, 2, 0),
(68, 16, 0),
(69, 16, 0),
(12, 31, 0),
(12, 29, 0),
(65, 30, 0),
(65, 33, 0),
(65, 34, 0),
(65, 32, 0),
(65, 31, 0),
(65, 29, 0),
(86, 16, 0),
(65, 22, 0),
(65, 36, 0),
(65, 35, 0),
(87, 24, 0),
(87, 2, 0),
(89, 23, 0),
(89, 2, 0),
(91, 20, 0),
(91, 2, 0),
(91, 30, 0),
(93, 16, 0),
(94, 16, 0),
(95, 16, 0),
(96, 16, 0),
(97, 30, 0),
(97, 33, 0),
(97, 34, 0),
(97, 21, 0),
(97, 19, 0),
(97, 36, 0),
(97, 35, 0),
(97, 2, 0),
(104, 30, 0),
(104, 33, 0),
(104, 20, 0),
(104, 19, 0),
(104, 23, 0),
(104, 2, 0),
(105, 19, 0),
(105, 2, 0),
(106, 31, 0),
(106, 21, 0),
(106, 2, 0),
(107, 22, 0),
(107, 21, 0),
(107, 36, 0),
(107, 2, 0),
(108, 36, 0),
(108, 23, 0),
(108, 2, 0),
(109, 21, 0),
(109, 20, 0),
(109, 35, 0),
(110, 16, 0),
(111, 16, 0),
(112, 16, 0),
(113, 16, 0),
(109, 4, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_term_taxonomy`
--

DROP TABLE IF EXISTS `cp_term_taxonomy`;
CREATE TABLE IF NOT EXISTS `cp_term_taxonomy` (
  `term_taxonomy_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `term_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `taxonomy` varchar(32) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `description` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `parent` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`term_taxonomy_id`),
  UNIQUE KEY `term_id_taxonomy` (`term_id`,`taxonomy`),
  KEY `taxonomy` (`taxonomy`)
) ENGINE=MyISAM AUTO_INCREMENT=37 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_term_taxonomy`
--

INSERT INTO `cp_term_taxonomy` (`term_taxonomy_id`, `term_id`, `taxonomy`, `description`, `parent`, `count`) VALUES
(1, 1, 'category', '', 0, 1),
(2, 2, 'product_type', '', 0, 15),
(3, 3, 'product_type', '', 0, 0),
(4, 4, 'product_type', '', 0, 1),
(5, 5, 'product_type', '', 0, 0),
(6, 6, 'product_visibility', '', 0, 0),
(7, 7, 'product_visibility', '', 0, 0),
(8, 8, 'product_visibility', '', 0, 0),
(9, 9, 'product_visibility', '', 0, 0),
(10, 10, 'product_visibility', '', 0, 0),
(11, 11, 'product_visibility', '', 0, 0),
(12, 12, 'product_visibility', '', 0, 0),
(13, 13, 'product_visibility', '', 0, 0),
(14, 14, 'product_visibility', '', 0, 0),
(15, 15, 'product_cat', '', 0, 0),
(16, 16, 'action-group', '', 0, 26),
(19, 19, 'product_cat', '', 28, 9),
(18, 18, 'nav_menu', '', 0, 15),
(20, 20, 'product_cat', '', 28, 9),
(21, 21, 'product_cat', '', 28, 9),
(22, 22, 'product_cat', '', 28, 7),
(23, 23, 'product_cat', '', 28, 9),
(24, 24, 'product_cat', '', 28, 7),
(27, 27, 'product_cat', '', 0, 0),
(28, 28, 'product_cat', '', 0, 0),
(29, 29, 'product_cat', '', 27, 3),
(30, 30, 'product_cat', '', 27, 6),
(31, 31, 'product_cat', '', 27, 4),
(32, 32, 'product_cat', '', 27, 3),
(33, 33, 'product_cat', '', 27, 5),
(34, 34, 'product_cat', '', 27, 4),
(35, 35, 'product_cat', '', 28, 3),
(36, 36, 'product_cat', '', 28, 4);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_usermeta`
--

DROP TABLE IF EXISTS `cp_usermeta`;
CREATE TABLE IF NOT EXISTS `cp_usermeta` (
  `umeta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`umeta_id`),
  KEY `user_id` (`user_id`),
  KEY `meta_key` (`meta_key`(191))
) ENGINE=MyISAM AUTO_INCREMENT=58 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_usermeta`
--

INSERT INTO `cp_usermeta` (`umeta_id`, `user_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'nickname', 'centurysports'),
(2, 1, 'first_name', ''),
(3, 1, 'last_name', ''),
(4, 1, 'description', ''),
(5, 1, 'rich_editing', 'true'),
(6, 1, 'syntax_highlighting', 'true'),
(7, 1, 'comment_shortcuts', 'false'),
(8, 1, 'admin_color', 'fresh'),
(9, 1, 'use_ssl', '0'),
(10, 1, 'show_admin_bar_front', 'true'),
(11, 1, 'locale', ''),
(12, 1, 'cp_capabilities', 'a:1:{s:13:\"administrator\";b:1;}'),
(13, 1, 'cp_user_level', '10'),
(14, 1, 'dismissed_wp_pointers', ''),
(15, 1, 'show_welcome_panel', '0'),
(16, 1, 'session_tokens', 'a:2:{s:64:\"53bf220d51f17fcc0d48c1af6da03c6e10dfb7be3a32023bde8e5404c7b98bb5\";a:4:{s:10:\"expiration\";i:1562437823;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562265023;}s:64:\"9ecc3cd733d6567aec94e30330b2c33359174fc355bf51d102c93f62a0ea9e53\";a:4:{s:10:\"expiration\";i:1562511261;s:2:\"ip\";s:3:\"::1\";s:2:\"ua\";s:115:\"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/75.0.3770.100 Safari/537.36\";s:5:\"login\";i:1562338461;}}'),
(17, 1, 'cp_dashboard_quick_press_last_post_id', '138'),
(18, 1, 'closedpostboxes_dashboard', 'a:0:{}'),
(19, 1, 'metaboxhidden_dashboard', 'a:4:{i:0;s:19:\"dashboard_right_now\";i:1;s:18:\"dashboard_activity\";i:2;s:21:\"dashboard_quick_press\";i:3;s:17:\"dashboard_primary\";}'),
(20, 1, '_woocommerce_tracks_anon_id', 'woo:tuOlm6PvDEuvTYmB/+vf1+uC'),
(21, 1, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:2:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:10;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1200;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1200;s:8:\"line_tax\";i:0;}s:32:\"a684eceee76fc522773286a895bc8436\";a:11:{s:3:\"key\";s:32:\"a684eceee76fc522773286a895bc8436\";s:10:\"product_id\";i:54;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:11.49;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:11.49;s:8:\"line_tax\";i:0;}}}'),
(22, 1, 'wc_last_active', '1562284800'),
(23, 1, 'cp_user-settings', 'libraryContent=browse&unfold=1&mfold=o&editor=html&hidetb=1'),
(24, 1, 'cp_user-settings-time', '1562263392'),
(25, 1, '_order_count', '0'),
(29, 1, 'closedpostboxes_destaque', 'a:1:{i:0;s:20:\"categoriaDestaquediv\";}'),
(28, 1, 'cp_r_tru_u_x', 'a:2:{s:2:\"id\";i:0;s:7:\"expires\";i:1561418612;}'),
(30, 1, 'metaboxhidden_destaque', 'a:2:{i:0;s:20:\"categoriaDestaquediv\";i:1;s:7:\"slugdiv\";}'),
(31, 1, 'managenav-menuscolumnshidden', 'a:5:{i:0;s:11:\"link-target\";i:1;s:11:\"css-classes\";i:2;s:3:\"xfn\";i:3;s:11:\"description\";i:4;s:15:\"title-attribute\";}'),
(32, 1, 'metaboxhidden_nav-menus', 'a:6:{i:0;s:21:\"add-post-type-product\";i:1;s:22:\"add-post-type-destaque\";i:2;s:22:\"add-post-type-promocao\";i:3;s:12:\"add-post_tag\";i:4;s:15:\"add-product_cat\";i:5;s:15:\"add-product_tag\";}'),
(33, 1, 'nav_menu_recently_edited', '18'),
(37, 2, 'nickname', 'agenciahcdesenvolvimentos'),
(38, 2, 'first_name', ''),
(39, 2, 'last_name', ''),
(40, 2, 'description', ''),
(41, 2, 'rich_editing', 'true'),
(42, 2, 'syntax_highlighting', 'true'),
(43, 2, 'comment_shortcuts', 'false'),
(44, 2, 'admin_color', 'fresh'),
(45, 2, 'use_ssl', '0'),
(46, 2, 'show_admin_bar_front', 'true'),
(47, 2, 'locale', ''),
(48, 2, 'cp_capabilities', 'a:1:{s:8:\"customer\";b:1;}'),
(49, 2, 'cp_user_level', '0'),
(51, 2, '_woocommerce_persistent_cart_1', 'a:1:{s:4:\"cart\";a:1:{s:32:\"2723d092b63885e0d7c260cc007e8b9d\";a:11:{s:3:\"key\";s:32:\"2723d092b63885e0d7c260cc007e8b9d\";s:10:\"product_id\";i:109;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"36f76fb62ac326633be66211f53a026c\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:123;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:123;s:8:\"line_tax\";i:0;}}}'),
(52, 2, 'wc_last_active', '1561852800');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_users`
--

DROP TABLE IF EXISTS `cp_users`;
CREATE TABLE IF NOT EXISTS `cp_users` (
  `ID` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_login` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_pass` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_nicename` varchar(50) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_email` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_url` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_registered` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `user_activation_key` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `user_status` int(11) NOT NULL DEFAULT '0',
  `display_name` varchar(250) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`ID`),
  KEY `user_login_key` (`user_login`),
  KEY `user_nicename` (`user_nicename`),
  KEY `user_email` (`user_email`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_users`
--

INSERT INTO `cp_users` (`ID`, `user_login`, `user_pass`, `user_nicename`, `user_email`, `user_url`, `user_registered`, `user_activation_key`, `user_status`, `display_name`) VALUES
(1, 'centurysports', '$P$B2G0NGM/dYPLgqXd3.KGFWtNjudq6j1', 'centurysports', 'devhcdesenvolvimentos@gmail.com', '', '2019-05-30 20:44:06', '', 0, 'centurysports'),
(2, 'agenciahcdesenvolvimentos', '$P$BrjECFE0V9J.6XKJyP77q0Es9XkA22/', 'agenciahcdesenvolvimentos', 'agenciahcdesenvolvimentos@gmail.com', '', '2019-06-30 19:34:30', '', 0, 'agenciahcdesenvolvimentos');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_admin_notes`
--

DROP TABLE IF EXISTS `cp_wc_admin_notes`;
CREATE TABLE IF NOT EXISTS `cp_wc_admin_notes` (
  `note_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `locale` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `title` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `icon` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `content_data` longtext COLLATE utf8mb4_unicode_520_ci,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_reminder` datetime DEFAULT NULL,
  `is_snoozable` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_admin_notes`
--

INSERT INTO `cp_wc_admin_notes` (`note_id`, `name`, `type`, `locale`, `title`, `content`, `icon`, `content_data`, `status`, `source`, `date_created`, `date_reminder`, `is_snoozable`) VALUES
(1, 'wc-admin-welcome-note', 'info', 'en_US', 'New feature(s)', 'Welcome to the new WooCommerce experience! In this new release you\'ll be able to have a glimpse of how your store is doing in the Dashboard, manage important aspects of your business (such as managing orders, stock, reviews) from anywhere in the interface, dive into your store data with a completely new Analytics section and more!', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-05-30 21:02:14', NULL, 0),
(2, 'wc-admin-wc-helper-connection', 'info', 'en_US', 'Connect to WooCommerce.com', 'Connect to get important product notifications and updates.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-05-30 21:02:14', NULL, 0),
(3, 'wc-admin-store-notice-giving-feedback', 'info', 'en_US', 'Giving feedback', 'Are you enjoying the new WooCommerce experience? We\'d love to get your feedback.', 'info', '{}', 'unactioned', 'woocommerce-admin', '2019-06-05 17:49:20', NULL, 0),
(4, 'wc-admin-mobile-app', 'info', 'en_US', 'Install Woo mobile app', 'Install the WooCommerce mobile app to manage orders, receive sales notifications, and view key metrics — wherever you are.', 'phone', '{}', 'unactioned', 'woocommerce-admin', '2019-06-05 17:49:20', NULL, 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_admin_note_actions`
--

DROP TABLE IF EXISTS `cp_wc_admin_note_actions`;
CREATE TABLE IF NOT EXISTS `cp_wc_admin_note_actions` (
  `action_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `note_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `query` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_primary` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`action_id`),
  KEY `note_id` (`note_id`)
) ENGINE=MyISAM AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_admin_note_actions`
--

INSERT INTO `cp_wc_admin_note_actions` (`action_id`, `note_id`, `name`, `label`, `query`, `status`, `is_primary`) VALUES
(1, 1, 'learn-more', 'Learn more', 'https://woocommerce.wordpress.com/', '', 0),
(2, 2, 'connect', 'Connect', '?page=wc-addons&section=helper', '', 0),
(3, 3, 'share-feedback', 'Share feedback', 'https://github.com/woocommerce/woocommerce-admin/issues/new/choose', '', 0),
(4, 4, 'learn-more', 'Learn more', 'https://woocommerce.com/mobile/', '', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_customer_lookup`
--

DROP TABLE IF EXISTS `cp_wc_customer_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_customer_lookup` (
  `customer_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `username` varchar(60) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `first_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_name` varchar(255) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `email` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `date_last_active` timestamp NULL DEFAULT NULL,
  `date_registered` timestamp NULL DEFAULT NULL,
  `country` char(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `postcode` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `city` varchar(100) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`customer_id`),
  UNIQUE KEY `user_id` (`user_id`),
  KEY `email` (`email`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_download_log`
--

DROP TABLE IF EXISTS `cp_wc_download_log`;
CREATE TABLE IF NOT EXISTS `cp_wc_download_log` (
  `download_log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `permission_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `user_ip_address` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  PRIMARY KEY (`download_log_id`),
  KEY `permission_id` (`permission_id`),
  KEY `timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_coupon_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_coupon_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_coupon_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `coupon_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `discount_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`coupon_id`),
  KEY `coupon_id` (`coupon_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_product_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_product_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_product_lookup` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `variation_id` bigint(20) UNSIGNED NOT NULL,
  `customer_id` bigint(20) UNSIGNED DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `product_qty` int(11) NOT NULL,
  `product_net_revenue` double NOT NULL DEFAULT '0',
  `product_gross_revenue` double NOT NULL DEFAULT '0',
  `coupon_amount` double NOT NULL DEFAULT '0',
  `tax_amount` double NOT NULL DEFAULT '0',
  `shipping_amount` double NOT NULL DEFAULT '0',
  `shipping_tax_amount` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`),
  KEY `product_id` (`product_id`),
  KEY `customer_id` (`customer_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_stats`
--

DROP TABLE IF EXISTS `cp_wc_order_stats`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_stats` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `parent_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `num_items_sold` int(11) NOT NULL DEFAULT '0',
  `gross_total` double NOT NULL DEFAULT '0',
  `tax_total` double NOT NULL DEFAULT '0',
  `shipping_total` double NOT NULL DEFAULT '0',
  `net_total` double NOT NULL DEFAULT '0',
  `returning_customer` tinyint(1) DEFAULT NULL,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `customer_id` bigint(20) UNSIGNED NOT NULL,
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`order_id`),
  KEY `date_created` (`date_created`),
  KEY `customer_id` (`customer_id`),
  KEY `status` (`status`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_order_tax_lookup`
--

DROP TABLE IF EXISTS `cp_wc_order_tax_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_order_tax_lookup` (
  `order_id` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `shipping_tax` double NOT NULL DEFAULT '0',
  `order_tax` double NOT NULL DEFAULT '0',
  `total_tax` double NOT NULL DEFAULT '0',
  PRIMARY KEY (`order_id`,`tax_rate_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `date_created` (`date_created`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_product_meta_lookup`
--

DROP TABLE IF EXISTS `cp_wc_product_meta_lookup`;
CREATE TABLE IF NOT EXISTS `cp_wc_product_meta_lookup` (
  `product_id` bigint(20) NOT NULL,
  `sku` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT '',
  `virtual` tinyint(1) DEFAULT '0',
  `downloadable` tinyint(1) DEFAULT '0',
  `min_price` decimal(10,2) DEFAULT NULL,
  `max_price` decimal(10,2) DEFAULT NULL,
  `onsale` tinyint(1) DEFAULT '0',
  `stock_quantity` double DEFAULT NULL,
  `stock_status` varchar(100) COLLATE utf8mb4_unicode_520_ci DEFAULT 'instock',
  `rating_count` bigint(20) DEFAULT '0',
  `average_rating` decimal(3,2) DEFAULT '0.00',
  `total_sales` bigint(20) DEFAULT '0',
  PRIMARY KEY (`product_id`),
  KEY `virtual` (`virtual`),
  KEY `downloadable` (`downloadable`),
  KEY `stock_status` (`stock_status`),
  KEY `stock_quantity` (`stock_quantity`),
  KEY `onsale` (`onsale`),
  KEY `min_max_price` (`min_price`,`max_price`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_wc_product_meta_lookup`
--

INSERT INTO `cp_wc_product_meta_lookup` (`product_id`, `sku`, `virtual`, `downloadable`, `min_price`, `max_price`, `onsale`, `stock_quantity`, `stock_status`, `rating_count`, `average_rating`, `total_sales`) VALUES
(12, '123', 0, 0, '120.00', '120.00', 0, 10, 'instock', 0, '0.00', 0),
(52, '', 0, 0, '0.00', '0.00', 0, NULL, 'instock', 0, '0.00', 0),
(54, '000999', 0, 0, '11.49', '11.49', 1, 88898, 'instock', 0, '0.00', 0),
(56, '455454', 0, 0, '242.00', '242.00', 1, 4543534, 'instock', 0, '0.00', 0),
(58, '2222', 0, 0, '12.50', '12.50', 0, 222, 'instock', 0, '0.00', 0),
(65, '9999', 0, 0, '180.00', '180.00', 0, 999, 'instock', 0, '0.00', 0),
(87, '88888', 0, 0, '123.00', '123.00', 0, 888, 'instock', 0, '0.00', 0),
(89, '7777', 0, 0, '50.00', '50.00', 0, 7777, 'instock', 0, '0.00', 0),
(91, '667777', 0, 0, '34.00', '34.00', 0, 777, 'instock', 0, '0.00', 0),
(97, '234242', 0, 0, '300.00', '300.00', 0, 43534, 'instock', 0, '0.00', 0),
(104, '6767667', 0, 0, '250.00', '250.00', 0, 4564564, 'instock', 0, '0.00', 0),
(105, '56565656', 0, 0, '350.00', '350.00', 0, 56456456, 'instock', 0, '0.00', 0),
(106, '45444554', 0, 0, '450.00', '450.00', 0, 4545454, 'instock', 0, '0.00', 0),
(107, '55454', 0, 0, '950.00', '950.00', 0, 3453453, 'instock', 0, '0.00', 0),
(108, '56555', 0, 0, '95.00', '95.00', 0, 3453453, 'instock', 0, '0.00', 0),
(109, '', 0, 0, '123.00', '200.00', 0, 123, 'instock', 0, '0.00', 0),
(115, '99999', 0, 0, '200.00', '200.00', 0, NULL, 'instock', 0, '0.00', 0),
(116, '999', 0, 0, '123.00', '123.00', 0, 12, 'instock', 0, '0.00', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_wc_webhooks`
--

DROP TABLE IF EXISTS `cp_wc_webhooks`;
CREATE TABLE IF NOT EXISTS `cp_wc_webhooks` (
  `webhook_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `status` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `delivery_url` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `secret` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `topic` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `date_created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_created_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `date_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `api_version` smallint(4) NOT NULL,
  `failure_count` smallint(10) NOT NULL DEFAULT '0',
  `pending_delivery` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`webhook_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_api_keys`
--

DROP TABLE IF EXISTS `cp_woocommerce_api_keys`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_api_keys` (
  `key_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `description` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `permissions` varchar(10) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_key` char(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `consumer_secret` char(43) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `nonces` longtext COLLATE utf8mb4_unicode_520_ci,
  `truncated_key` char(7) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `last_access` datetime DEFAULT NULL,
  PRIMARY KEY (`key_id`),
  KEY `consumer_key` (`consumer_key`),
  KEY `consumer_secret` (`consumer_secret`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_attribute_taxonomies`
--

DROP TABLE IF EXISTS `cp_woocommerce_attribute_taxonomies`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_attribute_taxonomies` (
  `attribute_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `attribute_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_label` varchar(200) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `attribute_type` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_orderby` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `attribute_public` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`attribute_id`),
  KEY `attribute_name` (`attribute_name`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_downloadable_product_permissions`
--

DROP TABLE IF EXISTS `cp_woocommerce_downloadable_product_permissions`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_downloadable_product_permissions` (
  `permission_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `download_id` varchar(36) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `product_id` bigint(20) UNSIGNED NOT NULL,
  `order_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `order_key` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_email` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `downloads_remaining` varchar(9) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `access_granted` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access_expires` datetime DEFAULT NULL,
  `download_count` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  PRIMARY KEY (`permission_id`),
  KEY `download_order_key_product` (`product_id`,`order_id`,`order_key`(16),`download_id`),
  KEY `download_order_product` (`download_id`,`order_id`,`product_id`),
  KEY `order_id` (`order_id`),
  KEY `user_order_remaining_expires` (`user_id`,`order_id`,`downloads_remaining`,`access_expires`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_log`
--

DROP TABLE IF EXISTS `cp_woocommerce_log`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_log` (
  `log_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `timestamp` datetime NOT NULL,
  `level` smallint(4) NOT NULL,
  `source` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `message` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `context` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`log_id`),
  KEY `level` (`level`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_order_itemmeta`
--

DROP TABLE IF EXISTS `cp_woocommerce_order_itemmeta`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_order_itemmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `order_item_id` (`order_item_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_order_items`
--

DROP TABLE IF EXISTS `cp_woocommerce_order_items`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_order_items` (
  `order_item_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `order_item_name` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `order_item_type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `order_id` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`order_item_id`),
  KEY `order_id` (`order_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_payment_tokenmeta`
--

DROP TABLE IF EXISTS `cp_woocommerce_payment_tokenmeta`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_payment_tokenmeta` (
  `meta_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `payment_token_id` bigint(20) UNSIGNED NOT NULL,
  `meta_key` varchar(255) COLLATE utf8mb4_unicode_520_ci DEFAULT NULL,
  `meta_value` longtext COLLATE utf8mb4_unicode_520_ci,
  PRIMARY KEY (`meta_id`),
  KEY `payment_token_id` (`payment_token_id`),
  KEY `meta_key` (`meta_key`(32))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_payment_tokens`
--

DROP TABLE IF EXISTS `cp_woocommerce_payment_tokens`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_payment_tokens` (
  `token_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `gateway_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `token` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL DEFAULT '0',
  `type` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `is_default` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`token_id`),
  KEY `user_id` (`user_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_sessions`
--

DROP TABLE IF EXISTS `cp_woocommerce_sessions`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_sessions` (
  `session_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `session_key` char(32) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_value` longtext COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `session_expiry` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`session_id`),
  UNIQUE KEY `session_key` (`session_key`)
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_sessions`
--

INSERT INTO `cp_woocommerce_sessions` (`session_id`, `session_key`, `session_value`, `session_expiry`) VALUES
(13, '1', 'a:11:{s:8:\"customer\";s:723:\"a:26:{s:2:\"id\";s:1:\"1\";s:13:\"date_modified\";s:0:\"\";s:8:\"postcode\";s:0:\"\";s:4:\"city\";s:0:\"\";s:9:\"address_1\";s:0:\"\";s:7:\"address\";s:0:\"\";s:9:\"address_2\";s:0:\"\";s:5:\"state\";s:2:\"PR\";s:7:\"country\";s:2:\"BR\";s:17:\"shipping_postcode\";s:0:\"\";s:13:\"shipping_city\";s:0:\"\";s:18:\"shipping_address_1\";s:0:\"\";s:16:\"shipping_address\";s:0:\"\";s:18:\"shipping_address_2\";s:0:\"\";s:14:\"shipping_state\";s:2:\"PR\";s:16:\"shipping_country\";s:2:\"BR\";s:13:\"is_vat_exempt\";s:0:\"\";s:19:\"calculated_shipping\";s:0:\"\";s:10:\"first_name\";s:0:\"\";s:9:\"last_name\";s:0:\"\";s:7:\"company\";s:0:\"\";s:5:\"phone\";s:0:\"\";s:5:\"email\";s:31:\"devhcdesenvolvimentos@gmail.com\";s:19:\"shipping_first_name\";s:0:\"\";s:18:\"shipping_last_name\";s:0:\"\";s:16:\"shipping_company\";s:0:\"\";}\";s:4:\"cart\";s:825:\"a:2:{s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";a:11:{s:3:\"key\";s:32:\"c20ad4d76fe97759aa27a0c99bff6710\";s:10:\"product_id\";i:12;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:10;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:1200;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:1200;s:8:\"line_tax\";i:0;}s:32:\"a684eceee76fc522773286a895bc8436\";a:11:{s:3:\"key\";s:32:\"a684eceee76fc522773286a895bc8436\";s:10:\"product_id\";i:54;s:12:\"variation_id\";i:0;s:9:\"variation\";a:0:{}s:8:\"quantity\";i:1;s:9:\"data_hash\";s:32:\"b5c1d5ca8bae6d4896cf1807cdf763f0\";s:13:\"line_tax_data\";a:2:{s:8:\"subtotal\";a:0:{}s:5:\"total\";a:0:{}}s:13:\"line_subtotal\";d:11.49;s:17:\"line_subtotal_tax\";i:0;s:10:\"line_total\";d:11.49;s:8:\"line_tax\";i:0;}}\";s:11:\"cart_totals\";s:412:\"a:15:{s:8:\"subtotal\";s:7:\"1211.49\";s:12:\"subtotal_tax\";d:0;s:14:\"shipping_total\";s:5:\"17.00\";s:12:\"shipping_tax\";d:0;s:14:\"shipping_taxes\";a:0:{}s:14:\"discount_total\";d:0;s:12:\"discount_tax\";d:0;s:19:\"cart_contents_total\";s:7:\"1211.49\";s:17:\"cart_contents_tax\";d:0;s:19:\"cart_contents_taxes\";a:0:{}s:9:\"fee_total\";s:4:\"0.00\";s:7:\"fee_tax\";d:0;s:9:\"fee_taxes\";a:0:{}s:5:\"total\";s:7:\"1228.49\";s:9:\"total_tax\";d:0;}\";s:15:\"applied_coupons\";s:6:\"a:0:{}\";s:22:\"coupon_discount_totals\";s:6:\"a:0:{}\";s:26:\"coupon_discount_tax_totals\";s:6:\"a:0:{}\";s:21:\"removed_cart_contents\";s:6:\"a:0:{}\";s:22:\"shipping_for_package_0\";s:401:\"a:2:{s:12:\"package_hash\";s:40:\"wc_ship_0c2d52daabb54db0a09a537b2c72cfb3\";s:5:\"rates\";a:1:{s:11:\"flat_rate:1\";O:16:\"WC_Shipping_Rate\":2:{s:7:\"\0*\0data\";a:6:{s:2:\"id\";s:11:\"flat_rate:1\";s:9:\"method_id\";s:9:\"flat_rate\";s:11:\"instance_id\";i:1;s:5:\"label\";s:9:\"Flat rate\";s:4:\"cost\";s:5:\"17.00\";s:5:\"taxes\";a:0:{}}s:12:\"\0*\0meta_data\";a:1:{s:5:\"Itens\";s:44:\"Produto Nome &times; 10, Produto 3 &times; 1\";}}}}\";s:25:\"previous_shipping_methods\";s:39:\"a:1:{i:0;a:1:{i:0;s:11:\"flat_rate:1\";}}\";s:23:\"chosen_shipping_methods\";s:29:\"a:1:{i:0;s:11:\"flat_rate:1\";}\";s:22:\"shipping_method_counts\";s:14:\"a:1:{i:0;i:1;}\";}', 1562437825);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zones`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zones`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zones` (
  `zone_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `zone_order` bigint(20) UNSIGNED NOT NULL,
  PRIMARY KEY (`zone_id`)
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zones`
--

INSERT INTO `cp_woocommerce_shipping_zones` (`zone_id`, `zone_name`, `zone_order`) VALUES
(1, 'Brazil', 0);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zone_locations`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zone_locations`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zone_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `location_id` (`location_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zone_locations`
--

INSERT INTO `cp_woocommerce_shipping_zone_locations` (`location_id`, `zone_id`, `location_code`, `location_type`) VALUES
(1, 1, 'BR', 'country');

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_shipping_zone_methods`
--

DROP TABLE IF EXISTS `cp_woocommerce_shipping_zone_methods`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_shipping_zone_methods` (
  `zone_id` bigint(20) UNSIGNED NOT NULL,
  `instance_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `method_id` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `method_order` bigint(20) UNSIGNED NOT NULL,
  `is_enabled` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`instance_id`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

--
-- Extraindo dados da tabela `cp_woocommerce_shipping_zone_methods`
--

INSERT INTO `cp_woocommerce_shipping_zone_methods` (`zone_id`, `instance_id`, `method_id`, `method_order`, `is_enabled`) VALUES
(1, 1, 'flat_rate', 1, 1),
(0, 2, 'flat_rate', 1, 1);

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_tax_rates`
--

DROP TABLE IF EXISTS `cp_woocommerce_tax_rates`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_tax_rates` (
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `tax_rate_country` varchar(2) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_state` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate` varchar(8) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_name` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  `tax_rate_priority` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_compound` int(1) NOT NULL DEFAULT '0',
  `tax_rate_shipping` int(1) NOT NULL DEFAULT '1',
  `tax_rate_order` bigint(20) UNSIGNED NOT NULL,
  `tax_rate_class` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT '',
  PRIMARY KEY (`tax_rate_id`),
  KEY `tax_rate_country` (`tax_rate_country`),
  KEY `tax_rate_state` (`tax_rate_state`(2)),
  KEY `tax_rate_class` (`tax_rate_class`(10)),
  KEY `tax_rate_priority` (`tax_rate_priority`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woocommerce_tax_rate_locations`
--

DROP TABLE IF EXISTS `cp_woocommerce_tax_rate_locations`;
CREATE TABLE IF NOT EXISTS `cp_woocommerce_tax_rate_locations` (
  `location_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `location_code` varchar(200) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `tax_rate_id` bigint(20) UNSIGNED NOT NULL,
  `location_type` varchar(40) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  PRIMARY KEY (`location_id`),
  KEY `tax_rate_id` (`tax_rate_id`),
  KEY `location_type_code` (`location_type`(10),`location_code`(20))
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;

-- --------------------------------------------------------

--
-- Estrutura da tabela `cp_woof_query_cache`
--

DROP TABLE IF EXISTS `cp_woof_query_cache`;
CREATE TABLE IF NOT EXISTS `cp_woof_query_cache` (
  `mkey` varchar(64) COLLATE utf8mb4_unicode_520_ci NOT NULL,
  `mvalue` text COLLATE utf8mb4_unicode_520_ci NOT NULL,
  KEY `mkey` (`mkey`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
