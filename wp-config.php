<?php
/**
 * As configurações básicas do WordPress
 *
 * O script de criação wp-config.php usa esse arquivo durante a instalação.
 * Você não precisa usar o site, você pode copiar este arquivo
 * para "wp-config.php" e preencher os valores.
 *
 * Este arquivo contém as seguintes configurações:
 *
 * * Configurações do MySQL
 * * Chaves secretas
 * * Prefixo do banco de dados
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/pt-br:Editando_wp-config.php
 *
 * @package WordPress
 */

// ** Configurações do MySQL - Você pode pegar estas informações com o serviço de hospedagem ** //
/** O nome do banco de dados do WordPress */
define( 'DB_NAME', 'projetos_centurysports_loja' );

/** Usuário do banco de dados MySQL */
define( 'DB_USER', 'root' );

/** Senha do banco de dados MySQL */
define( 'DB_PASSWORD', '123' );

/** Nome do host do MySQL */
define( 'DB_HOST', 'localhost' );

/** Charset do banco de dados a ser usado na criação das tabelas. */
define( 'DB_CHARSET', 'utf8mb4' );

/** O tipo de Collate do banco de dados. Não altere isso se tiver dúvidas. */
define('DB_COLLATE', '');

/**#@+
 * Chaves únicas de autenticação e salts.
 *
 * Altere cada chave para um frase única!
 * Você pode gerá-las
 * usando o {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org
 * secret-key service}
 * Você pode alterá-las a qualquer momento para invalidar quaisquer
 * cookies existentes. Isto irá forçar todos os
 * usuários a fazerem login novamente.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '>bae-Xok<d3HeMK~:%:7]s/!<bW0!,0M>t3)7].sRi;_Ak`2iqfXmGF=;<>YxK%e' );
define( 'SECURE_AUTH_KEY',  'CAy_J50?Icw&>9PkcXR+}<Sdqa~,+#61%vtqP8fGAfZQw@n-nU#`>2dEsq~e@#a ' );
define( 'LOGGED_IN_KEY',    'D{;zv|K**UC;Ap97q8#o[ m!XAzvvKzRUI,gs.w(jOj$dT!yN%h^12$x^iLR<&l*' );
define( 'NONCE_KEY',        '(>IVhLtU$Y5goO>tXS_lSO!cG!B.Dk6^ORm$@y/6B:[wWXzuO]9=5MFAASmsn@vo' );
define( 'AUTH_SALT',        '[^r|O~g`b7kM%_}0x3kCE23gLE_Afd`aJ/_;9ja)quPMgJF5d{9C9:Op;,@9>NDB' );
define( 'SECURE_AUTH_SALT', ':U8`:3$2r[oDlRL;i.NO+2]`F*OS}a9~9H!W)%<hwLsEFUP~Io|34J7V1>?69$XB' );
define( 'LOGGED_IN_SALT',   'wum{:m^dyXkF;U#&(#n5VACu[zB0,<hTVw 0?2oG:]G>Q2h;fEG:_W#aMZ9]ZpbQ' );
define( 'NONCE_SALT',       'KF^.Gjpxv|mBHd)6Z!SWb^j$SK;@ -B(:7RQu,ES!t#`0JHPduFn%;e+;}lapyrv' );

/**#@-*/

/**
 * Prefixo da tabela do banco de dados do WordPress.
 *
 * Você pode ter várias instalações em um único banco de dados se você der
 * um prefixo único para cada um. Somente números, letras e sublinhados!
 */
$table_prefix = 'cp_';

/**
 * Para desenvolvedores: Modo de debug do WordPress.
 *
 * Altere isto para true para ativar a exibição de avisos
 * durante o desenvolvimento. É altamente recomendável que os
 * desenvolvedores de plugins e temas usem o WP_DEBUG
 * em seus ambientes de desenvolvimento.
 *
 * Para informações sobre outras constantes que podem ser utilizadas
 * para depuração, visite o Codex.
 *
 * @link https://codex.wordpress.org/pt-br:Depura%C3%A7%C3%A3o_no_WordPress
 */
define('WP_DEBUG', false);

/* Isto é tudo, pode parar de editar! :) */

/** Caminho absoluto para o diretório WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Configura as variáveis e arquivos do WordPress. */
require_once(ABSPATH . 'wp-settings.php');
